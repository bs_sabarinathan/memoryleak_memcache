using System;
using System.Collections;
using System.Collections.Generic;


namespace BrandSystems.Marcom.Dal.Planning.Model
{

	/// <summary>
	/// EntityPresentationDao object for table 'PM_Entity_Presentation'.
	/// </summary>
	
	public partial class EntityPresentationDao : BaseDao, ICloneable
	{
		#region Member Variables

        protected int _entityid;
		protected DateTimeOffset _publishedon;
		protected string _content;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public EntityPresentationDao() {}
			
		public EntityPresentationDao(int pentityid, DateTimeOffset pPublishedOn, string pContent)
		{
            this._entityid = pentityid;
			this._publishedon = pPublishedOn; 
			this._content = pContent; 
		}
		
		#endregion
		
		#region Public Properties

        public virtual int EntityId
        {
            get { return _entityid; }
            set { _bIsChanged |= (_entityid != value); _entityid = value; }

        }

		public virtual DateTimeOffset PublishedOn
		{
			get { return _publishedon; }
			set { _bIsChanged |= (_publishedon != value); _publishedon = value; }
			
		}
		
		public virtual string Content
		{
			get { return _content; }
			set 
			{
			  if (value != null )
			    //throw new ArgumentOutOfRangeException("Content", "Content value, cannot contain more than 1073741823 characters");
			  _bIsChanged |= (_content != value); 
			  _content = value; 
			}
			
		}


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
			public const string EntityPresentationDao = "EntityPresentationDao";
            public const string Entityid = "Entityid";
			public const string PublishedOn = "PublishedOn";			
			public const string Content = "Content";			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
			public const string EntityPresentationDao = "EntityPresentationDao";
            public const string Entityid = "Entityid";
			public const string PublishedOn = "PublishedOn";			
			public const string Content = "Content";			
		}

		#endregion
		
		
	}
	
}
