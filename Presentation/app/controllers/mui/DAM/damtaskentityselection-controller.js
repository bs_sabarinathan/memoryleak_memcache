﻿(function (ng, app) {
    "use strict";

    function taskentitydamselectionCtrl($window, $location, $timeout, $scope, $cookies, $resource, $stateParams, $translate, DamTaskEntitySelectionService, $compile, $modalInstance, params) {
        var timerObj = {};
        $scope.PageNoobj = {};
        if (params.fromPlace == "task") {
            var view = $scope.DamViewName;
            var order = $scope.OrderbyName.orderbyname;
            $scope.DamViewName = {};
            $scope.OrderbyName = {};
            if ($scope.TaskBriefDetails == null || $scope.TaskBriefDetails == undefined) {
                $scope.TaskBriefDetails = {};
            }
            $scope.TaskBriefDetails.taskID = params.data.TaskId;
            $scope.TaskBriefDetails.EntityID = params.data.EntityID;
            $scope.DamViewName.viewName = view;
            $scope.OrderbyName.orderbyname = order;
        }
        $scope.callbackid = 0;
        var cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName + '\\' + TenantFilePath).replace(/\\/g, "\/");
        var TenantFilePath1 = TenantFilePath.replace(/\\/g, "\/");
        var imagesrcpath = TenantFilePath;
        if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
            TenantFilePath1 = cloudpath;
            imagesrcpath = cloudpath;
        }
        var apple_selected, tree;
        if ($scope.TaskBriefDetails.taskID == 0) {
            $scope.AssetaddTask = true;
            $scope.AssetEditTask = false;
            $scope.AsseTaskoption = "Select";
        } else {
            $scope.AssetaddTask = false;
            $scope.AssetEditTask = true;
            $scope.AsseTaskoption = "Save";
        }
        $scope.my_tree_handler = function (branch) {
            if ($scope.callbackid > 0) {
                $scope.PageNoobj.pageno = 1;
                var res = $.grep($scope.TaskStatusObj, function (e) {
                    return e.ID == $scope.FilterStatus.filterstatus;
                });
                $scope.selectedTree = branch;
                if ($scope.callbackid > 0) timerObj.getassetData = $timeout(function () {
                    GetAssetsData(branch.id, $scope.ParentEntityId, $scope.FilterStatus.filterstatus, $scope.OrderBy.order, $scope.PageNoobj.pageno);
                }, 100);
            } else {
                $scope.callbackid = $scope.callbackid + 1;
            }
            $scope.AssetSelectionFiles = $scope.TaskCreationAttachAsset.DAssetSelectionFiles;
        };
        $scope.FilterByOptionChange = function (id) {
            if (id != $scope.FilterStatus.filterstatus) {
                $scope.PageNoobj.pageno = 1;
                $scope.FilterStatus.filterstatus = id;
                var res = $.grep($scope.TaskStatusObj, function (e) {
                    return e.ID == id;
                });
                $scope.DamViewName.viewName = res[0].Name;
                var b;
                b = tree.get_selected_branch();
                if (b != null) {
                    GetAssetsData(b.id, $scope.ParentEntityId, $scope.FilterStatus.filterstatus, $scope.OrderBy.order, $scope.PageNoobj.pageno);
                }
            }
        }
        $scope.OrderbyOption = function (orderid) {
            var branch;
            branch = tree.get_selected_branch();
            if (orderid != $scope.OrderBy.order) {
                $scope.PageNoobj.pageno = 1;
                $scope.OrderBy.order = orderid;
                var orderobj = $.grep($scope.OrderbyObj, function (e) {
                    return e.ID == orderid;
                });
                $scope.OrderbyName.orderbyname = orderobj[0].Name;
                var res = $.grep($scope.TaskStatusObj, function (e) {
                    return e.ID == $scope.FilterStatus.filterstatus;
                });
                if (branch != null) {
                    GetAssetsData(branch.id, $scope.ParentEntityId, $scope.FilterStatus.filterstatus, $scope.OrderBy.order, $scope.PageNoobj.pageno);
                }
            }
        }
        $scope.my_data = [];
        $scope.my_tree = tree = {};
        $scope.ParentEntityId = 0;
        $scope.DamViewName.viewName = "Thumbnail";
        $scope.OrderbyName.orderbyname = "Creation date (Descending)";
        $scope.AssetTaskEditId = 0;
        $scope.openpopup = function () {
            $scope.AssetTaskEditId = params.TaskID;
            if ($scope.AssetTaskEditId > 0) { $scope.TaskBriefDetails.taskID = 0; }
            $scope.damtaskEntitySelectionVar.SelectedAssetFile.length = 0;
            if ($scope.TaskBriefDetails.taskID == 0) {
                $scope.AssetaddTask = true;
                $scope.AssetEditTask = false;
                $scope.AsseTaskoption = "Select";
            } else {
                $scope.AssetaddTask = false;
                $scope.AssetEditTask = true;
                $scope.AsseTaskoption = "Save";
            }
            $("#attachfromdam").modal("show");
            $scope.attachfromdam = true;
            $scope.ParentEntityId = ($scope.TaskBriefDetails.EntityID > 0) ? $scope.TaskBriefDetails.EntityID : $stateParams.ID;
            $scope.DamViewName.viewName = "Thumbnail";
            $scope.OrderbyName.orderbyname = "Creation date (Descending)";
            refreshViewObj();
            timerObj.pageload = $timeout(function () {
                PageLoad();
            }, 100);
        }
        $scope.UncheckAssetSelection = function () {
            for (var i = 0; i < $scope.AssetSelectionFiles.length; i++) {
                var id = $scope.AssetSelectionFiles[i];
                if ($scope.FilterStatus.filterstatus == 1) {
                    $scope.AssetSelectionClass["asset_" + id] = "th-Selection";
                    $scope.AssetChkClass["asset_" + id] = "checkbox";
                } else if ($scope.FilterStatus.filterstatus == 2) {
                    $scope.AssetSelectionClass["asset_" + id] = "th-Selection";
                    $scope.AssetChkClass["asset_" + id] = "checkbox";
                } else if ($scope.FilterStatus.filterstatus == 3) {
                    $scope.AssetSelectionClass["asset_" + id] = "th-Selection";
                    $scope.AssetChkClass["asset_" + id] = "checkbox";
                }
            }
            $scope.AssetSelectionFiles = [];
            $scope.damtaskEntitySelectionVar.SelectedAssetFile.length = 0;
        }
        $scope.SaveAssetFromTask = function (assetId, taskId) {
            DamTaskEntitySelectionService.AttachAssets(assetId, taskId, 0).then(function (result) {
                $('#save').removeAttr('disabled', 'disabled');
                if (result.Response == 0) NotifyError($translate.instant('LanguageContents.Res_4327.Caption'));
                else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4092.Caption'));
                }
                $("div[id^='attachfromdam']").modal('hide');
                $scope.closethispopup();
            });
        }
        $scope.SaveAttach = function () {
            $scope.AssetSelectionFiles = $scope.TaskCreationAttachAsset.DAssetSelectionFiles;
            $scope.TaskAttachAsset = $scope.TaskCreationAttachAsset;
            if ($scope.AssetSelectionFiles.length > 0) {
                if ($scope.TaskBriefDetails.taskID > 0) {
                    $('#save').attr('disabled', 'disabled');
                    $scope.SaveAssetFromTask($scope.AssetSelectionFiles, $scope.TaskBriefDetails.taskID)
                    $scope.UncheckAssetSelection();
                    $scope.ok();
                    $scope.attachfromdam = false;
                    ///timerObj.callbackattachment = $timeout(function() {
                    //$scope.$emit('CallBackAttachtak', $scope.TaskBriefDetails.taskID);
                    //}, 100);
                    $scope.$emit('callBackToDalimTaskEdit', $scope.TaskCreationAttachAsset.DAssetFiles);
                } else if ($scope.AssetTaskEditId > 0) {
                    if ($scope.AssetSelectionFiles.length > 1) bootbox.alert($translate.instant('Only one asset can be selected'));
                    else {
                        $scope.$emit('callBackToDalimTaskEdit', $scope.TaskCreationAttachAsset.DAssetFiles);
                        $scope.UncheckAssetSelection();
                        $scope.attachfromdam = false;
                        $scope.TaskCreationAttachAsset.DAssetSelectionFiles = [];
                        $scope.TaskCreationAttachAsset.DAssetFiles = [];
                        $scope.TaskCreationAttachAsset.DAssetDynamicData = [];
                        $scope.closethispopup();
                    }
                } else {
                    $scope.$emit('CallBackAttachtakdraw', $scope.TaskCreationAttachAsset.DAssetSelectionFiles);
                    $scope.UncheckAssetSelection();
                    $scope.attachfromdam = false;
                    $scope.closethispopup();
                }
            }
        }
        $scope.$on('removeassetAttach', function (event, ID) {
            timerObj.removeattachmentasset = $timeout(function () {
                if ($scope.FilterStatus.filterstatus == 1) {
                    $scope.AssetSelectionClass["asset_" + ID] = "th-Selection";
                    $scope.AssetChkClass["asset_" + ID] = "checkbox";
                } else if ($scope.FilterStatus.filterstatus == 2) {
                    $scope.AssetSelectionClass["asset_" + ID] = "th-Selection";
                    $scope.AssetChkClass["asset_" + ID] = "checkbox";
                } else if ($scope.FilterStatus.filterstatus == 3) {
                    $scope.AssetSelectionClass["asset_" + ID] = "th-Selection";
                    $scope.AssetChkClass["asset_" + ID] = "checkbox";
                }
                $scope.AssetSelectionFiles.splice($.inArray(ID, $scope.AssetSelectionFiles), 1);
            }, 100);
        });

        function PageLoad() {
            if (tree.length > 0) {
                tree.splice(0, tree.length);
            }
            $scope.my_tree = tree;
            if ($scope.my_data.length > 0) {
                $scope.my_data.splice(0, $scope.my_data.length);
            }
            $scope.selectedTree = {};
            $scope.subview = "Thumbnail";
            $scope.PageNoobj.pageno = 1;
            GetDAMViewSettings();
        }

        function GetDAMViewSettings() {
            DamTaskEntitySelectionService.GetDAMViewSettings().then(function (data) {
                if (data.Response != null) {
                    $scope.SettingsDamAttributes["ThumbnailSettings"] = data.Response[0].ThumbnailSettings;
                    $scope.SettingsDamAttributes["SummaryViewSettings"] = data.Response[0].SummaryViewSettings;
                    $scope.SettingsDamAttributes["ListViewSettings"] = data.Response[0].ListViewSettings;
                    $scope.try_async_load();
                }
            });
        }
        $scope.try_async_load = function () {
            $scope.doing_async = true;
            return $timeout(function () {
                DamTaskEntitySelectionService.GetEntityDamFolders($scope.ParentEntityId).then(function (data) {
                    var res = [];
                    res = JSON.parse(data.Response);
                    var Children = [];
                    if (res != null) {
                        for (var i = 0, child; child = res.Children[i++];) {
                            Children.push(child);
                        }
                        timerObj.getfolder = $timeout(function () {
                            if (Children.length > 0) {
                                $scope.initialTreeSelect = Children[0].Caption;
                            }
                            $scope.my_data = Children;
                            $scope.doing_async = false;
                            var treefolder = '<dam-task-tree tree-data="my_data" tree-control="my_tree"  on-select="my_tree_handler(branch)" expand-level="2" initial-selection="initialTreeSelect"></dam-task-tree>';
                            $("#taskentityDamFolder").html($compile(treefolder)($scope));
                            var res = $.grep($scope.TaskStatusObj, function (e) {
                                return e.ID == $scope.MulitipleFilterStatus;
                            });
                            $scope.doing_async = false;
                            tree.expand_all();
                            timerObj.gettree = $timeout(function () {
                                var b;
                                b = tree.get_selected_branch();
                                if (b != null) {
                                    var id = b.id;
                                } else {
                                    var id = 0;
                                }
                                LoadAssetsData(id, $scope.ParentEntityId, $scope.FilterStatus.filterstatus, $scope.OrderBy.order, $scope.PageNoobj.pageno);
                            }, 200);
                        }, 100);
                    }
                });
            }, 10);
        };

        function GetAssetsData(folderid, eid, filter, orderby, pageno) {
            DamTaskEntitySelectionService.GetEntityAsset(folderid, eid, filter, orderby, pageno, false).then(function (data) {
                var res = [];
                res = data.Response;
                if ($scope.FilterStatus.filterstatus == 1) LoadThumbnailView(res);
                else if ($scope.FilterStatus.filterstatus == 2) LoadSummaryView(res);
                else if ($scope.FilterStatus.filterstatus == 3) LoadListView(res);
            });
        }

        function LoadAssetsData(folderid, eid, filter, orderby, pageno) {
            DamTaskEntitySelectionService.GetEntityAsset(folderid, eid, filter, orderby, pageno, false).then(function (data) {
                var res = [];
                res = data.Response;
                if ($scope.FilterStatus.filterstatus == 1) LoadThumbnailView(res);
                else if ($scope.FilterStatus.filterstatus == 2) LoadSummaryView(res);
                else if ($scope.FilterStatus.filterstatus == 3) LoadListView(res);
                loadScrollSettings();
            });
        }

        function getMoreListItems() {
            $scope.PageNoobj.pageno = $scope.PageNoobj.pageno + 1;
            var b;
            b = tree.get_selected_branch();
            DamTaskEntitySelectionService.GetEntityAsset(b.id, $scope.ParentEntityId, $scope.FilterStatus.filterstatus, $scope.OrderBy.order, $scope.PageNoobj.pageno, false).then(function (data) {
                var res = [];
                res = data.Response;
                if ($scope.FilterStatus.filterstatus == 1) LoadThumbnailView(res);
                else if ($scope.FilterStatus.filterstatus == 2) LoadSummaryView(res);
                else if ($scope.FilterStatus.filterstatus == 3) LoadListView(res);
            });
        }

        function checkListItemContents() {
            getMoreListItems(function () { });
        }

        function loadScrollSettings() {
            $(".imglistSection").bind("scroll", function (e) {
                if ($(this)[0].scrollHeight - $(this).scrollTop() < 2000) {
                    checkListItemContents();
                }
            });
        }
        $scope.AssetChkClass = {};
        $scope.AssetSelectionClass = {};
        $scope.AssetFiles = [];
        var DynamicData = [];

        function refreshViewObj() {
            $(".imglistSection").scrollTop(0);
            var html = '';
            $(".taskentityassetdatadiv").html(html);
            $scope.AssetSelection = {};
            $scope.AssetChkClass = {};
            $scope.AssetSelectionClass = {};
            $scope.AssetFiles = [];
            $scope.AssetDynamicData = [];
        }

        function IsAssetSelected(assetid) {
            var remainRecord = [];
            remainRecord = $.grep($scope.AssetSelectionFiles, function (e) {
                return e == assetid;
            });
            if (remainRecord.length > 0) {
                return true;
            }
            return false;
        }

        function LoadThumbnailView(res) {
            var html = '';
            if ($scope.PageNoobj.pageno == 1) {
                refreshViewObj();
            }
            if (res != null) {
                var assets = [];
                assets = res[0].AssetFiles;
                for (var j = 0, asset; asset = assets[j++];) {
                    $scope.AssetFiles.push(asset);
                }
                DynamicData = res[0].AssetDynData;
                for (var k = 0, dyn; dyn = DynamicData[k++];) {
                    $scope.AssetDynamicData.push(dyn);
                }
                for (var i = 0, asset; asset = assets[i++];) {
                    html += '<div class="th-Box">';
                    $scope.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = false;
                    var assetselection = "";
                    if (IsAssetSelected(asset.AssetUniqueID)) {
                        assetselection = "th-Selection selected";
                        $scope.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox checked";
                    } else {
                        assetselection = $scope.AssetSelection["asset_'" + asset.AssetUniqueID + ""] == true ? "th-Selection selected" : "th-Selection";
                        $scope.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox";
                    }
                    $scope.AssetSelectionClass["asset_" + asset.AssetUniqueID + ""] = assetselection;
                    html += '<div id="select_' + i + '" ng-class=\"AssetSelectionClass.asset_' + asset.AssetUniqueID + '\" class="th-Selection">';
                    html += '<div class="th-chkBx">';
                    html += '<i class="icon-stop"></i>';
                    html += '<label class="checkbox checkbox-custom">';
                    html += '<input ng-click="checkSelection(' + asset.AssetUniqueID + ',' + asset.ActiveFileID + ', $event)" ng-model="AssetSelection.asset_' + asset.AssetUniqueID + '" id="chkBox_' + i + '" type="checkbox" />';
                    html += '<i id="chkBoxIcon_' + i + '" ng-class=\"AssetChkClass.asset_' + asset.AssetUniqueID + '\"></i>';
                    html += '</label>';
                    html += '</div>';
                    html += '<div class="th-Block">';
                    html += '<div class="th-ImgBlock">';
                    html += '<div  class="th-ImgContainer">';
                    if (asset.Category == 0) {
                        if (asset.Extension != null) {
                            if (asset.ProcessType > 0) {
                                if (asset.Status == 2) html += '<img id="file_' + asset.AssetUniqueID + '"  src="' + imagesrcpath + 'DAMFiles/Preview/Small_' + asset.FileGuid + '.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '\'" />';
                                else if (asset.Status == 1 || asset.Status == 0) html += '<img class="loadingImg" id=' + asset.ActiveFileID + ' data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' ng-click=\"Editasset(' + asset.AssetUniqueID + ')\" src="assets/img/loading.gif" alt="Image"   onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                                else if (asset.Status == 3) {
                                    html += '<img  src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                                }
                            } else {
                                html += '<img  src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                            }
                        } else html += '<img  src="' + imagesrcpath + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    } else if (asset.Category == 1) {
                        html += '<img id="file_' + asset.AssetUniqueID + '"  src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/BLANK.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    } else if (asset.Category == 2) {
                        html += '<img id="file_' + asset.AssetUniqueID + '"  src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/LINK.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    }
                    html += '</div>';
                    html += '</div>';
                    html += '<div class="th-DetailBlock">';
                    html += '<div class="th-ActionButtonContainer">';
                    html += '<span>';
                    html += '<i class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i>';
                    html += '<i class="icon-ok displayNone" my-qtip2 qtip-content="Approved and Combined with ..."></i>';
                    if (asset.Category == 0) {
                        if (asset.IsPublish == true) html += '<i id="Publishicon_' + asset.AssetID + '" data-location="publish" data-publishedon=\"' + asset.publishedON + '\" data-publishedby=\"' + asset.publishedby + '\"  dampublisheddetailtooltip id="Publishicon_' + asset.AssetID + '" class="icon-arrow-up" my-qtip2 qtip-content="Published"></i>';
                        else html += '<i id="Publishicon_' + asset.AssetID + '" class="icon-arrow-up displayNone" my-qtip2 qtip-content="Published"></i>';
                    } else {
                        if (asset.IsPublish == true) html += '<i id="Publishicon_' + asset.AssetUniqueID + '" data-location="publish" data-publishedon=\"' + asset.publishedON + '\" data-publishedby=\"' + asset.publishedby + '\"  dampublisheddetailtooltip id="Publishicon_' + asset.AssetID + '" class="icon-arrow-up" my-qtip2 qtip-content="Published"></i>';
                        else html += '<i id="Publishicon_' + asset.AssetUniqueID + '" class="icon-arrow-up displayNone" my-qtip2 qtip-content="Published"></i>';
                    }
                    html += '</span>';
                    html += '</div>';
                    html += '<div class="th-detail-eIconContainer">';
                    html += '<span class="th-eIcon" style="background-color: #' + asset.ColorCode + ';">' + asset.ShortDescription + '</span>';
                    html += '</div>';
                    html += '<div class="th-Details">';
                    html += GenereteDetailBlock(asset);
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                }
                $(".taskentityassetdatadiv").append($compile(html)($scope));
            } else {
                if ($scope.PageNoobj.pageno == 1) {
                    var emptyHtml = '<div class="emptyFolder">This folder is empty.</div>';
                    $(".taskentityassetdatadiv").html(emptyHtml);
                }
            }
        }
        $scope.checkSelection = function (assetid, activefileid, event) {
            var checkbox = event.target;
            if ($scope.FilterStatus.filterstatus == 1) $scope.AssetSelectionClass["asset_" + assetid] = checkbox.checked == true ? "th-Selection selected" : "th-Selection";
            else if ($scope.FilterStatus.filterstatus == 2) $scope.AssetSelectionClass["asset_" + assetid] = checkbox.checked == true ? "th-sum-Selection selected" : "th-sum-Selection";
            else if ($scope.FilterStatus.filterstatus == 3) $scope.AssetSelectionClass["asset_" + assetid] = checkbox.checked == true ? "li-Selection selected" : "li-Selection";
            var res = [];
            res = $.grep($scope.AssetFiles, function (e) {
                return e.AssetUniqueID == assetid;
            });
            var resdata = [];
            resdata = $.grep($scope.AssetDynamicData, function (e) {
                return e.ID == assetid;
            });
            if (checkbox.checked) {
                var remainRecord = [];
                var remainAssetSelectionRecord = [];
                var remainAssetFilesRecord = [];
                var remainRecorddata = [];
                if ($scope.TaskCreationAttachAsset != undefined) {
                    remainAssetSelectionRecord = $.grep($scope.TaskCreationAttachAsset.DAssetSelectionFiles, function (e) {
                        return e == assetid;
                    });
                    if (remainAssetSelectionRecord.length == 0) {
                        $scope.TaskCreationAttachAsset.DAssetSelectionFiles.push(assetid);
                    }
                    if ($scope.TaskCreationAttachAsset.DAssetFiles != undefined && $scope.TaskCreationAttachAsset.DAssetFiles != null) {
                        remainAssetFilesRecord = $.grep($scope.TaskCreationAttachAsset.DAssetFiles, function (e) {
                            return e.AssetID == assetid;
                        });
                        if (remainAssetFilesRecord.length == 0) {
                            if (res[0] != undefined) {
                                $scope.TaskCreationAttachAsset.DAssetFiles.push(res[0]);
                            }
                        }
                    } else {
                        if (res[0] != undefined) {
                            $scope.TaskCreationAttachAsset.DAssetFiles.push(res[0]);
                        }
                    }
                    if ($scope.TaskCreationAttachAsset.DAssetDynamicData != undefined && $scope.TaskCreationAttachAsset.DAssetDynamicData != null) {
                        remainRecorddata = $.grep($scope.TaskCreationAttachAsset.DAssetDynamicData, function (e) {
                            return e.ID == assetid;
                        });
                        if (remainRecorddata.length == 0) {
                            if (resdata[0] != undefined) {
                                $scope.TaskCreationAttachAsset.DAssetDynamicData.push(resdata[0]);
                            }
                        }
                    } else {
                        if (resdata[0] != undefined) {
                            $scope.TaskCreationAttachAsset.DAssetDynamicData.push(resdata[0]);
                        }
                    }
                }
                remainRecord = $.grep($scope.AssetSelectionFiles, function (e) {
                    return e == assetid;
                });
                if (remainRecord.length == 0) {
                    if (res[0] != undefined) {
                        $scope.damtaskEntitySelectionVar.SelectedAssetFile.push(assetid)
                        $scope.AssetSelectionFiles.push(assetid);
                    } else {
                        $scope.damtaskEntitySelectionVar.SelectedAssetFile.push(assetid)
                        $scope.AssetSelectionFiles.push(assetid);
                    }
                }
            } else {
                var remainRecord1 = [];
                remainRecord1 = $.grep($scope.damtaskEntitySelectionVar.SelectedAssetFile, function (e) {
                    return e == assetid;
                });
                if (remainRecord1.length > 0) {
                    $scope.damtaskEntitySelectionVar.SelectedAssetFile.splice($.inArray(remainRecord1[0], $scope.damtaskEntitySelectionVar.SelectedAssetFile), 1);
                }
                var remainRecord = [];
                remainRecord = $.grep($scope.AssetSelectionFiles, function (e) {
                    return e == assetid;
                });
                if (remainRecord.length > 0) {
                    $scope.AssetSelectionFiles.splice($.inArray(remainRecord[0], $scope.AssetSelectionFiles), 1);
                }
                if ($scope.TaskCreationAttachAsset != undefined) {
                    var remainRecordasset = [];
                    remainRecordasset = $.grep($scope.TaskCreationAttachAsset.DAssetSelectionFiles, function (e) {
                        return e == assetid;
                    });
                    if (remainRecordasset.length > 0) {
                        $scope.TaskCreationAttachAsset.DAssetSelectionFiles.splice($.inArray(remainRecordasset[0], $scope.TaskCreationAttachAsset.DAssetSelectionFiles), 1);
                    }
                    var remainAssetFilesRecord = [];
                    remainAssetFilesRecord = $.grep($scope.TaskCreationAttachAsset.DAssetFiles, function (e) {
                        return e.AssetID == assetid;
                    });
                    if (remainAssetFilesRecord.length > 0) {
                        $scope.TaskCreationAttachAsset.DAssetFiles.splice($.inArray(remainAssetFilesRecord[0], $scope.TaskCreationAttachAsset.DAssetFiles), 1);
                    }
                    var remainRecorddata = [];
                    remainRecorddata = $.grep($scope.TaskCreationAttachAsset.DAssetDynamicData, function (e) {
                        return e.ID == assetid;
                    });
                    if (remainRecorddata.length > 0) {
                        $scope.TaskCreationAttachAsset.DAssetDynamicData.splice($.inArray(remainRecorddata[0], $scope.TaskCreationAttachAsset.DAssetDynamicData), 1);
                    }
                }
            }
        }

        function GenereteDetailBlock(asset) {
            var attrRelation = [];
            attrRelation = $.grep($scope.SettingsDamAttributes["ThumbnailSettings"], function (e) {
                return e.assetType == asset.AssetTypeid;
            });
            var html = '';
            if (attrRelation.length > 0) {
                for (var i = 0, attr; attr = attrRelation[i++];) {
                    var cls = attr.ID == 68 ? "th-infoMain" : "th-infoSub";
                    if ($scope.AssetDynamicData.length > 0) {
                        var data = [];
                        data = $.grep($scope.AssetDynamicData, function (e) {
                            return e.ID == asset.AssetUniqueID;
                        });
                        if (data.length > 0) {
                            var attrval = [];
                            if (data[0]["" + attr.ID + ""] != "" && data[0]["" + attr.ID + ""] != null && data[0]["" + attr.ID + ""] != undefined) html += '<span class="' + cls.toString() + '">' + data[0]["" + attr.ID + ""] + '</span>';
                        } else html += '<span class="' + cls.toString() + '">-</span>';
                    } else html += '<span class="' + cls.toString() + '">-</span>';
                }
            }
            return html;
        }

        function LoadSummaryView(res) {
            var html = '';
            if ($scope.PageNoobj.pageno == 1) {
                refreshViewObj();
            }
            if (res != null) {
                var assets = [];
                assets = res[0].AssetFiles;
                for (var j = 0, asset; asset = assets[j++];) {
                    $scope.AssetFiles.push(asset);
                }
                DynamicData = res[0].AssetDynData;
                for (var k = 0, dyn; dyn = DynamicData[k++];) {
                    $scope.AssetDynamicData.push(dyn);
                }
                for (var i = 0, asset; asset = assets[i++];) {
                    $scope.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = false;
                    var assetselection = "";
                    if (IsAssetSelected(asset.AssetUniqueID)) {
                        assetselection = "th-sum-Selection selected";
                        $scope.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox checked";
                    } else {
                        assetselection = $scope.AssetSelection["asset_'" + asset.AssetUniqueID + ""] == true ? "th-sum-Selection selected" : "th-sum-Selection";
                        $scope.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox";
                    }
                    $scope.AssetSelectionClass["asset_" + asset.AssetUniqueID + ""] = assetselection;
                    html += '<div class="th-sum-Box"> ';
                    html += '        <div id="select_' + i + '" ng-class=\"AssetSelectionClass.asset_' + asset.AssetUniqueID + '\" class="th-sum-Selection"> ';
                    html += '            <div class="th-sum-chkBx"> ';
                    html += '                <i class="icon-stop"></i> ';
                    html += '                <label class="checkbox checkbox-custom"> ';
                    html += '                    <input ng-click="checkSelection(' + asset.AssetUniqueID + ',' + asset.ActiveFileID + ', $event)" ng-model="AssetSelection.asset_' + asset.AssetUniqueID + '" id="sumchkBoxIcon_' + i + '" type="checkbox" /> ';
                    html += '                    <i id="chkBoxIcon_' + i + '" ng-class=\"AssetChkClass.asset_' + asset.AssetUniqueID + '\"></i> ';
                    html += '                </label> ';
                    html += '            </div> ';
                    html += '            <div class="th-sum-Block"> ';
                    html += '                <div class="th-sum-leftSection"> ';
                    html += '                    <div class="th-sum-ImgBlock"> ';
                    html += '                        <div class="th-sum-ImgContainer"> ';
                    if (asset.Category == 0) {
                        if (asset.ProcessType > 0) {
                            if (asset.Status == 2) html += '<img  src="' + imagesrcpath + 'DAMFiles/Preview/Small_' + asset.FileGuid + '' + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '\'" />';
                            else if (asset.Status == 1 || asset.Status == 0) html += '<img class="loadingImg" id=' + asset.AssetUniqueID + ' data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' ng-click=\"Editasset(' + asset.AssetUniqueID + ')\" src="assets/img/loading.gif" alt="Image"   onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                            else if (asset.Status == 3) {
                                html += '<img  src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                            }
                        } else {
                            html += '<img  src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                        }
                    } else if (asset.Category == 1) {
                        html += '<img  src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/BLANK.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    } else if (asset.Category == 2) {
                        html += '<img  src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/LINK.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    }
                    html += '                        </div> ';
                    html += '                    </div> ';
                    html += '                    <div class="th-sum-baiscDetailBlock"> ';
                    html += '                        <div class="th-sum-ActionButtonContainer"> ';
                    html += '                            <span> ';
                    html += '                                <i class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i> ';
                    html += '                                <i class="icon-ok displayNone" my-qtip2 qtip-content="Approved and Combined with ..."></i> ';
                    if (asset.Category == 0) {
                        if (asset.IsPublish == true) html += '                                   <i data-publishedon=\"' + asset.publishedON + '\" data-location="publish" data-publishedby=\"' + asset.publishedby + '\"  dampublisheddetailtooltip id="Publishicon_' + asset.AssetID + '"  class="icon-arrow-up" title="Published"></i>';
                        else html += '                                <i id="Publishicon_' + asset.AssetID + '"  class="icon-arrow-up displayNone" my-qtip2 qtip-content="Published"></i> ';
                    } else {
                        if (asset.IsPublish == true) html += '                                    <i data-publishedon=\"' + asset.publishedON + '\" data-location="publish" data-publishedby=\"' + asset.publishedby + '\"  dampublisheddetailtooltip id="Publishicon_' + asset.AssetUniqueID + '"  class="icon-arrow-up" title="Published"></i>';
                        else html += '                                <i id="Publishicon_' + asset.AssetUniqueID + '"  class="icon-arrow-up displayNone" my-qtip2 qtip-content="Published"></i> ';
                    }
                    html += '                            </span> ';
                    html += '                        </div> ';
                    html += '                        <div class="th-sum-detail-eIconContainer"> ';
                    html += '                               <span class="th-sum-eIcon" style="background-color: #' + asset.ColorCode + ';">' + asset.ShortDescription + '</span>';
                    html += '                        </div> ';
                    html += '                        <div class="th-sum-basicDetails"> ';
                    html += GenereteSummaryDetailBlock(asset);
                    html += '                        </div> ';
                    html += '                    </div> ';
                    html += '                </div> ';
                    html += '                <div class="th-sum-rightSection"> ';
                    html += '                    <div class="th-sum-expandedDetailBlock"> ';
                    html += '                        <div class="th-sum-expandedHeader"> ';
                    html += '                            <h4>' + asset.AssetName + '</h4> ';
                    html += '                        </div> ';
                    html += '                        <div class="th-sum-expandedDetails"> ';
                    html += '                            <form class="form-horizontal"> ';
                    html += DrawSummaryBlock(asset);
                    html += '                            </form> ';
                    html += '                        </div> ';
                    html += '                    </div> ';
                    html += '                </div> ';
                    html += '            </div> ';
                    html += '        </div> ';
                    html += '    </div> ';
                }
                $(".taskentityassetdatadiv").append($compile(html)($scope));
            } else {
                if ($scope.PageNoobj.pageno == 1) {
                    var emptyHtml = '<div class="emptyFolder">This folder is empty.</div>';
                    $(".taskentityassetdatadiv").html(emptyHtml);
                }
            }
        }

        function GenereteSummaryDetailBlock(asset) {
            var html = '';
            var attrRelation = [];
            attrRelation = $.grep($scope.SettingsDamAttributes["ThumbnailSettings"], function (e) {
                return e.assetType == asset.AssetTypeid;
            });
            if (attrRelation.length > 0) {
                for (var i = 0, attr; attr = attrRelation[i++];) {
                    var cls = attr.ID == 68 ? "th-sum-infoMain" : "th-sum-infoSub";
                    if ($scope.AssetDynamicData.length > 0) {
                        var data = [];
                        data = $.grep($scope.AssetDynamicData, function (e) {
                            return e.ID == asset.AssetUniqueID;
                        });
                        if (data.length > 0) {
                            var attrval = [];
                            if (data[0]["" + attr.ID + ""] != "" && data[0]["" + attr.ID + ""] != null && data[0]["" + attr.ID + ""] != undefined) html += '<span class="' + cls.toString() + '">' + data[0]["" + attr.ID + ""] + '</span>';
                            else html += '<span class="' + cls.toString() + '">-</span>';
                        } else html += '<span class="' + cls.toString() + '">-</span>';
                    } else html += '<span class="' + cls.toString() + '">-</span>';
                }
            }
            return html;
        }

        function DrawSummaryBlock(asset) {
            var html = '';
            var attrRelation = [];
            attrRelation = $.grep($scope.SettingsDamAttributes["SummaryViewSettings"], function (e) {
                return e.assetType == asset.AssetTypeid;
            });
            if (attrRelation.length > 0) {
                for (var i = 0, attr; attr = attrRelation[i++];) {
                    html += '<div class="control-group"> ';
                    html += '                                    <label class="control-label">' + attr.Caption + '</label> ';
                    html += '                                    <div class="controls"> ';
                    if ($scope.AssetDynamicData.length > 0) {
                        var data = [];
                        data = $.grep($scope.AssetDynamicData, function (e) {
                            return e.ID == asset.AssetUniqueID;
                        });
                        if (data.length > 0) {
                            if (data[0]["" + attr.ID + ""] != "" && data[0]["" + attr.ID + ""] != null && data[0]["" + attr.ID + ""] != undefined) html += '<span class="control-label">' + data[0]["" + attr.ID + ""] + '</span>';
                            else html += '<span class="control-label">-</span>';
                        } else {
                            html += '<label class="control-label">-</label> ';
                        }
                    } else {
                        html += '<label class="control-label">-</label> ';
                    }
                    html += '                                    </div> ';
                    html += '                                </div> ';
                }
            }
            return html;
        }

        function LoadListView(res) {
            var html = '';
            if ($scope.PageNoobj.pageno == 1) {
                refreshViewObj();
            }
            if (res != null) {
                var assets = [];
                assets = res[0].AssetFiles;
                for (var j = 0, asset; asset = assets[j++];) {
                    $scope.AssetFiles.push(asset);
                }
                DynamicData = res[0].AssetDynData;
                for (var k = 0, dyn; dyn = DynamicData[k++];) {
                    $scope.AssetDynamicData.push(dyn);
                }
                if ($scope.PageNoobj.pageno == 1) {
                    html += '<div>';
                    html += '    <table id="AssetListViewTableTask" class="table table-normal-left ListviewTable">';
                    html += '        <thead>';
                    html += GenerateHeader();
                    html += '        </thead>';
                    html += '        <tbody>';
                    html += GenerateList(assets);
                    html += '        </tbody>';
                    html += '    </table>';
                    html += '</div>';
                    $(".taskentityassetdatadiv").append($compile(html)($scope));
                } else {
                    html += GenerateList(assets);
                    $("#AssetListViewTableTask tbody").append($compile(html)($scope));
                }
            } else {
                if ($scope.PageNoobj.pageno == 1) {
                    var emptyHtml = '<div class="emptyFolder">This folder is empty.</div>';
                    $(".taskentityassetdatadiv").html(emptyHtml);
                }
            }
        }

        function GenerateHeader() {
            var html = '';
            html += '<tr>';
            html += '   <th>';
            html += '       <label class="checkbox checkbox-custom">';
            html += '           <input id="SelectAllAssets" ng-click="SelectAllAssetFiles($event)" type="checkbox" />';
            html += '           <i class="checkbox"></i>';
            html += '       </label>';
            html += '   </th>';
            html += '   <th>';
            html += '       <span>Header</span>';
            html += '   </th>';
            if ($scope.SettingsDamAttributes["ListViewSettings"].length > 0) {
                for (var i = 0, attr; attr = $scope.SettingsDamAttributes["ListViewSettings"][i++];) {
                    html += '   <th>';
                    html += '       <span>' + attr.Caption + '</span>';
                    html += '   </th>';
                }
            }
            html += '</tr>';
            return html;
        }

        function GenerateList(assets) {
            var html = '';
            for (var j = 0, asset; asset = assets[j++];) {
                $scope.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = false;
                var assetselection = "";
                if (IsAssetSelected(asset.AssetUniqueID)) {
                    assetselection = "li-Selection selected";
                    $scope.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox checked";
                } else {
                    assetselection = $scope.AssetSelection["asset_'" + asset.AssetUniqueID + ""] == true ? "li-Selection selected" : "li-Selection";
                    $scope.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox";
                }
                $scope.AssetSelectionClass["asset_" + asset.AssetUniqueID + ""] = assetselection;
                html += '<tr ng-class=\"AssetSelectionClass.asset_' + asset.AssetUniqueID + '\">';
                html += '   <td>';
                html += '       <label class="checkbox checkbox-custom">';
                html += '           <input ng-click="checkSelection(' + asset.AssetUniqueID + ',' + asset.ActiveFileID + ', $event)" ng-model="AssetSelection.asset_' + asset.AssetUniqueID + '" id="chkBox_' + i + '" type="checkbox" />';
                html += '           <i id="chkBoxIcon_' + i + '" ng-class=\"AssetChkClass.asset_' + asset.AssetUniqueID + '\"></i>';
                html += '       </label>';
                html += '   </td>';
                html += '   <td>';
                html += '       <span class="thmbListviewImgSpan">';
                html += '           <div class="thmbListview-eIconContainer"> ';
                html += '               <span class="thmbListview-eIcon" style="background-color: #' + asset.ColorCode + ';">' + asset.ShortDescription + '</span>';
                html += '           </div>';
                html += '           <span class="thmbListviewImgName">' + asset.AssetName + '</span>';
                html += '       </span>';
                html += '   </td>';
                if ($scope.SettingsDamAttributes["ListViewSettings"].length > 0) {
                    for (var i = 0, attr; attr = $scope.SettingsDamAttributes["ListViewSettings"][i++];) {
                        html += '<td>';
                        if ($scope.AssetDynamicData.length > 0) {
                            var data = [];
                            data = $.grep($scope.AssetDynamicData, function (e) {
                                return e.ID == asset.AssetUniqueID;
                            });
                            if (data.length > 0) {
                                if (data[0]["" + attr.ID + ""] != "") html += '<span class="thmbListviewDescSpan">' + data[0]["" + attr.ID + ""] + '</span>';
                                else html += '<span class="thmbListviewDescSpan">-</span>';
                            } else html += '<span class="thmbListviewDescSpan">-</span>';
                        } else html += '<span class="thmbListviewDescSpan">-</span>';
                        html += '</td>';
                    }
                }
                html += '</tr>';
            }
            return html;
        }

        function CheckPreviewGenerator() {
            getPreviewIDs();
        }

        function getPreviewIDs() {
            var assetids = [];
            for (var i = 0; i < $('.loadingImg').length; i++) {
                assetids.push($('.loadingImg')[i].id);
            }
            if (assetids.length > 0) {
                var data = {};
                data.AssetIDs = assetids;
                DamTaskEntitySelectionService.CheckPreviewGeneratorID(data).then(function (result) {
                    if (result.Response != null) {
                        var generatedids = result.Response.split(',');
                        for (var j = 0; j < generatedids.length; j++) {
                            if (generatedids[j] != "") {
                                var src = $('#' + generatedids[j]).attr('data-src');
                                try {
                                    var upd = $.grep($scope.AssetFilesObj.AssetFiles, function (e) {
                                        return e.ActiveFileID == generatedids[j]
                                    });
                                    if (upd.length > 0) {
                                        $.grep($scope.AssetFilesObj.AssetFiles, function (e) {
                                            return e.ActiveFileID == generatedids[j]
                                        })[0].Status = 2;
                                    }
                                    var lightboxid = $.grep($scope.damlightbox.lightboxselection, function (e) {
                                        return e.AssetId == upd[0].AssetID
                                    });
                                    if (lightboxid.length > 0) {
                                        $.grep($scope.damlightbox.lightboxselection, function (e) {
                                            return e.AssetId == upd[0].AssetID
                                        })[0].Status = 2;
                                        $.grep($scope.damlightbox.lightboxselection, function (e) {
                                            return e.AssetId == upd[0].AssetID
                                        })[0].AssetStatus = 2;
                                    }
                                } catch (e) { }
                                var extn = '.jpg';
                                if ($('#AssetEditpopup').length > 0 && $('#AssetEditpopup').css('display') == 'block') {
                                    if (GlobalAssetEditID == generatedids[j]) {
                                        $('#asseteditimage').attr('src', imagesrcpath + 'DAMFiles/Preview/Big_' + src + '' + extn + '?' + generateUniqueTracker());
                                    }
                                }
                                $('#' + generatedids[j]).removeClass('loadingImg');
                                $('#' + generatedids[j]).attr('src', imagesrcpath + 'DAMFiles/Preview/Small_' + src + '' + extn + '?' + generateUniqueTracker());
                            }
                        }
                    }
                });
            }
            PreviewGeneratorTimer = $timeout(function () {
                CheckPreviewGenerator()
            }, 3000);
        }
        $scope.LoadAssetListViewMetadata = function (qtip_id, assetid, backcolor, shortdesc) {
            $scope.tempEntityTypeTreeText = '';
            if (assetid != null) {
                DamTaskEntitySelectionService.GetAttributeDetails(assetid).then(function (attrList) {
                    $scope.ActiveVersion = $.grep(attrList.Response.Files, function (e) {
                        return e.ID == attrList.Response.ActiveFileID;
                    })[0];
                    var html = '';
                    html += '       <div class="lv-ImgDetailedPreview">';
                    html += '           <div class="idp-Box"> ';
                    html += '               <div class="idp-Block"> ';
                    html += '                   <div class="idp-leftSection"> ';
                    html += '                       <div class="idp-ImgBlock"> ';
                    html += '                           <div class="idp-ImgDiv"> ';
                    html += '                               <div class="idp-ImgContainer"> ';
                    if (attrList.Response.Category == 0) {
                        if ($scope.ActiveVersion.ProcessType > 0) {
                            if ($scope.ActiveVersion.Status == 2) html += '<img src="' + imagesrcpath + 'DAMFiles/Preview/Small_' + $scope.ActiveVersion.Fileguid + '' + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"  onerror="this.onerror=null;this.src=\'' + imagesrcpath + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '\'" />';
                            else if ($scope.ActiveVersion.Status == 1 || $scope.ActiveVersion.Status == 0) html += '<img class="loadingImg" id=' + $scope.ActiveVersion.ActiveFileID + ' data-extn=' + $scope.ActiveVersion.Extension + ' data-src=' + $scope.ActiveVersion.Fileguid + ' ng-click=\"Editasset(' + $scope.ActiveVersion.AssetUniqueID + ')\" src="assets/img/loading.gif" alt="Image"   onerror="this.onerror=null;this.src=\'' + imagesrcpath + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                            else if ($scope.ActiveVersion.Status == 3) {
                                html += '<img src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/' + $scope.ActiveVersion.Extension.substring(1, $scope.ActiveVersion.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"  onerror="this.onerror=null;this.src=\'' + imagesrcpath + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                            }
                        } else {
                            html += '<img src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/' + $scope.ActiveVersion.Extension.substring(1, $scope.ActiveVersion.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"  onerror="this.onerror=null;this.src=\'' + imagesrcpath + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                        }
                    } else if (attrList.Response.Category == 1) {
                        html += '<img src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/BLANK.jpg?' + generateUniqueTracker() + '" alt="Image"  onerror="this.onerror=null;this.src=\'' + imagesrcpath + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    } else if (attrList.Response.Category == 2) {
                        html += '<img src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/LINK.jpg?' + generateUniqueTracker() + '" alt="Image"  onerror="this.onerror=null;this.src=\'' + imagesrcpath + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    }
                    html += '                               </div> ';
                    html += '                           </div> ';
                    html += '                       </div> ';
                    html += '                       <div class="idp-baiscDetailBlock"> ';
                    html += '                           <div class="idp-ActionButtonContainer"> ';
                    html += '                               <span> ';
                    html += '                                   <i class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i> ';
                    html += '                                   <i class="icon-ok displayNone" my-qtip2 qtip-content="Approved and Combined with ..."></i> ';
                    if (attrList.Response.Category == 0) {
                        if (attrList.Response.IsPublish == true) html += '                                   <i id="Publishicon_' + attrList.Response.ID + '"  class="icon-arrow-up" my-qtip2 qtip-content="Published"></i> ';
                        else html += '                                   <i id="Publishicon_' + attrList.Response.ID + '"  class="icon-arrow-up displayNone" my-qtip2 qtip-content="Published"></i> ';
                        if (attrList.Response.LinkedAssetID != 0) html += '<i id="Linkicon_' + attrList.Response.ID + '" class="icon-link" my-qtip2 qtip-content="Linked"></i>';
                        else html += '<i id="Linkicon_' + attrList.Response.ID + '" class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i>';
                    } else {
                        if (attrList.Response.IsPublish == true) html += '                                   <i id="Publishicon_' + attrList.Response.ID + '"  class="icon-arrow-up" my-qtip2 qtip-content="Published"></i> ';
                        else html += '                                   <i id="Publishicon_' + attrList.Response.ID + '"  class="icon-arrow-up displayNone" my-qtip2 qtip-content="Published"></i> ';
                        if (attrList.Response.LinkedAssetID != 0) html += '<i id="Linkicon_' + attrList.Response.ID + '" class="icon-link" my-qtip2 qtip-content="Linked"></i>';
                        else html += '<i id="Linkicon_' + attrList.Response.ID + '" class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i>';
                    }
                    html += '                               </span> ';
                    html += '                           </div> ';
                    html += '                           <div class="idp-detail-eIconContainer"> ';
                    html += '                               <span class="idp-eIcon" style="background-color: #' + backcolor + ';">' + shortdesc + '</span>';
                    html += '                           </div> ';
                    html += '                       </div> ';
                    html += '                   </div> ';
                    html += '                   <div class="idp-rightSection"> ';
                    html += '                       <div class="idp-expandedDetailBlock"> ';
                    html += '                           <div class="idp-expandedDetails"> ';
                    html += '                               <form class="form-horizontal"> ';
                    try {
                        html += DrawListBlock(attrList);
                    } catch (e) { }
                    html += '                               </form> ';
                    html += '                           </div> ';
                    html += '                       </div> ';
                    html += '                   </div> ';
                    html += '               </div> ';
                    html += '           </div> ';
                    html += '       </div>';
                    $('#' + qtip_id).html(html);
                });
            }
        };

        function DrawListBlock(attrList) {
            var html = '';
            $scope.dyn_Cont = '';
            refreshdata();
            $scope.attributedata = [];
            if (attrList.Response.AttributeData != null) {
                if ($scope.SettingsAttributes["ListViewToolTipSettings"].length > 0) {
                    var tooltipattrs = $.grep($scope.SettingsAttributes["ListViewToolTipSettings"], function (e) {
                        return e.assetType == attrList.Response.AssetTypeid
                    });
                }
                if (tooltipattrs.length > 0) {
                    for (var l = 0; l < tooltipattrs.length; l++) {
                        $scope.attributedata.push($.grep(attrList.Response.AttributeData, function (e) {
                            return e.ID == tooltipattrs[l].ID
                        })[0]);
                    }
                }
                html += '<div class="control-group recordRow">';
                html += '    <label class="control-label">Asset name:</label>';
                html += '    <div class="controls rytSide">';
                html += '        <label class="control-label">' + attrList.Response.Name + '</label>';
                html += '</div></div>';
                for (var i = 0; i < $scope.attributedata.length; i++) {
                    if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == false) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                        }
                        html += '<div  class=\"control-group recordRow\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls rytSide\"><span class="editable">' + $scope.attributedata[i].Caption + '</span></div></div>';
                    } else if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == true) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Value).text();
                        }
                        html += '<div class=\"control-group recordRow\"><label class=\"control-label\"\>' + 'Asset Name' + '</label><div class=\"controls rytSide\"><span class="editable">' + $scope.attributedata[i].Value + '</span></div></div>';
                    } else if ($scope.attributedata[i].TypeID == 2) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                        }
                        html += '<div class=\"control-group recordRow\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls rytSide\"><span class="editable">' + $scope.attributedata[i].Caption + '</span></div></div>';
                    } else if ($scope.attributedata[i].TypeID == 3) {
                        if ($scope.attributedata[i].ID == SystemDefiendAttributes.Owner && $scope.attributedata[i].IsSpecial == true) {
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><span class="editable">' + $scope.attributedata[i].Caption[0] + '</span></div></div>';
                            } else {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                html += '<div class="controls rytSide"><span>' + $scope.attributedata[i].Lable + '</span>';
                                html += '</div></div>';
                            }
                        } else {
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                if ($scope.attributedata[i].Caption[0].length > 1) {
                                    $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                    $scope.setFieldKeys();
                                    $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                    $scope.setNormalDropdownCaption();
                                    html += '<div  class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><span class="editable">' + $scope.attributedata[i].Caption[0] + '</span></div></div>';
                                }
                            } else {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div  class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><span class="editable">' + $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] + '</span></div></div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 4) {
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption.length > 1) {
                                $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalMultiDropdownCaption();
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><span class="editable">' + $scope.attributedata[i].Caption + '</span></div></div>';
                            }
                        } else {
                            $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = "-";
                            $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalMultiDropdownCaption();
                            html += '<div  class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><span class="editable">' + $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '</span></div></div>';
                        }
                    } else if ($scope.attributedata[i].TypeID == 17) {
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption.length > 1) {
                                $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalMultiDropdownCaption();
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><span class="editable">' + $scope.attributedata[i].Caption + '</span></div></div>';
                            }
                        } else {
                            $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = "-";
                            $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalMultiDropdownCaption();
                            html += '<div  class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><span class="editable">' + $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] + '</span></div></div>';
                        }
                    } else if ($scope.attributedata[i].TypeID == 10) {
                        var inlineEditabletitile = $scope.attributedata[i].Caption;
                        if ($scope.attributedata[i].Value == "-") {
                            $scope.fields["PeriodStartEndDate_" + $scope.attributedata[i].ID] = '-';
                            $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].ID] = "-";
                            html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                            html += '<div class="controls rytSide">';
                            html += '<span class="editable">' + $scope.fields["PeriodStartEndDate_" + $scope.attributedata[i].ID];
                            html += '</span>';
                            html += '</div>';
                            html += '</div>';
                            html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">Comment ' + inlineEditabletitile + '</label>';
                            html += '<div class="controls rytSide">';
                            html += '<span class="editable">' + $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].ID] + '</span>';
                            html += '</div>';
                            html += '</div>';
                        } else {
                            for (var j = 0; j < $scope.attributedata[i].Value.length; j++) {
                                var datStartUTCval = "";
                                var datstartval = "";
                                var datEndUTCval = "";
                                var datendval = "";
                                datStartUTCval = $scope.attributedata[i].Value[j].Startdate.substr(6, ($scope.attributedata[i].Value[j].Startdate.indexOf('+') - 6));
                                datstartval = new Date(parseInt(datStartUTCval));
                                datEndUTCval = $scope.attributedata[i].Value[j].EndDate.substr(6, ($scope.attributedata[i].Value[j].EndDate.indexOf('+') - 6));
                                datendval = new Date(parseInt(datEndUTCval));
                                $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datstartval));
                                $scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datendval));
                                if ($scope.attributedata[i].Value[j].Description == undefined) {
                                    $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = "-";
                                } else {
                                    $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = $scope.attributedata[i].Value[j].Description;
                                }
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                html += '<div class="controls rytSide">';
                                html += '<span class="editable">' + $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id];
                                html += ' to ' + $scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] + '</span>';
                                html += '</div>';
                                html += '</div>';
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">Comment ' + inlineEditabletitile + '</label>';
                                html += '<div class="controls rytSide">';
                                html += '<span class="editable">' + $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] + '</span>';
                                html += '</div>';
                                html += '</div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 17) {
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption.length > 1) {
                                $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalMultiDropdownCaption();
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><span class="editable">' + $scope.attributedata[i].Caption + '</span></div></div>';
                            }
                        } else {
                            $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = "-";
                            $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalMultiDropdownCaption();
                            html += '<div  class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><span class="editable">' + $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] + '</span></div></div>';
                        }
                    }
                }
            }
            return html;
        }

        function refreshdata() {
            $scope.ShowHideAttributeOnRelation = {};
            $scope.treelevels = {};
            $scope.fieldoptions = [];
            $scope.OwnerList = [];
            $scope.owner = {};
            $scope.NormalDropdownCaption = {};
            $scope.NormalDropdownCaptionObj = [];
            $scope.normaltreeSources = {};
            $scope.setNormalDropdownCaption = function () {
                var keys1 = [];
                angular.forEach($scope.NormalDropdownCaption, function (key) {
                    keys1.push(key);
                    $scope.NormalDropdownCaptionObj = keys1;
                });
            }
            $scope.NormalMultiDropdownCaption = {};
            $scope.NormalMultiDropdownCaptionObj = [];
            $scope.setNormalMultiDropdownCaption = function () {
                var keys1 = [];
                angular.forEach($scope.NormalMultiDropdownCaption, function (key) {
                    keys1.push(key);
                    $scope.NormalMultiDropdownCaptionObj = keys1;
                });
            }
            $scope.setFieldKeys = function () {
                var keys = [];
                angular.forEach($scope.fields, function (key) {
                    keys.push(key);
                    $scope.fieldKeys = keys;
                });
            }
            $scope.fields = {
                usersID: ''
            };
            $scope.UserId = $cookies['UserId'];
        }

        $scope.SelectAllAssetFiles = function (event) {
            var status = event.target.checked;
            $('#AssetListViewTableTask > tbody input:checkbox').each(function () {
                this.checked = status;
                if (status) {
                    $(this).next('i').addClass('checked');
                    var b;
                    for (var a = 0; b = $scope.AssetFiles[a]; a++) {
                        b["Checked"] = true;
                        $scope.checkSelection(b.AssetID, b.ActiveFileID, event);
                    }
                } else {
                    $(this).next('i').removeClass('checked');
                    var b;
                    for (var a = 0; b = $scope.AssetFiles[a]; a++) {
                        b["Checked"] = false;
                        $scope.checkSelection(b.AssetID, b.ActiveFileID, event);
                    }
                }
            });
        }

        $scope.$on('LoadDamTaskopenpopup', function (event) {
            var PreviewGeneratorTimer = $timeout(function () {
                CheckPreviewGenerator()
            }, 5000);
            $scope.openpopup();
        });
        $scope.$on('ClearTimerOnClose', function (event) {
            $(window).unbind('scroll');
            $timeout.cancel();
            $timeout.cancel(PreviewGeneratorTimer);
        });
        $scope.$on('callfromDalimTaskEditForAssetSelect', function (event, taskId, EntityID) {
            $scope.AssetTaskEditId = taskId;
            var PreviewGeneratorTimer = $timeout(function () {
                CheckPreviewGenerator()
            }, 5000);
            $scope.TaskBriefDetails.EntityID = EntityID;
            $scope.openpopup();
        });
        $timeout(function () { $scope.openpopup() }, 100);
        $scope.closethispopup = function () {
            $modalInstance.dismiss('cancel');
        }
        $scope.ok = function () {
            $modalInstance.close($scope.TaskBriefDetails.taskID);
        };
        $scope.$on("$destroy", function () {
            $(window).unbind('scroll');
            $timeout.cancel();
            $timeout.cancel(timerObj);
            $timeout.cancel(PreviewGeneratorTimer);
        });
        $scope.closedamselectionpopup = function () {
            $scope.closethispopup();
        }
    }
    app.controller("mui.DAM.taskentitydamselectionCtrl", ['$window', '$location', '$timeout', '$scope', '$cookies', '$resource', '$stateParams', '$translate', 'DamTaskEntitySelectionService', '$compile', '$modalInstance', 'params', taskentitydamselectionCtrl]);
})(angular, app);