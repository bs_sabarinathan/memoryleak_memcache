/*
Created using Microdesk MyGeneration NHibernate Template v1.1
[based on MyGeneration/Template/NHibernate (c) by Sharp 1.4]
*/
using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{

	/// <summary>
    /// Validation object for table 'MM_Validation'.
	/// </summary>
	
	internal class Validation : IValidation 
	{
		#region Member Variables

		protected int _id;
        protected int _entitytypeid;
        protected int _relationshipid;
        protected string _name;
        protected string _valuetype;
        protected string _errormessage;
        protected string _value;
        protected int _attributeid;
		#endregion
		
		#region Constructors
		public Validation() {}

        public Validation(int pId, int pentitytypeid, int prelationshipid, string pname, string pvaluetype, string perrormessage,string pvalue, int pattributeid)
		{
            this._id = pId;
            this._name = pname;
            this._entitytypeid = pentitytypeid;
            this._relationshipid = prelationshipid;
            this._valuetype = pvaluetype;
            this._errormessage = perrormessage;
            this._value = pvalue;
            this._attributeid = pattributeid;
		}

        public Validation(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}

        public virtual int EntityTypeID
        {
            get { return _entitytypeid; }
            set
            {
                _entitytypeid = value;
            }

        }

        public virtual int RelationShipID
        {
            get { return _relationshipid; }
            set
            {
                _relationshipid = value;
            }

        }

        public virtual string Name
        {
            get { return _name; }
            set
            {
                _name = value;
            }

        }

        public virtual string ValueType
        {
            get { return _valuetype; }
            set
            {
                _valuetype = value;
            }

        }

        public virtual string ErrorMessage
        {
            get { return _errormessage; }
            set
            {
                _errormessage = value;
            }

        }

        public virtual string Value
        {
            get { return _value; }
            set
            {
                _value = value;
            }

        }



        public virtual int AttributeID
        {
            get { return _attributeid; }
            set { _attributeid = value; }

        }






		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			Module castObj = null;
			try
			{
				castObj = (Module)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
