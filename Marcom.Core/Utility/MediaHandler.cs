﻿using BrandSystems.MediaHandler.Input;
using BrandSystems.MediaHandler;
using BrandSystems.MediaHandler.Output;
using BrandSystems.MediaHandler.CustomType;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using BrandSystems.Marcom.Core.Utility;
using System.Xml;
using System.IO;
using System.Runtime.Serialization;
using System.Reflection;
using BrandSystems.Marcom.Core.Metadata;
using System.Text;
using BrandSystems.Marcom.DAM;
using Newtonsoft.Json;
using NHibernate.Mapping;
using System.Collections;
using BrandSystems.Marcom.Core.AmazonStorageHelper;
using BrandSystems.Marcom.Core.Core.Managers.Proxy;
using BrandSystems.Marcom.Core.Interface;



namespace BrandSystems.Marcom.Core.Utility.MediaHandler
{
    public class MediaHandler
    {
        private decimal decFileSize;
        private const string FormatKB = "0";
        //private const decimal decInchToMMConst = 25.4;
        public const Decimal decInchToMMConst = 25;
        private const int maxFileSize = 400000;
        // private static BrandSystems.Marcom.Core.Utility.MediaHandler.MediaHandlerClient MHClient = new BrandSystems.Marcom.Core.Utility.MediaHandler.MediaHandlerClient(ConfigurationSettings.AppSettings.Item("MHUrl").ToString(), null, null, ConfigurationSettings.AppSettings.Item("IPAddress").ToString(), Convert.ToInt32(ConfigurationSettings.AppSettings.Item("MaxPort")), Convert.ToInt32(ConfigurationSettings.AppSettings.Item("MinPort")));
        private static BrandSystems.Marcom.Core.Utility.MediaHandler.MediaHandlerClient MHClient = new BrandSystems.Marcom.Core.Utility.MediaHandler.MediaHandlerClient(ConfigurationManager.AppSettings["MHUrl"].ToString(), null, null, ConfigurationManager.AppSettings["IPAddress"].ToString(), Convert.ToInt32(ConfigurationManager.AppSettings["MaxPort"].ToString()), Convert.ToInt32(ConfigurationManager.AppSettings["MinPort"].ToString()));
        public bool MHClientReady()
        {
            bool status = false;
            try
            {
                MHClient.Ping();
                status = true;
                return status;
            }
            catch (Exception ex)
            {
                LogHandler.LogInfo("MH not responding:" + ex.Message, LogHandler.LogType.Notify);
            }
            return status;
        }

        public bool GereratePreview(string sourcePath, string destinationPath, int maxHight, int maxWidth, bool ISs3amazonbucket = false, string s3accesskey = "", string s3securitykey = "", string s3region = "", string s3Foldername = "", string s3Filename = "", string s3FileSourcePath = "", string s3FileDestinationPath = "",int imgQuality=65)
        {
            MHClient.Ping();
            Guid gID = Guid.NewGuid();
            ImageRequestObject imgReq = new ImageRequestObject(Guid.NewGuid());
            imgReq.Thumbnail = true;
            imgReq.ClientID = 1000; //need to check
            imgReq.ApplicationID = 1000;

            imgReq.SourcePath = sourcePath;
            imgReq.DestinationPath = destinationPath;
            imgReq.ResizeHeight = maxHight;
            imgReq.ResizeWidth = maxWidth;
            imgReq.ColourSpace = ColourSpace.sRGB;
            imgReq.ISs3amazonbucket = ISs3amazonbucket;
            imgReq.s3accesskey = s3accesskey;
            imgReq.s3securitykey = s3securitykey;
            imgReq.s3region = s3region;
            imgReq.s3Foldername = s3Foldername;
            imgReq.s3Filename = s3Filename;
            imgReq.s3FileSourcePath = s3FileSourcePath;
            imgReq.s3FileDestinationPath = s3FileDestinationPath;
            //if (destinationPath.Contains("Small_") == true)            
            //    imgReq.Quality = 65;
            //else
            //    imgReq.Quality = 100;

            //imgReq.Quality = 65;
            imgReq.Quality = imgQuality;
            //imgReq.MetaDataNeeded = new MetaData[] { MetaData.ColourSpace, MetaData.Compression };
            //Need to create enum
            imgReq.TaskToPerform = BrandSystems.MediaHandler.CustomType.Task.Convert;


            //Validate Object ex. validateRequest();
            // Need to execute the task ex. mgResObj = MHClient.ExecuteSynchronousTask(imgReqObj)
            //ImageResponseObject imgResObj = new ImageResponseObject();
            ImageResponseObject imgResObj = new ImageResponseObject();
            //Execute Task
            imgResObj = (BrandSystems.MediaHandler.Output.ImageResponseObject)MHClient.ExecuteSynchronousTask(imgReq);

            //timeingly 
            //********************
            ////  imgReq.JobID = Guid.NewGuid();
            ////  imgReq.SourcePath = imgResObj.DestinationPath;
            ////  imgReq.MetaDataNeeded = new MetaData[] { MetaData.Height, MetaData.Width, MetaData.XResolution };

            ////  imgReq.TaskToPerform = BrandSystems.MediaHandler.CustomType.Task.GetInfo;

            ////  //Get Height Width for destination image
            ////  imgResObj = (BrandSystems.MediaHandler.Output.ImageResponseObject)MHClient.ExecuteSynchronousTask(imgReq);

            ////  return imgResObj;
            bool status = false;
            //********************
            if ((imgResObj.ErrorDetail == null | object.ReferenceEquals(imgResObj.ErrorDetail, "")))
            {
                // ErrDescription = imgResObj.ErrorDetail;
                status = true;


            }
            else
            {
                LogHandler.LogInfo("GereratePreview Issue:" + imgResObj.ErrorDetail, LogHandler.LogType.Notify);
                status = false;
            }

            return status;
            //////(BrandSystems.MediaHandler.Output.MediaHandlerResponseObject)
            ////Execute Task

            ////imgResObj = MHClient.ExecuteSynchronousTask(imgReq);
            //if (!(imgResObj.ErrorDetail == null | object.ReferenceEquals(imgResObj.ErrorDetail, "")))
            //{
            //   // ErrDescription = imgResObj.ErrorDetail;
            //    return null;
            //}

            //imgReq.JobID = Guid.NewGuid();
            //imgReq.SourcePath=imgResObj.DestinationPath;
            //imgReq.MetaDataNeeded = new MetaData[] {	MetaData.Height,MetaData.Width,	MetaData.XResolution};
            //imgReq.TaskToPerform = BrandSystems.MediaHandler.CustomType.Task.GetInfo;

            ////Get Height Width for destination image
            ////imgResObj = MHClient.ExecuteSynchronousTask(imgReq);

            //if ((imgResObj.ErrorDetail == null | object.ReferenceEquals(imgResObj.ErrorDetail, "")))
            //{
            //    return new int[] { (int)imgResObj.Height, (int)imgResObj.Width, (int)imgResObj.XResolution };
            //}
            //else
            //{
            //    //ErrDescription = imgResObj.ErrorDetail;
            //    return null;
            //}


        }

        public bool GereratePreviewOffice(string sourcePath, string destinationPath, int maxHight, int maxWidth)
        {
            MHClient.Ping();
            Guid gID = Guid.NewGuid();
            OfficeRequestObject offreq = new OfficeRequestObject(Guid.NewGuid());
            //ImageRequestObject offreq = new ImageRequestObject(Guid.NewGuid());
            string sExt = System.IO.Path.GetExtension(sourcePath).Replace(".", "").ToUpper();
            offreq.ThumbnailFormat = "JPG";
            offreq.ClientID = 1000; //need to check
            offreq.ApplicationID = 1000;

            offreq.SourcePath = sourcePath;
            offreq.DestinationPath = destinationPath;
            //offreq.FileCategory
            offreq.IsOpenXMLFile = true;
            offreq.ThumbnailHeight = maxHight;
            offreq.ThumbnailWidth = maxWidth;
            offreq.FileFormat = sExt;
            switch (sExt)
            {
                case "DOC":
                    offreq.FileCategory = OfficeFileCategory.Word;
                    offreq.IsOpenXMLFile = false;
                    break;
                case "DOCX":
                    offreq.FileCategory = OfficeFileCategory.Word;
                    offreq.IsOpenXMLFile = true;
                    break;
                case "XLS":
                    offreq.FileCategory = OfficeFileCategory.Excel;
                    offreq.IsOpenXMLFile = false;
                    break;
                case "XLSX":
                    offreq.FileCategory = OfficeFileCategory.Excel;
                    offreq.IsOpenXMLFile = true;
                    break;
                case "PPT":
                    offreq.FileCategory = OfficeFileCategory.PowerPoint;
                    offreq.IsOpenXMLFile = false;
                    break;
                case "PPTX":
                    offreq.FileCategory = OfficeFileCategory.PowerPoint;
                    offreq.IsOpenXMLFile = true;
                    break;


                default:
                    throw new MHValidationFailException("Invalid source file", null, "SourcePath", offreq.JobID, 63);
            }
            offreq.TaskToPerform = BrandSystems.MediaHandler.CustomType.Task.Convert;


            //Validate Object ex. validateRequest();
            // Need to execute the task ex. mgResObj = MHClient.ExecuteSynchronousTask(imgReqObj)
            //ImageResponseObject imgResObj = new ImageResponseObject();
            //ImageResponseObject imgResObj = new ImageResponseObject();
            OfficeResponseObject offResObj = new OfficeResponseObject();
            //Execute Task
            offResObj = (BrandSystems.MediaHandler.Output.OfficeResponseObject)MHClient.ExecuteSynchronousTask(offreq);

            //timeingly 
            //********************
            ////  imgReq.JobID = Guid.NewGuid();
            ////  imgReq.SourcePath = imgResObj.DestinationPath;
            ////  imgReq.MetaDataNeeded = new MetaData[] { MetaData.Height, MetaData.Width, MetaData.XResolution };

            ////  imgReq.TaskToPerform = BrandSystems.MediaHandler.CustomType.Task.GetInfo;

            ////  //Get Height Width for destination image
            ////  imgResObj = (BrandSystems.MediaHandler.Output.ImageResponseObject)MHClient.ExecuteSynchronousTask(imgReq);

            ////  return imgResObj;
            bool status = false;
            //********************
            if ((offResObj.ErrorDetail == null | object.ReferenceEquals(offResObj.ErrorDetail, "")))
            {
                // ErrDescription = imgResObj.ErrorDetail;
                status = true;


            }
            else
            {
                status = false;
            }

            return status;



        }

        public bool GereratePreviewVideo(string sourcePath, string SmallimagedestinationPath, string BigimagedestinationPath, string VideoPreviewdestinationPath, Double ThumbWidth, Double ThumbHight, int videoPerviewStartTime, int videoPerviewEndTime, int videoResolutionWidth, int videoResolutionHeight, string OutputVideoCodec, int NumberOfThumbnail, bool ISs3amazonbucket = false, string s3accesskey = "", string s3securitykey = "", string s3region = "", string s3Foldername = "", string s3Filename = "", string s3FileSourcePath = "", string s3FileDestinationPath = "")
        {
            bool status = false;
            try
            {
                MHClient.Ping();
                Guid gID = Guid.NewGuid();
                VideoRequestObject videoreq = new VideoRequestObject(Guid.NewGuid());
                videoreq.ClientID = 1000; //need to check
                videoreq.ApplicationID = 1000;
                string ThumbnailFrameTimeList = "";
                string Arrtime;
                int i;
                ArrayList ArrListTime = new ArrayList();
                //if (!(ThumbnailFrameTimeList == ""))
                //{
                //    Arrtime = ThumbnailFrameTimeList.Split(",");
                //    int j;
                //    for (j = 0; (j
                //                <= (Arrtime.Length - 1)); j++)
                //    {
                //        ArrListTime.Add(Arrtime(j));
                //    }
                //}
                ArrayList ThumbnailDestinationPatharr = new ArrayList();

                if (NumberOfThumbnail == 2)
                {
                    ThumbnailDestinationPatharr.Add(SmallimagedestinationPath);
                    ThumbnailDestinationPatharr.Add(BigimagedestinationPath);
                }
                else
                {
                    ThumbnailDestinationPatharr.Add(SmallimagedestinationPath);
                }

                videoreq.SourcePath = sourcePath;
                videoreq.ThumbnailDestinationPath = ThumbnailDestinationPatharr;
                videoreq.AditionalInfo = "UserThumbnailname";
                videoreq.Format = Path.GetExtension(sourcePath);
                videoreq.ConverstionDestinationPath = VideoPreviewdestinationPath;
                videoreq.ThumbnailWidth = ThumbWidth;
                videoreq.ThumbnailHeight = ThumbHight;
                videoreq.ThumbnailFrameTimeList = ArrListTime;
                //videoreq.MultiThumbnailStartTime = 1000;
                //videoreq.MultiThumbnailEndTime = 6000;
                videoreq.NumberOfThumbnail = NumberOfThumbnail;
                videoreq.EnableGIF = false;
                videoreq.EnableRAWImage = false;
                videoreq.VideoPreviewStartTime = videoPerviewStartTime;
                videoreq.VideoPreviewEndTime = videoPerviewEndTime;
                videoreq.VideoResolutionHeight = videoResolutionHeight;
                videoreq.VideoResolutionWidth = videoResolutionWidth;
                videoreq.OutputVideoCodec = OutputVideoCodec;
                videoreq.ISs3amazonbucket = ISs3amazonbucket;
                videoreq.s3accesskey = s3accesskey;
                videoreq.s3securitykey = s3securitykey;
                videoreq.s3region = s3region;
                videoreq.s3Foldername = s3Foldername;
                videoreq.s3Filename = s3Filename;
                videoreq.s3FileSourcePath = s3FileSourcePath;
                videoreq.s3FileDestinationPath = s3FileDestinationPath;


                videoreq.TaskToPerform = BrandSystems.MediaHandler.CustomType.Task.Convert;


                VideoResponseObject videoResObj = new VideoResponseObject();

                //Execute Task
                videoResObj = (BrandSystems.MediaHandler.Output.VideoResponseObject)MHClient.ExecuteSynchronousTask(videoreq);
                if ((videoResObj.ErrorDetail == null | object.ReferenceEquals(videoResObj.ErrorDetail, "")))
                {
                    // ErrDescription = imgResObj.ErrorDetail;
                    status = true;


                }
                else
                {
                    LogHandler.LogInfo("GereratePreviewVideo Issue:" + videoResObj.ErrorDetail, LogHandler.LogType.Notify);
                    status = false;
                }

            }
            catch (Exception ex)
            {
                return false;
            }
            //timeingly 
            //********************
            ////  imgReq.JobID = Guid.NewGuid();
            ////  imgReq.SourcePath = imgResObj.DestinationPath;
            ////  imgReq.MetaDataNeeded = new MetaData[] { MetaData.Height, MetaData.Width, MetaData.XResolution };

            ////  imgReq.TaskToPerform = BrandSystems.MediaHandler.CustomType.Task.GetInfo;

            ////  //Get Height Width for destination image
            ////  imgResObj = (BrandSystems.MediaHandler.Output.ImageResponseObject)MHClient.ExecuteSynchronousTask(imgReq);

            ////  return imgResObj;

            //********************

            return status;



        }

        public bool ValidateRequest()
        {


            return true;
        }

        public string getFileInfo(string strSourcePath, bool ISs3amazonbucket = false, string s3accesskey = "", string s3securitykey = "", string s3region = "", string s3Foldername = "", string s3Filename = "", string s3FileSourcePath = "")
        {
            MHClient.Ping();
            //Guid gID = Guid.NewGuid();
            StringBuilder fileinfo = new StringBuilder();
            ImageRequestObject imgReqObj = new ImageRequestObject(Guid.NewGuid());
            ImageResponseObject imgResObj = new ImageResponseObject();
            try
            {
                //ErrDescription = "";



                imgReqObj.ClientID = 1000;
                imgReqObj.ApplicationID = 1000;

                imgReqObj.SourcePath = strSourcePath;




                imgReqObj.JobID = Guid.NewGuid();

                imgReqObj.MetaDataNeeded = new MetaData[] { MetaData.Height, MetaData.Width, MetaData.XResolution };
                imgReqObj.ISs3amazonbucket = ISs3amazonbucket;
                imgReqObj.s3accesskey = s3accesskey;
                imgReqObj.s3securitykey = s3securitykey;
                imgReqObj.s3region = s3region;
                imgReqObj.s3Foldername = s3Foldername;
                imgReqObj.s3Filename = s3Filename;
                imgReqObj.s3FileSourcePath = s3FileSourcePath;


                imgReqObj.TaskToPerform = BrandSystems.MediaHandler.CustomType.Task.GetInfo;

                //Get Height Width for destination image
                imgResObj = (BrandSystems.MediaHandler.Output.ImageResponseObject)MHClient.ExecuteSynchronousTask(imgReqObj);

                //return imgResObj;

                //    return imgResObj;
            }
            catch (Exception ex)
            {
                return fileinfo.ToString();
            }



            if ((imgResObj.ErrorDetail == null | object.ReferenceEquals(imgResObj.ErrorDetail, "")))
            {

                DAMFileAdditionalinfo damfile = new DAMFileAdditionalinfo();
                damfile.Height = (int)imgResObj.Height;
                damfile.Width = (int)imgResObj.Width;
                damfile.Dpi = (int)imgResObj.XResolution;

                string fileinfodet = JsonConvert.SerializeObject(damfile);


                ////fileinfo.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                //fileinfo.Append("<Fileinfo>");
                //fileinfo.Append("<Height>" + (int)imgResObj.Height + "</Height>");
                //fileinfo.Append("<Width>" + (int)imgResObj.Width + "</Width>");
                //fileinfo.Append("<Dpi>" + (int)imgResObj.XResolution + "</Dpi>");
                //fileinfo.Append("</Fileinfo>");

                return fileinfodet;

            }
            else
            {
                return fileinfo.ToString();
            }
            //}
            //catch (MHValidationFailException ex)
            //{
            //    //ErrDescription = GetErrorDetails(ex.ErrorCode());
            //}
            //catch (Exception ex)
            //{
            //    ErrDescription = ex.Message;
            //    return false;
            //}

            //return true;
        }


        public string getVideoFileInfo(string strSourcePath, bool ISs3amazonbucket = false, string s3accesskey = "", string s3securitykey = "", string s3region = "", string s3Foldername = "", string s3Filename = "", string s3FileSourcePath = "")
        {
            MHClient.Ping();
            //Guid gID = Guid.NewGuid();
            StringBuilder fileinfo = new StringBuilder();
            VideoResponseObject VideoResObj = new VideoResponseObject();


            try
            {
                //ErrDescription = "";

                MHClient.Ping();
                Guid gID = Guid.NewGuid();
                VideoRequestObject videoreq = new VideoRequestObject(Guid.NewGuid());

                videoreq.ClientID = 1000; //need to check
                videoreq.ApplicationID = 1000;
                string ThumbnailFrameTimeList = "";
                string Arrtime;
                int i;
                ArrayList ArrListTime = new ArrayList();
                //if (!(ThumbnailFrameTimeList == ""))
                //{
                //    Arrtime = ThumbnailFrameTimeList.Split(",");
                //    int j;
                //    for (j = 0; (j
                //                <= (Arrtime.Length - 1)); j++)
                //    {
                //        ArrListTime.Add(Arrtime(j));
                //    }
                //}
                ArrayList ThumbnailDestinationPatharr = new ArrayList();


                videoreq.SourcePath = strSourcePath;
                videoreq.ThumbnailDestinationPath = ThumbnailDestinationPatharr;
                videoreq.AditionalInfo = "UserThumbnailname";
                videoreq.Format = Path.GetExtension(strSourcePath);
                videoreq.ConverstionDestinationPath = "";
                videoreq.ThumbnailWidth = 0;
                videoreq.ThumbnailHeight = 0;
                videoreq.ThumbnailFrameTimeList = ArrListTime;
                //videoreq.MultiThumbnailStartTime = 1000;
                //videoreq.MultiThumbnailEndTime = 6000;
                videoreq.NumberOfThumbnail = 0;
                videoreq.EnableGIF = false;
                videoreq.EnableRAWImage = false;
                videoreq.VideoPreviewStartTime = 0;
                videoreq.VideoPreviewEndTime = 0;
                videoreq.VideoResolutionHeight = 0;
                videoreq.VideoResolutionWidth = 0;
                videoreq.OutputVideoCodec = "";
                videoreq.JobID = Guid.NewGuid();
                videoreq.MetaDataNeeded = new VideoMetaData[] { VideoMetaData.TotalDuration, VideoMetaData.FrameWidth, VideoMetaData.FrameHeight, VideoMetaData.FrameRate };
                videoreq.ISs3amazonbucket = ISs3amazonbucket;
                videoreq.s3accesskey = s3accesskey;
                videoreq.s3securitykey = s3securitykey;
                videoreq.s3region = s3region;
                videoreq.s3Foldername = s3Foldername;
                videoreq.s3Filename = s3Filename;
                videoreq.s3FileSourcePath = s3FileSourcePath;
                videoreq.TaskToPerform = BrandSystems.MediaHandler.CustomType.Task.GetInfo;

                //Get Height Width for destination image
                VideoResObj = (BrandSystems.MediaHandler.Output.VideoResponseObject)MHClient.ExecuteSynchronousTask(videoreq);

                //return imgResObj;

                //    return imgResObj;
            }
            catch (Exception ex)
            {
                return fileinfo.ToString();
            }



            if ((VideoResObj.ErrorDetail == null | object.ReferenceEquals(VideoResObj.ErrorDetail, "")))
            {

                DAMFileAdditionalinfo damfile = new DAMFileAdditionalinfo();

                damfile.Width = (int)VideoResObj.FrameWidth;
                damfile.Height = (int)VideoResObj.FrameHeight;
                damfile.Dpi = 96;
                damfile.TotalDuration = VideoResObj.TotalDuration;
                damfile.FrameRate = VideoResObj.FrameRate;
                //damfile.FrameWidth = (int)VideoResObj.FrameWidth;
                //damfile.FrameHeight = (int)VideoResObj.FrameHeight;

                string fileinfodet = JsonConvert.SerializeObject(damfile);


                ////fileinfo.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                //fileinfo.Append("<Fileinfo>");
                //fileinfo.Append("<Height>" + (int)imgResObj.Height + "</Height>");
                //fileinfo.Append("<Width>" + (int)imgResObj.Width + "</Width>");
                //fileinfo.Append("<Dpi>" + (int)imgResObj.XResolution + "</Dpi>");
                //fileinfo.Append("</Fileinfo>");

                return fileinfodet;

            }
            else
            {
                return fileinfo.ToString();
            }
            //}
            //catch (MHValidationFailException ex)
            //{
            //    //ErrDescription = GetErrorDetails(ex.ErrorCode());
            //}
            //catch (Exception ex)
            //{
            //    ErrDescription = ex.Message;
            //    return false;
            //}

            //return true;
        }

        public bool Rescale(int TenantID, ItemRequest objItemRequest, string strSourcePath, string strDestinationPath, ref string[] ItemInfo, ref string ErrDescription, string session_id = null, bool ISs3amazonbucket = false, string s3accesskey = "", string s3securitykey = "", string s3region = "", string s3Foldername = "", string s3Filename = "", string s3FileSourcePath = "", string s3FileDestinationPath = "")
        {
            string m_strServername = ConfigurationManager.AppSettings["ServerName"];

            //MediaHandlerThread OBJMHT = default(MediaHandlerThread);
            IMarcomManager marcomManager;
            marcomManager = MarcomManagerFactory.GetMarcomManager(null,MarcomManagerFactory.GetSystemSession(TenantID));
            bool blnCallFromApplication = false;
            try
            {
                strSourcePath = strSourcePath.Replace("/", "\\");
                strDestinationPath = strDestinationPath.Replace("/", "\\");
                ////Modified by : Asish - 201005020
                //Dim job_id As Guid
                //job_id = Guid.NewGuid()

                ImageRequestObject imgReqObj = new ImageRequestObject(Guid.NewGuid());
                /////Chk the thread is the current thread/not
                //need to worke
                //foreach ( OBJMHT in ThreadQueue.objMHThread) {
                //    if (session_id == OBJMHT.SessionID) {
                //        imgReqObj.JobID = new Guid(OBJMHT.JobID);
                //        blnCallFromApplication = true;
                //        break; // TODO: might not be correct. Was : Exit For
                //    }
                //}

                MHClient.Ping();

                //ErrDescription = ""
                //Dim imgReqObj As New ImageRequestObject(Guid.NewGuid())
                ImageResponseObject imgResObj = new ImageResponseObject();
                bool blnFileType = false;

                imgReqObj.ClientID = 1;
                imgReqObj.ApplicationID = 2;

                int intCounter = 0;
                string strNewFileName = "";
                string strDestFilepath = "";
                //strDestFilepath= strDestFilepath.Replace(strDestinationPath, Path.GetFileName(strDestinationPath));
                strDestFilepath = System.IO.Path.GetDirectoryName(strDestinationPath);
                DirectoryInfo dirInfo = new DirectoryInfo(strDestFilepath);

                if (!(dirInfo.Exists))
                {
                    dirInfo.Create();
                }

                if (File.Exists(strDestinationPath))
                {
                    do
                    {
                        intCounter = intCounter + 1;
                        strNewFileName = strDestFilepath + Path.GetFileNameWithoutExtension(strDestinationPath) + "_" + intCounter + Path.GetExtension(strDestinationPath);
                    } while (File.Exists(strNewFileName));

                    strDestinationPath = strNewFileName;
                    strNewFileName = "";
                }

                if ((objItemRequest.Format).Length > 0)
                {
                    imgReqObj.DestinationPath = Path.ChangeExtension(GetServerPath(m_strServername, strDestinationPath), objItemRequest.Format);
                }
                else
                {
                    imgReqObj.DestinationPath = GetServerPath(m_strServername, strDestinationPath);
                }

                if (Path.GetExtension(strSourcePath) == ".pdf")
                {
                    blnFileType = true;
                }
                else
                {
                    blnFileType = FileIsGraphic(Path.GetFileName(strSourcePath));
                }

                ///'If blnFileType = True Then
                ///'    If Path.GetExtension(imgReqObj.DestinationPath) = ".pdf" Then
                ///'        blnFileType = True
                ///'    Else
                ///'        blnFileType = FileIsGraphic(imgReqObj.DestinationPath)
                ///'    End If
                ///'End If

                switch (objItemRequest.Unit)
                {
                    case ItemRequest.Units.Pixels:
                        imgReqObj.Units = BrandSystems.MediaHandler.CustomType.Units.Pixels;
                        break;
                    case ItemRequest.Units.Inches:
                        imgReqObj.Units = BrandSystems.MediaHandler.CustomType.Units.Inch;
                        break;
                    case ItemRequest.Units.Millimeters:
                        imgReqObj.Units = BrandSystems.MediaHandler.CustomType.Units.Millimeter;
                        break;
                }

                switch (objItemRequest.FlipDirection)
                {

                    case ItemRequest.Direction.Horizontal:
                        imgReqObj.FlipDirection = BrandSystems.MediaHandler.CustomType.Direction.Horizontal;
                        break;
                    case ItemRequest.Direction.Vertical:
                        imgReqObj.FlipDirection = BrandSystems.MediaHandler.CustomType.Direction.Vertical;
                        break;
                    default:
                        imgReqObj.FlipDirection = BrandSystems.MediaHandler.CustomType.Direction.NULL;
                        break;
                }

                imgReqObj.RotationAngle = objItemRequest.RotationAngle;

                imgReqObj.SourcePath = GetServerPath(m_strServername, strSourcePath);
                imgReqObj.CropTop = objItemRequest.CropTop;
                imgReqObj.CropLeft = objItemRequest.CropLeft;
                imgReqObj.CropHeight = objItemRequest.CropHeight;
                imgReqObj.CropWidth = objItemRequest.CropWidth;
                imgReqObj.ResizeHeight = objItemRequest.ResizeHeight;
                imgReqObj.ResizeWidth = objItemRequest.ResizeWidth;
                imgReqObj.ResizeInPercentage = objItemRequest.ResizeInPercentage;
                imgReqObj.Quality = objItemRequest.QualityPercentage;
                imgReqObj.ScaleHeight = objItemRequest.ScaleHeight;
                imgReqObj.ScaleWidth = objItemRequest.ScaleWidth;
                imgReqObj.ScaleInPercentage = objItemRequest.ScaleInPercentage;
                imgReqObj.XResolution = objItemRequest.XResolutionDPI;
                imgReqObj.YResolution = objItemRequest.YResolutionDPI;
                imgReqObj.ISs3amazonbucket = ISs3amazonbucket;
                imgReqObj.s3accesskey = s3accesskey;
                imgReqObj.s3securitykey = s3securitykey;
                imgReqObj.s3region = s3region;
                imgReqObj.s3Foldername = s3Foldername;
                imgReqObj.s3Filename = s3Filename;
                imgReqObj.s3FileSourcePath = s3FileSourcePath;
                imgReqObj.s3FileDestinationPath = s3FileDestinationPath;

                /// Added by Syed on 09-Sep-2010
                imgReqObj.TopLeftRadius = objItemRequest.TopLeftRadius;
                imgReqObj.TopRightRadius = objItemRequest.TopRightRadius;
                imgReqObj.BottomLeftRadius = objItemRequest.BottomLeftRadius;
                imgReqObj.BottomRightRadius = objItemRequest.BottomRightRadius;
                imgReqObj.CornerColor = objItemRequest.CornerColor;
                /// Added by Syed ends here


                //Add by Prabhu on 14-sep-2010 to check whether any of the round corner information is passed or not
                if (objItemRequest.CropTop == 0 & objItemRequest.CropLeft == 0 & objItemRequest.CropHeight == 0 & objItemRequest.CropWidth == 0 & objItemRequest.ResizeHeight == 0 & objItemRequest.ResizeWidth == 0 & objItemRequest.ResizeInPercentage == 0 & objItemRequest.QualityPercentage == 0 & objItemRequest.ScaleHeight == 0 & objItemRequest.ScaleWidth == 0 & objItemRequest.ScaleInPercentage == 0 & objItemRequest.XResolutionDPI == 0 & objItemRequest.YResolutionDPI == 0 & string.IsNullOrEmpty(objItemRequest.Colorspace) & string.IsNullOrEmpty(objItemRequest.ImageType) & string.IsNullOrEmpty(objItemRequest.Format) & imgReqObj.TopLeftRadius == 0 & imgReqObj.TopRightRadius == 0 & imgReqObj.BottomLeftRadius == 0 & imgReqObj.BottomRightRadius == 0 & string.IsNullOrEmpty(imgReqObj.CornerColor))
                {
                    // Copy sourcefile to the destination
                    if (!imgReqObj.ISs3amazonbucket)
                        System.IO.File.Copy(strSourcePath, strDestinationPath);
                    else if (imgReqObj.ISs3amazonbucket) // Copy sourcefile to the destination from bucket
                    {
                        if (imgReqObj.s3FileSourcePath.Length > 0 && imgReqObj.s3FileDestinationPath.Length > 0)
                        {
                            bool fileexists = AWSHelper.isKeyExists(marcomManager.User.AwsStorage.S3, marcomManager.User.AwsStorage.BucketName, imgReqObj.s3FileSourcePath);
                            if (fileexists)
                            {
                                AWSHelper.CopyS3Object(marcomManager.User.AwsStorage.S3, marcomManager.User.AwsStorage.BucketName, imgReqObj.s3FileSourcePath, marcomManager.User.AwsStorage.BucketName, imgReqObj.s3FileSourcePath);
                            }
                        }

                    }
                    if (blnFileType == true)
                    {
                        //imgReqObj.JobID = Guid.NewGuid()
                        imgReqObj.MetaDataNeeded = new MetaData[] {
					MetaData.Height,
					MetaData.Width,
					MetaData.XResolution,
					MetaData.YResolution,
					MetaData.Format
				};
                        imgReqObj.TaskToPerform = BrandSystems.MediaHandler.CustomType.Task.GetInfo;
                        //Get Height Width for destination image
                        imgResObj = (BrandSystems.MediaHandler.Output.ImageResponseObject)MHClient.ExecuteSynchronousTask(imgReqObj);
                        if ((imgResObj.ErrorDetail == null | object.ReferenceEquals(imgResObj.ErrorDetail, "")))
                        {
                            ItemInfo[0] = imgResObj.Width.ToString();
                            ItemInfo[1] = imgResObj.Height.ToString();
                            ItemInfo[2] = imgResObj.XResolution.ToString();
                            ItemInfo[3] = imgResObj.YResolution.ToString();
                            ItemInfo[4] = Path.GetExtension(imgReqObj.DestinationPath);
                            ItemInfo[5] = Path.GetFileName(imgReqObj.DestinationPath);
                            ItemInfo[6] = Path.GetFullPath(imgResObj.DestinationPath);
                            return true;
                        }
                        else
                        {

                            //intErrorNumber = Int32.Parse(imgResObj.ErrorDetail.Split(""(3));
                            //ErrDescription = GetErrorDetails(Int32.Parse(imgResObj.ErrorDetail.Split(" ")(3)));
                            return false;
                        }
                    }
                    else
                    {
                        ItemInfo[0] = "";
                        ItemInfo[1] = "";
                        ItemInfo[2] = "";
                        ItemInfo[3] = "";
                        ItemInfo[4] = Path.GetExtension(imgReqObj.DestinationPath);
                        ItemInfo[5] = Path.GetFileName(imgReqObj.DestinationPath);
                        ItemInfo[6] = "";
                        return true;
                    }

                }
                else if (blnFileType == false)
                {
                    //ErrDescription = objContent.GetText("miscInvalidFileFormat");
                    return false;
                }


                if (objItemRequest.Colorspace == "CMY")
                {
                    imgReqObj.ColourSpace = BrandSystems.MediaHandler.CustomType.ColourSpace.CMY;
                }
                else if (objItemRequest.Colorspace == "CMYK")
                {
                    imgReqObj.ColourSpace = BrandSystems.MediaHandler.CustomType.ColourSpace.CMYK;
                }
                else if (objItemRequest.Colorspace == "Gray")
                {
                    imgReqObj.ColourSpace = BrandSystems.MediaHandler.CustomType.ColourSpace.Gray;
                }
                else if (objItemRequest.Colorspace == "HSB")
                {
                    imgReqObj.ColourSpace = BrandSystems.MediaHandler.CustomType.ColourSpace.HSB;
                }
                else if (objItemRequest.Colorspace == "HSL")
                {
                    imgReqObj.ColourSpace = BrandSystems.MediaHandler.CustomType.ColourSpace.HSL;
                }
                else if (objItemRequest.Colorspace == "HWB")
                {
                    imgReqObj.ColourSpace = BrandSystems.MediaHandler.CustomType.ColourSpace.HWB;
                }
                else if (objItemRequest.Colorspace == "Lab")
                {
                    imgReqObj.ColourSpace = BrandSystems.MediaHandler.CustomType.ColourSpace.Lab;
                }
                else if (objItemRequest.Colorspace == "Log")
                {
                    imgReqObj.ColourSpace = BrandSystems.MediaHandler.CustomType.ColourSpace.Log;
                }
                else if (objItemRequest.Colorspace == "OHTA")
                {
                    imgReqObj.ColourSpace = BrandSystems.MediaHandler.CustomType.ColourSpace.OHTA;
                }
                else if (objItemRequest.Colorspace == "Rec601Luma")
                {
                    imgReqObj.ColourSpace = BrandSystems.MediaHandler.CustomType.ColourSpace.Rec601Luma;
                }
                else if (objItemRequest.Colorspace == "Rec601YCbCr")
                {
                    imgReqObj.ColourSpace = BrandSystems.MediaHandler.CustomType.ColourSpace.Rec601YCbCr;
                }
                else if (objItemRequest.Colorspace == "Rec709Luma")
                {
                    imgReqObj.ColourSpace = BrandSystems.MediaHandler.CustomType.ColourSpace.Rec709Luma;
                }
                else if (objItemRequest.Colorspace == "Rec709YCbCr")
                {
                    imgReqObj.ColourSpace = BrandSystems.MediaHandler.CustomType.ColourSpace.Rec709YCbCr;
                }
                else if (objItemRequest.Colorspace == "RGB")
                {
                    imgReqObj.ColourSpace = BrandSystems.MediaHandler.CustomType.ColourSpace.RGB;
                }
                else if (objItemRequest.Colorspace == "sRGB")
                {
                    imgReqObj.ColourSpace = BrandSystems.MediaHandler.CustomType.ColourSpace.sRGB;
                }
                else if (objItemRequest.Colorspace == "Transparent")
                {
                    imgReqObj.ColourSpace = BrandSystems.MediaHandler.CustomType.ColourSpace.Transparent;
                }
                else if (objItemRequest.Colorspace == "XYZ")
                {
                    imgReqObj.ColourSpace = BrandSystems.MediaHandler.CustomType.ColourSpace.XYZ;
                }
                else if (objItemRequest.Colorspace == "YCbCr")
                {
                    imgReqObj.ColourSpace = BrandSystems.MediaHandler.CustomType.ColourSpace.YCbCr;
                }
                else if (objItemRequest.Colorspace == "YCC")
                {
                    imgReqObj.ColourSpace = BrandSystems.MediaHandler.CustomType.ColourSpace.YCC;
                }
                else if (objItemRequest.Colorspace == "YIQ")
                {
                    imgReqObj.ColourSpace = BrandSystems.MediaHandler.CustomType.ColourSpace.YIQ;
                }
                else if (objItemRequest.Colorspace == "YPbPr")
                {
                    imgReqObj.ColourSpace = BrandSystems.MediaHandler.CustomType.ColourSpace.YPbPr;
                }
                else if (objItemRequest.Colorspace == "YUV")
                {
                    imgReqObj.ColourSpace = BrandSystems.MediaHandler.CustomType.ColourSpace.YUV;
                }
                else if (objItemRequest.Colorspace == null)
                {
                    imgReqObj.ColourSpace = BrandSystems.MediaHandler.CustomType.ColourSpace.NULL;
                }


                if (objItemRequest.ImageType == "Bilevel")
                {
                    imgReqObj.ImageType = BrandSystems.MediaHandler.CustomType.Type.Bilevel;
                }
                else if (objItemRequest.ImageType == "ColorSeparation")
                {
                    imgReqObj.ImageType = BrandSystems.MediaHandler.CustomType.Type.ColorSeparation;
                }
                else if (objItemRequest.ImageType == "ColorSeparationMatte")
                {
                    imgReqObj.ImageType = BrandSystems.MediaHandler.CustomType.Type.ColorSeparationMatte;
                }
                else if (objItemRequest.ImageType == "Grayscale")
                {
                    imgReqObj.ImageType = BrandSystems.MediaHandler.CustomType.Type.Grayscale;
                }
                else if (objItemRequest.ImageType == "GrayscaleMatte")
                {
                    imgReqObj.ImageType = BrandSystems.MediaHandler.CustomType.Type.GrayscaleMatte;
                }
                else if (objItemRequest.ImageType == "Optimize")
                {
                    imgReqObj.ImageType = BrandSystems.MediaHandler.CustomType.Type.Optimize;
                }
                else if (objItemRequest.ImageType == "Palette")
                {
                    imgReqObj.ImageType = BrandSystems.MediaHandler.CustomType.Type.Palette;
                }
                else if (objItemRequest.ImageType == "PaletteMatte")
                {
                    imgReqObj.ImageType = BrandSystems.MediaHandler.CustomType.Type.PaletteMatte;
                }
                else if (objItemRequest.ImageType == "TrueColor")
                {
                    imgReqObj.ImageType = BrandSystems.MediaHandler.CustomType.Type.TrueColor;
                }
                else if (objItemRequest.ImageType == "TrueColorMatte")
                {
                    imgReqObj.ImageType = BrandSystems.MediaHandler.CustomType.Type.TrueColorMatte;
                }
                else if (objItemRequest.ImageType == null)
                {
                    imgReqObj.ImageType = BrandSystems.MediaHandler.CustomType.Type.NULL;
                }

                imgReqObj.MetaDataNeeded = new MetaData[] {
			MetaData.Height,
			MetaData.Width,
			MetaData.XResolution,
			MetaData.YResolution,
			MetaData.Format
		};

                imgReqObj.TaskToPerform = BrandSystems.MediaHandler.CustomType.Task.Convert;
                //Add by Prabhu on 14-sep-2010 to validate the image request object
                ///imgReqObj.ValidateConvert(imgReqObj);

                //Execute Task
                try
                {
                    imgResObj = (BrandSystems.MediaHandler.Output.ImageResponseObject)MHClient.ExecuteSynchronousTask(imgReqObj);
                }
                catch (MHValidationFailException ex)
                {
                    ErrDescription = ex.Message;
                    return false;

                }
                catch (Exception ex)
                {
                    ErrDescription = ex.Message;
                    return false;
                }



                if ((imgResObj.ErrorDetail == null | object.ReferenceEquals(imgResObj.ErrorDetail, "")))
                {
                    //'imgReqObj.SourcePath = imgReqObj.DestinationPath

                    //////Modified by : Asish 
                    //'imgReqObj.JobID = Guid.NewGuid()

                    //'For Each OBJMHT In ThreadQueue.objMHThread
                    //'    If System.Threading.Thread.CurrentThread.Name.ToString = OBJMHT.ThreadName Then
                    //'        OBJMHT.ResponseDownloadStatus = "Requested"
                    //'        OBJMHT.ResponseJobID = imgReqObj.JobID.ToString()
                    //'    End If
                    //'Next

                    //imgReqObj.MetaDataNeeded = New MetaData() {MetaData.Height, MetaData.Width, MetaData.XResolution, MetaData.YResolution, MetaData.Format}
                    imgReqObj.TaskToPerform = BrandSystems.MediaHandler.CustomType.Task.GetInfo;
                    //Get Height Width for destination image
                    //imgResObj = MHClient.ExecuteSynchronousTask(imgReqObj)
                    //If (imgResObj.ErrorDetail Is Nothing Or imgResObj.ErrorDetail Is "") Then
                    ItemInfo[0] = imgResObj.Width.ToString();
                    ItemInfo[1] = imgResObj.Height.ToString();
                    ItemInfo[2] = imgResObj.XResolution.ToString();
                    ItemInfo[3] = imgResObj.YResolution.ToString();
                    ItemInfo[4] = Path.GetExtension(imgReqObj.DestinationPath);
                    ItemInfo[5] = Path.GetFileName(imgReqObj.DestinationPath);
                    ItemInfo[6] = Path.GetFullPath(imgResObj.DestinationPath);
                    ////Moved after testing
                    //OBJMHT.DownloadStatus = "Done"
                    return true;
                    //Else
                    //ErrDescription = imgResObj.ErrorDetail
                    // Return False
                    // End If
                }
                else
                {
                    //need toworke
                    //ErrDescription = GetErrorDetails(Int32.Parse(imgResObj.ErrorDetail.Split(" ")(3)));
                    //OBJMHT.DownloadStatus = "Done"
                    return false;
                }
                //Else
                // ErrDescription = imgResObj.ErrorDetail
                //Return False
                // End If
            }
            catch (MHValidationFailException ex)
            {
                ErrDescription = ex.Message;
                return false;
                //need toworke
                //intErrorNumber = ex.ErrorCode();
                //ErrDescription = GetErrorDetails(ex.ErrorCode());
                //if (blnCallFromApplication == true) {
                //    OBJMHT.ErrorDescription = ErrDescription;
                //}
            }
            catch (Exception ex)
            {
                ErrDescription = ex.Message;
                //need toworke
                //if (blnCallFromApplication == true) {
                //    OBJMHT.ErrorDescription = ErrDescription;
                //}
                return false;
            }
            return false;
        }


        private string GetServerPath(string strServerName, string strPath)
        {
            return "\\\\" + strServerName + "\\" + strPath.Replace(":", "$");
            //Return strPath
        }
        //private object GenerateXMLInfo(string strPath, int intPicHeight, int intPicWidth, int intDpi, int intFolderId = 0)
        //{
        //    decimal decPresDec = 0;
        //    decimal m_decPicWidthInches = 0;
        //    decimal m_decPicHeightInches = 0;
        //    decimal m_decPicWidthMM = 0;
        //    decimal m_decPicHeightMM = 0;
        //    string m_intPicSize = null;
        //    string XMLfilePath = null;
        //    bool blnFileType = false ;
        //    bool previewStatus = true;
        //    string strFilename = null;
        //    try
        //    {
        //        blnFileType = FileIsGraphic(Path.GetFileName(strPath));
        //        strFilename = Path.GetFileName(strPath);
        //        decFileSize = GetFileSize(strPath);
        //        XMLfilePath = strPath.Replace(Path.GetFileName(strPath), "") + Path.GetFileNameWithoutExtension(strPath) + ".xml";

        //        //get preliminary fileinfo - size on disc
        //        m_intPicSize = decFileSize.ToString(FormatKB);
        //        decFileSize = decFileSize / 1024;
        //        m_intPicSize = decFileSize.ToString(FormatKB);
        //        //& "Kb"
        //        decFileSize = Math.Round(decFileSize * 1024, 0);

        //        if (intPicWidth > 0 & intPicHeight > 0 & intDpi > 0 & blnFileType == true)
        //        {
        //            //calculate
        //            m_decPicWidthInches = Math.Round((decimal)(intPicWidth / intDpi), 1);
        //            m_decPicHeightInches = Math.Round((decimal)(intPicHeight / intDpi), 1);

        //            m_decPicWidthMM = Math.Round(m_decPicWidthInches * decInchToMMConst);
        //            m_decPicHeightMM = Math.Round(m_decPicHeightInches * decInchToMMConst);
        //        }

        //        // Writing to XML file
        //        XmlTextWriter xmlTW = new XmlTextWriter(XMLfilePath, null);
        //        xmlTW.Formatting = Formatting.Indented;
        //        xmlTW.WriteStartDocument();
        //        xmlTW.WriteComment("Attribtes of " + strFilename);
        //        xmlTW.WriteStartElement("File");
        //        xmlTW.WriteElementString("Name", strFilename);

        //        //Only for Graphic files
        //        if (blnFileType == true)
        //        {
        //            xmlTW.WriteElementString("MAB_DPI", intDpi.ToString());
        //            xmlTW.WriteElementString("MAB_Heightpx", intPicHeight.ToString());
        //            xmlTW.WriteElementString("MAB_Widthpx", intPicWidth.ToString());
        //            xmlTW.WriteElementString("MAB_HeightInches", m_decPicHeightInches.ToString());
        //            xmlTW.WriteElementString("MAB_WidthInches", m_decPicWidthInches.ToString());
        //            xmlTW.WriteElementString("MAB_HeightMM", m_decPicWidthMM.ToString());
        //            xmlTW.WriteElementString("MAB_WidthMM", m_decPicHeightMM.ToString());
        //        }

        //        xmlTW.WriteElementString("MAB_FileSize", decFileSize.ToString());

        //        if (intFolderId > 0)
        //        {
        //            xmlTW.WriteElementString("MAB_App", "UploadToFolder");
        //        }
        //        else
        //        {
        //            xmlTW.WriteElementString("MAB_App", "Upload");
        //        }

        //        xmlTW.WriteElementString("MAB_NavId", intFolderId.ToString());

        //        if (Convert.ToInt32(m_intPicSize) > maxFileSize| blnFileType == false | previewStatus == false)
        //        {
        //            xmlTW.WriteElementString("MAB_Preview", "Static");
        //        }
        //        else
        //        {
        //            xmlTW.WriteElementString("MAB_Preview", "Preview");
        //        }

        //        xmlTW.WriteEndElement();
        //        xmlTW.WriteEndDocument();
        //        xmlTW.Flush();
        //        xmlTW.Close();

        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}

        //        public bool GenerateAllPreview(string strSourcePath, string strDestinationPath, ref string strErrDescription, ref int intBigWidth, ref int intBigHeight, ref int intThumbWidth, ref int intThumbHeight, ref int intEnlargeHeight, ref int intEnlargeWidth, ref int intDpi,
        //bool blnGenerateXml, int intFolderid = 0)
        //        {
        //            try
        //            {
        //                strErrDescription = "";
        //                bool blnFileType = false;
        //                bool blnstatus = false;
        //                string strFileName = null;
        //                string StrDestPath = null;
        //                int intPicHeight = 0;
        //                int intPicWidth = 0;
        //                bool blgetFileInfo = false;
        //                strErrDescription = "";

        //                if (Path.GetExtension(strSourcePath) == ".pdf")
        //                {
        //                    blnFileType = true;
        //                }
        //                else
        //                {
        //                    blnFileType = FileIsGraphic(Path.GetFileName(strSourcePath));
        //                }



        //                if (blnFileType)
        //                {
        //                    //need on workgin the SetImageDimensions
        //                    //blnstatus = SetImageDimensions(strErrDescription);

        //                    if (blnstatus == false)
        //                    {
        //                        return false;
        //                    }

        //                    //blgetFileInfo= getFileInfo(strSourcePath, intPicHeight, intPicWidth, intDpi, strErrDescription);
        //                    //getFileInfo(strSourcePath, intPicHeight, intPicWidth, intDpi, strErrDescription);

        //                    strDestinationPath = strDestinationPath.Replace("\\\\", "\\");

        //                    if (Path.GetExtension(strSourcePath) == ".gif")
        //                    {
        //                        strFileName = Path.GetFileName(strDestinationPath);
        //                    }
        //                    else
        //                    {
        //                        strFileName = Path.GetFileNameWithoutExtension(strDestinationPath) + ".jpg";
        //                    }

        //                    StrDestPath = strDestinationPath.Replace(Path.GetFileName(strDestinationPath), "");

        //                    ////Generate big preview
        //                    //need on workgin the GereratePreview m_intMaxBigHeight,m_intMaxBigWidth
        //                    int[] bigArray = GereratePreview(strSourcePath, StrDestPath + "big" + strFileName, 350, 350); // GereratePreview(strSourcePath, StrDestPath + "big" + strFileName, 350, 350, strErrDescription);

        //                    if ((!object.ReferenceEquals(strErrDescription, "")))
        //                    {
        //                        return false;
        //                    }
        //                    else
        //                    {
        //                        intBigHeight = bigArray[0];
        //                        intBigWidth = bigArray[1];
        //                    }

        //                    ////Generate thumb preview
        //                    ////Generate thumb preview
        //                    //need on workgin the GereratePreview m_intMaxThumbHeight,m_intMaxThumbWidth
        //                    int[] thumbArray = GereratePreview(strSourcePath, StrDestPath + "thumb" + strFileName, 112, 112);
        //                    if ((!object.ReferenceEquals(strErrDescription, "")))
        //                    {
        //                        return false;
        //                    }
        //                    else
        //                    {
        //                        intThumbHeight = thumbArray[0];
        //                        intThumbWidth = thumbArray[1];
        //                    }

        //                    ////Generate enlarge preview
        //                    //if (blnEnlargePreview == true)
        //                    //{
        //                    //    int[] enlargeArray = GereratePreview(strSourcePath, StrDestPath + "enlarge" + strFileName, m_intMaxEnlargeHeight, m_intMaxEnlargeWidth, strErrDescription);
        //                    //    if ((!object.ReferenceEquals(strErrDescription, "")))
        //                    //    {
        //                    //        return false;
        //                    //    }
        //                    //    else
        //                    //    {
        //                    //        intEnlargeHeight = enlargeArray[0];
        //                    //        intEnlargeWidth = enlargeArray[1];
        //                    //    }
        //                    //}
        //                }

        //                if (blnGenerateXml == true)
        //                {
        //                    ////Create XML File
        //                    GenerateXMLInfo(strDestinationPath, intPicHeight, intPicWidth, intDpi, intFolderid);
        //                }
        //            }
        //            catch (MHValidationFailException ex)
        //            {
        //                //strErrDescription = GetErrorDetails(ex.ErrorCode());
        //            }
        //            catch (Exception ex)
        //            {
        //                strErrDescription = ex.Message.ToString();
        //                return false;
        //            }

        //            return true;
        //        }



        public bool FileIsGraphic(string strFileName)
        {
            strFileName = strFileName.ToLower();
            if (strFileName.EndsWith("gif") | strFileName.EndsWith("jpg") | strFileName.EndsWith("jpeg") | strFileName.EndsWith("eps") | strFileName.EndsWith("tif") | strFileName.EndsWith("tiff") | strFileName.EndsWith("bmp") | strFileName.EndsWith("png") | strFileName.EndsWith("psd") | strFileName.EndsWith("emf"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public decimal GetFileSize(string strFileName)
        {
            int intSafetyCounter = 0;
            decimal decReturn = default(decimal);
            while (!File.Exists(strFileName) & intSafetyCounter < 50)
            {
                System.Threading.Thread.Sleep(100);
                intSafetyCounter += 1;
            }
            if (File.Exists(strFileName))
            {
                FileInfo my_fileInfo = new FileInfo(strFileName);
                decReturn = my_fileInfo.Length;
            }
            else
            {
                decReturn = 0;
            }

            return decReturn;
        }

        private object GetErrorDetails(int intErrorCode)
        {
            string strErrordetail = ""; //objContent.GetText("miscErrcode" + intErrorCode);

            return strErrordetail;
        }

        public string[] GetMimeTypes(ref string strErrDescription)
        {
            string[] strArrMimeTypes = new string[-1 + 1];
            //If Not MHClient.Ping(strErrDescription) Then
            //    Return strArrMimeTypes
            //End If
            MHClient.Ping();
            strErrDescription = "";

            try
            {
                strArrMimeTypes = MHClient.GetMimeType();
            }

            catch (BrandSystems.MediaHandler.MHValidationFailException ex)
            {
                //strErrDescription = GetErrorDetails(ex.ErrorCode());
            }
            catch (Exception ex)
            {
                strErrDescription = ex.Message.ToString();
            }

            return strArrMimeTypes;

        }

        public static string ReadS3TenantBasePath(int TenantID, int identity = 0)
        {
            TenantSelection tfp = new TenantSelection();
            string TenantFilePath = tfp.GetTenantFilePath(TenantID);
            if (identity != 0)
            {
                if ((int)AWSFilePathIdentity.DamOriginal == identity)
                    return (TenantFilePath + @"DAMFiles\Original\");
                else if ((int)AWSFilePathIdentity.DAMPreview == identity)
                    return (TenantFilePath + @"DAMFiles\Preview\");
                else return TenantFilePath;
            }
            else return TenantFilePath;
        }
    }

    #region "CLASS - ITEMREQUEST"
    [Serializable()]
    public class ItemRequest
    {

        #region "PRIVATE VARIABLES"
        ///Private m_intArchiveRefId As Integer 'commented as property type is changed to array from integer. by hashmat 02/Aug/2010
        ///Private m_intItemRefId As Integer    'commented as property type is changed to array from integer. by hashmat 02/Aug/2010
        //Added to change the property type. by hashmat 02/Aug/2010
        private int[] m_intArchiveRefId;
        //Added to change the property type. by hashmat 02/Aug/2010
        private int[] m_intItemRefId;
        private int m_intFormatRefId;
        private int m_intDimensionRefId;
        private int m_intTopLeftRadius;
        private int m_intTopRightRadius;
        private int m_intBottomLeftRadius;
        private int m_intBottomRightRadius;
        private string m_strColorspace;
        private string m_strDirection;
        private string m_strUnits;
        private string m_strCornerColor;
        private int m_intDegree;
        private int m_intQualityPercentage;
        private double m_dblCropTop;
        private double m_dblScaleInPercentage;
        private double m_dblResizeInPercentage;
        private double m_dblCropLeft;
        private double m_dblCropHeight;
        private double m_dblCropWidth;
        private double m_dblResizeHeight;
        private double m_dblResizeWidth;
        private double m_dblXResolutionDPI;
        private double m_dblYResolutionDPI;
        private double m_dblScaleHeight;
        private double m_dblScaleWidth;
        private string m_strImageType;
        #endregion
        private string m_strFormat;
        public Direction DirectionObj;
        public Units UnitsObj;
        #region "PROPERTIES"

        public enum Direction
        {
            Null,
            Horizontal,
            Vertical
        }

        public enum Units
        {
            Pixels,
            Inches,
            Millimeters
            //Centimeters
        }

        ///Public Property ArchiveRefId() As Integer
        public int[] ArchiveRefIds
        {
            get { return m_intArchiveRefId; }
            ///changed from ByVal Value As Integer() 
            set { m_intArchiveRefId = value; }
        }

        ///Public Property ItemRefId() As Integer
        public int[] ItemRefIds
        {
            get { return m_intItemRefId; }
            /// changed from Set(ByVal Value As Integer)
            set { m_intItemRefId = value; }
        }

        public double CropTop
        {
            get { return m_dblCropTop; }
            set { m_dblCropTop = value; }
        }

        public double CropLeft
        {
            get { return m_dblCropLeft; }
            set { m_dblCropLeft = value; }
        }

        public double CropWidth
        {
            get { return m_dblCropWidth; }
            set { m_dblCropWidth = value; }
        }

        public double ResizeHeight
        {
            get { return m_dblResizeHeight; }
            set { m_dblResizeHeight = value; }
        }

        public double ResizeWidth
        {
            get { return m_dblResizeWidth; }
            set { m_dblResizeWidth = value; }
        }

        public double CropHeight
        {
            get { return m_dblCropHeight; }
            set { m_dblCropHeight = value; }
        }

        public double ResizeInPercentage
        {
            get { return m_dblResizeInPercentage; }
            set { m_dblResizeInPercentage = value; }
        }

        public string ImageType
        {
            get { return m_strImageType; }
            set { m_strImageType = value; }
        }

        public int QualityPercentage
        {
            get { return m_intQualityPercentage; }
            set { m_intQualityPercentage = value; }
        }

        public double ScaleInPercentage
        {
            get { return m_dblScaleInPercentage; }
            set { m_dblScaleInPercentage = value; }
        }

        public string Format
        {
            get { return m_strFormat; }
            set { m_strFormat = value; }
        }

        public string Colorspace
        {
            get { return m_strColorspace; }
            set { m_strColorspace = value; }
        }

        public double ScaleHeight
        {
            get { return m_dblScaleHeight; }
            set { m_dblScaleHeight = value; }
        }

        public double ScaleWidth
        {
            get { return m_dblScaleWidth; }
            set { m_dblScaleWidth = value; }
        }

        public double XResolutionDPI
        {
            get { return m_dblXResolutionDPI; }
            set { m_dblXResolutionDPI = value; }
        }

        public double YResolutionDPI
        {
            get { return m_dblYResolutionDPI; }
            set { m_dblYResolutionDPI = value; }
        }

        public Units Unit
        {
            get { return UnitsObj; }
            set { UnitsObj = value; }
            //get {  return (Units)Enum.Parse(typeof(Units), m_strUnits); }
            //set
            //{
            //   // m_strUnits = (Units)Enum.Parse(typeof(Units).GetType(), value.ToString());
            // }
        }


        public int RotationAngle
        {
            get { return m_intDegree; }
            set { m_intDegree = value; }
        }

        public Direction FlipDirection
        {
            get { return DirectionObj; }
            set { DirectionObj = value; }
            //get { return (Direction)Enum.Parse(typeof(Direction), m_strDirection); }
            //set { m_strDirection = value; }//(BrandSystems.Marcom.Core.Utility.MediaHandler.ItemRequest.Direction)(value); }
        }

        public int FormatRefId
        {
            get { return m_intFormatRefId; }
            set { m_intFormatRefId = value; }
        }

        public int DimensionRefId
        {
            get { return m_intDimensionRefId; }
            set { m_intDimensionRefId = value; }
        }
        /// Following are the properties added by Syed on 09-Sep-2010 as Media Handler version changed to V2.3.3 
        public int TopLeftRadius
        {
            get { return m_intTopLeftRadius; }
            set { m_intTopLeftRadius = value; }
        }
        public int TopRightRadius
        {
            get { return m_intTopRightRadius; }
            set { m_intTopRightRadius = value; }
        }
        public int BottomLeftRadius
        {
            get { return m_intBottomLeftRadius; }
            set { m_intBottomLeftRadius = value; }
        }
        public int BottomRightRadius
        {
            get { return m_intBottomRightRadius; }
            set { m_intBottomRightRadius = value; }
        }
        public string CornerColor
        {
            get { return m_strCornerColor; }
            set { m_strCornerColor = value; }
        }
        /// Added by Syed ends here on 09-Sep-2010
        #endregion

        #region "FUNCTIONS AND METHODS"


        public ItemRequest()
        {
        }

        public void AssignUnit(string strUnitName, ref ItemRequest objItemRequest)
        {
            switch (strUnitName.ToLower())
            {
                case "px":
                    objItemRequest.Unit = Units.Pixels;
                    break;
                case "inches":
                    objItemRequest.Unit = Units.Inches;
                    break;
                case "mm":
                    objItemRequest.Unit = Units.Millimeters;
                    break;
            }
        }
        #endregion

    }
    #endregion

    #region "CLASS - MHValidationFailException"
    public class MHValidationFailException : Exception, ISerializable
    {

        public MHValidationFailException()
            : base()
        {
            // Add implementation.
        }

        public MHValidationFailException(string message)
            : base(message)
        {
            // Add implementation.
        }

        public MHValidationFailException(string message, Exception inner)
            : base(message, inner)
        {
            // Add implementation.
        }

        public MHValidationFailException(string message, Exception inner, string PropertyName, Guid JobID)
            : base(message, inner)
        {

            strPropertyName = PropertyName;
            gJobID = JobID;
            // Add implementation.
        }

        public MHValidationFailException(string message, Exception inner, string PropertyName, Guid JobID, int ErrorCode)
            : base(message, inner)
        {

            strPropertyName = PropertyName;
            gJobID = JobID;
            intErrorCode = ErrorCode;
            // Add implementation.
        }

        // This constructor is needed for serialization.
        protected MHValidationFailException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            // Add implementation.
        }


        private string strPropertyName;
        public string PropertyName
        {
            get { return strPropertyName; }
            set { strPropertyName = value; }
        }


        private Guid gJobID;
        public Guid JobID
        {
            get { return gJobID; }
            set { gJobID = value; }
        }

        private int intErrorCode;
        public int ErrorCode
        {
            get { return intErrorCode; }
            set { intErrorCode = value; }
        }

    }

}

    #endregion