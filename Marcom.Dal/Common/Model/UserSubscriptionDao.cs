using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Common.Model
{

	/// <summary>
	/// UserSubscriptionDao object for table 'CM_User_Subscription'.
	/// </summary>
	
	public partial class UserSubscriptionDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
		protected int _userid;
		protected int _entityid;
		protected DateTimeOffset _subscribedon;
		protected DateTimeOffset _lastupdatedon;
		protected bool _iscomplex;
		protected bool _ismultilevel;
		protected int _entitytypeid;
        protected int _filterroleoption;
        
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public UserSubscriptionDao() {}

        public UserSubscriptionDao(int pUserid, int pEntityid, DateTimeOffset pSubscribedOn, DateTimeOffset pLastUpdatedOn, bool pIsComplex, bool pIsMultiLevel, int pEntityTypeid, int pFilterRoleOption)
		{
			this._userid = pUserid; 
			this._entityid = pEntityid; 
			this._subscribedon = pSubscribedOn; 
			this._lastupdatedon = pLastUpdatedOn; 
			this._iscomplex = pIsComplex; 
			this._ismultilevel = pIsMultiLevel; 
			this._entitytypeid = pEntityTypeid;
            this._filterroleoption = pFilterRoleOption;
		}
				
		public UserSubscriptionDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}
		
		public virtual int Userid
		{
			get { return _userid; }
			set { _bIsChanged |= (_userid != value); _userid = value; }
			
		}
		
		public virtual int Entityid
		{
			get { return _entityid; }
			set { _bIsChanged |= (_entityid != value); _entityid = value; }
			
		}
		
		public virtual DateTimeOffset SubscribedOn
		{
			get { return _subscribedon; }
			set { _bIsChanged |= (_subscribedon != value); _subscribedon = value; }
			
		}
		
		public virtual DateTimeOffset LastUpdatedOn
		{
			get { return _lastupdatedon; }
			set { _bIsChanged |= (_lastupdatedon != value); _lastupdatedon = value; }
			
		}
		
		public virtual bool IsComplex
		{
			get { return _iscomplex; }
			set { _bIsChanged |= (_iscomplex != value); _iscomplex = value; }
			
		}
		
		public virtual bool IsMultiLevel
		{
			get { return _ismultilevel; }
			set { _bIsChanged |= (_ismultilevel != value); _ismultilevel = value; }
			
		}
		
		public virtual int EntityTypeid
		{
			get { return _entitytypeid; }
			set { _bIsChanged |= (_entitytypeid != value); _entitytypeid = value; }
			
		}
        public virtual int FilterRoleOption
        {
            get { return _filterroleoption; }
            set { _bIsChanged |= (_filterroleoption != value); _filterroleoption = value; }

        }


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
			public const string UserSubscriptionDao = "UserSubscriptionDao";			
			public const string Id = "Id";			
			public const string Userid = "Userid";			
			public const string Entityid = "Entityid";			
			public const string SubscribedOn = "SubscribedOn";			
			public const string LastUpdatedOn = "LastUpdatedOn";			
			public const string IsComplex = "IsComplex";			
			public const string IsMultiLevel = "IsMultiLevel";			
			public const string EntityTypeid = "EntityTypeid";
            public const string FilterRoleOption = "FilterRoleOption";	
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
			public const string UserSubscriptionDao = "UserSubscriptionDao";			
			public const string Id = "Id";			
			public const string Userid = "Userid";			
			public const string Entityid = "Entityid";			
			public const string SubscribedOn = "SubscribedOn";			
			public const string LastUpdatedOn = "LastUpdatedOn";			
			public const string IsComplex = "IsComplex";			
			public const string IsMultiLevel = "IsMultiLevel";			
			public const string EntityTypeid = "EntityTypeid";
            public const string FilterRoleOption = "FilterRoleOption";	
		}

		#endregion
		
		
	}
	
}
