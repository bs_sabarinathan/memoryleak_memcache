﻿(function (ng, app) {
    "use strict"; function muiadmincloudsettingsCtrl($scope, $timeout, $resource, $location, $translate, AdminService) {
        AdminService.getcloudsettings().then(function (result) { if (result.Response != null) { $scope.accesskeyId = result.Response.AccesskeyID; $scope.secretacesskey = result.Response.SecretAccesskey; $scope.bucketName = result.Response.BusketName; $scope.serviceurl = result.Response.ServiceUrl; $scope.regionendpoint = result.Response.RegionEndPoint; $scope.filesystemMode = result.Response.FilesystemMode; $scope.UploaderUrl = result.Response.UploaderURL; } })
        $scope.savecloudsettings = function () {
            var cloudinfo = {}; cloudinfo.accesskeyId = $scope.accesskeyId; cloudinfo.secretacesskey = $scope.secretacesskey; cloudinfo.bucketName = $scope.bucketName; cloudinfo.serviceurl = $scope.serviceurl; cloudinfo.regionendpoint = $scope.regionendpoint; cloudinfo.filesystemMode = $scope.filesystemMode; cloudinfo.UploaderUrl = $scope.UploaderUrl; AdminService.savecloudsettings(cloudinfo).then(function (result) {
                if (result.Response) { NotifySuccess($translate.instant('LanguageContents.Res_4717.Caption')); }
                else { NotifyError($translate.instant('LanguageContents.Res_4329.Caption')); }
            })
        }
    }
    app.controller("mui.admin.cloudsettingsCtrl", ['$scope', '$timeout', '$resource', '$location', '$translate', 'AdminService', muiadmincloudsettingsCtrl]);
})(angular, app);