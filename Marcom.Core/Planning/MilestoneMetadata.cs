﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{
  internal  class MilestoneMetadata : IMilestoneMetadata
  {
      #region Member Variables
      protected int _entityId;
      protected int _parententityId;
      protected string _parententityname;
      protected string _ParentEntitytypeShortDescription;
      protected string _ParentEntitytypeColorCode;
      protected IList<IAttributeData> _attributedata;
      #endregion

      #region Public Properties



      public int EntityId
      {
          get { return _entityId; }
          set { _entityId = value; }

      }

      public int ParentEntityId
      {
          get { return _parententityId; }
          set { _parententityId = value; }

      }
      
      public string ParentEntityName
      {
          get { return _parententityname; }
          set { _parententityname = value; }
      }

      public string ParentEntityTypeShortDescription
      {
          get { return _ParentEntitytypeShortDescription; }
          set { _ParentEntitytypeShortDescription = value; }
      }

      public string ParentEntityTypeColorCode
      {
          get { return _ParentEntitytypeColorCode; }
          set { _ParentEntitytypeColorCode = value; }
      }
      public IList<IAttributeData> AttributeData
      {
          get { return _attributedata; }
          set { _attributedata = value; }

      }


      #endregion 

  }
}
