﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Core.Metadata.Interface
{
    public interface IMetadataVersion
    {
        #region Public Properties

        int ID
        {
            get;
            set;

        }


        string Name
        {
            get;
            set;

        }

        string Description
        {
            get;
            set;

        }

        int State
        {
            get;
            set;

        }

        DateTime StartDate
        {
            get;
            set;

        }

        DateTime EndDate
        {
            get;
            set;

        }


        #endregion
    }
}
