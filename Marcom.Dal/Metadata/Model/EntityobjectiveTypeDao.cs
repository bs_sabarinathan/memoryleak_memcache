﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{
    public partial class EntityobjectiveTypeDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _entitytypeid;
        protected int _objectivetypeid;
        protected int _objectivebasetypeid;
        protected int _unitid;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public EntityobjectiveTypeDao() { }

        public EntityobjectiveTypeDao(int pId, int pEntitytasktypeid, int pObjectivetypeid, int pObjectivebasetypeid, int pUnitid)
        {
            this._id = pId;
            this._entitytypeid = pEntitytasktypeid;
            this._objectivetypeid = pObjectivetypeid;
            this._objectivebasetypeid = pObjectivebasetypeid;
            this._unitid = pUnitid;
        }

        public EntityobjectiveTypeDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }


        public virtual int EntitytypeId
        {
            get { return _entitytypeid; }
            set { _bIsChanged |= (_entitytypeid != value); _entitytypeid = value; }

        }

        public virtual int ObjectiveTypeId
        {
            get { return _objectivetypeid; }
            set { _bIsChanged |= (_objectivetypeid != value); _objectivetypeid = value; }

        }

        public virtual int ObjectiveBaseTypeId
        {
            get { return _objectivebasetypeid; }
            set { _bIsChanged |= (_objectivebasetypeid != value); _objectivebasetypeid = value; }

        }

        public virtual int UnitId
        {
            get { return _unitid; }
            set { _bIsChanged |= (_unitid != value); _unitid = value; }

        }

        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string EntityobjectiveTypeDao = "EntityobjectiveTypeDao";
            public const string Id = "Id";
            public const string EntitytypeId = "EntitytypeId";
            public const string ObjectiveTypeId = "ObjectiveTypeId";
            public const string ObjectiveBaseTypeId = "ObjectiveBaseTypeId";
            public const string UnitId = "UnitId";

        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string EntityobjectiveTypeDao = "EntityobjectiveTypeDao";
            public const string Id = "Id";
            public const string EntitytypeId = "EntitytypeId";
            public const string ObjectiveTypeId = "ObjectiveTypeId";
            public const string ObjectiveBaseTypeId = "ObjectiveBaseTypeId";
            public const string UnitId = "UnitId";

        }

        #endregion
    }
}
