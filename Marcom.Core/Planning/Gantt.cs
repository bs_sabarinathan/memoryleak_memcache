﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{

    /// <summary>
    /// CurrencyType interface for table 'PM_CurrencyType'.
    /// </summary>

    public class Gantt
    {
        /// <summary>
        /// Represent news feed
        /// </summary>


        private int intID;

        public int ID
        {
            get { return intID; }
            set { intID = value; }
        }

        private int intParentID;
        public int ParentID
        {
            get { return intParentID; }
            set { intParentID = value; }
        }

        private int intTypeID;
        public int TypeID
        {
            get { return intTypeID; }
            set { intTypeID = value; }
        }

        private int intLevel;
        public int Level
        {
            get { return intLevel; }
            set { intLevel = value; }
        }

        private string strName;
        public string Name
        {
            get { return strName; }
            set { strName = value; }
        }

        private string strStatus;
        public string Status
        {
            get { return strStatus; }
            set { strStatus = value; }
        }

        private string strStartEndDate;
        public string StartEndDate
        {
            get { return strStartEndDate; }
            set { strStartEndDate = value; }
        }

        private string strTypeName;
        public string TypeName
        {
            get { return strTypeName; }
            set { strTypeName = value; }
        }


        private int strTotalChildrenCount;
        public int TotalChildrenCount
        {
            get { return strTotalChildrenCount; }
            set { strTotalChildrenCount = value; }
        }


        private string strColorCode;
        public string ColorCode
        {
            get { return strColorCode; }
            set { strColorCode = value; }
        }


        private string strUniqueKey;
        public string UniqueKey
        {
            get { return strUniqueKey; }
            set { strUniqueKey = value; }
        }


        private string strBudget;
        public string Budget
        {
            get { return strBudget; }
            set { strBudget = value; }
        }

        private string strOwner;
        public string Owner
        {
            get { return strOwner; }
            set { strOwner = value; }
        }

        private string strStartEndForDate;
        public string StartEndForDate
        {
            get { return strStartEndForDate; }
            set { strStartEndForDate = value; }
        }

        private string strCountry;
        public string Country
        {
            get { return strCountry; }
            set { strCountry = value; }
        }

        private string strEndCustomerSize;
        public string EndCustomerSize
        {
            get { return strEndCustomerSize; }
            set { strEndCustomerSize = value; }
        }
        private string strEndCustomerVertical;
        public string EndCustomerVertical
        {
            get { return strEndCustomerVertical; }
            set { strEndCustomerVertical = value; }
        }
        private string strSaleschannel;
        public string Saleschannel
        {
            get { return strSaleschannel; }
            set { strSaleschannel = value; }
        }
        private string strStrategy;
        public string Strategy
        {
            get { return strStrategy; }
            set { strStrategy = value; }
        }
        private string strOffer;
        public string Offer
        {
            get { return strOffer; }
            set { strOffer = value; }
        }
        private string strSponsoring;
        public string Sponsoring
        {
            get { return strSponsoring; }
            set { strSponsoring = value; }
        }
        private string strRingi;
        public string Ringi
        {
            get { return strRingi; }
            set { strRingi = value; }
        }
        private int strPlannedBudget;
        public int PlannedBudget
        {
            get { return strPlannedBudget; }
            set { strPlannedBudget = value; }
        }
        private int strApprovedAllocations;
        public int ApprovedAllocations
        {
            get { return strApprovedAllocations; }
            set { strApprovedAllocations = value; }
        }
        private int strCommited;
        public int Commited
        {
            get { return strCommited; }
            set { strCommited = value; }
        }
        private int strSpent;
        public int Spent
        {
            get { return strSpent; }
            set { strSpent = value; }
        }

    }

    /// <summary>
    /// Represent news feed
    /// </summary>

    public class GanttHolder
    {

        private List<Gantt> objGanttEntityHolder = new List<Gantt>();
        public List<Gantt> GanttEntityHolder
        {
            get { return objGanttEntityHolder; }
            set { objGanttEntityHolder = value; }
        }

    }



}
