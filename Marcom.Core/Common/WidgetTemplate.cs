﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;

namespace BrandSystems.Marcom.Core.Common
{
    internal  class WidgetTemplate :IWidgetTemplate
    {
        #region Member Variables

		protected int _id;
		protected string _TemplateName;
        protected string _TemplateDescription;
		#endregion
		
		#region Constructors
		public WidgetTemplate() {}

        public WidgetTemplate(string pTemplateName)
		{
            this._TemplateName = pTemplateName; 
		}

        public WidgetTemplate(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}
		
		 public string TemplateName
		{
            get { return _TemplateName; }
			set 
			{
			  if (value != null && value.Length > 100)
                  throw new ArgumentOutOfRangeException("TemplateName", "TemplateName value, cannot contain more than 100 characters");
              _TemplateName = value; 
			}
			
		}

         public string TemplateDescription
         {
             get { return _TemplateDescription; }
             set
             {
                 if (value != null && value.Length > 100)
                     throw new ArgumentOutOfRangeException("TemplateDescription", "TemplateDescription value, cannot contain more than 500 characters");
                 _TemplateDescription = value;
             }

         }

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
            WidgetTemplate castObj = null;
			try
			{
                castObj = (WidgetTemplate)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
    }
}
