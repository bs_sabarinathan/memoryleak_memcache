﻿using System;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Common
{
    internal class AvailableUpdates : IAvailableUpdates
    {
        #region Member Variables

        protected string _NewVersion;
        protected string _Feature;
        protected string _DateReleased;
        protected int _Id;
        protected string _DocPath;
        #endregion

        #region Constructors
        public AvailableUpdates() { }

        public AvailableUpdates(string pNewVersion, string pFeature, string pDateReleased, int pId, string pDocPath)
        {
            this._NewVersion = pNewVersion;
            this._Feature = pFeature;
            this._DateReleased = pDateReleased;
            this._Id = pId;
            this._DocPath = pDocPath;
        }
        #endregion

        #region Public Properties

        public string NewVersion
        {
            get { return _NewVersion; }
            set { _NewVersion = value; }

        }

        public string Feature
        {
            get { return _Feature; }
            set { _Feature = value; }
        }

        public string DateReleased
        {
            get { return _DateReleased; }
            set { _DateReleased = value; }
        }
        public int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
        public string DocPath
        {
            get { return _DocPath; }
            set { _DocPath = value; }
        }
        #endregion


    }
}
