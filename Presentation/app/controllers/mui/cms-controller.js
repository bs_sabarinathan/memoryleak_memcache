﻿(function (ng, app) {
    "use strict";
    function muiplanningtoolCtrl($scope) {
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.cmsCtrl']"));
        });
    }
    app.controller('mui.cmsCtrl', ['$scope',muiplanningtoolCtrl]);
})(angular, app);