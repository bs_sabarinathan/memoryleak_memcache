﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{
    /// <summary>
    /// SupplierDao object for table 'PM_Supplier'.
    /// </summary>
    public partial class SupplierDao : BaseDao, ICloneable
    {
        #region Member Variables
		
        protected int _id;
        protected string _companyname;
        protected string _department;
        protected string _address1;
        protected string _address2;
        protected string _city;
        protected string _zipcode;
        protected string _state;
        protected string _country;
        protected string _vat;
        protected string _phone;
        protected string _email;
        protected string _fax;
        protected string _customernumber;

		#endregion
		
		#region Constructors
        public SupplierDao() { }
        public SupplierDao(string pCompanyName, string pDepartment, string pAddress1, string pAddress2, string pCity, string pZipcode, string pState, string pCountry, string pVat, string pPhone, string pEmail, string pFax, string pCustomerNamer)
        {
            this._companyname = pCompanyName;
            this._department = pDepartment;
            this._address1 = pAddress1;
            this._address2 = pAddress2;
            this._city = pCity;
            this._zipcode = pZipcode;
            this._state = pState;
            this._country = pCountry;
            this._vat = pVat;
            this._phone = pPhone;
            this._email = pEmail;
            this._fax = pFax;
            this._customernumber = pCustomerNamer;
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }

        }

        public virtual string CompanyName
        {
            get { return _companyname; }
            set { _companyname = value; }
        }

        public virtual string Department
        {
            get { return _department; }
            set { _department = value; }
        }

        public virtual string Address1
        {
            get { return _address1; }
            set { _address1 = value; }
        }

        public virtual string Address2
        {
            get { return _address2; }
            set { _address2 = value; }
        }

        public virtual string City
        {
            get { return _city; }
            set { _city = value; }
        }
        public virtual string ZipCode
        {
            get { return _zipcode; }
            set { _zipcode = value; }
        }
        public virtual string State
        {
            get { return _state; }
            set { _state = value; }
        }
        public virtual string Country
        {
            get { return _country; }
            set { _country = value; }
        }
        public virtual string Vat
        {
            get { return _vat; }
            set { _vat = value; }
        }
        public virtual string Phone
        {
            get { return _phone; }
            set { _phone = value; }
        }
        public virtual string Email
        {
            get { return _email; }
            set { _email = value; }

        }
        public virtual string Fax
        {
            get { return _fax; }
            set { _fax = value; }
        }
        public virtual string CustomerNumber
        {
            get { return _customernumber; }
            set { _customernumber = value; }
        }
        #endregion
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string SupplierDao = "SupplierDao";
            public const string Id = "Id";
            public const string CompanyName = "CompanyName";
            public const string Department = "Department";
            public const string Address1 = "Address1";
            public const string Address2 = "Address2";
            public const string City = "City";
            public const string ZipCode = "ZipCode";
            public const string State = "State";
            public const string Country = "Country";
            public const string Vat = "Vat";
            public const string Phone = "Phone";
            public const string Email = "Email";
            public const string Fax = "Fax";
            public const string CustomerNumber = "CustomerNumber";	
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string SupplierDao = "SupplierDao";
            public const string Id = "Id";
            public const string CompanyName = "CompanyName";
            public const string Department = "Department";
            public const string Address1 = "Address1";
            public const string Address2 = "Address2";
            public const string City = "City";
            public const string ZipCode = "ZipCode";
            public const string State = "State";
            public const string Country = "Country";
            public const string Vat = "Vat";
            public const string Phone = "Phone";
            public const string Email = "Email";
            public const string Fax = "Fax";
            public const string CustomerNumber = "CustomerNumber";				
		}

		#endregion
    }
}
