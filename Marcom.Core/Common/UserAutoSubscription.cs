using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;

namespace BrandSystems.Marcom.Core.Common
{

	/// <summary>
	/// UserAutoSubscription object for table 'CM_UserAutoSubscription'.
	/// </summary>
	
	internal class UserAutoSubscription : IUserAutoSubscription 
	{
		#region Member Variables

		protected int _userid;
		protected int _entityid;
		protected ISubscriptionType _subscriptionid;
		protected int _entitytypeid;
		protected bool _isvalid;
		
		
		#endregion
		
		#region Constructors
		public UserAutoSubscription() {}
			
		public UserAutoSubscription(int pUserid, int pEntityid, SubscriptionType pSubscriptionid, int pEntityTypeid, bool pIsValid)
		{
			this._userid = pUserid; 
			this._entityid = pEntityid; 
			this._subscriptionid = pSubscriptionid; 
			this._entitytypeid = pEntityTypeid; 
			this._isvalid = pIsValid; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Userid
		{
			get { return _userid; }
			set { _userid = value; }
			
		}
		
		public int Entityid
		{
			get { return _entityid; }
			set { _entityid = value; }
			
		}
		
		public ISubscriptionType Subscriptionid
		{
			get { return _subscriptionid; }
			set { _subscriptionid = value; }
			
		}
		
		public int EntityTypeid
		{
			get { return _entitytypeid; }
			set { _entitytypeid = value; }
			
		}
		
		public bool IsValid
		{
			get { return _isvalid; }
			set { _isvalid = value; }
			
		}
		

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			UserAutoSubscription castObj = null;
			try
			{
				castObj = (UserAutoSubscription)obj;
			} catch(Exception) { return false; } 
			return castObj.GetHashCode() == this.GetHashCode();
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			return this.GetType().FullName.GetHashCode();
				
		}
		#endregion
		
	}
	
}
