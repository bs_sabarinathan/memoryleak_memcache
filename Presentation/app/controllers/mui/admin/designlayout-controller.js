﻿(function (ng, app) {
    "use strict "; function designlayoutCntrl($scope, $resource, $location, $compile, $timeout, $translate, AdminService) {
        $scope.TabEntityType = 1; $scope.layoutLocation = 1; $scope.TabEntityTypeOptions = [{ "Id": 1, "Caption": $translate.instant('LanguageContents.Res_4995.Caption') }, { "Id": 2, "Caption": $translate.instant('LanguageContents.Res_254.Caption') }, { "Id": 3, "Caption": $translate.instant('LanguageContents.Res_4535.Caption') }]; $scope.layoutLocationOptions = [{ "Id": 1, "Caption": $translate.instant('LanguageContents.Res_9.Caption') }, { "Id": 2, "Caption": $translate.instant('LanguageContents.Res_4994.Caption') }, { "Id": 3, "Caption": $translate.instant('LanguageContents.Res_4535.Caption') }]; var saveCover = { "root": { "block": [] } }; $scope.LayoutData = []; $timeout(function () { LoadFinancialReportSettingsXML(); }, 100); $scope.RepaintBlocks = function (obj, blocks, layoutlength) {
            if (layoutlength > blocks.length) { var loopcnt = layoutlength - blocks.length; if (loopcnt != 0) { for (var l = 0; l < loopcnt; l++) { var list = { "BlockWidth": 4, "listName": generateUUID(), "listID": generateUUID(), "listCaption": "Available sections", "list": [] }; blocks.push(list); } } }
            else {
                var optionstoAdd = []; for (var op = 0, val; val = blocks[op++];) { for (var c = 0, li; li = val.list[c++];) { optionstoAdd.push({ "label": li.label, "listUniqueID": li.listUniqueID }); } }
                obj.lists = []; var listStruct = []; for (var t = 0; t < layoutlength; t++) {
                    var listobj = { "BlockWidth": 4, "listName": generateUUID(), "listID": generateUUID(), "listCaption": "Available sections", "list": [] }; if (t == 0)
                        listobj["list"] = optionstoAdd; obj.lists.push(listobj);
                }
            }
        }
        function generateUUID() { var d = new Date().getTime(); var uuid = 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function (c) { var r = (d + Math.random() * 16) % 16 | 0; d = Math.floor(d / 16); return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16); }); return uuid; }; $scope.locationFilter = function (location) {
            if ($scope.TabEntityType == 1) { return true; }
            else if ($scope.TabEntityType == 2 || $scope.TabEntityType == 3) {
                if (location.Id > 1)
                    return false; else
                    return true;
            }
        }; $scope.RefreshLayout = function () {
            if ($scope.TabEntityType == 2 || $scope.TabEntityType == 3) { if ($scope.layoutLocation > 1) { $scope.layoutLocation = 1; } }
            LoadFinancialReportSettingsXML();
        }
        $scope.RefreshBlocks = function () { LoadFinancialReportSettingsXML(); }
        $scope.Addnewrow = function () { var block = { "LayoutStructure": "1", "lists": [{ "BlockWidth": "4", "list": [], "listCaption": "Available sections", "listID": generateUUID(), "listName": generateUUID() }], "RowNumber": $scope.LayoutData.root.block.length + 1 }; $scope.LayoutData.root.block.push(block); }
        $scope.RemoveRow = function (blocks, index) {
            if (index > 0) {
                var optionstoAdd = []; for (var p = 0, parent; parent = blocks.lists[p++];) {
                    for (var op = 0, val; val = parent.list[op++];) { optionstoAdd.push({ "label": val.label, "listUniqueID": val.listUniqueID }); }
                    parent.list = [];
                }
            }
            if (optionstoAdd.length > 0) { var rel = $scope.LayoutData.root.block[index - 1]; if (rel.lists.length > 0) { var lists = rel.lists[rel.lists.length - 1]; if (lists != null) { for (var op = 0, li; li = optionstoAdd[op++];) { lists.list.push({ "label": li.label, "listUniqueID": li.listUniqueID }); } } } }
            $scope.LayoutData.root.block.splice(index, 1);
        }
        var flag = 1; $scope.updateReportBlock = function () {
            var validflag = isRowSpanValidated(); if (validflag == 1) { var alertText = ""; saveCover.root.block = $scope.LayoutData.root.block; var insertupdatelayout = {}; insertupdatelayout.reportBlockData = saveCover; insertupdatelayout.TabType = $scope.layoutLocation; insertupdatelayout.TabLocation = $scope.TabEntityType; AdminService.insertupdatetabsettings(insertupdatelayout).then(function (result) { if (result != null && result != 0) { NotifySuccess($translate.instant('LanguageContents.Res_4785.Caption')); LoadFinancialReportSettingsXML(); } }); }
            else {
                if (validflag == 0) { bootbox.alert($translate.instant('LanguageContents.Res_4810.Caption')); }
                else { bootbox.alert($translate.instant('LanguageContents.Res_4460.Caption')); }
            }
        }
        function isRowSpanValidated() {
            flag = 1, rowMaxSpan = 0; for (var i = 0, blk; blk = $scope.LayoutData.root.block[i++];) {
                rowMaxSpan = 0; for (var j = 0, lis; lis = blk.lists[j++];) {
                    if (rowMaxSpan <= 12)
                        rowMaxSpan += parseInt(lis.BlockWidth); else
                        if (flag == true) { return 0; }
                    if (rowMaxSpan > 12) { return 0; }
                    if (lis.list == 0)
                        if (flag == true) { return -1; }
                }
            }
            return flag;
        }
        function LoadFinancialReportSettingsXML() { AdminService.GetLayoutData($scope.layoutLocation, $scope.TabEntityType).then(function (res) { var rst = []; rst = JSON.parse(res.Response); var block = rst.root.block; $scope.LayoutData = rst; }); }
        $scope.$watch('LayoutData', function (model) { $scope.modelAsJsondynamic = angular.toJson(model, true); }, true); $scope.apply = function () { var tabtypename = $.grep($scope.TabEntityTypeOptions, function (rel) { return rel.Id == $scope.TabEntityType })[0]; var locationname = $.grep($scope.layoutLocationOptions, function (rel) { return rel.Id == $scope.layoutLocation })[0]; var inserttasktempparams = {}; inserttasktempparams.tabtypename = tabtypename.Caption; inserttasktempparams.locationname = locationname.Caption; AdminService.LayoutSettingsApplyChanges(inserttasktempparams).then(function (result) { if (result != null && result != 0) { NotifySuccess($translate.instant('LanguageContents.Res_4785.Caption')); LoadFinancialReportSettingsXML(); } }); }
        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
            return ($q.reject(response.data.message));
        }
        function handleSuccess(response) { return (response.data); }
    }
    app.controller("mui.admin.designlayoutCntrl", ['$scope', '$resource', '$location', '$compile', '$timeout', '$translate', 'AdminService', designlayoutCntrl]);
})(angular, app);