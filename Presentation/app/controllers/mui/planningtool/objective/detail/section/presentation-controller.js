﻿(function (ng, app) {
    "use strict"; 
    function muiplanningtoolobjectivedetailpresentationCtrl($scope, $timeout, $stateParams, $http, $compile, $window, $resource, PresentationService) {        
        $scope.addUser = function () {
            $('#userModal').modal('show');
        }
        $scope.date2 = new Date(); $scope.presentationList = []; $scope.SelectionList = []; var routeid = $stateParams.ID; this.setSelected = function (c) {
            var checkopt = $.grep($scope.SelectionList, function (e) { return e == c.id; }); if (c.checked1 == true && checkopt.length == 0) { $scope.SelectionList.push(c.id); }
else if(c.checked1==false&&checkopt.length>0){$scope.SelectionList.splice($.inArray(c.id,$scope.SelectionList),1);}}
$scope.toggledate=function(){$scope.isDateteVisible=!$scope.isDateteVisible;};$scope.isDateteVisible=false;$scope.toggletext="Show publication for sub levels";$scope.isVisible=false;$scope.editPage=function(){$scope.showEditor=true;$scope.showHolder=false;$scope.htmlEditorText=$scope.presentationList.Content;};$('#PresentationObj').on('LoadPresentationsObj',function(event,ID){$scope.myTree=[];$('#EditorHolder').html('');routeid=ID;loadPresentationObj();});$scope.myTree=[];loadPresentationObj();function loadPresentationObj(){PresentationService.GetEntitydescendants(routeid).then(function(GetTree){$scope.myTree.push(JSON.parse(GetTree.Response));});PresentationService.GetPresentationById(routeid).then(function(EntityPresentationlist){if(EntityPresentationlist.Response!=null){$scope.showEditor=false;$scope.presentationList=EntityPresentationlist.Response;$('#EditorHolder').html($scope.presentationList.Content);$scope.showHolder=true;}});}
$scope.SaveEntityPresentationContent=function(){var presentationObj={};presentationObj.EntityID=routeid;presentationObj.entityList=$scope.SelectionList;presentationObj.PublishedOn=$scope.date2;if($scope.htmlEditorText==undefined){presentationObj.Content=null;}
else{presentationObj.Content=$scope.htmlEditorText;}
PresentationService.InsertPresentation(presentationObj).then(function(presentationObj1){$('#userModal').modal('hide');$scope.showEditor=false;$scope.showHolder=true;$('#EditorHolder').html($scope.htmlEditorText);});$scope.$on("$destroy",function(){RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.objective.detail.presentationCtrl']"));});}}

    app.controller("mui.planningtool.objective.detail.presentationCtrl", ['$scope', '$timeout', '$stateParams', '$http', '$compile', '$window', '$resource', 'PresentationService', muiplanningtoolobjectivedetailpresentationCtrl]);
})(angular, app);