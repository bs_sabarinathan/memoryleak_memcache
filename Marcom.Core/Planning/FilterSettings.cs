﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{
    /// <summary>
    /// FilterSettings object for table 'PM_Filter'.
    /// </summary>
    internal class FilterSettings : IFilterSettings
    {
         #region Member Variables

        protected int _filterid;
        protected string _filtername;
        protected string _keyword;
        protected int _userid;
        protected int _typeid;
        protected string _entitytypeid;
        protected string _startdate;
        protected string _enddate;
        protected string _wherecondition;
        protected IList<IFiltersettingsValues> _filtervalues;
        protected int _isdetailfilter;
        protected string _entitymemberid;        
        #endregion

        #region Constructors
        public FilterSettings() { }

        public FilterSettings(int pFilterId, string pFiletName, string PKeyword, int pUserId, int pTypeId, string pEntityTypeId, string pStartDate, string pEndDate, string pWhereCondition, int pIsDetailFilter, string pentitymemberid)
        {
            this._filterid = pFilterId;
            this._filtername = pFiletName;
            this._keyword = PKeyword;
            this._userid = pUserId;
            this._typeid = pTypeId;
            this._entitytypeid = pEntityTypeId;
            this._startdate = pStartDate;
            this._enddate = pEndDate;
            this._wherecondition = pWhereCondition;
            this._isdetailfilter = pIsDetailFilter;
            this._entitymemberid = pentitymemberid;
        }

        #endregion

        #region Public Properties


        public int FilterID
        {
            get { return _filterid; }
            set { _filterid = value; }

        }

        public string FilterName
        {
            get { return _filtername; }
            set { _filtername = value; }

        }

        public string Keyword
        {
            get { return _keyword; }
            set { _keyword = value; }

        }

        public int UserID
        {
            get { return _userid; }
            set { _userid = value; }

        }

        public int TypeID
        {
            get { return _typeid; }
            set { _typeid = value; }
        }

        public string EntityTypeID
        {
            get { return _entitytypeid; }
            set { _entitytypeid = value; }
        }
        public string StartDate
        {
            get { return _startdate; }
            set { _startdate = value; }
        }

        public string EndDate
        {
            get { return _enddate; }
            set { _enddate = value; }
        }
        public string WhereCondition
        {
            get { return _wherecondition; }
            set { _wherecondition = value; }
        }
        public IList<IFiltersettingsValues> FilterValues
        {
            get { return _filtervalues; }
            set { _filtervalues = value; }
        }
        public int IsDetailFilter
        {
            get { return _isdetailfilter; }
            set { _isdetailfilter = value; }
        }
       
        public string EntityMemberID
        {
            get { return _entitymemberid; }
            set { _entitymemberid = value; }
        }
        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            FiltersettingsValues castObj = null;
            try
            {
                castObj = (FiltersettingsValues)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                this._filterid.Equals(castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _filterid.GetHashCode();
            return hash;
        }
        #endregion
    }
}
