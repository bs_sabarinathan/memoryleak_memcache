using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;

namespace BrandSystems.Marcom.Core.Common
{

	/// <summary>
	/// UserDefaultSubscription object for table 'CM_UserDefaultSubscription'.
	/// </summary>
	
	internal class UserDefaultSubscription : IUserDefaultSubscription 
	{
		#region Member Variables

        protected int _id;
		protected int _userid;
        protected string _subscriptiontypeid;
        protected string _mailsubscriptiontypeid;
		
		
		#endregion
		
		#region Constructors
		public UserDefaultSubscription() {}

        public UserDefaultSubscription(int pUserid, string pSubscriptionTypeid, string pMailSubscriptionTypeid)
		{
			this._userid = pUserid; 
			this._subscriptiontypeid = pSubscriptionTypeid;
            this._mailsubscriptiontypeid = pMailSubscriptionTypeid; 

		}
        public UserDefaultSubscription(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _id = value; }

        }
		public int Userid
		{
			get { return _userid; }
			set { _userid = value; }
			
		}

        public string SubscriptionTypeid
		{
			get { return _subscriptiontypeid; }
			set { _subscriptiontypeid = value; }
			
		}
        public string MailSubscriptionTypeid
        {
            get { return _mailsubscriptiontypeid; }
            set { _mailsubscriptiontypeid = value; }

        }
		

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			UserDefaultSubscription castObj = null;
			try
			{
				castObj = (UserDefaultSubscription)obj;
			} catch(Exception) { return false; } 
			return castObj.GetHashCode() == this.GetHashCode();
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			return this.GetType().FullName.GetHashCode();
				
		}
		#endregion
		
	}
	
}
