﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace BrandSystems.Marcom.Dal.Common.Model
{
    public partial class ThemeDao : BaseDao, ICloneable
    {

        #region Member Variables
        protected int _id;
        protected int _Isactive;

        protected string _name;
        protected string _fileguid;

        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors

        public ThemeDao() { }

        public ThemeDao(string pname, string pfileguid)
        {
            this._name = pname;
            this._fileguid = pfileguid;

        }


        public ThemeDao(int pId, int pIsactive)
        {
            this._id = pId;
            this._Isactive = pIsactive;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual string Name
        {
            get { return _name; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("_name", "Name value, cannot contain more than 250 characters");
                _bIsChanged |= (_name != value);
                _name = value;
            }

        }
        public virtual int Isactive
        {
            get { return _Isactive; }
            set { _bIsChanged |= (_Isactive != value); _Isactive = value; }

        }

        public virtual string Fileguid
        {
            get { return _fileguid; }
            set
            {

                _bIsChanged |= (_fileguid != value);
                _fileguid = value;
            }

        }


        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string ThemeDao = "ThemeDao";
            public const string Id = "Id";
            public const string Isactive = "Isactive";
            public const string Name = "Name";
            public const string FileGuid = "Fileguid";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string ThemeDao = "ThemeDao";
            public const string Id = "Id";
            public const string Isactive = "Isactive";
            public const string Name = "Name";
            public const string FileGuid = "Fileguid";
        }

        #endregion
    }
}
