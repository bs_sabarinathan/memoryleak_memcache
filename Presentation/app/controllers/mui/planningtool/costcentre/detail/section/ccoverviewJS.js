///#source 1 1 /app/controllers/mui/planningtool/costcentre/detail/section/overview-controller.js
(function (ng, app) {
    "use strict";

    function muiplanningtoolcostcentredetailoverviewCtrl($scope, $timeout, $http, $compile, $resource, $cookies, $stateParams, $location, $window, $translate, CcoverviewService, $modal) {
        var cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName + '\\' + TenantFilePath).replace(/\\/g, "\/");
        var imagesrcpath = TenantFilePath;
        if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
            imagesrcpath = cloudpath;
        }
        $scope.GetNonBusinessDaysforDatePicker();
        $scope.Calanderopen = function ($event, model) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calanderopened = true;
            $scope.fields["DatePart_Calander_Open" + model] = true;
        };
        $scope.EntityUserListInCC = [];
        $scope.PersonalUserIdsInCC = [];
        $('#OverAllEntityStats').select2("enable", false)
        var cancelNewsTimerevent;
        $scope.StopeUpdateStatusonPageLoad = false;
        $scope.EntityStatusResult = [];
        $scope.listAttriToAttriResult = [];
        $scope.ShowHideAttributeOnRelation = {};
        $scope.items = [];
        $scope.treeSources = {};
        $scope.treelevels = {};
        $scope.uploader = {};
        $scope.UploaderCaption = {};
        $scope.NormalDropdownCaption = {};
        $scope.NormalMultiDropdownCaption = {};
        $scope.treeTexts = {};
        $scope.treeSelection = [];
        $scope.MilestonePrevStatus = '';
        $scope.normaltreeSources = {};
        $scope.userimg = parseInt($cookies['UserId']);
        $scope.OntimeStatusLists = [{
            Id: 0,
            Name: 'On time',
            BtnBgColor: 'btn-success',
            AbgColor: 'background-image: linear-gradient(to bottom, #62C462, #51A351); color: #fff;',
            SelectedClass: 'btn-success'
        }, {
            Id: 1,
            Name: 'Delayed',
            BtnBgColor: 'btn-warning',
            AbgColor: 'background-image: linear-gradient(to bottom, #FBB450, #F89406); color: #fff;',
            SelectedClass: 'btn-warning'
        }, {
            Id: 2,
            Name: 'On hold',
            BtnBgColor: 'btn-danger',
            AbgColor: 'background-image: linear-gradient(to bottom, #EE5F5B, #BD362F); color: #fff;',
            SelectedClass: 'btn-danger'
        }]
        $scope.Ontime = 'On time';
        var Version = 1;
        $scope.milestoneStatusOptions = [];
        $scope.milestoneStatusOptions = [{
            "Id": 1,
            "Caption": "Reached"
        }, {
            "Id": 0,
            "Caption": "Not Reached"
        }];
        $scope.fiscalyears = [];
        var getEntityNewsFeedResult = "";
        GlistFeedgroup();

        function GlistFeedgroup() {
            CcoverviewService.GetFeedFilter().then(function (GetFeedglistvalue) {
                $scope.FeedgResult = GetFeedglistvalue.Response;
                $scope.Feedglist = [];
                if ($scope.FeedgResult.length > 0) {
                    for (var i = 0; i < $scope.FeedgResult.length; i++) {
                        $scope.Feedglist.push({
                            ID: $scope.FeedgResult[i].Id,
                            Name: $scope.FeedgResult[i].FeedGroup
                        });
                    }
                }
            });
        };

        function LoadDetailTemp(AttrToAttrRelByIDFrEntInOverView, EntAttrDet, CostcentreFinOverview) {
            var ID = $stateParams.ID;
            try {
                $scope.fields = {
                    usersID: 0,
                };
                $scope.tree = {};
                $scope.fieldKeys = [];
                $scope.options = {};
                $scope.setFieldKeys = function () {
                    var keys = [];
                    angular.forEach($scope.fields, function (key) {
                        keys.push(key);
                        $scope.fieldKeys = keys;
                    });
                }
                $scope.ownername = $cookies['Username'];
                $scope.ownerid = $cookies['UserId'];
                $scope.ownerEmail = $cookies['UserEmail'];
                $scope.listAttriToAttriResult = [];
                try {
                    CcoverviewService.GetAttributeToAttributeRelationsByIDForEntityInOverView(0, ID).then(function () {
                        var entityAttrToAttrRelation = AttrToAttrRelByIDFrEntInOverView
                        $scope.listAttriToAttriResult = [];
                        $scope.listAttriToAttriResult = entityAttrToAttrRelation;
                    });
                } catch (ex) { }
                $scope.StopeUpdateStatusonPageLoad = false;
                $scope.dyn_Cont = '';
                var getentityattributesdetails = EntAttrDet;
                $scope.attributedata = getentityattributesdetails;
                $scope.dyn_Cont += '<div class="control-group"><label class="control-label" for="label">' + $translate.instant('LanguageContents.Res_69.Caption') + '</label><div class="controls"><label class="control-label" for="label">' + ID + '</label></div></div>'
                if ($scope.IsLock == undefined) $scope.IsLock = false;
                if ($scope.IsLock == false) $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\"\>' + $translate.instant('LanguageContents.Res_4985.Caption') + '</label><div class=\"controls\"><a xeditable href=\"javascript:;\" ng-click="costcentreentityclick()" attributeTypeID="1" entityid="' + ID + '" attributeTypeID="1" attributeid="68" id=\"ActivityName\" data-ng-model=\"ActivityName"\   my-qtip2 qtip-content=\"' + $translate.instant('LanguageContents.Res_4985.Caption') + '\"  data-type=\"text\" data-original-title=\"CostCentre Name\">' + $scope.DecodedTextName($scope.RootLevelEntityName) + '</a></div></div>';
                else if ($scope.IsLock == true) $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\"\>' + $translate.instant('LanguageContents.Res_4985.Caption') + '</label><div class=\"controls\"><label class="control-label">' + $scope.DecodedTextName($scope.RootLevelEntityName) + '</label></div></div>';
                $scope["showActivityName"] = true;
                $scope["EditActivityName"] = false;
                $scope["showOwner"] = true;
                $scope["EditOwner"] = false;
                for (var i = 0; i < $scope.attributedata.length; i++) {
                    if ($scope.attributedata[i].TypeID == 6) {
                        $scope.dyn_Cont2 = '';
                        var CaptionObj = $scope.attributedata[i].Caption.split(",");
                        for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                            if (j == 0) {
                                if (CaptionObj[j] != undefined) {
                                    $scope.items.push({
                                        caption: $scope.attributedata[i].Lable[j].Label,
                                        level: j + 1
                                    });
                                    $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                    $scope.settreeTexts();
                                    $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                    $scope.setFieldKeys();
                                } else {
                                    $scope.items.push({
                                        caption: $scope.attributedata[i].Lable[j].Label,
                                        level: j + 1
                                    });
                                    $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    $scope.settreeTexts();
                                    $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    $scope.setFieldKeys();
                                }
                            } else {
                                if (CaptionObj[j] != undefined) {
                                    $scope.items.push({
                                        caption: $scope.attributedata[i].Lable[j].Label,
                                        level: j + 1
                                    });
                                    $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                    $scope.settreeTexts();
                                    $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                    $scope.setFieldKeys();
                                } else {
                                    $scope.items.push({
                                        caption: $scope.attributedata[i].Lable[j].Label,
                                        level: j + 1
                                    });
                                    $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    $scope.settreeTexts();
                                    $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    $scope.setFieldKeys();
                                }
                            }
                        }
                        $scope.treelevels["dropdown_levels_" + $scope.attributedata[i].ID] = $scope.items;
                        $scope.settreelevels();
                        $scope.items = [];
                        $scope.settreeSources();
                        $scope.settreeTexts();
                        $scope.settreelevels();
                        $scope.setFieldKeys();
                        for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID + "_" + (j + 1)] = true;
                            var inlineEditabletitile = $scope.treelevels['dropdown_levels_' + $scope.attributedata[i].ID][j].caption;
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><a xeditabletreedropdown href=\"javascript:;\" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" InheritFromParent="' + $scope.attributedata[i].IsInheritFromParent + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '" attributeid="' + $scope.attributedata[i].ID + '" id="fields.DropDown_' + $scope.attributedata[i].ID + "_" + (j + 1) + '" data-ng-model=\"dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" attributename=' + inlineEditabletitile + ' data-type=' + inlineEditabletitile + ' >{{treeTexts.dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</a></div></div>';
                        }
                    } else if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == false) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        var textvalue = "-";
                        if ($scope.attributedata[i].Caption != undefined) textvalue = $scope.attributedata[i].Caption;
                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label">' + textvalue + '</label></div></div>';
                        } else {
                            if ($scope.IsLock == false) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditable href=\"javascript:;\" ng-click="costcentreentityclick()" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"SingleLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"SingleLineText_' + $scope.attributedata[i].ID + '\"  data-type=\"text\"  my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '">' + textvalue + '</a></div></div>';
                            else if ($scope.IsLock == true) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label">' + textvalue + '</label></div></div>';
                        }
                    } else if ($scope.attributedata[i].TypeID == 2) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        var textvalue = "-";
                        if ($scope.attributedata[i].Caption != undefined) textvalue = $scope.attributedata[i].Caption;
                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label">' + textvalue.replace(/\n/g, "<br/>") + '</label></div></div>';
                        } else {
                            if ($scope.IsLock == false) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditable href=\"javascript:;\" ng-click="costcentremultilineentityclick()" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"MultiLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"SingleLineText_' + $scope.attributedata[i].ID + '\" data-type=\"textarea\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '">' + textvalue.replace(/\n/g, "<br/>") + '</a></div></div>';
                            else if ($scope.IsLock == true) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label">' + textvalue.replace(/\n/g, "<br/>") + '</label></div></div>';
                        }
                    } else if ($scope.attributedata[i].TypeID == 11) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        $scope.fields["uploader_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                        $scope.setFieldKeys();
                        $scope.UploaderCaption["uploader_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group ng-scope\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable.toString() + '</label>';
                        $scope.dyn_Cont += '<div class=\"controls\">';
                        if ($scope.attributedata[i].Caption == "" || $scope.attributedata[i].Caption == null && $scope.attributedata[i].Caption == undefined) {
                            $scope.attributedata[i].Caption = $scope.attributedata[i].Lable;
                        }
                        if ($scope.attributedata[i].Value == "" || $scope.attributedata[i].Value == null && $scope.attributedata[i].Value == undefined) {
                            $scope.attributedata[i].Value = "NoThumpnail.jpg";
                        }
                        $scope.dyn_Cont += '<img src="' + imagesrcpath + 'UploadedImages/' + $scope.attributedata[i].Value + '" alt="' + $scope.attributedata[i].Caption + '"';
                        $scope.dyn_Cont += 'class="entityDetailImgPreview" id="UploaderPreview_' + $scope.attributedata[i].ID + '">';
                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '</a></div></div>';
                        } else {
                            if ($scope.IsLock == false) {
                                $scope.dyn_Cont += "<a class='margin-left10x' ng-click='UploadImagefileCC(" + $scope.attributedata[i].ID + ")' attributeTypeID='" + $scope.attributedata[i].TypeID + "'";
                                $scope.dyn_Cont += 'entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="Uploader_' + $scope.attributedata[i].ID + '"';
                                $scope.dyn_Cont += 'my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '"';
                                $scope.dyn_Cont += ' class="ng-pristine ng-valid" attributename=\"' + $scope.UploaderCaption["UploaderCaption_" + $scope.attributedata[i].ID] + '\">Select Image';
                                $scope.dyn_Cont += '</a></div></div>';
                            } else if ($scope.IsLock == true) $scope.dyn_Cont += '</a></div></div>';
                        }
                    } else if ($scope.attributedata[i].TypeID == 3) {
                        if ($scope.attributedata[i].ID == SystemDefiendAttributes.Owner && $scope.attributedata[i].IsSpecial == true) {
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                } else {
                                    if ($scope.IsLock == false) $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    else if ($scope.IsLock == true) $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                }
                            } else {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                    $scope.dyn_Cont += '<div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label>';
                                    $scope.dyn_Cont += '</div></div>';
                                } else {
                                    if ($scope.IsLock == false) {
                                        $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                        $scope.dyn_Cont += '<div class="controls"><a  xeditabledropdown href=\"javascript:;\"';
                                        $scope.dyn_Cont += 'attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '"';
                                        $scope.dyn_Cont += 'attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"';
                                        $scope.dyn_Cont += 'data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '"';
                                        $scope.dyn_Cont += 'attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\"';
                                        $scope.dyn_Cont += 'data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a>';
                                        $scope.dyn_Cont += '</div></div>';
                                    } else if ($scope.IsLock == true) {
                                        $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                        $scope.dyn_Cont += '<div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label>';
                                        $scope.dyn_Cont += '</div></div>';
                                    }
                                }
                            }
                        } else {
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                                if ($scope.attributedata[i].Caption[0].length > 1) {
                                    $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                    $scope.setFieldKeys();
                                    $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                    $scope.setNormalDropdownCaption();
                                    if ($scope.attributedata[i].IsReadOnly == true) {
                                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                    } else {
                                        if ($scope.IsLock == false) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                        else if ($scope.IsLock == true) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                    }
                                } else {
                                    $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                    $scope.setFieldKeys();
                                    $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                    $scope.setNormalDropdownCaption();
                                    if ($scope.attributedata[i].IsReadOnly == true) {
                                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                    } else {
                                        if ($scope.IsLock == false) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                        else if ($scope.IsLock == true) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                    }
                                }
                            } else {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                } else {
                                    if ($scope.IsLock == false) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    else if ($scope.IsLock == true) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                }
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 4) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption.length > 1) {
                                $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                $scope.setFieldKeys();
                                $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalMultiDropdownCaption();
                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                } else {
                                    if ($scope.IsLock == false) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditablemultiselectdropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalMultiDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    else if ($scope.IsLock == true) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                }
                            } else {
                                $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalMultiDropdownCaption();
                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                } else {
                                    if ($scope.IsLock == false) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditablemultiselectdropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalMultiDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    else if ($scope.IsLock == true) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                }
                            }
                        } else {
                            $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = "-";
                            $scope.setFieldKeys();
                            $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalMultiDropdownCaption();
                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                            } else {
                                if ($scope.IsLock == false) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditablemultiselectdropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalMultiDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                else if ($scope.IsLock == true) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 7) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        $scope.fields["Tree_" + $scope.attributedata[i].ID] = [];
                        $scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID] = JSON.parse($scope.attributedata[i].tree).Children;
                        $scope.staticTreesrcdirec["Attr_" + $scope.attributedata[i].ID] = JSON.parse($scope.attributedata[i].tree).Children;
                        GetTreeCheckedNodes($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID], $scope.attributedata[i].ID);
                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class="control-group relative">';
                        $scope.dyn_Cont += '<label class="control-label">' + $scope.attributedata[i].Lable + ' </label>';
                        $scope.dyn_Cont += '<div class="controls">';
                        if ($scope.IsLock == false) {
                            $scope.dyn_Cont += '<div xeditabletree  editabletypeid="treeType_' + $scope.attributedata[i].ID + '" attributename=\"' + $scope.attributedata[i].Lable + '\" isreadonly="' + $scope.attributedata[i].IsReadOnly + '\" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '"  data-type="treeType_' + $scope.attributedata[i].ID + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"' + $scope.attributedata[i].ID + '\" data-ng-model=\"tree_' + $scope.attributedata[i].ID + '"\    data-original-title=\"' + $scope.attributedata[i].Lable + '\">';
                            if ($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID].length > 0) {
                                treeTextVisbileflag = false;
                                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID])) {
                                    $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = true;
                                } else $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
                            } else {
                                $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
                            }
                            $scope.dyn_Cont += '<eu-tree ng-show=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\" tree-data=\"treesrcdirec.Attr_' + $scope.attributedata[i].ID + '\" treeplace="detail" node-attributeid="' + $scope.attributedata[i].ID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                            $scope.dyn_Cont += '<span ng-hide=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\">-<span>';
                            $scope.dyn_Cont += ' </div>';
                        } else {
                            if ($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID].length > 0) {
                                treeTextVisbileflag = false;
                                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID])) {
                                    $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = true;
                                } else $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
                            } else {
                                $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
                            }
                            $scope.dyn_Cont += '<eu-tree ng-show=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\" tree-data=\"treesrcdirec.Attr_' + $scope.attributedata[i].ID + '\"  node-attributeid="' + $scope.attributedata[i].ID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                            $scope.dyn_Cont += '<span ng-hide=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\">-<span>';
                        }
                        $scope.dyn_Cont += '</div></div>';
                    } else if ($scope.attributedata[i].TypeID == 12) {
                        $scope.dyn_Cont2 = '';
                        var CaptionObj = $scope.attributedata[i].Caption;
                        for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                            if ($scope.attributedata[i].Lable.length == 1) {
                                var k = j;
                                var treeTexts = [];
                                var fields = [];
                                $scope.items.push({
                                    caption: $scope.attributedata[i].Lable[j].Label,
                                    level: j + 1
                                });
                                if (k == CaptionObj.length) {
                                    $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                } else {
                                    if (CaptionObj[k] != undefined) {
                                        for (k; k < CaptionObj.length; k++) {
                                            treeTexts.push(CaptionObj[k]);
                                            $scope.settreeTexts();
                                            fields.push(CaptionObj[k]);
                                            $scope.setFieldKeys();
                                        }
                                        $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = treeTexts.join(", ");
                                        $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = fields.join(", ");
                                    } else {
                                        $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    }
                                }
                            } else {
                                if (j == 0) {
                                    if (CaptionObj[j] != undefined) {
                                        $scope.items.push({
                                            caption: $scope.attributedata[i].Lable[j].Label,
                                            level: j + 1
                                        });
                                        $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                        $scope.settreeTexts();
                                        $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                        $scope.setFieldKeys();
                                    } else {
                                        $scope.items.push({
                                            caption: $scope.attributedata[i].Lable[j].Label,
                                            level: j + 1
                                        });
                                        $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        $scope.settreeTexts();
                                        $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        $scope.setFieldKeys();
                                    }
                                } else {
                                    var k = j;
                                    if (j == ($scope.attributedata[i].Lable.length - 1)) {
                                        var treeTexts = [];
                                        var fields = [];
                                        $scope.items.push({
                                            caption: $scope.attributedata[i].Lable[j].Label,
                                            level: j + 1
                                        });
                                        if (k == CaptionObj.length) {
                                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        } else {
                                            if (CaptionObj[k] != undefined) {
                                                for (k; k < CaptionObj.length; k++) {
                                                    treeTexts.push(CaptionObj[k]);
                                                    $scope.settreeTexts();
                                                    fields.push(CaptionObj[k]);
                                                    $scope.setFieldKeys();
                                                }
                                                $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = treeTexts.join(", ");
                                                $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = fields.join(", ");
                                            } else {
                                                $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                                $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            }
                                        }
                                    } else {
                                        if (CaptionObj[j] != undefined) {
                                            $scope.items.push({
                                                caption: $scope.attributedata[i].Lable[j].Label,
                                                level: j + 1
                                            });
                                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                            $scope.settreeTexts();
                                            $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                            $scope.setFieldKeys();
                                        } else {
                                            $scope.items.push({
                                                caption: $scope.attributedata[i].Lable[j].Label,
                                                level: j + 1
                                            });
                                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            $scope.settreeTexts();
                                            $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            $scope.setFieldKeys();
                                        }
                                    }
                                }
                            }
                        }
                        $scope.treelevels["multiselectdropdown_levels_" + $scope.attributedata[i].ID] = $scope.items;
                        $scope.settreelevels();
                        $scope.items = [];
                        $scope.settreeSources();
                        $scope.settreeTexts();
                        $scope.settreelevels();
                        $scope.setFieldKeys();
                        for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID + "_" + (j + 1)] = true;
                            var inlineEditabletitile = $scope.treelevels['multiselectdropdown_levels_' + $scope.attributedata[i].ID][j].caption;
                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><label class="control-label">{{treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</label></div></div>';
                            } else {
                                if ($scope.IsLock == false) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><a xeditablemultiselecttreedropdown href=\"javascript:;\" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" InheritFromParent="' + $scope.attributedata[i].IsInheritFromParent + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '" attributeid="' + $scope.attributedata[i].ID + '" id="fields.MultiSelectDropDown_' + $scope.attributedata[i].ID + "_" + (j + 1) + '" data-ng-model=\"multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\"  attributename=' + inlineEditabletitile + ' data-type=' + inlineEditabletitile + ' >{{treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</a></div></div>';
                                } else if ($scope.IsLock == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><label class="control-label">{{treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</label></div></div>';
                                }
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 5 && $scope.attributedata[i].ID != SystemDefiendAttributes.ApproveTime) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        var datStartUTCval = "";
                        var datstartval = "";
                        var inlineEditabletitile = $scope.attributedata[i].Caption;
                        if ($scope.attributedata[i].Value != null || $scope.attributedata[i].Value != undefined) {
                            datStartUTCval = $scope.attributedata[i].Value.substr(0, 10);
                            datstartval = new Date.create(datStartUTCval);
                            $scope.fields["DateTime_" + $scope.attributedata[i].ID] = dateFormat(datstartval, $scope.GetDefaultSettings.DateFormat);
                            $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = formatteddateFormat(datstartval, "dd/MM/yyyy");
                        } else {
                            $scope.fields["DateTime_" + $scope.attributedata[i].ID] = "-";
                            $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = "-";
                        }
                        if ($scope.attributedata[i].ID != SystemDefiendAttributes.CreationDate) {
                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                            } else {
                                if ($scope.IsLock == false) {
                                    $scope.MinValue = $scope.attributedata[i].MinValue;
                                    $scope.MaxValue = $scope.attributedata[i].MaxValue;
                                    $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID] = new Date.create();
                                    $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID] = new Date.create();
                                    if ($scope.MinValue < 0) {
                                        $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MinValue + 1));
                                    } else {
                                        $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MinValue));
                                    }
                                    if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                        $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MaxValue - 1));
                                    } else {
                                        $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].getDate() + 100000);
                                    }
                                    var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID], $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID]);
                                    $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID] = (temp.MinDate);
                                    $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID] = (temp.MaxDate);
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a xeditabletreedatetime data-primaryid="' + $scope.attributedata[i].ID + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-datetime_id=\"DateTime_' + $scope.attributedata[i].ID + '\" data-ng-model=\"DateTime_' + $scope.attributedata[i].ID + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"datetime' + $scope.attributedata[i].ID + '\" href=\"javascript:;\" attributename="datetime">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    $scope.fields["DatePart_" + $scope.attributedata[i].ID] = null;
                                } else if ($scope.IsLock == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                }
                            }
                        } else {
                            $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                        }
                    } else if ($scope.attributedata[i].TypeID == 16) {
                        var dateExpireUTCval = "";
                        var dateExpireval = "";
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        var inlineEditabletitile = $scope.attributedata[i].Caption;
                        var dateExpire;
                        if ($scope.attributedata[i].Value != null || $scope.attributedata[i].Value != undefined) {
                            var dateExpireUTCval = "";
                            var dateExpireval = "";
                            dateExpireUTCval = $scope.attributedata[i].Value.substr(0, 10);
                            dateExpireval = new Date(dateExpireUTCval);
                            dateExpire = dateFormat(ConvertDateFromStringToString(ConvertDateToString(dateExpireUTCval)).toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3").toString('dd-MM-yyyy'), $scope.GetDefaultSettings.DateFormat);
                            $scope.fields["DateTime_" + $scope.attributedata[i].ID] = dateExpire;
                            $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = dateExpire;
                        } else {
                            dateExpire = "-"
                            $scope.fields["DateTime_" + $scope.attributedata[i].ID] = "-";
                            $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = "-";
                        }
                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">' + dateExpire + '</span></div></div>';
                        } else {
                            if ($scope.IsLock == false) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable"><a data-AttributeID="' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" data-expiredate="' + dateExpire + '" data-attrlable="' + $scope.attributedata[i].Lable + '" ng-click="openExpireAction($event)">' + dateExpire + '</a></span></div></div>';
                            } else if ($scope.IsLock == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable"> ' + dateExpire + '</span></div></div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 17) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption.length > 1) {
                                $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                $scope.fields["TagWordsSeleted_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Value;
                            }
                        } else {
                            $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = "-";
                            $scope.fields["TagWordsSeleted_" + $scope.attributedata[i].ID] = [];
                        }
                        $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                        } else {
                            if ($scope.IsLock == false) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabletagwords href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="TagWordsCaption_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.TagWordsCaption_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                            } else if ($scope.IsLock == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 18) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                        }
                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                    } else if ($scope.attributedata[i].TypeID == 9) {
                        $scope.fields["CheckBoxSelection_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Value;
                        $scope.dyn_Cont += "<div ng-show= ShowHideAttributeOnRelation.Attribute_" + $scope.attributedata[i].ID + "  class=\"control-group\"><label class=\"control-label\" for=\"fields.CheckBoxSelection_ " + $scope.attributedata[i].AttributeID + "\">" + $scope.attributedata[i].Caption + " : </label><div class=\"controls\"><input ng-click=\"saveDropdownTree(" + $scope.attributedata[i].ID + "," + $scope.attributedata[i].TypeID + "," + parseInt($stateParams.ID) + "," + $scope.fields["CheckBoxSelection_" + $scope.attributedata[i].ID] + "  )\"  type=\"checkbox\" ng-model=\"fields.CheckBoxSelection_" + $scope.attributedata[i].ID + "\" id=\"CheckBoxSelection_" + $scope.attributedata[i].ID + "\" placeholder=\"" + $scope.attributedata[i].Caption + "\"></div></div>";
                    }
                }
                $("#dynamicdetail").html($compile($scope.dyn_Cont)($scope));
                $scope.detailsLoader = false;
                $scope.detailsData = true;
                HideAttributeToAttributeRelationsOnPageLoad();
                i = 0;
                for (i = 0; i < $scope.attributedata.length; i++) {
                    if (($scope.attributedata[i].TypeID == 3 || $scope.attributedata[i].TypeID == 4 || $scope.attributedata[i].TypeID == 6 || $scope.attributedata[i].TypeID == 7 || $scope.attributedata[i].TypeID == 12) && $scope.attributedata[i].IsSpecial == false) {
                        if ($scope.attributedata[i].TypeID == 12) {
                            for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                                if ($scope.attributedata[i].Value[j] != undefined && $scope.attributedata[i].Value[j] != null) {
                                    if (($scope.attributedata[i].Lable.length - 1) == j) {
                                        var k = $scope.attributedata[i].Value.length - $scope.attributedata[i].Lable.length;
                                        for (k; k < $scope.attributedata[i].Value.length; k++) {
                                            $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Lable[j].Level, $scope.attributedata[i].Value[k].Nodeid, 12);
                                        }
                                    } else {
                                        $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Lable[j].Level, $scope.attributedata[i].Value[j].Nodeid, 12);
                                    }
                                }
                            }
                        } else if ($scope.attributedata[i].TypeID == 6) {
                            for (var j = 0; j < $scope.attributedata[i].Lable.length - 1; j++) {
                                if ($scope.attributedata[i].Value[j] != undefined && $scope.attributedata[i].Value[j] != null) $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Lable[j].Level, $scope.attributedata[i].Value[j].Nodeid, 6);
                            }
                        } else if ($scope.attributedata[i].TypeID == 4) {
                            $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Level, $scope.attributedata[i].Value, 4);
                        } else if ($scope.attributedata[i].TypeID == 7) {
                            $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Level, $scope.fields["Tree_" + $scope.attributedata[i].ID], 7);
                        } else {
                            $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Level, $scope.attributedata[i].Value, 3);
                        }
                    }
                }
                $scope.CurrentWorkflowStepName = "";
                $scope.workflowstepOptions = [];
                $scope.workflowSummaryList = [];
                $scope.SelectedWorkflowStep = 0;
                $scope.WorkFlowStepID = 0;
                $scope.WorkflowSummaryControls = '';
                try {
                    $scope.UpdateEntityWorkFlowStep = function (stepID) {
                        var TaskStatusData = {};
                        TaskStatusData.EntityID = parseInt($stateParams.ID, 10);
                        TaskStatusData.Status = stepID;
                        CcoverviewService.UpdateEntityActiveStatus(TaskStatusData).then(function (TaskStatusResult) {
                            if (TaskStatusResult.StatusCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_4375.Caption'));
                            } else {
                                NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                                for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                                    for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                                        var EntityId = $scope.ListViewDetails[i].data.Response.Data[j]["Id"];
                                        if (EntityId == parseInt($stateParams.ID, 10)) {
                                            if ($scope.ListViewDetails[i].data.Response.Data[j][SystemDefiendAttributes.Status] != undefined) $scope.ListViewDetails[i].data.Response.Data[j][SystemDefiendAttributes.Status] = $scope.CurrentWorkflowStepName;
                                            else $scope.ListViewDetails[i].data.Response.Data[j]["Status"] = $scope.CurrentWorkflowStepName;
                                            return false;
                                        }
                                    }
                                }
                                $scope.SelectedWorkflowStep = 0;
                            }
                        });
                    }
                } catch (exc) { }
                $scope.openExpireAction = function (event) {
                    var attrevent = event.currentTarget;
                    $scope.SourceAttributeID = attrevent.dataset.attributeid;
                    $scope.SourceAttributename = attrevent.dataset.attrlable;
                    $scope.dateExpireval = attrevent.dataset.expiredate;
                    var sourcefrom = 2;
                    var modalInstance = $modal.open({
                        templateUrl: 'views/mui/expirehandleractions.html',
                        controller: "mui.muiexpireaddActionsCtrl",
                        resolve: {
                            params: function () {
                                return {
                                    dateExpireval: $scope.dateExpireval,
                                    AssetEditID: parseInt($stateParams.ID),
                                    assetEntityId: parseInt($stateParams.ID),
                                    sourcefrom: sourcefrom,
                                    assettypeidfrbtn: $scope.EntityTypeID,
                                    ProductionEntityID: parseInt($stateParams.ID),
                                    SourceAttributeID: $scope.SourceAttributeID,
                                    SourceAttributename: $scope.SourceAttributename
                                };
                            }
                        },
                        scope: $scope,
                        windowClass: 'expirehandlerAddActionsPopup popup-widthL',
                        backdrop: "static"
                    });
                    modalInstance.result.then(function (selectedItem) {
                        $scope.selected = selectedItem;
                    }, function () { });
                }
                $scope.saveDropdownTree = function (attrID, attributetypeid, entityTypeid, optionarray) {
                    if (attrID == 57) {
                        var updateentityattrib = {};
                        updateentityattrib.NewValue = optionarray == "" ? ["0"] : [optionarray];
                        updateentityattrib.AttributetypeID = attributetypeid;
                        updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                        updateentityattrib.AttributeID = attrID;
                        updateentityattrib.Level = 0;
                        CcoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                            if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                            else NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                            UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                        });
                        $scope.treeSelection = [];
                    } else if (attributetypeid == 1) {
                        var updateentityattrib = {};
                        updateentityattrib.NewValue = optionarray == "" ? ["0"] : [optionarray];
                        updateentityattrib.AttributetypeID = attributetypeid;
                        updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                        updateentityattrib.AttributeID = attrID;
                        updateentityattrib.Level = 0;
                        if (attrID == 68) {
                            $(window).trigger("UpdateEntityName", optionarray);
                            $("ul li a[data-entityid='" + $stateParams.ID + "'] span[data-name='text']").text([optionarray]);
                        }
                        CcoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                            if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                            else {
                                $scope.fields['SingleLineTextValue_' + attrID] = optionarray;
                                NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                                UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                                if (attrID == 68) {
                                    $('#breadcrumlink').text(optionarray);
                                }
                            }
                        });
                        $scope.treeSelection = [];
                    } else if (attributetypeid == 3 || attributetypeid == 4 || attributetypeid == 17) {
                        var updateentityattrib = {};
                        updateentityattrib.NewValue = optionarray;
                        updateentityattrib.AttributetypeID = attributetypeid;
                        updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                        updateentityattrib.AttributeID = attrID;
                        updateentityattrib.Level = 0;
                        CcoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                            if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                            else $scope.ShowHideAttributeToAttributeRelations(attrID, 0, 0, attributetypeid, optionarray);
                            NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                            UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                        });
                        $scope.treeSelection = [];
                    } else if (attributetypeid == 6) {
                        var updateentityattrib = {};
                        updateentityattrib.NewValue = optionarray == "" ? ["0"] : [optionarray];
                        updateentityattrib.AttributetypeID = attributetypeid;
                        updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                        updateentityattrib.AttributeID = attrID;
                        updateentityattrib.Level = 0;
                        CcoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                            if (updateentityattribresult.StatusCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                            } else {
                                NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                                for (var i = 0; i < optionarray.length; i++) {
                                    if (optionarray[i] != 0) {
                                        var level = i + 1;
                                        UpdateTreeScope(attrID + "_" + level, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"][data-ng-model="dropdown_text_' + attrID + "_" + level + '"]').text());
                                    }
                                }
                                for (var j = 0; j < optionarray.length; j++) {
                                    if (optionarray[j] != 0) {
                                        $scope.ShowHideAttributeToAttributeRelations(attrID, (j) + 1, optionarray.length, attributetypeid, optionarray[j]);
                                    }
                                }
                            }
                        });
                        $scope.treeSelection = [];
                    } else if (attributetypeid == 12) {
                        var updateentityattrib = {};
                        updateentityattrib.NewValue = optionarray == "" ? ["0"] : [optionarray];
                        updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                        updateentityattrib.AttributeID = attrID;
                        updateentityattrib.Level = 0;
                        updateentityattrib.AttributetypeID = attributetypeid;
                        CcoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                            if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                            else {
                                for (var j = 0; j < optionarray.length; j++) {
                                    if (optionarray[j] != 0) {
                                        $scope.ShowHideAttributeToAttributeRelations(attrID, (j) + 1, optionarray.length, attributetypeid, optionarray[j]);
                                    }
                                }
                                NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                                UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                            }
                        });
                        $scope.treeSelection = [];
                    } else if (attributetypeid == 8) {
                        if (!isNaN(optionarray.toString())) {
                            var updateentityattrib = {};
                            updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                            updateentityattrib.AttributeID = attrID;
                            updateentityattrib.Level = 0;
                            updateentityattrib.NewValue = optionarray == "" ? ["0"] : [optionarray];
                            updateentityattrib.AttributetypeID = attributetypeid;
                            CcoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                                if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                                else {
                                    $scope.fields['SingleLineTextValue_' + attrID] = optionarray == "" ? "0" : optionarray;
                                    NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                                    UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                                }
                            });
                        } else {
                            NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                        }
                        $scope.treeSelection = [];
                    } else if (attributetypeid == 5) {
                        var sdate = new Date($scope.fields['DateTime_Dir_' + attrID].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
                        if (sdate == 'NaN-NaN-NaN') {
                            sdate = ConvertDateToString(ConvertStringToDateByFormat($scope.fields['DateTime_Dir_' + periodid].toString(), GlobalUserDateFormat))
                        }
                        var updateentityattrib = {};
                        updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                        updateentityattrib.AttributeID = attrID;
                        updateentityattrib.Level = 0;
                        updateentityattrib.NewValue = [sdate];
                        updateentityattrib.AttributetypeID = attributetypeid;
                        CcoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                            if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                            else {
                                NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                                UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                                $scope.fields["DateTime_" + attrID] = ConvertDateFromStringToString(ConvertDateToString(sdate));
                            }
                        });
                        $scope.treeSelection = [];
                    } else if (attributetypeid == 9) {
                        var updateentityattrib = {};
                        updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                        updateentityattrib.AttributeID = attrID;
                        updateentityattrib.Level = 0;
                        updateentityattrib.NewValue = ($('#CheckBoxSelection_' + attrID)[0].checked) ? [1] : [0];
                        updateentityattrib.AttributetypeID = attributetypeid;
                        CcoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                            if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                            else {
                                NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                                $scope.fields['CheckBoxSelection_' + attrID] = $('#CheckBoxSelection_' + attrID)[0].checked;
                            }
                        });
                    }
                    $scope.treeSelection = [];
                    $timeout(function () {
                        $scope.TimerForLatestFeed();
                    }, 5000);
                };
                $scope.enabletext = function () {
                    $('#username').editable({
                        placement: 'right',
                        title: 'Select Status'
                    });
                }
                $scope.saveDetailBlock = function (val) {
                    var NewValue = '';
                    if (val == 1) {
                        NewValue = $scope.txtActivityName;
                        $scope["showActivityName"] = true;
                        $scope["EditActivityName"] = false;
                    }
                    if (val == 2) {
                        NewValue = $scope.fields.DropDown_Members.Id;
                        $scope["showOwner"] = true;
                        $scope["EditOwner"] = false;
                    }
                    if (val == 3) {
                        NewValue = $scope.fields.DropDown_FiscalYear.Id;
                        $scope["HideInlineEdit0"] = true;
                        $scope["showinlineedit0"] = false;
                    }
                    var updateentityattrib = {};
                    updateentityattrib.ID = val;
                    updateentityattrib.EntityID = ID;
                    updateentityattrib.NewValue = NewValue;
                    CcoverviewService.SaveDetailBlock(updateentityattrib).then(function (updateentityattrib) {
                        $scope.TimerForLatestFeed();
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                    });
                };
            } catch (e) { }
            $scope.AddNewsFeed = function () {
                if ($("#feedtextholder").text() != "") {
                    return false;
                }
                var addnewsfeed = {};
                addnewsfeed.Actor = parseInt($cookies['UserId'], 10);
                addnewsfeed.TemplateID = 2;
                addnewsfeed.EntityID = parseInt($stateParams.ID);
                addnewsfeed.TypeName = "";
                addnewsfeed.AttributeName = "";
                addnewsfeed.FromValue = "";
                addnewsfeed.ToValue = $('#feedcomment').text();
                addnewsfeed.PersonalUserIds = $scope.PersonalUserIdsInCC;
                CcoverviewService.PostFeed(addnewsfeed).then(function (savenewsfeed) { });
                $scope.PersonalUserIdsInCC = [];
                $('#feedcomment').empty();
                if (document.getElementById('feedcomment').innerHTML.replace(/(<([^>]+)>)/ig, '').replace(/ /g, '').length == 0) {
                    document.getElementById('feedcomment').innerHTML = '<span id=\'feedtextholder\' class=\'placeholder\'>' + $translate.instant('LanguageContents.Res_1202.Caption') + '...</span>';
                }
                $scope.TimerForLatestFeed();
            };
            try {
                $scope.GettingNewsFeedTime = function (serverTime, happendTime) {
                    var appServerTime = serverTime;
                    var feedHappendTime = happendTime;
                    var timeDiff = Math.abs(appServerTime - feedHappendTime);
                }
            } catch (e) { }
            try {
                $scope.financialchartpoints = [];
                var GetFinancialSummaryBlockResult = CostcentreFinOverview;
                var FinancialResult = GetFinancialSummaryBlockResult;
                $scope.financialchartpoints = [{
                    label: $translate.instant('LanguageContents.Res_4971.Caption'),
                    y: parseInt((FinancialResult['AvailabletoSpent'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                }, {
                    label: $translate.instant('LanguageContents.Res_1725.Caption'),
                    y: parseInt((FinancialResult['Spent'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                }, {
                    label: $translate.instant('LanguageContents.Res_5531.Caption'),
                    y: parseInt((FinancialResult['Committed'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                }, {
                    label: $translate.instant('LanguageContents.Res_4970.Caption'),
                    y: parseInt((FinancialResult['UnAllocatedAmount'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                }, {
                    label: $translate.instant('LanguageContents.Res_1740.Caption'),
                    y: parseInt((FinancialResult['ApprovedAllocation'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                }, {
                    label: $translate.instant('LanguageContents.Res_4968.Caption'),
                    y: parseInt((FinancialResult['BudgetDeviation'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                }, {
                    label: $translate.instant('LanguageContents.Res_4967.Caption'),
                    y: parseInt((FinancialResult['ApprovedBudget'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                }, {
                    label: $translate.instant('LanguageContents.Res_1703.Caption'),
                    y: parseInt((FinancialResult['InRequests'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                }, {
                    label: $translate.instant('LanguageContents.Res_1744.Caption'),
                    y: parseInt((FinancialResult['PlannedAmount'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                }, {
                    label: $translate.instant('LanguageContents.Res_5532.Caption'),
                    y: parseInt((FinancialResult['TotalAssignedAmount'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                }];
                CanvasJS.addColorSet("finColorSet1", ["#BDF2A1", "#9ECF6E", "#DDDD69", "#4DA3CE", "#B08BEB", "#3EA0DD", "#F5A52A", "#23BFAA", "#FAA586", "#EB8CC6"]);
                var chart = new CanvasJS.Chart("financialchart", {
                    animationEnabled: true,
                    colorSet: "finColorSet1",
                    axisX: {
                        tickColor: "white",
                        tickLength: 5,
                        labelFontFamily: "sans-serif",
                        labelFontColor: "#7F7F7F",
                        labelFontSize: 10,
                        labelFontWeight: "normal",
                    },
                    axisY: {
                        tickLength: 0,
                        labelFontSize: 0,
                        lineThickness: 0,
                        gridThickness: 0,
                    },
                    data: [{
                        type: "stackedBar",
                        indexLabel: "{y}",
                        indexLabelFontSize: 12,
                        indexLabelFontWeight: "bold",
                        indexLabelPlacement: "outside",
                        indexLabelOrientation: "horizontal",
                        legendMarkerColor: "gray",
                        dataPoints: $scope.financialchartpoints
                    }]
                });
                chart.render();
            } catch (e) { }
        };
        $scope.FeedgroupFilterByOptionChange = function () {
            if ($scope.Newsfilter.FeedgroupMulitipleFilterStatus.length == 0 || $scope.Newsfilter.FeedgroupMulitipleFilterStatus == undefined) {
                $scope.Newsfilter.FeedgroupMulitipleFilterStatus = [];
                $scope.Newsfilter.Feedgrouplistvalues = '-1';
                $scope.LoadNewsFeedBlock();
            } else {
                $scope.Newsfilter.FeedgroupMulitipleFilterStatus = $scope.Newsfilter.FeedgroupMulitipleFilterStatus;
                $scope.Newsfilter.Feedgrouplistvalues = '';
                $scope.Newsfilter.Feedgrouplistvalues = $scope.Newsfilter.FeedgroupMulitipleFilterStatus.join(',');
                $scope.LoadNewsFeedBlock();
            }
        }
        $scope.treeSourcesObj = [];
        $scope.settreeSources = function () {
            var keys = [];
            angular.forEach($scope.treeSources, function (key) {
                keys.push(key);
                $scope.treeSourcesObj = keys;
            });
        }
        $scope.treelevelsObj = [];
        $scope.settreelevels = function () {
            var keys1 = [];
            angular.forEach($scope.treelevels, function (key) {
                keys1.push(key);
                $scope.treelevelsObj = keys1;
            });
        }
        $scope.NormalDropdownCaptionObj = [];
        $scope.setNormalDropdownCaption = function () {
            var keys1 = [];
            angular.forEach($scope.NormalDropdownCaption, function (key) {
                keys1.push(key);
                $scope.NormalDropdownCaptionObj = keys1;
            });
        }
        $scope.NormalMultiDropdownCaptionObj = [];
        $scope.setNormalMultiDropdownCaption = function () {
            var keys1 = [];
            angular.forEach($scope.NormalMultiDropdownCaption, function (key) {
                keys1.push(key);
                $scope.NormalMultiDropdownCaptionObj = keys1;
            });
        }
        $scope.treeTextsObj = [];
        $scope.settreeTexts = function () {
            var keys2 = [];
            angular.forEach($scope.treeTexts, function (key) {
                keys2.push(key);
                $scope.treeTextsObj = keys2;
            });
        }
        $scope.fields = {
            usersID: ''
        };
        $scope.fieldKeys = [];
        $scope.setFieldKeys = function () {
            var keys = [];
            angular.forEach($scope.fields, function (key) {
                keys.push(key);
                $scope.fieldKeys = keys;
            });
        }
        app.value('$strapConfig', {
            datepicker: {
                language: 'fr',
                format: 'M d, yyyy'
            }
        });
        $scope.tempresultHolder = [];
        $scope.GetEntityMembers = function () {
            $scope.PersonalUserIdsInCC = [];
            $scope.EntityUserListInCC = [];
            CcoverviewService.GetMember($stateParams.ID).then(function (member) {
                var IsUnique = true;
                if (member.Response != null) {
                    $.each(member.Response, function (index, value) {
                        if (value.IsInherited == false) {
                            IsUnique = true;
                            if ($scope.EntityUserListInCC.length > 0) {
                                $.each($scope.EntityUserListInCC, function (i, v) {
                                    if (v.Userid == value.Userid) {
                                        IsUnique = false;
                                    }
                                });
                                if (IsUnique == true) $scope.EntityUserListInCC.push(value);
                            } else $scope.EntityUserListInCC.push(value);
                        }
                    });
                }
            });
        }
        $scope.LoadCostcenterRelatedData = function () {
            CcoverviewService.GetCostCenterRelatedDataOnLoad(parseInt($stateParams.ID), parseInt($cookies['UserId']), 0, false).then(function (CostcenterRelatedDataResult) {
                if (CostcenterRelatedDataResult.StatusCode != 405) {
                    if (CostcenterRelatedDataResult.Response != null) {
                        $scope.tempresultHolder = CostcenterRelatedDataResult.Response;
                        funloadEntityStatusForCCEdit($scope.tempresultHolder[0], $scope.tempresultHolder[1]);
                        LoadTaskSummaryDetlEdit($scope.tempresultHolder[2]);
                        $scope.LoadCostcenterRelatedDataSet2();
                    }
                }
            });
        }
        $scope.LoadCostcenterRelatedData();
        $scope.tempresultHolderSet2 = [];
        $scope.LoadCostcenterRelatedDataSet2 = function () {
            CcoverviewService.GetCostCenterRelatedDataOnLoad_Set2(parseInt($stateParams.ID), parseInt($cookies['UserId']), 0, false).then(function (CostcenterRelatedDataResultSet2) {
                if (CostcenterRelatedDataResultSet2.StatusCode != 405) {
                    if (CostcenterRelatedDataResultSet2.Response != null) {
                        $scope.tempresultHolderSet2 = CostcenterRelatedDataResultSet2.Response;
                        LoadDetailTemp($scope.tempresultHolderSet2[0], $scope.tempresultHolderSet2[1], $scope.tempresultHolderSet2[2]);
                    }
                }
            });
        }
        $scope.LoadNewsFeedBlock = function () {
            var ID = $stateParams.ID;
            var PagenoforScroll = 0;
            $scope.FeedHtml = '';
            $scope.entityNewsFeeds = {};
            $scope.userimgsrc = '';
            $scope.commentuserimgsrc = '';
            try {
                $('#EntityFeedsdiv').unbind('scroll');
                $('#EntityFeedsdiv').scroll(function () {
                    if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                        PagenoforScroll += 2;
                        CcoverviewService.GetEnityFeeds(ID, PagenoforScroll, $scope.Newsfilter.Feedgrouplistvalues).then(function (getEntityNewsFeedResultForScroll) {
                            var feeddivHtml = '';
                            for (var i = 0; i < getEntityNewsFeedResultForScroll.Response.length; i++) {
                                var feedcomCount = getEntityNewsFeedResultForScroll.Response[i].FeedComment != null ? getEntityNewsFeedResultForScroll.Response[i].FeedComment.length : 0;
                                feeddivHtml = '';
                                if (getEntityNewsFeedResultForScroll.Response[i].Actor == parseInt($cookies['UserId'])) {
                                    $scope.userimgsrc = $scope.NewUserImgSrc;
                                } else {
                                    $scope.userimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResultForScroll.Response[i].Actor;
                                }
                                feeddivHtml = feeddivHtml + '<li data-parent="NewsParent"  data-ID=' + getEntityNewsFeedResultForScroll.Response[i].FeedId + '>';
                                feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                                feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.userimgsrc + ' alt="Avatar"></div>';
                                feeddivHtml = feeddivHtml + '<div class="cmnt" data-parent="parentforcomment">';
                                feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResultForScroll.Response[i].UserEmail + '>' + getEntityNewsFeedResultForScroll.Response[i].UserName + '</a></h5></div>';
                                feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResultForScroll.Response[i].FeedText + '</p></div>';
                                feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResultForScroll.Response[i].FeedHappendTime + '</span>';
                                feeddivHtml = feeddivHtml + '<span class="cmntActions" ><a  data-DynHTML="CommentHTML"  data-commnetid="' + getEntityNewsFeedResultForScroll.Response[i].FeedId + '" id="OverviewComment_' + getEntityNewsFeedResultForScroll.Response[i].FeedId + '" >' + $translate.instant('LanguageContents.Res_5051.Caption') + '</a></span>';
                                if (feedcomCount > 0) {
                                    feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResult.Response[i].FeedId + '" > ' + $translate.instant('LanguageContents.Res_4896.Caption') + ' ' + (feedcomCount - 1) + +$translate.instant('LanguageContents.Res_5054.Caption') + '</a></span></div></div></div>';
                                } else {
                                    feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResult.Response[i].FeedId + '" ></a></span></div></div></div>';
                                }
                                feeddivHtml = feeddivHtml + '<ul class="subComment">';
                                if (getEntityNewsFeedResultForScroll.Response[i].FeedComment != '' && getEntityNewsFeedResultForScroll.Response[i].FeedComment != null) {
                                    var j = 0;
                                    if (getEntityNewsFeedResultForScroll.Response[i].FeedComment[j].Actor == parseInt($cookies['UserId'])) {
                                        $scope.commentuserimgsrc = $scope.NewUserImgSrc;
                                    } else {
                                        $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResultForScroll.Response[i].FeedComment[j].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                    }
                                    for (var k = 0; k < getEntityNewsFeedResultForScroll.Response[i].FeedComment.length; k++) {
                                        $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                        if (k == 0) {
                                            feeddivHtml = feeddivHtml + '<li';
                                        } else {
                                            feeddivHtml = feeddivHtml + '<li style = "display:none;"';
                                        }
                                        feeddivHtml = feeddivHtml + ' id="feedcomment_' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].Id + '"">';
                                        feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                                        feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.commentuserimgsrc + ' alt="Avatar"></div>';
                                        feeddivHtml = feeddivHtml + '<div class="cmnt">';
                                        feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].UserEmail + '>' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].UserName + '</a></h5></div>';
                                        feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].Comment + '</p></div>';
                                        feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].CommentedOn + '</span></div>';
                                        feeddivHtml = feeddivHtml + '</div></div></li>';
                                    }
                                }
                                feeddivHtml = feeddivHtml + '</ul></li>';
                                $('#EntityFeedsdiv').append(feeddivHtml);
                            }
                        });
                    }
                });
            } catch (e) { }
            try {
                $('#EntityFeedsdiv').html('');
                CcoverviewService.GetEnityFeeds(ID, 0, $scope.Newsfilter.Feedgrouplistvalues).then(function (getEntityNewsFeedRes) {
                    getEntityNewsFeedResult = getEntityNewsFeedRes;
                    var feeddivHtml = '';
                    $('#EntityFeedsdiv').html('');
                    if (getEntityNewsFeedResult.Response != null) {
                        for (var i = 0; i < getEntityNewsFeedResult.Response.length; i++) {
                            var feedcomCount = getEntityNewsFeedResult.Response[i].FeedComment != null ? getEntityNewsFeedResult.Response[i].FeedComment.length : 0;
                            feeddivHtml = '';
                            if (getEntityNewsFeedResult.Response[i].Actor == parseInt($cookies['UserId'])) {
                                $scope.userimgsrc = $scope.NewUserImgSrc;
                            } else {
                                $scope.userimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].Actor;
                            }
                            feeddivHtml = feeddivHtml + '<li data-parent="NewsParent" data-ID=' + getEntityNewsFeedResult.Response[i].FeedId + '>';
                            feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                            feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.userimgsrc + ' alt="Avatar"></div>';
                            feeddivHtml = feeddivHtml + '<div class="cmnt" data-parent="parentforcomment">';
                            feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResult.Response[i].UserEmail + '>' + getEntityNewsFeedResult.Response[i].UserName + '</a></h5></div>';
                            feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResult.Response[i].FeedText + '</p></div>';
                            feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResult.Response[i].FeedHappendTime + '</span>';
                            feeddivHtml = feeddivHtml + '<span class="cmntActions" ><a  data-DynHTML="CommentHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedId + '" id="OverviewComment_' + getEntityNewsFeedResult.Response[i].FeedId + '" >' + $translate.instant('LanguageContents.Res_5051.Caption') + '</a></span>';
                            if (feedcomCount > 0) {
                                feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResult.Response[i].FeedId + '" >' + $translate.instant('LanguageContents.Res_4896.Caption') + ' ' + (feedcomCount - 1) + $translate.instant('LanguageContents.Res_5054.Caption') + '</a></span></div></div></div>';
                            } else {
                                feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResult.Response[i].FeedId + '" ></a></span></div></div></div>';
                            }
                            feeddivHtml = feeddivHtml + '<ul class="subComment">';
                            if (getEntityNewsFeedResult.Response[i].FeedComment != '' && getEntityNewsFeedResult.Response[i].FeedComment != null) {
                                var j = 0;
                                if (getEntityNewsFeedResult.Response[i].FeedComment[j].Actor == parseInt($cookies['UserId'])) {
                                    $scope.commentuserimgsrc = $scope.NewUserImgSrc;
                                } else {
                                    $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].FeedComment[j].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                }
                                for (var k = 0; k < getEntityNewsFeedResult.Response[i].FeedComment.length; k++) {
                                    $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].FeedComment[k].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                    if (k == 0) {
                                        feeddivHtml = feeddivHtml + '<li';
                                    } else {
                                        feeddivHtml = feeddivHtml + '<li style = "display:none;"';
                                    }
                                    feeddivHtml = feeddivHtml + ' id="feedcomment_' + getEntityNewsFeedResult.Response[i].FeedComment[k].Id + '"">';
                                    feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                                    feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.commentuserimgsrc + ' alt="Avatar"></div>';
                                    feeddivHtml = feeddivHtml + '<div class="cmnt">';
                                    feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResult.Response[i].FeedComment[k].UserEmail + '>' + getEntityNewsFeedResult.Response[i].FeedComment[k].UserName + '</a></h5></div>';
                                    feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResult.Response[i].FeedComment[k].Comment + '</p></div>';
                                    feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResult.Response[i].FeedComment[k].CommentedOn + '</span></div>';
                                    feeddivHtml = feeddivHtml + '</div></div></li>';
                                }
                            }
                            feeddivHtml = feeddivHtml + '</ul></li>';
                            $('#EntityFeedsdiv').append(feeddivHtml);
                        }
                    }
                    NewsFeedUniqueTimer = $timeout(function () {
                        $scope.TimerForLatestFeed();
                    }, 30000);
                });
            } catch (e) { }
            $scope.TimerForLastestFeedCallBack = function () {
                $scope.TimerForLatestFeed();
                NewsFeedUniqueTimer = $timeout(function () {
                    $scope.TimerForLastestFeedCallBack();
                }, 30000);
            }
            try {
                $scope.TimerForLatestFeed = function () {
                    $scope.FeedHtml = '';
                    $scope.entityNewsFeeds = {};
                    CcoverviewService.GetLastEntityFeeds(ID, $scope.Newsfilter.Feedgrouplistvalues).then(function (getEntityNewsFeedResult1) {
                        var feeddivHtml = '';
                        if (getEntityNewsFeedResult1.Response != null) {
                            for (var i = 0; i < getEntityNewsFeedResult1.Response.length; i++) {
                                var feedcomCount = getEntityNewsFeedResult1.Response[i].FeedComment != null ? getEntityNewsFeedResult1.Response[i].FeedComment.length : 0;
                                feeddivHtml = '';
                                if (getEntityNewsFeedResult1.Response[i].Actor == parseInt($cookies['UserId'])) {
                                    $scope.userimgsrc = $scope.NewUserImgSrc;
                                } else {
                                    $scope.userimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult1.Response[i].Actor;
                                }
                                feeddivHtml = feeddivHtml + '<li data-parent="NewsParent" data-ID=' + getEntityNewsFeedResult1.Response[i].FeedId + '>';
                                feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                                feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.userimgsrc + ' alt="Avatar"></div>';
                                feeddivHtml = feeddivHtml + '<div class="cmnt" data-parent="parentforcomment">';
                                feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResult1.Response[i].UserEmail + '>' + getEntityNewsFeedResult1.Response[i].UserName + '</a></h5></div>';
                                feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResult1.Response[i].FeedText + '</p></div>';
                                feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResult1.Response[i].FeedHappendTime + '</span>';
                                feeddivHtml = feeddivHtml + '<span class="cmntActions" ><a  data-DynHTML="CommentHTML"  data-commnetid="' + getEntityNewsFeedResult1.Response[i].FeedId + '" id="OverviewComment_' + getEntityNewsFeedResult1.Response[i].FeedId + '" >' + $translate.instant('LanguageContents.Res_5051.Caption') + '</a></span>';
                                if (feedcomCount > 0) {
                                    feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResult1.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResult1.Response[i].FeedId + '" > ' + $translate.instant('LanguageContents.Res_4896.Caption') + ' ' + (feedcomCount - 1) + $translate.instant('LanguageContents.Res_5054.Caption') + '</a></span></div></div></div>';
                                } else {
                                    feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResult1.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResult1.Response[i].FeedId + '" ></a></span></div></div></div>';
                                }
                                feeddivHtml = feeddivHtml + '<ul class="subComment"';
                                if (getEntityNewsFeedResult1.Response[i].FeedComment != '' && getEntityNewsFeedResult1.Response[i].FeedComment != null) {
                                    var j = 0;
                                    if (getEntityNewsFeedResult1.Response[i].FeedComment[j].Actor == parseInt($cookies['UserId'])) {
                                        $scope.commentuserimgsrc = $scope.NewUserImgSrc;
                                    } else {
                                        $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult1.Response[i].FeedComment[j].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                    }
                                    for (var k = 0; k < getEntityNewsFeedResult1.Response[i].FeedComment.length; k++) {
                                        $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult1.Response[i].FeedComment[k].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                        if (k == 0) {
                                            feeddivHtml = feeddivHtml + '<li';
                                        } else {
                                            feeddivHtml = feeddivHtml + '<li style = "display:none;"';
                                        }
                                        feeddivHtml = feeddivHtml + ' id="feedcomment_' + getEntityNewsFeedResult1.Response[i].FeedComment[k].Id + '"">';
                                        feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                                        feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.commentuserimgsrc + ' alt="Avatar"></div>';
                                        feeddivHtml = feeddivHtml + '<div class="cmnt">';
                                        feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResult1.Response[i].FeedComment[k].UserEmail + '>' + getEntityNewsFeedResult1.Response[i].FeedComment[k].UserName + '</a></h5></div>';
                                        feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResult1.Response[i].FeedComment[k].Comment + '</p></div>';
                                        feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResult1.Response[i].FeedComment[k].CommentedOn + '</span></div>';
                                        feeddivHtml = feeddivHtml + '</div></div></li>';
                                    }
                                }
                                feeddivHtml = feeddivHtml + '</ul></li>';
                                $('#EntityFeedsdiv [data-id]').each(function () {
                                    if (parseInt($(this).attr('data-id')) == getEntityNewsFeedResult1.Response[i].FeedId) {
                                        $(this).remove();
                                    }
                                });
                                $('#EntityFeedsdiv').prepend(feeddivHtml);
                            }
                        }
                    });
                }
            } catch (e) { }
            $('#EntityFeedsdiv').on('click', 'a[data-DynHTML="CommentshowHTML"]', function (event) {
                event.stopImmediatePropagation();
                var feedid1 = event.target.attributes["id"].nodeValue;
                var feedid = feedid1.substring(13);
                var feedfilter = $.grep(getEntityNewsFeedResult.Response, function (e) {
                    return e.FeedId == parseInt(feedid);
                });
                $("#OverviewCostfeed_" + feedid).next('div').hide();
                $("#OverviewCostfeed_" + feedid).hide();
                for (var i = 0; i < feedfilter[0].FeedComment.length; i++) {
                    if ($('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '')[0].style.display != "none") {
                        $(this)[0].innerHTML = $translate.instant('LanguageContents.Res_4896.Caption') + (feedfilter[0].FeedComment.length - 1) + $translate.instant('LanguageContents.Res_5054.Caption');
                        if (i != 0) $('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideUp(1000, function () { });
                        else $('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { });
                    } else {
                        $('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { });
                        $(this)[0].innerHTML = $translate.instant('LanguageContents.Res_4960.Caption');
                    }
                }
            });
            $('#EntityFeedsdiv').on('click', 'a[data-DynHTML="CommentHTML"]', function (event) {
                var commentuniqueid = this.id;
                event.stopImmediatePropagation();
                var imgpath = "Handlers/UserImage.ashx?id=" + parseInt($cookies['UserId']) + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                $(this).hide();
                var feeddivHtml = '';
                feeddivHtml = feeddivHtml + '<li class="writeNewComment">';
                feeddivHtml = feeddivHtml + '<div class="newComment"><div class="userAvatar"><img data-role="user-avatar" src=\'Handlers/UserImage.ashx?id=' + parseInt($cookies['UserId']) + '&time=' + $scope.DefaultImageSettings.ImageSpan + '\'></div>';
                feeddivHtml = feeddivHtml + '<div class="textarea-wrapper" data-role="textarea"><div contenteditable="true" tabindex="0" class="textarea" id="feedcomment_' + commentuniqueid + '"></div></div>';
                feeddivHtml = feeddivHtml + '<button type="submit" class="btn btn-primary" data-dynCommentBtn="ButtonHTML" >' + $translate.instant('LanguageContents.Res_5051.Caption') + '</button></div></li>';
                var currentobj = $(this).parents('li[data-parent="NewsParent"]').find('ul');
                if ($(this).parents('li[data-parent="NewsParent"]').find('ul').children().length > 0) $(this).parents('li[data-parent="NewsParent"]').find('ul li:first').before(feeddivHtml);
                else $(this).parents('li[data-parent="NewsParent"]').find('ul').html(feeddivHtml);
                $timeout(function () {
                    $('#feedcomment_' + commentuniqueid).html('').focus();
                }, 10);
            });
            $('#EntityFeedsdiv').on('click', 'button[data-dyncommentbtn="ButtonHTML"]', function (event) {
                event.stopImmediatePropagation();
                if ($(this).prev().eq(0).text().toString().trim().length > 0) {
                    var addfeedcomment = {};
                    var FeedidforComment = $(this).parents("li:eq(1)").attr('data-id');
                    var myDate = new Date();
                    addfeedcomment.FeedID = $(this).parents("li:eq(1)").attr('data-id');
                    addfeedcomment.Actor = parseInt($cookies['UserId']);
                    addfeedcomment.Comment = $(this).prev().eq(0).text();
                    var d = new Date();
                    var month = d.getMonth() + 1;
                    var day = d.getDate();
                    var hrs = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                    var output = d.getFullYear() + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day + ' ' + hrs;
                    var _this = $(this);
                    CcoverviewService.InsertFeedComment(addfeedcomment).then(function (saveusercomment) {
                        var feeddivHtml = '';
                        feeddivHtml = feeddivHtml + '<li>';
                        feeddivHtml = feeddivHtml + '<div class=\"newsFeed\">';
                        feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=\'Handlers/UserImage.ashx?id=' + parseInt($cookies['UserId']) + '&time=' + $scope.DefaultImageSettings.ImageSpan + '\' alt="Avatar"></div>';
                        feeddivHtml = feeddivHtml + '<div class="cmnt">';
                        feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5> <a href="undefined">' + $cookies['Username'] + '</a></h5></div>';
                        feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + saveusercomment.Response + '</p></div>';
                        feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + $translate.instant('LanguageContents.Res_5055.Caption') + '</span></div>';
                        feeddivHtml = feeddivHtml + '</div></div></div></li>';
                        $("#EntityFeedsdiv").find('li[data-id=' + FeedidforComment + ']').first().find('li:first').before(feeddivHtml)
                        $(".writeNewComment").remove();
                        $('#OverviewComment_' + addfeedcomment.FeedID).show();
                    });
                }
            });
        }
        $scope.LoadNewsFeedBlock();
        $scope.$on('LoadCostcentreSectionDetail', function (event, data) {
            $('#OverAllEntityStats').select2("enable", false)
            if (NewsFeedUniqueTimer != undefined) {
                $timeout.cancel(NewsFeedUniqueTimer);
            }
            EnableDisableLoader();
            $scope.GetEntityMembers();
            $scope.LoadNewsFeedBlock();
            $scope.EntityStatusResult = [];
            $scope.DynamicTaskListDescriptionObj = '-';
            $scope.LoadCostcenterRelatedData();
            $scope.StatusAttributeData = [];
            $scope.LoadStatusblock = false;
            $timeout(function () { OverviewStatusAttribute(); }, 50);
        });
        $scope.LoadFinancialSummanyBlock = function (CostCentreID) {
            try {
                $scope.financialchartpoints = [];
                CcoverviewService.GettingCostcentreFinancialOverview(CostCentreID).then(function (GetFinancialSummaryBlockResult) {
                    var FinancialResult = GetFinancialSummaryBlockResult.Response;
                    $scope.financialchartpoints = [{
                        label: $translate.instant('LanguageContents.Res_4971.Caption'),
                        y: parseInt((FinancialResult['AvailabletoSpent'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                    }, {
                        label: $translate.instant('LanguageContents.Res_1725.Caption'),
                        y: parseInt((FinancialResult['Spent'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                    }, {
                        label: $translate.instant('LanguageContents.Res_5531.Caption'),
                        y: parseInt((FinancialResult['Committed'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                    }, {
                        label: $translate.instant('LanguageContents.Res_4970.Caption'),
                        y: parseInt((FinancialResult['UnAllocatedAmount'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                    }, {
                        label: $translate.instant('LanguageContents.Res_1740.Caption'),
                        y: parseInt((FinancialResult['ApprovedAllocation'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                    }, {
                        label: $translate.instant('LanguageContents.Res_4968.Caption'),
                        y: parseInt((FinancialResult['BudgetDeviation'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                    }, {
                        label: $translate.instant('LanguageContents.Res_4967.Caption'),
                        y: parseInt((FinancialResult['ApprovedBudget'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                    }, {
                        label: $translate.instant('LanguageContents.Res_1703.Caption'),
                        y: parseInt((FinancialResult['InRequests'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                    }, {
                        label: $translate.instant('LanguageContents.Res_1744.Caption'),
                        y: parseInt((FinancialResult['PlannedAmount'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                    }, {
                        label: $translate.instant('LanguageContents.Res_5531.Caption'),
                        y: parseInt((FinancialResult['TotalAssignedAmount'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                    }];
                    CanvasJS.addColorSet("finColorSet1", ["#BDF2A1", "#9ECF6E", "#DDDD69", "#4DA3CE", "#B08BEB", "#3EA0DD", "#F5A52A", "#23BFAA", "#FAA586", "#EB8CC6"]);
                    var chart = new CanvasJS.Chart("financialchart", {
                        animationEnabled: true,
                        colorSet: "finColorSet1",
                        axisX: {
                            tickColor: "white",
                            tickLength: 5,
                            labelFontFamily: "sans-serif",
                            labelFontColor: "#7F7F7F",
                            labelFontSize: 10,
                            labelFontWeight: "normal",
                        },
                        axisY: {
                            tickLength: 0,
                            labelFontSize: 0,
                            lineThickness: 0,
                            gridThickness: 0,
                        },
                        data: [{
                            type: "stackedBar",
                            indexLabel: "{y}",
                            indexLabelFontSize: 12,
                            indexLabelFontWeight: "bold",
                            indexLabelPlacement: "outside",
                            indexLabelOrientation: "horizontal",
                            legendMarkerColor: "gray",
                            dataPoints: $scope.financialchartpoints
                        }]
                    });
                    chart.render();
                });
            } catch (e) { }
        }

        function LoadDetail() {
            var ID = $stateParams.ID;
            try {
                $scope.fields = {
                    usersID: 0,
                };
                $scope.tree = {};
                $scope.fieldKeys = [];
                $scope.options = {};
                $scope.setFieldKeys = function () {
                    var keys = [];
                    angular.forEach($scope.fields, function (key) {
                        keys.push(key);
                        $scope.fieldKeys = keys;
                    });
                }
                $scope.ownername = $cookies['Username'];
                $scope.ownerid = $cookies['UserId'];
                $scope.ownerEmail = $cookies['UserEmail'];
                $scope.listAttriToAttriResult = [];
                try {
                    CcoverviewService.GetAttributeToAttributeRelationsByIDForEntityInOverView(0, ID).then(function (entityAttrToAttrRelation) {
                        $scope.listAttriToAttriResult = [];
                        $scope.listAttriToAttriResult = entityAttrToAttrRelation.Response;
                    });
                } catch (ex) { }
                $scope.StopeUpdateStatusonPageLoad = false;
                $scope.dyn_Cont = '';
                CcoverviewService.GetEntityAttributesDetails(ID).then(function (getentityattributesdetails) {
                    $scope.attributedata = getentityattributesdetails.Response;
                    $scope.dyn_Cont += '<div class="control-group"><label class="control-label" for="label">' + $translate.instant('LanguageContents.Res_69.Caption') + '</label><div class="controls"><label class="control-label" for="label">' + ID + '</label></div></div>'
                    if ($scope.IsLock == undefined) $scope.IsLock = false;
                    if ($scope.IsLock == false) $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\"\>' + $translate.instant('LanguageContents.Res_4985.Caption') + '</label><div class=\"controls\"><a xeditable href=\"javascript:;\" ng-click="costcentreentityclick()" attributeTypeID="1" entityid="' + ID + '" attributeTypeID="1" attributeid="68" id=\"ActivityName\" data-ng-model=\"ActivityName"\   my-qtip2 qtip-content=\"' + $translate.instant('LanguageContents.Res_4985.Caption') + '\"  data-type=\"text\" data-original-title=\"CostCentre Name\">' + $scope.DecodedTextName($scope.RootLevelEntityName) + '</a></div></div>';
                    else if ($scope.IsLock == true) $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\"\>' + $translate.instant('LanguageContents.Res_4985.Caption') + '</label><div class=\"controls\"><label class="control-label">' + $scope.DecodedTextName(getentityattributesdetails.Response[0].Value) + '</label></div></div>';
                    $scope["showActivityName"] = true;
                    $scope["EditActivityName"] = false;
                    $scope["showOwner"] = true;
                    $scope["EditOwner"] = false;
                    for (var i = 0; i < $scope.attributedata.length; i++) {
                        if ($scope.attributedata[i].TypeID == 6) {
                            $scope.dyn_Cont2 = '';
                            var CaptionObj = $scope.attributedata[i].Caption.split(",");
                            for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                                if (j == 0) {
                                    if (CaptionObj[j] != undefined) {
                                        $scope.items.push({
                                            caption: $scope.attributedata[i].Lable[j].Label,
                                            level: j + 1
                                        });
                                        $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                        $scope.settreeTexts();
                                        $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                        $scope.setFieldKeys();
                                    } else {
                                        $scope.items.push({
                                            caption: $scope.attributedata[i].Lable[j].Label,
                                            level: j + 1
                                        });
                                        $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        $scope.settreeTexts();
                                        $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        $scope.setFieldKeys();
                                    }
                                } else {
                                    if (CaptionObj[j] != undefined) {
                                        $scope.items.push({
                                            caption: $scope.attributedata[i].Lable[j].Label,
                                            level: j + 1
                                        });
                                        $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                        $scope.settreeTexts();
                                        $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                        $scope.setFieldKeys();
                                    } else {
                                        $scope.items.push({
                                            caption: $scope.attributedata[i].Lable[j].Label,
                                            level: j + 1
                                        });
                                        $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        $scope.settreeTexts();
                                        $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        $scope.setFieldKeys();
                                    }
                                }
                            }
                            $scope.treelevels["dropdown_levels_" + $scope.attributedata[i].ID] = $scope.items;
                            $scope.settreelevels();
                            $scope.items = [];
                            $scope.settreeSources();
                            $scope.settreeTexts();
                            $scope.settreelevels();
                            $scope.setFieldKeys();
                            for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID + "_" + (j + 1)] = true;
                                var inlineEditabletitile = $scope.treelevels['dropdown_levels_' + $scope.attributedata[i].ID][j].caption;
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><a xeditabletreedropdown href=\"javascript:;\" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" InheritFromParent="' + $scope.attributedata[i].IsInheritFromParent + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '" attributeid="' + $scope.attributedata[i].ID + '" id="fields.DropDown_' + $scope.attributedata[i].ID + "_" + (j + 1) + '" data-ng-model=\"dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" attributename=' + inlineEditabletitile + ' data-type=' + inlineEditabletitile + ' >{{treeTexts.dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</a></div></div>';
                            }
                        } else if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == false) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                            var textvalue = "-";
                            if ($scope.attributedata[i].Caption != undefined) textvalue = $scope.attributedata[i].Caption;
                            if ($scope.IsLock == false) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditable href=\"javascript:;\" ng-click="costcentreentityclick()" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"SingleLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"SingleLineText_' + $scope.attributedata[i].ID + '\"  data-type=\"text\"  my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '">' + textvalue + '</a></div></div>';
                            else if ($scope.IsLock == true) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label">' + textvalue + '</label></div></div>';
                        } else if ($scope.attributedata[i].TypeID == 2) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                            var textvalue = "-";
                            if ($scope.attributedata[i].Caption != undefined) textvalue = $scope.attributedata[i].Caption;
                            if ($scope.IsLock == false) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditable href=\"javascript:;\" ng-click="costcentremultilineentityclick()" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"MultiLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"SingleLineText_' + $scope.attributedata[i].ID + '\" data-type=\"textarea\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '">' + textvalue.replace(/\n/g, "<br/>") + '</a></div></div>';
                            else if ($scope.IsLock == true) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label">' + textvalue.replace(/\n/g, "<br/>") + '</label></div></div>';
                        } else if ($scope.attributedata[i].TypeID == 11) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                            $scope.fields["uploader_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                            $scope.setFieldKeys();
                            $scope.UploaderCaption["uploader_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group ng-scope\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable.toString() + '</label>';
                            $scope.dyn_Cont += '<div class=\"controls\">';
                            if ($scope.attributedata[i].Caption == "" || $scope.attributedata[i].Caption == null && $scope.attributedata[i].Caption == undefined) {
                                $scope.attributedata[i].Caption = $scope.attributedata[i].Lable;
                            }
                            if ($scope.attributedata[i].Value == "" || $scope.attributedata[i].Value == null && $scope.attributedata[i].Value == undefined) {
                                $scope.attributedata[i].Value = "NoThumpnail.jpg";
                            }
                            $scope.dyn_Cont += '<img id="UploaderPreview_' + $scope.attributedata[i].ID + '" src="' + imagesrcpath + 'UploadedImages/' + $scope.attributedata[i].Value + '" alt="' + $scope.attributedata[i].Caption + '"';
                            $scope.dyn_Cont += 'class="entityDetailImgPreview" style="height: 112px; display: inline-block; margin-top: 5px; width: 112px; border: 1px solid #ddd; background-color: #eee; text-align: center;">';
                            if ($scope.IsLock == false) {
                                $scope.dyn_Cont += "<a  style='margin-left: 17px;' ng-click='UploadImagefileCC(" + $scope.attributedata[i].ID + ")' attributeTypeID='" + $scope.attributedata[i].TypeID + "'";
                                $scope.dyn_Cont += 'entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="Uploader_' + $scope.attributedata[i].ID + '"';
                                $scope.dyn_Cont += 'my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '"';
                                $scope.dyn_Cont += ' class="ng-pristine ng-valid" attributename=\"' + $scope.UploaderCaption["UploaderCaption_" + $scope.attributedata[i].ID] + '\">Select Image';
                                $scope.dyn_Cont += '</a></div></div>';
                            } else if ($scope.IsLock == true) $scope.dyn_Cont += '</a></div></div>';
                        } else if ($scope.attributedata[i].TypeID == 3) {
                            if ($scope.attributedata[i].ID == SystemDefiendAttributes.Owner && $scope.attributedata[i].IsSpecial == true) {
                                if ($scope.attributedata[i].Caption[0] != undefined) {
                                    $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                    $scope.setFieldKeys();
                                    $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                    $scope.setNormalDropdownCaption();
                                    if ($scope.IsLock == false) $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    else if ($scope.IsLock == true) $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                } else {
                                    $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                    $scope.setFieldKeys();
                                    $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                    $scope.setNormalDropdownCaption();
                                    if ($scope.IsLock == false) {
                                        $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                        $scope.dyn_Cont += '<div class="controls"><a  xeditabledropdown href=\"javascript:;\"';
                                        $scope.dyn_Cont += 'attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '"';
                                        $scope.dyn_Cont += 'attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"';
                                        $scope.dyn_Cont += 'data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '"';
                                        $scope.dyn_Cont += 'attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\"';
                                        $scope.dyn_Cont += 'data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a>';
                                        $scope.dyn_Cont += '</div></div>';
                                    } else if ($scope.IsLock == true) {
                                        $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                        $scope.dyn_Cont += '<div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label>';
                                        $scope.dyn_Cont += '</div></div>';
                                    }
                                }
                            } else {
                                if ($scope.attributedata[i].Caption[0] != undefined) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                                    if ($scope.attributedata[i].Caption[0].length > 1) {
                                        $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                        $scope.setFieldKeys();
                                        $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                        $scope.setNormalDropdownCaption();
                                        if ($scope.IsLock == false) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                        else if ($scope.IsLock == true) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                    } else {
                                        $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                        $scope.setFieldKeys();
                                        $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                        $scope.setNormalDropdownCaption();
                                        if ($scope.IsLock == false) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                        else if ($scope.IsLock == true) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                    }
                                } else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                                    $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                    $scope.setFieldKeys();
                                    $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                    $scope.setNormalDropdownCaption();
                                    if ($scope.IsLock == false) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    else if ($scope.IsLock == true) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                }
                            }
                        } else if ($scope.attributedata[i].TypeID == 4) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                if ($scope.attributedata[i].Caption.length > 1) {
                                    $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                    $scope.setFieldKeys();
                                    $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                    $scope.setNormalMultiDropdownCaption();
                                    if ($scope.IsLock == false) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditablemultiselectdropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalMultiDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    else if ($scope.IsLock == true) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                } else {
                                    $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = "-";
                                    $scope.setFieldKeys();
                                    $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                    $scope.setNormalMultiDropdownCaption();
                                    if ($scope.IsLock == false) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditablemultiselectdropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalMultiDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    else if ($scope.IsLock == true) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                }
                            } else {
                                $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalMultiDropdownCaption();
                                if ($scope.IsLock == false) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditablemultiselectdropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalMultiDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                else if ($scope.IsLock == true) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                            }
                        } else if ($scope.attributedata[i].TypeID == 10) {
                            var inlineEditabletitile = $scope.attributedata[i].Caption;
                            perioddates = [];
                            $scope.dyn_Cont += '<div class="period control-group nomargin" data-periodcontainerID="periodcontainerID">';
                            if ($scope.attributedata[i].Value == "-") {
                                $scope.IsStartDateEmpty = true;
                                $scope.dyn_Cont += '<div class="control-group nomargin" data-addperiodID="addperiodID">';
                                $scope.dyn_Cont += '</div>';
                            } else {
                                for (var j = 0; j < $scope.attributedata[i].Value.length; j++) {
                                    var datStartUTCval = "";
                                    var datstartval = "";
                                    var datEndUTCval = "";
                                    var datendval = "";
                                    datStartUTCval = $scope.attributedata[i].Value[j].Startdate.substr(0, 10);
                                    datstartval = new Date.create(datStartUTCval);
                                    datEndUTCval = $scope.attributedata[i].Value[j].EndDate.substr(0, 10);
                                    datendval = new Date.create(datEndUTCval);
                                    perioddates.push({
                                        ID: $scope.attributedata[i].Value[j].Id,
                                        value: datendval
                                    });
                                    $scope.fields["PeriodStartDateopen_" + $scope.attributedata[i].Value[j].Id] = false;
                                    $scope.fields["PeriodEndDateopen_" + $scope.attributedata[i].Value[j].Id] = false;
                                    $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id] = dateFormat(datstartval, $scope.GetDefaultSettings.DateFormat);
                                    $scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] = dateFormat(datendval, $scope.GetDefaultSettings.DateFormat);
                                    $scope.fields["PeriodStartDate_Dir_" + $scope.attributedata[i].Value[j].Id] = formatteddateFormat(datstartval, "dd/MM/yyyy");
                                    $scope.fields["PeriodEndDate_Dir_" + $scope.attributedata[i].Value[j].Id] = formatteddateFormat(datendval, "dd/MM/yyyy");
                                    if ($scope.attributedata[i].Value[j].Description == undefined) {
                                        $scope.fields["PeriodDateDesc_Dir_" + $scope.attributedata[i].Value[j].Id] = "-";
                                        $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = "";
                                    } else {
                                        $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = $scope.attributedata[i].Value[j].Description;
                                        $scope.fields["PeriodDateDesc_Dir_" + $scope.attributedata[i].Value[j].Id] = $scope.attributedata[i].Value[j].Description;
                                    }
                                    $('#fsedateid').css("visibility", "hidden");
                                    $scope.dyn_Cont += '<div class="control-group nomargin" data-dynPeriodID="' + $scope.attributedata[i].Value[j].Id + '">';
                                    $scope.dyn_Cont += '<div class="inputHolder span11">';
                                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label>';
                                    $scope.dyn_Cont += '<div class="controls">';
                                    if ($scope.attributedata[i].IsReadOnly == true) {
                                        $scope.dyn_Cont += '<label class="control-label">{{fields.PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '}}';
                                        $scope.dyn_Cont += ' to {{fields.PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '}}</label>';
                                    } else {
                                        if ($scope.IsLock == false) {
                                            $scope.MinValue = $scope.attributedata[i].MinValue;
                                            $scope.MaxValue = $scope.attributedata[i].MaxValue;
                                            $scope.MinValue = $scope.attributedata[i].MinValue;
                                            $scope.MaxValue = $scope.attributedata[i].MaxValue;
                                            $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID] = new Date.create();
                                            $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID] = new Date.create();
                                            if ($scope.MinValue < 0) {
                                                $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MinValue + 1));
                                            } else {
                                                $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MinValue));
                                            }
                                            if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                                $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MaxValue - 1));
                                            } else {
                                                $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].getDate() + 100000);
                                            }
                                            var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID], $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID]);
                                            $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID] = (temp.MinDate);
                                            $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID] = (temp.MaxDate);
                                            $scope.dyn_Cont += '<a xeditabletreedropdown1 data-primaryid="' + $scope.attributedata[i].Value[j].Id + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodstartdate_id=\"PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '\" data-ng-model=\"PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"perdiodate' + $scope.attributedata[i].Value[j].Id + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '}}</a>';
                                            $scope.dyn_Cont += '<a> to </a><a xeditabletreedropdown1 data-primaryid="' + $scope.attributedata[i].Value[j].Id + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodenddate_id=\"PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '\" data-ng-model=\"PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\"  data-type=\"perdiodate' + $scope.attributedata[i].Value[j].Id + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '}}</a>';
                                        } else if ($scope.IsLock == true) {
                                            $scope.dyn_Cont += '<label class="control-label">{{fields.PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '}}';
                                            $scope.dyn_Cont += ' to {{fields.PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '}}</label>';
                                        }
                                    }
                                    $scope.dyn_Cont += '</div></div>';
                                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $translate.instant('LanguageContents.Res_5051.Caption') + inlineEditabletitile + '</label>';
                                    $scope.dyn_Cont += '<div class="controls">';
                                    if ($scope.attributedata[i].IsReadOnly == true) {
                                        $scope.dyn_Cont += '<label class="control-label">{{fields.PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '}}</label>';
                                    } else {
                                        if ($scope.IsLock == false) {
                                            $scope.dyn_Cont += '<a xeditabletreedropdown1 data-primaryid="' + $scope.attributedata[i].Value[j].Id + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodenddate_id=\"PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '\" data-ng-model=\"PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\"  data-type=\"perdiodate' + $scope.attributedata[i].Value[j].Id + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '}}</a>';
                                        } else if ($scope.IsLock == true) {
                                            $scope.dyn_Cont += '<label class="control-label">{{fields.PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '}}</label>';
                                        }
                                    }
                                    $scope.dyn_Cont += '</div></div></div>';
                                    if (j != 0) {
                                        if ($scope.IsLock == false) {
                                            $scope.dyn_Cont += '<div class="buttonHolder span1"><a ng-click="deletePeriodDate(' + $scope.attributedata[i].Value[j].Id + ')"><i class="icon-remove"></i></a></div>';
                                        }
                                    }
                                    $scope.dyn_Cont += '</div>';
                                    if (j == ($scope.attributedata[i].Value.length - 1)) {
                                        $scope.dyn_Cont += '<div class="control-group nomargin" data-addperiodID="addperiodID">';
                                        $scope.dyn_Cont += '</div>';
                                    }
                                }
                            }
                            $scope.dyn_Cont += ' </div>';
                            $scope.dyn_Cont += '<div class="control-group nomargin">';
                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<label  data-tempid="startendID" class="control-label" for="label">' + $translate.instant('LanguageContents.Res_5056.Caption') + '</label>';
                                $scope.dyn_Cont += '<a class="ng-pristine ng-valid editable editable-click" xeditabletreedropdown1 data-primaryid="0" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodstartdate_id=\"PeriodStartDate_0" data-ng-model=\"PeriodStartDate_0"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"perdiodate0" href=\"javascript:;\" attributename="perdiodate">[' + $translate.instant('LanguageContents.Res_5065.Caption') + ' ]</a>';
                            } else {
                                if ($scope.IsLock == false) {
                                    if ($scope.attributedata[i].Value == "-") {
                                        $scope.dyn_Cont += '<label id="fsedateid"  class="control-label" for="label">' + inlineEditabletitile + '</label>';
                                    }
                                    $scope.dyn_Cont += '<div class="controls">';
                                    $scope.dyn_Cont += '<a class="ng-pristine ng-valid editable editable-click" xeditabletreedropdown1 data-primaryid="0" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodstartdate_id=\"PeriodStartDate_0" data-ng-model=\"PeriodStartDate_0"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"perdiodate0" href=\"javascript:;\" attributename="perdiodate">[Add ' + inlineEditabletitile + ' ]</a>';
                                    $scope.dyn_Cont += '</div>';
                                } else if ($scope.IsLock == true) {
                                    $scope.dyn_Cont += '<span></span>';
                                }
                            }
                            $scope.dyn_Cont += '</div>';
                        } else if ($scope.attributedata[i].TypeID == 5 && $scope.attributedata[i].ID != SystemDefiendAttributes.ApproveTime) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                            var datStartUTCval = "";
                            var datstartval = "";
                            var inlineEditabletitile = $scope.attributedata[i].Caption;
                            if ($scope.attributedata[i].Value != null || $scope.attributedata[i].Value != undefined) {
                                datStartUTCval = $scope.attributedata[i].Value.substr(0, 10);
                                datstartval = new Date.create(datStartUTCval);
                                $scope.fields["DateTime_" + $scope.attributedata[i].ID] = dateFormat(datstartval, $scope.GetDefaultSettings.DateFormat);
                                $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = formatteddateFormat(datstartval, "dd/MM/yyyy");
                            } else {
                                $scope.fields["DateTime_" + $scope.attributedata[i].ID] = "-";
                                $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = "-";
                            }
                            if ($scope.attributedata[i].ID != SystemDefiendAttributes.CreationDate) {
                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                } else {
                                    if ($scope.IsLock == false) {
                                        $scope.MinValue = $scope.attributedata[i].MinValue;
                                        $scope.MaxValue = $scope.attributedata[i].MaxValue;
                                        $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID] = new Date.create();
                                        $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID] = new Date.create();
                                        if ($scope.MinValue < 0) {
                                            $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MinValue + 1));
                                        } else {
                                            $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MinValue));
                                        }
                                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                            $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MaxValue - 1));
                                        } else {
                                            $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].getDate() + 100000);
                                        }
                                        var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID], $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID]);
                                        $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID] = (temp.MinDate);
                                        $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID] = (temp.MaxDate);
                                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a xeditabletreedatetime data-primaryid="' + $scope.attributedata[i].ID + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-datetime_id=\"DateTime_' + $scope.attributedata[i].ID + '\" data-ng-model=\"DateTime_' + $scope.attributedata[i].ID + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"datetime' + $scope.attributedata[i].ID + '\" href=\"javascript:;\" attributename="datetime">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                        $scope.fields["DatePart_" + $scope.attributedata[i].ID] = null;
                                    } else if ($scope.IsLock == true) {
                                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                    }
                                }
                            } else {
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                            }
                        } else if ($scope.attributedata[i].TypeID == 12) {
                            $scope.dyn_Cont2 = '';
                            var CaptionObj = $scope.attributedata[i].Caption;
                            for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                                if ($scope.attributedata[i].Lable.length == 1) {
                                    var k = j;
                                    var treeTexts = [];
                                    var fields = [];
                                    $scope.items.push({
                                        caption: $scope.attributedata[i].Lable[j].Label,
                                        level: j + 1
                                    });
                                    if (k == CaptionObj.length) {
                                        $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    } else {
                                        if (CaptionObj[k] != undefined) {
                                            for (k; k < CaptionObj.length; k++) {
                                                treeTexts.push(CaptionObj[k]);
                                                $scope.settreeTexts();
                                                fields.push(CaptionObj[k]);
                                                $scope.setFieldKeys();
                                            }
                                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = treeTexts.join(", ");
                                            $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = fields.join(", ");
                                        } else {
                                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        }
                                    }
                                } else {
                                    if (j == 0) {
                                        if (CaptionObj[j] != undefined) {
                                            $scope.items.push({
                                                caption: $scope.attributedata[i].Lable[j].Label,
                                                level: j + 1
                                            });
                                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                            $scope.settreeTexts();
                                            $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                            $scope.setFieldKeys();
                                        } else {
                                            $scope.items.push({
                                                caption: $scope.attributedata[i].Lable[j].Label,
                                                level: j + 1
                                            });
                                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            $scope.settreeTexts();
                                            $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            $scope.setFieldKeys();
                                        }
                                    } else {
                                        var k = j;
                                        if (j == ($scope.attributedata[i].Lable.length - 1)) {
                                            var treeTexts = [];
                                            var fields = [];
                                            $scope.items.push({
                                                caption: $scope.attributedata[i].Lable[j].Label,
                                                level: j + 1
                                            });
                                            if (k == CaptionObj.length) {
                                                $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                                $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            } else {
                                                if (CaptionObj[k] != undefined) {
                                                    for (k; k < CaptionObj.length; k++) {
                                                        treeTexts.push(CaptionObj[k]);
                                                        $scope.settreeTexts();
                                                        fields.push(CaptionObj[k]);
                                                        $scope.setFieldKeys();
                                                    }
                                                    $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = treeTexts.join(", ");
                                                    $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = fields.join(", ");
                                                } else {
                                                    $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                                    $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                                }
                                            }
                                        } else {
                                            if (CaptionObj[j] != undefined) {
                                                $scope.items.push({
                                                    caption: $scope.attributedata[i].Lable[j].Label,
                                                    level: j + 1
                                                });
                                                $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                                $scope.settreeTexts();
                                                $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                                $scope.setFieldKeys();
                                            } else {
                                                $scope.items.push({
                                                    caption: $scope.attributedata[i].Lable[j].Label,
                                                    level: j + 1
                                                });
                                                $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                                $scope.settreeTexts();
                                                $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                                $scope.setFieldKeys();
                                            }
                                        }
                                    }
                                }
                            }
                            $scope.treelevels["multiselectdropdown_levels_" + $scope.attributedata[i].ID] = $scope.items;
                            $scope.settreelevels();
                            $scope.items = [];
                            $scope.settreeSources();
                            $scope.settreeTexts();
                            $scope.settreelevels();
                            $scope.setFieldKeys();
                            for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID + "_" + (j + 1)] = true;
                                var inlineEditabletitile = $scope.treelevels['multiselectdropdown_levels_' + $scope.attributedata[i].ID][j].caption;
                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><label class="control-label">{{treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</label></div></div>';
                                } else {
                                    if ($scope.IsLock == false) {
                                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><a xeditablemultiselecttreedropdown href=\"javascript:;\" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" InheritFromParent="' + $scope.attributedata[i].IsInheritFromParent + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '" attributeid="' + $scope.attributedata[i].ID + '" id="fields.MultiSelectDropDown_' + $scope.attributedata[i].ID + "_" + (j + 1) + '" data-ng-model=\"multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\"  attributename=' + inlineEditabletitile + ' data-type=' + inlineEditabletitile + ' >{{treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</a></div></div>';
                                    } else if ($scope.IsLock == true) {
                                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><label class="control-label">{{treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</label></div></div>';
                                    }
                                }
                            }
                        } else if ($scope.attributedata[i].TypeID == 5 && $scope.attributedata[i].ID != SystemDefiendAttributes.ApproveTime) {
                            var datStartUTCval = "";
                            var datstartval = "";
                            datstartval = new Date($scope.attributedata[i].Value);
                            $scope.fields["DateTime_" + $scope.attributedata[i].ID] = dateFormat(ConvertDateFromStringToString(ConvertDateToString(datstartval)).toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3").toString('dd-MM-yyyy'), $scope.GetDefaultSettings.DateFormat);
                            $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = dateFormat(ConvertDateFromStringToString(ConvertDateToString(datstartval)).toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3").toString('dd-MM-yyyy'), $scope.GetDefaultSettings.DateFormat);
                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                            } else {
                                if ($scope.IsLock == false) {
                                    $scope.MinValue = $scope.attributedata[i].MinValue;
                                    $scope.MaxValue = $scope.attributedata[i].MaxValue;
                                    $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID] = new Date.create();
                                    $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID] = new Date.create();
                                    if ($scope.MinValue < 0) {
                                        $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MinValue + 1));
                                    } else {
                                        $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MinValue));
                                    }
                                    if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                        $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MaxValue - 1));
                                    } else {
                                        $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].getDate() + 100000);
                                    }
                                    var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID], $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID]);
                                    $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID] = (temp.MinDate);
                                    $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID] = (temp.MaxDate);
                                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a xeditabletreedatetime data-primaryid="' + $scope.attributedata[i].ID + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-datetime_id=\"DateTime_' + $scope.attributedata[i].ID + '\" data-ng-model=\"DateTime_' + $scope.attributedata[i].ID + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"datetime' + $scope.attributedata[i].ID + '\" href=\"javascript:;\" attributename="datetime">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                } else if ($scope.IsLock == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                }
                            }
                        } else if ($scope.attributedata[i].TypeID == 17) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                if ($scope.attributedata[i].Caption.length > 1) {
                                    $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                    $scope.fields["TagWordsSeleted_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Value;
                                }
                            } else {
                                $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = "-";
                                $scope.fields["TagWordsSeleted_" + $scope.attributedata[i].ID] = [];
                            }
                            $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                            } else {
                                if ($scope.IsLock == false) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabletagwords href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="TagWordsCaption_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.TagWordsCaption_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                } else if ($scope.IsLock == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                }
                            }
                        }
                    }
                    $("#dynamicdetail").html($compile($scope.dyn_Cont)($scope));
                    HideAttributeToAttributeRelationsOnPageLoad();
                    i = 0;
                    for (i = 0; i < $scope.attributedata.length; i++) {
                        if (($scope.attributedata[i].TypeID == 3 || $scope.attributedata[i].TypeID == 6 || $scope.attributedata[i].TypeID == 12) && $scope.attributedata[i].IsSpecial == false) {
                            if ($scope.attributedata[i].TypeID == 12) {
                                for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                                    if ($scope.attributedata[i].Value[j] != undefined && $scope.attributedata[i].Value[j] != null) {
                                        if (($scope.attributedata[i].Lable.length - 1) == j) {
                                            var k = $scope.attributedata[i].Value.length - $scope.attributedata[i].Lable.length;
                                            for (k; k < $scope.attributedata[i].Value.length; k++) {
                                                $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Lable[j].Level, $scope.attributedata[i].Value[k].Nodeid);
                                            }
                                        } else {
                                            $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Lable[j].Level, $scope.attributedata[i].Value[j].Nodeid);
                                        }
                                    }
                                }
                            } else if ($scope.attributedata[i].TypeID == 6) {
                                for (var j = 0; j < $scope.attributedata[i].Lable.length - 1; j++) {
                                    if ($scope.attributedata[i].Value[j] != undefined && $scope.attributedata[i].Value[j] != null) $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Lable[j].Level, $scope.attributedata[i].Value[j].Nodeid);
                                }
                            } else {
                                $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Level, $scope.attributedata[i].Value);
                            }
                        }
                    }
                });
                $scope.CurrentWorkflowStepName = "";
                $scope.workflowstepOptions = [];
                $scope.workflowSummaryList = [];
                $scope.SelectedWorkflowStep = 0;
                $scope.WorkFlowStepID = 0;
                $scope.WorkflowSummaryControls = '';
                try {
                    $scope.UpdateEntityWorkFlowStep = function (stepID) {
                        var TaskStatusData = {};
                        TaskStatusData.EntityID;
                        parseInt($stateParams.ID, 10);
                        TaskStatusData.Status = stepID;
                        CcoverviewService.UpdateEntityActiveStatus(TaskStatusData).then(function (TaskStatusResult) {
                            if (TaskStatusResult.StatusCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_4375.Caption'));
                            } else {
                                NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                                for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                                    for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                                        var EntityId = $scope.ListViewDetails[i].data.Response.Data[j]["Id"];
                                        if (EntityId == parseInt($stateParams.ID, 10)) {
                                            if ($scope.ListViewDetails[i].data.Response.Data[j][SystemDefiendAttributes.Status] != undefined) $scope.ListViewDetails[i].data.Response.Data[j][SystemDefiendAttributes.Status] = $scope.CurrentWorkflowStepName;
                                            else $scope.ListViewDetails[i].data.Response.Data[j]["Status"] = $scope.CurrentWorkflowStepName;
                                            return false;
                                        }
                                    }
                                }
                                $scope.SelectedWorkflowStep = 0;
                            }
                        });
                    }
                } catch (exc) { }
                $scope.saveDropdownTree = function (attrID, attributetypeid, entityTypeid, optionarray) {
                    if (attrID == 57) {
                        var updateentityattrib = {};
                        updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                        updateentityattrib.AttributeID = attrID;
                        updateentityattrib.Level = 0;
                        updateentityattrib.NewValue = optionarray == "" ? ["0"] : [optionarray];
                        updateentityattrib.AttributetypeID = attributetypeid;
                        CcoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                            if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                            else NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                            UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                        });
                        $scope.treeSelection = [];
                    } else if (attributetypeid == 1) {
                        var updateentityattrib = {};
                        updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                        updateentityattrib.AttributeID = attrID;
                        updateentityattrib.Level = 0;
                        updateentityattrib.NewValue = optionarray == "" ? ["0"] : [optionarray]
                        updateentityattrib.AttributetypeID = attributetypeid;
                        if (attrID == 68) {
                            $(window).trigger("UpdateEntityName", optionarray);
                            $("ul li a[data-entityid='" + $stateParams.ID + "'] span[data-name='text']").text([optionarray]);
                        }
                        CcoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                            if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                            else {
                                $scope.fields['SingleLineTextValue_' + attrID] = optionarray;
                                NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                                UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                                if (attrID == 68) {
                                    $('#breadcrumlink').text(optionarray);
                                }
                            }
                        });
                        $scope.treeSelection = [];
                    } else if (attributetypeid == 3 || attributetypeid == 4 || attributetypeid == 17) {
                        var updateentityattrib = {};
                        updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                        updateentityattrib.AttributeID = attrID;
                        updateentityattrib.Level = 0;
                        updateentityattrib.NewValue = optionarray == "" ? ["0"] : [optionarray];
                        updateentityattrib.AttributetypeID = attributetypeid;
                        CcoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                            if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                            else $scope.ShowHideAttributeToAttributeRelations(attrID, 0, 0, attributetypeid, optionarray);
                            NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                            UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                        });
                        $scope.treeSelection = [];
                    } else if (attributetypeid == 6) {
                        var updateentityattrib = {};
                        updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                        updateentityattrib.AttributeID = attrID;
                        updateentityattrib.Level = 0;
                        updateentityattrib.NewValue = optionarray;
                        updateentityattrib.AttributetypeID = attributetypeid;
                        CcoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                            if (updateentityattribresult.StatusCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                            } else {
                                NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                                for (var i = 0; i < optionarray.length; i++) {
                                    if (optionarray[i] != 0) {
                                        var level = i + 1;
                                        UpdateTreeScope(attrID + "_" + level, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"][data-ng-model="dropdown_text_' + attrID + "_" + level + '"]').text());
                                    }
                                }
                                for (var j = 0; j < optionarray.length; j++) {
                                    if (optionarray[j] != 0) {
                                        $scope.ShowHideAttributeToAttributeRelations(attrID, (j) + 1, optionarray.length, attributetypeid, optionarray[j]);
                                    }
                                }
                            }
                        });
                        $scope.treeSelection = [];
                    } else if (attributetypeid == 12) {
                        var updateentityattrib = {};
                        updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                        updateentityattrib.AttributeID = attrID;
                        updateentityattrib.Level = 0;
                        updateentityattrib.NewValue = optionarray;
                        updateentityattrib.AttributetypeID = attributetypeid;
                        CcoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                            if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                            else {
                                for (var j = 0; j < optionarray.length; j++) {
                                    if (optionarray[j] != 0) {
                                        $scope.ShowHideAttributeToAttributeRelations(attrID, (j) + 1, optionarray.length, attributetypeid, optionarray[j]);
                                    }
                                }
                                NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                                UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                            }
                        });
                        $scope.treeSelection = [];
                    } else if (attributetypeid == 8) {
                        if (!isNaN(optionarray.toString())) {
                            var updateentityattrib = {};
                            updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                            updateentityattrib.AttributeID = attrID;
                            updateentityattrib.Level = 0;
                            updateentityattrib.NewValue = optionarray == "" ? ["0"] : [optionarray];
                            updateentityattrib.AttributetypeID = attributetypeid;
                            CcoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                                if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                                else {
                                    $scope.fields['SingleLineTextValue_' + attrID] = optionarray == "" ? 0 : optionarray;
                                    NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                                    UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                                }
                            });
                        } else {
                            NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                        }
                        $scope.treeSelection = [];
                    } else if (attributetypeid == 5) {
                        var sdate = new Date($scope.fields['DateTime_Dir_' + attrID].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
                        if (sdate == 'NaN-NaN-NaN') {
                            sdate = ConvertDateToString(ConvertStringToDateByFormat($scope.fields['DateTime_Dir_' + periodid].toString(), GlobalUserDateFormat))
                        }
                        var updateentityattrib = {};
                        updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                        updateentityattrib.AttributeID = attrID;
                        updateentityattrib.Level = 0;
                        updateentityattrib.NewValue = [sdate];
                        updateentityattrib.AttributetypeID = attributetypeid;
                        CcoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                            if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                            else {
                                NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                                UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                                $scope.fields["DateTime_" + attrID] = ConvertDateFromStringToString(ConvertDateToString(sdate));
                            }
                        });
                        $scope.treeSelection = [];
                    }
                    $scope.treeSelection = [];
                    $timeout(function () {
                        $scope.TimerForLatestFeed();
                    }, 5000);
                };
                $scope.enabletext = function () {
                    $('#username').editable({
                        placement: 'right',
                        title: 'Select Status'
                    });
                }
                $scope.saveDetailBlock = function (val) {
                    var NewValue = '';
                    if (val == 1) {
                        NewValue = $scope.txtActivityName;
                        $scope["showActivityName"] = true;
                        $scope["EditActivityName"] = false;
                    }
                    if (val == 2) {
                        NewValue = $scope.fields.DropDown_Members.Id;
                        $scope["showOwner"] = true;
                        $scope["EditOwner"] = false;
                    }
                    if (val == 3) {
                        NewValue = $scope.fields.DropDown_FiscalYear.Id;
                        $scope["HideInlineEdit0"] = true;
                        $scope["showinlineedit0"] = false;
                    }
                    var updateentityattrib = {};
                    updateentityattrib.ID = val;
                    updateentityattrib.EntityID = ID;
                    updateentityattrib.NewValue = NewValue;
                    CcoverviewService.SaveDetailBlock(updateentityattrib).then(function () {
                        $scope.TimerForLatestFeed();
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                    });
                };
            } catch (e) { }
            try {
                $scope.GettingNewsFeedTime = function (serverTime, happendTime) {
                    var appServerTime = serverTime;
                    var feedHappendTime = happendTime;
                    var timeDiff = Math.abs(appServerTime - feedHappendTime);
                }
            } catch (e) { }
            $scope.LoadFinancialSummanyBlock(ID);
        };
        EnableDisableLoader();

        function EnableDisableLoader() {
            $scope.detailsLoader = true;
            $scope.detailsData = false;
        }
        $scope.treesrcdirec = {};
        $scope.staticTreesrcdirec = {};
        $scope.TreeEmptyAttributeObj = {};
        $scope.treeNodeSelectedHolder = new Array();
        var apple_selected, tree, treedata_avm, treedata_geography;
        $scope.my_tree_handler = function (branch, parentArr) {
            var _ref;
            if ((_ref = branch.data) != null ? _ref.description : void 0) {
                return $scope.output += '(' + branch.data.description + ')';
            }
        };
        var treeTextVisbileflag = false;

        function IsNotEmptyTree(treeObj) {
            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    treeTextVisbileflag = true;
                    return treeTextVisbileflag;
                } else {
                    IsNotEmptyTree(node.Children);
                }
            }
            return treeTextVisbileflag;
        }
        $scope.savetreeDetail = function (attrID, attributetypeid, entityTypeid) {
            if (attributetypeid == 7) {
                $scope.treeNodeSelectedHolder = [];
                GetTreeObjecttoSave(attrID);
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.NewValue = $scope.treeNodeSelectedHolder;
                updateentityattrib.newTree = $scope.staticTreesrcdirec["Attr_" + attrID];
                updateentityattrib.oldTree = $scope.treesrcdirec["Attr_" + attrID];
                updateentityattrib.AttributetypeID = attributetypeid;
                CcoverviewService.SaveDetailBlockForTreeLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    } else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        CcoverviewService.GetAttributeTreeNodeByEntityID(attrID, parseInt($stateParams.ID, 10)).then(function (GetTree) {
                            $scope.treesrcdirec["Attr_" + attrID] = JSON.parse(GetTree.Response).Children;
                            if ($scope.treeNodeSelectedHolder.length > 0) $scope.TreeEmptyAttributeObj["Attr_" + attrID] = true;
                            else $scope.TreeEmptyAttributeObj["Attr_" + attrID] = false;
                            $timeout(function () {
                                $scope.TimerForLatestFeed();
                            }, 3000);
                        });
                        $scope.fields["Tree_" + attrID].splice(0, $scope.fields["Tree_" + attrID].length);
                        GetTreeCheckedNodes($scope.treeNodeSelectedHolder, attrID);
                        $scope.ShowHideAttributeToAttributeRelations(attrID, 0, 0, attributetypeid, $scope.fields["Tree_" + attrID]);
                    }
                });
            }
        }

        function GetTreeObjecttoSave(attributeid) {
            GenerateTreeStructure($scope.staticTreesrcdirec["Attr_" + attributeid]);
        }
        var treeformflag = false;

        function GenerateTreeStructure(treeobj) {
            for (var i = 0, node; node = treeobj[i++];) {
                if (node.ischecked == true) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                        return e.AttributeId == node.AttributeId && e.id == node.id;
                    });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolder.push(node);
                    }
                    treeformflag = false;
                    if (ischildSelected(node.Children)) {
                        GenerateTreeStructure(node.Children);
                    } else {
                        GenerateTreeStructure(node.Children);
                    }
                } else GenerateTreeStructure(node.Children);
            }
        }

        function ischildSelected(children) {
            for (var j = 0, child; child = children[j++];) {
                if (child.ischecked == true) {
                    treeformflag = true;
                    return treeformflag
                }
            }
            return treeformflag;
        }
        $scope.costcentreentityclick = function () {
            $timeout(function () {
                $('[class=input-medium]').focus().select()
            }, 10);
        };
        $scope.costcentremultilineentityclick = function () {
            $timeout(function () {
                $('[class=input-large]').focus().select()
            }, 10);
        };

        function GetTreeCheckedNodes(treeobj, attrID) {
            for (var i = 0, node; node = treeobj[i++];) {
                if (node.ischecked == true) {
                    $scope.fields["Tree_" + attrID].push(node.id);
                }
                if (node.Children.length > 0) GetTreeCheckedNodes(node.Children, attrID);
            }
        }

        function HideAttributeToAttributeRelationsOnPageLoad() {
            try {
                if ($scope.listAttriToAttriResult != undefined) {
                    for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
                        var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
                                } else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                }
                            }
                        }
                    }
                }
            } catch (e) { }
        }
        $scope.ShowHideAttributeToAttributeRelationsOnpageLoad = function (attrID, attributeLevel, attrVal, attrType) {
            try {
                var optionValue = attrVal;
                var attributesToShow = [];
                if (attrType == 3) {
                    attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeOptionID == optionValue;
                    })[0]);
                } else if (attrType == 4) {
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 7) {
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 6 || attrType == 12) {
                    if (attrVal != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrVal != null) ? parseInt(attrVal, 10) : 0) && e.AttributeLevel == ((attributeLevel != null) ? parseInt(attributeLevel, 10) : 0));
                        })[0]);
                    }
                }
                if (attributesToShow[0] != undefined) {
                    for (var i = 0; i < attributesToShow.length; i++) {
                        var attrRelIDs = attributesToShow[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = true;
                                } else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = true;
                                }
                            }
                        }
                    }
                }
            } catch (e) { }
        }

        function RecursiveAttributeToAttributeHide(res, attrID, attrLevel, attrType) {
            var recursiveAttrID = '';
            var attributesToHide = [];
            if (attrLevel > 0) {
                attributesToHide.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID && e.AttributeLevel >= attrLevel);
                }));
            } else {
                attributesToHide.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID);
                }));
            }
            if (attributesToHide[0] != undefined) {
                for (var i = 0; i < attributesToHide[0].length; i++) {
                    var attrRelIDs = attributesToHide[0][i].AttributeRelationID.toString().split(',');
                    if (attrRelIDs != undefined) {
                        for (var j = 0; j < attrRelIDs.length; j++) {
                            if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return (e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))) && e.AttributeLevel == attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].length));
                                }));
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType);
                                    }
                                }
                            } else {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                $scope.fields['ListSingleSelection_' + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = "";
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                }));
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $scope.ShowHideAttributeToAttributeRelations = function (attrID, attributeLevel, levelcnt, attrType, attrVal) {
            try {
                var recursiveAttrID = '';
                var optionValue = '';
                var attributesToHide = [];
                var hideAttributeOtherThanSelected = [];
                if (attrID == SystemDefiendAttributes.FiscalYear) {
                    try {
                        $scope.changeCostCenterSource();
                    } catch (e) { }
                }
                if ($scope.listAttriToAttriResult == null && $scope.listAttriToAttriResult == undefined) {
                    return false;
                }
                RecursiveAttributeToAttributeHide($scope.listAttriToAttriResult, attrID, attributeLevel, attrType);
                if (attrType == 3) {
                    optionValue = attrVal;
                    attributesToHide.push($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && e.AttributeOptionID == optionValue;
                    })[0]);
                } else if (attrType == 4) {
                    optionValue = attrVal;
                    attributesToHide = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 6) {
                    var attrval = $scope.fields["DropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToHide = [];
                        attributesToHide.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                } else if (attrType == 7) {
                    optionValue = attrVal;
                    attributesToHide = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 12) {
                    var attrval = $scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToHide = [];
                        attributesToHide.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                }
                if (attributesToHide[0] != undefined) {
                    for (var i = 0; i < attributesToHide.length; i++) {
                        var attrRelIDs = attributesToHide[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = true;
                                } else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = true;
                                }
                            }
                        }
                    }
                }
            } catch (e) { }
        }
        $scope.OverAllEntityStatusVis = true;
        $scope.ReloadFromTimeout = false;

        function funloadEntityStatusForCCEdit(EntityStatusByID, EntityStatus) {
            var EntityStatusResult = EntityStatusByID;
            $scope.OverAllEntityStatusVis = true;
            var EntityStatusRes = EntityStatus;
            $scope.OverAllEntityStatusVis = true;
            if (EntityStatusRes.length > 0) {
                $scope.EntityStatusResult = EntityStatusRes;
                $scope.Showstatus = false;
                $timeout(function () {
                    if (EntityStatusResult != null) {
                        $scope.OverAllEntityStatus = EntityStatusResult.Status;
                        $scope.OverAllEntityInTime = EntityStatusResult.TimeStatus;
                        $scope.DynamicTaskListDescriptionObj = (EntityStatusResult.Comment != null ? EntityStatusResult.Comment : '-');
                        $scope.Ontime = OnSelectionChange(EntityStatusResult.TimeStatus);
                        $scope.ReloadFromTimeout = true;
                        $scope.StopeUpdateStatusonPageLoad = true;
                    }
                }, 100);
            } else {
                $scope.Showstatus = true;
            }
            $scope.OverAllEntityStatusVis = false;
            lockEntityStatus()
        }

        function lockEntityStatus() {
            if ($scope.IsLock == true) {
                $timeout(function () {
                    $('#OverAllEntityStats').select2("enable", false)
                    $('#OntimeStatus').attr('disabled', 'disabled');
                    $("#entitystatuscommentID").attr('readonly', true);
                    $scope.updatecommnet = false;
                    $scope.cancelcommnet = false;
                    50
                });
            }
            else if ($scope.IsLock == false) {
                $timeout(function () {
                    $('#OverAllEntityStats').select2("enable", true)
                    $('#OntimeStatus').attr('disabled', false);
                    $("#entitystatuscommentID").attr('readonly', false);
                    $scope.updatecommnet = true;
                    $scope.cancelcommnet = true;
                    50
                });
            }
        }

        function OnSelectionChange(Selectionid) {
            var Newvalue = $.grep($scope.OntimeStatusLists, function (n, i) {
                return ($scope.OntimeStatusLists[i].Id == Selectionid);
            });
            $scope.DynamicClass = Newvalue[0].SelectedClass;
            return Newvalue[0].Name;
        }
        $scope.TaskListSummerydetal = []
        $scope.GetEntityMembers();
        $scope.LoadNewsFeedBlock();
        $scope.DynamicTaskListDescriptionObj = "-";

        function ClearSummaryDetl() {
            $scope.TaskListSummerydetal = []
            $scope.DynamicTaskListDescriptionObj = "-";
            $scope.Ontime = OnSelectionChange(0);
            $scope.Ontimecomment = '-';
            $scope.TasksInprgress = 0;
            $scope.Unassigned = 0;
            $scope.Overduetasks = 0;
            $scope.Unabletocomplete = 0;
            $scope.TaskListSummerydetal = [];
            FindmaxWidth();
        }
        $scope.DynamicClass = 'btn-success';

        function FindmaxWidth() {
            var maxwidth = $scope.TasksInprgress;
            if (maxwidth < $scope.Unassigned) maxwidth = $scope.Unassigned;
            if (maxwidth < $scope.Overduetasks) maxwidth = $scope.Overduetasks;
            if (maxwidth < $scope.Unabletocomplete) maxwidth = $scope.Unabletocomplete;
            if (maxwidth == 0) {
                $scope.taskinprgwidth = 0;
                $scope.unassignwidth = 0;
                $scope.overduetaskwidth = 0;
                $scope.unabletocompletewidth = 0;
                if (document.getElementById('taskinprg') != undefined) {
                    document.getElementById('taskinprg').style.width = $scope.taskinprgwidth + '%';
                }
                if (document.getElementById('unassign') != undefined) {
                    document.getElementById('unassign').style.width = $scope.unassignwidth + '%';
                }
                if (document.getElementById('overduetask') != undefined) {
                    document.getElementById('overduetask').style.width = $scope.overduetaskwidth + '%';
                }
                if (document.getElementById('unabletocomplete') != undefined) {
                    document.getElementById('unabletocomplete').style.width = $scope.unabletocompletewidth + '%';
                }
            } else {
                $scope.taskinprgwidth = ((($scope.TasksInprgress / maxwidth) * 100) / 100) * 80;
                $scope.unassignwidth = ((($scope.Unassigned / maxwidth) * 100) / 100) * 80;
                $scope.overduetaskwidth = ((($scope.Overduetasks / maxwidth) * 100) / 100) * 80;
                $scope.unabletocompletewidth = ((($scope.Unabletocomplete / maxwidth) * 100) / 100) * 80;
                if (document.getElementById('taskinprg') != undefined) {
                    document.getElementById('taskinprg').style.width = $scope.taskinprgwidth + '%';
                }
                if (document.getElementById('unassign') != undefined) {
                    document.getElementById('unassign').style.width = $scope.unassignwidth + '%';
                }
                if (document.getElementById('overduetask') != undefined) {
                    document.getElementById('overduetask').style.width = $scope.overduetaskwidth + '%';
                }
                if (document.getElementById('unabletocomplete') != undefined) {
                    document.getElementById('unabletocomplete').style.width = $scope.unabletocompletewidth + '%';
                }
            }
        }

        function LoadTaskSummaryDetlEdit(EntityTaskLists) {
            ClearSummaryDetl();
            $scope.TaskListSummerydetal = []
            $scope.DynamicTaskListDescriptionObj = "-";
            var TaskSummaryresult = EntityTaskLists;
            if (TaskSummaryresult.length > 0) {
                $scope.ShowEmptyTaskSummary = false;
                $scope.ShowLoadedTaskSummary = true;
                $scope.TaskListSummerydetal = TaskSummaryresult;
                $scope.OverAllStatus = $scope.TaskListSummerydetal[0].ActiveEntityStateID;
                $scope.Ontimecomment = ($scope.TaskListSummerydetal[0].OnTimeComment != null ? $scope.TaskListSummerydetal[0].OnTimeComment : '-');
                $scope.TasksInprgress = $scope.TaskListSummerydetal[0].TaskInProgress;
                $scope.Unassigned = $scope.TaskListSummerydetal[0].UnAssignedTasks;
                $scope.Overduetasks = $scope.TaskListSummerydetal[0].OverDueTasks;
                $scope.Unabletocomplete = $scope.TaskListSummerydetal[0].UnableToComplete;
                FindmaxWidth();
            } else {
                $scope.ShowEmptyTaskSummary = true;
                $scope.ShowLoadedTaskSummary = false;
            }
        }
        $('#EntityFeedsdiv').on('click', 'a[data-id="taskpopupopen"]', function (event) {
            var entityid = $(this).attr('data-entityid');
            var taskid = $(this).attr('data-taskid');
            var typeid = $(this).attr('data-typeid');
            CcoverviewService.IsActiveEntity(taskid).then(function (result) {
                if (result.Response == true) {
                    $scope.$emit("pingMUITaskEdit", {
                        TaskId: taskid,
                        EntityId: entityid
                    });
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                }
            });
        });
        $('#EntityFeedsdiv').on('click', 'a[data-id="feedpath"]', function (event) {
            event.stopImmediatePropagation();
            var entityid = $(this).attr('data-entityid');
            var typeid = $(this).attr('data-typeid');
            var parentid = $(this).attr('data-parentid');
            CcoverviewService.IsActiveEntity($(this).attr('data-entityid')).then(function (result) {
                if (result.Response == true) {
                    if (typeid == 11 || typeid == 10) $location.path('/mui/planningtool/detail/section/' + parentid + '/' + 'objective' + '');
                    else if (typeid == 5) $location.path('/mui/planningtool/costcentre/detail/section/' + entityid + '/overview');
                    else $location.path('/mui/planningtool/detail/section/' + entityid + '/overview');
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                }
            });
        });
        $('#EntityFeedsdiv').on('click', 'a[data-command="openlink"]', function (event) {
            var TargetControl = $(this);
            var mypage = TargetControl.attr('data-Name');
            var myname = TargetControl.attr('data-Name');
            var w = 1200;
            var h = 800
            var winprops = 'height=' + h + ',width=' + w + ',top=0,left=50,scrollbars=yes,resizable'
            var win = window.open(mypage, myname, winprops)
        });
        $(window).on("ontaskActionnotification", function (event) {
            $("#loadNotificationtask").modal("hide");
        });
        $('#EntityFeedsdiv').on('click', 'a[data-id="openfundrequestpopup"]', function (event) {
            var EntityID = $(this).attr('data-parentid');
            var funderqid = $(this).attr('data-entityid');
            CcoverviewService.IsActiveEntity(funderqid).then(function (result) {
                if (result.Response == true) {
                    $("#feedFundingRequestModal").modal("show");
                    //$('#feedFundingRequestModal').trigger("onNewsfeedCostCentreFundingRequestsAction", [funderqid]);
                    $scope.$emit('onNewsfeedCostCentreFundingRequestsAction', funderqid);
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                }
            });
        });
        $('#EntityFeedsdiv').on('click', 'a[data-id="costcenterlink"]', function (event) {
            event.stopImmediatePropagation();
            var entityid = $(this).attr('data-costcentreid');
            CcoverviewService.IsActiveEntity($(this).attr('data-costcentreid')).then(function (result) {
                if (result.Response == true) {
                    $location.path('/mui/planningtool/costcentre/detail/section/' + entityid + '/overview');
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                }
            });
        });
        $scope.Clearxeditabletreedropdown = function Clearxeditabletreedropdown(attrid, levelcnt, currentlevel) {
            currentlevel = currentlevel + 1;
            for (var i = currentlevel; i <= levelcnt; i++) {
                $scope['dropdown_' + attrid + '_' + i] = "";
            }
        }
        $scope.AddDefaultEndDate = function (stratdateval, enddateval) {
            if ($scope.fields[stratdateval] == null) {
                $scope.fields[enddateval] = null;
            } else {
                var objsetenddate = new Date($scope.fields[stratdateval]);
                $scope.fields[enddateval] = objsetenddate.addDays(7);
            }
        };
                
        $scope.drpdirectiveSource = {};

        $scope.IsSourceformed = function (attrID, levelcnt, attributeLevel, attrType) {
            if (levelcnt > 0) {
                var dropdown_text = '', subid = 0;
                var currntlevel = attributeLevel + 1;
                if (attributeLevel == 1) {
                    var dropdown_text = 'dropdown_text_' + attrID + '_' + attributeLevel;
                    var idtomatch = $scope['dropdown_' + attrID + '_' + attributeLevel] != undefined ? ($scope['dropdown_' + attrID + '_' + attributeLevel].id != undefined ? $scope['dropdown_' + attrID + '_' + attributeLevel].id : $scope['dropdown_' + attrID + '_' + attributeLevel]) : 0;
                    if (idtomatch != 0)
                        $scope.drpdirectiveSource["dropdown_" + attrID + "_" + attributeLevel] = $.grep($scope.treeSources["dropdown_" + attrID].Children,
                            function (e) {
                                return e.id == idtomatch;
                            }
                        )[0];
                }
                for (var j = currntlevel; j <= levelcnt; j++) {
                    dropdown_text = 'dropdown_text_' + attrID + '_' + j;
                    subid = $scope['dropdown_' + attrID + '_' + (j)] != undefined ? ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined ? $scope['dropdown_' + attrID + '_' + (j - 1)].id : $scope['dropdown_' + attrID + '_' + (j)]) : 0;
                    $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = {};
                    if (subid != 0 && subid > 0) {
                        if ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined) {
                            if ($scope['dropdown_' + attrID + '_' + (j - 1)].Children != undefined)
                                $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $scope['dropdown_' + attrID + '_' + (j - 1)].Children;
                            $scope['dropdown_' + attrID + '_' + (j)] = 0;
                        }
                        else {
                            if ($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)].Children != undefined && $scope['dropdown_' + attrID + '_' + (j - 1)] != undefined)
                                $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = ($.grep($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)].Children, function (e) { return e.id == subid; }))[0];
                        }
                    }
                    else {
                        if ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined) {
                            if ($scope['dropdown_' + attrID + '_' + (j - 1)].Children != undefined)
                                $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $scope['dropdown_' + attrID + '_' + (j - 1)].Children;
                            $scope['dropdown_' + attrID + '_' + j] = 0;
                        }
                        else {
                            if ($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)] != undefined) {
                                var res = [];
                                if ($scope['dropdown_' + attrID + '_' + (j)] > 0)
                                    $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $.grep($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)]['Children'], function (e) { return e.id == $scope['dropdown_' + attrID + '_' + (j)] })[0];
                                else
                                    $scope['dropdown_' + attrID + '_' + (j)] = 0;
                            }
                        }
                    }
                }
            }
        }

        $scope.BindChildDropdownSource = function (attrID, levelcnt, attributeLevel, attrType) {

            if (levelcnt > 0) {
                if (attrType == 6) {
                    $scope.IsSourceformed(attrID, levelcnt, attributeLevel, attrType);
                }
                var currntlevel = attributeLevel + 1;
                for (var j = currntlevel; j <= levelcnt; j++) {
                    $scope["ddtoption_" + attrID + "_" + j].data.splice(0, $scope["ddtoption_" + attrID + "_" + j].data.length);
                    if (attrType == 6) {
                        $scope["ddtoption_" + attrID + "_" + (attributeLevel + 1)].data.splice(0, $scope["ddtoption_" + attrID + "_" + (attributeLevel + 1)].data.length);
                    } else if (attrType == 12) {
                        if (j == levelcnt) $scope["multiselectdropdown_" + attrID + "_" + j] = [];
                        else $scope["multiselectdropdown_" + attrID + "_" + j] = "";
                    }
                }
                if (attrType == 6) {
                    if ($scope["dropdown_" + attrID + "_" + attributeLevel].id == undefined) {
                        if ($scope.drpdirectiveSource["dropdown_" + attrID + "_" + attributeLevel] != null && $scope.drpdirectiveSource["dropdown_" + attrID + "_" + attributeLevel] != undefined) {
                            if ($scope.drpdirectiveSource["dropdown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                                $.each($scope.drpdirectiveSource["dropdown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                    $scope["ddtoption_" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                                });
                            }
                        }
                    }
                    else {
                        if ($scope["dropdown_" + attrID + "_" + attributeLevel] != null && $scope["dropdown_" + attrID + "_" + attributeLevel] != undefined) {
                            if ($scope["dropdown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                                $.each($scope["dropdown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                    $scope["ddtoption_" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                                });
                            }
                        }
                    }
                } else if (attrType == 12) {
                    if ($scope["multiselectdropdown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                        $.each($scope["multiselectdropdown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                            $scope["ddtoption_" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                        });
                    }
                }
            }
        }

        $scope.isModelUpdated = function (attrID, levelcnt, attributeLevel, attrType, flag) {
            var currntlevel = attributeLevel + 1;
            var dropdown_text = 'dropdown_text_' + attrID + '_' + currntlevel, modelVal = {};
            for (var j = currntlevel; j <= levelcnt; j++) {
                $scope['dropdown_' + attrID + '_' + currntlevel] = 0;
                modelVal = ($.grep($scope["ddtoption_" + attrID + "_" + currntlevel].data, function (e) {
                    return e.Caption.toString() == $scope.treeTexts[dropdown_text].toString().trim()
                }))[0];
                if (modelVal != undefined) {
                    $scope['dropdown_' + attrID + '_' + currntlevel] = modelVal.id;
                }

            }
        }        

        function UpdateTreeScope(ColumnName, Value) {
            for (var k = 0; k < $scope.ListViewDetails[0].data.Response.ColumnDefs.length; k++) {
                if (ColumnName == $scope.ListViewDetails[0].data.Response.ColumnDefs[k].Field) {
                    for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                        for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                            var EntityId = $scope.ListViewDetails[i].data.Response.Data[j]["Id"];
                            if (EntityId == parseInt($stateParams.ID, 10)) {
                                $scope.ListViewDetails[i].data.Response.Data[j][ColumnName] = Value;
                                return false;
                            }
                        }
                    }
                }
            }
        }
        $scope.StatusAttributeData = [];
        $scope.LoadStatusblock = false;
        $timeout(function () { OverviewStatusAttribute(); }, 50);

        function OverviewStatusAttribute() {
            CcoverviewService.FetchEntityStatusTree($stateParams.ID).then(function (GetStatusvalue) {
                $scope.StatusAttributeData = GetStatusvalue.Response;
                $scope.LoadStatusblock = true;
            });
        }
        $scope.changestatusoption = function (attrid, optionid, optioncaption, optionCmt, optionLvl, type, parentnode, optioncolorcode) {
            if (type == "option") {
                if (optionid == $scope.entitystatuscomment["LevelSelectedId_" + parentnode]) return false;
            }
            if (type == "comment") {
                if (optionCmt == $scope.entitystatuscomment["LevelComment_" + parentnode]) return false;
            }
            var metadata = [{
                "AttributeId": attrid,
                "EntityId": $stateParams.ID,
                "Level": optionLvl,
                "NodeId": optionid,
                "comment": optionCmt,
                "type": type,
                "ParentNode": parentnode,
                "optioncaption": optioncaption,
                "optioncolorcode": optioncolorcode
            }];
            UpdateEntityStatusMetadata(metadata);
        }
        $scope.entitystatuscomment = {};
        $scope.setfocusonComment = function (id, comment) {
            $scope.entitystatuscomment["LevelComment_" + id] = comment;
            $timeout(function () {
                $("#statuscomment_" + id + "").focus().select()
            }, 10);
        };
        $scope.getSelectedOptionvalue = function (levelid, selectvalue, selectid, colorcode) {
            $scope.entitystatuscomment["LevelSelectedCaption_" + levelid] = selectvalue;
            $scope.entitystatuscomment["LevelSelectedId_" + levelid] = selectid;
            $scope.entitystatuscomment["LevelSelectedColorCode_" + levelid] = colorcode;
        }
        $scope.ClearEntityComment = function (attrid, id, option) {
            refreshStatus(attrid, id, option);
        }

        function refreshStatus(attrid, id, option) {
            var updatecopedata = $.grep($scope.StatusAttributeData, function (rel) {
                return rel.attributeid == attrid && rel.levelid == id;
            })[0];
            if (updatecopedata != null) {
                if (option == "comment") updatecopedata.optioncomment = $scope.entitystatuscomment["LevelComment_" + id];
                else {
                    updatecopedata.selectid = $scope.entitystatuscomment["LevelSelectedId_" + id];
                    updatecopedata.selectvalue = $scope.entitystatuscomment["LevelSelectedCaption_" + id];
                    updatecopedata.optioncolorcode = $scope.entitystatuscomment["LevelSelectedColorCode_" + id];
                }
            }
        }

        function UpdateEntityStatusMetadata(metadata) {
            var upd = {};
            upd.EntityID = $stateParams.ID;
            upd.metadata = metadata;
            CcoverviewService.updateOverviewStatus(upd).then(function (Result) {
                if (Result.StatusCode == 200) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                    $scope.entitystatuscomment["LevelSelectedCaption_" + metadata[0]["ParentNode"]] = metadata[0]["optioncaption"];
                    $scope.entitystatuscomment["LevelSelectedId_" + metadata[0]["ParentNode"]] = metadata[0]["NodeId"];
                    $scope.entitystatuscomment["LevelSelectedColorCode_" + metadata[0]["ParentNode"]] = metadata[0]["optioncolorcode"];
                    $scope.entitystatuscomment["LevelComment_" + metadata[0]["ParentNode"]] = metadata[0]["comment"] != "" ? metadata[0]["comment"] : "-";
                    refreshStatus(metadata[0]["AttributeId"], metadata[0]["ParentNode"], metadata[0]["type"]);
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4351.Caption'));
                    refreshStatus(metadata[0]["AttributeId"], metadata[0]["ParentNode"], metadata[0]["type"]);
                }
            });
        }
        $scope.UpdateEntityOverAllStatus = function (OverAllEntityStatus) {
            if (OverAllEntityStatus != "" && OverAllEntityStatus != null && OverAllEntityStatus != undefined) {
                var upd = {};
                upd.EntityID = parseInt($stateParams.ID);
                upd.EntityStatusID = $scope.OverAllEntityStatus;
                upd.OnTimeStatus = $scope.OverAllEntityInTime;
                upd.OnTimeComment = $scope.DynamicTaskListDescriptionObj;
                CcoverviewService.UpdateEntityStatus(upd).then(function (Result) {
                    if (Result.StatusCode == 200) {
                        NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                        for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                            for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                                var EntityId = $scope.ListViewDetails[i].data.Response.Data[j]["Id"];
                                if (EntityId == parseInt($stateParams.ID, 10)) {
                                    if ($scope.ListViewDetails[i].data.Response.Data[j][SystemDefiendAttributes.EntityOnTimeStatus] != undefined) {
                                        $scope.ListViewDetails[i].data.Response.Data[j][SystemDefiendAttributes.EntityOnTimeStatus] = $("#OntimeStatus").text().trim();
                                    } else {
                                        $scope.ListViewDetails[i].data.Response.Data[j]["EntityOnTimeStatus"] = $("#OntimeStatus").text().trim();
                                    }
                                    if ($scope.ListViewDetails[i].data.Response.Data[j][SystemDefiendAttributes.EntityStatus] != undefined) $scope.ListViewDetails[i].data.Response.Data[j][SystemDefiendAttributes.EntityStatus] = $('#OverAllEntityStats option:selected').text();
                                    else $scope.ListViewDetails[i].data.Response.Data[j]["Status"] = $('#OverAllEntityStats option:selected').text();
                                    $timeout(function () {
                                        $scope.TimerForLatestFeed();
                                    }, 2000);
                                    return false;
                                }
                            }
                        }
                    }
                });
            }
        }
        $scope.UploadAttributeId = 0;
        $scope.UploadImagefileCC = function (attributeId) {
            $scope.UploadAttributeId = attributeId;
            $("#pickfilesUploaderAttrCC").click();
        }
        $timeout(function () {
            StrartUpload_UploaderAttrCC();
        }, 100);

        function StrartUpload_UploaderAttrCC() {
            $('.moxie-shim').remove();
            var uploader_Attr = new plupload.Uploader({
                runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                browse_button: 'pickfilesUploaderAttrCC',
                container: 'filescontainerfrCC',
                max_file_size: '10000mb',
                flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                url: 'Handlers/UploadHandler.ashx?Type=Attachment',
                chunk_size: '64Kb',
                multi_selection: false,
                multipart_params: {}
            });
            uploader_Attr.bind('Init', function (up, params) { });
            uploader_Attr.init();
            uploader_Attr.bind('FilesAdded', function (up, files) {
                up.refresh();
                uploader_Attr.start();
            });
            uploader_Attr.bind('UploadProgress', function (up, file) { });
            uploader_Attr.bind('Error', function (up, err) {
                bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                up.refresh();
            });
            uploader_Attr.bind('FileUploaded', function (up, file, response) {
                SaveFileDetails(file, response.response);
            });
            uploader_Attr.bind('BeforeUpload', function (up, file) {
                $.extend(up.settings.multipart_params, {
                    id: file.id,
                    size: file.size
                });
            });
        }

        function SaveFileDetails(file, response) {
            var extension = file.name.substring(file.name.lastIndexOf("."))
            var resultArr = response.split(",");
            var uplaodImageObject = {};
            uplaodImageObject.Name = file.name;
            uplaodImageObject.VersionNo = 1;
            uplaodImageObject.MimeType = resultArr[1];
            uplaodImageObject.Extension = extension;
            uplaodImageObject.OwnerID = $cookies['UserId'];
            uplaodImageObject.CreatedOn = Date.now();
            uplaodImageObject.Checksum = "";
            uplaodImageObject.ModuleID = 1;
            uplaodImageObject.EntityID = parseInt($stateParams.ID, 10);
            uplaodImageObject.AttributeID = $scope.UploadAttributeId;
            uplaodImageObject.Size = file.size;
            uplaodImageObject.FileName = resultArr[0] + extension;
            var PreviewID = "UploaderPreview_" + $scope.UploadAttributeId;
            CcoverviewService.UpdateImageName(uplaodImageObject).then(function (uplaodImageObjectResult) {
                if (uplaodImageObjectResult.Response != 0) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4808.Caption'));
                    $('#' + PreviewID).attr('src', 'UploadedImages/' + uplaodImageObject.FileName);
                }
            });
        }
        $scope.ChangeCostCentreCurrencyType = function () {
            $scope.SelectedCostCentreCurrency.Name = $.grep($scope.CurrencyFormatsList, function (e) {
                return e.Id == $scope.SelectedCostCentreCurrency.TypeId;
            })[0].Name;
            $scope.SelectedCostCentreCurrency.ShortName = $.grep($scope.CurrencyFormatsList, function (e) {
                return e.Id == $scope.SelectedCostCentreCurrency.TypeId;
            })[0].ShortName;
            CcoverviewService.GetCostCentreCurrencyRateById(0, $scope.SelectedCostCentreCurrency.TypeId, false).then(function (resCurrencyRate) {
                if (resCurrencyRate.Response != null) {
                    $scope.SelectedCostCentreCurrency.Rate = parseFloat(resCurrencyRate.Response[1]);
                    $scope.LoadFinancialSummanyBlock($stateParams.ID);
                }
            });
        }
        $scope.changeduedate_changed = function (duedate, ID) {
            if (duedate != null) {
                var test = isValidDate(duedate.toString(), $scope.format.toString());
                if (test) {
                    var a = $.grep($scope.tempholidays, function (e) {
                        return e == dateFormat(duedate, $scope.format);
                    });
                    if (a != null) {
                        if (a.length > 0) {
                            bootbox.alert("Selected date should not same as non businessday");
                            $scope.fields["DatePart_" + ID] = "";
                        }
                    }
                } else {
                    $scope.fields["DatePart_" + ID] = "";
                    bootbox.alert("Select valid duedate");
                }
            }
        }
        $scope.changeperioddate_changed = function (date, datetype) {
            if (date != null) {
                var test = isValidDate(date.toString(), $scope.format.toString());
                if (test) {
                    var a = $.grep($scope.tempholidays, function (e) {
                        return e == dateFormat(date, $scope.format);
                    });
                    if (a != null) {
                        if (a.length > 0) {
                            bootbox.alert("Selected date should not same as non businessday");
                            if (datetype == "StartDate") $scope.items[0].startDate = "";
                            else $scope.item[0].endDate = "";
                        }
                    }
                } else {
                    if (datetype == "StartDate") $scope.items[0].startDate = "";
                    else $scope.item[0].endDate = "";
                    bootbox.alert("Select valid duedate");
                }
            }
        }

        function isValidDate(dateval, dateformat) {
            var formatlen;
            var defaultdateVal = [];
            defaultdateVal = dateval.length;
            formatlen = dateformat.length;
            if (formatlen == defaultdateVal || defaultdateVal > formatlen) return true;
            else return false;
        };

        $scope.settxtColor = function (hexcolor) {
            var r = parseInt(hexcolor.substr(0, 2), 16);
            var g = parseInt(hexcolor.substr(2, 2), 16);
            var b = parseInt(hexcolor.substr(4, 2), 16);
            var yiq = ((r * 299) + (g * 587) + (b * 114)) / 1000;
            return (yiq >= 160) ? 'black' : 'white';
        }

        $scope.overallstatusformat = function (data) {
            var fontColor = 'blue';
            var txtcolor = 'white';
            $("#select2-drop").addClass("bgDropdown");
            if (data.id != "" || data.text != "") {
                for (var i = 0; i < $scope.EntityStatusResult.length; i++) {
                    var res = $scope.EntityStatusResult[i];
                    if (res.ID == data.id) {
                        var stausoption = res.StatusOptions;
                        var colorcode = res.ColorCode;
                        if (colorcode == null || colorcode.trim() == "")
                            txtcolor = $scope.settxtColor("ffffff");
                        else
                            txtcolor = $scope.settxtColor(colorcode);
                        break;
                    }

                }
            }
            else {
                return "<div class='select2-chosen-label' style='background-color:#" + fontColor + "; color:" + txtcolor + "'>" + data.text + "</div>";
            }

            return "<div class='select2-chosen-label'style='background-color:#" + colorcode + "; color:" + txtcolor + "'>" + stausoption + "</div>";
        };

        $scope.select2Config = {
            formatResult: $scope.overallstatusformat,
            formatSelection: $scope.overallstatusformat
        };

        $scope.$on("$destroy", function () {
            $timeout.cancel(NewsFeedUniqueTimer);
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.costcentre.detail.overviewCtrl']"));
        });
    }
    app.controller("mui.planningtool.costcentre.detail.overviewCtrl", ['$scope', '$timeout', '$http', '$compile', '$resource', '$cookies', '$stateParams', '$location', '$window', '$translate', 'CcoverviewService', '$modal', muiplanningtoolcostcentredetailoverviewCtrl]);
})(angular, app);
///#source 1 1 /app/services/ccoverview-service.js
(function(ng,app){"use strict";function CcoverviewService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({GetAttributeToAttributeRelationsByIDForEntityInOverView:GetAttributeToAttributeRelationsByIDForEntityInOverView,SaveDetailBlockForLevels:SaveDetailBlockForLevels,SaveDetailBlock:SaveDetailBlock,SaveDetailBlockForTreeLevels:SaveDetailBlockForTreeLevels,GetAttributeTreeNodeByEntityID:GetAttributeTreeNodeByEntityID,FetchEntityStatusTree:FetchEntityStatusTree,GetFeedFilter:GetFeedFilter,PostFeed:PostFeed,GetEnityFeeds:GetEnityFeeds,GetLastEntityFeeds:GetLastEntityFeeds,InsertFeedComment:InsertFeedComment,IsActiveEntity:IsActiveEntity,UpdateEntityActiveStatus:UpdateEntityActiveStatus,GetMember:GetMember,GetCostCenterRelatedDataOnLoad:GetCostCenterRelatedDataOnLoad,GetCostCenterRelatedDataOnLoad_Set2:GetCostCenterRelatedDataOnLoad_Set2,GettingCostcentreFinancialOverview:GettingCostcentreFinancialOverview,GetEntityAttributesDetails:GetEntityAttributesDetails,updateOverviewStatus:updateOverviewStatus,UpdateEntityStatus:UpdateEntityStatus,UpdateImageName:UpdateImageName,GetCostCentreCurrencyRateById:GetCostCentreCurrencyRateById,copyuploadedImage:copyuploadedImage});function GetAttributeToAttributeRelationsByIDForEntityInOverView(EntityTypeID,EntityID){var request=$http({method:"get",url:"api/Metadata/GetAttributeToAttributeRelationsByIDForEntityInOverView/"+EntityTypeID+"/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function SaveDetailBlockForLevels(jobj){var request=$http({method:"post",url:"api/Metadata/SaveDetailBlockForLevels/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function SaveDetailBlock(formobj){var request=$http({method:"post",url:"api/Metadata/SaveDetailBlock/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function SaveDetailBlockForTreeLevels(formobj){var request=$http({method:"post",url:"api/Metadata/SaveDetailBlockForTreeLevels/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetAttributeTreeNodeByEntityID(AttributeID,EntityID){var request=$http({method:"get",url:"api/Metadata/GetAttributeTreeNodeByEntityID/"+AttributeID+"/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function FetchEntityStatusTree(EntityID){var request=$http({method:"get",url:"api/Metadata/FetchEntityStatusTree/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetFeedFilter(){var request=$http({method:"get",url:"api/common/GetFeedFilter/",params:{action:"get",}});return(request.then(handleSuccess,handleError));}
function PostFeed(addnewsfeed){var request=$http({method:"post",url:"api/common/PostFeed/",params:{action:"add",},data:addnewsfeed});return(request.then(handleSuccess,handleError));}
function GetEnityFeeds(EntityID,pageNo,Feedsgroupid){var request=$http({method:"get",url:"api/common/GetEnityFeeds/"+EntityID+"/"+pageNo+"/"+Feedsgroupid,params:{action:"get",}});return(request.then(handleSuccess,handleError));}
function GetLastEntityFeeds(EntityID,Feedsgroupid){var request=$http({method:"get",ignoreLoadingBar:true,url:"api/common/GetLastEntityFeeds/"+EntityID+"/"+Feedsgroupid,params:{action:"get",}});return(request.then(handleSuccess,handleError));}
function InsertFeedComment(addfeedcomment){var request=$http({method:"post",url:"api/common/InsertFeedComment/",params:{action:"add",},data:addfeedcomment});return(request.then(handleSuccess,handleError));}
function IsActiveEntity(EntityID){var request=$http({method:"get",url:"api/common/IsActiveEntity/"+EntityID,params:{action:"get",}});return(request.then(handleSuccess,handleError));}
function UpdateEntityActiveStatus(dataobj){var request=$http({method:"put",url:"api/Planning/UpdateEntityActiveStatus/",params:{action:"update"},data:dataobj});return(request.then(handleSuccess,handleError));}
function GetMember(EntityID){var request=$http({method:"get",url:"api/Planning/GetMember/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetCostCenterRelatedDataOnLoad(EntityID,UserID,EntityTypeID,IsAdmin){var request=$http({method:"get",url:"api/Planning/GetCostCenterRelatedDataOnLoad/"+EntityID+"/"+UserID+"/"+EntityTypeID+"/"+IsAdmin,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetCostCenterRelatedDataOnLoad_Set2(EntityID,UserID,EntityTypeID,IsAdmin){var request=$http({method:"get",url:"api/Planning/GetCostCenterRelatedDataOnLoad_Set2/"+EntityID+"/"+UserID+"/"+EntityTypeID+"/"+IsAdmin,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GettingCostcentreFinancialOverview(EntityId){var request=$http({method:"get",url:"api/Planning/GettingCostcentreFinancialOverview/"+EntityId,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetEntityAttributesDetails(ID){var request=$http({method:"get",url:"api/Planning/GetEntityAttributesDetails/"+ID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function updateOverviewStatus(formobj){var request=$http({method:"put",url:"api/Planning/updateOverviewStatus/",params:{action:"update"},data:formobj});return(request.then(handleSuccess,handleError));}
function UpdateEntityStatus(dataobj){var request=$http({method:"put",url:"api/Planning/UpdateEntityStatus/",params:{action:"update"},data:dataobj});return(request.then(handleSuccess,handleError));}
function UpdateImageName(formobj){var request=$http({method:"post",url:"api/Planning/UpdateImageName/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetCostCentreCurrencyRateById(EntityId,CurrencyId,IsCostCentreCreation){var request=$http({method:"get",url:"api/Planning/GetCostCentreCurrencyRateById/"+EntityId+"/"+CurrencyId+"/"+IsCostCentreCreation,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function copyuploadedImage(filename) { var request = $http({ method: "post", url: "api/Planning/copyuploadedImage/", params: { action: "save" }, data: { filename: filename } }); return (request.then(handleSuccess, handleError)); }
function handleError(response) {
    if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
return($q.reject(response.data.message));}
function handleSuccess(response){response.data.StatusCode=response.status;return(response.data);}}
app.service("CcoverviewService",['$http','$q',CcoverviewService]);})(angular,app);
///#source 1 1 /app/services/ServiceJs.js
///#source 1 1 /app/services/access-service.js
(function(ng,app){"use strict";function AccessService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({InsertEntityUserRole:InsertEntityUserRole,DeleteEntityUserRole:DeleteEntityUserRole,Role:Role,PostRole:PostRole,PutRole:PutRole,DeleteRole:DeleteRole,InsertRole:InsertRole,InsertEntityACL:InsertEntityACL,GetGlobalAcl:GetGlobalAcl,CheckGlobalAccessAvaiilability:CheckGlobalAccessAvaiilability,InsertGlobalACL:InsertGlobalACL,DeleteGlobalACL:DeleteGlobalACL,GetGlobalRole:GetGlobalRole,GetGlobalRoleByID:GetGlobalRoleByID,InsertUpdateGlobalRole:InsertUpdateGlobalRole,DeleteGlobalRole:DeleteGlobalRole,InsertGlobalRoleUser:InsertGlobalRoleUser,GetGlobalRoleUserByID:GetGlobalRoleUserByID,CheckUserIsAdmin:CheckUserIsAdmin,GetUserAccessforsso:GetUserAccessforsso,InsertUpdateGlobalEntitTypeACL:InsertUpdateGlobalEntitTypeACL,GetGlobalEntityTypeAcl:GetGlobalEntityTypeAcl,DeleteGlobalEntityTypeACL:DeleteGlobalEntityTypeACL,GetAllEntityRole:GetAllEntityRole,GetEntityTypeRoleAccess:GetEntityTypeRoleAccess,InsertAssetAccess:InsertAssetAccess,GetAssetAccessByID:GetAssetAccessByID,IsGlobaAccessLiveChat:IsGlobaAccessLiveChat,GetSuperAdminModule:GetSuperAdminModule,SaveUpdateSuperAdminRoleFeatures:SaveUpdateSuperAdminRoleFeatures,DeleteGlobalByID:DeleteGlobalByID,GetSuperglobalacl:GetSuperglobalacl,GetGlobalRoleUserByRoleID:GetGlobalRoleUserByRoleID,SaveUpdateNewsfeedRoleFeatures:SaveUpdateNewsfeedRoleFeatures,SaveUpdateNotificationRoleFeatures:SaveUpdateNotificationRoleFeatures,GetSelectedFeedFilter:GetSelectedFeedFilter,GetSelectedNotificationFilter:GetSelectedNotificationFilter});function InsertEntityUserRole(jobj){var request=$http({method:"post",url:"api/access/InsertEntityUserRole/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function DeleteEntityUserRole(ID){var request=$http({method:"delete",url:"api/access/DeleteEntityUserRole/"+ID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function Role(){var request=$http({method:"get",url:"api/access/Role/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function PostRole(jobj){var request=$http({method:"post",url:"api/access/PostRole/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function PutRole(formobj){var request=$http({method:"put",url:"api/access/PutRole/",params:{action:"update"},data:formobj});return(request.then(handleSuccess,handleError));}
function DeleteRole(ID){var request=$http({method:"delete",url:"api/access/DeleteRole/"+ID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function InsertRole(jobj){var request=$http({method:"post",url:"api/access/InsertRole/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function InsertEntityACL(jobj){var request=$http({method:"post",url:"api/access/InsertEntityACL/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function GetGlobalAcl(RoleID){var request=$http({method:"get",url:"api/access/GetGlobalAcl/"+RoleID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function CheckGlobalAccessAvaiilability(GlobalRole,Module,Features){var request=$http({method:"get",url:"api/access/CheckGlobalAccessAvaiilability/"+GlobalRole+"/"+Module+"/"+Features,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function InsertGlobalACL(jobj){var request=$http({method:"post",url:"api/access/InsertGlobalACL/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function DeleteGlobalACL(ID){var request=$http({method:"delete",url:"api/access/DeleteGlobalACL/"+ID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function GetGlobalRole(){var request=$http({method:"get",url:"api/access/GetGlobalRole/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetGlobalRoleByID(ID){var request=$http({method:"get",url:"api/access/GetGlobalRoleByID/"+ID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function InsertUpdateGlobalRole(jobj){var request=$http({method:"post",url:"api/access/InsertUpdateGlobalRole/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function DeleteGlobalRole(ID){var request=$http({method:"delete",url:"api/access/DeleteGlobalRole/"+ID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function InsertGlobalRoleUser(jobj){var request=$http({method:"post",url:"api/access/InsertGlobalRoleUser/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function GetGlobalRoleUserByID(ID){var request=$http({method:"get",url:"api/access/GetGlobalRoleUserByID/"+ID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function CheckUserIsAdmin(){var request=$http({method:"get",url:"api/access/CheckUserIsAdmin/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetUserAccessforsso(){var request=$http({method:"get",url:"api/access/GetUserAccessforsso/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function InsertUpdateGlobalEntitTypeACL(jobj){var request=$http({method:"post",url:"api/access/InsertUpdateGlobalEntitTypeACL/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function GetGlobalEntityTypeAcl(RoleID,moduleID){var request=$http({method:"get",url:"api/access/GetGlobalEntityTypeAcl/"+RoleID+"/"+moduleID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function DeleteGlobalEntityTypeACL(ID){var request=$http({method:"delete",url:"api/access/DeleteGlobalEntityTypeACL/"+ID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function GetAllEntityRole(){var request=$http({method:"get",url:"api/access/GetAllEntityRole/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetEntityTypeRoleAccess(EntityTypeID){var request=$http({method:"get",url:"api/access/GetEntityTypeRoleAccess/"+EntityTypeID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function InsertAssetAccess(jobj){var request=$http({method:"post",url:"api/access/InsertAssetAccess/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function GetAssetAccessByID(ID){var request=$http({method:"get",url:"api/access/GetAssetAccessByID/"+ID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function IsGlobaAccessLiveChat(){var request=$http({method:"get",url:"api/access/IsGlobaAccessLiveChat/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetSuperAdminModule(Id){var request=$http({method:"get",url:"api/access/GetSuperAdminModule/"+Id,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function SaveUpdateSuperAdminRoleFeatures(jobj){var request=$http({method:"post",url:"api/access/SaveUpdateSuperAdminRoleFeatures/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function DeleteGlobalByID(ID,Menu){var request=$http({method:"delete",url:"api/access/DeleteGlobalByID/"+ID+"/"+Menu,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function GetSuperglobalacl(){var request=$http({method:"get",url:"api/access/GetSuperglobalacl/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetGlobalRoleUserByRoleID(TaskIds){var request=$http({method:"post",url:"api/access/GetGlobalRoleUserByRoleID/",params:{action:"add"},data:TaskIds});return(request.then(handleSuccess,handleError));}
function SaveUpdateNewsfeedRoleFeatures(formobj){var request=$http({method:"post",url:"api/access/SaveUpdateNewsfeedRoleFeatures/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function SaveUpdateNotificationRoleFeatures(formobj){var request=$http({method:"post",url:"api/access/SaveUpdateNotificationRoleFeatures/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetSelectedFeedFilter(GlobalRoleID){var request=$http({method:"get",url:"api/access/GetSelectedFeedFilter/"+GlobalRoleID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetSelectedNotificationFilter(GlobalRoleID){var request=$http({method:"get",url:"api/access/GetSelectedNotificationFilter/"+GlobalRoleID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}}
app.service("AccessService",['$http','$q',AccessService]);})(angular,app);
///#source 1 1 /app/services/planning-service.js
(function (ng, app) {
    function PlanningService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({ GetDAMViewSettings: GetDAMViewSettings, GetMilestoneById: GetMilestoneById, DeleteMileStone: DeleteMileStone, InsertCostCenter: InsertCostCenter, DeleteCostCenter: DeleteCostCenter, InsertFundingRequest: InsertFundingRequest, UpdateFundingRequest: UpdateFundingRequest, DeleteFundingRequest: DeleteFundingRequest, GetfundingRequestsByEntityID: GetfundingRequestsByEntityID, EntityPlannedAmountInsert: EntityPlannedAmountInsert, EntityApprovedPlannedAmountInsert: EntityApprovedPlannedAmountInsert, ReleaseFund: ReleaseFund, UpdateFundRequestStatus: UpdateFundRequestStatus, ApprovePlannedAmountUpdate: ApprovePlannedAmountUpdate, AdjustApprovePlannedAmount: AdjustApprovePlannedAmount, EntityMoneyTransfer: EntityMoneyTransfer, GetObjectiveById: GetObjectiveById, UpdateObjective: UpdateObjective, DeleteObjective1: DeleteObjective1, InsertUnits: InsertUnits, GetUnitsById: GetUnitsById, GetEntityAttributesDetails: GetEntityAttributesDetails, DeleteUnits: DeleteUnits, UpdateUnits: UpdateUnits, InsertRatings: InsertRatings, GetRatingsById: GetRatingsById, DeleteRatings: DeleteRatings, GetPresentationById: GetPresentationById, InsertPresentation: InsertPresentation, UpdatePresentation: UpdatePresentation, InsertPublishThisLevel: InsertPublishThisLevel, InsertAttachments: InsertAttachments, GetAttachmentsById: GetAttachmentsById, DeleteAttachments: DeleteAttachments, InsertEntityPeriod: InsertEntityPeriod, GetEntityPeriodById: GetEntityPeriodById, UpdateEntityPeriod: UpdateEntityPeriod, UpdateEntityPeriodUsingPopUp: UpdateEntityPeriodUsingPopUp, DeleteEntityPeriod: DeleteEntityPeriod, InsertEntityColorCode: InsertEntityColorCode, GetEntityColorCodeById: GetEntityColorCodeById, UpdateEntityColorCode: UpdateEntityColorCode, DeleteActivityReleation: DeleteActivityReleation, DeleteEntityColorCode: DeleteEntityColorCode, CreateMilestone: CreateMilestone, UpdateMilestone: UpdateMilestone, CreateObjective: CreateObjective, SelectEntityByID: SelectEntityByID, SelectAllchildEntityObject: SelectAllchildEntityObject, SelectObjectiveByID: SelectObjectiveByID, CreateCostcentre: CreateCostcentre, GetCostcentre: GetCostcentre, GetCostcentreforEntityCreation: GetCostcentreforEntityCreation, GetGlobalMembers: GetGlobalMembers, GetCostcentreforFinancial: GetCostcentreforFinancial, AddCostCenterForFinancial: AddCostCenterForFinancial, GetEntityFinancialdDetail: GetEntityFinancialdDetail, GetCostcenterBeforeApprovalAmountDetails: GetCostcenterBeforeApprovalAmountDetails, DeleteCostcentreentity: DeleteCostcentreentity, DeleteCostcentreFinancial: DeleteCostcentreFinancial, UpdateCostcentre: UpdateCostcentre, DeleteObjective: DeleteObjective, Objectiveentityvalues: Objectiveentityvalues, ObjectiveConditionvalues: ObjectiveConditionvalues, GetEntitydescendants: GetEntitydescendants, CreateEntity: CreateEntity, CreateFundingRequest: CreateFundingRequest, UpdateEntity: UpdateEntity, DeleteEntity: DeleteEntity, InsertEntityAttributes: InsertEntityAttributes, GetEntityAttributes: GetEntityAttributes, UpdateAttributeData: UpdateAttributeData, Get_EntityIDs: Get_EntityIDs, GetChildTreeNodes: GetChildTreeNodes, GetParentTreeNodes: GetParentTreeNodes, InsertFilterSettings: InsertFilterSettings, GetFilterSettings: GetFilterSettings, GetFilterSettingsForDetail: GetFilterSettingsForDetail, GetFilterSettingValuesByFilertId: GetFilterSettingValuesByFilertId, GetMember: GetMember, GetFundrequestTaskMember: GetFundrequestTaskMember, PutMember: PutMember, UpdateMemberInEntity: UpdateMemberInEntity, Member: Member, PostMember: PostMember, GetMilestoneMetadata: GetMilestoneMetadata, GetMilestoneforWidget: GetMilestoneforWidget, DeleteFilterSettings: DeleteFilterSettings, GetEntityPeriod: GetEntityPeriod, PostEntityPeriod1: PostEntityPeriod1, EntityPeriod: EntityPeriod, PostEntityPeriod: PostEntityPeriod, DeleteRootCostcentre: DeleteRootCostcentre, GettingCostcentreFinancialOverview: GettingCostcentreFinancialOverview, UpdateTotalAssignedAmount: UpdateTotalAssignedAmount, GettingObjectiveUnits: GettingObjectiveUnits, AddEntity: AddEntity, UpdateEntityforSearch: UpdateEntityforSearch, RemoveEntity: RemoveEntity, QuickSearch: QuickSearch, QuickSearch1: QuickSearch1, UpdateSearchEngine: UpdateSearchEngine, Search: Search, GettingObjectiveSummaryBlockDetails: GettingObjectiveSummaryBlockDetails, GettingObjectiveFulfillmentBlockDetails: GettingObjectiveFulfillmentBlockDetails, DuplicateEntities: DuplicateEntities, InsertAdditionalObjective: InsertAdditionalObjective, GettingObjectivestoEntitySelect: GettingObjectivestoEntitySelect, UpdateObjectiveSummaryBlockData: UpdateObjectiveSummaryBlockData, GettingEditObjectiveFulfillmentDetails: GettingEditObjectiveFulfillmentDetails, GettingEntityPredefineObjectives: GettingEntityPredefineObjectives, InsertPredefineObjectivesforEntity: InsertPredefineObjectivesforEntity, GettingPredefineObjectivesForEntityMetadata: GettingPredefineObjectivesForEntityMetadata, LoadPredefineObjectives: LoadPredefineObjectives, UpdatePredefineObjectivesforEntity: UpdatePredefineObjectivesforEntity, GettingAddtionalObjectives: GettingAddtionalObjectives, UpdatingObjectiveOverDetails: UpdatingObjectiveOverDetails, UpdateAdditionalObjectivesforEntity: UpdateAdditionalObjectivesforEntity, UpdateObjectiveFulfillmentCondition: UpdateObjectiveFulfillmentCondition, DeleteObjectiveFulfillment: DeleteObjectiveFulfillment, GettingAdditionalObjRatings: GettingAdditionalObjRatings, GettingPredefineObjRatings: GettingPredefineObjRatings, UpdateImageName: UpdateImageName, EntityForecastAmountInsert: EntityForecastAmountInsert, GetForeCastForCCDetl: GetForeCastForCCDetl, EntityForecastAmountUpdate: EntityForecastAmountUpdate, FundRequestByID: FundRequestByID, GetAllWorkFlowStepsWithTasks: GetAllWorkFlowStepsWithTasks, InsertTaskWithAttachments: InsertTaskWithAttachments, GetWorkFlowTaskDetails: GetWorkFlowTaskDetails, UpdatingMilestoneStatus: UpdatingMilestoneStatus, UpdateEntityActiveStatus: UpdateEntityActiveStatus, UpdateTaskStatus: UpdateTaskStatus, UpdateFundingReqStatus: UpdateFundingReqStatus, UpdatePredefineObjectiveinLineData: UpdatePredefineObjectiveinLineData, UpdateAdditionalObjectiveinLineData: UpdateAdditionalObjectiveinLineData, UpdateUnassignedTaskStatus: UpdateUnassignedTaskStatus, GetWorkFlowSummary: GetWorkFlowSummary, DeleteCostcentre: DeleteCostcentre, DeleteActivityPredefineObjective: DeleteActivityPredefineObjective, DeleteAdditionalObjective: DeleteAdditionalObjective, EntityOwnersList: EntityOwnersList, UpdateObjectiveOwner: UpdateObjectiveOwner, InsertTaskMembers: InsertTaskMembers, InsertTaskAttachments: InsertTaskAttachments, GetTaskAttachmentFile: GetTaskAttachmentFile, DeleteFileByID: DeleteFileByID, UpdateObjectivestatus: UpdateObjectivestatus, financialcostcentrestatus: financialcostcentrestatus, GetEntitiPeriodByIdForGantt: GetEntitiPeriodByIdForGantt, GetPeriodByIdForGantt: GetPeriodByIdForGantt, GetEntitysLinkedToCostCenter: GetEntitysLinkedToCostCenter, DuplicateRootLevelEntities: DuplicateRootLevelEntities, GetFundRequestTaskDetails: GetFundRequestTaskDetails, GetNewsfeedFundRequestTaskDetails: GetNewsfeedFundRequestTaskDetails, GetMilestoneByEntityID: GetMilestoneByEntityID, PendingFundRequest: PendingFundRequest, UpdateLock: UpdateLock, IsLockAvailable: IsLockAvailable, UpdateCostCentreApprovedBudget: UpdateCostCentreApprovedBudget, EnableDisableWorkFlow: EnableDisableWorkFlow, GetApprovedBudgetDate: GetApprovedBudgetDate, GetAllPurchaseOrdersByEntityID: GetAllPurchaseOrdersByEntityID, CreateNewPurchaseOrder: CreateNewPurchaseOrder, GetAllCurrencyType: GetAllCurrencyType, GetAllSupplier: GetAllSupplier, GetCurrencyListFFsettings: GetCurrencyListFFsettings, InsertUpdateCurrencyListFFSettings: InsertUpdateCurrencyListFFSettings, DeleteCurrencyListFFSettings: DeleteCurrencyListFFSettings, getDivisonIds: getDivisonIds, SetDivisonsFFSettings: SetDivisonsFFSettings, GetDivisonName: GetDivisonName, EnableDisableWorkFlowStatus: EnableDisableWorkFlowStatus, WorkFlowTaskCount: WorkFlowTaskCount, ApprovePurchaseOrders: ApprovePurchaseOrders, SendPurchaseOrders: SendPurchaseOrders, RejectPurchaseOrders: RejectPurchaseOrders, CreateNewSupplier: CreateNewSupplier, GetAllInvoiceByEntityID: GetAllInvoiceByEntityID, GetAllSentPurchaseOrdersByEntityID: GetAllSentPurchaseOrdersByEntityID, CreateNewInvoice: CreateNewInvoice, CreateInvoiceAndPurchaseOrder: CreateInvoiceAndPurchaseOrder, UpdatePurchaseOrder: UpdatePurchaseOrder, PeriodAvailability: PeriodAvailability, GetPlanningTransactionsByEID: GetPlanningTransactionsByEID, DeletePlanTransactions: DeletePlanTransactions, GetEntityAttributesValidationDetails: GetEntityAttributesValidationDetails, GetEntityStatusByEntityID: GetEntityStatusByEntityID, UpdateEntityStatus: UpdateEntityStatus, CheckForMemberAvailabilityForEntity: CheckForMemberAvailabilityForEntity, CreateAttributeGroupRecord: CreateAttributeGroupRecord, GetEntityRelatedDataOnLoad: GetEntityRelatedDataOnLoad, GetEntityRelatedDataOnLoadSet1: GetEntityRelatedDataOnLoadSet1, GetDataForBreadcrumLoadWithLocalWorkSpacePath: GetDataForBreadcrumLoadWithLocalWorkSpacePath, GetDataForBreadcrumLoadWithPath: GetDataForBreadcrumLoadWithPath, GetEntityRelatedDataOnLoad_Set2: GetEntityRelatedDataOnLoad_Set2, GetCostCenterRelatedDataOnLoad_Set2: GetCostCenterRelatedDataOnLoad_Set2, GetObjectiveRelatedDataOnLoad: GetObjectiveRelatedDataOnLoad, GetObjectiveRelatedDataOnLoadSet2: GetObjectiveRelatedDataOnLoadSet2, DeleteEntityAttributeGroupRecord: DeleteEntityAttributeGroupRecord, SaveUploaderImage: SaveUploaderImage, FilterSettingsLoad: FilterSettingsLoad, GetEntityAttributesDetailsUserDetails: GetEntityAttributesDetailsUserDetails, GetCurrentDivisionId: GetCurrentDivisionId, GetFundingCostcenterMetadata: GetFundingCostcenterMetadata, Qtipservicedata: Qtipservicedata, SaveFinancialDynamicValues: SaveFinancialDynamicValues, GetCaptionofPeriod: GetCaptionofPeriod, MemberAvailable: MemberAvailable, GetFinancialforecastData: GetFinancialforecastData, UpdateFFData: UpdateFFData, GetLockStatus: GetLockStatus, GetEntityDetailsByID: GetEntityDetailsByID, InsertUpdateEntityPeriodLst: InsertUpdateEntityPeriodLst, GetAttachmentEditFeature: GetAttachmentEditFeature, updateOverviewStatus: updateOverviewStatus, CreateCalender: CreateCalender, GetEntitiesfrCalender: GetEntitiesfrCalender, GetCalenders: GetCalenders, GetEntitiesforSelectedCalender: GetEntitiesforSelectedCalender, GettingCalenderFulfillmentBlockDetail: GettingCalenderFulfillmentBlockDetail, GettingEditCalenderFulfillmentDetails: GettingEditCalenderFulfillmentDetails, UpdateCalenderFulfillmentCondition: UpdateCalenderFulfillmentCondition, UpdatingCalenderOverDetails: UpdatingCalenderOverDetails, SaveCalenderDetails: SaveCalenderDetails, PerformViewMoreSearch: PerformViewMoreSearch, GetCalendarDetailsbyExternalID: GetCalendarDetailsbyExternalID, CreateCmsPageEntity: CreateCmsPageEntity, getfinancialForecastIds: getfinancialForecastIds, updatefinancialforecastsettings: updatefinancialforecastsettings, GetFinancialForecastsettings: GetFinancialForecastsettings, GetEntityFinancialdForecastHeadings: GetEntityFinancialdForecastHeadings, GetlastUpdatedtime: GetlastUpdatedtime, GetPeriodAvailability: GetPeriodAvailability, GetCostCenterRelatedDataOnLoad: GetCostCenterRelatedDataOnLoad, getSearchSnippetSourceData: getSearchSnippetSourceData, CallbackSnippetSearch: CallbackSnippetSearch, GetCostcentreTreeforPlanCreation: GetCostcentreTreeforPlanCreation, GetCostCentreAssignedAmount: GetCostCentreAssignedAmount, GetCostCentreCurrencyRateById: GetCostCentreCurrencyRateById, DeleteApprovedAlloc: DeleteApprovedAlloc, GetApprvdAlloc: GetApprvdAlloc, InsertUpdateFinancialTrancation: InsertUpdateFinancialTrancation, GetTaskMinMaxValue: GetTaskMinMaxValue, GetDirectEntityMember: GetDirectEntityMember, UpdateTotalAssigAmtInFinancialTransaction: UpdateTotalAssigAmtInFinancialTransaction, GetDynamicfinancialattributes: GetDynamicfinancialattributes, GetObjectiveUnitsOptionValues: GetObjectiveUnitsOptionValues }); function GetMilestoneById(ID) { var request = $http({ method: "get", url: "api/Planning/GetMilestoneById/" + ID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function DeleteMileStone(ID) { var request = $http({ method: "delete", url: "api/Planning/DeleteMileStone/" + ID, params: { action: "delete" } }); return (request.then(handleSuccess, handleError)); }
        function InsertCostCenter(formobj) { var request = $http({ method: "post", url: "api/Planning/InsertCostCenter/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function DeleteCostCenter(ID) { var request = $http({ method: "delete", url: "api/Planning/DeleteCostCenter/" + ID, params: { action: "delete" } }); return (request.then(handleSuccess, handleError)); }
        function InsertFundingRequest(formobj) { var request = $http({ method: "post", url: "api/Planning/InsertFundingRequest/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function UpdateFundingRequest(formobj) { var request = $http({ method: "put", url: "api/Planning/UpdateFundingRequest/", params: { action: "update" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function DeleteFundingRequest(formobj) { var request = $http({ method: "delete", url: "api/Planning/DeleteFundingRequest/", params: { action: "delete" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetfundingRequestsByEntityID(EntityID) { var request = $http({ method: "get", url: "api/Planning/GetfundingRequestsByEntityID/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function EntityPlannedAmountInsert(formobj) { var request = $http({ method: "post", url: "api/Planning/EntityPlannedAmountInsert/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function EntityApprovedPlannedAmountInsert(formobj) { var request = $http({ method: "post", url: "api/Planning/EntityApprovedPlannedAmountInsert/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function ReleaseFund(formobj) { var request = $http({ method: "put", url: "api/Planning/ReleaseFund/", params: { action: "update" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function UpdateFundRequestStatus(EntityID, CostCenterID, FundRequestID, Status) { var request = $http({ method: "put", url: "api/Planning/UpdateFundRequestStatus/" + EntityID + "/" + CostcenterID + "/" + FundRequestID + "/" + Status, params: { action: "update" } }); return (request.then(handleSuccess, handleError)); }
        function ApprovePlannedAmountUpdate(formobj) { var request = $http({ method: "put", url: "api/Planning/ApprovePlannedAmountUpdate/", params: { action: "update" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function AdjustApprovePlannedAmount(EntityID) { var request = $http({ method: "put", url: "api/Planning/AdjustApprovePlannedAmount/" + EntityID, params: { action: "update" } }); return (request.then(handleSuccess, handleError)); }
        function EntityMoneyTransfer(formobj) { var request = $http({ method: "put", url: "api/Planning/EntityMoneyTransfer/", params: { action: "update" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetObjectiveById(ID) { var request = $http({ method: "get", url: "api/Planning/GetObjectiveById/" + ID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function UpdateObjective(formobj) { var request = $http({ method: "put", url: "api/Planning/UpdateObjective/", params: { action: "update" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function DeleteObjective1(ID) { var request = $http({ method: "delete", url: "api/Planning/DeleteObjective1/" + ID, params: { action: "delete" } }); return (request.then(handleSuccess, handleError)); }
        function InsertUnits(formobj) { var request = $http({ method: "post", url: "api/Planning/InsertUnits/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetUnitsById(ID) { var request = $http({ method: "get", url: "api/Planning/GetUnitsById/" + ID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetEntityAttributesDetails(ID) { var request = $http({ method: "get", url: "api/Planning/GetEntityAttributesDetails/" + ID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function DeleteUnits(formobj) { var request = $http({ method: "delete", url: "api/Planning/DeleteUnits/", params: { action: "delete" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function UpdateUnits(formobj) { var request = $http({ method: "put", url: "api/Planning/UpdateUnits/", params: { action: "update" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function InsertRatings(formobj) { var request = $http({ method: "post", url: "api/Planning/InsertRatings/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetRatingsById(ID) { var request = $http({ method: "get", url: "api/Planning/GetRatingsById/" + ID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function DeleteRatings(formobj) { var request = $http({ method: "delete", url: "api/Planning/DeleteRatings/", params: { action: "delete" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetPresentationById(ID) { var request = $http({ method: "get", url: "api/Planning/GetPresentationById/" + ID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function InsertPresentation(formobj) { var request = $http({ method: "post", url: "api/Planning/InsertPresentation/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function UpdatePresentation(formobj) { var request = $http({ method: "put", url: "api/Planning/UpdatePresentation/", params: { action: "update" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function InsertPublishThisLevel(jobj) { var request = $http({ method: "post", url: "api/Planning/InsertPublishThisLevel/", params: { action: "add" }, data: jobj }); return (request.then(handleSuccess, handleError)); }
        function InsertAttachments(formobj) { var request = $http({ method: "post", url: "api/Planning/InsertAttachments/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetAttachmentsById(ID) { var request = $http({ method: "get", url: "api/Planning/GetAttachmentsById/" + ID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function DeleteAttachments(formobj) { var request = $http({ method: "delete", url: "api/Planning/DeleteAttachments/", params: { action: "delete" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function InsertEntityPeriod(formobj) { var request = $http({ method: "post", url: "api/Planning/InsertEntityPeriod/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetEntityPeriodById(ID) { var request = $http({ method: "get", url: "api/Planning/GetEntityPeriodById/" + ID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function UpdateEntityPeriod(formobj) { var request = $http({ method: "put", url: "api/Planning/UpdateEntityPeriod/", params: { action: "update" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function UpdateEntityPeriodUsingPopUp(formobj) { var request = $http({ method: "put", url: "api/Planning/UpdateEntityPeriodUsingPopUp/", params: { action: "update" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function DeleteEntityPeriod(ID) { var request = $http({ method: "delete", url: "api/Planning/DeleteEntityPeriod/" + ID, params: { action: "delete" } }); return (request.then(handleSuccess, handleError)); }
        function InsertEntityColorCode(formobj) { var request = $http({ method: "post", url: "api/Planning/InsertEntityColorCode/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetEntityColorCodeById(ID) { var request = $http({ method: "get", url: "api/Planning/GetEntityColorCodeById/" + ID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function UpdateEntityColorCode(formobj) { var request = $http({ method: "put", url: "api/Planning/UpdateEntityColorCode/", params: { action: "update" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function DeleteActivityReleation(formobj) { var request = $http({ method: "delete", url: "api/Planning/DeleteActivityReleation/", params: { action: "delete" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function DeleteEntityColorCode(formobj) { var request = $http({ method: "delete", url: "api/Planning/DeleteEntityColorCode/", params: { action: "delete" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function CreateMilestone(formobj) { var request = $http({ method: "post", url: "api/Planning/CreateMilestone/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function UpdateMilestone(formobj) { var request = $http({ method: "post", url: "api/Planning/UpdateMilestone/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function CreateObjective(formobj) { var request = $http({ method: "post", url: "api/Planning/CreateObjective/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function SelectEntityByID(ID) { var request = $http({ method: "get", url: "api/Planning/SelectEntityByID/" + ID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function SelectAllchildEntityObject(ID) { var request = $http({ method: "get", url: "api/Planning/SelectAllchildEntityObject/" + ID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function SelectObjectiveByID(ID) { var request = $http({ method: "get", url: "api/Planning/SelectObjectiveByID/" + ID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function CreateCostcentre(formobj) { var request = $http({ method: "post", url: "api/Planning/CreateCostcentre/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetCostcentre(ID) { var request = $http({ method: "get", url: "api/Planning/GetCostcentre/" + ID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetCostcentreforEntityCreation(EntityTypeID, FiscalYear, EntityID) { var request = $http({ method: "get", url: "api/Planning/GetCostcentreforEntityCreation/" + EntityTypeID + "/" + FiscalYear + "/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetGlobalMembers(EntityID) { var request = $http({ method: "get", url: "api/Planning/GetGlobalMembers/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetCostcentreforFinancial(EntityID) { var request = $http({ method: "get", url: "api/Planning/GetCostcentreforFinancial/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function AddCostCenterForFinancial(formobj) { var request = $http({ method: "post", url: "api/Planning/AddCostCenterForFinancial/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetEntityFinancialdDetail(EntityID, UserID, startRow, endRow, includedetails) { var request = $http({ method: "get", url: "api/Planning/GetEntityFinancialdDetail/" + EntityID + "/" + UserID + "/" + startRow + "/" + endRow + "/" + includedetails, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetCostcenterBeforeApprovalAmountDetails(CostCenterID, EntityID) { var request = $http({ method: "get", url: "api/Planning/GetCostcenterBeforeApprovalAmountDetails/" + CostCenterID + "/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function DeleteCostcentreentity(ID) { var request = $http({ method: "delete", url: "api/Planning/DeleteCostcentreentity/" + ID, params: { action: "delete" } }); return (request.then(handleSuccess, handleError)); }
        function DeleteCostcentreFinancial(EntityID, CostCenterID) { var request = $http({ method: "delete", url: "api/Planning/DeleteCostcentreFinancial/" + EntityID + "/" + CostCenterID, params: { action: "delete" } }); return (request.then(handleSuccess, handleError)); }
        function UpdateCostcentre(formobj) { var request = $http({ method: "put", url: "api/Planning/UpdateCostcentre/", params: { action: "update" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function DeleteObjective() { var request = $http({ method: "delete", url: "api/Planning/DeleteObjective/", params: { action: "delete" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function Objectiveentityvalues(formobj) { var request = $http({ method: "post", url: "api/Planning/Objectiveentityvalues/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function ObjectiveConditionvalues(formobj) { var request = $http({ method: "post", url: "api/Planning/ObjectiveConditionvalues/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetEntitydescendants(AttributeID) { var request = $http({ method: "get", url: "api/Planning/GetEntitydescendants/" + AttributeID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function CreateEntity(formobj) { var request = $http({ method: "post", url: "api/Planning/CreateEntity/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function CreateFundingRequest(formobj) { var request = $http({ method: "post", url: "api/Planning/CreateFundingRequest/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function UpdateEntity(formobj) { var request = $http({ method: "put", url: "api/Planning/UpdateEntity/", params: { action: "update" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function DeleteEntity(formobj) { var request = $http({ method: "post", url: "api/Planning/DeleteEntity/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function InsertEntityAttributes(formobj) { var request = $http({ method: "post", url: "api/Planning/InsertEntityAttributes/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetEntityAttributes(ID) { var request = $http({ method: "get", url: "api/Planning/GetEntityAttributes/" + ID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function UpdateAttributeData(formobj) { var request = $http({ method: "post", url: "api/Planning/UpdateAttributeData/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function Get_EntityIDs(ID) { var request = $http({ method: "get", url: "api/Planning/Get_EntityIDs/" + ID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetChildTreeNodes(ParentID) { var request = $http({ method: "get", url: "api/Planning/GetChildTreeNodes/" + ParentID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetParentTreeNodes(formobj) { var request = $http({ method: "post", url: "api/Planning/GetParentTreeNodes/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function InsertFilterSettings(formobj) { var request = $http({ method: "post", url: "api/Planning/InsertFilterSettings/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetFilterSettings(TypeID) { var request = $http({ method: "get", url: "api/Planning/GetFilterSettings/" + TypeID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetFilterSettingsForDetail(TypeID) { var request = $http({ method: "get", url: "api/Planning/GetFilterSettingsForDetail/" + TypeID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetFilterSettingValuesByFilertId(FilterID) { var request = $http({ method: "get", url: "api/Planning/GetFilterSettingValuesByFilertId/" + FilterID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetMember(EntityID) { var request = $http({ method: "get", url: "api/Planning/GetMember/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetFundrequestTaskMember(EntityID) { var request = $http({ method: "get", url: "api/Planning/GetFundrequestTaskMember/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function PutMember(dataobj) { var request = $http({ method: "put", url: "api/Planning/PutMember/", params: { action: "update" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function UpdateMemberInEntity(dataobj) { var request = $http({ method: "put", url: "api/Planning/UpdateMemberInEntity/", params: { action: "update" }, data: dataobj }); return (request.then(handleSuccess, handleError)); }
        function Member(ID) { var request = $http({ method: "delete", url: "api/Planning/Member/" + ID, params: { action: "delete" } }); return (request.then(handleSuccess, handleError)); }
        function PostMember(dataobj) { var request = $http({ method: "post", url: "api/Planning/PostMember/", params: { action: "add" }, data: dataobj }); return (request.then(handleSuccess, handleError)); }
        function GetMilestoneMetadata(EntityID, EntityTypeId) { var request = $http({ method: "get", url: "api/Planning/GetMilestoneMetadata/" + EntityID + "/" + EntityTypeId, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetMilestoneforWidget(EntityID, EntityTypeId) { var request = $http({ method: "get", url: "api/Planning/GetMilestoneforWidget/" + EntityID + "/" + EntityTypeId, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function DeleteFilterSettings(FilterId) { var request = $http({ method: "delete", url: "api/Planning/DeleteFilterSettings/" + FilterId, params: { action: "delete" } }); return (request.then(handleSuccess, handleError)); }
        function GetEntityPeriod(EntityID) { var request = $http({ method: "get", url: "api/Planning/GetEntityPeriod/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function PostEntityPeriod1(formobj) { var request = $http({ method: "post", url: "api/Planning/PostEntityPeriod1/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function EntityPeriod(ID) { var request = $http({ method: "delete", url: "api/Planning/EntityPeriod/" + ID, params: { action: "delete" } }); return (request.then(handleSuccess, handleError)); }
        function PostEntityPeriod(formobj) { var request = $http({ method: "post", url: "api/Planning/PostEntityPeriod/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function DeleteRootCostcentre(formobj) { var request = $http({ method: "post", url: "api/Planning/DeleteRootCostcentre/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GettingCostcentreFinancialOverview(EntityId) { var request = $http({ method: "get", url: "api/Planning/GettingCostcentreFinancialOverview/" + EntityId, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function UpdateTotalAssignedAmount(formobj) { var request = $http({ method: "post", url: "api/Planning/UpdateTotalAssignedAmount/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GettingObjectiveUnits() { var request = $http({ method: "get", url: "api/Planning/GettingObjectiveUnits/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function AddEntity(EntityID, Name) { var request = $http({ method: "get", url: "api/Planning/AddEntity/" + EntityID + "/" + Name, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function UpdateEntityforSearch(EntityID, Name) { var request = $http({ method: "get", url: "api/Planning/UpdateEntityforSearch/" + EntityID + "/" + Name, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function RemoveEntity(formobj) { var request = $http({ method: "post", url: "api/Planning/RemoveEntity/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function QuickSearch(formobj) { var request = $http({ method: "post", url: "api/Planning/QuickSearch/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function QuickSearch1(formobj) { var request = $http({ method: "post", url: "api/Planning/QuickSearch1/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function UpdateSearchEngine(formobj) { var request = $http({ method: "post", url: "api/Planning/UpdateSearchEngine/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function Search(formobj) { var request = $http({ method: "post", url: "api/Planning/Search/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GettingObjectiveSummaryBlockDetails(ObjectiveId) { var request = $http({ method: "get", url: "api/Planning/GettingObjectiveSummaryBlockDetails/" + ObjectiveId, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GettingObjectiveFulfillmentBlockDetails(ObjectiveId) { var request = $http({ method: "get", url: "api/Planning/GettingObjectiveFulfillmentBlockDetails/" + ObjectiveId, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function DuplicateEntities(formobj) { var request = $http({ method: "post", url: "api/Planning/DuplicateEntities/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function InsertAdditionalObjective(formobj) { var request = $http({ method: "post", url: "api/Planning/InsertAdditionalObjective/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GettingObjectivestoEntitySelect(EntityId) { var request = $http({ method: "get", url: "api/Planning/GettingObjectivestoEntitySelect/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function UpdateObjectiveSummaryBlockData(formobj) { var request = $http({ method: "post", url: "api/Planning/UpdateObjectiveSummaryBlockData/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GettingEditObjectiveFulfillmentDetails(ObjectiveID) { var request = $http({ method: "get", url: "api/Planning/GettingEditObjectiveFulfillmentDetails/" + ObjectiveID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GettingEntityPredefineObjectives(EntityID) { var request = $http({ method: "get", url: "api/Planning/GettingEntityPredefineObjectives/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function InsertPredefineObjectivesforEntity(formobj) { var request = $http({ method: "post", url: "api/Planning/InsertPredefineObjectivesforEntity/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GettingPredefineObjectivesForEntityMetadata(formobj) { var request = $http({ method: "post", url: "api/Planning/GettingPredefineObjectivesForEntityMetadata/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function LoadPredefineObjectives(EntityId) { var request = $http({ method: "get", url: "api/Planning/LoadPredefineObjectives/" + EntityId, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function UpdatePredefineObjectivesforEntity(formobj) { var request = $http({ method: "post", url: "api/Planning/UpdatePredefineObjectivesforEntity/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GettingAddtionalObjectives(EntityID) { var request = $http({ method: "get", url: "api/Planning/GettingAddtionalObjectives/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function UpdatingObjectiveOverDetails(formobj) { var request = $http({ method: "post", url: "api/Planning/UpdatingObjectiveOverDetails/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function UpdateAdditionalObjectivesforEntity(formobj) { var request = $http({ method: "post", url: "api/Planning/UpdateAdditionalObjectivesforEntity/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function UpdateObjectiveFulfillmentCondition(formobj) { var request = $http({ method: "post", url: "api/Planning/UpdateObjectiveFulfillmentCondition/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function DeleteObjectiveFulfillment(ObjectiveID) { var request = $http({ method: "delete", url: "api/Planning/DeleteObjectiveFulfillment/" + ObjectiveID, params: { action: "delete" } }); return (request.then(handleSuccess, handleError)); }
        function GettingAdditionalObjRatings(ObjectiveID) { var request = $http({ method: "get", url: "api/Planning/GettingAdditionalObjRatings/" + ObjectiveID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GettingPredefineObjRatings(ObjectiveID) { var request = $http({ method: "get", url: "api/Planning/GettingPredefineObjRatings/" + ObjectiveID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function UpdateImageName(formobj) { var request = $http({ method: "post", url: "api/Planning/UpdateImageName/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function EntityForecastAmountInsert(formobj) { var request = $http({ method: "post", url: "api/Planning/EntityForecastAmountInsert/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetForeCastForCCDetl(CostcenterId) { var request = $http({ method: "get", url: "api/Planning/GetForeCastForCCDetl/" + CostcenterId, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function EntityForecastAmountUpdate(EntityID) { var request = $http({ method: "post", url: "api/Planning/EntityForecastAmountUpdate/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function FundRequestByID(ID, EntityID) { var request = $http({ method: "delete", url: "api/Planning/FundRequestByID/" + ID + "/" + EntityID, params: { action: "delete" } }); return (request.then(handleSuccess, handleError)); }
        function GetAllWorkFlowStepsWithTasks(EntityID) { var request = $http({ method: "get", url: "api/Planning/GetAllWorkFlowStepsWithTasks/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function InsertTaskWithAttachments(formobj) { var request = $http({ method: "post", url: "api/Planning/InsertTaskWithAttachments/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetWorkFlowTaskDetails(TaskID) { var request = $http({ method: "get", url: "api/Planning/GetWorkFlowTaskDetails/" + TaskID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function UpdatingMilestoneStatus(formobj) { var request = $http({ method: "post", url: "api/Planning/UpdatingMilestoneStatus/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function UpdateEntityActiveStatus(dataobj) { var request = $http({ method: "put", url: "api/Planning/UpdateEntityActiveStatus/", params: { action: "update" }, data: dataobj }); return (request.then(handleSuccess, handleError)); }
        function UpdateTaskStatus(EntityID, Status) { var request = $http({ method: "put", url: "api/Planning/UpdateTaskStatus/" + EntityID + "/" + Status, params: { action: "update" } }); return (request.then(handleSuccess, handleError)); }
        function UpdateFundingReqStatus(TaskID, Status, EntityID) { var request = $http({ method: "put", url: "api/Planning/UpdateFundingReqStatus/" + TaskID + "/" + Status + "/" + EntityID, params: { action: "update" } }); return (request.then(handleSuccess, handleError)); }
        function UpdatePredefineObjectiveinLineData(formobj) { var request = $http({ method: "post", url: "api/Planning/UpdatePredefineObjectiveinLineData/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function UpdateAdditionalObjectiveinLineData(formobj) { var request = $http({ method: "post", url: "api/Planning/UpdateAdditionalObjectiveinLineData/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function UpdateUnassignedTaskStatus(PredefinedTaskID, EntityID) { var request = $http({ method: "put", url: "api/Planning/UpdateUnassignedTaskStatus/" + PredefinedTaskID + "/" + EntityID, params: { action: "update" } }); return (request.then(handleSuccess, handleError)); }
        function GetWorkFlowSummary(EntityID) { var request = $http({ method: "get", url: "api/Planning/GetWorkFlowSummary/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function DeleteCostcentre(formobj) { var request = $http({ method: "post", url: "api/Planning/DeleteCostcentre/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function DeleteActivityPredefineObjective(formobj) { var request = $http({ method: "post", url: "api/Planning/DeleteActivityPredefineObjective/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function DeleteAdditionalObjective(formobj) { var request = $http({ method: "post", url: "api/Planning/DeleteAdditionalObjective/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function EntityOwnersList(EntityID) { var request = $http({ method: "get", url: "api/Planning/EntityOwnersList/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function UpdateObjectiveOwner(formobj) { var request = $http({ method: "post", url: "api/Planning/UpdateObjectiveOwner/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function InsertTaskMembers(formobj) { var request = $http({ method: "post", url: "api/Planning/InsertTaskMembers/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function InsertTaskAttachments(formobj) { var request = $http({ method: "post", url: "api/Planning/InsertTaskAttachments/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetTaskAttachmentFile(TaskID) { var request = $http({ method: "get", url: "api/Planning/GetTaskAttachmentFile/" + TaskID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function DeleteFileByID(ID) { var request = $http({ method: "delete", url: "api/Planning/DeleteFileByID/" + ID, params: { action: "delete" } }); return (request.then(handleSuccess, handleError)); }
        function UpdateObjectivestatus(formobj) { var request = $http({ method: "post", url: "api/Planning/UpdateObjectivestatus/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function financialcostcentrestatus(EntityID, CostcentreID) { var request = $http({ method: "get", url: "api/Planning/financialcostcentrestatus/" + EntityID + "/" + CostcentreID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetEntitiPeriodByIdForGantt(EntityID) { var request = $http({ method: "get", url: "api/Planning/GetEntitiPeriodByIdForGantt/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetPeriodByIdForGantt(ID) { var request = $http({ method: "get", url: "api/Planning/GetPeriodByIdForGantt/" + ID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetEntitysLinkedToCostCenter(ID) { var request = $http({ method: "get", url: "api/Planning/GetEntitysLinkedToCostCenter/" + ID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function DuplicateRootLevelEntities(formobj) { var request = $http({ method: "post", url: "api/Planning/DuplicateRootLevelEntities/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetFundRequestTaskDetails(EntityUniqueKey, CostCentreID) { var request = $http({ method: "get", url: "api/Planning/GetFundRequestTaskDetails/" + EntityUniqueKey + "/" + CostCentreID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetNewsfeedFundRequestTaskDetails(FundID) { var request = $http({ method: "get", url: "api/Planning/GetNewsfeedFundRequestTaskDetails/" + FundID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetMilestoneByEntityID(EntityID) { var request = $http({ method: "get", url: "api/Planning/GetMilestoneByEntityID/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function PendingFundRequest(EntityID) { var request = $http({ method: "get", url: "api/Planning/PendingFundRequest/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function UpdateLock(EntityID, IsLock) { var request = $http({ method: "get", url: "api/Planning/UpdateLock/" + EntityID + "/" + IsLock, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function IsLockAvailable(formobj) { var request = $http({ method: "post", url: "api/Planning/IsLockAvailable/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function UpdateCostCentreApprovedBudget(formobj) { var request = $http({ method: "post", url: "api/Planning/UpdateCostCentreApprovedBudget/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function EnableDisableWorkFlow(EntityID, IsEnableWorkflow) { var request = $http({ method: "delete", url: "api/Planning/DeletePredWorkflowFileByID/" + EntityID + "/" + IsEnableWorkflow, params: { action: "delete" } }); return (request.then(handleSuccess, handleError)); }
        function GetApprovedBudgetDate(ListId) { var request = $http({ method: "get", url: "api/Planning/GetApprovedBudgetDate/" + ListId, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetAllPurchaseOrdersByEntityID(EntityID) { var request = $http({ method: "put", url: "api/Planning/GetAllPurchaseOrdersByEntityID/" + EntityID, params: { action: "update" } }); return (request.then(handleSuccess, handleError)); }
        function CreateNewPurchaseOrder(formobj) { var request = $http({ method: "post", url: "api/Planning/CreateNewPurchaseOrder/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetAllCurrencyType() { var request = $http({ method: "get", url: "api/Planning/GetAllCurrencyType/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetAllSupplier() { var request = $http({ method: "get", url: "api/Planning/GetAllSupplier/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetCurrencyListFFsettings() { var request = $http({ method: "get", url: "api/Planning/GetCurrencyListFFsettings/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function InsertUpdateCurrencyListFFSettings(formobj) { var request = $http({ method: "post", url: "api/Planning/InsertUpdateCurrencyListFFSettings/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function DeleteCurrencyListFFSettings(ID) { var request = $http({ method: "delete", url: "api/Planning/DeleteCurrencyListFFSettings/" + ID, params: { action: "delete" } }); return (request.then(handleSuccess, handleError)); }
        function getDivisonIds() { var request = $http({ method: "get", url: "api/Planning/getDivisonIds/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function SetDivisonsFFSettings(formobj) { var request = $http({ method: "post", url: "api/Planning/SetDivisonsFFSettings/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetDivisonName() { var request = $http({ method: "get", url: "api/Planning/GetDivisonName/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function EnableDisableWorkFlowStatus(EntityID) { var request = $http({ method: "get", url: "api/Planning/EnableDisableWorkFlowStatus/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function WorkFlowTaskCount(EntityID) { var request = $http({ method: "get", url: "api/Planning/WorkFlowTaskCount/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function ApprovePurchaseOrders(formobj) { var request = $http({ method: "post", url: "api/Planning/ApprovePurchaseOrders/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function SendPurchaseOrders(formobj) { var request = $http({ method: "post", url: "api/Planning/SendPurchaseOrders/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function RejectPurchaseOrders(formobj) { var request = $http({ method: "post", url: "api/Planning/RejectPurchaseOrders/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function CreateNewSupplier(formobj) { var request = $http({ method: "post", url: "api/Planning/CreateNewSupplier/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetAllInvoiceByEntityID(EntityID) { var request = $http({ method: "get", url: "api/Planning/GetAllInvoiceByEntityID/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetAllSentPurchaseOrdersByEntityID(EntityID) { var request = $http({ method: "get", url: "api/Planning/GetAllSentPurchaseOrdersByEntityID/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function CreateNewInvoice(formobj) { var request = $http({ method: "post", url: "api/Planning/CreateNewInvoice/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function CreateInvoiceAndPurchaseOrder(formobj) { var request = $http({ method: "post", url: "api/Planning/CreateInvoiceAndPurchaseOrder/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function UpdatePurchaseOrder(formobj) { var request = $http({ method: "post", url: "api/Planning/UpdatePurchaseOrder/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function PeriodAvailability(EntityTypeID) { var request = $http({ method: "get", url: "api/Planning/PeriodAvailability/" + EntityTypeID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetPlanningTransactionsByEID(formobj) { var request = $http({ method: "post", url: "api/Planning/GetPlanningTransactionsByEID/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function DeletePlanTransactions(formobj) { var request = $http({ method: "post", url: "api/Planning/DeletePlanTransactions/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetEntityAttributesValidationDetails(ID, entityTypeID) { var request = $http({ method: "get", url: "api/Planning/GetEntityAttributesValidationDetails/" + ID + "/" + entityTypeID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetEntityStatusByEntityID(entityId) { var request = $http({ method: "get", url: "api/Planning/GetEntityStatusByEntityID/" + entityId, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function UpdateEntityStatus(dataobj) { var request = $http({ method: "put", url: "api/Planning/UpdateEntityStatus/", params: { action: "update" }, data: dataobj }); return (request.then(handleSuccess, handleError)); }
        function CheckForMemberAvailabilityForEntity(EntityID) { var request = $http({ method: "get", url: "api/Planning/CheckForMemberAvailabilityForEntity/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function CreateAttributeGroupRecord(formobj) { var request = $http({ method: "post", url: "api/Planning/CreateAttributeGroupRecord/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetEntityRelatedDataOnLoad(EntityID, UserID, EntityTypeID, IsAdmin, MilestoneEntityTypeID) { var request = $http({ method: "get", ignoreLoadingBar:true,url: "api/Planning/GetEntityRelatedDataOnLoad/" + EntityID + "/" + UserID + "/" + EntityTypeID + "/" + IsAdmin + "/" + MilestoneEntityTypeID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetEntityRelatedDataOnLoadSet1(EntityID, UserID, EntityTypeID, IsAdmin, MilestoneEntityTypeID) { var request = $http({ method: "get",ignoreLoadingBar:true, url: "api/Planning/GetEntityRelatedDataOnLoadSet1/" + EntityID + "/" + UserID + "/" + EntityTypeID + "/" + IsAdmin + "/" + MilestoneEntityTypeID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetDataForBreadcrumLoadWithLocalWorkSpacePath(EntityID, IsWorkspace) { var request = $http({ method: "get", url: "api/Planning/GetDataForBreadcrumLoadWithLocalWorkSpacePath/" + EntityID + "/" + IsWorkspace, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetDataForBreadcrumLoadWithPath(EntityID) { var request = $http({ method: "get", url: "api/Planning/GetDataForBreadcrumLoadWithPath/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetEntityRelatedDataOnLoad_Set2(EntityID, UserID, EntityTypeID, IsAdmin, MilestoneEntityTypeID) { var request = $http({ method: "get",ignoreLoadingBar:true, url: "api/Planning/GetEntityRelatedDataOnLoad_Set2/" + EntityID + "/" + UserID + "/" + EntityTypeID + "/" + IsAdmin + "/" + MilestoneEntityTypeID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetCostCenterRelatedDataOnLoad_Set2(EntityID, UserID, EntityTypeID, IsAdmin) { var request = $http({ method: "get", url: "api/Planning/GetCostCenterRelatedDataOnLoad_Set2/" + EntityID + "/" + UserID + "/" + EntityTypeID + "/" + IsAdmin, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetObjectiveRelatedDataOnLoad(ObjectiveID) { var request = $http({ method: "get", url: "api/Planning/GetObjectiveRelatedDataOnLoad/" + ObjectiveID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetObjectiveRelatedDataOnLoadSet2(ObjectiveID) { var request = $http({ method: "get", url: "api/Planning/GetObjectiveRelatedDataOnLoadSet2/" + ObjectiveID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function DeleteEntityAttributeGroupRecord(GroupID, GroupRecordID, ParentID) { var request = $http({ method: "delete", url: "api/Planning/DeleteEntityAttributeGroupRecord/" + GroupID + "/" + GroupRecordID + "/" + ParentID, params: { action: "delete" } }); return (request.then(handleSuccess, handleError)); }
        function SaveUploaderImage(formobj) { var request = $http({ method: "post", url: "api/Planning/SaveUploaderImage/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function FilterSettingsLoad(elementNode, EntityTypeID, FilterType, OptionFrom, TypeID) { var request = $http({ method: "get", url: "api/Planning/FilterSettingsLoad/" + elementNode + "/" + EntityTypeID + "/" + FilterType + "/" + OptionFrom + "/" + TypeID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetEntityAttributesDetailsUserDetails(UserID) { var request = $http({ method: "get", url: "api/Planning/GetEntityAttributesDetailsUserDetails/" + UserID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetCurrentDivisionId() { var request = $http({ method: "get", url: "api/Planning/GetCurrentDivisionId/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetFundingCostcenterMetadata(Metadatatype) { var request = $http({ method: "get", url: "api/Planning/GetFundingCostcenterMetadata/" + Metadatatype, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function Qtipservicedata(id, timeformat) { var request = $http({ method: "get", url: "api/Planning/Qtipservicedata/" + id + "/" + timeformat, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function SaveFinancialDynamicValues(formobj) { var request = $http({ method: "post", url: "api/Planning/SaveFinancialDynamicValues/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetCaptionofPeriod(EntityTypeID) { var request = $http({ method: "get", url: "api/Planning/GetCaptionofPeriod/" + EntityTypeID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function MemberAvailable(formobj) { var request = $http({ method: "post", url: "api/Planning/MemberAvailable/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetFinancialforecastData() { var request = $http({ method: "get", url: "api/Planning/GetFinancialforecastData/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function UpdateFFData(formobj) { var request = $http({ method: "post", url: "api/Planning/UpdateFFData/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetCostCenterRelatedDataOnLoad(EntityID, UserID, EntityTypeID, IsAdmin) { var request = $http({ method: "get", url: "api/Planning/GetCostCenterRelatedDataOnLoad/" + EntityID + "/" + UserID + "/" + EntityTypeID + "/" + IsAdmin, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetLockStatus(EntityID) { var request = $http({ method: "get", url: "api/Planning/GetLockStatus/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetEntityDetailsByID(EntityID) { var request = $http({ method: "get", url: "api/Planning/GetEntityDetailsByID/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function InsertUpdateEntityPeriodLst(formobj) { var request = $http({ method: "post", url: "api/Planning/InsertUpdateEntityPeriodLst/", params: { action: "add", }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetAttachmentEditFeature() { var request = $http({ method: "get", url: "api/Planning/GetAttachmentEditFeature/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function updateOverviewStatus(formobj) { var request = $http({ method: "put", url: "api/Planning/updateOverviewStatus/", params: { action: "update" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function CreateCalender(formobj) { var request = $http({ method: "post", url: "api/Planning/CreateCalender/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetEntitiesfrCalender(formobj) { var request = $http({ method: "post", url: "api/Planning/GetEntitiesfrCalender/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetCalenders() { var request = $http({ method: "get", url: "api/Planning/GetCalenders/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetEntitiesforSelectedCalender(CalID) { var request = $http({ method: "get", url: "api/Planning/GetEntitiesforSelectedCalender/" + CalID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GettingCalenderFulfillmentBlockDetail(CalenderId) { var request = $http({ method: "get", url: "api/Planning/GettingCalenderFulfillmentBlockDetail/" + CalenderId, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GettingEditCalenderFulfillmentDetails(CalenderId) { var request = $http({ method: "get", url: "api/Planning/GettingEditCalenderFulfillmentDetails/" + CalenderId, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function UpdateCalenderFulfillmentCondition(formobj) { var request = $http({ method: "post", url: "api/Planning/UpdateCalenderFulfillmentCondition/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function UpdatingCalenderOverDetails(formobj) { var request = $http({ method: "post", url: "api/Planning/UpdatingCalenderOverDetails/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function SaveCalenderDetails(formobj) { var request = $http({ method: "post", url: "api/Planning/SaveCalenderDetails/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function PerformViewMoreSearch(formobj) { var request = $http({ method: "post", url: "api/Planning/PerformViewMoreSearch/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetCalendarDetailsbyExternalID(formobj) { var request = $http({ method: "post", url: "api/Planning/GetCalendarDetailsbyExternalID/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function CreateCmsPageEntity(formobj) { var request = $http({ method: "post", url: "api/Planning/CreateCmsPageEntity/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function getfinancialForecastIds() { var request = $http({ method: "get", url: "api/Planning/getfinancialForecastIds/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function updatefinancialforecastsettings(formobj) { var request = $http({ method: "post", url: "api/Planning/updatefinancialforecastsettings/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetFinancialForecastsettings() { var request = $http({ method: "get", url: "api/Planning/GetFinancialForecastsettings/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetEntityFinancialdForecastHeadings(EntityID, DivisionID, Iscc) { var request = $http({ method: "get", url: "api/Planning/GetEntityFinancialdForecastHeadings/" + EntityID + "/" + DivisionID + "/" + Iscc, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetlastUpdatedtime(EntityID) { var request = $http({ method: "get", url: "api/Planning/GetlastUpdatedtime/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function CallbackSnippetSearch(formobj) { var request = $http({ method: "post", url: "api/Planning/CallbackSnippetSearch/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function getSearchSnippetSourceData() { var request = $http({ method: "get", url: "api/Planning/getSearchSnippetSourceData/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetCostcentreTreeforPlanCreation(EntityTypeID, FiscalYear, EntityID) { var request = $http({ method: "get", url: "api/Planning/GetCostcentreTreeforPlanCreation/" + EntityTypeID + "/" + FiscalYear + "/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetCostCentreAssignedAmount(CostCentreID) { var request = $http({ method: "get", url: "api/Planning/GetCostCentreAssignedAmount/" + CostCentreID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetCostCentreCurrencyRateById(EntityId, CurrencyId, IsCostCentreCreation) { var request = $http({ method: "get", url: "api/Planning/GetCostCentreCurrencyRateById/" + EntityId + "/" + CurrencyId + "/" + IsCostCentreCreation, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetPeriodAvailability(EntityID, IsWorkspace, LocalIsWorkspaces) {
            if (LocalIsWorkspaces) { GetDataForBreadcrumLoadWithLocalWorkSpacePath(EntityID, IsWorkspace); }
            else { GetDataForBreadcrumLoadWithPath(EntityID); }
        }
        function DeleteApprovedAlloc(UniqueId) { var request = $http({ method: "get", url: "api/Planning/DeleteApprovedAlloc/" + UniqueId, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetApprvdAlloc(EntityId) { var request = $http({ method: "post", url: "api/Planning/GetApprvdAlloc/", params: { action: "add" }, data: EntityId }); return (request.then(handleSuccess, handleError)); }
        function InsertUpdateFinancialTrancation(formobj) { var request = $http({ method: "post", url: "api/Planning/InsertUpdateFinancialTrancation/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function UpdateTotalAssigAmtInFinancialTransaction(formobj) { var request = $http({ method: "post", url: "api/Planning/UpdateTotalAssigAmtInFinancialTransaction/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetTaskMinMaxValue(TasktypeId) { var request = $http({ method: "get", url: "api/Planning/GetTaskMinMaxValue/" + TasktypeId, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetDirectEntityMember(EntityId) { var request = $http({ method: "get", url: "api/Planning/GetDirectEntityMember/" + EntityId, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetDynamicfinancialattributes() {
            var request = $http({
                method: "get",
                url: "api/Planning/GetDynamicfinancialattributes",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetObjectiveUnitsOptionValues(unitTypeId) {
            var request = $http({
                method: "get",
                url: "api/Planning/GetObjectiveUnitsOptionValues/" + unitTypeId,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
            return ($q.reject(response.data.message));
        }
        function handleSuccess(response) { return (response.data); }
        function GetDAMViewSettings() { var request = $http({ method: "get", url: "api/dam/GetDAMViewSettings/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    }
    app.service("PlanningService", ['$http', '$q', PlanningService]);
})(angular, app);
///#source 1 1 /app/services/common-services.js
(function(ng, app) {
    "use strict";

    function CommonService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session');
        return ({
            GetAllSubscriptionType: GetAllSubscriptionType,
            GetUserSubscriptionSettings: GetUserSubscriptionSettings,
            UpdateUserSubscriptionSettings: UpdateUserSubscriptionSettings,
            GetNotification: GetNotification,
            GetNotificationForMypageSettings: GetNotificationForMypageSettings,
            UpdateIsviewedStatusNotification: UpdateIsviewedStatusNotification,
            InsertNavigation: InsertNavigation,
            UpdateUserSingleEntitySubscription: UpdateUserSingleEntitySubscription,
            UpdateNavigationByID: UpdateNavigationByID,
            GetNavigationByIDandParentid: GetNavigationByIDandParentid,
            UpdateNavigationSortOrder: UpdateNavigationSortOrder,
            DeleteNavigation: DeleteNavigation,
            GetGroupIDForNavigation: GetGroupIDForNavigation,
            GetUserDefaultSubscription: GetUserDefaultSubscription,
            InsertSubscriptionNotificationsettings: InsertSubscriptionNotificationsettings,
            UpdateRecapNotificationsettings: UpdateRecapNotificationsettings,
            GetFeedByID: GetFeedByID,
            InsertFeedComment: InsertFeedComment,
            UserMultiSubscription: UserMultiSubscription,
            PostMultiSubscription: PostMultiSubscription,
            PutMultiSubscription: PutMultiSubscription,
            GetUnsubscribeMultiSubscriptionById: GetUnsubscribeMultiSubscriptionById,
            CheckUserPermissionForEntity: CheckUserPermissionForEntity,
            PostFeedTemplate: PostFeedTemplate,
            FeedTemplate: FeedTemplate,
            PostFeed: PostFeed,
            Feed: Feed,
            AdminSettingsforRootLevelInsertUpdate: AdminSettingsforRootLevelInsertUpdate,
            AdminSettingsforReportInsertUpdate: AdminSettingsforReportInsertUpdate,
            AdminSettingsforGanttViewInsertUpdate: AdminSettingsforGanttViewInsertUpdate,
            AdminSettingsforDetailFilterInsertUpdate: AdminSettingsforDetailFilterInsertUpdate,
            AdminSettingsforListViewInsertUpdate: AdminSettingsforListViewInsertUpdate,
            AdminSettingsForRootLevelDelete: AdminSettingsForRootLevelDelete,
            AdminSettingsforRootLevelFilterSettingsInsertUpdate: AdminSettingsforRootLevelFilterSettingsInsertUpdate,
            AdminSettingsForRootLevelDeleteByAttributeID: AdminSettingsForRootLevelDeleteByAttributeID,
            GetNavigationConfig: GetNavigationConfig,
            GetMediabankNavigationConfig: GetMediabankNavigationConfig,
            GetNavigationExternalLinksByID: GetNavigationExternalLinksByID,
            GetAdminSettingselemntnode: GetAdminSettingselemntnode,
            InsertFile: InsertFile,
            InsertFileInPlanAttchTab: InsertFileInPlanAttchTab,
            InsertLink: InsertLink,
            EntityTypeForFeeds: EntityTypeForFeeds,
            DeleteFileByID: DeleteFileByID,
            DeleteLinkByID: DeleteLinkByID,
            GetFileByEntityID: GetFileByEntityID,
            GetFilesandLinksByEntityID: GetFilesandLinksByEntityID,
            GetEnityFeeds: GetEnityFeeds,
            GetLastEntityFeeds: GetLastEntityFeeds,
            GetEnityFeedsForFundingReq: GetEnityFeedsForFundingReq,
            InsertUserSingleEntitySubscription: InsertUserSingleEntitySubscription,
            GetEntityTypeforSubscription: GetEntityTypeforSubscription,
            GetAutoSubscriptionDetails: GetAutoSubscriptionDetails,
            GetWidgetTemplateByID: GetWidgetTemplateByID,
            GetWidgetTypesByID: GetWidgetTypesByID,
            GetWidgetTypeRolesByID: GetWidgetTypeRolesByID,
            GetWidgetTypeDimensionByID: GetWidgetTypeDimensionByID,
            InsertWidgetTemplate: InsertWidgetTemplate,
            UpdateWidgetTemplateByID: UpdateWidgetTemplateByID,
            InsertWidgetTypeRoles: InsertWidgetTypeRoles,
            DeleteWidgetTypeRoles: DeleteWidgetTypeRoles,
            InsertWidgetTemplateRoles: InsertWidgetTemplateRoles,
            GetWidgetTemplateRolesByTemplateID: GetWidgetTemplateRolesByTemplateID,
            DeleteWidgetTemplateRoles: DeleteWidgetTemplateRoles,
            GetUserDefaultSubscriptionByUserID: GetUserDefaultSubscriptionByUserID,
            SaveDefaultSubscriptionByUserID: SaveDefaultSubscriptionByUserID,
            GetWidgetDetailsByUserID: GetWidgetDetailsByUserID,
            InsertNotificationByMail: InsertNotificationByMail,
            InsertTaskNotificationByMail: InsertTaskNotificationByMail,
            Widget: Widget,
            WidgetDragEditing: WidgetDragEditing,
            InsertUserNotification: InsertUserNotification,
            GetDynamicwidgetContentUserID: GetDynamicwidgetContentUserID,
            GetSubscriptionByUserId: GetSubscriptionByUserId,
            GetTaskSubscriptionByUserId: GetTaskSubscriptionByUserId,
            GetIsSubscribedFromSettings: GetIsSubscribedFromSettings,
            GetPoSSettings: GetPoSSettings,
            InsertPoSettingXML: InsertPoSettingXML,
            InsertUpdateAdditionalSettings: InsertUpdateAdditionalSettings,
            GetAdditionalSettings: GetAdditionalSettings,
            InsertAdminSettingXML: InsertAdminSettingXML,
            GetEmailIds: GetEmailIds,
            GetLanguageTypes: GetLanguageTypes,
            SaveNewLanguage: SaveNewLanguage,
            GetLanguageContent: GetLanguageContent,
            UpdateLanguageContent: UpdateLanguageContent,
            GetLanguageSettings: GetLanguageSettings,
            GetCurrentPONumber: GetCurrentPONumber,
            GetTotalPopularTagWordsToShow: GetTotalPopularTagWordsToShow,
            UpdateLanguageName: UpdateLanguageName,
            SetDefaultLanguage: SetDefaultLanguage,
            LanguageSearch: LanguageSearch,
            LanguageSearchs: LanguageSearchs,
            GetDefaultLangFromXML: GetDefaultLangFromXML,
            GetUserDefaultTaskNotificationMailSettings: GetUserDefaultTaskNotificationMailSettings,
            GetSupportText: GetSupportText,
            InsertEditortext: InsertEditortext,
            GetSSODetails: GetSSODetails,
            UpdateSSOSettings: UpdateSSOSettings,
            IsActiveEntity: IsActiveEntity,
            GetUnits: GetUnits,
            GetFeedFilter: GetFeedFilter,
            InsertUpdateFeedFilterGroup: InsertUpdateFeedFilterGroup,
            DeleteFeedGroupByid: DeleteFeedGroupByid,
            GetFeedTemplates: GetFeedTemplates,
            GetUnitsByID: GetUnitsByID,
            InsertUpdateUnits: InsertUpdateUnits,
            DeleteUnitsByid: DeleteUnitsByid,
            GetTopNavigation: GetTopNavigation,
            InsertAdminNotificationSettings: InsertAdminNotificationSettings,
            InsertCurrencyFormat: InsertCurrencyFormat,
            InsertUpdateGanttHeaderBar: InsertUpdateGanttHeaderBar,
            GetAllGanttHeaderBar: GetAllGanttHeaderBar,
            DeleteGanttHeaderBar: DeleteGanttHeaderBar,
            GetUniqueuserhit: GetUniqueuserhit,
            GetApplicationhit: GetApplicationhit,
            GetBrowserStatistic: GetBrowserStatistic,
            GetBrowserVersionStatistic: GetBrowserVersionStatistic,
            GetUserStatistic: GetUserStatistic,
            GetOSStatistic: GetOSStatistic,
            GetstartpageStatistic: GetstartpageStatistic,
            GetUserRoleStatistic: GetUserRoleStatistic,
            GetEnityStatistic: GetEnityStatistic,
            GetEnityCreateationStatistic: GetEnityCreateationStatistic,
            GetbandwidthStatistic: GetbandwidthStatistic,
            GetBroadcastMessages: GetBroadcastMessages,
            GetBroadcastMessagesbyuser: GetBroadcastMessagesbyuser,
            InsertBroadcastMessages: InsertBroadcastMessages,
            updateBroadcastMessagesbyuser: updateBroadcastMessagesbyuser,
            GetCurrencyconverter: GetCurrencyconverter,
            Insertupdatecurrencyconverter: Insertupdatecurrencyconverter,
            DeleteCurrencyconverter: DeleteCurrencyconverter,
            GetRatesByID: GetRatesByID,
            GetExchangesratesbyCurrencytype: GetExchangesratesbyCurrencytype,
            InsertEntityAttachmentsVersion: InsertEntityAttachmentsVersion,
            GetCustomTabsByTypeID: GetCustomTabsByTypeID,
            InsertUpdateCustomTab: InsertUpdateCustomTab,
            DeleteCustomtabByID: DeleteCustomtabByID,
            UpdateCustomTabSortOrder: UpdateCustomTabSortOrder,
            InsertUpdateApplicationUrlTrack: InsertUpdateApplicationUrlTrack,
            GetApplicationUrlTrackByID: GetApplicationUrlTrackByID,
            GetCustomEntityTabsByTypeID: GetCustomEntityTabsByTypeID,
            GetCustomTabUrlTabsByTypeID: GetCustomTabUrlTabsByTypeID,
            UpdateCustomTabSettings: UpdateCustomTabSettings,
            GetCustomTabSettingDetails: GetCustomTabSettingDetails,
            GetConvertedcurrencies: GetConvertedcurrencies,
            GetUpdateSettings: GetUpdateSettings,
            GetPasswordPolicyDetails: GetPasswordPolicyDetails,
            UpdatePasswordPolicy: UpdatePasswordPolicy,
            GetPlantabsettings: GetPlantabsettings,
            UpdatePlanTabIds: UpdatePlanTabIds,
            GetCustomTabEncryptionByID: GetCustomTabEncryptionByID,
            GetOptimakerAddresspoints: GetOptimakerAddresspoints,
            GetLastAssetFeeds: GetLastAssetFeeds,
            FeedAsset: FeedAsset,
            IsAvailableAsset: IsAvailableAsset,
            GetCustomTabAccessByID: GetCustomTabAccessByID,
            GetAdminSettings: GetAdminSettings,
            GetAdminLayoutSettings: GetAdminLayoutSettings,
            GetAdminLayoutFinSettings: GetAdminLayoutFinSettings,
            GetAdminLayoutObjectiveSettings: GetAdminLayoutObjectiveSettings,
            LayoutDesign: LayoutDesign,
            GetAttributeSearchCriteria: GetAttributeSearchCriteria,
            SearchadminSettingsforRootLevelInsertUpdate: SearchadminSettingsforRootLevelInsertUpdate,
            GetCustomEntityTabsfrCalID: GetCustomEntityTabsfrCalID,
            LayoutSettingsApplyChanges: LayoutSettingsApplyChanges,
            GetProofHQSSettings: GetProofHQSSettings,
            UpdateProofHQSSettings: UpdateProofHQSSettings,
            GetCalendarNavigationConfig: GetCalendarNavigationConfig,
            UpdateExpirytime: UpdateExpirytime,
            Getexpirytime: Getexpirytime,
            GetWidgetByID: GetWidgetByID,
            DeleteWidget: DeleteWidget,
            UpdatePopularTagWordsToShow: UpdatePopularTagWordsToShow,
            GettingAssetsFeedSelectionDashbord: GettingAssetsFeedSelectionDashbord,
            GetAssetCreateationStatistic: GetAssetCreateationStatistic,
            InsertLanguageImport: InsertLanguageImport,
            GetProofInitiatorStatistic: GetProofInitiatorStatistic,
            GetSearchtype: GetSearchtype,
            GetassignedAcess: GetassignedAcess,
            UpdateThemeSettings: UpdateThemeSettings,
            RestoreDefaultTheme: RestoreDefaultTheme,
            GetThemeData: GetThemeData,
            GetThemeValues: GetThemeValues,
            InsertNotifydisplaytime: InsertNotifydisplaytime,
            InsertNotifycycledisplaytime: InsertNotifycycledisplaytime,
            Getnotifytimesetbyuser: Getnotifytimesetbyuser,
            Getnotifyrecycletimesetbyuser: Getnotifyrecycletimesetbyuser,
            GetTaskLIveUpdateRecords: GetTaskLIveUpdateRecords,
            UpdateTitleSettings: UpdateTitleSettings,
            GetTitleLogoSettings: GetTitleLogoSettings,
            GetAlltableswithmetadata: GetAlltableswithmetadata,
            ManipulateQueryEditorQuery: ManipulateQueryEditorQuery,
            GetAlltablesnames: GetAlltablesnames,
            getsnippet: getsnippet,
            GetLanguageExport: GetLanguageExport,
            getTenantClientPath: getTenantClientPath,
            GetObjectLanguageContent: GetObjectLanguageContent,
            GetlanguagetypeByID: GetlanguagetypeByID,
            UpdateJsonLanguageContent: UpdateJsonLanguageContent,
            InsertNewLanguage: InsertNewLanguage,
            GetAssetFeeds: GetAssetFeeds,
            GetModuleID: GetModuleID,
            getredirectPath: getredirectPath,
            UpdateAccLockPasswordPolicy: UpdateAccLockPasswordPolicy,
            GetAccLockDetails: GetAccLockDetails,
            InsertUpdateDecimalSettings: InsertUpdateDecimalSettings,
            GetDecimalSettingsValue: GetDecimalSettingsValue,
            getNewsfeedforTask: getNewsfeedforTask,
            GetDalimSettings: GetDalimSettings,
            UpdateDalimSettings: UpdateDalimSettings,
            updateDefaultFolderInfo: updateDefaultFolderInfo,
            getDefaultFolderInfo: getDefaultFolderInfo,
            UpdatetopIsviewedStatusNotification: UpdatetopIsviewedStatusNotification,
            SaveHolidayDetails: SaveHolidayDetails,
            GetNonBusinessDays: GetNonBusinessDays,
            InsertHolidayDetails: InsertHolidayDetails,
            GetHolidaysDetails: GetHolidaysDetails,
            DeleteHoliday: DeleteHoliday,
            UpdateNewFeedconfig: UpdateNewFeedconfig,
            GetNewsFeedConfigInfo: GetNewsFeedConfigInfo,
            savecloudsettings: savecloudsettings,
            getcloudsettings: getcloudsettings,
            GetNavigationandLanguageConfig: GetNavigationandLanguageConfig,
            GetObjectiveTabAdminSettings: GetObjectiveTabAdminSettings,
            AdminSettingsObjectivetabRootLevelInsertUpdate: AdminSettingsObjectivetabRootLevelInsertUpdate
        });

        function GetAllSubscriptionType() {
            var request = $http({
                method: "get",
                url: "api/common/GetAllSubscriptionType/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetUserSubscriptionSettings(SubscribtionTypeID) {
            var request = $http({
                method: "get",
                url: "api/common/GetUserSubscriptionSettings/" + SubscribtionTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateUserSubscriptionSettings(ID) {
            var request = $http({
                method: "put",
                url: "api/common/UpdateUserSubscriptionSettings/" + ID,
                params: {
                    action: "update"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetNotification(flag) {
            var request = $http({
                method: "get",
                url: "api/common/GetNotification/" + flag,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetNotificationForMypageSettings(flag, pageNo) {
            var request = $http({
                method: "get",
                url: "api/common/GetNotificationForMypageSettings/" + flag + "/" + pageNo,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateIsviewedStatusNotification(updatestae) {
            var request = $http({
                method: "post",
                url: "api/common/UpdateIsviewedStatusNotification/",
                params: {
                    action: "add"
                },
                data: {
                    UserID: updatestae.UserID,
                    FirstFiveNotifications: updatestae.FirstFiveNotifications,
                    Flag: updatestae.Flag
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertNavigation(addmodule) {
            var request = $http({
                method: "post",
                url: "api/common/InsertNavigation/",
                params: {
                    action: "add"
                },
                data: addmodule
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateUserSingleEntitySubscription(jobj) {
            var request = $http({
                method: "post",
                url: "api/common/UpdateUserSingleEntitySubscription/",
                params: {
                    action: "add"
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateNavigationByID(formobj) {
            var request = $http({
                method: "put",
                url: "api/common/UpdateNavigationByID/",
                params: {
                    action: "update"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetNavigationByIDandParentid(IsParentID, UserID, Flag) {
            var request = $http({
                method: "get",
                url: "api/common/GetNavigationByIDandParentid/" + IsParentID + "/" + UserID + "/" + Flag,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateNavigationSortOrder(AttributeArray) {
            var request = $http({
                method: "post",
                url: "api/common/UpdateNavigationSortOrder/",
                params: {
                    action: "add",
                },
                data: AttributeArray
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteNavigation(ID) {
            var request = $http({
                method: "delete",
                url: "api/common/DeleteNavigation/" + ID,
                params: {
                    action: "delete",
                },
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetGroupIDForNavigation(NavigationID, UserID) {
            var request = $http({
                method: "get",
                url: "api/common/GetGroupIDForNavigation/" + NavigationID + "/" + UserID,
                params: {
                    action: "get",
                },
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetUserDefaultSubscription(ID, Caption, IsAutomated) {
            var request = $http({
                method: "get",
                url: "api/common/GetUserDefaultSubscription/" + ID + "/" + Caption + "/" + IsAutomated,
                params: {
                    action: "get",
                },
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertSubscriptionNotificationsettings(jobj) {
            var request = $http({
                method: "post",
                url: "api/common/InsertSubscriptionNotificationsettings/",
                params: {
                    action: "add",
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateRecapNotificationsettings(jobj) {
            var request = $http({
                method: "put",
                url: "api/common/UpdateRecapNotificationsettings/",
                params: {
                    action: "update",
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetFeedByID(EntitiyID) {
            var request = $http({
                method: "get",
                url: "api/common/GetFeedByID/" + EntitiyID,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertFeedComment(addfeedcomment) {
            var request = $http({
                method: "post",
                url: "api/common/InsertFeedComment/",
                params: {
                    action: "add",
                },
                data: addfeedcomment
            });
            return (request.then(handleSuccess, handleError));
        }

        function UserMultiSubscription(EntitiyID, UserID) {
            var request = $http({
                method: "get",
                url: "api/common/UserMultiSubscription/" + EntitiyID + "/" + UserID,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function PostMultiSubscription(jobj) {
            var request = $http({
                method: "post",
                url: "api/common/PostMultiSubscription/",
                params: {
                    action: "add",
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function PutMultiSubscription(formobj) {
            var request = $http({
                method: "post",
                url: "api/common/PutMultiSubscription/",
                params: {
                    action: "add",
                },
                data: {
                    LEVELS: formobj.levels,
                    EntitiyID: formobj.EntitiyID,
                    UserID: formobj.UserID,
                    IsMultiLevel: formobj.IsMultiLevel,
                    EntityTypeID: formobj.EntityTypeID
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetUnsubscribeMultiSubscriptionById(EntitiyID, UserID) {
            var request = $http({
                method: "delete",
                url: "api/common/GetUnsubscribeMultiSubscriptionById/" + EntitiyID + "/" + UserID,
                params: {
                    action: "delete",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function CheckUserPermissionForEntity(EntitiyID) {
            var request = $http({
                method: "get",
                url: "api/common/CheckUserPermissionForEntity/" + EntitiyID,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function PostFeedTemplate(jobj) {
            var request = $http({
                method: "post",
                url: "api/common/PostFeedTemplate/",
                params: {
                    action: "add",
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function FeedTemplate(jobj) {
            var request = $http({
                method: "put",
                url: "api/common/FeedTemplate/",
                params: {
                    action: "update",
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function PostFeed(addnewsfeed) {
            var request = $http({
                method: "post",
                url: "api/common/PostFeed/",
                params: {
                    action: "add",
                },
                data: addnewsfeed
            });
            return (request.then(handleSuccess, handleError));
        }

        function Feed(addnewsfeed) {
            var request = $http({
                method: "put",
                url: "api/common/Feed/",
                params: {
                    action: "update",
                },
                data: addnewsfeed
            });
            return (request.then(handleSuccess, handleError));
        }

        function AdminSettingsforRootLevelInsertUpdate(listSettings) {
            var request = $http({
                method: "post",
                url: "api/common/AdminSettingsforRootLevelInsertUpdate/",
                params: {
                    action: "add",
                },
                data: listSettings
            });
            return (request.then(handleSuccess, handleError));
        }

        function AdminSettingsforReportInsertUpdate(key) {
            var request = $http({
                method: "post",
                url: "api/common/AdminSettingsforReportInsertUpdate/",
                params: {
                    action: "add",
                },
                data: key
            });
            return (request.then(handleSuccess, handleError));
        }

        function AdminSettingsforGanttViewInsertUpdate(key) {
            var request = $http({
                method: "post",
                url: "api/common/AdminSettingsforGanttViewInsertUpdate/",
                params: {
                    action: "add",
                },
                data: key
            });
            return (request.then(handleSuccess, handleError));
        }

        function AdminSettingsforDetailFilterInsertUpdate(key) {
            var request = $http({
                method: "post",
                url: "api/common/AdminSettingsforDetailFilterInsertUpdate/",
                params: {
                    action: "add",
                },
                data: key
            });
            return (request.then(handleSuccess, handleError));
        }

        function AdminSettingsforListViewInsertUpdate(key) {
            var request = $http({
                method: "post",
                url: "api/common/AdminSettingsforListViewInsertUpdate/",
                params: {
                    action: "add",
                },
                data: key
            });
            return (request.then(handleSuccess, handleError));
        }

        function AdminSettingsforRootLevelFilterSettingsInsertUpdate(key, EntityTypeID, AttribtueID) {
            var request = $http({
                method: "delete",
                url: "api/common/AdminSettingsforRootLevelFilterSettingsInsertUpdate/" + key + "/" + EntityTypeID + "/" + AttribtueID,
                params: {
                    action: "delete",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function AdminSettingsForRootLevelDelete(key, EntityTypeID) {
            var request = $http({
                method: "delete",
                url: "api/common/AdminSettingsForRootLevelDelete/" + key + "/" + EntityTypeID,
                params: {
                    action: "delete",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function AdminSettingsForRootLevelDeleteByAttributeID(key, EntityTypeID, AttribtueID) {
            var request = $http({
                method: "delete",
                url: "api/common/AdminSettingsForRootLevelDeleteByAttributeID/" + key + "/" + EntityTypeID + "/" + AttribtueID,
                params: {
                    action: "delete",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetNavigationConfig() {
            var request = $http({
                method: "get",
                url: "api/common/GetNavigationConfig/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetMediabankNavigationConfig() {
            var request = $http({
                method: "get",
                url: "api/common/GetMediabankNavigationConfig/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetNavigationExternalLinksByID(ID) {
            var request = $http({
                method: "get",
                url: "api/common/GetNavigationExternalLinksByID/" + ID,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAdminSettingselemntnode(LogoSettings, elemntnode, typeid) {
            var request = $http({
                method: "get",
                url: "api/common/GetAdminSettingselemntnode/" + LogoSettings + "/" + elemntnode + "/" + typeid,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertFile(jobj) {
            var request = $http({
                method: "post",
                url: "api/common/InsertFile/",
                params: {
                    action: "add",
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertFileInPlanAttchTab(jobj) {
            var request = $http({
                method: "post",
                url: "api/common/InsertFileInPlanAttchTab/",
                params: {
                    action: "add",
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertLink(jobj) {
            var request = $http({
                method: "post",
                url: "api/common/InsertLink/",
                params: {
                    action: "add",
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteFileByID(ID) {
            var request = $http({
                method: "delete",
                url: "api/common/DeleteFileByID/" + ID,
                params: {
                    action: "delete",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteLinkByID(ID) {
            var request = $http({
                method: "delete",
                url: "api/common/DeleteLinkByID/" + ID,
                params: {
                    action: "delete",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetFileByEntityID(EntityID) {
            var request = $http({
                method: "get",
                url: "api/common/GetFileByEntityID/" + EntityID,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetFilesandLinksByEntityID(EntityID) {
            var request = $http({
                method: "get",
                url: "api/common/GetFilesandLinksByEntityID/" + EntityID,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEnityFeeds(EntityID, pageNo, Feedsgroupid) {
            var request = $http({
                method: "get",
                url: "api/common/GetEnityFeeds/" + EntityID + "/" + pageNo + "/" + Feedsgroupid,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetLastEntityFeeds(EntityID, Feedsgroupid) {
            var request = $http({
                method: "get",
                url: "api/common/GetLastEntityFeeds/" + EntityID + "/" + Feedsgroupid,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEnityFeedsForFundingReq(EntityID, pageNo, islatestfeed) {
            var request = $http({
                method: "get",
                url: "api/common/GetEnityFeedsForFundingReq/" + EntityID + "/" + pageNo + "/" + islatestfeed,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertUserSingleEntitySubscription(jobj) {
            var request = $http({
                method: "post",
                url: "api/common/InsertUserSingleEntitySubscription/",
                params: {
                    action: "add",
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityTypeforSubscription(ID) {
            var request = $http({
                method: "get",
                url: "api/common/GetEntityTypeforSubscription/" + ID,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAutoSubscriptionDetails(UserID, EntityID) {
            var request = $http({
                method: "get",
                url: "api/common/GetAutoSubscriptionDetails/" + UserID + "/" + EntityID,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetWidgetTemplateByID(TemplateID) {
            var request = $http({
                method: "get",
                url: "api/common/GetWidgetTemplateByID/" + TemplateID,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetWidgetTypesByID(UserId, ISAdmin, TypeID) {
            var request = $http({
                method: "get",
                url: "api/common/GetWidgetTypesByID/" + UserId + "/" + ISAdmin + "/" + TypeID,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetWidgetTypeRolesByID(WidgetTypeID) {
            var request = $http({
                method: "get",
                url: "api/common/GetWidgetTypeRolesByID/" + WidgetTypeID,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetWidgetTypeDimensionByID(WidgetTypeID) {
            var request = $http({
                method: "get",
                url: "api/common/GetWidgetTypeDimensionByID/" + WidgetTypeID,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertWidgetTemplate(jobj) {
            var request = $http({
                method: "post",
                url: "api/common/InsertWidgetTemplate/",
                params: {
                    action: "add",
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateWidgetTemplateByID(jobj) {
            var request = $http({
                method: "put",
                url: "api/common/UpdateWidgetTemplateByID/",
                params: {
                    action: "update",
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertWidgetTypeRoles(jobj) {
            var request = $http({
                method: "post",
                url: "api/common/InsertWidgetTypeRoles/",
                params: {
                    action: "add",
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteWidgetTypeRoles(widgetTypeID) {
            var request = $http({
                method: "delete",
                url: "api/common/DeleteWidgetTypeRoles/" + widgetTypeID,
                params: {
                    action: "delete",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertWidgetTemplateRoles(jobj) {
            var request = $http({
                method: "post",
                url: "api/common/InsertWidgetTemplateRoles/",
                params: {
                    action: "add",
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetWidgetTemplateRolesByTemplateID(WidgetTemplateID) {
            var request = $http({
                method: "get",
                url: "api/common/GetWidgetTemplateRolesByTemplateID/" + WidgetTemplateID,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteWidgetTemplateRoles(WidgetTemplateID) {
            var request = $http({
                method: "delete",
                url: "api/common/DeleteWidgetTemplateRoles/" + WidgetTemplateID,
                params: {
                    action: "delete",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetUserDefaultSubscriptionByUserID() {
            var request = $http({
                method: "get",
                url: "api/common/GetUserDefaultSubscriptionByUserID/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function SaveDefaultSubscriptionByUserID(formobj) {
            var request = $http({
                method: "post",
                url: "api/common/SaveDefaultSubscriptionByUserID/",
                params: {
                    action: "add",
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetWidgetDetailsByUserID(UserID, IsAdmin, GlobalTemplateID) {
            var request = $http({
                method: "get",
                url: "api/common/GetWidgetDetailsByUserID/" + UserID + "/" + IsAdmin + "/" + GlobalTemplateID,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertNotificationByMail(formobj) {
            var request = $http({
                method: "post",
                url: "api/common/InsertNotificationByMail/",
                params: {
                    action: "add",
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertTaskNotificationByMail(formobj) {
            var request = $http({
                method: "post",
                url: "api/common/InsertTaskNotificationByMail/",
                params: {
                    action: "add",
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function Widget(formobj) {
            var request = $http({
                method: "post",
                url: "api/common/Widget/",
                params: {
                    action: "add",
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function WidgetDragEditing(widgetdatalist) {
            var request = $http({
                method: "post",
                url: "api/common/WidgetDragEditing/",
                params: {
                    action: "add",
                },
                data: widgetdatalist
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetWidgetByID(WidgetID, TemplateID, IsAdminPage) {
            var request = $http({
                method: "get",
                url: "api/common/GetWidgetByID/" + WidgetID + "/" + TemplateID + "/" + IsAdminPage,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteWidget(TemplateID, WidgetID, IsAdminPage) {
            var request = $http({
                method: "delete",
                url: "api/common/DeleteWidget/" + WidgetID + "/" + TemplateID + "/" + IsAdminPage,
                params: {
                    action: "delete",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertUserNotification(formobj) {
            var request = $http({
                method: "post",
                url: "api/common/InsertUserNotification/",
                params: {
                    action: "add",
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetDynamicwidgetContentUserID(UserID, WidgetTypeID, WidgetID, DimensionID) {
            var request = $http({
                method: "get",
                url: "api/common/GetDynamicwidgetContentUserID/" + UserID + "/" + WidgetTypeID + "/" + WidgetID + "/" + DimensionID,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetSubscriptionByUserId() {
            var request = $http({
                method: "get",
                url: "api/common/GetSubscriptionByUserId/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTaskSubscriptionByUserId() {
            var request = $http({
                method: "get",
                url: "api/common/GetTaskSubscriptionByUserId/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetIsSubscribedFromSettings() {
            var request = $http({
                method: "get",
                url: "api/common/GetIsSubscribedFromSettings/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function EntityTypeForFeeds() {
            var request = $http({
                method: "get",
                url: "api/common/EntityTypeForFeeds/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetPoSSettings(PoSettings) {
            var request = $http({
                method: "get",
                url: "api/common/GetPoSSettings/" + PoSettings,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertPoSettingXML(formobj) {
            var request = $http({
                method: "post",
                url: "api/common/InsertPoSettingXML/",
                params: {
                    action: "add",
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertUpdateAdditionalSettings(dataobj) {
            var request = $http({
                method: "post",
                url: "api/common/InsertUpdateAdditionalSettings/",
                params: {
                    action: "add",
                },
                data: dataobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAdditionalSettings() {
            var request = $http({
                method: "get",
                url: "api/common/GetAdditionalSettings/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertAdminSettingXML(emailids) {
            var request = $http({
                method: "post",
                url: "api/common/InsertAdminSettingXML/",
                params: {
                    action: "add",
                },
                data: emailids
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEmailIds() {
            var request = $http({
                method: "get",
                url: "api/common/GetEmailIds/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetLanguageTypes() {
            var request = $http({
                method: "get",
                url: "api/common/GetLanguageTypes/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function SaveNewLanguage(formobj) {
            var request = $http({
                method: "post",
                url: "api/common/SaveNewLanguage/",
                params: {
                    action: "add",
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetLanguageContent(StartRows, NextRows) {
            var request = $http({
                method: "get",
                url: "api/common/GetLanguageContent/" + StartRows + "/" + NextRows,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateLanguageContent(formobj) {
            var request = $http({
                method: "post",
                url: "api/common/UpdateLanguageContent/",
                params: {
                    action: "add",
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetLanguageSettings(LangID) {
            var request = $http({
                method: "GET",
                url: "api/common/GetLanguageSettings/" + LangID,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetCurrentPONumber() {
            var request = $http({
                method: "get",
                url: "api/common/GetCurrentPONumber/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTotalPopularTagWordsToShow() {
            var request = $http({
                method: "get",
                url: "api/common/GetTotalPopularTagWordsToShow/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateLanguageName(formobj) {
            var request = $http({
                method: "post",
                url: "api/common/UpdateLanguageName/",
                params: {
                    action: "add",
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function SetDefaultLanguage(LangID) {
            var request = $http({
                method: "post",
                url: "api/common/SetDefaultLanguage/",
                params: {
                    action: "add",
                },
                data: LangID
            });
            return (request.then(handleSuccess, handleError));
        }

        function LanguageSearch(formobj) {
            var request = $http({
                method: "post",
                url: "api/common/LanguageSearch/",
                params: {
                    action: "add",
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function LanguageSearchs(formobj) {
            var request = $http({
                method: "post",
                url: "api/common/LanguageSearchs/",
                params: {
                    action: "add",
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetDefaultLangFromXML() {
            var request = $http({
                method: "get",
                url: "api/common/GetDefaultLangFromXML/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetUserDefaultTaskNotificationMailSettings() {
            var request = $http({
                method: "get",
                url: "api/common/GetUserDefaultTaskNotificationMailSettings/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetSupportText() {
            var request = $http({
                method: "get",
                url: "api/common/GetSupportText/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertEditortext(jobj) {
            var request = $http({
                method: "post",
                url: "api/common/InsertEditortext/",
                params: {
                    action: "add",
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetSSODetails() {
            var request = $http({
                method: "get",
                url: "api/common/GetSSODetails/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateSSOSettings(jobj) {
            var request = $http({
                method: "post",
                url: "api/common/UpdateSSOSettings/",
                params: {
                    action: "add",
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function IsActiveEntity(EntityID) {
            var request = $http({
                method: "get",
                url: "api/common/IsActiveEntity/" + EntityID,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetUnits() {
            var request = $http({
                method: "get",
                url: "api/common/GetUnits/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetFeedFilter() {
            var request = $http({
                method: "get",
                url: "api/common/GetFeedFilter/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertUpdateFeedFilterGroup(jobj) {
            var request = $http({
                method: "post",
                url: "api/common/InsertUpdateFeedFilterGroup/",
                params: {
                    action: "add",
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteFeedGroupByid(ID) {
            var request = $http({
                method: "delete",
                url: "api/common/DeleteFeedGroupByid/" + ID,
                params: {
                    action: "delete",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetFeedTemplates() {
            var request = $http({
                method: "get",
                url: "api/common/GetFeedTemplates/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetUnitsByID(ID) {
            var request = $http({
                method: "get",
                url: "api/common/GetUnitsByID/" + ID,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertUpdateUnits(jobj) {
            var request = $http({
                method: "post",
                url: "api/common/InsertUpdateUnits/",
                params: {
                    action: "add",
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteUnitsByid(ID) {
            var request = $http({
                method: "delete",
                url: "api/common/DeleteUnitsByid/" + ID,
                params: {
                    action: "delete",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTopNavigation() {
            var request = $http({
                method: "get",
                url: "api/common/GetTopNavigation/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertAdminNotificationSettings(subscriptionObject) {
            var request = $http({
                method: "post",
                url: "api/common/InsertAdminNotificationSettings/",
                params: {
                    action: "add",
                },
                data: subscriptionObject
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertCurrencyFormat(jobj) {
            var request = $http({
                method: "post",
                url: "api/common/InsertCurrencyFormat/",
                params: {
                    action: "add",
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertUpdateGanttHeaderBar(jobj) {
            var request = $http({
                method: "post",
                url: "api/common/InsertUpdateGanttHeaderBar/",
                params: {
                    action: "add",
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAllGanttHeaderBar() {
            var request = $http({
                method: "get",
                url: "api/common/GetAllGanttHeaderBar/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteGanttHeaderBar(ID) {
            var request = $http({
                method: "delete",
                url: "api/common/DeleteGanttHeaderBar/" + ID,
                params: {
                    action: "delete",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetUniqueuserhit(year, month) {
            var request = $http({
                method: "get",
                url: "api/common/GetUniqueuserhit/" + year + "/" + month,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetApplicationhit(year, month) {
            var request = $http({
                method: "get",
                url: "api/common/GetApplicationhit/" + year + "/" + month,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetBrowserStatistic(year, month) {
            var request = $http({
                method: "get",
                url: "api/common/GetBrowserStatistic/" + year + "/" + month,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetBrowserVersionStatistic(year, month) {
            var request = $http({
                method: "get",
                url: "api/common/GetBrowserVersionStatistic/" + year + "/" + month,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetUserStatistic() {
            var request = $http({
                method: "get",
                url: "api/common/GetUserStatistic/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetOSStatistic() {
            var request = $http({
                method: "get",
                url: "api/common/GetOSStatistic/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetstartpageStatistic() {
            var request = $http({
                method: "get",
                url: "api/common/GetstartpageStatistic/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetUserRoleStatistic() {
            var request = $http({
                method: "get",
                url: "api/common/GetUserRoleStatistic/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEnityStatistic() {
            var request = $http({
                method: "get",
                url: "api/common/GetEnityStatistic/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEnityCreateationStatistic(year, month) {
            var request = $http({
                method: "get",
                url: "api/common/GetEnityCreateationStatistic/" + year + "/" + month,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetbandwidthStatistic(year, month) {
            var request = $http({
                method: "get",
                url: "api/common/GetbandwidthStatistic/" + year + "/" + month,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetBroadcastMessages() {
            var request = $http({
                method: "get",
                url: "api/common/GetBroadcastMessages/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetBroadcastMessagesbyuser() {
            var request = $http({
                method: "get",
                url: "api/common/GetBroadcastMessagesbyuser/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertBroadcastMessages(jobj) {
            var request = $http({
                method: "post",
                url: "api/common/InsertBroadcastMessages/",
                params: {
                    action: "add",
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function updateBroadcastMessagesbyuser() {
            var request = $http({
                method: "post",
                url: "api/common/updateBroadcastMessagesbyuser/",
                params: {
                    action: "add",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetCurrencyconverter() {
            var request = $http({
                method: "get",
                url: "api/common/GetCurrencyconverter/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function Insertupdatecurrencyconverter(jobj) {
            var request = $http({
                method: "post",
                url: "api/common/Insertupdatecurrencyconverter/",
                params: {
                    action: "add",
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteCurrencyconverter(ID) {
            var request = $http({
                method: "delete",
                url: "api/common/DeleteCurrencyconverter/" + ID,
                params: {
                    action: "delete",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetRatesByID(ID) {
            var request = $http({
                method: "get",
                url: "api/common/GetRatesByID/" + ID,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetExchangesratesbyCurrencytype(ID) {
            var request = $http({
                method: "get",
                url: "api/common/GetExchangesratesbyCurrencytype/" + ID,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertEntityAttachmentsVersion(jobj) {
            var request = $http({
                method: "post",
                url: "api/common/InsertEntityAttachmentsVersion/",
                params: {
                    action: "add",
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetCustomTabsByTypeID(TypeID) {
            var request = $http({
                method: "get",
                url: "api/common/GetCustomTabsByTypeID/" + TypeID,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertUpdateCustomTab(jobj) {
            var request = $http({
                method: "post",
                url: "api/common/InsertUpdateCustomTab/",
                params: {
                    action: "add",
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteCustomtabByID(ID, AttributeTypeID, EntityTypeID) {
            var request = $http({
                method: "delete",
                url: "api/common/DeleteCustomtabByID/" + ID + "/" + AttributeTypeID + "/" + EntityTypeID,
                params: {
                    action: "delete",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateCustomTabSortOrder(ID, SortOrder) {
            var request = $http({
                method: "put",
                url: "api/common/UpdateCustomTabSortOrder/" + ID + "/" + SortOrder,
                params: {
                    action: "update",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertUpdateApplicationUrlTrack(jobj) {
            var request = $http({
                method: "post",
                url: "api/common/InsertUpdateApplicationUrlTrack/",
                params: {
                    action: "add",
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetApplicationUrlTrackByID(TrackID) {
            var request = $http({
                method: "get",
                url: "api/common/GetApplicationUrlTrackByID/" + TrackID,
                params: {
                    action: "get",
                },
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetCustomEntityTabsByTypeID(TypeID, EntityTypeID, EntityID, CalID) {
            if (arguments.length == 1) {
                var request = $http({
                    method: "get",
                    url: "api/common/GetCustomEntityTabsByTypeID/" + TypeID,
                    params: {
                        action: "get",
                    }
                });
            } else {
                var request = $http({
                    method: "get",
                    url: "api/common/GetCustomEntityTabsByTypeID/" + TypeID + "/" + CalID + "/" + EntityTypeID + "/" + EntityID,
                    params: {
                        action: "get",
                    },
                });
            }
            return (request.then(handleSuccess, handleError));
        }

        function GetCustomTabUrlTabsByTypeID(tabID, entityID) {
            var request = $http({
                method: "get",
                url: "api/common/GetCustomTabUrlTabsByTypeID/" + tabID + "/" + entityID,
                params: {
                    action: "get",
                },
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateCustomTabSettings(jobj) {
            var request = $http({
                method: "post",
                url: "api/common/UpdateCustomTabSettings/",
                params: {
                    action: "add",
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetCustomTabSettingDetails() {
            var request = $http({
                method: "get",
                url: "api/common/GetCustomTabSettingDetails/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetConvertedcurrencies(ConvertedCurrenciesObj) {
            var request = $http({
                method: "post",
                url: "api/common/GetConvertedcurrencies/",
                params: {
                    action: "add",
                },
                data: ConvertedCurrenciesObj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetUpdateSettings() {
            var request = $http({
                method: "get",
                url: "api/common/GetUpdateSettings/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetPasswordPolicyDetails() {
            var request = $http({
                method: "get",
                url: "api/common/GetPasswordPolicyDetails/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdatePasswordPolicy(jobj) {
            var request = $http({
                method: "post",
                url: "api/common/UpdatePasswordPolicy/",
                params: {
                    action: "add",
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetPlantabsettings() {
            var request = $http({
                method: "get",
                url: "api/common/GetPlantabsettings/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdatePlanTabIds(jsondata) {
            var request = $http({
                method: "post",
                url: "api/common/UpdatePlanTabIds/",
                params: {
                    action: "add",
                },
                data: jsondata
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetCustomTabEncryptionByID() {
            var request = $http({
                method: "get",
                url: "api/common/GetCustomTabEncryptionByID/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetOptimakerAddresspoints() {
            var request = $http({
                method: "get",
                url: "api/common/GetOptimakerAddresspoints/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetLastAssetFeeds(AssetID) {
            var request = $http({
                method: "get",
                url: "api/common/GetLastAssetFeeds/" + AssetID,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function FeedAsset(jobj) {
            var request = $http({
                method: "post",
                url: "api/common/FeedAsset/",
                params: {
                    action: "add",
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function IsAvailableAsset(AssetID) {
            var request = $http({
                method: "get",
                url: "api/common/IsAvailableAsset/" + AssetID,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetCustomTabAccessByID(TabID) {
            var request = $http({
                method: "get",
                url: "api/common/GetCustomTabAccessByID/" + TabID,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAdminSettings(LogoSettings, typeid) {
            var request = $http({
                method: "get",
                url: "api/common/GetAdminSettings/" + LogoSettings + "/" + typeid,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAdminLayoutSettings(LogoSettings, typeid) {
            var request = $http({
                method: "get",
                url: "api/common/GetAdminLayoutSettings/" + LogoSettings + "/" + typeid,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAdminLayoutFinSettings(LogoSettings, typeid) {
            var request = $http({
                method: "get",
                url: "api/common/GetAdminLayoutFinSettings/" + LogoSettings + "/" + typeid,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAdminLayoutObjectiveSettings(LogoSettings, typeid) {
            var request = $http({
                method: "get",
                url: "api/common/GetAdminLayoutObjectiveSettings/" + LogoSettings + "/" + typeid,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function LayoutDesign(TabType, TabLocation) {
            var request = $http({
                method: "get",
                url: "api/common/LayoutDesign/" + TabType + "/" + TabLocation,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeSearchCriteria(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/common/GetAttributeSearchCriteria/" + EntityTypeID,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function SearchadminSettingsforRootLevelInsertUpdate(jobj) {
            var request = $http({
                method: "post",
                url: "api/common/SearchadminSettingsforRootLevelInsertUpdate/",
                params: {
                    action: "add",
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetCustomEntityTabsfrCalID(TypeID, CalID) {
            var request = $http({
                method: "get",
                url: "api/common/GetCustomEntityTabsfrCalID/" + TypeID + "/" + CalID,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function LayoutSettingsApplyChanges(dataobj) {
            var request = $http({
                method: "post",
                url: "api/common/LayoutSettingsApplyChanges/",
                params: {
                    action: "add",
                },
                data: dataobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetProofHQSSettings() {
            var request = $http({
                method: "get",
                url: "api/common/GetProofHQSSettings/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateProofHQSSettings(dataobj) {
            var request = $http({
                method: "post",
                url: "api/common/UpdateProofHQSSettings/",
                params: {
                    action: "add",
                },
                data: dataobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetCalendarNavigationConfig() {
            var request = $http({
                method: "get",
                url: "api/common/GetCalendarNavigationConfig/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateExpirytime(Time) {
            var request = $http({
                method: "post",
                url: "api/common/UpdateExpirytime/",
                params: {
                    action: "add",
                },
                data: Time
            });
            return (request.then(handleSuccess, handleError));
        }

        function Getexpirytime() {
            var request = $http({
                method: "get",
                url: "api/common/Getexpirytime/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdatePopularTagWordsToShow(TotalTagWords) {
            var request = $http({
                method: "post",
                url: "api/common/UpdatePopularTagWordsToShow/",
                params: {
                    action: "add",
                },
                data: TotalTagWords
            });
            return (request.then(handleSuccess, handleError));
        }

        function GettingAssetsFeedSelectionDashbord(Topx, FeedTemplateID, Newsfeedid) {
            var request = $http({
                method: "get",
                url: "api/common/GettingAssetsFeedSelectionDashbord/" + Topx + "/" + FeedTemplateID + "/" + Newsfeedid,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAssetCreateationStatistic(year, month) {
            var request = $http({
                method: "get",
                url: "api/common/GetAssetCreateationStatistic/" + year + "/" + month,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertLanguageImport(dataobj) {
            var request = $http({
                method: "post",
                url: "api/common/InsertLanguageImport/",
                params: {
                    action: "add",
                },
                data: dataobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetProofInitiatorStatistic(dataobj) {
            var request = $http({
                method: "post",
                url: "api/common/GetProofInitiatorStatistic/",
                params: {
                    action: "add",
                },
                data: dataobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetSearchtype(userid) {
            var request = $http({
                method: "get",
                url: "api/common/GetSearchtype/" + userid,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetassignedAcess(userid) {
            var request = $http({
                method: "get",
                url: "api/common/GetassignedAcess/" + userid,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateThemeSettings(dataobj) {
            var request = $http({
                method: "post",
                url: "api/common/UpdateThemeSettings/",
                params: {
                    action: "add",
                },
                data: dataobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function RestoreDefaultTheme() {
            var request = $http({
                method: "post",
                url: "api/common/RestoreDefaultTheme/",
                params: {
                    action: "add",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetThemeData(Theme) {
            var request = $http({
                method: "get",
                url: "api/common/GetThemeData/" + Theme,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetThemeValues() {
            var request = $http({
                method: "get",
                url: "api/common/GetThemeValues/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertNotifydisplaytime(dataobj) {
            var request = $http({
                method: "post",
                url: "api/common/InsertNotifydisplaytime/",
                params: {
                    action: "add",
                },
                data: dataobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function Getnotifytimesetbyuser() {
            var request = $http({
                method: "get",
                url: "api/common/Getnotifytimesetbyuser/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function Getnotifyrecycletimesetbyuser() {
            var request = $http({
                method: "get",
                url: "api/common/Getnotifyrecycletimesetbyuser/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertNotifycycledisplaytime(dataobj) {
            var request = $http({
                method: "post",
                url: "api/common/InsertNotifycycledisplaytime/",
                params: {
                    action: "add",
                },
                data: dataobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTaskLIveUpdateRecords() {
            var request = $http({
                method: "get",
                url: "api/common/GetTaskLIveUpdateRecords/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateTitleSettings(dataobj) {
            var request = $http({
                method: "post",
                url: "api/common/UpdateTitleSettings/",
                params: {
                    action: "add",
                },
                data: dataobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTitleLogoSettings(TitleSettings) {
            var request = $http({
                method: "get",
                url: "api/common/GetTitleLogoSettings/" + TitleSettings,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAlltableswithmetadata(Tablename) {
            var request = $http({
                method: "get",
                url: "api/common/GetAlltableswithmetadata/" + Tablename,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function ManipulateQueryEditorQuery(dataobj) {
            var request = $http({
                method: "post",
                url: "api/common/ManipulateQueryEditorQuery/",
                params: {
                    action: "add",
                },
                data: dataobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAlltablesnames() {
            var request = $http({
                method: "get",
                url: "api/common/GetAlltablesnames/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function getsnippet(token) {
            var request = $http({
                method: "get",
                url: "api/common/getsnippet/" + token,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetLanguageExport(ID, languageTypename, Filename) {
            var request = $http({
                method: "get",
                url: "api/common/GetLanguageExport/" + ID + "/" + languageTypename + "/" + Filename,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function getTenantClientPath() {
            var request = $http({
                method: "get",
                url: "api/Common/getTenantClientPath/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAssetFeeds(AssetID, PageNo) {
            var request = $http({
                method: "get",
                url: "api/Common/GetAssetFeeds/" + AssetID + "/" + PageNo,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetObjectLanguageContent(LangEditID) {
            var request = $http({
                method: "get",
                url: "api/Common/GetObjectLanguageContent/" + LangEditID,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetlanguagetypeByID(languageTypeID) {
            var request = $http({
                method: "get",
                url: "api/Common/GetlanguagetypeByID/" + languageTypeID,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateJsonLanguageContent(LangContentObj) {
            var request = $http({
                method: "post",
                url: "api/Common/UpdateJsonLanguageContent/",
                params: {
                    action: "add",
                },
                data: LangContentObj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetModuleID(EntityId) {
            var request = $http({
                method: "get",
                url: "api/common/GetModuleID/" + EntityId,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertNewLanguage(LangContentType) {
            var request = $http({
                method: "post",
                url: "api/Common/InsertNewLanguage/",
                params: {
                    action: "add",
                },
                data: LangContentType
            });
            return (request.then(handleSuccess, handleError));
        }

        function getredirectPath(entityID) {
            var request = $http({
                method: "get",
                url: "api/Common/getredirectPath/" + entityID,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateAccLockPasswordPolicy(jobj) {
            var request = $http({
                method: "post",
                url: "api/Common/UpdateAccLockPasswordPolicy/",
                params: {
                    action: "add",
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAccLockDetails() {
            var request = $http({
                method: "get",
                url: "api/Common/GetAccLockDetails/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertUpdateDecimalSettings(DecimalSettingsVal) {
            var request = $http({
                method: "post",
                url: "api/Common/InsertUpdateDecimalSettings/",
                params: {
                    action: "add",
                },
                data: DecimalSettingsVal
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetDecimalSettingsValue() {
            var request = $http({
                method: "get",
                url: "api/common/GetDecimalSettingsValue/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function getNewsfeedforTask(EntityID, pageNo, islatestfeed) {
            var request = $http({
                method: "get",
                url: "api/common/GetEnityFeedsForFundingReq/" + EntityID + "/" + pageNo + "/" + islatestfeed,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetDalimSettings() {
            var request = $http({
                method: "get",
                url: "api/common/GetDalimSettings/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateDalimSettings(dataobj) {
            var request = $http({
                method: "post",
                url: "api/common/UpdateDalimSettings/",
                params: {
                    action: "add",
                },
                data: dataobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function updateDefaultFolderInfo(dataobj) {
            var request = $http({
                method: "post",
                url: "api/common/updateDefaultFolderInfo/",
                params: {
                    action: "add",
                },
                data: dataobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function getDefaultFolderInfo() {
            var request = $http({
                method: "get",
                url: "api/common/getDefaultFolderInfo/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdatetopIsviewedStatusNotification(jsonparameter) {
            var request = $http({
                method: "post",
                url: "api/common/UpdatetopIsviewedStatusNotification",
                params: {
                    action: "add"
                },
                data: jsonparameter
            });
            return (request.then(handleSuccess, handleError));
        }

        function SaveHolidayDetails(day) {
            var request = $http({
                method: "post",
                url: "api/common/SaveHolidayDetails/",
                params: {
                    action: "add",
                },
                data: {
                    "nonbusinessday": day
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetNonBusinessDays() {
            var request = $http({
                method: "get",
                url: "api/Common/GetNonBusinessDays/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function savecloudsettings(dataobj) {
            var request = $http({
                method: "post",
                url: "api/common/savecloudsettings/",
                params: {
                    action: "add",
                },
                data: dataobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function getcloudsettings() {
            var request = $http({
                method: "get",
                url: "api/common/getcloudsettings/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateNewFeedconfig(obj) {
            var request = $http({
                method: "post",
                url: "api/common/UpdateNewFeedconfig/",
                params: {
                    action: "add"
                },
                data: obj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetNewsFeedConfigInfo() {
            var request = $http({
                method: "get",
                url: "api/common/GetNewsFeedConfigInfo/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertHolidayDetails(holiday) {
            var request = $http({
                method: "post",
                url: "api/Common/InsertHolidayDetails/",
                params: {
                    action: "add",
                },
                data: holiday
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetHolidaysDetails() {
            var request = $http({
                method: "get",
                url: "api/Common/GetHolidaysDetails/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteHoliday(ID) {
            var request = $http({
                method: "delete",
                url: "api/common/DeleteHoliday/" + ID,
                params: {
                    action: "delete",
                },
            });
            return (request.then(handleSuccess, handleError));
        }
        function GetNavigationandLanguageConfig()
        {
            var request = $http({
                method: "get",
                url: "api/common/GetNavigationandLanguageConfig/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetObjectiveTabAdminSettings(LogoSettings, typeid) {
            var request = $http({
                method: "get",
                url: "api/common/GetObjectiveTabAdminSettings/" + LogoSettings + "/" + typeid,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function AdminSettingsObjectivetabRootLevelInsertUpdate(listSettings) {
            var request = $http({
                method: "post",
                url: "api/common/AdminSettingsObjectivetabRootLevelInsertUpdate/",
                params: {
                    action: "add",
                },
                data: listSettings
            });
            return (request.then(handleSuccess, handleError));
        }

        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject("An unknown error occurred."));
            }
            return ($q.reject(response.data.message));
        }

        function handleSuccess(response) {
            return (response.data);
        }
    }
    app.service("CommonService", ['$http', '$q', CommonService]);
})(angular, app);
///#source 1 1 /app/services/Metadata-service.js
(function (ng, app) {
    "use strict";

    function MetadataService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session');
        return ({
            EntityType: EntityType,
            PutEntityType: PutEntityType,
            DeleteEntityType: DeleteEntityType,
            InsertEntityTypeFeature: InsertEntityTypeFeature,
            DeleteEntityTypeFeature: DeleteEntityTypeFeature,
            Attribute: Attribute,
            PutAttribute: PutAttribute,
            DeleteAttribute: DeleteAttribute,
            Module: Module,
            PutModule: PutModule,
            GetModuleByID: GetModuleByID,
            Attributetype: Attributetype,
            PutAttributetype: PutAttributetype,
            DeleteAttributetype: DeleteAttributetype,
            DeleteModule: DeleteModule,
            Modulefeature: Modulefeature,
            PutModulefeature: PutModulefeature,
            GetEntityType: GetEntityType,
            GetEntityTypeIsAssociate: GetEntityTypeIsAssociate,
            GetEntityTypefromDB: GetEntityTypefromDB,
            GetFulfillmentFinicalAttribute: GetFulfillmentFinicalAttribute,
            GetEntityTypeByID: GetEntityTypeByID,
            GetAttributetype: GetAttributetype,
            GetAttributeTypeByEntityTypeID: GetAttributeTypeByEntityTypeID,
            GetAttributeByID: GetAttributeByID,
            DeleteModuleFeature: DeleteModuleFeature,
            GetMultiSelect: GetMultiSelect,
            Option: Option,
            PutOption: PutOption,
            DeleteOption: DeleteOption,
            TreeLevel: TreeLevel,
            GetTreelevelByAttributeID: GetTreelevelByAttributeID,
            GetAdminTreelevelByAttributeID: GetAdminTreelevelByAttributeID,
            PutTreeLevel: PutTreeLevel,
            DeleteTreeLevel: DeleteTreeLevel,
            InsertTreeNode: InsertTreeNode,
            InsertAttributeSequencePattern: InsertAttributeSequencePattern,
            DeleteTreeNode: DeleteTreeNode,
            TreeValue: TreeValue,
            PutTreeValue: PutTreeValue,
            DeleteTreeValue: DeleteTreeValue,
            Validation: Validation,
            PostValidation: PostValidation,
            GetAttributeValidationByEntityTypeId: GetAttributeValidationByEntityTypeId,
            GetValidationDationByEntitytype: GetValidationDationByEntitytype,
            DeleteAttributeValidation: DeleteAttributeValidation,
            DeleteValidation: DeleteValidation,
            GetAttribute: GetAttribute,
            GetAttributeWithLevels: GetAttributeWithLevels,
            GetNavigationType: GetNavigationType,
            GetModule: GetModule,
            GetModulefeature: GetModulefeature,
            GetMultiSelect1: GetMultiSelect1,
            GetOption: GetOption,
            GetOptionListID: GetOptionListID,
            GetAdminOptionListID: GetAdminOptionListID,
            GetAttributeSequenceByID: GetAttributeSequenceByID,
            GetOptionDetailListByID: GetOptionDetailListByID,
            GetTreelevel: GetTreelevel,
            GetTreeNode: GetTreeNode,
            GetAttributeTreeNode: GetAttributeTreeNode,
            GetAdminTreeNode: GetAdminTreeNode,
            GetTreeValue: GetTreeValue,
            GetValidation: GetValidation,
            ListSetting: ListSetting,
            EntityTypeAttributeRelation: EntityTypeAttributeRelation,
            GetEntityTypeAttributeRelation: GetEntityTypeAttributeRelation,
            GetEntityTypeAttributeRelationByID: GetEntityTypeAttributeRelationByID,
            GetEntityTypeAttributeRelationWithLevelsByID: GetEntityTypeAttributeRelationWithLevelsByID,
            GetDropDownTree: GetDropDownTree,
            DeleteEntityAttributeRelation: DeleteEntityAttributeRelation,
            SyncToDb: SyncToDb,
            GetFeature: GetFeature,
            GetEntityTypefeature: GetEntityTypefeature,
            GetEntityTypefeatureByID: GetEntityTypefeatureByID,
            GetVersionsCountAndCurrentVersion: GetVersionsCountAndCurrentVersion,
            UpdateActiveVersion: UpdateActiveVersion,
            UpdateWorkingVersion: UpdateWorkingVersion,
            GetTreeNodeByLevel: GetTreeNodeByLevel,
            GettingFilterAttribute: GettingFilterAttribute,
            GettingChildEntityTypes: GettingChildEntityTypes,
            InsertEntityTypeHierarchy: InsertEntityTypeHierarchy,
            GetAttributesForDetailBlock: GetAttributesForDetailBlock,
            SaveDetailBlock: SaveDetailBlock,
            SaveDetailBlockForLevels: SaveDetailBlockForLevels,
            UpdateDropDownTreePricing: UpdateDropDownTreePricing,
            SaveDetailBlockForTreeLevels: SaveDetailBlockForTreeLevels,
            GettingEntityTypeHierarchy: GettingEntityTypeHierarchy,
            GettingEntityTypeHierarchyForRootLevel: GettingEntityTypeHierarchyForRootLevel,
            GetOwnerForEntity: GetOwnerForEntity,
            GetOptionToBind: GetOptionToBind,
            DeleteEntityTypeHierarchy: DeleteEntityTypeHierarchy,
            GetOptionsFromXML: GetOptionsFromXML,
            GetFulfillmentEntityTypes: GetFulfillmentEntityTypes,
            GetFulfillmentEntityTypesfrCal: GetFulfillmentEntityTypesfrCal,
            GetFulfillmentAttribute: GetFulfillmentAttribute,
            GetFulfillmentAttributeOptions: GetFulfillmentAttributeOptions,
            GetAllEntityIds: GetAllEntityIds,
            ActivityRootLevel: ActivityRootLevel,
            ActivityDetail: ActivityDetail,
            ActivityDetailReport: ActivityDetailReport,
            CostCentreDetailReport: CostCentreDetailReport,
            ObjectiveDetailReport: ObjectiveDetailReport,
            CostCentreRootLevel: CostCentreRootLevel,
            CostCentreDetail: CostCentreDetail,
            GetActivityByID: GetActivityByID,
            ObjectiveRootLevel: ObjectiveRootLevel,
            ObjectiveDetail: ObjectiveDetail,
            GetPath: GetPath,
            DeleteOptionByAttributeID: DeleteOptionByAttributeID,
            DeleteSequencByeAttributeID: DeleteSequencByeAttributeID,
            InsertEntityHistory: InsertEntityHistory,
            GetEntityHistoryByID: GetEntityHistoryByID,
            GetTopActivityByID: GetTopActivityByID,
            GetTopMyTaskByID: GetTopMyTaskByID,
            GetWorkFlowDetails: GetWorkFlowDetails,
            CreateWorkFlow: CreateWorkFlow,
            GetAttributefromDB: GetAttributefromDB,
            GetAttributesforDetailFilter: GetAttributesforDetailFilter,
            GettingEntityTypeHierarchyForAdminTree: GettingEntityTypeHierarchyForAdminTree,
            InsertUpdatePredefinedWorkFlow: InsertUpdatePredefinedWorkFlow,
            GetPredefinedWorkflowByID: GetPredefinedWorkflowByID,
            GetWorkFlowDetailsByID: GetWorkFlowDetailsByID,
            GetWorkFlowTStepByID: GetWorkFlowTStepByID,
            GetWorkFlowStepPredefinedTaskByID: GetWorkFlowStepPredefinedTaskByID,
            DeleteWorkflowByID: DeleteWorkflowByID,
            GetPredefinedWorkflowFilesAttchedByID: GetPredefinedWorkflowFilesAttchedByID,
            DeletePredWorkflowFileByID: DeletePredWorkflowFileByID,
            DeletePredefinedWorkflowByID: DeletePredefinedWorkflowByID,
            GetModuleFeatures: GetModuleFeatures,
            GetModuleFeaturesForNavigation: GetModuleFeaturesForNavigation,
            UpdateFeature: UpdateFeature,
            GetTreeNodeByAttributeID: GetTreeNodeByAttributeID,
            SetWorkingVersionFlag: SetWorkingVersionFlag,
            GetMetadataVersion: GetMetadataVersion,
            InsertMetadataVersion: InsertMetadataVersion,
            GetTaskFulfillmentEntityTypes: GetTaskFulfillmentEntityTypes,
            GetXmlNodes_CheckIfValueExistsOrNot: GetXmlNodes_CheckIfValueExistsOrNot,
            GettingEntityForRootLevel: GettingEntityForRootLevel,
            GettingEntityTypeHierarchyForChildActivityType: GettingEntityTypeHierarchyForChildActivityType,
            GetAttributeRelationByIDs: GetAttributeRelationByIDs,
            InsertUpdateAttributeToAttributeRelations: InsertUpdateAttributeToAttributeRelations,
            DeleteAttributeToAttributeRelation: DeleteAttributeToAttributeRelation,
            GetAttributeToAttributeRelationsByID: GetAttributeToAttributeRelationsByID,
            GetTreeNodeByEntityID: GetTreeNodeByEntityID,
            GetAttributeToAttributeRelationsByIDForEntity: GetAttributeToAttributeRelationsByIDForEntity,
            GetAttributeToAttributeRelationsByIDForEntityInOverView: GetAttributeToAttributeRelationsByIDForEntityInOverView,
            GetAttributeOptionsInAttrToAttrRelations: GetAttributeOptionsInAttrToAttrRelations,
            EntityTypeStatusOptions: EntityTypeStatusOptions,
            DamTypeFileExtensionOptions: DamTypeFileExtensionOptions,
            GetEntityStatusOptions: GetEntityStatusOptions,
            GetDamTypeFileExtensionOptions: GetDamTypeFileExtensionOptions,
            GetAllDamTypeFileExtensionOptions: GetAllDamTypeFileExtensionOptions,
            DeleteEntityTypeStatusOptions: DeleteEntityTypeStatusOptions,
            DeleteDamTypeFileExtensionOptions: DeleteDamTypeFileExtensionOptions,
            GetEntityStatus: GetEntityStatus,
            RootLevelEntityTypeHierarchy: RootLevelEntityTypeHierarchy,
            ChildEntityTypeHierarchy: ChildEntityTypeHierarchy,
            MyworkSpaceDetails: MyworkSpaceDetails,
            GetOptionDetailListByIDOptimised: GetOptionDetailListByIDOptimised,
            GetWorkspacePath: GetWorkspacePath,
            ReportViewCreationAndPushSchema: ReportViewCreationAndPushSchema,
            GetAllModuleFeatures: GetAllModuleFeatures,
            InsertUpdateAttributeGroupAndAttributeRelation: InsertUpdateAttributeGroupAndAttributeRelation,
            GetAttributeGroup: GetAttributeGroup,
            GetAttributeGroupAttributeRelation: GetAttributeGroupAttributeRelation,
            DeleteAttributeGroup: DeleteAttributeGroup,
            DeleteAttributeGroupAttributeRelation: DeleteAttributeGroupAttributeRelation,
            InsertUpdateEntityTypeAttributeGroup: InsertUpdateEntityTypeAttributeGroup,
            GetEntityTypeAttributeGroupRelation: GetEntityTypeAttributeGroupRelation,
            DeleteEntityTypeAttributeGroupRelation: DeleteEntityTypeAttributeGroupRelation,
            GetAttributeGroupAttributeOptions: GetAttributeGroupAttributeOptions,
            GetEntityAttributesGroupValues: GetEntityAttributesGroupValues,
            DuplicateEntityType: DuplicateEntityType,
            GetEntityTypeAttributeRelationWithLevelsByIDForUserDetails: GetEntityTypeAttributeRelationWithLevelsByIDForUserDetails,
            GetUserDetailsAttributes: GetUserDetailsAttributes,
            GetOptionDetailListByIDForMyPage: GetOptionDetailListByIDForMyPage,
            GetUserRegistrationAttributes: GetUserRegistrationAttributes,
            GetOptionValuesForUserRegistration: GetOptionValuesForUserRegistration,
            GetDynamicAttrValidationDetails: GetDynamicAttrValidationDetails,
            SaveDetailBlockForLevelsFromMyPage: SaveDetailBlockForLevelsFromMyPage,
            GetUserVisiblity: GetUserVisiblity,
            InsertUserVisibleInfo: InsertUserVisibleInfo,
            InsertUpdateUserVisibleInfo: InsertUpdateUserVisibleInfo,
            addFinancialAttribute: addFinancialAttribute,
            addFinancialOption: addFinancialOption,
            GetFinancialAttribute: GetFinancialAttribute,
            DeleteFinancialAttribute: DeleteFinancialAttribute,
            DeleteFinancialOptionByAttributeID: DeleteFinancialOptionByAttributeID,
            UpdateFinancialMetadata: UpdateFinancialMetadata,
            DeleteFinMetadata: DeleteFinMetadata,
            UpdateFinMetadataSortOrder: UpdateFinMetadataSortOrder,
            GetFinancialAttributeOptions: GetFinancialAttributeOptions,
            DeleteFinancialOption: DeleteFinancialOption,
            GettingEntityTypeTreeStructure: GettingEntityTypeTreeStructure,
            GetSubEntityTypeAccessPermission: GetSubEntityTypeAccessPermission,
            InsertUpdateEntityTypeRoleAccess: InsertUpdateEntityTypeRoleAccess,
            GetEntityTypeRoleAcl: GetEntityTypeRoleAcl,
            DeleteEntityTypeRoleAcl: DeleteEntityTypeRoleAcl,
            GetRoleFeatures: GetRoleFeatures,
            SaveUpdateRoleFeatures: SaveUpdateRoleFeatures,
            GetAttributeTreeNodeByEntityID: GetAttributeTreeNodeByEntityID,
            GetDAMEntityTypes: GetDAMEntityTypes,
            GetWorkspacePermission: GetWorkspacePermission,
            GetDropDownTreePricingObject: GetDropDownTreePricingObject,
            GetDropDownTreePricingObjectFromParentDetail: GetDropDownTreePricingObjectFromParentDetail,
            SaveDetailBlockForLink: SaveDetailBlockForLink,
            DamDeleteEntityAttributeRelation: DamDeleteEntityAttributeRelation,
            DeleteEntitytypeAttributeGrpAccessRole: DeleteEntitytypeAttributeGrpAccessRole,
            GetAttributeGroupImportedFileColumnName: GetAttributeGroupImportedFileColumnName,
            InsertImportedAttributeGroupData: InsertImportedAttributeGroupData,
            FetchEntityStatusTree: FetchEntityStatusTree,
            GetTaskEntityType: GetTaskEntityType,
            GetTaskTypes: GetTaskTypes,
            GetEntityTaskType: GetEntityTaskType,
            InsertUpdateEntityTaskType: InsertUpdateEntityTaskType,
            GetEntityAttributesGroupLabelNames: GetEntityAttributesGroupLabelNames,
            CalenderRootLevel: CalenderRootLevel,
            CalenderDetail: CalenderDetail,
            GetTagOptionList: GetTagOptionList,
            InsertUpdateTagOption: InsertUpdateTagOption,
            DeleteTagOption: DeleteTagOption,
            AssetAccess: AssetAccess,
            DeleteAssetAccessOption: DeleteAssetAccessOption,
            GetAssestAccessSaved: GetAssestAccessSaved,
            GetAssignedAssetRoleIDs: GetAssignedAssetRoleIDs,
            GetAssignedAssetRoleName: GetAssignedAssetRoleName,
            GetAssetAccess: GetAssetAccess,
            ExportMetadataSettingsXml: ExportMetadataSettingsXml,
            ImportMetadataSettingsXml: ImportMetadataSettingsXml,
            SetIsCurrentWorkingXml: SetIsCurrentWorkingXml,
            GetOwnerName: GetOwnerName,
            CostCentreTreeDetail: CostCentreTreeDetail,
            getTreeCount: getTreeCount,
            CalenderDetailReport: CalenderDetailReport,
            getParentID: getParentID,
            GetEntityGlobalAccessInDetail: GetEntityGlobalAccessInDetail,
            InsertEntityGlobalAccessInDetail: InsertEntityGlobalAccessInDetail,
            GettingFilterEntityMember: GettingFilterEntityMember,
            GetTaskLinkedToEntity: GetTaskLinkedToEntity,
            GetListOfCmsCustomFont: GetListOfCmsCustomFont,
            InsertUpdateCmsCustomFont: InsertUpdateCmsCustomFont,
            DeleteCmsCustomFont: DeleteCmsCustomFont,
            InsertUpdateCmsCustomStyle: InsertUpdateCmsCustomStyle,
            GetListOfCmsCustomStyle: GetListOfCmsCustomStyle,
            DeleteCmsCustomStyle: DeleteCmsCustomStyle,
            InsertUpdateCmsTreeStyleSettings: InsertUpdateCmsTreeStyleSettings,
            GetCmsTreeStyle: GetCmsTreeStyle,
            SaveImageFromBaseFormat: SaveImageFromBaseFormat,
            CheckBeforeRemovechecklist: CheckBeforeRemovechecklist,
            InsertApprovalRole: InsertApprovalRole,
            GetApprovalRoles: GetApprovalRoles,
            DeleteApprovalRole: DeleteApprovalRole,
            GetEntityTypeAttributes: GetEntityTypeAttributes,
            updateentitytypehelptext: updateentitytypehelptext,
            GetAllFinancialAttribute: GetAllFinancialAttribute,
            UpdateFinancialHelptext: UpdateFinancialHelptext,
            ExportEntityHelpTextListtoExcel: ExportEntityHelpTextListtoExcel,
            InsertEntityHelpTextImport: InsertEntityHelpTextImport
            , InsertUpdateEntityObjectiveType: InsertUpdateEntityObjectiveType
            , GetObjectiveEntityType: GetObjectiveEntityType,

            getEntityToSetOverallStauts: getEntityToSetOverallStauts,
            getCurrentEntityStatus: getCurrentEntityStatus,
            updateEntityOverviewStatus: updateEntityOverviewStatus
        });

        function GetTaskLinkedToEntity(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetTaskLinkedToEntity/" + ID,
                parms: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function EntityType(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/EntityType/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function PutEntityType(formobj) {
            var request = $http({
                method: "put",
                url: "api/Metadata/PutEntityType/",
                params: {
                    action: "update"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteEntityType(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteEntityType/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertEntityTypeFeature(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertEntityTypeFeature/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteEntityTypeFeature(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteEntityTypeFeature/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function Attribute(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/Attribute/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function PutAttribute(formobj) {
            var request = $http({
                method: "put",
                url: "api/Metadata/PutAttribute/",
                params: {
                    action: "update"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteAttribute(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteAttribute/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function Module(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/Module/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function PutModule(formobj) {
            var request = $http({
                method: "put",
                url: "api/Metadata/PutModule/",
                params: {
                    action: "update"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetModuleByID(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetModuleByID/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function Attributetype(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/Attributetype/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function PutAttributetype(formobj) {
            var request = $http({
                method: "put",
                url: "api/Metadata/PutAttributetype/",
                params: {
                    action: "update"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteAttributetype(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteAttributetype/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteModule(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteModule/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function Modulefeature(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/Modulefeature/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function PutModulefeature(formobj) {
            var request = $http({
                method: "put",
                url: "api/Metadata/PutModulefeature/",
                params: {
                    action: "update"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityType(ModuleID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityType/" + ModuleID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityTypeIsAssociate() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityTypeIsAssociate/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityTypefromDB() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityTypefromDB/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetFulfillmentFinicalAttribute(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetFulfillmentFinicalAttribute/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityTypeByID(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityTypeByID/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributetype() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributetype/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeTypeByEntityTypeID(EntityTypeID, IsAdmin) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributeTypeByEntityTypeID/" + EntityTypeID + "/" + IsAdmin,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeByID(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributeByID/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteModuleFeature(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteModuleFeature/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetMultiSelect(EntityID, AttributeID, OptionID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetMultiSelect/" + EntityID + "/" + AttributeID + "/" + OptionID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function Option(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/Option/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function PutOption(formobj) {
            var request = $http({
                method: "put",
                url: "api/Metadata/PutOption/",
                params: {
                    action: "update"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteOption(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteOption/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function TreeLevel(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/TreeLevel/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTreelevelByAttributeID(AttributeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetTreelevelByAttributeID/" + AttributeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAdminTreelevelByAttributeID(AttributeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAdminTreelevelByAttributeID/" + AttributeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function PutTreeLevel(formobj) {
            var request = $http({
                method: "put",
                url: "api/Metadata/PutTreeLevel/",
                params: {
                    action: "update"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteTreeLevel(TreeLevelID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteTreeLevel/" + TreeLevelID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertTreeNode(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertTreeNode",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertAttributeSequencePattern(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertAttributeSequencePattern",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteTreeNode(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteTreeNode/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function TreeValue(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/TreeValue/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function PutTreeValue(formobj) {
            var request = $http({
                method: "put",
                url: "api/Metadata/PutTreeValue/",
                params: {
                    action: "update"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteTreeValue(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteTreeValue/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function Validation() {
            var request = $http({
                method: "post",
                url: "api/Metadata/Validation/",
                params: {
                    action: "add"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function PostValidation(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/PostValidation/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeValidationByEntityTypeId(EntityTypeID, AttributeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributeValidationByEntityTypeId/" + EntityTypeID + "/" + AttributeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetValidationDationByEntitytype(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetValidationDationByEntitytype/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteAttributeValidation(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteAttributeValidation/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteValidation(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteValidation/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttribute() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttribute/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeWithLevels() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributeWithLevels/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetNavigationType() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetNavigationType/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetModule() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetModule/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetModulefeature(Version) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetModulefeature/" + Version,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetMultiSelect1(Version) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetMultiSelect1/" + Version,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetOption(Version) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetOption/" + Version,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetOptionListID(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetOptionListID/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAdminOptionListID(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAdminOptionListID/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeSequenceByID(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributeSequenceByID/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetOptionDetailListByID(ID, EntityID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetOptionDetailListByID/" + ID + "/" + EntityID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTreelevel(Version) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetTreelevel/" + Version,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTreeNode(AttributeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetTreeNode/" + AttributeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeTreeNode(AttributeID, EntityID, Ischoosefromparent) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributeTreeNode/" + AttributeID + "/" + EntityID + "/" + Ischoosefromparent,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAdminTreeNode(AttributeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAdminTreeNode/" + AttributeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTreeValue(Version) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetTreeValue/" + Version,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetValidation(Version) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetValidation/" + Version,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function ListSetting(elementNode, entitytype) {
            var request = $http({
                method: "get",
                url: "api/Metadata/ListSetting/" + elementNode + "/" + entitytype,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function EntityTypeAttributeRelation(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/EntityTypeAttributeRelation/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityTypeAttributeRelation(Version) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityTypeAttributeRelation/" + Version,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityTypeAttributeRelationByID(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityTypeAttributeRelationByID/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetDropDownTree(choosefromparent, AttributeID, IsInheritFromParent, Isfromparent, Entityid, ParentId) {
            if (choosefromparent) GetDropDownTreePricingObjectFromParentDetail(AttributeID, IsInheritFromParent, Isfromparent, Entityid, ParentId);
            else GetDropDownTreePricingObject(AttributeID, IsInheritFromParent, Isfromparent, Entityid, ParentId);
        }

        function GetEntityTypeAttributeRelationWithLevelsByID(ID, ParentID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityTypeAttributeRelationWithLevelsByID/" + ID + "/" + ParentID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteEntityAttributeRelation(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteEntityAttributeRelation/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function SyncToDb() {
            var request = $http({
                method: "post",
                url: "api/Metadata/SyncToDb/",
                params: {
                    action: "add"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetFeature() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetFeature/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityTypefeature(Version) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityTypefeature/" + Version,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityTypefeatureByID(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityTypefeatureByID/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetVersionsCountAndCurrentVersion(key) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetVersionsCountAndCurrentVersion/" + key,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateActiveVersion(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/UpdateActiveVersion/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateWorkingVersion(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/UpdateWorkingVersion/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTreeNodeByLevel(AttributeID, Level) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetTreeNodeByLevel/" + AttributeID + "/" + Level,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GettingFilterAttribute(formobj) {
            var request = $http({
                method: "post",
                ignoreLoadingBar: true,
                url: "api/Metadata/GettingFilterAttribute/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GettingChildEntityTypes(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GettingChildEntityTypes/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertEntityTypeHierarchy(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertEntityTypeHierarchy/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributesForDetailBlock(EntityID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributesForDetailBlock/" + EntityID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function SaveDetailBlock(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/SaveDetailBlock/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function SaveDetailBlockForLevels(jobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/SaveDetailBlockForLevels/",
                params: {
                    action: "add"
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateDropDownTreePricing(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/UpdateDropDownTreePricing/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function SaveDetailBlockForTreeLevels(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/SaveDetailBlockForTreeLevels/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GettingEntityTypeHierarchy(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GettingEntityTypeHierarchy/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GettingEntityTypeHierarchyForRootLevel(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GettingEntityTypeHierarchyForRootLevel/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetOwnerForEntity(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetOwnerForEntity/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetOptionToBind() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetOwnerForEntity/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteEntityTypeHierarchy(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteEntityTypeHierarchy/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetOptionsFromXML(elementNode, typeid) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetOptionsFromXML/" + elementNode + "/" + typeid,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetFulfillmentEntityTypes() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetFulfillmentEntityTypes/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetFulfillmentEntityTypesfrCal() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetFulfillmentEntityTypesfrCal/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetFulfillmentAttribute(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetFulfillmentAttribute/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetFulfillmentAttributeOptions(AttributeID, AttributeLevel) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetFulfillmentAttributeOptions/" + AttributeID + "/" + AttributeLevel,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAllEntityIds() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAllEntityIds/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function ActivityRootLevel(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/ActivityRootLevel/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function ActivityDetail(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/ActivityDetail/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function ActivityDetailReport(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/ActivityDetailReport/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function CostCentreDetailReport(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/CostCentreDetailReport/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function ObjectiveDetailReport(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/ObjectiveDetailReport/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function ActivityFilter(dimenID, StartRowNo, MaxNoofRow, FilterID, SortOrderColumn, IsDesc, IncludeChildren, EntityID, UserID, Level, ExpandingEntityID, IsSingleID, getactivities) {
            if (dimenID = "1") CostCentreRootLevel(StartRowNo, MaxNoofRow, FilterID, SortOrderColumn, IsDesc, IncludeChildren, EntityID, UserID, Level, getactivities);
            else if (dimenID = "2") ActivityDetail(StartRowNo, MaxNoofRow, FilterID, SortOrderColumn, IsDesc, IncludeChildren, EntityID, Level, ExpandingEntityID, IsSingleID, getactivities);
        }

        function CostCentreRootLevel(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/CostCentreRootLevel/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function CostCentreDetail(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/CostCentreDetail/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetActivityByID(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/GetActivityByID/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function ObjectiveRootLevel(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/ObjectiveRootLevel/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function ObjectiveDetail(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/ObjectiveDetail/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetPath(EntityID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetPath/" + EntityID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteOptionByAttributeID(attributeid) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteOptionByAttributeID/" + attributeid,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteSequencByeAttributeID(attributeid) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteSequencByeAttributeID/" + attributeid,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertEntityHistory(EntityID, UserID) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertEntityHistory/",
                params: {
                    action: "add"
                },
                data: {
                    EntityID: EntityID,
                    UserID: UserID
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityHistoryByID(UserID, Topx) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityHistoryByID/" + UserID + "/" + Topx,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTopActivityByID(UserID, Topx) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetTopActivityByID/" + UserID + "/" + Topx,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTopMyTaskByID(UserID, Topx) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetTopMyTaskByID/" + UserID + "/" + Topx,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetWorkFlowDetails() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetWorkFlowDetails/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function CreateWorkFlow(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/CreateWorkFlow/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributefromDB() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributefromDB/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributesforDetailFilter() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributesforDetailFilter/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GettingEntityTypeHierarchyForAdminTree(EntityTypeID, ModuleID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GettingEntityTypeHierarchyForAdminTree/" + EntityTypeID + "/" + ModuleID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertUpdatePredefinedWorkFlow(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertUpdatePredefinedWorkFlow/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetPredefinedWorkflowByID(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetPredefinedWorkflowByID/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetWorkFlowDetailsByID(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetWorkFlowDetailsByID/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetWorkFlowTStepByID(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetWorkFlowTStepByID/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetWorkFlowStepPredefinedTaskByID(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetWorkFlowStepPredefinedTaskByID/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteWorkflowByID(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteWorkflowByID/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetPredefinedWorkflowFilesAttchedByID(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetPredefinedWorkflowFilesAttchedByID/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeletePredWorkflowFileByID(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeletePredWorkflowFileByID/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeletePredefinedWorkflowByID(ID, Name, Description, WorkflowType) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeletePredefinedWorkflowByID/" + ID + "/" + Name + "/" + Description + "/" + WorkflowType,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetModuleFeatures(moduleID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetModuleFeatures/" + moduleID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetModuleFeaturesForNavigation(moduleID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetModuleFeaturesForNavigation/" + moduleID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateFeature(formobj) {
            var request = $http({
                method: "put",
                url: "api/Metadata/UpdateFeature/",
                params: {
                    action: "update"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTreeNodeByAttributeID(AttributeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetTreeNodeByAttributeID/" + AttributeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function SetWorkingVersionFlag(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/SetWorkingVersionFlag/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetMetadataVersion() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetMetadataVersion/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertMetadataVersion(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertMetadataVersion/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTaskFulfillmentEntityTypes() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetTaskFulfillmentEntityTypes/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetXmlNodes_CheckIfValueExistsOrNot() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetXmlNodes_CheckIfValueExistsOrNot/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GettingEntityForRootLevel(IsRootLevel) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GettingEntityForRootLevel/" + IsRootLevel,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GettingEntityTypeHierarchyForChildActivityType(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GettingEntityTypeHierarchyForChildActivityType/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeRelationByIDs(formobj) {

            var request = $http({
                method: "post",
                url: "api/Metadata/GetAttributeRelationByIDs/",
                params: {
                    action: "add"
                },
                data:
                     formobj
            });
            return (request.then(handleSuccess, handleError));

        }

        function InsertUpdateAttributeToAttributeRelations(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertUpdateAttributeToAttributeRelations/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteAttributeToAttributeRelation(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteAttributeToAttributeRelation/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeToAttributeRelationsByID(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributeToAttributeRelationsByID/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTreeNodeByEntityID(AttributeID, ParentEntityID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetTreeNodeByEntityID/" + AttributeID + "/" + ParentEntityID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeToAttributeRelationsByIDForEntity(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributeToAttributeRelationsByIDForEntity/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeToAttributeRelationsByIDForEntityInOverView(EntityTypeID, EntityID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributeToAttributeRelationsByIDForEntityInOverView/" + EntityTypeID + "/" + EntityID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeOptionsInAttrToAttrRelations(Attrval) {
            var request = $http({
                method: "post",
                url: "api/Metadata/GetAttributeOptionsInAttrToAttrRelations/",
                params: {
                    action: "add"
                },
                data: Attrval
            });
            return (request.then(handleSuccess, handleError));
        }

        function EntityTypeStatusOptions(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/EntityTypeStatusOptions/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DamTypeFileExtensionOptions(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/DamTypeFileExtensionOptions/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityStatusOptions(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityStatusOptions/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetDamTypeFileExtensionOptions(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetDamTypeFileExtensionOptions/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAllDamTypeFileExtensionOptions() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAllDamTypeFileExtensionOptions/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteEntityTypeStatusOptions(EntityTypeID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteEntityTypeStatusOptions/" + EntityTypeID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteDamTypeFileExtensionOptions(EntityTypeID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteDamTypeFileExtensionOptions/" + EntityTypeID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityStatus(entityTypeID, isAdmin, entityId) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityStatus/" + entityTypeID + "/" + isAdmin + "/" + entityId,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function RootLevelEntityTypeHierarchy() {
            var request = $http({
                method: "get",
                url: "api/Metadata/RootLevelEntityTypeHierarchy/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function ChildEntityTypeHierarchy() {
            var request = $http({
                method: "get",
                url: "api/Metadata/ChildEntityTypeHierarchy/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function MyworkSpaceDetails(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/MyworkSpaceDetails/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetOptionDetailListByIDOptimised(EntityTypeID, EntityID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetOptionDetailListByIDOptimised/" + EntityTypeID + "/" + EntityID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetWorkspacePath(EntityID, IsWorkspace) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetWorkspacePath/" + EntityID + "/" + IsWorkspace,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function ReportViewCreationAndPushSchema() {
            var request = $http({
                method: "get",
                url: "api/Metadata/ReportViewCreationAndPushSchema/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAllModuleFeatures(moduleID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAllModuleFeatures/" + moduleID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertUpdateAttributeGroupAndAttributeRelation(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertUpdateAttributeGroupAndAttributeRelation/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeGroup() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributeGroup/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeGroupAttributeRelation(AttributeGroupID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributeGroupAttributeRelation/" + AttributeGroupID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteAttributeGroup(attributegroupid) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteAttributeGroup/" + attributegroupid,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteAttributeGroupAttributeRelation(attributeRelationId) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteAttributeGroupAttributeRelation/" + attributeRelationId,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertUpdateEntityTypeAttributeGroup(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertUpdateEntityTypeAttributeGroup/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityTypeAttributeGroupRelation(entitytypeId, EntityID, AttributeGroupId) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityTypeAttributeGroupRelation/" + entitytypeId + "/" + EntityID + "/" + AttributeGroupId,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteEntityTypeAttributeGroupRelation(attributegroupId) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteEntityTypeAttributeGroupRelation/" + attributegroupId,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeGroupAttributeOptions(ID, EntityID, AttributeRecordID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributeGroupAttributeOptions/" + ID + "/" + EntityID + "/" + AttributeRecordID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityAttributesGroupValues(EntityID, EntityTypeID, GroupID, IsCmsContent) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityAttributesGroupValues/" + EntityID + "/" + EntityTypeID + "/" + GroupID + "/" + IsCmsContent,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DuplicateEntityType(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/DuplicateEntityType/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityTypeAttributeRelationWithLevelsByIDForUserDetails(ID, ParentID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityTypeAttributeRelationWithLevelsByIDForUserDetails/" + ID + "/" + ParentID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetUserDetailsAttributes(TypeID, UserID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetUserDetailsAttributes/" + TypeID + "/" + UserID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetOptionDetailListByIDForMyPage(ID, UserID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetOptionDetailListByIDForMyPage/" + ID + "/" + UserID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetUserRegistrationAttributes() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetUserRegistrationAttributes/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetOptionValuesForUserRegistration() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetOptionValuesForUserRegistration/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetDynamicAttrValidationDetails() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetDynamicAttrValidationDetails/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function SaveDetailBlockForLevelsFromMyPage(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/SaveDetailBlockForLevelsFromMyPage/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetUserVisiblity() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetUserVisiblity/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertUserVisibleInfo(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertUserVisibleInfo/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertUpdateUserVisibleInfo(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertUpdateUserVisibleInfo/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertSetUpToolAttributes(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertSetUpToolAttributes/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function addFinancialAttribute(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/addFinancialAttribute/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function addFinancialOption(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/addFinancialOption/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetFinancialAttribute() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetFinancialAttribute/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteFinancialAttribute(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/DeleteFinancialAttribute/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteFinancialOptionByAttributeID(attributeid) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteFinancialOptionByAttributeID/" + attributeid,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateFinancialMetadata(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/UpdateFinancialMetadata/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteFinMetadata(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteFinMetadata/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateFinMetadataSortOrder(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/UpdateFinMetadataSortOrder/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetFinancialAttributeOptions(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetFinancialAttributeOptions/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteFinancialOption(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteFinancialOption/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GettingEntityTypeTreeStructure(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GettingEntityTypeTreeStructure/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetSubEntityTypeAccessPermission(EntityID, EntityTypeID, moduleID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetSubEntityTypeAccessPermission/" + EntityID + "/" + EntityTypeID + "/" + moduleID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertUpdateEntityTypeRoleAccess(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertUpdateEntityTypeRoleAccess/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityTypeRoleAcl(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityTypeRoleAcl/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteEntityTypeRoleAcl(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteEntityTypeRoleAcl/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetRoleFeatures(GlobalRoleID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetRoleFeatures/" + GlobalRoleID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function SaveUpdateRoleFeatures(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/SaveUpdateRoleFeatures/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeTreeNodeByEntityID(AttributeID, EntityID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributeTreeNodeByEntityID/" + AttributeID + "/" + EntityID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetDAMEntityTypes() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetDAMEntityTypes/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetWorkspacePermission() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetWorkspacePermission/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetDropDownTreePricingObject(AttributeID, IsInheritFromParent, Isfromparent, Entityid, ParentId) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetDropDownTreePricingObject/" + AttributeID + "/" + IsInheritFromParent + "/" + Isfromparent + "/" + Entityid + "/" + ParentId,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetDropDownTreePricingObjectFromParentDetail(AttributeID, IsInheritFromParent, Isfromparent, Entityid, ParentId) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetDropDownTreePricingObjectFromParentDetail/" + AttributeID + "/" + IsInheritFromParent + "/" + Isfromparent + "/" + Entityid + "/" + ParentId,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function SaveDetailBlockForLink(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/SaveDetailBlockForLink/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DamDeleteEntityAttributeRelation(ID, AttributeID, EntitytypeID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DamDeleteEntityAttributeRelation/" + ID + "/" + AttributeID + "/" + EntitytypeID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteEntitytypeAttributeGrpAccessRole(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteEntitytypeAttributeGrpAccessRole/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeGroupImportedFileColumnName(FileID) {
            var request = $http({
                method: "post",
                url: "api/Metadata/GetAttributeGroupImportedFileColumnName/",
                params: {
                    action: "add"
                },
                data: { 'FileID': FileID }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertImportedAttributeGroupData(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertImportedAttributeGroupData/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function FetchEntityStatusTree(EntityID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/FetchEntityStatusTree/" + EntityID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTaskEntityType() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetTaskEntityType/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTaskTypes() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetTaskTypes/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityTaskType(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityTaskType/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertUpdateEntityTaskType(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertUpdateEntityTaskType/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityAttributesGroupLabelNames(EntityID, GroupID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityAttributesGroupLabelNames/" + EntityID + "/" + GroupID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function CalenderRootLevel(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/CalenderRootLevel/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function CalenderDetail(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/CalenderDetail/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTagOptionList(IsAdmin) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetTagOptionList/" + IsAdmin,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertUpdateTagOption(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertUpdateTagOption/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteTagOption(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteTagOption/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function AssetAccess(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/AssetAccess/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteAssetAccessOption(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteAssetAccessOption/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAssestAccessSaved() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAssestAccessSaved/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAssignedAssetRoleIDs(AssetId) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAssignedAssetRoleIDs/" + AssetId,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAssignedAssetRoleName(RoleId) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAssignedAssetRoleName/" + RoleId,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAssetAccess() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAssetAccess/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function ExportMetadataSettingsXml(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/ExportMetadataSettingsXml/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function ImportMetadataSettingsXml(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/ImportMetadataSettingsXml/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function CostCentreTreeDetail(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/CostCentreTreeDetail/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function SetIsCurrentWorkingXml() {
            var request = $http({
                method: "post",
                url: "api/Metadata/SetIsCurrentWorkingXml/",
                params: {
                    action: "add"
                },
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetOwnerName(EntityID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetOwnerName/" + EntityID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function getTreeCount(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/getTreeCount/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function CalenderDetailReport(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/CalenderDetailReport/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function getParentID(ID) {
            var request = $http({
                method: 'get',
                url: 'api/metadata/getParentID/' + ID,
                params: {
                    action: 'get'
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityGlobalAccessInDetail(EntityID) {
            var request = $http({
                method: 'get',
                url: 'api/metadata/GetEntityGlobalAccessInDetail/' + EntityID,
                params: {
                    action: 'get'
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertEntityGlobalAccessInDetail(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertEntityGlobalAccessInDetail/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetListOfCmsCustomFont() {
            var request = $http({
                method: 'get',
                url: 'api/metadata/GetListOfCmsCustomFont',
                params: {
                    action: 'get'
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertUpdateCmsCustomFont(customfonts) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertUpdateCmsCustomFont/",
                params: {
                    action: "add"
                },
                data: customfonts
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteCmsCustomFont(ProviderId) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteCmsCustomFont/" + ProviderId,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertUpdateCmsCustomStyle(customstyle) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertUpdateCmsCustomStyle/",
                params: {
                    action: "add"
                },
                data: customstyle
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetListOfCmsCustomStyle() {
            var request = $http({
                method: 'get',
                url: 'api/metadata/GetListOfCmsCustomStyle',
                params: {
                    action: 'get'
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteCmsCustomStyle(Id) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteCmsCustomStyle/" + Id,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertUpdateCmsTreeStyleSettings(treestyle) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertUpdateCmsTreeStyleSettings/",
                params: {
                    action: "add"
                },
                data: treestyle
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetCmsTreeStyle() {
            var request = $http({
                method: 'get',
                url: 'api/metadata/GetCmsTreeStyle',
                params: {
                    action: 'get'
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function SaveImageFromBaseFormat(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/SaveImageFromBaseFormat/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GettingFilterEntityMember(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/GettingFilterEntityMember/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function CheckBeforeRemovechecklist(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/CheckBeforeRemovechecklist/" + ID,
                parms: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetApprovalRoles() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetApprovalRoles/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertApprovalRole(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertApprovalRole/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteApprovalRole(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteApprovalRole/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }
        function UpdateFinancialHelptext(fromobj) {
            var request = $http({
                method: "post",
                url: 'api/Metadata/UpdateFinancialHelptext',
                parms: {
                    action: "add"
                },
                data: fromobj

            });
            return (request.then(handleSuccess, handleError));
        }
        function GetAllFinancialAttribute() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAllFinancialAttribute",
                parms: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityTypeAttributes() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityTypeAttributes",
                parms: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function updateentitytypehelptext(fromobj) {
            var request = $http({
                method: "post",
                url: 'api/Metadata/updateentitytypehelptext',
                parms: {
                    action: "add"
                },
                data: fromobj

            });
            return (request.then(handleSuccess, handleError));
        }

        function ExportEntityHelpTextListtoExcel() {
            var request = $http({
                method: "get",
                url: "api/Metadata/ExportEntityHelpTextListtoExcel",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertEntityHelpTextImport(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertEntityHelpTextImport/",
                params: {
                    action: "add"
                },
                data: formobj
            });

            return (request.then(handleSuccess, handleError));
        }

        function InsertUpdateEntityObjectiveType(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertUpdateEntityObjectiveType/",
                params: {
                    action: "add"
                },
                data:
                     formobj

            });
            return (request.then(handleSuccess, handleError));
        }
        function GetObjectiveEntityType(IsAdmin) {

            var request = $http({
                method: "get",
                url: "api/Metadata/GetObjectiveEntityType/" + IsAdmin,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function getEntityToSetOverallStauts(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/getEntityToSetOverallStauts/" + ID,
                parms: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function getCurrentEntityStatus(entityID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/getCurrentEntityStatus/" + entityID,
                parms: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function updateEntityOverviewStatus(entityTypeId, entityID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/updateEntityOverviewStatus/" + entityTypeId + "/" + entityID,
                parms: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject("An unknown error occurred."));
            }
            return ($q.reject(response.data.message));
        }

        function handleSuccess(response) {
            response.data.StatusCode = response.status;
            return (response.data);
        }
        
    }
    app.service("MetadataService", ['$http', '$q', MetadataService]);
})(angular, app);
///#source 1 1 /app/services/dam-service.js
(function(ng,app){"use strict";function DamService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({GetSearchCriteriaAdminSettings:GetSearchCriteriaAdminSettings,GetAttributeDAMCreate:GetAttributeDAMCreate,GetAllExtensionTypesforDAM:GetAllExtensionTypesforDAM,GetDAMAttribute:GetDAMAttribute,DAMGetAdminSettings:DAMGetAdminSettings,DAMadminSettingsforRootLevelInsertUpdate:DAMadminSettingsforRootLevelInsertUpdate,PostDAMadminSettingsforRootLevelInsertUpdate:PostDAMadminSettingsforRootLevelInsertUpdate,GetDAMCommonAttribute:GetDAMCommonAttribute,GetDamAttributeRelation:GetDamAttributeRelation,GetOwner:GetOwner,GetEntityDamFolders:GetEntityDamFolders,SaveAsset:SaveAsset,GetEntityAsset:GetEntityAsset,GetLightBoxAsset:GetLightBoxAsset,GetAttributeDetails:GetAttributeDetails,GetDAMViewSettings:GetDAMViewSettings,CreateNewFolder:CreateNewFolder,UpdateFolder:UpdateFolder,deleteFolder:deleteFolder,deleteAssets:deleteAssets,SaveFiletoAsset:SaveFiletoAsset,UpdateAssetVersion:UpdateAssetVersion,DownloadFiles:DownloadFiles,DuplicateAssetFiles:DuplicateAssetFiles,CheckPreviewGeneratorID:CheckPreviewGeneratorID,SaveDetailBlockForAssets:SaveDetailBlockForAssets,UpdateAssetAccess:UpdateAssetAccess,PublishAssets:PublishAssets,GetAllAssetTypesforDAM:GetAllAssetTypesforDAM,SaveBlankAsset:SaveBlankAsset,SendMail:SendMail,MoveFilesToUploadImagesToCreateTask:MoveFilesToUploadImagesToCreateTask,DeleteAttachmentVersionByAssetID:DeleteAttachmentVersionByAssetID,GetOptimakerSettings:GetOptimakerSettings,GetOptimakerCatagories:GetOptimakerCatagories,GetCategoryTreeCollection:GetCategoryTreeCollection,InsertUpdateCatergory:InsertUpdateCatergory,InsertUpdateOptimakerSettings:InsertUpdateOptimakerSettings,DeleteOptimakeSetting:DeleteOptimakeSetting,GetAllOptimakerSettings:GetAllOptimakerSettings,GetOptimakerSettingsBaseURL:GetOptimakerSettingsBaseURL,GetMediaGeneratorSettingsBaseURL:GetMediaGeneratorSettingsBaseURL,GetStatusFilteredEntityAsset:GetStatusFilteredEntityAsset,CreateMediaGeneratorAsset:CreateMediaGeneratorAsset,DeleteOptimakerCategory:DeleteOptimakerCategory,UpdateAssetDetails:UpdateAssetDetails,GetDAMViewAdminSettings:GetDAMViewAdminSettings,UpdateDamViewStatus:UpdateDamViewStatus,GetDAMToolTipSettings:GetDAMToolTipSettings,GetWordTemplateSettings:GetWordTemplateSettings,DeleteWordtempSetting:DeleteWordtempSetting,InsertUpdateWordTemplateSettings:InsertUpdateWordTemplateSettings,CreateDOCGeneratedAsset:CreateDOCGeneratedAsset,GetPublishedAssets:GetPublishedAssets,AddPublishedFilesToDAM:AddPublishedFilesToDAM,AddPublishedLinkFiles:AddPublishedLinkFiles,getkeyvaluefromwebconfig:getkeyvaluefromwebconfig,GetAssetActiveFileinfo:GetAssetActiveFileinfo,GetMimeType:GetMimeType,DeleteFileProfile:DeleteFileProfile,InsertUpdateProfileFiles:InsertUpdateProfileFiles,CreateCropRescale:CreateCropRescale,GetProfileFiles:GetProfileFiles,GetProfileFilesByUser:GetProfileFilesByUser,GetBreadCrumFolderPath:GetBreadCrumFolderPath,CreateNewsFeedinfo:CreateNewsFeedinfo,GetAllFolderStructure:GetAllFolderStructure,SaveCropedImageForLightBox:SaveCropedImageForLightBox,GetMediaAssets:GetMediaAssets,GetMediaBankFilterAttributes:GetMediaBankFilterAttributes,GetCustomFilterAttributes:GetCustomFilterAttributes,MoveAssets:MoveAssets,SaveDetailBlockForLink:SaveDetailBlockForLink,AttachAssets:AttachAssets,AttachAssetsforProofTask:AttachAssetsforProofTask,CreateproofTask:CreateproofTask,changeAssettypeservice:changeAssettypeservice,GetAssetAttributueswithTypeID:GetAssetAttributueswithTypeID,SaveAssetTypeofAsset:SaveAssetTypeofAsset,SendMailWithMetaData:SendMailWithMetaData,UpdateAssetThumpnail:UpdateAssetThumpnail,GetlistoflinkedAsset:GetlistoflinkedAsset,DownloadAssetWithMetadata:DownloadAssetWithMetadata,IsActiveLinkEntity:IsActiveLinkEntity,UnPublishAssets:UnPublishAssets,GetReformatProfile:GetReformatProfile,SaveReformatAsset:SaveReformatAsset,UpdateAssetAccessSettings:UpdateAssetAccessSettings,GetAssetDetsforSelectedAssets:GetAssetDetsforSelectedAssets,GetAssetsbasedonFolderandEntity:GetAssetsbasedonFolderandEntity,GetExternalAssetContent:GetExternalAssetContent,InsertUpdateExternalAssetContent:InsertUpdateExternalAssetContent,GetAssetCategoryTreeCollection:GetAssetCategoryTreeCollection,InsertUpdateAssetCatergory:InsertUpdateAssetCatergory,DeleteAssetCategory:DeleteAssetCategory,GetAssetTypeAssetCategoryRelation:GetAssetTypeAssetCategoryRelation,GetSelectedCategoryTreeCollection:GetSelectedCategoryTreeCollection,InsertUpdateAssetCategoryRelation:InsertUpdateAssetCategoryRelation,GetAssetCategoryTree:GetAssetCategoryTree,GetAssetCategoryPathInAssetEdit:GetAssetCategoryPathInAssetEdit,GetAllAssetsBasedOnFolderandEntity:GetAllAssetsBasedOnFolderandEntity,GetAssetTypeFileExtensionRelation:GetAssetTypeFileExtensionRelation,InsertAssetTypeFileExtension:InsertAssetTypeFileExtension,DeleteAssetTypeFileExtension:DeleteAssetTypeFileExtension,GetAssetSnippetTemplates:GetAssetSnippetTemplates,getassetSnippetData:getassetSnippetData,GetDamTreeNodeByEntityID:GetDamTreeNodeByEntityID,GetDamTreeNode:GetDamTreeNode,GetDropDownTreePricingObject:GetDropDownTreePricingObject,GetDropDownTreePricingObjectFromParentDetail:GetDropDownTreePricingObjectFromParentDetail,UpdateDropDownTreePricing:UpdateDropDownTreePricing,InsertAssetPeriod:InsertAssetPeriod,AssetPeriod:AssetPeriod,DeleteAssetPeriod:DeleteAssetPeriod,GetAttributeTreeNode:GetAttributeTreeNode,SaveDetailBlockForTreeLevels:SaveDetailBlockForTreeLevels,CreateReformatProfile:CreateReformatProfile,ReformatRescale:ReformatRescale,AssetRoleAccess:AssetRoleAccess,UpdateAssetImageName:UpdateAssetImageName,getsnippethoturl:getsnippethoturl,GetOptionDetailListByAssetID:GetOptionDetailListByAssetID,Get2ImagineTemplateUrl:Get2ImagineTemplateUrl,Download2ImagineAsset:Download2ImagineAsset,saveTemplateEngine:saveTemplateEngine,getAllTemplateEngines:getAllTemplateEngines,deleteTemplateEngine:deleteTemplateEngine,getServerUrls:getServerUrls,Get2ImagineResourceUrl:Get2ImagineResourceUrl,getCurrentTemplateAssetInfo:getCurrentTemplateAssetInfo,IsFileExist:IsFileExist,S3Settings:S3Settings,copydeleteTemplateAsset:copydeleteTemplateAsset,});function GetSearchCriteriaAdminSettings(LogoSettings,Key,typeid){var request=$http({method:"get",url:"api/Dam/GetSearchCriteriaAdminSettings/"+LogoSettings+"/"+Key+"/"+typeid,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function PostDAMadminSettingsforRootLevelInsertUpdate(jobj){var request=$http({method:"post",url:"api/Dam/PostDAMadminSettingsforRootLevelInsertUpdate/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function GetDAMCommonAttribute(){var request=$http({method:"get",url:"api/Dam/GetDAMCommonAttribute/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function SaveAsset(SaveAsset){var request=$http({method:"post",url:"api/dam/CreateAsset/",params:{action:"add"},data:{saveAsset:SaveAsset}});return(request.then(handleSuccess,handleError));}
function SaveBlankAsset(jobj){var request=$http({method:"post",url:"api/dam/CreateBlankAsset/",params:{action:"add"},data:{saveAsset:jobj}});return(request.then(handleSuccess,handleError));}
function SaveFiletoAsset(fileDetails){var request=$http({method:"post",url:"api/dam/SaveFiletoAsset/",params:{action:"add"},data:{fileDetails:fileDetails}});return(request.then(handleSuccess,handleError));}
function GetDamAttributeRelation(damid){var request=$http({method:"get",url:"api/dam/GetDamAttributeRelation/"+damid,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetDAMAttribute(EntityTypeID){var request=$http({method:"get",url:"api/dam/GetDAMAttribute/"+EntityTypeID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetAttributeDetails(damid){var request=$http({method:"get",ignoreLoadingBar:true,url:"api/dam/GetAssetAttributesDetails/"+damid,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetAllExtensionTypesforDAM(){var request=$http({method:"get",url:"api/dam/GetDAMExtensions/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetOwner(ID){var request=$http({method:"get",url:"api/user/GetUserById/"+ID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetEntityDamFolders(EntityID){var request=$http({method:"get",url:"api/dam/GetEntityDamFolder/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetEntityAsset(FolderID,EntityID,View,Orderby,PageNo,ViewAll){var request=$http({method:"get",url:"api/dam/GetAssets/"+FolderID+"/"+EntityID+"/"+View+"/"+Orderby+"/"+PageNo+"/"+ViewAll,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetLightBoxAsset(jobj){var request=$http({method:"post",url:"api/dam/GetLightboxAssets/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function GetDAMViewSettings(){var request=$http({method:"get",url:"api/dam/GetDAMViewSettings/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function CreateNewFolder(folderObj){var request=$http({method:"post",url:"api/dam/CreateFolder/",params:{action:"add"},data:folderObj});return(request.then(handleSuccess,handleError));}
function UpdateFolder(folderObj){var request=$http({method:"post",url:"api/dam/UpdateFolder/",params:{action:"add"},data:folderObj});return(request.then(handleSuccess,handleError));}
function DownloadFiles(arrObj){var request=$http({method:"post",url:"api/dam/DownloadDamZip/",params:{action:"add"},data:{AssetArr:arrObj}});return(request.then(handleSuccess,handleError));}
function deleteFolder(idArr){var request=$http({method:"post",url:"api/dam/DeleteFolder/",params:{action:"add"},data:{FolderArr:idArr}});return(request.then(handleSuccess,handleError));}
function deleteAssets(idArr){var request=$http({method:"post",url:"api/dam/DeleteAssets/",params:{action:"add"},data:idArr});return(request.then(handleSuccess,handleError));}
function UpdateAssetAccess(jobj){var request=$http({method:"post",url:"api/dam/UpdateAssetAccess/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function DuplicateAssetFiles(arrObj){var request=$http({method:"post",url:"api/dam/DuplicateAssets/",params:{action:"add"},data:{AssetArr:arrObj}});return(request.then(handleSuccess,handleError));}
function CheckPreviewGeneratorID(Assetids) { var request = $http({ method: "post", ignoreLoadingBar: true, url: "api/dam/CheckPreviewGenerator/", params: { action: "add" }, data: { AssetIDs: Assetids } }); return (request.then(handleSuccess, handleError)); }
function PublishAssets(arrObj){var request=$http({method:"post",url:"api/dam/PublishAssets/",params:{action:"add"},data:{AssetArr:arrObj}});return(request.then(handleSuccess,handleError));}
function GetAllAssetTypesforDAM(){var request=$http({method:"get",url:"api/dam/GetDAMEntityTypes/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function UpdateAssetVersion(AssetID,fileid){var request=$http({method:"post",url:"api/dam/UpdateAssetVersion/",params:{action:"add"},data:{AssetID:AssetID,fileid:fileid}});return(request.then(handleSuccess,handleError));}
function SaveDetailBlockForAssets(AssetID,AttributeID,Level,AssetDetials){var request=$http({method:"post",url:"api/dam/SaveDetailBlockForAssets/",params:{action:"add"},data:{AssetID:AssetID,AttributeID:AttributeID,Level:Level,AssetDetails:AssetDetials}});return(request.then(handleSuccess,handleError));}
function SendMail(srcObj){var request=$http({method:"post",url:"api/dam/SendMail/",params:{action:"add"},data:{AssetArr:srcObj,}});return(request.then(handleSuccess,handleError));}
function MoveFilesToUploadImagesToCreateTask(jobj){var request=$http({method:"post",url:"api/dam/MoveFilesToUploadImagesToCreateTask/"+ParentEntityID,params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function DeleteAttachmentVersionByAssetID(fileid){var request=$http({method:"delete",url:"api/dam/DeleteAttachmentVersionByAssetID/"+fileid,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function UpdateAssetDetails(assetupdatedets){var request=$http({method:"post",url:"api/dam/UpdateAssetDetails/",params:{action:"add"},data:{assetupdatedets:assetupdatedets}});return(request.then(handleSuccess,handleError));}
function GetOptimakerSettings(){var request=$http({method:"Get",url:"api/dam/GetOptimakerSettings/",params:{action:"Get"}});return(request.then(handleSuccess,handleError));}
function GetOptimakerCatagories(){var request=$http({method:"Get",url:"api/dam/GetOptmakerCatagories/",params:{action:"Get"}});return(request.then(handleSuccess,handleError));}
function GetCategoryTreeCollection(){var request=$http({method:"Get",url:"api/dam/GetCategoryTreeCollection/",params:{action:"Get"}});return(request.then(handleSuccess,handleError));}
function InsertUpdateCatergory(CategoryColl){var request=$http({method:"post",url:"api/dam/InsertUpdateCatergory/",params:{action:"add"},data:CategoryColl});return(request.then(handleSuccess,handleError));}
function InsertUpdateOptimakerSettings(objdata){var request=$http({method:"post",url:"api/dam/InsertUpdateOptimakerSettings/",params:{action:"add"},data:objdata});return(request.then(handleSuccess,handleError));}
function DeleteOptimakeSetting(ID){var request=$http({method:"delete",url:"api/dam/DeleteOptimakeSetting/"+ID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function DeleteOptimakerCategory(ID){var request=$http({method:"delete",url:"api/dam/DeleteOptimakerCategory/"+ID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function GetAllOptimakerSettings(categoryID){var request=$http({method:"get",url:"api/dam/GetAllOptimakerSettings/"+categoryID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetOptimakerSettingsBaseURL(){var request=$http({method:"get",url:"api/dam/GetOptimakerSettingsBaseURL/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetMediaGeneratorSettingsBaseURL(){var request=$http({method:"get",url:"api/dam/GetMediaGeneratorSettingsBaseURL/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetStatusFilteredEntityAsset(FolderID,EntityID,View,Orderby,PageNo,StatusFilter){var request=$http({method:"get",url:"api/dam/GetStatusFilteredEntityAsset/"+FolderID+"/"+EntityID+"/"+View+"/"+Orderby+"/"+PageNo+"/"+StatusFilter,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function CreateMediaGeneratorAsset(EntityID,FolderID,AssetTypeID,CreatedBy,DocVersionID,ProductionName){var request=$http({method:"post",url:"api/dam/CreateMediaGeneratorAsset/"+EntityID+"/"+FolderID+"/"+AssetTypeID+"/"+CreatedBy+"/"+DocVersionID+"/"+ProductionName,params:{action:"add"}});return(request.then(handleSuccess,handleError));}
function GetDAMViewAdminSettings(ViewType,DamType){var request=$http({method:"get",url:"api/dam/GetDAMViewAdminSettings/"+ViewType+"/"+DamType,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function UpdateDamViewStatus(jobj){var request=$http({method:"put",url:"api/dam/UpdateDamViewStatus/",params:{action:"update"},data:jobj});return(request.then(handleSuccess,handleError));}
function GetDAMToolTipSettings(){var request=$http({method:"get",url:"api/dam/GetDAMToolTipSettings/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetWordTemplateSettings(){var request=$http({method:"Get",url:"api/dam/GetWordTemplateSettings/",params:{action:"Get"}});return(request.then(handleSuccess,handleError));}
function CreateDOCGeneratedAsset(EntityID,FolderID,AssetTypeID,CreatedBy,filename,assetname){var request=$http({method:"post",url:"api/dam/CreateDOCGeneratedAsset/"+EntityID+"/"+FolderID+"/"+AssetTypeID+"/"+CreatedBy+"/"+filename+"/"+assetname,params:{action:"add"}});return(request.then(handleSuccess,handleError));}
function InsertUpdateWordTemplateSettings(objdata){var request=$http({method:"post",url:"api/dam/InsertUpdateWordTemplateSettings/",params:{action:"add"},data:objdata});return(request.then(handleSuccess,handleError));}
function DeleteWordtempSetting(ID){var request=$http({method:"delete",url:"api/dam/DeleteWordtempSetting/"+ID,params:{action:"delete"},});return(request.then(handleSuccess,handleError));}
function GetPublishedAssets(View,Orderby,PageNo){var request=$http({method:"get",url:"api/dam/GetPublishedAssets/"+View+"/"+Orderby+"/"+PageNo,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function AddPublishedFilesToDAM(jobj){var request=$http({method:"post",url:"api/dam/AddPublishedFilesToDAM/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function AddPublishedLinkFiles(jobj){var request=$http({method:"post",url:"api/dam/AddPublishedLinkFiles/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function getkeyvaluefromwebconfig(key){var request=$http({method:"get",url:"api/dam/getkeyvaluefromwebconfig/"+key,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetAssetActiveFileinfo(assetId){var request=$http({method:"get",url:"api/dam/GetAssetActiveFileinfo/"+assetId,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetMimeType(){var request=$http({method:"get",url:"api/dam/GetMimeType/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function CreateCropRescale(fileCropRescale){var request=$http({method:"post",url:"api/dam/CropRescale/",params:{action:"add"},data:fileCropRescale});return(request.then(handleSuccess,handleError));}
function GetProfileFiles(){var request=$http({method:"get",url:"api/dam/GetProfileFiles/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetProfileFilesByUser(UserId){var request=$http({method:"get",url:"api/dam/GetProfileFilesByUser/"+UserId,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetBreadCrumFolderPath(EntityID,CurrentFolderID){var request=$http({method:"get",url:"api/dam/GetBreadCrumFolderPath/"+EntityID+"/"+CurrentFolderID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetAllFolderStructure(EntityID){var request=$http({method:"get",url:"api/dam/GetAllFolderStructure/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function CreateNewsFeedinfo(newsFeedinfo){var request=$http({method:"post",url:"api/dam/AddNewsFeedinfo/",params:{action:"add"},data:{newsFeedinfo:newsFeedinfo}});return(request.then(handleSuccess,handleError));}
function SaveCropedImageForLightBox(arrObj){var request=$http({method:"post",url:"api/dam/SaveCropedImageForLightBox/",params:{action:"add"},data:arrObj});return(request.then(handleSuccess,handleError));}
function GetMediaAssets(jobj){var request=$http({method:"post",url:"api/dam/GetMediaAssets/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function GetMediaBankFilterAttributes(){var request=$http({method:"get",url:"api/dam/GetMediaBankFilterAttributes/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetCustomFilterAttributes(typeid){var request=$http({method:"get",url:"api/dam/GetCustomFilterAttributes/"+typeid,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function MoveAssets(arrObj,folderid,entityid,actioncode){var request=$http({method:"post",url:"api/dam/MoveAssets/",params:{action:"add"},data:{AssetArr:arrObj,FolerId:folderid,EntityID:entityid,ActionCode:actioncode}});return(request.then(handleSuccess,handleError));}
function SaveDetailBlockForLink(jobj){var request=$http({method:"post",url:"api/metadata/SaveDetailBlockForLink/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function AttachAssets(assetarrObj,entityid,folderid){var request=$http({method:"post",url:"api/dam/AttachAssets/",params:{action:"add"},data:{AssetArr:assetarrObj,EntityID:entityid,FolerId:folderid}});return(request.then(handleSuccess,handleError));}
function AttachAssetsforProofTask(jobj){var request=$http({method:"post",url:"api/dam/AttachAssetsforProofTask/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function CreateproofTask(jobj){var request=$http({method:"post",url:"api/dam/CreateProofTaskWithAttachment/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function changeAssettypeservice(assetTypeID,assetID){var request=$http({method:"get",url:"api/dam/ChangeAssettype/"+assetTypeID+"/"+assetID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetAssetAttributueswithTypeID(assetTypeID){var request=$http({method:"get",url:"api/dam/GetAssetAttributueswithTypeID/"+assetTypeID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function SaveAssetTypeofAsset(SaveAsset){var request=$http({method:"post",url:"api/dam/SaveAssetTypeofAsset/",params:{action:"add"},data:{saveAsset:SaveAsset}});return(request.then(handleSuccess,handleError));}
function SendMailWithMetaData(srcObj){var request=$http({method:"post",url:"api/dam/SendMailWithMetaData/",params:{action:"add"},data:srcObj});return(request.then(handleSuccess,handleError));}
function UpdateAssetAccessSettings(srcObj){var request=$http({method:"post",url:"api/dam/UpdateAssetAccessSettings/",params:{action:"add"},data:srcObj});return(request.then(handleSuccess,handleError));}
function GetAttributeDAMCreate(EntityTypeID){var request=$http({method:"get",url:"api/dam/GetAttributeDAMCreate/"+EntityTypeID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function DAMadminSettingsforRootLevelInsertUpdate(DamAdminData,DamAdminID){var request=$http({method:"post",url:"api/dam/DAMadminSettingsforRootLevelInsertUpdate/",params:{action:"add"},data:{DamAdminData:DamAdminData,Key:DamAdminID.Key,LogoSettings:DamAdminID.LogoSettings,TypeId:DamAdminID.TypeId}});return(request.then(handleSuccess,handleError));}
function DAMGetAdminSettings(LogoSettings,Key,typeid){var request=$http({method:"get",url:"api/dam/DAMGetAdminSettings/"+LogoSettings+"/"+Key+"/"+typeid,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function DeleteFileProfile(ID){var request=$http({method:"delete",url:"api/dam/DeleteFileProfile/"+ID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function InsertUpdateProfileFiles(srcObj){var request=$http({method:"post",url:"api/dam/InsertUpdateProfileFiles/",params:{action:"add"},data:srcObj});return(request.then(handleSuccess,handleError));}
function UpdateAssetThumpnail(srcObj){var request=$http({method:"post",url:"api/dam/UpdateAssetThumpnail/",params:{action:"add"},data:srcObj,});return(request.then(handleSuccess,handleError));}
function GetlistoflinkedAsset(jobj){var request=$http({method:"post",url:"api/dam/GetlistoflinkedAsset/",params:{action:"add"},data:{EntityID:jobj.entityID,FolderArr:jobj.idArr}});return(request.then(handleSuccess,handleError));}
function DownloadAssetWithMetadata(assetid,result){var request=$http({method:"post",url:"api/dam/DownloadAssetWithMetadata/",params:{action:"add"},data:{assetid:assetid,result:result}});return(request.then(handleSuccess,handleError));}
function IsActiveLinkEntity(entityID){var request=$http({method:"get",url:"api/dam/IsActiveLinkEntity/"+entityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function UnPublishAssets(arrObj){var request=$http({method:"post",url:"api/dam/UnPublishAssets/",params:{action:"add"},data:{AssetArr:arrObj}});return(request.then(handleSuccess,handleError));}
function GetReformatProfile(ProfileID){var request=$http({method:"get",url:"api/dam/GetReformatProfile/"+ProfileID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function ReformatRescale(SaveReformatRescale){var request=$http({method:"post",url:"api/dam/ReformatRescale/",params:{action:"add"},data:SaveReformatRescale});return(request.then(handleSuccess,handleError));}
function SaveReformatAsset(jobj){var request=$http({method:"post",url:"api/dam/ReformateCreateAsset/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function GetAssetDetsforSelectedAssets(FolderID,EntityID,View,Orderby,PageNo,ViewAll,selAssetList){var request=$http({method:"post",url:"api/dam/GetAssetDetsforSelectedAssets/",params:{action:"add"},data:{AssetArr:selAssetList,FolderID:FolderID,EntityID:EntityID,View:View,Orderby:Orderby,PageNo:PageNo,ViewAll:ViewAll}});return(request.then(handleSuccess,handleError));}
function GetAssetsbasedonFolderandEntity(entityID,FolderID){var request=$http({method:"get",url:"api/dam/GetAssetsbasedonFolderandEntity/"+entityID+"/"+FolderID+"",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetExternalAssetContent(){var request=$http({method:"get",url:"api/dam/GetExternalAssetContent/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function InsertUpdateExternalAssetContent(htmlcontent){var request=$http({method:"post",url:"api/dam/InsertUpdateExternalAssetContent/",params:{action:"add"},data:htmlcontent});return(request.then(handleSuccess,handleError));}
function GetAssetCategoryTreeCollection(){var request=$http({method:"Get",url:"api/dam/GetAssetCategoryTreeCollection/",params:{action:"Get"}});return(request.then(handleSuccess,handleError));}
function InsertUpdateAssetCatergory(CategoryColl){var request=$http({method:"post",url:"api/dam/InsertUpdateAssetCatergory",params:{action:"add"},data:CategoryColl,});return(request.then(handleSuccess,handleError));}
function DeleteAssetCategory(ID){var request=$http({method:"delete",url:"api/dam/DeleteAssetCategory/"+ID,params:{action:"delete"},});return(request.then(handleSuccess,handleError));}
function GetAssetTypeAssetCategoryRelation(){var request=$http({method:"Get",url:"api/dam/GetAssetTypeAssetCategoryRelation/",params:{action:"Get"}});return(request.then(handleSuccess,handleError));}
function GetSelectedCategoryTreeCollection(CategoryID){var request=$http({method:"Get",url:"api/dam/GetSelectedCategoryTreeCollection/"+CategoryID,params:{action:"Get"}});return(request.then(handleSuccess,handleError));}
function InsertUpdateAssetCategoryRelation(AssetCategoryRelation){var request=$http({method:"post",url:"api/dam/InsertUpdateAssetCategoryRelation/",params:{action:"add"},data:AssetCategoryRelation});return(request.then(handleSuccess,handleError));}
function GetAssetCategoryTree(AsetTypeID){var request=$http({method:"Get",url:"api/dam/GetAssetCategoryTree/"+AsetTypeID,params:{action:"Get"}});return(request.then(handleSuccess,handleError));}
function GetAssetCategoryPathInAssetEdit(AssetTypeID){var request=$http({method:"Get",url:"api/dam/GetAssetCategoryPathInAssetEdit/"+AssetTypeID,params:{action:"Get"}});return(request.then(handleSuccess,handleError));}
function GetAllAssetsBasedOnFolderandEntity(jobj){var request=$http({method:"post",url:"api/dam/GetAllAssetsBasedOnFolderandEntity/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function GetAssetTypeFileExtensionRelation(){var request=$http({method:"Get",url:"api/dam/GetAssetTypeFileExtensionRelation/",params:{action:"Get"}});return(request.then(handleSuccess,handleError));}
function InsertAssetTypeFileExtension(AssetFileExtRelation){var request=$http({method:"post",url:"api/dam/InsertAssetTypeFileExtension/",params:{action:"add"},data:AssetFileExtRelation});return(request.then(handleSuccess,handleError));}
function DeleteAssetTypeFileExtension(ID){var request=$http({method:"delete",url:"api/dam/DeleteAssetTypeFileExtension/"+ID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function GetAssetSnippetTemplates(assetSnippetData){var request=$http({method:"post",url:"api/dam/GetAssetSnippetTemplates/",params:{action:"add"},data:assetSnippetData});return(request.then(handleSuccess,handleError));}
function getassetSnippetData(){var request=$http({method:"Get",url:"api/dam/getassetSnippetData/",params:{action:"Get"}});return(request.then(handleSuccess,handleError));}
function GetDamTreeNodeByEntityID(AttributeID,ParentEntityID){var request=$http({method:"Get",url:"api/dam/GetDamTreeNodeByEntityID/"+AttributeID+"/"+ParentEntityID,params:{action:"Get"}});return(request.then(handleSuccess,handleError));}
function GetDamTreeNode(AttributeID){var request=$http({method:"Get",url:"api/dam/GetDamTreeNode/"+AttributeID,params:{action:"Get"}});return(request.then(handleSuccess,handleError));}
function GetDropDownTreePricingObject(AttributeID,IsInheritFromParent,Isfromparent,Entityid,ParentId){var request=$http({method:"Get",url:"api/dam/GetDropDownTreePricingObject/"+AttributeID+"/"+IsInheritFromParent+"/"+Isfromparent+"/"+Entityid+"/"+ParentId,params:{action:"Get"}});return(request.then(handleSuccess,handleError));}
function GetDropDownTreePricingObjectFromParentDetail(AttributeID,IsInheritFromParent,Isfromparent,Entityid,ParentId){var request=$http({method:"Get",url:"api/dam/GetDropDownTreePricingObjectFromParentDetail/"+AttributeID+"/"+IsInheritFromParent+"/"+Isfromparent+"/"+Entityid+"/"+ParentId,params:{action:"Get"}});return(request.then(handleSuccess,handleError));}
function UpdateDropDownTreePricing(jobj){var request=$http({method:"post",url:"api/dam/UpdateDropDownTreePricing/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function InsertAssetPeriod(jobj){var request=$http({method:"post",url:"api/dam/InsertAssetPeriod/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function DeleteAssetPeriod(ID){var request=$http({method:"delete",url:"api/dam/DeleteAssetPeriod/"+ID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function GetAttributeTreeNode(AttributeID,Entityid,Ischoosefromparent){var request=$http({method:"Get",url:"api/dam/GetAttributeTreeNode/"+AttributeID+"/"+Entityid+"/"+Ischoosefromparent,params:{action:"Get"}});return(request.then(handleSuccess,handleError));}
function SaveDetailBlockForTreeLevels(jobj){var request=$http({method:"post",url:"api/dam/SaveDetailBlockForTreeLevels/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function CreateReformatProfile(jobj){var request=$http({method:"post",url:"api/dam/CreateReformatProfile/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function AssetPeriod(jobj){var request=$http({method:"post",url:"api/dam/AssetPeriod/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function UpdateAssetImageName(jobj){var request=$http({method:"post",url:"api/dam/UpdateAssetImageName/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function AssetRoleAccess(jobj){var request=$http({method:"post",url:"api/dam/AssetRoleAccess/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function getsnippethoturl(){var request=$http({method:"Get",url:"api/common/getsnippethoturl",params:{action:"Get"}});return(request.then(handleSuccess,handleError));}
function GetOptionDetailListByAssetID(ID,AssetID){var request=$http({method:"Get",url:"api/dam/GetOptionDetailListByAssetID/"+ID+"/"+AssetID+"/",params:{action:"Get"}});return(request.then(handleSuccess,handleError));}
function Get2ImagineTemplateUrl(get2ImgUrl){var request=$http({method:"post",url:"api/dam/Get2ImagineTemplateUrl",params:{action:"post"},data:get2ImgUrl});return(request.then(handleSuccess,handleError));}
function Download2ImagineAsset(jObj){var request=$http({method:"post",url:"api/dam/Download2ImagineAsset/",params:{action:"post"},data:jObj});return(request.then(handleSuccess,handleError));}
function saveTemplateEngine(jObj){var request=$http({method:"post",url:"api/dam/saveTemplateEngine",params:{action:"post"},data:jObj});return(request.then(handleSuccess,handleError));}
function getAllTemplateEngines(){var request=$http({method:"get",url:"api/dam/getAllTemplateEngines",params:{action:"Get"}});return(request.then(handleSuccess,handleError));}
function deleteTemplateEngine(ID){var request=$http({method:"delete",url:"api/dam/deleteTemplateEngine/"+ID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function getServerUrls(){var request=$http({method:"get",url:"api/dam/getServerUrls",params:{action:"Get"}});return(request.then(handleSuccess,handleError));}
function Get2ImagineResourceUrl(getResUrl){var request=$http({method:"post",url:"api/dam/Get2ImagineResourceUrl",params:{action:"post"},data:getResUrl});return(request.then(handleSuccess,handleError));}
function getCurrentTemplateAssetInfo(DocID){var request=$http({method:"get",url:"api/dam/getCurrentTemplateAssetInfo/"+DocID,params:{action:"Get"}});return(request.then(handleSuccess,handleError));}
function IsFileExist(filePath){var request=$http({method:"post",url:"api/dam/IsFileExist/",params:{action:"post"},data:{filePath:filePath}});return(request.then(handleSuccess,handleError));}
function S3Settings(){var request=$http({method:"get",url:"api/dam/S3Settings",params:{action:"Get"}});return(request.then(handleSuccess,handleError));}
function copydeleteTemplateAsset(tempGuid){var request=$http({method:"get",url:"api/dam/copydeleteTemplateAsset/"+tempGuid,params:{action:"Get"}});return(request.then(handleSuccess,handleError));}
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}}
app.service("DamService",['$http','$q',DamService]);})(angular,app);
///#source 1 1 /app/services/task-service.js
(function(ng,app){"use strict";function TaskService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({InsertUpdateTaskList:InsertUpdateTaskList,AddUpdateTaskFlag:AddUpdateTaskFlag,DeleteTaskFlagByID:DeleteTaskFlagByID,DeleteSystemTaskList:DeleteSystemTaskList,GetTaskFlags:GetTaskFlags,GetTaskList:GetTaskList,GetTaskTypes:GetTaskTypes,UpdateTaskListSortOrder:UpdateTaskListSortOrder,InsertTaskWithAttachments:InsertTaskWithAttachments,InsertUpdateTaskTemplateCondition:InsertUpdateTaskTemplateCondition,GetTaskTemplateDetails:GetTaskTemplateDetails,InsertTempTaskList:InsertTempTaskList,InsertUpdateTemplate:InsertUpdateTemplate,UpdateAdminTask:UpdateAdminTask,DeleteTaskTemplateListById:DeleteTaskTemplateListById,GetTaskAttachmentFile:GetTaskAttachmentFile,GetEntityTaskAttachmentFile:GetEntityTaskAttachmentFile,ViewAllFilesByEntityID:ViewAllFilesByEntityID,UpdateAttachmentVersionNo:UpdateAttachmentVersionNo,InsertTaskAttachments:InsertTaskAttachments,InsertEntityTaskAttachmentsVersion:InsertEntityTaskAttachmentsVersion,InsertEntityTaskAttachments:InsertEntityTaskAttachments,UpdateTemplateTaskListSortOrder:UpdateTemplateTaskListSortOrder,DeleteAttachments:DeleteAttachments,GetEntityTaskList:GetEntityTaskList,InsertEntityTaskWithAttachments:InsertEntityTaskWithAttachments,UpdateOverviewEntityTaskList:UpdateOverviewEntityTaskList,InsertTaskMembers:InsertTaskMembers,InsertTaskOwner:InsertTaskOwner,UpdateTaskStatus:UpdateTaskStatus,UpdateTaskStatusInTaskTab:UpdateTaskStatusInTaskTab,GetOverViewEntityTaskList:GetOverViewEntityTaskList,updateTaskMemberFlag:updateTaskMemberFlag,InsertUpdateEntityTaskList:InsertUpdateEntityTaskList,UpdateEntityTaskListSortOrder:UpdateEntityTaskListSortOrder,InsertUnassignedEntityTaskWithAttachments:InsertUnassignedEntityTaskWithAttachments,DuplicateEntityTaskList:DuplicateEntityTaskList,GetMytasks:GetMytasks,DeleteTaskEntityListID:DeleteTaskEntityListID,DuplicateEntityTask:DuplicateEntityTask,DeleteEntityTask:DeleteEntityTask,CompleteUnnassignedEntityTask:CompleteUnnassignedEntityTask,UpdatetasktoNotApplicableandUnassigned:UpdatetasktoNotApplicableandUnassigned,DeleteAdminTask:DeleteAdminTask,DeleteTemplateConditionById:DeleteTemplateConditionById,DeleteAdminTemplateTaskRelationById:DeleteAdminTemplateTaskRelationById,DeleteTaskMemberById:DeleteTaskMemberById,CopyFileFromTaskToEntityAttachments:CopyFileFromTaskToEntityAttachments,UpdatetaskAttachmentDescription:UpdatetaskAttachmentDescription,UpdatetaskLinkDescription:UpdatetaskLinkDescription,DeleteFileByID:DeleteFileByID,InsertLink:InsertLink,InsertLinkInAdminTasks:InsertLinkInAdminTasks,SendReminderNotification:SendReminderNotification,DeleteLinkByID:DeleteLinkByID,UpdatetaskEntityTaskDetails:UpdatetaskEntityTaskDetails,UpdatetaskEntityTaskDetailsInTaskTab:UpdatetaskEntityTaskDetailsInTaskTab,UpdatetaskEntityTaskDueDate:UpdatetaskEntityTaskDueDate,GetEntityTaskDetails:GetEntityTaskDetails,UpdateTaskSortOrder:UpdateTaskSortOrder,UpdateEntityTaskSortOrder:UpdateEntityTaskSortOrder,DeleteAdminTaskCheckListByID:DeleteAdminTaskCheckListByID,getTaskchecklist:getTaskchecklist,InsertTaskCheckList:InsertTaskCheckList,ChecksTaskCheckList:ChecksTaskCheckList,DeleteEntityCheckListByID:DeleteEntityCheckListByID,GetSublevelTaskList:GetSublevelTaskList,GetEntityUpcomingTaskList:GetEntityUpcomingTaskList,GetFilesandLinksByEntityID:GetFilesandLinksByEntityID,CopyAttachmentsfromtask:CopyAttachmentsfromtask,CopytoGeneralAttachments:CopytoGeneralAttachments,CopyAttachmentsfromtaskToExistingTasks:CopyAttachmentsfromtaskToExistingTasks,EnableDisableTaskflag:EnableDisableTaskflag,gettaskflagstatus:gettaskflagstatus,DeleteTaskFileByid:DeleteTaskFileByid,DeleteTaskLinkByid:DeleteTaskLinkByid,DeleteAdminTaskLinkByID:DeleteAdminTaskLinkByID,InsertUpdateEntityTaskCheckList:InsertUpdateEntityTaskCheckList,InsertUpdateEntityTaskCheckListInTask:InsertUpdateEntityTaskCheckListInTask,GetEntityTaskListWithoutTasks:GetEntityTaskListWithoutTasks,GetEntityTaskListDetails:GetEntityTaskListDetails,GettaskCountByStatus:GettaskCountByStatus,InsertUnAssignedTaskMembers:InsertUnAssignedTaskMembers,UpdatetaskAdminLinkDescription:UpdatetaskAdminLinkDescription,UpdatetaskAdminAttachmentDescription:UpdatetaskAdminAttachmentDescription,GetExistingEntityTasksByEntityID:GetExistingEntityTasksByEntityID,GetMyFundingRequests:GetMyFundingRequests,FetchUnassignedTaskforReassign:FetchUnassignedTaskforReassign,AssignUnassignTasktoMembers:AssignUnassignTasktoMembers,GetAdminTasksByTaskListID:GetAdminTasksByTaskListID,GetAdminTasksById:GetAdminTasksById,getAdminTaskchecklist:getAdminTaskchecklist,GetAdminTaskMetadatabyTaskID:GetAdminTaskMetadatabyTaskID,GetTaskTemplateConditionByTaskTempId:GetTaskTemplateConditionByTaskTempId,GetFiltertaskCountByStatus:GetFiltertaskCountByStatus,GetTemplateAdminTaskList:GetTemplateAdminTaskList,UpdateEntityTask:UpdateEntityTask,GetSourceToDestinationmembers:GetSourceToDestinationmembers,GetDestinationEntityIdRoleAccess:GetDestinationEntityIdRoleAccess,InsertUpdateDragTaskMembers:InsertUpdateDragTaskMembers,UpdateDragEntityTaskListByTask:UpdateDragEntityTaskListByTask,GetEntityAttributesDetails:GetEntityAttributesDetails,GetEntityTaskAttachmentinfo:GetEntityTaskAttachmentinfo,GetEntityTaskCollection:GetEntityTaskCollection,InsertTaskLibImport:InsertTaskLibImport,ExportTaskList:ExportTaskList,GetAllApprovalFlowPhasesSteps:GetAllApprovalFlowPhasesSteps,GetAllApprovalFlowTemplates:GetAllApprovalFlowTemplates,HighResPdfDownload:HighResPdfDownload,getAllTemplateTask:getAllTemplateTask,InsertUpdateTaskTemplate:InsertUpdateTaskTemplate,DeleteApprovalTemplate:DeleteApprovalTemplate,InsertUpdateTaskTemplateStep:InsertUpdateTaskTemplateStep,InsertUpdateTaskTemplatePhase:InsertUpdateTaskTemplatePhase,UpdatetaskTemplateSortOrder:UpdatetaskTemplateSortOrder,UpdateTemplatePhaseSortOrder:UpdateTemplatePhaseSortOrder,DeleteApprovalTaskStep:DeleteApprovalTaskStep,InsertUpdateAdminTaskTemplate:InsertUpdateAdminTaskTemplate,GetAllAdminApprovalFlowPhasesSteps:GetAllAdminApprovalFlowPhasesSteps,DeleteAdminApprovalTaskStep:DeleteAdminApprovalTaskStep,UpdateAdmintaskPhaseSortOrder:UpdateAdmintaskPhaseSortOrder,UpdateAdminTasklistSortOrder:UpdateAdminTasklistSortOrder,InsertUpdateVersion:InsertUpdateVersion,GetTakUIData:GetTakUIData,CreateNewVersion:CreateNewVersion,GetTaskDetails:GetTaskDetails,AddNewPhase:AddNewPhase,RemovePhase:RemovePhase,AddPhaseStep:AddPhaseStep,AddMembersToPhaseStep:AddMembersToPhaseStep,DeletePhaseStep:DeletePhaseStep,RemovePhaseStepApprovar:RemovePhaseStepApprovar,sendReminderNotificationforTaskPhase:sendReminderNotificationforTaskPhase,sendReminderNotify:sendReminderNotify,getAllMemberIDbyStepID:getAllMemberIDbyStepID,GetListOfDocumentWorkFlowFromDalim:GetListOfDocumentWorkFlowFromDalim,GetListOfWorkFlowFromDalim:GetListOfWorkFlowFromDalim,CollectApprovalFlowSteps:CollectApprovalFlowSteps,GetApprovalFlowLibraryList:GetApprovalFlowLibraryList,ReorderOverviewStructure:ReorderOverviewStructure,getEntityApprovalRoleUsers:getEntityApprovalRoleUsers,getTaskApprovalRolesUsers:getTaskApprovalRolesUsers});function InsertUpdateTaskList(formobj){var request=$http({method:"post",url:"api/task/InsertUpdateTaskList/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function AddUpdateTaskFlag(formobj){var request=$http({method:"post",url:"api/task/AddUpdateTaskFlag/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function DeleteTaskFlagByID(ID){var request=$http({method:"delete",url:"api/task/DeleteTaskFlagByID/"+ID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function DeleteSystemTaskList(ID){var request=$http({method:"delete",url:"api/task/DeleteSystemTaskList/"+ID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function GetTaskFlags(){var request=$http({method:"get",url:"api/task/GetTaskFlags/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetTaskList(){var request=$http({method:"get",url:"api/task/GetTaskList/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetTaskTypes(){var request=$http({method:"get",url:"api/task/GetTaskTypes/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function UpdateTaskListSortOrder(TaskListID,SortOrderID){var request=$http({method:"put",url:"api/task/UpdateTaskListSortOrder/"+TaskListID+"/"+SortOrderID,params:{action:"update"}});return(request.then(handleSuccess,handleError));}
function InsertTaskWithAttachments(formobj){var request=$http({method:"post",url:"api/task/InsertTaskWithAttachments/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function InsertUpdateTaskTemplateCondition(formobj){var request=$http({method:"post",url:"api/task/InsertUpdateTaskTemplateCondition/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetTaskTemplateDetails(){var request=$http({method:"get",url:"api/task/GetTaskTemplateDetails/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function InsertTempTaskList(formobj){var request=$http({method:"post",url:"api/task/InsertTempTaskList/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function InsertUpdateTemplate(formobj){var request=$http({method:"post",url:"api/task/InsertUpdateTemplate/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function UpdateAdminTask(formobj){var request=$http({method:"post",url:"api/task/UpdateAdminTask/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function DeleteTaskTemplateListById(ID){var request=$http({method:"delete",url:"api/task/DeleteTaskTemplateListById/"+ID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function GetTaskAttachmentFile(TaskID){var request=$http({method:"get",url:"api/task/GetTaskAttachmentFile/"+TaskID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetEntityTaskAttachmentFile(TaskID){var request=$http({method:"get",url:"api/task/GetEntityTaskAttachmentFile/"+TaskID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function ViewAllFilesByEntityID(TaskID,VersionFileID){var request=$http({method:"get",url:"api/task/ViewAllFilesByEntityID/"+TaskID+"/"+VersionFileID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function UpdateAttachmentVersionNo(dataobj){var request=$http({method:"post",url:"api/task/UpdateAttachmentVersionNo/",params:{action:"add"},data:dataobj});return(request.then(handleSuccess,handleError));}
function InsertTaskAttachments(formobj){var request=$http({method:"post",url:"api/task/InsertTaskAttachments/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function InsertEntityTaskAttachmentsVersion(formobj){var request=$http({method:"post",url:"api/task/InsertEntityTaskAttachmentsVersion/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function InsertEntityTaskAttachments(formobj){var request=$http({method:"post",url:"api/task/InsertEntityTaskAttachments/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function UpdateTemplateTaskListSortOrder(TempID,TaskListID,SortOrderID){var request=$http({method:"put",url:"api/task/UpdateTemplateTaskListSortOrder/"+TempID+"/"+TaskListID+"/"+SortOrderID,params:{action:"update"}});return(request.then(handleSuccess,handleError));}
function DeleteAttachments(ActiveFileid){var request=$http({method:"delete",url:"api/task/DeleteAttachments/"+ActiveFileid,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function GetEntityTaskList(EntityID){var request=$http({method:"get",url:"api/task/GetEntityTaskList/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function InsertEntityTaskWithAttachments(formobj){var request=$http({method:"post",url:"api/task/InsertEntityTaskWithAttachments/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function UpdateOverviewEntityTaskList(ID,OnTimeStatus,OnTimeComment){var request=$http({method:"put",url:"api/task/UpdateOverviewEntityTaskList/"+ID+"/"+OnTimeStatus+"/"+OnTimeComment,params:{action:"update"}});return(request.then(handleSuccess,handleError));}
function InsertTaskMembers(formobj){var request=$http({method:"post",url:"api/task/InsertTaskMembers/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function InsertTaskOwner(formobj){var request=$http({method:"post",url:"api/task/InsertTaskOwner/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function UpdateTaskStatus(TaskID,Status){var request=$http({method:"put",url:"api/task/UpdateTaskStatus/"+TaskID+"/"+Status,params:{action:"update"}});return(request.then(handleSuccess,handleError));}
function UpdateTaskStatusInTaskTab(dataobj){var request=$http({method:"post",url:"api/task/UpdateTaskStatus",params:{action:"add"},data:dataobj});return(request.then(handleSuccess,handleError));}
function GetOverViewEntityTaskList(EntityID){var request=$http({method:"get",url:"api/task/GetOverViewEntityTaskList/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function updateTaskMemberFlag(formobj){var request=$http({method:"post",url:"api/Task/updateTaskMemberFlag/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function InsertUpdateEntityTaskList(formobj){var request=$http({method:"post",url:"api/task/InsertUpdateEntityTaskList/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function UpdateEntityTaskListSortOrder(formobj){var request=$http({method:"post",url:"api/task/UpdateEntityTaskListSortOrder/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function InsertUnassignedEntityTaskWithAttachments(formobj){var request=$http({method:"post",url:"api/task/InsertUnassignedEntityTaskWithAttachments/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function DuplicateEntityTaskList(dataobj){var request=$http({method:"put",url:"api/task/DuplicateEntityTaskList/",params:{action:"update"},data:dataobj});return(request.then(handleSuccess,handleError));}
function GetMytasks(formobj){var request=$http({method:"post",url:"api/task/GetMytasks/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function DeleteTaskEntityListID(TaskListID,EntityID){var request=$http({method:"delete",url:"api/task/DeleteTaskEntityListID/"+TaskListID+"/"+EntityID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function DuplicateEntityTask(dataobj){var request=$http({method:"put",url:"api/task/DuplicateEntityTask/",params:{action:"update"},data:dataobj});return(request.then(handleSuccess,handleError));}
function DeleteEntityTask(TaskID,EntityID){var request=$http({method:"delete",url:"api/task/DeleteEntityTask/"+TaskID+"/"+EntityID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function CompleteUnnassignedEntityTask(formobj){var request=$http({method:"post",url:"api/task/CompleteUnnassignedEntityTask/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function UpdatetasktoNotApplicableandUnassigned(TaskID){var request=$http({method:"put",url:"api/task/UpdatetasktoNotApplicableandUnassigned/"+TaskID,params:{action:"update"}});return(request.then(handleSuccess,handleError));}
function DeleteAdminTask(TaskID){var request=$http({method:"delete",url:"api/task/DeleteAdminTask/"+TaskID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function DeleteTemplateConditionById(TemplateCondID){var request=$http({method:"delete",url:"api/task/DeleteTemplateConditionById/"+TemplateCondID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function DeleteAdminTemplateTaskRelationById(TaskListID,TemplateID){var request=$http({method:"delete",url:"api/task/DeleteAdminTemplateTaskRelationById/"+TaskListID+"/"+TemplateID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function DeleteTaskMemberById(ID,TaskID){var request=$http({method:"delete",url:"api/task/DeleteTaskMemberById/"+ID+"/"+TaskID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function CopyFileFromTaskToEntityAttachments(ActivFileID,EntityID){var request=$http({method:"get",url:"api/task/CopyFileFromTaskToEntityAttachments/"+ActivFileID+"/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function UpdatetaskAttachmentDescription(formobj){var request=$http({method:"put",url:"api/task/UpdatetaskAttachmentDescription/",params:{action:"update"},data:formobj});return(request.then(handleSuccess,handleError));}
function UpdatetaskLinkDescription(formobj){var request=$http({method:"put",url:"api/task/UpdatetaskLinkDescription/",params:{action:"update"},data:formobj});return(request.then(handleSuccess,handleError));}
function DeleteFileByID(ID){var request=$http({method:"delete",url:"api/task/DeleteFileByID/"+ID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function InsertLink(formobj){var request=$http({method:"post",url:"api/task/InsertLink/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function InsertLinkInAdminTasks(formobj){var request=$http({method:"post",url:"api/task/InsertLinkInAdminTasks/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function SendReminderNotification(TaskMemberID,TaskID){var request=$http({method:"post",url:"api/task/InsertLinkInAdminTasks/"+TaskMemberID+"/"+TaskID,params:{action:"add"}});return(request.then(handleSuccess,handleError));}
function DeleteLinkByID(ID){var request=$http({method:"delete",url:"api/task/DeleteLinkByID/"+ID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function UpdatetaskEntityTaskDetails(formobj){var request=$http({method:"post",url:"api/task/UpdatetaskEntityTaskDetails/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function UpdatetaskEntityTaskDetailsInTaskTab(formobj){var request=$http({method:"post",url:"api/task/UpdatetaskEntityTaskDetailsInTaskTab/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function UpdatetaskEntityTaskDueDate(formobj){var request=$http({method:"post",url:"api/task/UpdatetaskEntityTaskDueDate/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetEntityTaskDetails(EntityTaskID){var request=$http({method:"get",url:"api/task/GetEntityTaskDetails/"+EntityTaskID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function UpdateTaskSortOrder(TaskId,TaskListID,SortOrderID){var request=$http({method:"put",url:"api/task/UpdateTaskSortOrder/"+TaskId+"/"+TaskListID+"/"+SortOrderID,params:{action:"update"}});return(request.then(handleSuccess,handleError));}
function UpdateEntityTaskSortOrder(formobj){var request=$http({method:"post",url:"api/task/UpdateEntityTaskSortOrder/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function DeleteAdminTaskCheckListByID(ID){var request=$http({method:"delete",url:"api/task/DeleteAdminTaskCheckListByID/"+ID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function getTaskchecklist(TaskID){var request=$http({method:"get",url:"api/task/getTaskchecklist/"+TaskID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function InsertTaskCheckList(formobj){var request=$http({method:"post",url:"api/task/InsertTaskCheckList/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function ChecksTaskCheckList(ID,Status){var request=$http({method:"put",url:"api/task/ChecksTaskCheckList/"+ID+"/"+Status,params:{action:"update"},data:formobj});return(request.then(handleSuccess,handleError));}
function DeleteEntityCheckListByID(ID){var request=$http({method:"delete",url:"api/task/DeleteEntityCheckListByID/"+ID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function GetSublevelTaskList(EntityID){var request=$http({method:"get",url:"api/task/GetSublevelTaskList/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetEntityUpcomingTaskList(FilterByentityID,FilterStatusID,EntityID,IsChildren){var request=$http({method:"get",url:"api/task/GetEntityUpcomingTaskList/"+FilterByentityID+"/"+FilterStatusID+"/"+EntityID+"/"+IsChildren,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetFilesandLinksByEntityID(EntityID){var request=$http({method:"get",url:"api/task/GetFilesandLinksByEntityID/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function CopyAttachmentsfromtask(formobj){var request=$http({method:"put",url:"api/task/CopyAttachmentsfromtask/",params:{action:"update"},data:formobj});return(request.then(handleSuccess,handleError));}
function CopytoGeneralAttachments(formobj){var request=$http({method:"put",url:"api/task/CopytoGeneralAttachments/",params:{action:"update"},data:formobj});return(request.then(handleSuccess,handleError));}
function CopyAttachmentsfromtaskToExistingTasks(formobj){var request=$http({method:"put",url:"api/task/CopyAttachmentsfromtaskToExistingTasks/",params:{action:"update"},data:formobj});return(request.then(handleSuccess,handleError));}
function EnableDisableTaskflag(status){var request=$http({method:"post",url:"api/task/EnableDisableTaskflag/"+status,params:{action:"add"}});return(request.then(handleSuccess,handleError));}
function gettaskflagstatus(){var request=$http({method:"get",url:"api/task/gettaskflagstatus/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function DeleteTaskFileByid(ID,EntityId){var request=$http({method:"delete",url:"api/task/DeleteTaskFileByid/"+ID+"/"+EntityId,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function DeleteTaskLinkByid(ID,EntityId){var request=$http({method:"delete",url:"api/task/DeleteTaskLinkByid/"+ID+"/"+EntityId,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function DeleteAdminTaskLinkByID(ID){var request=$http({method:"delete",url:"api/task/DeleteAdminTaskLinkByID/"+ID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function InsertUpdateEntityTaskCheckList(formobj){var request=$http({method:"post",url:"api/task/InsertUpdateEntityTaskCheckList/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function InsertUpdateEntityTaskCheckListInTask(formobj){var request=$http({method:"post",url:"api/task/InsertUpdateEntityTaskCheckListInTask/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetEntityTaskListWithoutTasks(EntityID){var request=$http({method:"get",url:"api/task/GetEntityTaskListWithoutTasks/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetEntityTaskListDetails(EntityID,TaskListID){var request=$http({method:"get",url:"api/task/GetEntityTaskListDetails/"+EntityID+"/"+TaskListID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GettaskCountByStatus(formobj){var request=$http({method:"put",url:"api/task/GettaskCountByStatus/",params:{action:"update"},data:formobj});return(request.then(handleSuccess,handleError));}
function InsertUnAssignedTaskMembers(formobj){var request=$http({method:"post",url:"api/task/InsertUnAssignedTaskMembers/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function UpdatetaskAdminLinkDescription(formobj){var request=$http({method:"put",url:"api/task/UpdatetaskAdminLinkDescription/",params:{action:"update"},data:formobj});return(request.then(handleSuccess,handleError));}
function UpdatetaskAdminAttachmentDescription(formobj){var request=$http({method:"put",url:"api/task/UpdatetaskAdminAttachmentDescription/",params:{action:"update"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetExistingEntityTasksByEntityID(EntityID){var request=$http({method:"get",url:"api/task/GetExistingEntityTasksByEntityID/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetMyFundingRequests(formobj){var request=$http({method:"post",url:"api/task/GetMyFundingRequests/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function FetchUnassignedTaskforReassign(EntityID,TaskListID){var request=$http({method:"get",url:"api/task/FetchUnassignedTaskforReassign/"+EntityID+"/"+TaskListID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function AssignUnassignTasktoMembers(formobj){var request=$http({method:"post",url:"api/task/AssignUnassignTasktoMembers/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetAdminTasksByTaskListID(TaskListID){var request=$http({method:"get",url:"api/task/GetAdminTasksByTaskListID/"+TaskListID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetAdminTasksById(formobj){var request=$http({method:"post",url:"api/task/GetAdminTasksById/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function getAdminTaskchecklist(TaskID){var request=$http({method:"get",url:"api/task/getAdminTaskchecklist/"+TaskID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetAdminTaskMetadatabyTaskID(TaskID){var request=$http({method:"get",url:"api/task/GetAdminTaskMetadatabyTaskID/"+TaskID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetTaskTemplateConditionByTaskTempId(TaskTempID){var request=$http({method:"get",url:"api/task/GetTaskTemplateConditionByTaskTempId/"+TaskTempID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetFiltertaskCountByStatus(formobj){var request=$http({method:"post",url:"api/task/GetFiltertaskCountByStatus/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetTemplateAdminTaskList(){var request=$http({method:"get",url:"api/task/GetTemplateAdminTaskList/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function UpdateEntityTask(formobj){var request=$http({method:"post",url:"api/task/UpdateEntityTask/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetSourceToDestinationmembers(taskID,sourceEntityID,destinationEntityId){var request=$http({method:"get",url:"api/task/GetSourceToDestinationmembers/"+taskID+"/"+sourceEntityID+"/"+destinationEntityId,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetDestinationEntityIdRoleAccess(destinationEntityId){var request=$http({method:"get",url:"api/task/GetDestinationEntityIdRoleAccess/"+destinationEntityId,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function InsertUpdateDragTaskMembers(formobj){var request=$http({method:"post",url:"api/task/InsertUpdateDragTaskMembers/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function UpdateDragEntityTaskListByTask(formobj){var request=$http({method:"post",url:"api/task/UpdateDragEntityTaskListByTask/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetEntityAttributesDetails(TaskID){var request=$http({method:"get",url:"api/task/GetEntityAttributesDetails/"+TaskID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetEntityTaskAttachmentinfo(TaskID){var request=$http({method:"get",url:"api/task/GetEntityTaskAttachmentinfo/"+TaskID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetEntityTaskCollection(EntityID,TaskListID,pageno){var request=$http({method:"get",url:"api/task/GetEntityTaskCollection/"+EntityID+"/"+TaskListID+"/"+pageno,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function InsertTaskLibImport(formobj){var request=$http({method:"post",url:"api/task/InsertTaskLibImport/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function ExportTaskList(TaskListID,NewGuid,TaskLibName){var request=$http({method:"get",url:"api/task/ExportTaskList/"+TaskListID+"/"+NewGuid+"/"+TaskLibName,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetAllApprovalFlowPhasesSteps(templateid){var request=$http({method:"get",url:"api/task/GetAllApprovalFlowPhasesSteps/"+templateid,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function InsertUpdateTaskTemplate(formobj){var request=$http({method:"post",url:"api/task/InsertUpdateTaskTemplate/",params:{action:"add"},data:{"ID":formobj.ID,"Name":formobj.Name,"Description":formobj.Description}});return(request.then(handleSuccess,handleError));}
function GetAllApprovalFlowTemplates(){var request=$http({method:"get",url:"api/task/GetAllApprovalFlowTemplates/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function DeleteApprovalTemplate(TempID){var request=$http({method:"delete",url:"api/task/DeleteApprovalTemplate/"+TempID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function InsertUpdateTaskTemplateStep(formobj){var request=$http({method:"post",url:"api/task/InsertUpdateTaskTemplateStep/",params:{action:"add"},data:{"Name":formobj.Name,"Description":formobj.Description,"Stepduration":formobj.Stepduration,"StepMinApproval":formobj.StepMinApproval,"Steproles":formobj.Steproles,"IsMandatory":formobj.IsMandatory,"PhaseId":formobj.PhaseId,"StepId":formobj.StepID,"IsUpdate":formobj.IsUpdate}});return(request.then(handleSuccess,handleError));}
function InsertUpdateTaskTemplatePhase(formobj){var request=$http({method:"post",url:"api/task/InsertUpdateTaskTemplatePhase/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function UpdatetaskTemplateSortOrder(formobj){var request=$http({method:"post",url:"api/task/UpdatetaskTemplateSortOrder/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function UpdateTemplatePhaseSortOrder(formobj){var request=$http({method:"post",url:"api/task/UpdateTemplatePhaseSortOrder/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function DeleteApprovalTaskStep(StepId){var request=$http({method:"delete",url:"api/task/DeleteApprovalTaskStep/"+StepId,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function HighResPdfDownload(HiResPdf){var request=$http({method:"get",url:"api/task/HighResPdfDownload/"+HiResPdf,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function getAllTemplateTask(){var request=$http({method:"get",url:"api/task/getAllTemplateTask/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function InsertUpdateAdminTaskTemplate(Phasestepdetails,EntityId){var request=$http({method:"post",url:"api/task/InsertUpdateAdminTaskTemplate/",params:{action:"add"},data:{"PhaseStepDetails":Phasestepdetails,"EntityID":EntityId}});return(request.then(handleSuccess,handleError));}
function DeleteAdminApprovalTaskStep(StepId){var request=$http({method:"delete",url:"api/task/DeleteAdminApprovalTaskStep/"+StepId,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function GetAllAdminApprovalFlowPhasesSteps(EntityId){var request=$http({method:"get",url:"api/task/GetAllAdminApprovalFlowPhasesSteps/"+EntityId,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function UpdateAdminTasklistSortOrder(formobj){var request=$http({method:"post",url:"api/task/UpdateAdminTasklistSortOrder/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function UpdateAdmintaskPhaseSortOrder(formobj){var request=$http({method:"post",url:"api/task/UpdateAdmintaskPhaseSortOrder/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function InsertUpdateVersion(jobj){var request=$http({method:"post",url:"api/task/InsertUpdateVersion/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function GetTakUIData(taskid,versionNo){var request=$http({method:"get",url:"api/task/GetTakUIData/"+taskid+"/"+versionNo,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function CreateNewVersion(jobj){var request=$http({method:"post",url:"api/task/CreateNewVersion/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function GetTaskDetails(obj){var request=$http({method:"post",ignoreLoadingBar:true,url:"api/task/GetTaskDetails/",data:obj,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function AddNewPhase(obj){var request=$http({method:"post",url:"api/task/AddNewPhase/",data:obj,params:{action:"add"}});return(request.then(handleSuccess,handleError));}
function RemovePhase(phaseID,EntityID){var request=$http({method:"delete",url:"api/task/RemovePhase/"+phaseID+"/"+EntityID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function AddPhaseStep(obj){var request=$http({method:"post",url:"api/task/AddPhaseStep/",data:obj,params:{action:"add"}});return(request.then(handleSuccess,handleError));}
function AddMembersToPhaseStep(obj){var request=$http({method:"post",url:"api/task/AddMembersToPhaseStep/",data:obj,params:{action:"add"}});return(request.then(handleSuccess,handleError));}
function DeletePhaseStep(stepID,EntityID){var request=$http({method:"delete",url:"api/task/DeletePhaseStep/"+stepID+"/"+EntityID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function RemovePhaseStepApprovar(jobj){var request=$http({method:"post",url:"api/task/RemovePhaseStepApprovar/",data:jobj,params:{action:"add"}});return(request.then(handleSuccess,handleError));}
function sendReminderNotificationforTaskPhase(jObj){var request=$http({method:"post",url:"api/task/sendReminderNotificationforTaskPhase/",params:{action:"post"},data:jObj});return(request.then(handleSuccess,handleError));}
function sendReminderNotify(memberID,taskID){var request=$http({method:"post",url:"api/task/SendReminderNotification/"+memberID+"/"+taskID,params:{action:"post"}});return(request.then(handleSuccess,handleError));}
function getAllMemberIDbyStepID(stepID){var request=$http({method:"get",url:"api/task/getAllMemberIDbyStepID/"+stepID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetListOfDocumentWorkFlowFromDalim(){var request=$http({method:"get",url:"api/task/GetListOfDocumentWorkFlowFromDalim",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetListOfWorkFlowFromDalim(jsonparameter){var request=$http({method:"post",url:"api/task/GetListOfWorkFlowFromDalim/",params:{action:"add"},data:jsonparameter});return(request.then(handleSuccess,handleError));}
function GetDalimUserFromDB(username){var request=$http({method:"get",url:"api/task/GetDalimUserFromDB/"+username,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetApprovalFlowLibraryList(typeid,IsAdminSide){var request=$http({method:"get",url:"api/task/GetApprovalFlowLibraryList/"+typeid+"/"+IsAdminSide,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function CollectApprovalFlowSteps(jsonparameter){var request=$http({method:"post",url:"api/task/CollectApprovalFlowSteps",params:{action:"add"},data:jsonparameter});return(request.then(handleSuccess,handleError));}
function ReorderOverviewStructure(jsonparameter){var request=$http({method:"post",url:"api/task/ReorderOverviewStructure",params:{action:"add"},data:jsonparameter});return(request.then(handleSuccess,handleError));}
function getTaskApprovalRolesUsers(jsonparameter){var request=$http({method:"post",url:"api/task/getTaskApprovalRolesUsers",params:{action:"add"},data:jsonparameter});return(request.then(handleSuccess,handleError));}
function getEntityApprovalRoleUsers(jsonparameter){var request=$http({method:"post",url:"api/task/getEntityApprovalRoleUsers",params:{action:"add"},data:jsonparameter});return(request.then(handleSuccess,handleError));}
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}}
app.service("TaskService",['$http','$q',TaskService]);})(angular,app);
///#source 1 1 /app/services/user-service.js
(function(ng,app){"use strict";function UserService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({InsertUser:InsertUser,RegisterUser:RegisterUser,UpdateUser:UpdateUser,DeleteUser:DeleteUser,CheckUserInvolvement:CheckUserInvolvement,GetLogoLoginDetails:GetLogoLoginDetails,ClientIntranetUrl:ClientIntranetUrl,ValidUser:ValidUser,MediaBankValidUser:MediaBankValidUser,SSO:SSO,ValidUserForForgotPwd:ValidUserForForgotPwd,Resetpwdlink:Resetpwdlink,IsUserExistsForSetpassword:IsUserExistsForSetpassword,UpdateSetPassword:UpdateSetPassword,GetUserById:GetUserById,Getstartpages:Getstartpages,AutoCompleteMemberList:AutoCompleteMemberList,GetUsers:GetUsers,GetPendingUsers:GetPendingUsers,ApproveRejectRegisteredUsers:ApproveRejectRegisteredUsers,GetUserByEntityID:GetUserByEntityID,GetMembersByEntityID:GetMembersByEntityID,UserDateTime:UserDateTime,IsUserSesstionPresent:IsUserSesstionPresent,UpdateUserByColumn:UpdateUserByColumn,SaveUserImage:SaveUserImage,GetAllObjectiveMembers:GetAllObjectiveMembers,UpdateNewLanguageType:UpdateNewLanguageType,IsSSOUser:IsSSOUser,Logout:Logout,Resetpassword:Resetpassword,Forgetpassword:Forgetpassword,GetAPIusersDetails:GetAPIusersDetails,GenerateGuidforSelectedAPI:GenerateGuidforSelectedAPI,InsertUserRegisterDynamicData:InsertUserRegisterDynamicData,SetupToolUserRegister:SetupToolUserRegister,CalendarValidUser:CalendarValidUser,getdimensionunit:getdimensionunit,GetDimensionunitsettings:GetDimensionunitsettings,GetTenantPath:GetTenantPath,GetTenantXMLFile:GetTenantXMLFile,LockResetpassword:LockResetpassword,GetLockuserDetails:GetLockuserDetails,SendPasswordResetMail:SendPasswordResetMail,GetusermailbyGuid:GetusermailbyGuid,GetApprovalrole:GetApprovalrole,InsertApprovalUserRoles:InsertApprovalUserRoles,GetApprovalRoleUserByID:GetApprovalRoleUserByID,GetDalimUser:GetDalimUser,SaveUpdateDalimUser:SaveUpdateDalimUser,DeleteDalimUser:DeleteDalimUser});function InsertUser(jobj){var request=$http({method:"post",url:"api/user/InsertUser/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function RegisterUser(jobj){var request=$http({method:"post",url:"api/user/RegisterUser/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function UpdateUser(jobj){var request=$http({method:"put",url:"api/user/UpdateUser/",params:{action:"update"},data:jobj});return(request.then(handleSuccess,handleError));}
function DeleteUser(ID){var request=$http({method:"delete",url:"api/user/DeleteUser/"+ID,params:{action:"delete",},});return(request.then(handleSuccess,handleError));}
function CheckUserInvolvement(ID){var request=$http({method:"get",url:"api/user/CheckUserInvolvement/"+ID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetLogoLoginDetails(){var request=$http({method:"get",url:"api/user/GetLogoLoginDetails/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function ClientIntranetUrl(){var request=$http({method:"get",url:"api/user/ClientIntranetUrl/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function ValidUser(jobj){var request=$http({method:"post",url:"api/user/ValidUser/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function MediaBankValidUser(jobj){var request=$http({method:"post",url:"api/user/MediaBankValidUser/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function SSO(jobj){var request=$http({method:"post",url:"api/user/SSO/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function ValidUserForForgotPwd(jobj){var request=$http({method:"post",url:"api/user/ValidUserForForgotPwd/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function Resetpwdlink(jobj){var request=$http({method:"post",url:"api/user/Resetpwdlink/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function IsUserExistsForSetpassword(jobj){var request=$http({method:"post",url:"api/user/IsUserExistsForSetpassword/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function UpdateSetPassword(jobj){var request=$http({method:"post",url:"api/user/UpdateSetPassword/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function GetUserById(ID){var request=$http({method:"get",url:"api/user/GetUserById/"+ID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function Getstartpages(){var request=$http({method:"get",url:"api/user/Getstartpages/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function AutoCompleteMemberList(QueryStr,GroupID){var request=$http({method:"post",url:"api/user/AutoCompleteMemberList",data:{QueryString:QueryStr},params:{action:"add"}});return(request.then(handleSuccess,handleError));}
function GetUsers(){var request=$http({method:"get",url:"api/user/GetUsers/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetPendingUsers(){var request=$http({method:"get",url:"api/user/GetPendingUsers/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function ApproveRejectRegisteredUsers(jobj){var request=$http({method:"post",url:"api/user/ApproveRejectRegisteredUsers/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function GetUserByEntityID(EntityID){var request=$http({method:"get",url:"api/user/GetUserByEntityID/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetMembersByEntityID(EntityID){var request=$http({method:"get",url:"api/user/GetMembersByEntityID/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function UserDateTime(){var request=$http({method:"get",url:"api/user/UserDateTime/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function IsUserSesstionPresent(){var request=$http({method:"get",url:"api/user/IsUserSesstionPresent/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function UpdateUserByColumn(jobj){var request=$http({method:"post",url:"api/user/UpdateUserByColumn/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function SaveUserImage(jobj){var request=$http({method:"post",url:"api/user/SaveUserImage/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function GetAllObjectiveMembers(EntityTypeId){var request=$http({method:"get",url:"api/user/GetAllObjectiveMembers/"+EntityTypeId,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function UpdateNewLanguageType(jobj){var request=$http({method:"post",url:"api/user/UpdateNewLanguageType/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function IsSSOUser(){var request=$http({method:"get",url:"api/user/IsSSOUser/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function Logout(){var request=$http({method:"get",url:"api/user/Logout/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function Resetpassword(jobj){var request=$http({method:"post",url:"api/user/Resetpassword/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function Forgetpassword(jobj){var request=$http({method:"post",url:"api/user/Forgetpassword/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function GetAPIusersDetails(){var request=$http({method:"get",url:"api/user/GetAPIusersDetails/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GenerateGuidforSelectedAPI(jobj){var request=$http({method:"post",url:"api/user/GenerateGuidforSelectedAPI/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function InsertUserRegisterDynamicData(jobj){var request=$http({method:"post",url:"api/user/InsertUserRegisterDynamicData/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function SetupToolUserRegister(){var request=$http({method:"get",url:"api/user/SetupToolUserRegister/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function CalendarValidUser(jobj){var request=$http({method:"post",url:"api/user/CalendarValidUser/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function getdimensionunit(){var request=$http({method:"get",url:"api/user/getdimensionunit/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetDimensionunitsettings(){var request=$http({method:"get",url:"api/user/GetDimensionunitsettings/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetTenantPath(){var request=$http({method:"get",url:"api/user/GetTenantPath/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetTenantXMLFile(){var request=$http({method:"get",url:"api/user/GetTenantXMLFile/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function LockResetpassword(jobj){var request=$http({method:"post",url:"api/user/LockResetpassword/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function SendPasswordResetMail(jobj){var request=$http({method:"post",url:"api/user/SendPasswordResetMail/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function GetLockuserDetails(){var request=$http({method:"get",url:"api/user/GetLockuserDetails/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetusermailbyGuid(jobj){var request=$http({method:"post",url:"api/user/GetusermailbyGuid/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function GetApprovalrole(){var request=$http({method:"get",url:"api/User/GetApprovalrole/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function InsertApprovalUserRoles(jobj){var request=$http({method:"post",url:"api/User/InsertApprovalUserRoles/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function GetApprovalRoleUserByID(ID){var request=$http({method:"get",url:"api/User/GetApprovalRoleUserByID/"+ID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetDalimUser(){var request=$http({method:"get",url:"api/User/GetDalimUser",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function SaveUpdateDalimUser(jobj){var request=$http({method:"post",url:"api/User/SaveUpdateDalimUser/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function DeleteDalimUser(ID){var request=$http({method:"delete",url:"api/user/DeleteDalimUser/"+ID,params:{action:"delete",},});return(request.then(handleSuccess,handleError));}
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}}
app.service("UserService",['$http','$q',UserService]);})(angular,app);
///#source 1 1 /app/services/report-service.js
(function (ng, app) {
    "use strict"; function ReportService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({ InsertUpdateReportCredential: InsertUpdateReportCredential, validateReportCredential: validateReportCredential, InsertUpdateReport: InsertUpdateReport, UpdateReportImage: UpdateReportImage, GetReportCredentialByID: GetReportCredentialByID, GetReportByOID: GetReportByOID, GetCustomViewsByID: GetCustomViewsByID, GetViews: GetViews, GetViewsValidate: GetViewsValidate, CustomViews_Update: CustomViews_Update, DeleteView: DeleteView, InsertCustomViews: InsertCustomViews, GetDataviewbyusername: GetDataviewbyusername, ReportLogin: ReportLogin, GetListOfReports: GetListOfReports, ShowReports: ShowReports, UpdateReportSchemaResponse: UpdateReportSchemaResponse, pushviewSchema: pushviewSchema, GetReportViewSchemaResponse: GetReportViewSchemaResponse, ActivityDetailReportSystem: ActivityDetailReportSystem, CostCentreDetailReportSystem: CostCentreDetailReportSystem, ObjectiveDetailReportSystem: ObjectiveDetailReportSystem, GetFinancialSummaryDetlRpt: GetFinancialSummaryDetlRpt, GetFinancialSummaryDetlRptByAttribute: GetFinancialSummaryDetlRptByAttribute, GetEntityFinancialSummaryDetl: GetEntityFinancialSummaryDetl, GetStrucuralRptDetail: GetStrucuralRptDetail, GetReportJSONData: GetReportJSONData, GetEntityTypeAttributeRelationWithLevelsByID: GetEntityTypeAttributeRelationWithLevelsByID, InsertUpdateReportSettingXML: InsertUpdateReportSettingXML, insertupdatefinancialreportsettings: insertupdatefinancialreportsettings, GetFinancialReportSettings: GetFinancialReportSettings, GetAllEntityTypeAttributeRelationWithLevels: GetAllEntityTypeAttributeRelationWithLevels, GenerateFinancialExcel: GenerateFinancialExcel, UpdateFinancialSettingsReportImage: UpdateFinancialSettingsReportImage, DeletefinancialreportByID: DeletefinancialreportByID, GetTaskListReport: GetTaskListReport, GetFulfillmentAttribute: GetFulfillmentAttribute, InsertUpdateCustomlist: InsertUpdateCustomlist, GetAllCustomList: GetAllCustomList, DeleteCustomList: DeleteCustomList, ValidateCustomList: ValidateCustomList, GetFulfillmentAttributeOptions: GetFulfillmentAttributeOptions, insertupdatetabsettings: insertupdatetabsettings, GetLayoutData: GetLayoutData, CalendarDetailReportSystem: CalendarDetailReportSystem, GettableauReportSettings: GettableauReportSettings }); function InsertUpdateReportCredential(formobj) { var request = $http({ method: "post", url: "api/Report/InsertUpdateReportCredential/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
function validateReportCredential(formobj){var request=$http({method:"post",url:"api/Report/validateReportCredential/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function InsertUpdateReport(formobj){var request=$http({method:"post",url:"api/Report/InsertUpdateReport/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function UpdateReportImage(formobj){var request=$http({method:"post",url:"api/Report/UpdateReportImage/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetReportCredentialByID(ID){var request=$http({method:"get",url:"api/Report/GetReportCredentialByID/"+ID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetReportByOID(OID){var request=$http({method:"get",url:"api/Report/GetReportByOID/"+OID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetCustomViewsByID(ID){var request=$http({method:"get",url:"api/Report/GetCustomViewsByID/"+ID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetViews(){var request=$http({method:"get",url:"api/Report/GetViews/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetViewsValidate(formobj){var request=$http({method:"post",url:"api/Report/GetViewsValidate/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function CustomViews_Update(formobj){var request=$http({method:"post",url:"api/Report/CustomViews_Update/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function DeleteView(ID){var request=$http({method:"delete",url:"api/Report/DeleteView/"+ID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function InsertCustomViews(formobj){var request=$http({method:"post",url:"api/Report/InsertCustomViews/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetDataviewbyusername(formobj){var request=$http({method:"post",url:"api/Report/GetDataviewbyusername/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function ReportLogin(formobj){var request=$http({method:"post",url:"api/Report/ReportLogin/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetListOfReports(formobj){var request=$http({method:"post",url:"api/Report/GetListOfReports/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function ShowReports(OID,Show){var request=$http({method:"get",url:"api/Report/ShowReports/"+OID+"/"+Show,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function UpdateReportSchemaResponse(formobj){var request=$http({method:"post",url:"api/Report/UpdateReportSchemaResponse/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function pushviewSchema(formobj){var request=$http({method:"post",url:"api/Report/pushviewSchema/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetReportViewSchemaResponse(){var request=$http({method:"get",url:"api/Report/GetReportViewSchemaResponse/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function ActivityDetailReportSystem(formobj){var request=$http({method:"post",url:"api/Report/ActivityDetailReportSystem/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function CostCentreDetailReportSystem(formobj){var request=$http({method:"post",url:"api/Report/CostCentreDetailReportSystem/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function ObjectiveDetailReportSystem(formobj){var request=$http({method:"post",url:"api/Report/ObjectiveDetailReportSystem/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetFinancialSummaryDetlRpt(SelectedEntityTypeIDs){var request=$http({method:"post",url:"api/Report/GetFinancialSummaryDetlRpt/",params:{action:"add"},data:SelectedEntityTypeIDs});return(request.then(handleSuccess,handleError));}
function GetFinancialSummaryDetlRptByAttribute(jobj){var request=$http({method:"post",url:"api/Report/GetFinancialSummaryDetlRptByAttribute/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function GetEntityFinancialSummaryDetl(EntityTypeID,AttributeID){var request=$http({method:"get",url:"api/Report/GetEntityFinancialSummaryDetl/"+EntityTypeID+"/"+AttributeID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetStrucuralRptDetail(IsshowFinancialDetl,IsDetailIncluded,IsshowTaskDetl,IsshowMemberDetl,ExpandingEntityIDStr,IncludeChildrenStr){var request=$http({method:"get",url:"api/Report/GetStrucuralRptDetail/"+IsshowFinancialDetl+"/"+IsDetailIncluded+"/"+IsshowTaskDetl+"/"+IsshowMemberDetl+"/"+ExpandingEntityIDStr+"/"+IncludeChildrenStr,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetReportJSONData(ReportID){var request=$http({method:"get",url:"api/Report/GetReportJSONData/"+ReportID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetEntityTypeAttributeRelationWithLevelsByID(IDs){var request=$http({method:"get",url:"api/Report/GetEntityTypeAttributeRelationWithLevelsByID/"+IDs,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function InsertUpdateReportSettingXML(formobj){var request=$http({method:"post",url:"api/Report/InsertUpdateReportSettingXML/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function insertupdatefinancialreportsettings(formobj){var request=$http({method:"post",url:"api/Report/insertupdatefinancialreportsettings/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetFinancialReportSettings(){var request=$http({method:"get",url:"api/Report/GetFinancialReportSettings/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetAllEntityTypeAttributeRelationWithLevels(){var request=$http({method:"get",url:"api/Report/GetAllEntityTypeAttributeRelationWithLevels/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GenerateFinancialExcel(ReportID,SessionID){var request=$http({method:"get",url:"api/Report/GenerateFinancialExcel/"+ReportID+"/"+SessionID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function UpdateFinancialSettingsReportImage(formobj){var request=$http({method:"post",url:"api/Report/UpdateFinancialSettingsReportImage/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function DeletefinancialreportByID(ID){var request=$http({method:"delete",url:"api/Report/DeletefinancialreportByID/"+ID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function GetTaskListReport(EntityID,TaskListId,IsEntireList,IsIncludeSublevel){var request=$http({method:"get",url:"api/Report/GetTaskListReport/"+EntityID+"/"+TaskListId+"/"+IsEntireList+"/"+IsIncludeSublevel,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetFulfillmentAttribute(formobj){var request=$http({method:"post",url:"api/Report/GetFulfillmentAttribute/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function InsertUpdateCustomlist(formobj){var request=$http({method:"post",url:"api/Report/InsertUpdateCustomlist/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetAllCustomList(){var request=$http({method:"get",url:"api/Report/GetAllCustomList/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function DeleteCustomList(ID){var request=$http({method:"delete",url:"api/Report/DeleteCustomList/"+ID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function ValidateCustomList(formobj){var request=$http({method:"post",url:"api/Report/ValidateCustomList/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetFulfillmentAttributeOptions(AttributeID,AttributeLevel){var request=$http({method:"get",url:"api/Report/GetFulfillmentAttributeOptions/"+AttributeID+"/"+AttributeLevel,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function insertupdatetabsettings(formobj){var request=$http({method:"post",url:"api/Report/insertupdatetabsettings/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetEntityFinancialSummaryDetl(formobj){var request=$http({method:"post",url:"api/Report/GetEntityFinancialSummaryDetl/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetLayoutData(TabType,TabLocation){var request=$http({method:"get",url:"api/Report/GetLayoutData/"+TabType+"/"+TabLocation,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function CalendarDetailReportSystem(formobj) { var request = $http({ method: "post", url: "api/Report/CalendarDetailReportSystem/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
function GettableauReportSettings(ID) { var request = $http({ method: "get", url: "api/Report/GettableauReportSettings/"+ID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}}
app.service("ReportService",['$http','$q',ReportService]);})(angular,app);
///#source 1 1 /app/services/expirehandler-service.js
(function(ng,app){"use strict";function ExpirehandlerService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({CreateExpireAction:CreateExpireAction,UpdateExpireActionDate:UpdateExpireActionDate,DeleteExpireAction:DeleteExpireAction,GetExpireActions:GetExpireActions,getEntityAttributes:getEntityAttributes,GetEntityTypeMetadataAttributes:GetEntityTypeMetadataAttributes,ValidExpireActionDate:ValidExpireActionDate,GetExpireHandlermetadata:GetExpireHandlermetadata,GetEntityTypeExpireRoleAccess:GetEntityTypeExpireRoleAccess});function CreateExpireAction(CreateExpireAction){var request=$http({method:"post",url:"api/ExpireHandler/CreateExpireAction/",params:{action:"add"},data:{CreateExpireAction:CreateExpireAction}});return(request.then(handleSuccess,handleError));}
function UpdateExpireActionDate(UpdateExpireActionDate){var request=$http({method:"post",url:"api/ExpireHandler/UpdateExpireActionDate/",params:{action:"add"},data:{UpdateExpireActionDate:UpdateExpireActionDate}});return(request.then(handleSuccess,handleError));}
function GetExpireActions(sourceID,AttributeID,ActionID){var request=$http({method:"get",url:"api/ExpireHandler/GetExpireActions/"+sourceID+"/"+AttributeID+"/"+ActionID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function DeleteExpireAction(ActionsourceId){var request=$http({method:"delete",url:"api/ExpireHandler/DeleteExpireAction/"+ActionsourceId,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function getEntityAttributes(entityID){var request=$http({method:"get",url:"api/ExpireHandler/GetEntityAttributes/"+entityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetEntityTypeMetadataAttributes(EntityId){var request=$http({method:"get",url:"api/ExpireHandler/GetEntityTypeAttributeRelationWithLevelsByID/"+EntityId,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function ValidExpireActionDate(ValidExpireActionDate){var request=$http({method:"post",url:"api/ExpireHandler/ValidExpireActionDate/",params:{action:"add"},data:{ValidExpireActionDate:ValidExpireActionDate}});return(request.then(handleSuccess,handleError));}
function GetExpireHandlermetadata(ActionSourceID){var request=$http({method:"get",url:"api/ExpireHandler/GetExpireHandlermetadata/"+ActionSourceID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetEntityTypeExpireRoleAccess(EntityTypeID)
{var request=$http({method:"get",url:"api/ExpireHandler/GetEntityTypeExpireRoleAccess/"+EntityTypeID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}}
app.service("ExpirehandlerService",['$http','$q',ExpirehandlerService]);})(angular,app);
///#source 1 1 /app/services/cms-service.js
(function(ng,app){"use strict";function CmsService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({InsertCMSEntity:InsertCMSEntity,GetAllCmsEntitiesByNavID:GetAllCmsEntitiesByNavID,DeleteCmsEntity:DeleteCmsEntity,InsertRevisedEntityContent:InsertRevisedEntityContent,UpdateRevisedEntityContent:UpdateRevisedEntityContent,GetRevisedContentByFeature:GetRevisedContentByFeature,GetCmsEntityAttributeDetails:GetCmsEntityAttributeDetails,UpdateCmsEntityDetailsBlockValues:UpdateCmsEntityDetailsBlockValues,SaveUploaderImage:SaveUploaderImage,GetCmsEntityPageAccess:GetCmsEntityPageAccess,UpdateCmsEntityPageAccess:UpdateCmsEntityPageAccess,GetCmsEntityPublishVersion:GetCmsEntityPublishVersion,GettingCmsFeedsByEntityID:GettingCmsFeedsByEntityID,InsertNewsFeed:InsertNewsFeed,GetCmsEntityIsActive:GetCmsEntityIsActive,SaveCmsEntityColorcode:SaveCmsEntityColorcode,DuplicateCmsEntities:DuplicateCmsEntities,GetCmsEntitiesByID:GetCmsEntitiesByID,GetCMSBreadCrum:GetCMSBreadCrum,GetIsEditFeatureEnabled:GetIsEditFeatureEnabled,DeleteRevisedEntityContentID:DeleteRevisedEntityContentID,GetAllCmsSnippetTemplates:GetAllCmsSnippetTemplates,DeleteCmsSnippetTemplate:DeleteCmsSnippetTemplate,InsertCmsSnippetTemplate:InsertCmsSnippetTemplate,LinkpublishedfilestoCMS:LinkpublishedfilestoCMS,UpdateSnippetTemplate:UpdateSnippetTemplate,DuplicateCmsSnippetTemplate:DuplicateCmsSnippetTemplate,});function GetAllCmsEntitiesByNavID(NavID,StartpageNo,MaxPageNo){var request=$http({method:"get",url:"api/cms/GetAllCmsEntitiesByNavID/"+NavID+"/"+StartpageNo+"/"+MaxPageNo,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function InsertCMSEntity(cmsentityobj){var request=$http({method:"post",url:"api/cms/InsertCMSEntity/",params:{action:"add"},data:cmsentityobj});return(request.then(handleSuccess,handleError));}
function DeleteCmsEntity(ID){var request=$http({method:"delete",url:"api/cms/DeleteCmsEntity/"+ID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function InsertRevisedEntityContent(revisecmsentityobj){var request=$http({method:"post",url:"api/cms/InsertRevisedEntityContent/",params:{action:"add"},data:revisecmsentityobj});return(request.then(handleSuccess,handleError));}
function UpdateRevisedEntityContent(jobj){var request=$http({method:"post",url:"api/cms/UpdateRevisedEntityContent/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function GetRevisedContentByFeature(EntityID){var request=$http({method:"get",url:"api/cms/GetRevisedContentByFeature/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetCmsEntityAttributeDetails(CmsEntityID){var request=$http({method:"get",url:"api/cms/GetCmsEntityAttributeDetails/"+CmsEntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function UpdateCmsEntityDetailsBlockValues(cmsentityattrvalue){var request=$http({method:"post",url:"api/cms/UpdateCmsEntityDetailsBlockValues/",params:{action:"add"},data:cmsentityattrvalue});return(request.then(handleSuccess,handleError));}
function SaveUploaderImage(jobj){var request=$http({method:"post",url:"api/cms/SaveUploaderImage/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function GetCmsEntityPageAccess(CmsEntityID){var request=$http({method:"get",url:"api/cms/GetCmsEntityPageAccess/"+CmsEntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function UpdateCmsEntityPageAccess(cmspageaccess){var request=$http({method:"post",url:"api/cms/UpdateCmsEntityPageAccess/",params:{action:"add"},data:cmspageaccess});return(request.then(handleSuccess,handleError));}
function GetCmsEntityPublishVersion(CmsEntityID){var request=$http({method:"get",url:"api/cms/GetCmsEntityPublishVersion/"+CmsEntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GettingCmsFeedsByEntityID(CmsEntityID,PageNo,IsRealTime,grouplist){var request=$http({method:"get",url:"api/cms/GettingCmsFeedsByEntityID/"+CmsEntityID+"/"+PageNo+"/"+IsRealTime+"/"+grouplist,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function InsertNewsFeed(cmsnewsfeed){var request=$http({method:"post",url:"api/cms/InsertCmsFeed/",params:{action:"add"},data:cmsnewsfeed});return(request.then(handleSuccess,handleError));}
function SaveCmsEntityColorcode(cmsEntityColor){var request=$http({method:"post",url:"api/cms/SaveCMSEntityColor/",params:{action:"add"},data:cmsEntityColor});return(request.then(handleSuccess,handleError));}
function GetCmsEntityIsActive(CmsEntityID){var request=$http({method:"get",url:"api/cms/IsActiveEntity/"+CmsEntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetCMSBreadCrum(ID){var request=$http({method:"get",url:"api/cms/GetCMSBreadCrum/"+ID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function DuplicateCmsEntities(cmsentitydupobj){var request=$http({method:"post",url:"api/cms/DuplicateCmsEntities/",params:{action:"add"},data:cmsentitydupobj});return(request.then(handleSuccess,handleError));}
function GetCmsEntitiesByID(CmsEntityID){var request=$http({method:"get",url:"api/cms/GetCmsEntitiesByID/"+CmsEntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetIsEditFeatureEnabled(){var request=$http({method:"get",url:"api/cms/GetIsEditFeatureEnabled/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function DeleteRevisedEntityContentID(ID){var request=$http({method:"delete",url:"api/cms/DeleteRevisedEntityContentID/"+ID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function GetAllCmsSnippetTemplates(){var request=$http({method:"get",url:"api/cms/GetAllCmsSnippetTemplates/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function InsertCmsSnippetTemplate(snippetObj){var request=$http({method:"post",url:"api/cms/InsertCmsSnippetTemplate/",params:{action:"add"},data:snippetObj});return(request.then(handleSuccess,handleError));}
function DeleteCmsSnippetTemplate(ID){var request=$http({method:"delete",url:"api/cms/DeleteCmsSnippetTemplate/"+ID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function LinkpublishedfilestoCMS(jobj){var request=$http({method:"post",url:"api/cms/LinkpublishedfilestoCMS/",params:{action:"add"},data:jobj});return(request.then(handleSuccess,handleError));}
function UpdateSnippetTemplate(snippetObj){var request=$http({method:"post",url:"api/cms/UpdateSnippetTemplate/",params:{action:"add"},data:snippetObj});return(request.then(handleSuccess,handleError));}
function DuplicateCmsSnippetTemplate(snippetObj){var request=$http({method:"post",url:"api/cms/DuplicateCmsSnippetTemplate/",params:{action:"add"},data:snippetObj});return(request.then(handleSuccess,handleError));}
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}}
app.service("CmsService",['$http','$q',CmsService]);})(angular,app);
///#source 1 1 /app/services/calender-service.js
(function(ng,app){"use strict";function CalenderService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({GetAllTabCollections:GetAllTabCollections,CreateCalender:CreateCalender,getEntitiesfrCalender:getEntitiesfrCalender,GetCalenders:GetCalenders,GetEntitiesforSelectedCalender:GetEntitiesforSelectedCalender,GettingCalenderFulfillmentBlockDetails:GettingCalenderFulfillmentBlockDetails});function GetAllTabCollections(TypeID){var request=$http({method:"get",url:"api/common/GetCustomTabsByTypeID/"+TypeID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function getEntitiesfrCalender(CalenderFulfill){var request=$http({method:"post",url:"api/planning/GetEntitiesfrCalender/",params:{action:"add"},data:CalenderFulfill});return(request.then(handleSuccess,handleError));}
function CreateCalender(CalenderEntity){var request=$http({method:"post",url:"api/planning/CreateCalender/",params:{action:"add"},data:CalenderEntity});return(request.then(handleSuccess,handleError));}
function GetCalenders(){var request=$http({method:"get",url:"api/planning/GetCalenders/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetEntitiesforSelectedCalender(CalID){var request=$http({method:"get",url:"api/planning/GetEntitiesforSelectedCalender/"+CalID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GettingCalenderFulfillmentBlockDetails(CalID){var request=$http({method:"get",url:"api/planning/GettingCalenderFulfillmentBlockDetail/"+CalID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}}
app.service("CalenderService",['$http','$q',CalenderService]);})(angular,app);

