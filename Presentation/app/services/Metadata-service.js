﻿(function (ng, app) {
    "use strict";

    function MetadataService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session');
        return ({
            EntityType: EntityType,
            PutEntityType: PutEntityType,
            DeleteEntityType: DeleteEntityType,
            InsertEntityTypeFeature: InsertEntityTypeFeature,
            DeleteEntityTypeFeature: DeleteEntityTypeFeature,
            Attribute: Attribute,
            PutAttribute: PutAttribute,
            DeleteAttribute: DeleteAttribute,
            Module: Module,
            PutModule: PutModule,
            GetModuleByID: GetModuleByID,
            Attributetype: Attributetype,
            PutAttributetype: PutAttributetype,
            DeleteAttributetype: DeleteAttributetype,
            DeleteModule: DeleteModule,
            Modulefeature: Modulefeature,
            PutModulefeature: PutModulefeature,
            GetEntityType: GetEntityType,
            GetEntityTypeIsAssociate: GetEntityTypeIsAssociate,
            GetEntityTypefromDB: GetEntityTypefromDB,
            GetFulfillmentFinicalAttribute: GetFulfillmentFinicalAttribute,
            GetEntityTypeByID: GetEntityTypeByID,
            GetAttributetype: GetAttributetype,
            GetAttributeTypeByEntityTypeID: GetAttributeTypeByEntityTypeID,
            GetAttributeByID: GetAttributeByID,
            DeleteModuleFeature: DeleteModuleFeature,
            GetMultiSelect: GetMultiSelect,
            Option: Option,
            PutOption: PutOption,
            DeleteOption: DeleteOption,
            TreeLevel: TreeLevel,
            GetTreelevelByAttributeID: GetTreelevelByAttributeID,
            GetAdminTreelevelByAttributeID: GetAdminTreelevelByAttributeID,
            PutTreeLevel: PutTreeLevel,
            DeleteTreeLevel: DeleteTreeLevel,
            InsertTreeNode: InsertTreeNode,
            InsertAttributeSequencePattern: InsertAttributeSequencePattern,
            DeleteTreeNode: DeleteTreeNode,
            TreeValue: TreeValue,
            PutTreeValue: PutTreeValue,
            DeleteTreeValue: DeleteTreeValue,
            Validation: Validation,
            PostValidation: PostValidation,
            GetAttributeValidationByEntityTypeId: GetAttributeValidationByEntityTypeId,
            GetValidationDationByEntitytype: GetValidationDationByEntitytype,
            DeleteAttributeValidation: DeleteAttributeValidation,
            DeleteValidation: DeleteValidation,
            GetAttribute: GetAttribute,
            GetAttributeWithLevels: GetAttributeWithLevels,
            GetNavigationType: GetNavigationType,
            GetModule: GetModule,
            GetModulefeature: GetModulefeature,
            GetMultiSelect1: GetMultiSelect1,
            GetOption: GetOption,
            GetOptionListID: GetOptionListID,
            GetAdminOptionListID: GetAdminOptionListID,
            GetAttributeSequenceByID: GetAttributeSequenceByID,
            GetOptionDetailListByID: GetOptionDetailListByID,
            GetTreelevel: GetTreelevel,
            GetTreeNode: GetTreeNode,
            GetAttributeTreeNode: GetAttributeTreeNode,
            GetAdminTreeNode: GetAdminTreeNode,
            GetTreeValue: GetTreeValue,
            GetValidation: GetValidation,
            ListSetting: ListSetting,
            EntityTypeAttributeRelation: EntityTypeAttributeRelation,
            GetEntityTypeAttributeRelation: GetEntityTypeAttributeRelation,
            GetEntityTypeAttributeRelationByID: GetEntityTypeAttributeRelationByID,
            GetEntityTypeAttributeRelationWithLevelsByID: GetEntityTypeAttributeRelationWithLevelsByID,
            GetDropDownTree: GetDropDownTree,
            DeleteEntityAttributeRelation: DeleteEntityAttributeRelation,
            SyncToDb: SyncToDb,
            GetFeature: GetFeature,
            GetEntityTypefeature: GetEntityTypefeature,
            GetEntityTypefeatureByID: GetEntityTypefeatureByID,
            GetVersionsCountAndCurrentVersion: GetVersionsCountAndCurrentVersion,
            UpdateActiveVersion: UpdateActiveVersion,
            UpdateWorkingVersion: UpdateWorkingVersion,
            GetTreeNodeByLevel: GetTreeNodeByLevel,
            GettingFilterAttribute: GettingFilterAttribute,
            GettingChildEntityTypes: GettingChildEntityTypes,
            InsertEntityTypeHierarchy: InsertEntityTypeHierarchy,
            GetAttributesForDetailBlock: GetAttributesForDetailBlock,
            SaveDetailBlock: SaveDetailBlock,
            SaveDetailBlockForLevels: SaveDetailBlockForLevels,
            UpdateDropDownTreePricing: UpdateDropDownTreePricing,
            SaveDetailBlockForTreeLevels: SaveDetailBlockForTreeLevels,
            GettingEntityTypeHierarchy: GettingEntityTypeHierarchy,
            GettingEntityTypeHierarchyForRootLevel: GettingEntityTypeHierarchyForRootLevel,
            GetOwnerForEntity: GetOwnerForEntity,
            GetOptionToBind: GetOptionToBind,
            DeleteEntityTypeHierarchy: DeleteEntityTypeHierarchy,
            GetOptionsFromXML: GetOptionsFromXML,
            GetFulfillmentEntityTypes: GetFulfillmentEntityTypes,
            GetFulfillmentEntityTypesfrCal: GetFulfillmentEntityTypesfrCal,
            GetFulfillmentAttribute: GetFulfillmentAttribute,
            GetFulfillmentAttributeOptions: GetFulfillmentAttributeOptions,
            GetAllEntityIds: GetAllEntityIds,
            ActivityRootLevel: ActivityRootLevel,
            ActivityDetail: ActivityDetail,
            ActivityDetailReport: ActivityDetailReport,
            CostCentreDetailReport: CostCentreDetailReport,
            ObjectiveDetailReport: ObjectiveDetailReport,
            CostCentreRootLevel: CostCentreRootLevel,
            CostCentreDetail: CostCentreDetail,
            GetActivityByID: GetActivityByID,
            ObjectiveRootLevel: ObjectiveRootLevel,
            ObjectiveDetail: ObjectiveDetail,
            GetPath: GetPath,
            DeleteOptionByAttributeID: DeleteOptionByAttributeID,
            DeleteSequencByeAttributeID: DeleteSequencByeAttributeID,
            InsertEntityHistory: InsertEntityHistory,
            GetEntityHistoryByID: GetEntityHistoryByID,
            GetTopActivityByID: GetTopActivityByID,
            GetTopMyTaskByID: GetTopMyTaskByID,
            GetWorkFlowDetails: GetWorkFlowDetails,
            CreateWorkFlow: CreateWorkFlow,
            GetAttributefromDB: GetAttributefromDB,
            GetAttributesforDetailFilter: GetAttributesforDetailFilter,
            GettingEntityTypeHierarchyForAdminTree: GettingEntityTypeHierarchyForAdminTree,
            InsertUpdatePredefinedWorkFlow: InsertUpdatePredefinedWorkFlow,
            GetPredefinedWorkflowByID: GetPredefinedWorkflowByID,
            GetWorkFlowDetailsByID: GetWorkFlowDetailsByID,
            GetWorkFlowTStepByID: GetWorkFlowTStepByID,
            GetWorkFlowStepPredefinedTaskByID: GetWorkFlowStepPredefinedTaskByID,
            DeleteWorkflowByID: DeleteWorkflowByID,
            GetPredefinedWorkflowFilesAttchedByID: GetPredefinedWorkflowFilesAttchedByID,
            DeletePredWorkflowFileByID: DeletePredWorkflowFileByID,
            DeletePredefinedWorkflowByID: DeletePredefinedWorkflowByID,
            GetModuleFeatures: GetModuleFeatures,
            GetModuleFeaturesForNavigation: GetModuleFeaturesForNavigation,
            UpdateFeature: UpdateFeature,
            GetTreeNodeByAttributeID: GetTreeNodeByAttributeID,
            SetWorkingVersionFlag: SetWorkingVersionFlag,
            GetMetadataVersion: GetMetadataVersion,
            InsertMetadataVersion: InsertMetadataVersion,
            GetTaskFulfillmentEntityTypes: GetTaskFulfillmentEntityTypes,
            GetXmlNodes_CheckIfValueExistsOrNot: GetXmlNodes_CheckIfValueExistsOrNot,
            GettingEntityForRootLevel: GettingEntityForRootLevel,
            GettingEntityTypeHierarchyForChildActivityType: GettingEntityTypeHierarchyForChildActivityType,
            GetAttributeRelationByIDs: GetAttributeRelationByIDs,
            InsertUpdateAttributeToAttributeRelations: InsertUpdateAttributeToAttributeRelations,
            DeleteAttributeToAttributeRelation: DeleteAttributeToAttributeRelation,
            GetAttributeToAttributeRelationsByID: GetAttributeToAttributeRelationsByID,
            GetTreeNodeByEntityID: GetTreeNodeByEntityID,
            GetAttributeToAttributeRelationsByIDForEntity: GetAttributeToAttributeRelationsByIDForEntity,
            GetAttributeToAttributeRelationsByIDForEntityInOverView: GetAttributeToAttributeRelationsByIDForEntityInOverView,
            GetAttributeOptionsInAttrToAttrRelations: GetAttributeOptionsInAttrToAttrRelations,
            EntityTypeStatusOptions: EntityTypeStatusOptions,
            DamTypeFileExtensionOptions: DamTypeFileExtensionOptions,
            GetEntityStatusOptions: GetEntityStatusOptions,
            GetDamTypeFileExtensionOptions: GetDamTypeFileExtensionOptions,
            GetAllDamTypeFileExtensionOptions: GetAllDamTypeFileExtensionOptions,
            DeleteEntityTypeStatusOptions: DeleteEntityTypeStatusOptions,
            DeleteDamTypeFileExtensionOptions: DeleteDamTypeFileExtensionOptions,
            GetEntityStatus: GetEntityStatus,
            RootLevelEntityTypeHierarchy: RootLevelEntityTypeHierarchy,
            ChildEntityTypeHierarchy: ChildEntityTypeHierarchy,
            MyworkSpaceDetails: MyworkSpaceDetails,
            GetOptionDetailListByIDOptimised: GetOptionDetailListByIDOptimised,
            GetWorkspacePath: GetWorkspacePath,
            ReportViewCreationAndPushSchema: ReportViewCreationAndPushSchema,
            GetAllModuleFeatures: GetAllModuleFeatures,
            InsertUpdateAttributeGroupAndAttributeRelation: InsertUpdateAttributeGroupAndAttributeRelation,
            GetAttributeGroup: GetAttributeGroup,
            GetAttributeGroupAttributeRelation: GetAttributeGroupAttributeRelation,
            DeleteAttributeGroup: DeleteAttributeGroup,
            DeleteAttributeGroupAttributeRelation: DeleteAttributeGroupAttributeRelation,
            InsertUpdateEntityTypeAttributeGroup: InsertUpdateEntityTypeAttributeGroup,
            GetEntityTypeAttributeGroupRelation: GetEntityTypeAttributeGroupRelation,
            DeleteEntityTypeAttributeGroupRelation: DeleteEntityTypeAttributeGroupRelation,
            GetAttributeGroupAttributeOptions: GetAttributeGroupAttributeOptions,
            GetEntityAttributesGroupValues: GetEntityAttributesGroupValues,
            DuplicateEntityType: DuplicateEntityType,
            GetEntityTypeAttributeRelationWithLevelsByIDForUserDetails: GetEntityTypeAttributeRelationWithLevelsByIDForUserDetails,
            GetUserDetailsAttributes: GetUserDetailsAttributes,
            GetOptionDetailListByIDForMyPage: GetOptionDetailListByIDForMyPage,
            GetUserRegistrationAttributes: GetUserRegistrationAttributes,
            GetOptionValuesForUserRegistration: GetOptionValuesForUserRegistration,
            GetDynamicAttrValidationDetails: GetDynamicAttrValidationDetails,
            SaveDetailBlockForLevelsFromMyPage: SaveDetailBlockForLevelsFromMyPage,
            GetUserVisiblity: GetUserVisiblity,
            InsertUserVisibleInfo: InsertUserVisibleInfo,
            InsertUpdateUserVisibleInfo: InsertUpdateUserVisibleInfo,
            addFinancialAttribute: addFinancialAttribute,
            addFinancialOption: addFinancialOption,
            GetFinancialAttribute: GetFinancialAttribute,
            DeleteFinancialAttribute: DeleteFinancialAttribute,
            DeleteFinancialOptionByAttributeID: DeleteFinancialOptionByAttributeID,
            UpdateFinancialMetadata: UpdateFinancialMetadata,
            DeleteFinMetadata: DeleteFinMetadata,
            UpdateFinMetadataSortOrder: UpdateFinMetadataSortOrder,
            GetFinancialAttributeOptions: GetFinancialAttributeOptions,
            DeleteFinancialOption: DeleteFinancialOption,
            GettingEntityTypeTreeStructure: GettingEntityTypeTreeStructure,
            GetSubEntityTypeAccessPermission: GetSubEntityTypeAccessPermission,
            InsertUpdateEntityTypeRoleAccess: InsertUpdateEntityTypeRoleAccess,
            GetEntityTypeRoleAcl: GetEntityTypeRoleAcl,
            DeleteEntityTypeRoleAcl: DeleteEntityTypeRoleAcl,
            GetRoleFeatures: GetRoleFeatures,
            SaveUpdateRoleFeatures: SaveUpdateRoleFeatures,
            GetAttributeTreeNodeByEntityID: GetAttributeTreeNodeByEntityID,
            GetDAMEntityTypes: GetDAMEntityTypes,
            GetWorkspacePermission: GetWorkspacePermission,
            GetDropDownTreePricingObject: GetDropDownTreePricingObject,
            GetDropDownTreePricingObjectFromParentDetail: GetDropDownTreePricingObjectFromParentDetail,
            SaveDetailBlockForLink: SaveDetailBlockForLink,
            DamDeleteEntityAttributeRelation: DamDeleteEntityAttributeRelation,
            DeleteEntitytypeAttributeGrpAccessRole: DeleteEntitytypeAttributeGrpAccessRole,
            GetAttributeGroupImportedFileColumnName: GetAttributeGroupImportedFileColumnName,
            InsertImportedAttributeGroupData: InsertImportedAttributeGroupData,
            FetchEntityStatusTree: FetchEntityStatusTree,
            GetTaskEntityType: GetTaskEntityType,
            GetTaskTypes: GetTaskTypes,
            GetEntityTaskType: GetEntityTaskType,
            InsertUpdateEntityTaskType: InsertUpdateEntityTaskType,
            GetEntityAttributesGroupLabelNames: GetEntityAttributesGroupLabelNames,
            CalenderRootLevel: CalenderRootLevel,
            CalenderDetail: CalenderDetail,
            GetTagOptionList: GetTagOptionList,
            InsertUpdateTagOption: InsertUpdateTagOption,
            DeleteTagOption: DeleteTagOption,
            AssetAccess: AssetAccess,
            DeleteAssetAccessOption: DeleteAssetAccessOption,
            GetAssestAccessSaved: GetAssestAccessSaved,
            GetAssignedAssetRoleIDs: GetAssignedAssetRoleIDs,
            GetAssignedAssetRoleName: GetAssignedAssetRoleName,
            GetAssetAccess: GetAssetAccess,
            ExportMetadataSettingsXml: ExportMetadataSettingsXml,
            ImportMetadataSettingsXml: ImportMetadataSettingsXml,
            SetIsCurrentWorkingXml: SetIsCurrentWorkingXml,
            GetOwnerName: GetOwnerName,
            CostCentreTreeDetail: CostCentreTreeDetail,
            getTreeCount: getTreeCount,
            CalenderDetailReport: CalenderDetailReport,
            getParentID: getParentID,
            GetEntityGlobalAccessInDetail: GetEntityGlobalAccessInDetail,
            InsertEntityGlobalAccessInDetail: InsertEntityGlobalAccessInDetail,
            GettingFilterEntityMember: GettingFilterEntityMember,
            GetTaskLinkedToEntity: GetTaskLinkedToEntity,
            GetListOfCmsCustomFont: GetListOfCmsCustomFont,
            InsertUpdateCmsCustomFont: InsertUpdateCmsCustomFont,
            DeleteCmsCustomFont: DeleteCmsCustomFont,
            InsertUpdateCmsCustomStyle: InsertUpdateCmsCustomStyle,
            GetListOfCmsCustomStyle: GetListOfCmsCustomStyle,
            DeleteCmsCustomStyle: DeleteCmsCustomStyle,
            InsertUpdateCmsTreeStyleSettings: InsertUpdateCmsTreeStyleSettings,
            GetCmsTreeStyle: GetCmsTreeStyle,
            SaveImageFromBaseFormat: SaveImageFromBaseFormat,
            CheckBeforeRemovechecklist: CheckBeforeRemovechecklist,
            InsertApprovalRole: InsertApprovalRole,
            GetApprovalRoles: GetApprovalRoles,
            DeleteApprovalRole: DeleteApprovalRole,
            GetEntityTypeAttributes: GetEntityTypeAttributes,
            updateentitytypehelptext: updateentitytypehelptext,
            GetAllFinancialAttribute: GetAllFinancialAttribute,
            UpdateFinancialHelptext: UpdateFinancialHelptext,
            ExportEntityHelpTextListtoExcel: ExportEntityHelpTextListtoExcel,
            InsertEntityHelpTextImport: InsertEntityHelpTextImport
            , InsertUpdateEntityObjectiveType: InsertUpdateEntityObjectiveType
            , GetObjectiveEntityType: GetObjectiveEntityType,

            getEntityToSetOverallStauts: getEntityToSetOverallStauts,
            getCurrentEntityStatus: getCurrentEntityStatus,
            updateEntityOverviewStatus: updateEntityOverviewStatus
        });

        function GetTaskLinkedToEntity(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetTaskLinkedToEntity/" + ID,
                parms: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function EntityType(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/EntityType/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function PutEntityType(formobj) {
            var request = $http({
                method: "put",
                url: "api/Metadata/PutEntityType/",
                params: {
                    action: "update"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteEntityType(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteEntityType/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertEntityTypeFeature(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertEntityTypeFeature/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteEntityTypeFeature(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteEntityTypeFeature/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function Attribute(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/Attribute/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function PutAttribute(formobj) {
            var request = $http({
                method: "put",
                url: "api/Metadata/PutAttribute/",
                params: {
                    action: "update"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteAttribute(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteAttribute/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function Module(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/Module/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function PutModule(formobj) {
            var request = $http({
                method: "put",
                url: "api/Metadata/PutModule/",
                params: {
                    action: "update"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetModuleByID(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetModuleByID/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function Attributetype(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/Attributetype/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function PutAttributetype(formobj) {
            var request = $http({
                method: "put",
                url: "api/Metadata/PutAttributetype/",
                params: {
                    action: "update"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteAttributetype(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteAttributetype/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteModule(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteModule/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function Modulefeature(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/Modulefeature/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function PutModulefeature(formobj) {
            var request = $http({
                method: "put",
                url: "api/Metadata/PutModulefeature/",
                params: {
                    action: "update"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityType(ModuleID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityType/" + ModuleID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityTypeIsAssociate() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityTypeIsAssociate/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityTypefromDB() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityTypefromDB/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetFulfillmentFinicalAttribute(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetFulfillmentFinicalAttribute/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityTypeByID(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityTypeByID/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributetype() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributetype/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeTypeByEntityTypeID(EntityTypeID, IsAdmin) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributeTypeByEntityTypeID/" + EntityTypeID + "/" + IsAdmin,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeByID(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributeByID/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteModuleFeature(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteModuleFeature/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetMultiSelect(EntityID, AttributeID, OptionID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetMultiSelect/" + EntityID + "/" + AttributeID + "/" + OptionID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function Option(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/Option/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function PutOption(formobj) {
            var request = $http({
                method: "put",
                url: "api/Metadata/PutOption/",
                params: {
                    action: "update"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteOption(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteOption/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function TreeLevel(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/TreeLevel/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTreelevelByAttributeID(AttributeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetTreelevelByAttributeID/" + AttributeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAdminTreelevelByAttributeID(AttributeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAdminTreelevelByAttributeID/" + AttributeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function PutTreeLevel(formobj) {
            var request = $http({
                method: "put",
                url: "api/Metadata/PutTreeLevel/",
                params: {
                    action: "update"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteTreeLevel(TreeLevelID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteTreeLevel/" + TreeLevelID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertTreeNode(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertTreeNode",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertAttributeSequencePattern(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertAttributeSequencePattern",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteTreeNode(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteTreeNode/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function TreeValue(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/TreeValue/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function PutTreeValue(formobj) {
            var request = $http({
                method: "put",
                url: "api/Metadata/PutTreeValue/",
                params: {
                    action: "update"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteTreeValue(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteTreeValue/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function Validation() {
            var request = $http({
                method: "post",
                url: "api/Metadata/Validation/",
                params: {
                    action: "add"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function PostValidation(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/PostValidation/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeValidationByEntityTypeId(EntityTypeID, AttributeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributeValidationByEntityTypeId/" + EntityTypeID + "/" + AttributeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetValidationDationByEntitytype(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetValidationDationByEntitytype/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteAttributeValidation(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteAttributeValidation/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteValidation(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteValidation/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttribute() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttribute/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeWithLevels() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributeWithLevels/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetNavigationType() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetNavigationType/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetModule() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetModule/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetModulefeature(Version) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetModulefeature/" + Version,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetMultiSelect1(Version) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetMultiSelect1/" + Version,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetOption(Version) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetOption/" + Version,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetOptionListID(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetOptionListID/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAdminOptionListID(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAdminOptionListID/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeSequenceByID(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributeSequenceByID/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetOptionDetailListByID(ID, EntityID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetOptionDetailListByID/" + ID + "/" + EntityID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTreelevel(Version) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetTreelevel/" + Version,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTreeNode(AttributeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetTreeNode/" + AttributeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeTreeNode(AttributeID, EntityID, Ischoosefromparent) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributeTreeNode/" + AttributeID + "/" + EntityID + "/" + Ischoosefromparent,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAdminTreeNode(AttributeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAdminTreeNode/" + AttributeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTreeValue(Version) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetTreeValue/" + Version,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetValidation(Version) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetValidation/" + Version,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function ListSetting(elementNode, entitytype) {
            var request = $http({
                method: "get",
                url: "api/Metadata/ListSetting/" + elementNode + "/" + entitytype,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function EntityTypeAttributeRelation(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/EntityTypeAttributeRelation/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityTypeAttributeRelation(Version) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityTypeAttributeRelation/" + Version,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityTypeAttributeRelationByID(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityTypeAttributeRelationByID/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetDropDownTree(choosefromparent, AttributeID, IsInheritFromParent, Isfromparent, Entityid, ParentId) {
            if (choosefromparent) GetDropDownTreePricingObjectFromParentDetail(AttributeID, IsInheritFromParent, Isfromparent, Entityid, ParentId);
            else GetDropDownTreePricingObject(AttributeID, IsInheritFromParent, Isfromparent, Entityid, ParentId);
        }

        function GetEntityTypeAttributeRelationWithLevelsByID(ID, ParentID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityTypeAttributeRelationWithLevelsByID/" + ID + "/" + ParentID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteEntityAttributeRelation(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteEntityAttributeRelation/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function SyncToDb() {
            var request = $http({
                method: "post",
                url: "api/Metadata/SyncToDb/",
                params: {
                    action: "add"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetFeature() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetFeature/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityTypefeature(Version) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityTypefeature/" + Version,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityTypefeatureByID(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityTypefeatureByID/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetVersionsCountAndCurrentVersion(key) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetVersionsCountAndCurrentVersion/" + key,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateActiveVersion(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/UpdateActiveVersion/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateWorkingVersion(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/UpdateWorkingVersion/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTreeNodeByLevel(AttributeID, Level) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetTreeNodeByLevel/" + AttributeID + "/" + Level,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GettingFilterAttribute(formobj) {
            var request = $http({
                method: "post",
                ignoreLoadingBar: true,
                url: "api/Metadata/GettingFilterAttribute/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GettingChildEntityTypes(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GettingChildEntityTypes/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertEntityTypeHierarchy(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertEntityTypeHierarchy/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributesForDetailBlock(EntityID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributesForDetailBlock/" + EntityID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function SaveDetailBlock(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/SaveDetailBlock/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function SaveDetailBlockForLevels(jobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/SaveDetailBlockForLevels/",
                params: {
                    action: "add"
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateDropDownTreePricing(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/UpdateDropDownTreePricing/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function SaveDetailBlockForTreeLevels(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/SaveDetailBlockForTreeLevels/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GettingEntityTypeHierarchy(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GettingEntityTypeHierarchy/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GettingEntityTypeHierarchyForRootLevel(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GettingEntityTypeHierarchyForRootLevel/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetOwnerForEntity(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetOwnerForEntity/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetOptionToBind() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetOwnerForEntity/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteEntityTypeHierarchy(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteEntityTypeHierarchy/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetOptionsFromXML(elementNode, typeid) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetOptionsFromXML/" + elementNode + "/" + typeid,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetFulfillmentEntityTypes() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetFulfillmentEntityTypes/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetFulfillmentEntityTypesfrCal() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetFulfillmentEntityTypesfrCal/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetFulfillmentAttribute(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetFulfillmentAttribute/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetFulfillmentAttributeOptions(AttributeID, AttributeLevel) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetFulfillmentAttributeOptions/" + AttributeID + "/" + AttributeLevel,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAllEntityIds() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAllEntityIds/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function ActivityRootLevel(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/ActivityRootLevel/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function ActivityDetail(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/ActivityDetail/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function ActivityDetailReport(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/ActivityDetailReport/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function CostCentreDetailReport(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/CostCentreDetailReport/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function ObjectiveDetailReport(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/ObjectiveDetailReport/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function ActivityFilter(dimenID, StartRowNo, MaxNoofRow, FilterID, SortOrderColumn, IsDesc, IncludeChildren, EntityID, UserID, Level, ExpandingEntityID, IsSingleID, getactivities) {
            if (dimenID = "1") CostCentreRootLevel(StartRowNo, MaxNoofRow, FilterID, SortOrderColumn, IsDesc, IncludeChildren, EntityID, UserID, Level, getactivities);
            else if (dimenID = "2") ActivityDetail(StartRowNo, MaxNoofRow, FilterID, SortOrderColumn, IsDesc, IncludeChildren, EntityID, Level, ExpandingEntityID, IsSingleID, getactivities);
        }

        function CostCentreRootLevel(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/CostCentreRootLevel/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function CostCentreDetail(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/CostCentreDetail/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetActivityByID(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/GetActivityByID/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function ObjectiveRootLevel(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/ObjectiveRootLevel/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function ObjectiveDetail(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/ObjectiveDetail/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetPath(EntityID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetPath/" + EntityID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteOptionByAttributeID(attributeid) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteOptionByAttributeID/" + attributeid,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteSequencByeAttributeID(attributeid) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteSequencByeAttributeID/" + attributeid,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertEntityHistory(EntityID, UserID) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertEntityHistory/",
                params: {
                    action: "add"
                },
                data: {
                    EntityID: EntityID,
                    UserID: UserID
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityHistoryByID(UserID, Topx) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityHistoryByID/" + UserID + "/" + Topx,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTopActivityByID(UserID, Topx) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetTopActivityByID/" + UserID + "/" + Topx,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTopMyTaskByID(UserID, Topx) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetTopMyTaskByID/" + UserID + "/" + Topx,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetWorkFlowDetails() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetWorkFlowDetails/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function CreateWorkFlow(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/CreateWorkFlow/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributefromDB() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributefromDB/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributesforDetailFilter() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributesforDetailFilter/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GettingEntityTypeHierarchyForAdminTree(EntityTypeID, ModuleID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GettingEntityTypeHierarchyForAdminTree/" + EntityTypeID + "/" + ModuleID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertUpdatePredefinedWorkFlow(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertUpdatePredefinedWorkFlow/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetPredefinedWorkflowByID(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetPredefinedWorkflowByID/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetWorkFlowDetailsByID(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetWorkFlowDetailsByID/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetWorkFlowTStepByID(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetWorkFlowTStepByID/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetWorkFlowStepPredefinedTaskByID(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetWorkFlowStepPredefinedTaskByID/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteWorkflowByID(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteWorkflowByID/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetPredefinedWorkflowFilesAttchedByID(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetPredefinedWorkflowFilesAttchedByID/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeletePredWorkflowFileByID(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeletePredWorkflowFileByID/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeletePredefinedWorkflowByID(ID, Name, Description, WorkflowType) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeletePredefinedWorkflowByID/" + ID + "/" + Name + "/" + Description + "/" + WorkflowType,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetModuleFeatures(moduleID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetModuleFeatures/" + moduleID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetModuleFeaturesForNavigation(moduleID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetModuleFeaturesForNavigation/" + moduleID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateFeature(formobj) {
            var request = $http({
                method: "put",
                url: "api/Metadata/UpdateFeature/",
                params: {
                    action: "update"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTreeNodeByAttributeID(AttributeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetTreeNodeByAttributeID/" + AttributeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function SetWorkingVersionFlag(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/SetWorkingVersionFlag/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetMetadataVersion() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetMetadataVersion/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertMetadataVersion(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertMetadataVersion/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTaskFulfillmentEntityTypes() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetTaskFulfillmentEntityTypes/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetXmlNodes_CheckIfValueExistsOrNot() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetXmlNodes_CheckIfValueExistsOrNot/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GettingEntityForRootLevel(IsRootLevel) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GettingEntityForRootLevel/" + IsRootLevel,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GettingEntityTypeHierarchyForChildActivityType(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GettingEntityTypeHierarchyForChildActivityType/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeRelationByIDs(formobj) {

            var request = $http({
                method: "post",
                url: "api/Metadata/GetAttributeRelationByIDs/",
                params: {
                    action: "add"
                },
                data:
                     formobj
            });
            return (request.then(handleSuccess, handleError));

        }

        function InsertUpdateAttributeToAttributeRelations(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertUpdateAttributeToAttributeRelations/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteAttributeToAttributeRelation(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteAttributeToAttributeRelation/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeToAttributeRelationsByID(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributeToAttributeRelationsByID/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTreeNodeByEntityID(AttributeID, ParentEntityID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetTreeNodeByEntityID/" + AttributeID + "/" + ParentEntityID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeToAttributeRelationsByIDForEntity(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributeToAttributeRelationsByIDForEntity/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeToAttributeRelationsByIDForEntityInOverView(EntityTypeID, EntityID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributeToAttributeRelationsByIDForEntityInOverView/" + EntityTypeID + "/" + EntityID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeOptionsInAttrToAttrRelations(Attrval) {
            var request = $http({
                method: "post",
                url: "api/Metadata/GetAttributeOptionsInAttrToAttrRelations/",
                params: {
                    action: "add"
                },
                data: Attrval
            });
            return (request.then(handleSuccess, handleError));
        }

        function EntityTypeStatusOptions(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/EntityTypeStatusOptions/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DamTypeFileExtensionOptions(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/DamTypeFileExtensionOptions/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityStatusOptions(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityStatusOptions/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetDamTypeFileExtensionOptions(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetDamTypeFileExtensionOptions/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAllDamTypeFileExtensionOptions() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAllDamTypeFileExtensionOptions/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteEntityTypeStatusOptions(EntityTypeID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteEntityTypeStatusOptions/" + EntityTypeID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteDamTypeFileExtensionOptions(EntityTypeID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteDamTypeFileExtensionOptions/" + EntityTypeID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityStatus(entityTypeID, isAdmin, entityId) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityStatus/" + entityTypeID + "/" + isAdmin + "/" + entityId,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function RootLevelEntityTypeHierarchy() {
            var request = $http({
                method: "get",
                url: "api/Metadata/RootLevelEntityTypeHierarchy/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function ChildEntityTypeHierarchy() {
            var request = $http({
                method: "get",
                url: "api/Metadata/ChildEntityTypeHierarchy/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function MyworkSpaceDetails(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/MyworkSpaceDetails/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetOptionDetailListByIDOptimised(EntityTypeID, EntityID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetOptionDetailListByIDOptimised/" + EntityTypeID + "/" + EntityID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetWorkspacePath(EntityID, IsWorkspace) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetWorkspacePath/" + EntityID + "/" + IsWorkspace,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function ReportViewCreationAndPushSchema() {
            var request = $http({
                method: "get",
                url: "api/Metadata/ReportViewCreationAndPushSchema/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAllModuleFeatures(moduleID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAllModuleFeatures/" + moduleID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertUpdateAttributeGroupAndAttributeRelation(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertUpdateAttributeGroupAndAttributeRelation/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeGroup() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributeGroup/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeGroupAttributeRelation(AttributeGroupID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributeGroupAttributeRelation/" + AttributeGroupID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteAttributeGroup(attributegroupid) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteAttributeGroup/" + attributegroupid,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteAttributeGroupAttributeRelation(attributeRelationId) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteAttributeGroupAttributeRelation/" + attributeRelationId,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertUpdateEntityTypeAttributeGroup(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertUpdateEntityTypeAttributeGroup/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityTypeAttributeGroupRelation(entitytypeId, EntityID, AttributeGroupId) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityTypeAttributeGroupRelation/" + entitytypeId + "/" + EntityID + "/" + AttributeGroupId,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteEntityTypeAttributeGroupRelation(attributegroupId) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteEntityTypeAttributeGroupRelation/" + attributegroupId,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeGroupAttributeOptions(ID, EntityID, AttributeRecordID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributeGroupAttributeOptions/" + ID + "/" + EntityID + "/" + AttributeRecordID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityAttributesGroupValues(EntityID, EntityTypeID, GroupID, IsCmsContent) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityAttributesGroupValues/" + EntityID + "/" + EntityTypeID + "/" + GroupID + "/" + IsCmsContent,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DuplicateEntityType(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/DuplicateEntityType/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityTypeAttributeRelationWithLevelsByIDForUserDetails(ID, ParentID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityTypeAttributeRelationWithLevelsByIDForUserDetails/" + ID + "/" + ParentID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetUserDetailsAttributes(TypeID, UserID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetUserDetailsAttributes/" + TypeID + "/" + UserID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetOptionDetailListByIDForMyPage(ID, UserID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetOptionDetailListByIDForMyPage/" + ID + "/" + UserID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetUserRegistrationAttributes() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetUserRegistrationAttributes/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetOptionValuesForUserRegistration() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetOptionValuesForUserRegistration/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetDynamicAttrValidationDetails() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetDynamicAttrValidationDetails/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function SaveDetailBlockForLevelsFromMyPage(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/SaveDetailBlockForLevelsFromMyPage/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetUserVisiblity() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetUserVisiblity/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertUserVisibleInfo(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertUserVisibleInfo/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertUpdateUserVisibleInfo(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertUpdateUserVisibleInfo/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertSetUpToolAttributes(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertSetUpToolAttributes/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function addFinancialAttribute(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/addFinancialAttribute/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function addFinancialOption(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/addFinancialOption/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetFinancialAttribute() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetFinancialAttribute/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteFinancialAttribute(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/DeleteFinancialAttribute/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteFinancialOptionByAttributeID(attributeid) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteFinancialOptionByAttributeID/" + attributeid,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateFinancialMetadata(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/UpdateFinancialMetadata/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteFinMetadata(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteFinMetadata/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateFinMetadataSortOrder(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/UpdateFinMetadataSortOrder/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetFinancialAttributeOptions(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetFinancialAttributeOptions/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteFinancialOption(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteFinancialOption/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GettingEntityTypeTreeStructure(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GettingEntityTypeTreeStructure/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetSubEntityTypeAccessPermission(EntityID, EntityTypeID, moduleID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetSubEntityTypeAccessPermission/" + EntityID + "/" + EntityTypeID + "/" + moduleID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertUpdateEntityTypeRoleAccess(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertUpdateEntityTypeRoleAccess/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityTypeRoleAcl(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityTypeRoleAcl/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteEntityTypeRoleAcl(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteEntityTypeRoleAcl/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetRoleFeatures(GlobalRoleID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetRoleFeatures/" + GlobalRoleID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function SaveUpdateRoleFeatures(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/SaveUpdateRoleFeatures/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeTreeNodeByEntityID(AttributeID, EntityID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributeTreeNodeByEntityID/" + AttributeID + "/" + EntityID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetDAMEntityTypes() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetDAMEntityTypes/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetWorkspacePermission() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetWorkspacePermission/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetDropDownTreePricingObject(AttributeID, IsInheritFromParent, Isfromparent, Entityid, ParentId) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetDropDownTreePricingObject/" + AttributeID + "/" + IsInheritFromParent + "/" + Isfromparent + "/" + Entityid + "/" + ParentId,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetDropDownTreePricingObjectFromParentDetail(AttributeID, IsInheritFromParent, Isfromparent, Entityid, ParentId) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetDropDownTreePricingObjectFromParentDetail/" + AttributeID + "/" + IsInheritFromParent + "/" + Isfromparent + "/" + Entityid + "/" + ParentId,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function SaveDetailBlockForLink(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/SaveDetailBlockForLink/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DamDeleteEntityAttributeRelation(ID, AttributeID, EntitytypeID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DamDeleteEntityAttributeRelation/" + ID + "/" + AttributeID + "/" + EntitytypeID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteEntitytypeAttributeGrpAccessRole(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteEntitytypeAttributeGrpAccessRole/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeGroupImportedFileColumnName(FileID) {
            var request = $http({
                method: "post",
                url: "api/Metadata/GetAttributeGroupImportedFileColumnName/",
                params: {
                    action: "add"
                },
                data: { 'FileID': FileID }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertImportedAttributeGroupData(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertImportedAttributeGroupData/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function FetchEntityStatusTree(EntityID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/FetchEntityStatusTree/" + EntityID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTaskEntityType() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetTaskEntityType/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTaskTypes() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetTaskTypes/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityTaskType(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityTaskType/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertUpdateEntityTaskType(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertUpdateEntityTaskType/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityAttributesGroupLabelNames(EntityID, GroupID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityAttributesGroupLabelNames/" + EntityID + "/" + GroupID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function CalenderRootLevel(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/CalenderRootLevel/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function CalenderDetail(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/CalenderDetail/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTagOptionList(IsAdmin) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetTagOptionList/" + IsAdmin,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertUpdateTagOption(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertUpdateTagOption/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteTagOption(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteTagOption/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function AssetAccess(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/AssetAccess/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteAssetAccessOption(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteAssetAccessOption/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAssestAccessSaved() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAssestAccessSaved/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAssignedAssetRoleIDs(AssetId) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAssignedAssetRoleIDs/" + AssetId,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAssignedAssetRoleName(RoleId) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAssignedAssetRoleName/" + RoleId,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAssetAccess() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAssetAccess/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function ExportMetadataSettingsXml(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/ExportMetadataSettingsXml/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function ImportMetadataSettingsXml(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/ImportMetadataSettingsXml/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function CostCentreTreeDetail(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/CostCentreTreeDetail/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function SetIsCurrentWorkingXml() {
            var request = $http({
                method: "post",
                url: "api/Metadata/SetIsCurrentWorkingXml/",
                params: {
                    action: "add"
                },
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetOwnerName(EntityID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetOwnerName/" + EntityID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function getTreeCount(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/getTreeCount/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function CalenderDetailReport(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/CalenderDetailReport/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function getParentID(ID) {
            var request = $http({
                method: 'get',
                url: 'api/metadata/getParentID/' + ID,
                params: {
                    action: 'get'
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityGlobalAccessInDetail(EntityID) {
            var request = $http({
                method: 'get',
                url: 'api/metadata/GetEntityGlobalAccessInDetail/' + EntityID,
                params: {
                    action: 'get'
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertEntityGlobalAccessInDetail(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertEntityGlobalAccessInDetail/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetListOfCmsCustomFont() {
            var request = $http({
                method: 'get',
                url: 'api/metadata/GetListOfCmsCustomFont',
                params: {
                    action: 'get'
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertUpdateCmsCustomFont(customfonts) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertUpdateCmsCustomFont/",
                params: {
                    action: "add"
                },
                data: customfonts
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteCmsCustomFont(ProviderId) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteCmsCustomFont/" + ProviderId,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertUpdateCmsCustomStyle(customstyle) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertUpdateCmsCustomStyle/",
                params: {
                    action: "add"
                },
                data: customstyle
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetListOfCmsCustomStyle() {
            var request = $http({
                method: 'get',
                url: 'api/metadata/GetListOfCmsCustomStyle',
                params: {
                    action: 'get'
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteCmsCustomStyle(Id) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteCmsCustomStyle/" + Id,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertUpdateCmsTreeStyleSettings(treestyle) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertUpdateCmsTreeStyleSettings/",
                params: {
                    action: "add"
                },
                data: treestyle
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetCmsTreeStyle() {
            var request = $http({
                method: 'get',
                url: 'api/metadata/GetCmsTreeStyle',
                params: {
                    action: 'get'
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function SaveImageFromBaseFormat(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/SaveImageFromBaseFormat/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GettingFilterEntityMember(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/GettingFilterEntityMember/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function CheckBeforeRemovechecklist(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/CheckBeforeRemovechecklist/" + ID,
                parms: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetApprovalRoles() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetApprovalRoles/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertApprovalRole(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertApprovalRole/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteApprovalRole(ID) {
            var request = $http({
                method: "delete",
                url: "api/Metadata/DeleteApprovalRole/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }
        function UpdateFinancialHelptext(fromobj) {
            var request = $http({
                method: "post",
                url: 'api/Metadata/UpdateFinancialHelptext',
                parms: {
                    action: "add"
                },
                data: fromobj

            });
            return (request.then(handleSuccess, handleError));
        }
        function GetAllFinancialAttribute() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAllFinancialAttribute",
                parms: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityTypeAttributes() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityTypeAttributes",
                parms: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function updateentitytypehelptext(fromobj) {
            var request = $http({
                method: "post",
                url: 'api/Metadata/updateentitytypehelptext',
                parms: {
                    action: "add"
                },
                data: fromobj

            });
            return (request.then(handleSuccess, handleError));
        }

        function ExportEntityHelpTextListtoExcel() {
            var request = $http({
                method: "get",
                url: "api/Metadata/ExportEntityHelpTextListtoExcel",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertEntityHelpTextImport(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertEntityHelpTextImport/",
                params: {
                    action: "add"
                },
                data: formobj
            });

            return (request.then(handleSuccess, handleError));
        }

        function InsertUpdateEntityObjectiveType(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertUpdateEntityObjectiveType/",
                params: {
                    action: "add"
                },
                data:
                     formobj

            });
            return (request.then(handleSuccess, handleError));
        }
        function GetObjectiveEntityType(IsAdmin) {

            var request = $http({
                method: "get",
                url: "api/Metadata/GetObjectiveEntityType/" + IsAdmin,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function getEntityToSetOverallStauts(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/getEntityToSetOverallStauts/" + ID,
                parms: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function getCurrentEntityStatus(entityID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/getCurrentEntityStatus/" + entityID,
                parms: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function updateEntityOverviewStatus(entityTypeId, entityID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/updateEntityOverviewStatus/" + entityTypeId + "/" + entityID,
                parms: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject("An unknown error occurred."));
            }
            return ($q.reject(response.data.message));
        }

        function handleSuccess(response) {
            response.data.StatusCode = response.status;
            return (response.data);
        }
        
    }
    app.service("MetadataService", ['$http', '$q', MetadataService]);
})(angular, app);