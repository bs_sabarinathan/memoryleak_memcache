﻿(function (ng, app) {
    function muiadminattributeCtrl($scope, $location, $resource, $timeout, $http, $cookies, $translate, MetadataService) {
        $scope.myModel = null;
        $scope.myModelId = null;
        var Version = 1;
        var levelid = 1;
        $scope.systemdefinedattributes = [];
        $scope.nonsystemdefinedattribtues = [];
        $scope.optionListSer = {};
        $scope.wizard = {
            attributeType: 0,
            AttributeId: 0,
            Description: '',
            Caption: '',
            newOption: '',
            EditOption: '',
            OptionID: 0,
            TreeLevelNumberByNode: [],
            TreeLevels: [],
            IsDisableAttributeType: false,
            TotalPopulartags: 5,
            TagwordsSearch: ''
        };
        if ($scope.CurrentMetadataVersionInfo == 0)
            $scope.IsOlderMetadataVersion.IsOlderVersion = false;
        else
            $scope.IsOlderMetadataVersion.IsOlderVersion = true;
        $scope.optionsLists = [];
        $scope.SequenceObject = {
            Id: 0,
            AttributeID: 0,
            StartWithValue: 0,
            Pattern: "",
            CurrentValue: 0
        };
        $scope.TagOptionsLists = [];
        $scope.TagOptionsListsForSearch = [];
        $scope.AttrOptionSteps = ['Entry', 'Option'];
        $scope.AttrTagOptionSteps = ['Entry', 'TagOption'];
        $scope.AttrSequenceSteps = ['Entry', 'Sequence'];
        $scope.AttrTreeSteps = ['Entry', 'Tree'];
        $scope.AttrOverviewStatusSteps = ['Entry', 'Overviewstatus'];
        $scope.AttrTreeLevelSteps = ['Entry', 'Tree', 'TreeLevel'];
        $scope.AttrSubmitSteps = ['Entry'];
        $scope.step = 0;
        $scope.isCurrentStep = function (step) {
            return $scope.step === step;
        };
        $scope.setCurrentStep = function (step) {
            $scope.step = step;
        };
        $scope.getCurrentStep = function () {
            if ($scope.wizard.attributeType > 0) {
                if ($scope.wizard.attributeType === "3" || $scope.wizard.attributeType === "4") {
                    $scope.steps = $scope.AttrOptionSteps;
                    return $scope.AttrOptionSteps[$scope.step];
                } else if ($scope.wizard.attributeType === "7") {
                    $scope.steps = $scope.AttrTreeSteps;
                    return $scope.AttrTreeSteps[$scope.step];
                } else if ($scope.wizard.attributeType === "6" || $scope.wizard.attributeType === "12" || $scope.wizard.attributeType === "13") {
                    $scope.steps = $scope.AttrTreeLevelSteps;
                    return $scope.AttrTreeLevelSteps[$scope.step];
                } else if ($scope.wizard.attributeType === "15") {
                    $scope.steps = $scope.AttrOverviewStatusSteps;
                    return $scope.AttrOverviewStatusSteps[$scope.step];
                } else if ($scope.wizard.attributeType === "17") {
                    $scope.steps = $scope.AttrTagOptionSteps;
                    return $scope.AttrTagOptionSteps[$scope.step];
                } else if ($scope.wizard.attributeType === "18") {
                    $scope.steps = $scope.AttrSequenceSteps;
                    return $scope.AttrSequenceSteps[$scope.step];
                } else {
                    $scope.steps = $scope.AttrSubmitSteps;
                    return $scope.AttrSubmitSteps[$scope.step];
                }
            } else {
                $scope.steps = $scope.AttrSubmitSteps;
                return $scope.AttrSubmitSteps[$scope.step];
            }
        };
        $scope.isFirstStep = function () {
            return $scope.step === 0;
        };
        $scope.isLastStep = function () {
            return $scope.step === ($scope.steps.length - 1);
        };
        $scope.visibleLastStep = true;
        $scope.getNextLabel = function () {
            $scope.visibleLastStep = true;
            if ($scope.IsOlderMetadataVersion.IsOlderVersion == false && $scope.isLastStep())
                $scope.visibleLastStep = false;
            return ($scope.isLastStep()) ? 'Submit' : 'Next';
        };
        $scope.handlePrevious = function () {
            $scope.step -= ($scope.isFirstStep()) ? 0 : 1;
        };
        MetadataService.GetAttributetype().then(function (res) {
            $scope.listdata = res.Response;
            $scope.attributeTypes = $scope.listdata;
        });
        MetadataService.GetAttribute().then(function (res) {
            $scope.attributes = res.Response;
            $scope.systemdefinedattributes = $.grep($scope.attributes, function (e) {
                return e.IsSystemDefined == true
            });
            $scope.nonsystemdefinedattribtues = $.grep($scope.attributes, function (e) {
                return e.IsSystemDefined == false
            });
        });
        $scope.add_child = function (parent_node, Caption) {
            var child_node = {
                Caption: Caption,
                Level: parent_node.Level + 1,
                ID: 0,
                IsDeleted: false,
                Children: []
            };
            if (parent_node.Caption != 'Root') {
                child_node.Level = parent_node.Level + 1;
            }
            if (parent_node.Children) {
                parent_node.Children.push(child_node);
                var newLevel = $.inArray(child_node.Level, $scope.wizard.TreeLevelNumberByNode);
                $scope.wizard.TreeLevelNumberByNode.push(child_node.Level);
                if (newLevel == -1) {
                    var level = {
                        Level: child_node.Level,
                        Caption: '',
                        IsPercentage: false,
                        ID: 0
                    }
                    $scope.wizard.TreeLevels.push(level);
                }
            } else {
                parent_node.Children = [child_node];
            }
        };
        $scope.delete_child = function (node, parent_node) {
            var level = node.Level;
            data.IsDeleted = true;
            $scope.wizard.TreeLevelNumberByNode.splice($.inArray(level, $scope.wizard.TreeLevelNumberByNode), 1);
            if ($.inArray(level, $scope.wizard.TreeLevelNumberByNode) == -1) {
                $scope.wizard.TreeLevels.splice($.inArray(level, $scope.wizard.TreeLevels), 1);
            }
        };
        $scope.deletetreeOptionsNewChild = function (data, ParentTree) {
            if (data.id == 0) {
                var total_childrens = data.Children;
                var level = data.Level;
                data.IsDeleted = true;
                $scope.wizard.TreeLevelNumberByNode.splice($.inArray(level, $scope.wizard.TreeLevelNumberByNode), 1);
                if ($.inArray(level, $scope.wizard.TreeLevelNumberByNode) == -1) {
                    var removableLevels = $.grep($scope.wizard.TreeLevels, function (e) {
                        return e.Level == level;
                    });
                    for (var r = 0, obj; obj = removableLevels[r++];) {
                        $scope.wizard.TreeLevels.splice($.inArray(obj, $scope.wizard.TreeLevels), 1);
                    }
                }
                if (data.Children.length > 0) {
                    RemovalofNewChild(total_childrens, data);
                }
            } else {
                if (data.Children.length > 0) {
                    for (var i = 0; i <= data.Children.length - 1; i++) {
                        if (data.Children[i].id == 0) {
                            var level = data.Children[i].Level;
                            data.Children[i].IsDeleted = true;
                            $scope.wizard.TreeLevelNumberByNode.splice($.inArray(level, $scope.wizard.TreeLevelNumberByNode), 1);
                            if ($.inArray(level, $scope.wizard.TreeLevelNumberByNode) == -1) {
                                var removableLevels = $.grep($scope.wizard.TreeLevels, function (e) {
                                    return e.Level == level;
                                });
                                for (var r = 0, obj; obj = removableLevels[r++];) {
                                    $scope.wizard.TreeLevels.splice($.inArray(obj, $scope.wizard.TreeLevels), 1);
                                }
                            }
                        } else if (data.Children[i].IsDeleted == false) {
                            var level = data.Children[i].Level;
                            data.Children[i].IsDeleted = true;
                            $scope.wizard.TreeLevelNumberByNode.splice($.inArray(level, $scope.wizard.TreeLevelNumberByNode), 1);
                            if ($.inArray(level, $scope.wizard.TreeLevelNumberByNode) == -1) {
                                var removableLevels = $.grep($scope.wizard.TreeLevels, function (e) {
                                    return e.Level == level;
                                });
                                for (var r = 0, obj; obj = removableLevels[r++];) {
                                    $scope.wizard.TreeLevels.splice($.inArray(obj, $scope.wizard.TreeLevels), 1);
                                }
                            }
                        }
                        RemovalofNewChild(data.Children[i].Children, data.Children[i]);
                    }
                    var level = data.Level;
                    data.IsDeleted = true;
                    $scope.wizard.TreeLevelNumberByNode.splice($.inArray(level, $scope.wizard.TreeLevelNumberByNode), 1);
                    if ($.inArray(level, $scope.wizard.TreeLevelNumberByNode) == -1) {
                        var removableLevels = $.grep($scope.wizard.TreeLevels, function (e) {
                            return e.Level == level;
                        });
                        for (var r = 0, obj; obj = removableLevels[r++];) {
                            $scope.wizard.TreeLevels.splice($.inArray(obj, $scope.wizard.TreeLevels), 1);
                        }
                    }
                } else {
                    var level = data.Level;
                    data.IsDeleted = true;
                    $scope.wizard.TreeLevelNumberByNode.splice($.inArray(level, $scope.wizard.TreeLevelNumberByNode), 1);
                    if ($.inArray(level, $scope.wizard.TreeLevelNumberByNode) == -1) {
                        var removableLevels = $.grep($scope.wizard.TreeLevels, function (e) {
                            return e.Level == level;
                        });
                        for (var r = 0, obj; obj = removableLevels[r++];) {
                            $scope.wizard.TreeLevels.splice($.inArray(obj, $scope.wizard.TreeLevels), 1);
                        }
                    }
                }
            }
        };

        function RemovalofNewChild(childrens, parent) {
            if (childrens != undefined) {
                if (childrens.length > 0) {
                    for (var z = 0, child; child = childrens[z++];) {
                        if (child.Children.length > 0) {
                            for (var x = 0, grandchild; grandchild = child.Children[x++];) {
                                var level = grandchild.Level;
                                grandchild.IsDeleted = true;
                                $scope.wizard.TreeLevelNumberByNode.splice($.inArray(level, $scope.wizard.TreeLevelNumberByNode), 1);
                                if ($.inArray(level, $scope.wizard.TreeLevelNumberByNode) == -1) {
                                    var removableLevels = $.grep($scope.wizard.TreeLevels, function (e) {
                                        return e.Level == level;
                                    });
                                    for (var r = 0, obj; obj = removableLevels[r++];) {
                                        $scope.wizard.TreeLevels.splice($.inArray(obj, $scope.wizard.TreeLevels), 1);
                                    }
                                }
                                RemovalofNewChild(grandchild.Children, grandchild);
                            }
                        }
                        var level = child.Level;
                        child.IsDeleted = true;
                        $scope.wizard.TreeLevelNumberByNode.splice($.inArray(level, $scope.wizard.TreeLevelNumberByNode), 1);
                        if ($.inArray(level, $scope.wizard.TreeLevelNumberByNode) == -1) {
                            var removableLevels = $.grep($scope.wizard.TreeLevels, function (e) {
                                return e.Level == level;
                            });
                            for (var r = 0, obj; obj = removableLevels[r++];) {
                                $scope.wizard.TreeLevels.splice($.inArray(obj, $scope.wizard.TreeLevels), 1);
                            }
                        }
                    }
                }
            }
        }
        $scope.AddtreeOptionsNewChild = function (data) {
            var post = data.Children.length + 1;
            var newName = data.Caption + '-' + post;
            newName = "";
            var newColorCode = data.ColorCode;
            newColorCode = "";
            var child_node = {
                Caption: newName,
                Level: data.Level + 1,
                id: 0,
                IsDeleted: false,
                Children: [],
                "ColorCode": newColorCode
            };
            if (data.Caption != 'Root') {
                child_node.Level = data.Level + 1;
            }
            if (data.Children) {
                data.Children.push(child_node);
                var newLevel = $.inArray(child_node.Level, $scope.wizard.TreeLevelNumberByNode);
                $scope.wizard.TreeLevelNumberByNode.push(child_node.Level);
                if (newLevel == -1) {
                    var level = {
                        Level: child_node.Level,
                        Caption: '',
                        IsPercentage: false,
                        ID: 0
                    }
                    $scope.wizard.TreeLevels.push(level);
                }
            } else {
                data.Children = [child_node];
            }
        };
        $scope.tree = [{
            Caption: "Root",
            Level: 0,
            id: 0,
            IsDeleted: false,
            Children: [],
            "ColorCode": ""
        }];

        function addtreesvaluesinlist(ParentID, items) {
            angular.forEach(items.Children, function (childItem) {
                var AddChildTree = {};
                AddChildTree.NodeID = 1;
                AddChildTree.ParentNodeID = ParentID;
                AddChildTree.Level = 0;
                AddChildTree.KEY = "";
                AddChildTree.AttributeID = $scope.status;
                AddChildTree.Caption = childItem.Caption;
                AddChildTree.ID = 0;
                AddChildTree.SortOrder = 1;
                MetadataService.InsertTreeNode(AddChildTree).then(function (res) {
                    if (res.StatusCode == 405) { } else { }
                    var chParentID = res.Response;
                    levelid = levelid + 1;
                    if (childItem.Children.length > 0) {
                        addtreesvaluesinlist(chParentID, childItem)
                    }
                });
            });
        };
        $scope.count = 1;
        $scope.addOptions = function () {
            if ($scope.wizard.newOption == '') {
                bootbox.alert($translate.instant('LanguageContents.Res_1809.Caption'));
                return true;
            }
            if ($scope.wizard.OptionID != 0) {
                ($.grep($scope.optionsLists, function (e) {
                    return e.ID == $scope.wizard.AttributeId;
                }))[0].Caption = $scope.wizard.newOption;
            } else {
                $scope.optionsLists.push({
                    "PId": $scope.count,
                    "ID": 0,
                    "Caption": $scope.wizard.newOption,
                    "SortOrder": ($scope.optionsLists.length + 1)
                });
            }
            $scope.wizard.newOption = '';
            $scope.count = $scope.count + 1;
        };
        $scope.EditOptions = function () {
            if ($scope.wizard.OptionID != 0) {
                var UpdateOption = $.grep($scope.optionsLists, function (e) {
                    return e.PId == $scope.wizard.PrimaryID;
                });
                UpdateOption[0].Caption = $scope.wizard.newOption;
            } else if ($scope.wizard.OptionID == 0) {
                var UpdateOption = $.grep($scope.optionsLists, function (e) {
                    return e.PId == $scope.wizard.PrimaryID;
                });
                UpdateOption[0].Caption = $scope.wizard.newOption;
            }
            $scope.wizard.EditOption = '';
            $scope.wizard.newOption = '';
            $scope.wizard.OptionID = 0;
            $scope.EnableOptionUpdate = false;
            $scope.EnableOptionAdd = true;
        };
        $scope.addTagOptions = function () {
            if ($scope.wizard.newOption == '') {
                bootbox.alert($translate.instant('LanguageContents.Res_1809.Caption'));
                return true;
            }
            if ($scope.wizard.OptionID == 0) {
                var checkTagOption = $.grep($scope.TagOptionsLists, function (e) {
                    return e.Caption == $scope.wizard.newOption
                });
                if (checkTagOption.length == 0) {
                    $scope.TagOptionsLists.push({
                        "PId": $scope.count,
                        "ID": 0,
                        "Caption": $scope.wizard.newOption.toLowerCase(),
                        "CreatedBy": $cookies['Username'],
                        "CreatedOn": ConvertDateFromStringToString(ConvertDateToString(new Date())),
                        "SortOrder": ($scope.TagOptionsLists.length + 1)
                    });
                    var newtag = [{
                        "PId": $scope.count,
                        "ID": 0,
                        "Caption": $scope.wizard.newOption.toLowerCase(),
                        "CreatedBy": $cookies['Username'],
                        "CreatedOn": ConvertDateFromStringToString(ConvertDateToString(new Date())),
                        "SortOrder": ($scope.TagOptionsLists.length + 1)
                    }];
                    var addoptions = {};
                    addoptions.TagOptions = newtag;
                    addoptions.AttributeID = 87;
                    addoptions.IsforAdmin = true;
                    addoptions.TotalPopulartags = 5;
                    MetadataService.InsertUpdateTagOption(addoptions).then(function (addoptionsResponse) {
                        if (addoptionsResponse.StatusCode == 200)
                            NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        $scope.optionsLists = [];
                    });
                } else {
                    bootbox.alert($scope.wizard.newOption + ' ' + $translate.instant('LanguageContents.Res_1149.Caption'));
                    return false;
                }
            }
            $scope.wizard.newOption = '';
        };
        $scope.ClearTagOption = function () {
            $scope.wizard.EditOption = '';
            $scope.wizard.newOption = '';
            $scope.wizard.OptionID = 0;
            $scope.EnableOptionUpdate = false;
            $scope.EnableOptionAdd = true;
        }
        $scope.EditTagOptions = function () {
            if ($scope.wizard.OptionID != 0) {
                var UpdateOption = $.grep($scope.TagOptionsLists, function (e) {
                    return e.PId == $scope.wizard.PrimaryID;
                });
                UpdateOption[0].Caption = $scope.wizard.newOption.toLowerCase();
            }
            var AddOptions = {};
            AddOptions.TagOptions = UpdateOption;
            AddOptions.AttributeID = 87;
            AddOptions.IsforAdmin = true;
            AddOptions.TotalPopulartags = 5;
            MetadataService.InsertUpdateTagOption(AddOptions).then(function (res) {
                if (res.StatusCode == 200)
                    NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                $scope.optionsLists = [];
            });
            $scope.wizard.EditOption = '';
            $scope.wizard.newOption = '';
            $scope.wizard.OptionID = 0;
            $scope.EnableOptionUpdate = false;
            $scope.EnableOptionAdd = true;
        };
        $scope.syseditableInPopup = '<a class="iconLink" data-toggle="modal" ng-click="GetAttributesID(row)" ng-show="row.entity.IsSystemDefined==1 && row.entity.Id == 87  ||(row.entity.IsSystemDefined==1 && row.entity.Id == 1)||(row.entity.IsSystemDefined==1 && row.entity.Id == 91)" data-toggle="modal"><i class="icon-edit"></i></a> '
        $scope.editableInPopup = '<a class="iconLink" data-toggle="modal" ng-click="GetAttributesID(row)" ng-show="row.entity.IsSystemDefined==0" data-toggle="modal" data-target="#attributeModal"><i class="icon-edit"></i></a> '
        $scope.delete = '<a class="iconLink" data-toggle="modal" ng-click="deleteAttribute(row)" ng-show="(row.entity.IsSystemDefined==0 && IsOlderMetadataVersion.IsOlderVersion==true)"><i class="icon-remove"></i></a> '
        $scope.editableInsubPopup = '<a class="iconLink" ng-click="GetOptionbyID(row)"><i class="icon-edit"></i></a> '
        $scope.deletesub = '<a class="iconLink" ng-click="deleteOptions(row)" ><i class="icon-remove"></i></a> '
        $scope.GetOptionbyID = function GetOptionbyID(row) {
            $scope.wizard.PrimaryID = row.PId;
            $scope.wizard.OptionID = row.ID;
            $scope.wizard.EditOption = row.Caption;
            $scope.wizard.newOption = row.Caption;
            $scope.EnableOptionUpdate = true;
            $scope.EnableOptionAdd = false;
        };
        $scope.GetTagOptionbyID = function GetTagOptionbyID(row) {
            $scope.wizard.PrimaryID = row.PId;
            $scope.wizard.OptionID = row.ID;
            $scope.wizard.EditOption = row.Caption;
            $scope.wizard.newOption = row.Caption;
            $scope.EnableOptionUpdate = true;
            $scope.EnableOptionAdd = false;
        };
        $scope.GetAttributesID = function GetAttributesID(row) {
            $scope.optionsLists = [];
            $scope.TagOptionsLists = [];
            $scope.wizard.TreeLevels = [];
            $scope.wizard.AttributeId = row.entity.Id;
            $scope.wizard.attributeType = row.entity.AttributeTypeID.toString();
            $scope.wizard.Caption = row.entity.Caption;
            $scope.wizard.Description = row.entity.Description;
            $scope.steps = $scope.AttrSubmitSteps;
            $scope.wizard.IsDisableAttributeType = true;
            $scope.wizard.TreeLevelNumberByNode = [];
            $scope.tree = [];
            $scope.wizard.TreeLevels = [];
            $scope.wizard.TreeLevelNumberByNode = [];
            var ID = row.entity.Id;
            if (ID == 87)
                $("#addEditTagWordsPopup").modal('show');
            else if (ID == 1 || ID == 91)
                $("#attributeModal").modal('show');
            MetadataService.GetAdminOptionListID(ID).then(function (optionlist) {
                $scope.AttrOptionSteps = ['Entry', 'Option'];
                $scope.optionListSer = optionlist.Response;
                $scope.count = 1;
                for (var i = 0; i < $scope.optionListSer.length; i++) {
                    $scope.optionsLists.push({
                        "PId": $scope.count,
                        "ID": $scope.optionListSer[i].Id,
                        "Caption": $scope.optionListSer[i].Caption,
                        "SortOrder": $scope.optionListSer[i].SortOrder
                    });
                    $scope.count = $scope.count + 1;
                }
            });
            MetadataService.GetAdminTreeNode(ID).then(function (GetTree) {
                $scope.tree = [];
                $scope.tree.push(JSON.parse(GetTree.Response));
            });
            $scope.TreeLevelResult = [];
            MetadataService.GetTreeNodeByAttributeID(ID).then(function (GetTreeNodesData) {
                $scope.TreeLevelResult = [];
                $scope.TreeLevelResult = GetTreeNodesData.Response;
                if ($scope.TreeLevelResult != undefined)
                    for (var i = 0; i < $scope.TreeLevelResult.length; i++) {
                        $scope.wizard.TreeLevelNumberByNode.push($scope.TreeLevelResult[i].Level);
                    }
            });
            if ($scope.wizard.attributeType == "6" || $scope.wizard.attributeType == "12" || $scope.wizard.attributeType === "13" || $scope.wizard.attributeType === "15") {
                MetadataService.GetAdminTreelevelByAttributeID(ID).then(function (res) {
                    $scope.AttrTreeLevelSteps = ['Entry', 'Tree', 'TreeLevel'];
                    $scope.levelListSer = res.Response;
                    $scope.count = 1;
                    for (var i = 0; i < $scope.levelListSer.length; i++) {
                        var level = {
                            Level: $scope.levelListSer[i].Level,
                            Caption: $scope.levelListSer[i].LevelName,
                            IsPercentage: $scope.levelListSer[i].IsPercentage,
                            ID: $scope.levelListSer[i].Id
                        }
                        $scope.wizard.TreeLevels.push(level);
                    }
                });
            }
            if ($scope.wizard.attributeType == "18") {
                MetadataService.GetAttributeSequenceByID(ID).then(function (optionlist) {
                    $scope.AttrSequenceSteps = ['Entry', 'Sequence'];
                    $scope.SequenceObject = {
                        Id: optionlist.Response.Id,
                        AttributeID: ID,
                        StartWithValue: optionlist.Response.StartWithValue,
                        Pattern: optionlist.Response.Pattern,
                        CurrentValue: optionlist.Response.CurrentValue
                    };
                });
            }
            if ($scope.wizard.attributeType == "17") {
                $scope.attributeTypes = $scope.listdata;
                MetadataService.GetTagOptionList(true).then(function (tagoptionlist) {
                    $scope.AttrTagOptionSteps = ['Entry', 'TagOption'];
                    if (tagoptionlist.Response != null) {
                        $scope.optionListSer = tagoptionlist.Response;
                        $scope.wizard.TotalPopulartags = tagoptionlist.Response[0].PopularTagsToShow;
                        $scope.count = 1;
                        for (var i = 0; i < $scope.optionListSer.length; i++) {
                            $scope.TagOptionsLists.push({
                                "PId": $scope.count,
                                "ID": $scope.optionListSer[i].Id,
                                "Caption": $scope.optionListSer[i].Caption,
                                "CreatedBy": $scope.optionListSer[i].UserName,
                                "CreatedOn": ConvertDateFromStringToString($scope.optionListSer[i].CreatedOn),
                                "SortOrder": $scope.optionListSer[i].SortOrder
                            });
                            $scope.count = $scope.count + 1;
                        }
                        $scope.TagOptionsListsForSearch = $scope.TagOptionsLists;
                    }
                });
            }
            $scope.step = 0;
            $scope.EnableUpdate = true;
            $scope.EnableAdd = false;
            $scope.EnableOptionUpdate = false;
            $scope.EnableOptionAdd = true;
            $timeout(function () {
                $('#Caption').focus().select();
            }, 1000);
        };
        $scope.deleteAttribute = function deleteAttribute(row) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2050.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        var ID = row.entity.Id;
                        MetadataService.DeleteAttribute(ID).then(function (deleteAttributeById) {
                            if (deleteAttributeById.StatusCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_4289.Caption'));
                            } else {
                                if (deleteAttributeById.Response == 1) {
                                    var index = $scope.nonsystemdefinedattribtues.indexOf(row.entity)
                                    $scope.nonsystemdefinedattribtues.splice(index, 1);
                                    MetadataService.DeleteOptionByAttributeID(ID).then(function (deleteoptionbyAttributeId) {
                                        if (deleteoptionbyAttributeId.StatusCode == 405) {
                                            NotifyError($translate.instant('LanguageContents.Res_4289.Caption'));
                                        } else {
                                            NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption'));
                                        }
                                    });
                                    MetadataService.DeleteSequencByeAttributeID(ID).then(function (deletesequencebyAttributeId) {
                                        if (deletesequencebyAttributeId.StatusCode == 405) {
                                            NotifyError($translate.instant('LanguageContents.Res_4289.Caption'));
                                        }
                                    });
                                } else if (deleteAttributeById.Response == 0) {
                                    bootbox.alert($translate.instant('LanguageContents.Res_1810.Caption'));
                                } else {
                                    NotifyError($translate.instant('LanguageContents.Res_4289.Caption'));
                                }
                            }
                        });
                    }, 100);
                }
            });
        };
        $scope.deleteOptions = function deleteOptions(row) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2050.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        try {
                            if (row.ID != 0) {
                                var ID = row.ID;
                                MetadataService.DeleteOption(ID).then(function (deleteAttributeById) {
                                    if (deleteAttributeById.StatusCode == 405) {
                                        NotifyError($scpe.LanguageContents.Res_4303);
                                    } else {
                                        var index = $scope.optionsLists.indexOf(row)
                                        $scope.optionsLists.splice(index, 1);
                                        NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption'));
                                        $scope.wizard.newOption = '';
                                        $scope.EnableOptionUpdate = false;
                                        $scope.EnableOptionAdd = true;
                                    }
                                });
                            } else {
                                var index = $scope.optionsLists.indexOf(row)
                                $scope.optionsLists.splice(index, 1);
                                NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption'));
                                $scope.wizard.newOption = '';
                                $scope.EnableOptionUpdate = false;
                                $scope.EnableOptionAdd = true;
                            }
                        } catch (e) {
                            NotifyError($translate.instant('LanguageContents.Res_4524.Caption'));
                        }
                    }, 100);
                }
            });
        };
        $scope.deleteTagOptions = function deleteOptions(row) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2050.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        try {
                            if (row.ID != 0) {
                                var ID = row.ID;
                                MetadataService.DeleteTagOption(ID).then(function (deleteAttributeById) {
                                    if (deleteAttributeById.StatusCode == 405) {
                                        NotifyError($scpe.LanguageContents.Res_4303);
                                    } else {
                                        var index = $scope.TagOptionsLists.indexOf(row)
                                        $scope.TagOptionsLists.splice(index, 1);
                                        NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption'));
                                        $scope.wizard.newOption = '';
                                        $scope.EnableOptionUpdate = false;
                                        $scope.EnableOptionAdd = true;
                                    }
                                });
                            } else {
                                var index = $scope.TagOptionsLists.indexOf(row)
                                $scope.TagOptionsLists.splice(index, 1);
                                NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption'));
                                $scope.wizard.newOption = '';
                                $scope.EnableOptionUpdate = false;
                                $scope.EnableOptionAdd = true;
                            }
                        } catch (e) {
                            NotifyError($translate.instant('LanguageContents.Res_4524.Caption'));
                        }
                    }, 100);
                }
            });
        };
        $scope.slno = '<span class="slno">{{row.rowIndex+1}}</span>';
        $scope.filterSystemdefined = {
            filterText: ''
        };
        $scope.filterOthers = {
            filterText: ''
        };
        $scope.gridOptionssystemdefined = {
            data: 'systemdefinedattributes',
            enablePinning: false,
            enableColumnReordering: false,
            filterOptions: $scope.filterSystemdefined,
            columnDefs: [{
                field: "Id",
                displayName: $translate.instant('LanguageContents.Res_69.Caption'),
                width: 40
            }, {
                field: "Caption",
                displayName: $translate.instant('LanguageContents.Res_44.Caption'),
                width: 100
            }, {
                field: "Description",
                displayName: $translate.instant('LanguageContents.Res_22.Caption'),
                width: 150
            }, {
                field: "Type",
                displayName: $translate.instant('LanguageContents.Res_4863.Caption'),
                width: 150
            }, {
                field: "AttributeTypeID",
                displayName: $translate.instant('LanguageContents.Res_5047.Caption'),
                visible: false,
                width: 0
            }, {
                field: "IsSystemDefined",
                displayName: $translate.instant('LanguageContents.Res_2145.Caption'),
                visible: false,
                width: 0
            }, {
                field: "IsSpecial",
                displayName: $translate.instant('LanguageContents.Res_5048.Caption'),
                width: 100
            }, {
                field: "",
                displayName: '',
                cellTemplate: $scope.syseditableInPopup,
                width: 30
            }, {
                field: "",
                displayName: '',
                cellTemplate: $scope.delete,
                width: 40
            }]
        };
        $scope.gridOptionsnonsystemdefined = {
            data: 'nonsystemdefinedattribtues',
            enablePinning: false,
            enableColumnReordering: false,
            filterOptions: $scope.filterOthers,
            columnDefs: [{
                field: "Id",
                displayName: $translate.instant('LanguageContents.Res_69.Caption'),
                width: 60
            }, {
                field: "Caption",
                displayName: $translate.instant('LanguageContents.Res_44.Caption'),
                width: 150
            }, {
                field: "Description",
                displayName: $translate.instant('LanguageContents.Res_22.Caption'),
                width: 150
            }, {
                field: "Type",
                displayName: $translate.instant('LanguageContents.Res_4863.Caption'),
                width: 120
            }, {
                field: "AttributeTypeID",
                displayName: $translate.instant('LanguageContents.Res_5047.Caption'),
                visible: false,
                width: 0
            }, {
                field: "IsSystemDefined",
                displayName: $translate.instant('LanguageContents.Res_2145.Caption'),
                visible: false,
                width: 0
            }, {
                field: "IsSpecial",
                displayName: $translate.instant('LanguageContents.Res_5048.Caption'),
                width: 100
            }, {
                field: "",
                displayName: '',
                cellTemplate: $scope.editableInPopup,
                width: 30
            }, {
                field: "",
                displayName: '',
                cellTemplate: $scope.delete,
                width: 40
            }]
        };
        $scope.update = function () {
            $('#attributeModal').modal('hide');
        };
        $scope.addAttribute = function () {
            $("#attributeModal").modal('show');
            $scope.attributeTypes = $.grep($scope.listdata, function (e) {
                return e.Id != 17
            });
            $scope.wizard.IsDisableAttributeType = false;
            $scope.wizard.Description = "";
            $scope.wizard.Caption = "";
            $scope.EnableAdd = true;
            $scope.wizard.AttributeId = 0;
            $scope.wizard.attributeType = "0";
            $scope.optionsLists = [];
            $scope.EnableUpdate = false;
            $scope.step = 0;
            $scope.EnableOptionUpdate = false;
            $scope.EnableOptionAdd = true;
            $scope.tree = [{
                Caption: "Root",
                Level: 0,
                id: 0,
                IsDeleted: false,
                Children: [],
                "ColorCode": ""
            }];
            $scope.SequenceObject = {
                Id: 0,
                AttributeID: 0,
                StartWithValue: 0,
                Pattern: "",
                CurrentValue: 0
            };
            $scope.wizard.TreeLevels = [];
            $scope.wizard.TreeLevelNumberByNode = [];
            $timeout(function () {
                $('#Caption').focus();
            }, 1000);
        };
        $scope.handleNext = function () {
            if ($scope.isLastStep()) {
                if ($scope.wizard.Caption == '' || $scope.wizard.Caption == undefined || $scope.wizard.Description == '' || $scope.wizard.Description == undefined || $scope.wizard.attributeType == '0') {
                    bootbox.alert($translate.instant('LanguageContents.Res_1811.Caption'));
                    return true;
                }
                if ($scope.wizard.attributeType === "3" || $scope.wizard.attributeType === "4") {
                    if ($scope.optionsLists == "") {
                        bootbox.alert($translate.instant('LanguageContents.Res_1812.Caption'));
                        return true;
                    }
                }
                if ($scope.wizard.attributeType === "17") {
                    if ($scope.TagOptionsLists == "") {
                        bootbox.alert($translate.instant('LanguageContents.Res_1812.Caption'));
                        return true;
                    }
                }
                if ($scope.wizard.attributeType === "6" || $scope.wizard.attributeType === "7" || $scope.wizard.attributeType === "12" || $scope.wizard.attributeType === "13" || $scope.wizard.attributeType === "15") {
                    var IsEmpty = false;
                    $('.TreeNodeEditorRowForm input[type="text"]').each(function () {
                        if ($.trim(this.value) == '') {
                            if ($(this).data('isdeleted') == "false" || $(this).data('isdeleted') == false) {
                                IsEmpty = true;
                            }
                        }
                    });
                    if (IsEmpty) {
                        bootbox.alert($translate.instant('LanguageContents.Res_1813.Caption'));
                        return false;
                    }
                }
                if ($scope.wizard.attributeType === "18") {
                    var IsEmpty = false;
                    if ($scope.SequenceObject.StartWithValue === "" || $scope.SequenceObject.StartWithValue === undefined) {
                        IsEmpty = true;
                    }
                    if ($scope.SequenceObject.Pattern == "" || $scope.SequenceObject.Pattern == undefined) {
                        IsEmpty = true;
                    }
                    if (IsEmpty) {
                        bootbox.alert($translate.instant('LanguageContents.Res_1813.Caption'));
                        return false;
                    }
                }
                IsEmpty = false;
                if ($scope.wizard.attributeType === "6" || $scope.wizard.attributeType === "7" || $scope.wizard.attributeType === "12" || $scope.wizard.attributeType === "15") {
                    IsEmpty = false;
                    $('.TreeLevelEditorRowForm input[type="text"]').each(function () {
                        if ($.trim(this.value) == '') {
                            IsEmpty = true;
                        }
                    });
                    if (IsEmpty) {
                        bootbox.alert($translate.instant('LanguageContents.Res_4590.Caption'));
                        return true;
                    }
                }
                if ($scope.wizard.attributeType === "15") {
                    var isEmptyStatusOptions = true;
                    if ($scope.tree[0].Children.length == 0) {
                        bootbox.alert($translate.instant('LanguageContents.Res_4600.Caption'));
                        return false;
                    }
                    for (var t = 0, tree; tree = $scope.tree[0].Children[t++];) {
                        if (tree.Children.length == 0) {
                            if (isEmptyStatusOptions)
                                isEmptyStatusOptions = false;
                        }
                    }
                    if (!isEmptyStatusOptions) {
                        bootbox.alert($translate.instant('LanguageContents.Res_4600.Caption'));
                        return false;
                    }
                }
                var AddAttribute = {};
                AddAttribute.AttributeTypeID = parseInt($scope.wizard.attributeType);
                var AttributeTypeName = ($.grep($scope.attributeTypes, function (e) {
                    return e.Id == AddAttribute.AttributeTypeID;
                }))[0].ClassName;
                AddAttribute.Caption = $scope.wizard.Caption;
                AddAttribute.Description = $scope.wizard.Description;
                AddAttribute.IsSystemDefined = ($scope.wizard.AttributeId == 1 || ($scope.wizard.AttributeId == 91)) ? 1 : 0;
                AddAttribute.IsSpecial = 0;
                AddAttribute.ID = $scope.wizard.AttributeId;
                MetadataService.Attribute(AddAttribute).then(function (getattribute) {
                    if ($scope.wizard.AttributeId > 0) {
                        $scope.status = $scope.wizard.AttributeId;
                    } else {
                        $scope.status = getattribute.Response;
                        $scope.nonsystemdefinedattribtues.push({
                            "Id": $scope.status,
                            "Type": AttributeTypeName,
                            "Caption": $scope.wizard.Caption,
                            "AttributeTypeID": $scope.wizard.attributeType,
                            "Description": $scope.wizard.Description,
                            "IsSystemDefined": 0,
                            "IsSpecial": "false"
                        });
                    }
                    if ($scope.wizard.attributeType > 0) {
                        if ($scope.wizard.attributeType === "3" || $scope.wizard.attributeType === "4") {
                            var AddOptions = {};
                            AddOptions.Options = $scope.optionsLists;
                            AddOptions.AttributeID = $scope.status;
                            MetadataService.Option(AddOptions).then(function (res) {
                                $scope.optionsLists = [];
                            });
                        } else if ($scope.wizard.attributeType === "7") {
                            var AddTree = {};
                            AddTree.AttributeID = $scope.status;
                            AddTree.Tree = $scope.tree[0];
                            AddTree.TreeLevels = $scope.wizard.TreeLevels;
                            MetadataService.InsertTreeNode(AddTree).then(function (res) {
                                if (res.StatusCode == 405) { } else {
                                    $scope.tree = [{
                                        Caption: "Root",
                                        Level: 0,
                                        id: 0,
                                        IsDeleted: false,
                                        Children: [],
                                        "ColorCode": ""
                                    }];
                                    $scope.wizard.TreeLevels = [];
                                }
                            });
                        } else if ($scope.wizard.attributeType === "18") {
                            $scope.SequenceObject.AttributeID = $scope.status;
                            MetadataService.InsertAttributeSequencePattern($scope.SequenceObject).then(function (savesequence) {
                                if (savesequence.StatusCode == 405) { } else {
                                    $scope.SequenceObject = {
                                        Id: 0,
                                        AttributeID: 0,
                                        StartWithValue: 0,
                                        Pattern: ""
                                    };
                                }
                            });
                        } else if ($scope.wizard.attributeType === "6" || $scope.wizard.attributeType === "12" || $scope.wizard.attributeType === "13" || $scope.wizard.attributeType === "15") {
                            var addTreewithLevel = {};
                            addTreewithLevel.AttributeID = $scope.status;
                            addTreewithLevel.Tree = $scope.tree[0];
                            addTreewithLevel.TreeLevels = $scope.wizard.TreeLevels;
                            MetadataService.InsertTreeNode(addTreewithLevel).then(function (saveTree) {
                                if (saveTree.StatusCode == 405) { } else {
                                    $scope.tree = [{
                                        Caption: "Root",
                                        Level: 0,
                                        id: 0,
                                        IsDeleted: false,
                                        Children: [],
                                        "ColorCode": ""
                                    }];
                                    $scope.wizard.TreeLevels = [];
                                }
                            });
                        } else if ($scope.wizard.attributeType === "17") {
                            var AddOptions = {};
                            AddOptions.TagOptions = $scope.TagOptionsLists;
                            AddOptions.AttributeID = $scope.status;
                            AddOptions.IsforAdmin = true;
                            AddOptions.TotalPopulartags = $scope.wizard.TotalPopulartags;
                            MetadataService.InsertUpdateTagOption(AddOptions).then(function (res) {
                                $scope.optionsLists = [];
                            });
                        } else { }
                        MetadataService.GetAttribute().then(function (res) {
                            $scope.attributes = res.Response;
                            $scope.systemdefinedattributes = $.grep($scope.attributes, function (e) {
                                return e.IsSystemDefined == true
                            });
                            $scope.nonsystemdefinedattribtues = $.grep($scope.attributes, function (e) {
                                return e.IsSystemDefined == false
                            });
                        });
                    }
                    $scope.wizard.AttributeId = 0;
                });
                NotifySuccess($translate.instant('LanguageContents.Res_4116.Caption'));
                $('#attributeModal').modal('hide');
                MetadataService.GetAttribute().then(function (res) {
                    $scope.attributes = res.Response;
                    $scope.systemdefinedattributes = $.grep($scope.attributes, function (e) {
                        return e.IsSystemDefined == true
                    });
                    $scope.nonsystemdefinedattribtues = $.grep($scope.attributes, function (e) {
                        return e.IsSystemDefined == false
                    });
                });
            } else {
                if ($scope.wizard.Caption == '' || $scope.wizard.Description == '' || $scope.wizard.attributeType == '0') {
                    bootbox.alert($translate.instant('LanguageContents.Res_1811.Caption'));
                    return true;
                }
                if ($scope.wizard.attributeType === "6" || $scope.wizard.attributeType === "7" || $scope.wizard.attributeType === "12" || $scope.wizard.attributeType === "15") {
                    var IsEmpty = false;
                    $('.TreeNodeEditorRowForm input[data-isdeleted="false"]').each(function () {
                        if ($.trim(this.value) == "") {
                            IsEmpty = true;
                        }
                    });
                    if (IsEmpty) {
                        bootbox.alert($translate.instant('LanguageContents.Res_1813.Caption'));
                        return false;
                    }
                }
                $scope.step += 1;
            }
        };
        $scope.ExpandCollapseTreeLIClass = function (minimized) {
            if (minimized == true)
                return "display:none;";
            else if (minimized == false)
                return "display:block;";
            else
                return "";
        }
        $scope.alternateColor = function (index) {
            if (index % 2)
                return "FDFDFD";
            else
                return "F3F3F3";
        }
        $scope.ExpandCollapseTreeIconClass = function (minimized) {
            if (minimized == true)
                return "icon-caret-right";
            else if (minimized == false)
                return "icon-caret-down";
            else
                return "icon-caret-down";
        }
        $scope.toggleMinimized = function (child) {
            child.minimized = !child.minimized;
        };
        var timer = 0;
        $("#OntagwordSearch").keyup(function (event) {
            clearTimeout(timer);
            timer = setTimeout(function () {
                $timeout(function () {
                    $scope.TagOptionsLists = $.grep($scope.TagOptionsListsForSearch, function (element) {
                        return element.Caption.toLowerCase().indexOf($scope.wizard.TagwordsSearch.toLowerCase()) != -1;
                    })
                }, 10);
            }, 750);
        });
        $scope.Sortable = {
            start: function (event, ui) {
                var $elem = $('.ui-state-highlight'),
                 drag = ui.item;
                $elem.height(drag.height()).width(drag.width());
            },
            axis: 'y',
            cursor: 'move',
            opacity: 0.7,
            forceHelperSize: true,
            forcePlaceholderSize: true,
            stop: function (event, ui) {
                $scope.Treesortorderupdate(event, ui);
                $scope.$apply();
            },
            placeholder: "ui-state-highlight"
        };
        $scope.Treesortorderupdate = function (event, ui) {
            var root = event.target,
             item = ui.item,
             parent = item.parent(),
             target = item.parent().scope().$parent.data,
             child = item.scope().data,
             index = item.index();
            var remainRecord = $.grep(target.Children, function (e) {
                return e == child;
            });

            function walk(target, child) {
                var Children = target.Children,
                 i;
                if (Children) {
                    i = Children.length;
                    while (i--) {
                        if (Children[i] === child) {
                            return Children.splice(i, 1);
                        } else {
                            walk(Children[i], child);
                        }
                    }
                }
            }
            walk(target, child);
            target.Children.splice(index, 0, child);
        };
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.admin.attributeCtrl']"));
        });
        $scope.colorVal = '#00ff00';
        $scope.ColorOptions = {
            preferredFormat: "hex",
            showInput: true,
            showAlpha: false,
            allowEmpty: true,
            showPalette: true,
            showPaletteOnly: false,
            togglePaletteOnly: true,
            togglePaletteMoreText: 'more',
            togglePaletteLessText: 'less',
            showSelectionPalette: true,
            chooseText: "Choose",
            cancelText: "Cancel",
            showButtons: true,
            palette: [
             ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)", "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
             ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)", "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
             ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)", "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)", "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)", "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)", "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)", "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)", "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)", "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)", "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)", "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
            ]
        };
        $scope.colorchange = function (color) { }
        $scope.sequenceTooltip = function () {
            var html = '';
            html += '<b>{0}</b>: (Required) Sequence number. One or more zeros enclosed in curly braces represent the sequence number itself. The number of zeros in the curly braces dictates the minimum number of digits that will be displayed. If the actual number has fewer digits than this, it will be padded with leading zeros. Maximum is 10 digits.';
            html += '<br/>';
            html += '<br/>';
            html += '<b>{YY}</b>, <b>{YYYY}</b>: (Optional) Year. 2 or 4 "Y" characters enclosed in curly braces represent the year of the record creation date. It can display 2 digits (for example, "04") or all 4 digits (for example, "2004") of the year.';
            html += '<br/>';
            html += '<br/>';
            html += '<b>{MM}</b>: (Optional) Month. 2 "M" characters enclosed in curly braces represent the numeric month (for example, "01" for January, and “02" for February) of the record creation date.';
            html += '<br/>';
            html += '<br/>';
            html += '<b>{DD}</b>: (Optional) Day. 2 "D" characters enclosed in curly braces represent the numeric day of the month (for example, "01" to "31" are valid days in January) of the record creation date.';
            html += '<br/>';
            html += '<br/>';
            html += '<b>{@ST}</b>: (Optional) this is a special substitution variable, it will print the short text for the specific entity type.';
            html += '<br/>';
            html += '<br/>';
            html += '<b>{@TYD}</b>: (Optional) Type ID of the specific entity type.';
            return html;
        }
        $scope.set_color = function (clr) {
            return {
                'background-color': "#" + clr.toString().trim()
            }
        }
    }
    app.controller("mui.admin.attributeCtrl", ['$scope', '$location', '$resource', '$timeout', '$http', '$cookies', '$translate', 'MetadataService', muiadminattributeCtrl]);
})(angular, app);