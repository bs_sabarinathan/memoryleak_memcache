﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    public interface IEntityStatus
    {
        #region Public Properties

        int ID
        {
            get;
            set;

        }

        int EntityID
        {
            get;
            set;

        }

        int Status
        {
            get;
            set;

        }

        int TimeStatus
        {
            get;
            set;

        }

        string Comment
        {
            get;
            set;

        }

        #endregion
    }
}
