using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{

	/// <summary>
	/// EntityColorCode object for table 'PM_EntityColorCode'.
	/// </summary>
	
	internal class EntityColorCode : IEntityColorCode 
	{
		#region Member Variables

		protected int _id;
		protected int _entitytypeid;
		protected int _attributeid;
		protected int _optionid;
		protected string _colorcode;
		
		
		#endregion
		
		#region Constructors
		public EntityColorCode() {}
			
		public EntityColorCode(int pEntityTypeid, int pAttributeid, int pOptionid, string pColorCode)
		{
			this._entitytypeid = pEntityTypeid; 
			this._attributeid = pAttributeid; 
			this._optionid = pOptionid; 
			this._colorcode = pColorCode; 
		}
				
		public EntityColorCode(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}
		
		public int EntityTypeid
		{
			get { return _entitytypeid; }
			set { _entitytypeid = value; }
			
		}
		
		public int Attributeid
		{
			get { return _attributeid; }
			set { _attributeid = value; }
			
		}
		
		public int Optionid
		{
			get { return _optionid; }
			set { _optionid = value; }
			
		}
		
		public string ColorCode
		{
			get { return _colorcode; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("ColorCode", "ColorCode value, cannot contain more than 50 characters");
			  _colorcode = value; 
			}
			
		}
		

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			EntityColorCode castObj = null;
			try
			{
				castObj = (EntityColorCode)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
