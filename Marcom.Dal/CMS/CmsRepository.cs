﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandSystems.Marcom.Dal.Base;
using NHibernate;

namespace BrandSystems.Marcom.Dal.CMS
{
    public class CmsRepository : GenericRepository
    {

        public CmsRepository(ISessionFactory sessionFactory)
            : base(sessionFactory)
        {

        }

    }
}
