using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;

namespace BrandSystems.Marcom.Core.Common
{

	/// <summary>
	/// UserSubscription object for table 'CM_User_Subscription'.
	/// </summary>
	
	internal class UserSubscription : IUserSubscription 
	{
		#region Member Variables

		protected int _id;
		protected int _userid;
		protected int _entityid;
		protected DateTimeOffset _subscribedon;
		protected DateTimeOffset _lastupdatedon;
		protected bool _iscomplex;
		protected bool _ismultilevel;
		protected int _entitytypeid;
        protected int _filterroleoption;
		
		
		#endregion
		
		#region Constructors
		public UserSubscription() {}

        public UserSubscription(int pUserid, int pEntityid, DateTimeOffset pSubscribedOn, DateTimeOffset pLastUpdatedOn, bool pIsComplex, bool pIsMultiLevel, int pEntityTypeid, int pFilterRoleOption)
		{
			this._userid = pUserid; 
			this._entityid = pEntityid; 
			this._subscribedon = pSubscribedOn; 
			this._lastupdatedon = pLastUpdatedOn; 
			this._iscomplex = pIsComplex; 
			this._ismultilevel = pIsMultiLevel; 
			this._entitytypeid = pEntityTypeid;
            this._filterroleoption = pFilterRoleOption;
		}
				
		public UserSubscription(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}
		
		public int Userid
		{
			get { return _userid; }
			set { _userid = value; }
			
		}
		
		public int Entityid
		{
			get { return _entityid; }
			set { _entityid = value; }
			
		}
		
		public DateTimeOffset SubscribedOn
		{
			get { return _subscribedon; }
			set { _subscribedon = value; }
			
		}
		
		public DateTimeOffset LastUpdatedOn
		{
			get { return _lastupdatedon; }
			set { _lastupdatedon = value; }
			
		}
		
		public bool IsComplex
		{
			get { return _iscomplex; }
			set { _iscomplex = value; }
			
		}
		
		public bool IsMultiLevel
		{
			get { return _ismultilevel; }
			set { _ismultilevel = value; }
			
		}
		
		public int EntityTypeid
		{
			get { return _entitytypeid; }
			set { _entitytypeid = value; }
			
		}
        public virtual int FilterRoleOption
        {
            get { return _filterroleoption; }
            set {  _filterroleoption = value; }

        }

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			UserSubscription castObj = null;
			try
			{
				castObj = (UserSubscription)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
