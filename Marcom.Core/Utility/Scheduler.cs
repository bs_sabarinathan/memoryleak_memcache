﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Threading;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.Interface;
using Mail;
using BrandSystems.Marcom.Core.Utility;


/// <summary>
/// This is the manager class that handles running the email operation on a background thread
/// </summary>
public class Scheduler : IDisposable
{


    private bool _Cancelled = false;
    /// <summary>
    /// Determines the status fo the Scheduler
    /// </summary>        
    /// 
    public bool Cancelled
    {
        get { return _Cancelled; }
        set { _Cancelled = value; }
    }

    private Dictionary<string, string> ApplicationSettings = new Dictionary<string, string>();
    public Scheduler(Dictionary<string, string> AppSettings)
        : base()
    {

        ApplicationSettings = AppSettings;
        MailServer ms = new MailServer();
        ms.ApplicationSettings = AppSettings;
    }

    /// <summary>
    /// The frequency of checks for mails present in database are 
    /// performed in Seconds.
    /// </summary>

    private int CheckFrequency = 180;

    private AutoResetEvent WaitHandle = new AutoResetEvent(false);

    private object SyncLock = new Object();

    public Scheduler()
    {
    }

    /// <summary>
    /// Starts the background thread processing       
    /// </summary>
    /// <param name="CheckFrequency">Frequency that checks are performed in seconds</param>
    public void Start(int checkFrequency, int TenantID)
    {
        // *** Ensure that any waiting instances are shut down
        //this.WaitHandle.Set();

        this.CheckFrequency = checkFrequency;
        this.Cancelled = false;

        var t = new Thread(
                        () => Run(TenantID));

        //Thread t = new Thread();
        t.Start();
    }

    /// <summary>
    /// Causes the processing to stop. If the operation is still
    /// active it will stop after the current email processing
    /// completes
    /// </summary>
    public void Stop()
    {
        lock (this.SyncLock)
        {
            if (Cancelled)
            {
                return;
            }

            this.Cancelled = true;
            this.WaitHandle.Set();
        }
    }

    /// <summary>
    /// Runs the actual processing loop by checking the mail box
    /// </summary>

    private void Run(int TenantID)
    {
        // *** Start out  waiting
        this.WaitHandle.WaitOne(this.CheckFrequency * 1000, true);


        while (!Cancelled)
        {
            //if (MarcomManagerFactory._systemSessionId == Guid.Empty)
            //{
                MarcomManagerFactory.EnsureInitialized();
                // MarcomManagerFactory.Reinitialize();

            //}
                FeedNotificationServer fns = new FeedNotificationServer(TenantID);

            fns.ScheduledMailcheck(TenantID);
            // *** Http Ping to force the server to stay alive 
            this.PingServer();
            // *** Put in 

            this.WaitHandle.WaitOne(this.CheckFrequency * 1000, true);
            //BusinessLogicLayer BLL = new BusinessLogicLayer();

            //List<int> UserIDs = new List<int>();

            //UserIDs = BLL.GetUserIDsForMailSubscription();


            //if (UserIDs != null)
            //{
            //    //Need to chnage the design
            //    foreach (int UserID in UserIDs)
            //    {
                  
            //        List<BrandSystems.MarcomPlatform.CommonComponents.BusinessEntities.MarcomPlatform.Notification> Notifications = new List<BrandSystems.MarcomPlatform.CommonComponents.BusinessEntities.MarcomPlatform.Notification>();
            //        Notifications = BLL.GetUserNotificationForMail(UserID);


            //        BrandSystems.MarcomPlatform.CommonComponents.BusinessEntities.MarcomPlatform.MailSubscription UserMailSubscription = new BrandSystems.MarcomPlatform.CommonComponents.BusinessEntities.MarcomPlatform.MailSubscription();
            //        UserMailSubscription = BLL.GetMailSubscriptionSettingsByUserID(UserID);

            //        string MailType = "";

            //        if (UserMailSubscription.DayName == "Daily")
            //        {
            //            MailType = "Daily";
            //        }
            //        else
            //        {
            //            MailType = "Weekly";
            //        }

            //        DateTime CurrentDateTime = DateAndTime.Now();
            //        List<string> NotificationIDs = new List<string>();

            //        StringBuilder Body = new StringBuilder();

            //        Body.Append("<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"border-collapse: collapse;");
            //        Body.Append("    width: 98%\">");
            //        Body.Append("    <tbody>");
            //        Body.Append("        <tr>");
            //        Body.Append("            <td style=\"font-size: 12px; font-family: 'lucida grande',tahoma,verdana,arial,sans-serif\">");
            //        Body.Append("                <table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse: collapse; width: 620px\">");
            //        Body.Append("                    <tbody>");
            //        Body.Append("                        <tr>");
            //        Body.Append("                            <td style=\"font-size: 16px; font-family: 'lucida grande',tahoma,verdana,arial,sans-serif;");
            //        Body.Append("                                background: #fff; color: #ffffff; font-weight: bold; vertical-align: baseline;");
            //        Body.Append("                                letter-spacing: 0; text-align: left; padding: 10px 0 15px\">");
            //        Body.Append("                                <img src=\"" + ApplicationSettings["LogoImagePath"] + "\" />");
            //        Body.Append("                            </td>");
            //        Body.Append("                        </tr>");
            //        Body.Append("                    </tbody>");
            //        Body.Append("                </table>");
            //        Body.Append("                <table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse: collapse; width: 620px\">");
            //        Body.Append("                    <tbody>");
            //        Body.Append("                        <tr>");
            //        Body.Append("                            <td style=\"font-size: 16px; font-family: 'lucida grande',tahoma,verdana,arial,sans-serif;");
            //        Body.Append("                                background: #A5A5A5; color: #ffffff; font-weight: bold; vertical-align: baseline;");
            //        Body.Append("                                letter-spacing: 0; text-align: left; padding: 5px 20px; border-top: 2px solid #E4E4E4; border-bottom:2px solid #E4E4E4;\">");
            //        Body.Append("                                <span style=\"background: #A5A5A5; color: #fff; font-weight: bold; font-family: 'lucida grande',tahoma,verdana,arial,sans-serif;");
            //        Body.Append("                                    vertical-align: middle; font-size: 13px; letter-spacing: 0; text-align: left;");
            //        Body.Append("                                    vertical-align: baseline\">Recap Report " + MailType + "</span>");
            //        Body.Append("                            </td>");
            //        Body.Append("                        </tr>");
            //        Body.Append("                    </tbody>");
            //        Body.Append("                </table>");
            //        Body.Append("                <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"620px\" style=\"border-collapse: collapse;");
            //        Body.Append("                    width: 620px\">");
            //        Body.Append("                    <tbody>");
            //        Body.Append("                        <tr>");
            //        Body.Append("                            <td style=\"padding: 0px; background-color: #f2f2f2; border-left: none; border-right: none;");
            //        Body.Append("                                border-top: none; border-bottom: none\">");
            //        Body.Append("                                <table cellspacing=\"0\" cellpadding=\"0\" width=\"620px\" style=\"border-collapse: collapse\">");
            //        Body.Append("                                    <tbody>");
            //        Body.Append("                                        <tr>");
            //        Body.Append("                                            <td style=\"font-size: 11px; font-family: 'lucida grande',tahoma,verdana,arial,sans-serif;");
            //        Body.Append("                                                padding: 0px; width: 620px\">");
            //        Body.Append("                                                <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"border-collapse: collapse;");
            //        Body.Append("                                                    width: 100%\">");
            //        Body.Append("                                                    <tbody>");
            //        Body.Append("                                                        <tr>");
            //        Body.Append("                                                            <td style=\"padding: 20px; background-color: #fff; border-left: none; border-right: none;");
            //        Body.Append("                                                                border-top: none; border-bottom: none\">");
            //        Body.Append("                                                                <table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse: collapse\">");
            //        Body.Append("                                                                    <tbody>");
            //        Body.Append("                                                                        <tr>");
            //        Body.Append("                                                                            <td valign=\"top\" style=\"font-size: 12px; font-family: 'lucida grande',tahoma,verdana,arial,sans-serif;");
            //        Body.Append("                                                                                width: 100%; text-align: left\">");
            //        Body.Append("                                                                                <table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse: collapse; width: 100%\">");
            //        Body.Append("                                                                                    <tbody>");
            //        Body.Append("                                                                                        <tr>");
            //        Body.Append("                                                                                            <td style=\"font-size: 13px; font-family: 'lucida grande',tahoma,verdana,arial,sans-serif;");
            //        Body.Append("                                                                                                padding-top: 5px; padding-bottom: 5px\">");
            //        Body.Append("                                                                                                <table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse: collapse; width: 100%\">");
            //        Body.Append("                                                                                                    <tbody>");

            //        //For index = 0 To Notifications.Count - 1

            //        //    Dim Item As BrandSystems.MarcomPlatform.CommonComponents.BusinessEntities.MarcomPlatform.Notification = Notifications(index)


            //        //Next

            //        //For Each Item As BrandSystems.MarcomPlatform.CommonComponents.BusinessEntities.MarcomPlatform.Notification In Notifications

            //        for (index = 0; index <= Notifications.Count - 1; index++)
            //        {
            //            BrandSystems.MarcomPlatform.CommonComponents.BusinessEntities.MarcomPlatform.Notification Item = Notifications[index];

            //            string AdditionalStyle = "border-bottom: 1px solid #DDDDDD;";

            //            if (index == Notifications.Count - 1)
            //            {
            //                AdditionalStyle = "";
            //            }

            //            Body.Append("                                                                                                         <tr>");
            //            Body.Append("                                                                                                            <td  style=\"vertical-align: top; padding-bottom: 5px; padding-top: 5px; " + AdditionalStyle + "\">");
            //            Body.Append("                                                                                                                <a style=\"border: 1px solid #DCDCDC; display: inline-block; height: 58px; vertical-align: top;");
            //            Body.Append("                                                                                                                    width: 50px;\">");
            //            Body.Append("                                                                                                                    <img width=\"50\" height=\"58\" src=\"" + ApplicationSettings["ApplicationURL"] + "/" + Item.ActorImageUrl + "\" style=\"width: 50px; height: 58px;\"></a>");
            //            Body.Append("                                                                                                            </td>");
            //            Body.Append("                                                                                                            <td style=\"vertical-align: top;  padding: 5px; " + AdditionalStyle + "\">");


            //            switch (Item.NotificationID)
            //            {
            //                case 0:
            //                    Body.Append("<img  src=\"" + ApplicationSettings["ApplicationURL"] + "/App_Themes/Default/image/comments.gif\" />");
            //                    break;
            //                case 1:
            //                    Body.Append("<img  src=\"" + ApplicationSettings["ApplicationURL"] + "/App_Themes/Default/image/Created.png\" />");
            //                    break;
            //                case 2:
            //                    Body.Append("<img  src=\"" + ApplicationSettings["ApplicationURL"] + "/App_Themes/Default/image/Updated.jpg\" />");
            //                    break;
            //                case 3:
            //                    Body.Append("<img  src=\"" + ApplicationSettings["ApplicationURL"] + "/App_Themes/Default/image/Delete.png\" />");
            //                    break;
            //                case 4:
            //                    Body.Append("<img  src=\"" + ApplicationSettings["ApplicationURL"] + "/App_Themes/Default/image/Created.png\" />");
            //                    break;
            //                case 5:
            //                    Body.Append("<img  src=\"" + ApplicationSettings["ApplicationURL"] + "/App_Themes/Default/image/Updated.jpg\" />");
            //                    break;
            //                case 6:
            //                    Body.Append("<img  src=\"" + ApplicationSettings["ApplicationURL"] + "/App_Themes/Default/image/Created.png\" />");
            //                    break;
            //                case 7:
            //                    Body.Append("<img  src=\"" + ApplicationSettings["ApplicationURL"] + "/App_Themes/Default/image/Change-status.jpg\" />");
            //                    break;
            //                case 8:
            //                    Body.Append("<img  src=\"" + ApplicationSettings["ApplicationURL"] + "/App_Themes/Default/image/Created.png\" />");
            //                    break;
            //                case 9:
            //                    Body.Append("<img  src=\"" + ApplicationSettings["ApplicationURL"] + "/App_Themes/Default/image/Updated.jpg\" />");
            //                    break;
            //                case 10:
            //                    Body.Append("<img  src=\"" + ApplicationSettings["ApplicationURL"] + "/App_Themes/Default/image/Deleted.png\" />");
            //                    break;
            //                case 11:
            //                    Body.Append("<img  src=\"" + ApplicationSettings["ApplicationURL"] + "/App_Themes/Default/image/Change-status.jpg\" />");
            //                    break;
            //                case 12:
            //                    Body.Append("<img  src=\"" + ApplicationSettings["ApplicationURL"] + "/App_Themes/Default/image/attachement.png\" />");
            //                    break;
            //                case 13:
            //                    Body.Append("<img  src=\"" + ApplicationSettings["ApplicationURL"] + "/App_Themes/Default/image/attachement.png\" />");
            //                    break;
            //                case 14:
            //                    Body.Append("<img  src=\"" + ApplicationSettings["ApplicationURL"] + "/App_Themes/Default/image/editor.png\" />");
            //                    break;
            //                case 15:
            //                    Body.Append("<img  src=\"" + ApplicationSettings["ApplicationURL"] + "/App_Themes/Default/image/editor.png\" />");
            //                    break;
            //                case 16:
            //                    Body.Append("<img  src=\"" + ApplicationSettings["ApplicationURL"] + "/App_Themes/Default/image/Delete.png\" />");
            //                    break;
            //                case 17:
            //                    Body.Append("<img  src=\"" + ApplicationSettings["ApplicationURL"] + "/App_Themes/Default/image/comments.gif\" />");
            //                    break;
            //                case 18:
            //                    Body.Append("<img  src=\"" + ApplicationSettings["ApplicationURL"] + "/App_Themes/Default/image/Duplicate.jpg\" />");
            //                    break;
            //                case 19:
            //                    Body.Append("<img  src=\"" + ApplicationSettings["ApplicationURL"] + "/App_Themes/Default/image/Created.png\" />");
            //                    break;
            //                case 20:
            //                    Body.Append("<img  src=\"" + ApplicationSettings["ApplicationURL"] + "/App_Themes/Default/image/FinancialUpdate.jpg\" />");
            //                    break;
            //                case 21:
            //                    Body.Append("<img  src=\"" + ApplicationSettings["ApplicationURL"] + "/App_Themes/Default/image/FinancialUpdate.jpg\" />");
            //                    break;
            //                case 22:
            //                    Body.Append("<img  src=\"" + ApplicationSettings["ApplicationURL"] + "/App_Themes/Default/image/Updated.jpg\" />");
            //                    break;
            //                case 23:
            //                    Body.Append("<img  src=\"" + ApplicationSettings["ApplicationURL"] + "/App_Themes/Default/image/FinancialUpdate.jpg\" />");
            //                    break;
            //                case 24:
            //                    Body.Append("<img  src=\"" + ApplicationSettings["ApplicationURL"] + "/App_Themes/Default/image/FinancialUpdate.jpg\" />");
            //                    break;
            //                case 25:
            //                    Body.Append("<img  src=\"" + ApplicationSettings["ApplicationURL"] + "/App_Themes/Default/image/FinancialUpdate.jpg\" />");
            //                    break;
            //                case 26:
            //                    Body.Append("<img  src=\"" + ApplicationSettings["ApplicationURL"] + "/App_Themes/Default/image/FinancialUpdate.jpg\" />");
            //                    break;
            //                case 27:
            //                    Body.Append("<img  src=\"" + ApplicationSettings["ApplicationURL"] + "/App_Themes/Default/image/FinancialUpdate.jpg\" />");
            //                    break;
            //                case 28:
            //                    Body.Append("<img  src=\"" + ApplicationSettings["ApplicationURL"] + "/App_Themes/Default/image/FinancialUpdate.jpg\" />");
            //                    break;
            //                case 29:
            //                    Body.Append("<img  src=\"" + ApplicationSettings["ApplicationURL"] + "/App_Themes/Default/image/FinancialUpdate.jpg\" />");
            //                    break;
            //                case 30:
            //                    Body.Append("<img  src=\"" + ApplicationSettings["ApplicationURL"] + "/App_Themes/Default/image/Change-status.jpg\" />");
            //                    break;
            //                case 31:
            //                    Body.Append("<img  src=\"" + ApplicationSettings["ApplicationURL"] + "/App_Themes/Default/image/Created.png\" />");
            //                    break;
            //                case 32:
            //                    Body.Append("<img  src=\"" + ApplicationSettings["ApplicationURL"] + "/App_Themes/Default/image/Deleted.png\" />");
            //                    break;
            //            }

            //            Body.Append("                                                                                                            </td>");
            //            Body.Append("                                                                                                            <td  style=\"vertical-align: top; padding-bottom: 5px; padding-top: 5px; " + AdditionalStyle + "\">");
            //            Body.Append("                                                                                                                <div style=\"display: inline-block; font-family: verdana; font-size: 11px; line-height: 13px;");
            //            Body.Append("                                                                                                                    width: 500px; vertical-align: top; font-weight: bold;\">");
            //            Body.Append(Item.NotificationText);
            //            Body.Append("                                                                                                                    <span style=\"color: #999999; cursor: default; font-size: 10px; font-weight: normal;\">");
            //            Body.Append("                                                                                                                        " + System.DateTime.Parse(Item.CreatedOn).ToString("yyyy-MM-dd HH:mm") + "</span></div>");
            //            Body.Append("                                                                                                            </td>");
            //            Body.Append("                                                                                                        </tr>");

            //            NotificationIDs.Add(Item.NotificationID.ToString());

            //        }

            //        Body.Append("                                                                                                    </tbody>");
            //        Body.Append("                                                                                                </table>");
            //        Body.Append("                                                                                            </td>");
            //        Body.Append("                                                                                        </tr>");
            //        Body.Append("                                                                                    </tbody>");
            //        Body.Append("                                                                                </table>");
            //        Body.Append("                                                                            </td>");
            //        Body.Append("                                                                        </tr>");
            //        Body.Append("                                                                    </tbody>");
            //        Body.Append("                                                                </table>");
            //        Body.Append("                                                            </td>");
            //        Body.Append("                                                        </tr>");
            //        Body.Append("                                                    </tbody>");
            //        Body.Append("                                                </table>");
            //        Body.Append("                                            </td>");
            //        Body.Append("                                        </tr>");
            //        Body.Append("                                        <tr>");
            //        Body.Append("                                            <td style=\"font-size: 11px; font-family: 'lucida grande',tahoma,verdana,arial,sans-serif;");
            //        Body.Append("                                                padding: 0px; width: 620px\">");
            //        Body.Append("                                                <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"border-collapse: collapse;");
            //        Body.Append("                                                    width: 100%\">");
            //        Body.Append("                                                    <tbody>");
            //        Body.Append("                                                        <tr>");
            //        Body.Append("                                                            <td style=\"padding: 7px 20px; background-color: #f2f2f2; border-left: none; border-right: none;");
            //        Body.Append("                                                                border-top: 1px solid #ccc; border-bottom: 1px solid #ccc\">");
            //        Body.Append("                                                            </td>");
            //        Body.Append("                                                        </tr>");
            //        Body.Append("                                                    </tbody>");
            //        Body.Append("                                                </table>");
            //        Body.Append("                                            </td>");
            //        Body.Append("                                        </tr>");
            //        Body.Append("                                    </tbody>");
            //        Body.Append("                                </table>");
            //        Body.Append("                            </td>");
            //        Body.Append("                        </tr>");
            //        Body.Append("                    </tbody>");
            //        Body.Append("                </table>");
            //        Body.Append("                <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"border-collapse: collapse;");
            //        Body.Append("                    width: 620px\">");
            //        Body.Append("                    <tbody>");
            //        Body.Append("                        <tr>");
            //        Body.Append("                            <td style=\"border-right: none; color: #999999; font-size: 11px; border-bottom: none;");
            //        Body.Append("                                font-family: 'lucida grande',tahoma,verdana,arial,sans-serif; border: none; border-top: none;");
            //        Body.Append("                                padding: 30px 20px; border-left: none\">");
            //        Body.Append("                                <span>");
            //        Body.Append("                                    <br />");
            //        Body.Append("                                    This is a autogenerated mail from Brandsystems Planning tool (COMSYS). You can´t");
            //        Body.Append("                                    respond to this mail in any form. If you encounter any problem with Brandsystems");
            //        Body.Append("                                    Planning tool (COMSYS), please contact the COMSYS Helpdesk, at email:comsys.noreply@brandsystems.in<br />");
            //        Body.Append("                                    <br />");
            //        Body.Append("                                    Legal Notice:</span>");
            //        Body.Append("                                <br />");
            //        Body.Append("                                <span>Unless expressly stated otherwise, this message is confidential and may be privileged.It");
            //        Body.Append("                                    is intended for the addressee(s) only. Access to this e-mail by anyone else is unauthorized.");
            //        Body.Append("                                    If you are not an addressee, any disclosure or copying of the contents of this e-mail");
            //        Body.Append("                                    or any action taken (or not taken) in reliance on it is unauthorized and may be");
            //        Body.Append("                                    unlawful. If you are not an addressee,please inform the sender immediately.</span>");
            //        Body.Append("                            </td>");
            //        Body.Append("                        </tr>");
            //        Body.Append("                    </tbody>");
            //        Body.Append("                </table>");
            //        Body.Append("                <span style=\"width: 620px\">");
            //        Body.Append("                    <img style=\"border: 0; width: 1px; min-height: 1px\"><u></u></span>");
            //        Body.Append("            </td>");
            //        Body.Append("        </tr>");
            //        Body.Append("    </tbody>");
            //        Body.Append("</table>");


            //        if (NotificationIDs.Count > 0)
            //        {
            //            bool Status = false;

            //            Status = Email.SendMail(BLL.GetUserByID(UserID).Email, "Recap Report " + MailType, Body.ToString());
            //            //Status = Email.SendMail("arun.prasad@brandsystems.in", "Marcom Notification", Body.ToString())


            //            if (Status)
            //            {
            //                //Update Database for User and Notification
            //                BLL.UpdateUserMailStatusForSubscription(UserID, CurrentDateTime, string.Join(",", NotificationIDs));
            //            }
            //        }
            //    }

            //}

         

            //Dim objMailList As New List(Of BO.Mail)
            //objMailList = BLL.GetAllInProgressMails()

            //For Each Item In objMailList

            //    Dim Status As Boolean = False

            //    Status = Email.SendMail(Item.ToMailAddress, Item.Subject, WebUtility.HtmlDecode(Item.Body))

            //    BLL.UpdateMailStatus(Item.ID, Item.Iteration + 1, Status)

            //Next
          
        }

    }

    public void PingServer()
    {
        try
        {
            WebClient http = new WebClient();
         //   string Result = http.DownloadString(System.Configuration.ConfigurationManager.AppSettings("AppUrl"));
        }
        catch (Exception ex)
        {
            string Message = ex.Message;
        }
    }


    #region "IDisposable Members"

    public void Dispose()
    {
        this.Stop();
    }

    #endregion

}