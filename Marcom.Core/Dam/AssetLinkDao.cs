﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrandSystems.Marcom.Core.Dam.Interface;

namespace BrandSystems.Marcom.Core.Dam
{
    public class AssetLink : IAssetlink
    {
        #region "Member variables"

        protected int _id;
        protected int _entityID;
        protected int _assetID;
        protected int _targetentityID;
        protected int _linkownerID;
        protected int _folderID;
        protected int _linkedby;

        #endregion

        #region "Constructor"

        public AssetLink() { }


        public AssetLink(int pid, int pEntityID, int pAssetID, int pTargetEntityID, int pLinkOwnerID, int pFolderID,int pLinkedBy)
        {
            this._id = pid;
            this._entityID = pEntityID;
            this._assetID = pAssetID;
            this._targetentityID = pTargetEntityID;
            this._linkownerID = pLinkOwnerID;
            this._folderID = pFolderID;
            this._linkedby = pLinkedBy;
        }

        #endregion


        #region "Public propertiers"

        public int ID
        {
            get { return _id; }
            set { _id = value; }

        }

        public int EntityID
        {
            get { return _entityID; }
            set
            {
                _entityID = value;
            }

        }


        public int AssetID
        {
            get { return _assetID; }
            set { _assetID = value; }

        }
        public virtual int TargetEntityID
        {
            get { return _targetentityID; }
            set { _targetentityID = value; }
        }

        public virtual int LinkOwnerID
        {
            get { return _linkownerID; }
            set { _linkownerID = value; }
        }

        public virtual int FolderID
        {
            get { return _folderID; }
            set { _folderID = value; }
        }

        public virtual int LinkedBy
        {
            get { return _linkedby; }
            set { _linkedby = value; }
        }

        #endregion



    }
}
