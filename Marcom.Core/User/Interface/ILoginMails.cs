﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.User.Interface
{
     public interface ILoginMails
    {
        #region Public Properties

        int Id
        {
            get;
            set;
        }
        string Email
        {
            get;
            set;

        }
        string FileGuId
        {
            get;
            set;
        }
        string Captcha
        {
            get;
            set;
        }
        string Unlockedby
        {
            get;
            set;

        }
        DateTime UnlockedTime
        {
            get;
            set;
        }

        #endregion
    }
}
