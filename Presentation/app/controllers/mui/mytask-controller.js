﻿(function (ng, app) {
    "use strict";

    function muimytaskCtrl($scope, $cookies, $resource, $compile, $window, $translate, $timeout, $location, MyTaskService, $modal) {
        $scope.ComplexTaskDataStructure = [];
        $scope.ComplexTaskFilterDataStructure = [];
        $scope.ClientCustomStatus = [];
        $scope.CustomMulitipleStatus = [];
        $scope.ShowTaskStatus = [{
            ID: 1,
            Name: 'In Progress'
        }, {
            ID: 2,
            Name: 'Completed'
        }, {
            ID: 4,
            Name: 'Unable to complete'
        }, {
            ID: 3,
            Name: 'Approved'
        }, {
            ID: 5,
            Name: 'Rejected'
        }];
        $scope.MulitipleStatus = [1];
        $scope.ShowTasksByAssignCriteria = [{
            ID: 1,
            Name: 'Assigned by me'
        }, {
            ID: 4,
            Name: 'Assigned to me'
        }];
        $scope.SortByCriteria = [{
            ID: 0,
            Name: 'Chronological'
        }, {
            ID: 2,
            Name: 'Main location'
        }];
        $scope.AssignRole = 4;
        $scope.FilterByentityID = 2;
        $scope.FilterStatusID = 1;
        $scope.PageNo = 1;
        var mytasktimer = $timeout(function () {
            GetMyTaskLists();
            gettingCustomFilterValues();
        }, 10);
        $scope.muiiscalender = {};
        $scope.muiiscalender.Iscalender = false;
        $scope.muiiscalender.CalenderID = 0;
        $scope.TaskAssignCriteria = 'Assigned to me';
        $scope.TaskCriteria = 'Main location';
        $scope.CuurenttaskStatus = 'In Progress';
        $scope.mttempWizardID = $scope.WizardID;
        if ($scope.mttempWizardID != undefined) {
            if ($scope.mttempWizardID.length > 0) {
                $scope.ngShowMyTask = false;
                $('#container.myTask.myPage').parents('.box-content').find('.InProgressForMyTask').remove();
            } else $scope.ngShowMyTask = true;
        } else $scope.ngShowMyTask = true;
        $scope.Total_rows = 0;

        function GetMyTaskLists() {
            var MyTaskParams = {};
            MyTaskParams.FilterByentityID = $scope.FilterByentityID;
            MyTaskParams.PageNo = $scope.PageNo;
            MyTaskParams.AssignRole = $scope.AssignRole;
            MyTaskParams.FilterStatusID = $scope.MulitipleStatus;
            MyTaskParams.CustomFilterID = $scope.CustomMulitipleStatus;
            if ($scope.MulitipleStatus.contains(2))
                $scope.MulitipleStatus.push(8);
            MyTaskService.GetMytasks(MyTaskParams).then(function (GetMytasksResult) {
                $scope.NewTime = $scope.DefaultImageSettings.ImageSpan;
                if (GetMytasksResult.StatusCode == 200) {
                    var response = GetMytasksResult.Response;
                    $scope.ComplexTaskFilterDataStructure = [];
                    if (response != undefined) {
                        if (response.length > 0) $scope.Total_rows = response[0].Total_COUNT;
                        else $scope.Total_rows = 0;
                        CreateSource(response);
                        $timeout(function () { loadScrollSettings(); }, 10);
                    }
                }
            });
            $(window).AdjustHeightWidth();
        }

        function CreateSource(response) {
            var LstofEntityIds = new Array();
            if ($scope.FilterByentityID == 2) {
                for (var e = 0, CurrentId; CurrentId = response[e++];) {
                    if (!LstofEntityIds.contains(CurrentId["EntityId"])) {
                        LstofEntityIds.push(CurrentId["EntityId"]);
                    }
                }
                var mytaskcollection = [];
                for (var l = 0, EntityId; EntityId = LstofEntityIds[l++];) {
                    var mytaskCurrentdata = [];
                    var resultObj = $.grep(response, function (rel) {
                        return rel.EntityId == EntityId;
                    });
                    if (resultObj != undefined) {
                        var mytasklst = [];
                        for (var v = 0, val; val = resultObj[v++];) {
                            var Currentmytask = {};
                            Currentmytask.UserName = val["UserName"];
                            Currentmytask.AssigneesName = val["UserName1"];
                            Currentmytask.AssigneesIdArr = [];
                            Currentmytask.AssigneesIdArr = val["UserID1"].split(",");
                            Currentmytask.UserID = val["UserID"];
                            Currentmytask.DueDate = val["DueDate"];
                            Currentmytask.EntityId = val["EntityId"];
                            Currentmytask.EntityName = val["EntityName"];
                            Currentmytask.TaskListID = val["TaskListID"];
                            if (new Date.create(val["DueDate"]) > new Date.create()) {
                                Currentmytask.Noofdays = datediff(val["DueDate"], ReturnFormattedTodayDate()) - HolidayCount(new Date.create(val["DueDate"]));
                            } else {
                                Currentmytask.Noofdays = datediff(val["DueDate"], ReturnFormattedTodayDate()) + HolidayCount(new Date.create(val["DueDate"]));
                            }
                            Currentmytask.RoleId = val["RoleId"];
                            Currentmytask.TaskDescription = val["TaskDescription"];
                            Currentmytask.TaskId = val["TaskId"];
                            Currentmytask.IsAdminTask = val["IsAdminTask"];
                            Currentmytask.TaskName = val["TaskName"];
                            Currentmytask.colorcode = val["colorcode"];
                            Currentmytask.shortdesc = (val["ShortDescription"] != undefined) ? val["ShortDescription"] : val["shortdesc"];
                            Currentmytask.Path = [];
                            $($.parseXML(val.pathinfo)).find('p').each(function () {
                                Currentmytask.Path.push({
                                    "ID": $(this).attr('i'),
                                    "TypeID": $(this).attr('t'),
                                    "Name": $(this).attr('n')
                                });
                            });
                            Currentmytask.TaskStatus = $scope.AssignRole == 4 ? (val["TaskStatus"] == 2 ? val["TaskStatus"] : val["ApprovalStatus"]) : val["TaskStatus"];
                            Currentmytask.TaskTypeId = (val["TaskType"] != undefined) ? val["TaskType"] : val["TaskTypeId"];
                            Currentmytask.ProgressCount = "";
                            if (val["ProgressCount"].length > 0) {
                                Currentmytask.ProgressCount = "(" + val["ProgressCount"] + ")";
                            }

                            if (val["TaskStatus"] == 2) {
                                if ((val["ApprovalStatus"] == 1 || val["ApprovalStatus"] == null)) {
                                    val["ApprovalStatus"] = 2;
                                    Currentmytask.TaskStatus = 2;
                                }
                            }

                            if (val["TaskStatus"] == 5) {
                                if (!(val["ApprovalStatus"] == 1 || val["ApprovalStatus"] == null)) {
                                    mytasklst.push(Currentmytask);
                                }
                            } else {
                                mytasklst.push(Currentmytask);
                            }
                            if ($scope.FilterByentityID == 2) {
                                mytaskCurrentdata.FilterHeaderName = val["EntityName"];
                                mytaskCurrentdata.EntityId = val["EntityId"];
                                mytaskCurrentdata.EntityColorCode = val["EColorCode"];
                                mytaskCurrentdata.EntityShortDescription = val["EShortDescription"];
                            }
                            if ($scope.FilterByentityID == 1) mytaskCurrentdata.FilterHeaderName = val["EntityName"];
                        }
                        mytaskCurrentdata.TaskCollection = mytasklst;
                        if (mytasklst.length > 0) {
                            mytaskcollection.push(mytaskCurrentdata);
                        }
                    }
                }
            } else if ($scope.FilterByentityID == 1) {
                for (var l = 0, EntityId; EntityId = LstofEntityIds[l++];) {
                    var mytaskCurrentdata = [];
                    var resultObj = $.grep(response, function (rel) {
                        return rel.EntityId == EntityId;
                    });
                    if (resultObj != undefined) {
                        var mytasklst = [];
                        for (var v = 0, val; val = resultObj[v++];) {
                            var Currentmytask = {};
                            Currentmytask.UserName = val["UserName"];
                            Currentmytask.AssigneesName = val["UserName1"];
                            Currentmytask.AssigneesIdArr = [];
                            Currentmytask.AssigneesIdArr = val["UserID1"].split(",");
                            Currentmytask.UserID = val["UserID"];
                            Currentmytask.DueDate = val["DueDate"];
                            Currentmytask.EntityId = val["EntityId"];
                            Currentmytask.EntityName = val["EntityName"];
                            Currentmytask.TaskListID = val["TaskListID"];
                            if (new Date.create(val["DueDate"]) > new Date.create()) {
                                Currentmytask.Noofdays = datediff(val["DueDate"], ReturnFormattedTodayDate()) - HolidayCount(new Date.create(val["DueDate"]));
                            } else {
                                Currentmytask.Noofdays = datediff(val["DueDate"], ReturnFormattedTodayDate()) + HolidayCount(new Date.create(val["DueDate"]));
                            }
                            Currentmytask.RoleId = val["RoleId"];
                            Currentmytask.TaskDescription = val["TaskDescription"];
                            Currentmytask.TaskId = val["TaskId"];
                            Currentmytask.IsAdminTask = val["IsAdminTask"];
                            Currentmytask.TaskName = val["TaskName"];
                            Currentmytask.colorcode = val["colorcode"];
                            Currentmytask.shortdesc = (val["ShortDescription"] != undefined) ? val["ShortDescription"] : val["shortdesc"];
                            Currentmytask.Path = [];
                            $($.parseXML(val.pathinfo)).find('p').each(function () {
                                Currentmytask.Path.push({
                                    "ID": $(this).attr('i'),
                                    "TypeID": $(this).attr('t'),
                                    "Name": $(this).attr('n')
                                });
                            });
                            Currentmytask.TaskStatus = $scope.AssignRole == 4 ? (val["TaskStatus"] == 2 ? val["TaskStatus"] : val["ApprovalStatus"]) : val["TaskStatus"];
                            Currentmytask.TaskTypeId = (val["TaskType"] != undefined) ? val["TaskType"] : val["TaskTypeId"];
                            Currentmytask.ProgressCount = "";
                            if (val["TaskStatus"] == 2) {
                                if ((val["ApprovalStatus"] == 1 || val["ApprovalStatus"] == null)) {
                                    val["ApprovalStatus"] = 2;
                                    Currentmytask.TaskStatus = 2;
                                }
                            }
                            if (val["ProgressCount"].length > 0) {
                                Currentmytask.ProgressCount = "(" + val["ProgressCount"] + ")";
                            }
                            if (val["TaskStatus"] == 5) {
                                if (!(val["ApprovalStatus"] == 1 || val["ApprovalStatus"] == null)) {
                                    mytasklst.push(Currentmytask);
                                }
                            } else {
                                mytasklst.push(Currentmytask);
                            }
                            if ($scope.FilterByentityID == 2) {
                                mytaskCurrentdata.FilterHeaderName = val["EntityName"];
                                mytaskCurrentdata.EntityId = val["EntityId"];
                                mytaskCurrentdata.EntityColorCode = val["EColorCode"];
                                mytaskCurrentdata.EntityShortDescription = val["EShortDescription"];
                            }
                            if ($scope.FilterByentityID == 1) mytaskCurrentdata.FilterHeaderName = val["EntityName"];
                        }
                        mytaskCurrentdata.TaskCollection = mytasklst;
                        if (mytasklst.length > 0) {
                            mytaskcollection.push(mytaskCurrentdata);
                        }
                    }
                }
            } else {
                var mytaskcollection = [];
                var mytaskCurrentdata = [];
                var mytasklst = [];
                for (var v = 0, val; val = response[v++];) {
                    var Currentmytask = {};
                    Currentmytask.UserName = val["UserName"];
                    Currentmytask.AssigneesName = val["UserName1"];
                    Currentmytask.AssigneesIdArr = [];
                    Currentmytask.AssigneesIdArr = val["UserID1"].split(",");
                    Currentmytask.UserID = val["UserID"];
                    Currentmytask.DueDate = val["DueDate"];
                    Currentmytask.EntityId = val["EntityId"];
                    Currentmytask.EntityName = val["EntityName"];
                    Currentmytask.TaskListID = val["TaskListID"];
                    if (new Date.create(val["DueDate"]) > new Date.create()) {
                        Currentmytask.Noofdays = datediff(val["DueDate"], ReturnFormattedTodayDate()) - HolidayCount(new Date.create(val["DueDate"]));
                    } else {
                        Currentmytask.Noofdays = datediff(val["DueDate"], ReturnFormattedTodayDate()) + HolidayCount(new Date.create(val["DueDate"]));
                    }
                    Currentmytask.RoleId = val["RoleId"];
                    Currentmytask.TaskDescription = val["TaskDescription"];
                    Currentmytask.TaskId = val["TaskId"];
                    Currentmytask.IsAdminTask = val["IsAdminTask"];
                    Currentmytask.TaskName = val["TaskName"];
                    Currentmytask.colorcode = val["colorcode"];
                    Currentmytask.shortdesc = (val["ShortDescription"] != undefined) ? val["ShortDescription"] : val["shortdesc"];
                    Currentmytask.Path = [];
                    $($.parseXML(val.pathinfo)).find('p').each(function () {
                        Currentmytask.Path.push({
                            "ID": $(this).attr('i'),
                            "TypeID": $(this).attr('t'),
                            "Name": $(this).attr('n')
                        });
                    });
                    Currentmytask.TaskStatus = $scope.AssignRole == 4 ? (val["TaskStatus"] == 2 ? val["TaskStatus"] : val["ApprovalStatus"]) : val["TaskStatus"];
                    Currentmytask.TaskTypeId = (val["TaskType"] != undefined) ? val["TaskType"] : val["TaskTypeId"];
                    Currentmytask.ProgressCount = "";

                    if (val["TaskStatus"] == 2) {
                        if ((val["ApprovalStatus"] == 1 || val["ApprovalStatus"] == null)) {
                            val["ApprovalStatus"] = 2;
                            Currentmytask.TaskStatus = 2;
                        }
                    }

                    if (val["TaskStatus"] == 5) {
                        if (!(val["ApprovalStatus"] == 1 || val["ApprovalStatus"] == null)) {
                            mytasklst.push(Currentmytask);
                        }
                    } else {
                        mytasklst.push(Currentmytask);
                    }
                    if (val["ProgressCount"].length > 0) {
                        Currentmytask.ProgressCount = "(" + val["ProgressCount"] + ")";
                    }
                }
                mytaskCurrentdata.TaskCollection = mytasklst;
                if (mytasklst.length > 0) {
                    mytaskcollection.push(mytaskCurrentdata);
                }
            }
            for (var i = 0, mytaskObj; mytaskObj = mytaskcollection[i++];) {
                if ($scope.FilterByentityID == 2) {
                    var isExist = $.grep($scope.ComplexTaskFilterDataStructure, function (rel) {
                        return rel.EntityGroupId == mytaskObj.EntityId;
                    });
                    if (isExist != undefined) {
                        if (isExist.length > 0) {
                            UpdateRecord(isExist[0], mytaskObj);
                        } else {
                            InsertRecord(mytaskObj);
                        }
                    }
                } else {
                    if ($scope.ComplexTaskFilterDataStructure.length == 0) {
                        InsertRecord(mytaskObj);
                    } else {
                        UpdateRecord($scope.ComplexTaskFilterDataStructure[0], mytaskObj);
                    }
                }
                $(window).AdjustHeightWidth();
            }
        }

        function InsertRecord(mytaskObj) {
            if ($scope.FilterByentityID == 2) {
                var data = {
                    DivVisible: [],
                    Header: [],
                    body: [],
                    EntityGroupId: 0,
                    EntityColorCode: [],
                    EntityShortDescription: []
                };
                data.DivVisible = true;
                data.Header = mytaskObj.FilterHeaderName;
                data.body = mytaskObj.TaskCollection;
                data.EntityGroupId = mytaskObj.EntityId;
                data.TaskListID = mytaskObj.TaskListID;
                data.EntityColorCode = mytaskObj.EntityColorCode;
                data.EntityShortDescription = mytaskObj.EntityShortDescription;
            } else {
                var data = {
                    DivVisible: [],
                    Header: [],
                    body: [],
                    EntityColorCode: [],
                    EntityShortDescription: []
                };
                if ($scope.FilterByentityID == 0) {
                    data.DivVisible = false;
                    data.Header = "";
                    data.body = mytaskObj.TaskCollection;
                    data.EntityColorCode = mytaskObj.EntityColorCode;
                    data.EntityShortDescription = mytaskObj.EntityShortDescription;
                } else {
                    data.DivVisible = true;
                    data.Header = mytaskObj.FilterHeaderName;
                    data.body = mytaskObj.TaskCollection;
                    data.EntityColorCode = mytaskObj.EntityColorCode;
                    data.EntityShortDescription = mytaskObj.EntityShortDescription;
                }
            }
            $scope.ComplexTaskFilterDataStructure.push(data);
        }

        function UpdateRecord(currentData, mytaskObj) {
            if (currentData.body.length == 0) {
                currentData.body = mytaskObj.TaskCollection;
            } else {
                if (mytaskObj != undefined) if (mytaskObj.TaskCollection.length > 0) {
                    for (var u = 0, val; val = mytaskObj.TaskCollection[u++];) {
                        var Currentmytask = {};
                        Currentmytask.UserName = val["UserName"];
                        if (val["UserName1"] != undefined) Currentmytask.AssigneesName = val["UserName1"];
                        else {
                            if (val["AssigneesName"] != undefined) Currentmytask.AssigneesName = val["AssigneesName"];
                        }
                        Currentmytask.AssigneesIdArr = [];
                        if (val["UserID1"] != undefined) Currentmytask.AssigneesIdArr = val["UserID1"];
                        else {
                            if (val["AssigneesIdArr"] != undefined) Currentmytask.AssigneesIdArr = val["AssigneesIdArr"];
                        }
                        Currentmytask.UserID = val["UserID"];
                        Currentmytask.DueDate = val["DueDate"];
                        Currentmytask.EntityId = val["EntityId"];
                        Currentmytask.EntityName = val["EntityName"];
                        Currentmytask.TaskListID = val["TaskListID"];
                        if (new Date.create(val["DueDate"]) > new Date.create()) {
                            Currentmytask.Noofdays = datediff(val["DueDate"], ReturnFormattedTodayDate()) - HolidayCount(new Date.create(val["DueDate"]));
                        } else {
                            Currentmytask.Noofdays = datediff(val["DueDate"], ReturnFormattedTodayDate()) + HolidayCount(new Date.create(val["DueDate"]));
                        }
                        Currentmytask.RoleId = val["RoleId"];
                        Currentmytask.TaskDescription = val["TaskDescription"];
                        Currentmytask.TaskId = val["TaskId"];
                        Currentmytask.IsAdminTask = val["IsAdminTask"];
                        Currentmytask.TaskName = val["TaskName"];
                        Currentmytask.colorcode = val["colorcode"];
                        Currentmytask.shortdesc = (val["ShortDescription"] != undefined) ? val["ShortDescription"] : val["shortdesc"];
                        Currentmytask.Path = val.Path;
                        $($.parseXML(val.pathinfo)).find('p').each(function () {
                            Currentmytask.Path.push({
                                "ID": $(this).attr('i'),
                                "TypeID": $(this).attr('t'),
                                "Name": $(this).attr('n')
                            });
                        });
                        Currentmytask.TaskStatus = $scope.AssignRole == 4 ? (val["TaskStatus"] == 2 ? val["TaskStatus"] : val["ApprovalStatus"]) : val["TaskStatus"];

                        if (val["TaskStatus"] == 2) {
                            if ((val["ApprovalStatus"] == 1 || val["ApprovalStatus"] == null)) {
                                val["ApprovalStatus"] = 2;
                                Currentmytask.TaskStatus = 2;
                            }
                        }

                        Currentmytask.TaskTypeId = (val["TaskType"] != undefined) ? val["TaskType"] : val["TaskTypeId"];
                        Currentmytask.ProgressCount = "";
                        if (val["ProgressCount"].length > 0 && (val["ProgressCount"].contains("(") == false)) {
                            Currentmytask.ProgressCount = "(" + val["ProgressCount"] + ")";
                        } else {
                            Currentmytask.ProgressCount = val["ProgressCount"];
                        }
                        currentData.body.push(Currentmytask);
                    }
                }
            }
        }
        $scope.ShowtasksChangeVal = function (ID) {
            ChangeSelectedShowtasks(ID);
        }

        function ChangeSelectedShowtasks(ID) {
            var Currentitem = $.grep($scope.ShowTasksByAssignCriteria, function (n, i) {
                return $scope.ShowTasksByAssignCriteria[i].ID == ID;
            });
            $scope.AssignRole = ID;
            $scope.TaskAssignCriteria = Currentitem[0].Name;
            $scope.ComplexTaskDataStructure = {
                Header: '',
                body: ''
            };
            $scope.PageNo = 1;
            $scope.Total_rows = 0;
            GetMyTaskLists();
        }
        $scope.SelctedCriteriaChangeVal = function (ID) {
            Changeselectedval(ID);
        }

        function Changeselectedval(ID) {
            var Currentitem = $.grep($scope.SortByCriteria, function (n, i) {
                return $scope.SortByCriteria[i].ID == ID;
            });
            $scope.FilterByentityID = ID;
            $scope.ComplexTaskDataStructure = {
                Header: '',
                body: ''
            };
            $scope.TaskCriteria = Currentitem[0].Name;
            $scope.PageNo = 1;
            $scope.Total_rows = 0;
            GetMyTaskLists();
        }
        $scope.ShowTaskStatusChangeVal = function () {
            $scope.PageNo = 1;
            $scope.Total_rows = 0;
            GetMyTaskLists();
        }
        $scope.StatusDivClasses = function (statusCode, overdue, IsAdminTask) {
            var baseClass = 'ListItem ';
            if (statusCode == 0 && overdue >= 0 && IsAdminTask == 0) return baseClass + 'Unassigned';
            if (statusCode == 0 && overdue >= 0 && IsAdminTask >= 0) return baseClass + 'Unassigned';
            if (statusCode == 0 && overdue < 0 && IsAdminTask > 0) return baseClass + 'UnableToComplete OverDue';
            if (statusCode == 0 && overdue == 0 && IsAdminTask > 0) return baseClass + 'Unassigned';
            if (statusCode == 0 && overdue < 0 && IsAdminTask >= 0) return baseClass + 'Unassigned OverDue';
            if (statusCode == 1 && overdue < 0) return baseClass + 'InProgress OverDue';
            if (statusCode == 1) return baseClass + 'InProgress';
            if (statusCode == 2) return baseClass + 'Completed';
            if (statusCode == 3) return baseClass + 'Approved';
            if (statusCode == 4) return baseClass + 'UnableToComplete';
            if (statusCode == 5 || statusCode == 6) return baseClass + 'Rejected';
            if (statusCode == 7) return baseClass + 'NotApplicable';
            if (statusCode == 8) return baseClass + 'Completed';
        }
        $scope.manipulatedate = function (date) {
            if (date != "") return true;
            else return false;
        }
        $scope.AddStatustext = function (statusCode, overdue, IsAdminTask) {
            if (statusCode == 0 && overdue >= 0 && IsAdminTask == 0) return 'Unassigned';
            if (statusCode == 0 && overdue >= 0 && IsAdminTask >= 0) return 'Unassigned';
            if (statusCode == 0 && overdue < 0 && IsAdminTask > 0) return 'Unable to complete overDue';
            if (statusCode == 0 && overdue == 0 && IsAdminTask > 0) return 'Unassigned';
            if (statusCode == 0 && overdue < 0 && IsAdminTask >= 0) return 'Unassigned overDue';
            if (statusCode == 1) return 'In progress';
            if (statusCode == 2) return 'Completed';
            if (statusCode == 3) return 'Approved';
            if (statusCode == 4) return 'Unable to complete';
            if (statusCode == 5 || statusCode == 6) return 'Rejected';
            if (statusCode == 7) return 'Not applicable';
            if (statusCode == 8) return 'Completed';
        }
        $scope.manipulatedate = function (date) {
            if (date != "") return true;
            else return false;
        }
        $scope.CustomDateFormat = function (date) {
            if (date != "") return FormatStandard(date, $scope.DefaultSettings.DateFormat);
            else return "-";
        }
        $scope.TaskDetails = [];
        $scope.OpenPopUpMyTask = function (taskID, IsAdminTask, taskTypeId, TaskObject, taskStatus, assetid) {
            $scope.ReorderOverviewStructure = false;
            var modalInstance = $modal.open({
                templateUrl: 'views/mui/task/TaskEdit.html',
                controller: "mui.task.muitaskFlowEditCtrl",
                resolve: {
                    items: function () {
                        return {
                            TaskId: taskID,
                            EntityId: TaskObject.EntityId,
                            taskStatus: taskStatus
                        };
                    }
                },
                scope: $scope,
                windowClass: 'taskImprovementPopup TaskPopup TaskPopupTabbed popup-widthL',
                backdrop: "static"
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                //console.log('Modal dismissed at: ' + new Date());
            });
        }
        if ($scope.EntityID != null || $scope.EntityID != undefined) {
            $timeout(function () {
                $scope.OpenPopUpMyTask(parseInt($scope.TaskID), parseInt($scope.EntityTypeID), parseInt($scope.EntityID))
            }, 2000);
        }

        function BindFundRequestTaskDetails(taskTypeId, TaskObject) {
            $scope.ShowCompleteBtn = false;
            $scope.ShowApproveBtn = false;
            $scope.ShowRejectedBtn = false;
            $scope.ShowWithdrawBtn = false;
            $scope.ShowUnabletoCompleteBtn = false;
            $("#ReviewTask,#ApprovalTask,#WorkTask").removeClass();
            $("#ReviewTask,#ApprovalTask,#WorkTask").addClass("modal hide fade TaskPopup " + TaskObject.StatusName.replace(/\s+/g, ""));
            $scope.listPredfWorkflowFilesAttch = TaskObject.taskAttachment;
            var isThisMemberPresent = $.grep(TaskObject.taskMembers, function (e) {
                return (e.UserID == parseInt($cookies['UserId']) && e.RoleID != 1);
            });
            var taskOwnerObj = $.grep(TaskObject.taskMembers, function (e) {
                return (e.RoleID == 1);
            })[0];
            if (taskTypeId == 2) {
                if (TaskObject.TaskStatus == 1 && taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
                    if (taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
                        $scope.ShowWithdrawBtn = false;
                    }
                }
                if (TaskObject.TaskStatus == 1 && isThisMemberPresent.length > 1) {
                    $scope.ShowCompleteBtn = true;
                    $scope.ShowWithdrawBtn = true;
                }
            } else if (taskTypeId == 3) {
                if (TaskObject.TaskStatus == 1 && taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
                    if (taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
                        $scope.ShowWithdrawBtn = false;
                    }
                }
                if (TaskObject.TaskStatus == 1 && isThisMemberPresent.length >= 1) {
                    if ($scope.IsLock != true) {
                        if (isThisMemberPresent.length == 1) {
                            if (isThisMemberPresent[0].ApprovalStatus == null) {
                                $scope.ShowApproveBtn = true;
                                $scope.ShowRejectedBtn = true;
                                $scope.ShowUnabletoCompleteBtn = false;
                            }
                        }
                    }
                }
            } else if (taskTypeId == 31) {
                if (TaskObject.TaskStatus == 1 && taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
                    if (taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
                        $scope.ShowWithdrawBtn = false;
                    }
                }
                if (TaskObject.TaskStatus == 1 && isThisMemberPresent.length >= 1) {
                    if ($scope.IsLock != true) {
                        if (isThisMemberPresent.length == 1) {
                            if (isThisMemberPresent[0].ApprovalStatus == null) {
                                $scope.ShowApproveBtn = false;
                                $scope.ShowCompleteBtn = true;
                                $scope.ShowRejectedBtn = false;
                                $scope.ShowUnabletoCompleteBtn = true;
                            }
                        }
                    }
                }
            }
            $scope.TaskBriefDetails.taskListUniqueID = TaskObject.TaskListID;
            $scope.TaskBriefDetails.taskName = TaskObject.Name;
            $scope.TaskBriefDetails.taskTypeId = taskTypeId;
            $scope.TaskBriefDetails.taskTypeName = TaskObject.TaskTypeName;
            $scope.TaskBriefDetails.dueDate = dateFormat(TaskObject.strDate, $scope.DefaultSettings.DateFormat);
            $scope.TaskBriefDetails.dueIn = TaskObject.totalDueDays;
            $scope.TaskBriefDetails.taskOwner = taskOwnerObj.UserName;
            $scope.TaskBriefDetails.status = TaskObject.StatusName;
            $scope.TaskBriefDetails.statusID = TaskObject.TaskStatus;
            $scope.TaskBriefDetails.Description = TaskObject.Description;
            $scope.TaskBriefDetails.taskID = TaskObject.Id;
            $scope.TaskBriefDetails.ownerId = taskOwnerObj.UserID;
            $scope.TaskBriefDetails.EntityID = TaskObject.EntityID;
            $scope.TaskBriefDetails.taskmembersList = TaskObject.taskAssigness;
            $scope.TaskBriefDetails.totalTaskMembers = TaskObject.TotaltaskAssigness;
            var unresponsedMembers = $.grep(TaskObject.taskAssigness, function (e) {
                return (e.ApprovalStatus != null);
            });
            $scope.TaskBriefDetails.taskProgressCount = unresponsedMembers.length.toString() + "/" + TaskObject.taskAssigness.length.toString();
            $scope.TaskMemberList = TaskObject.taskAssigness;
            $scope.TaskBriefDetails.taskAttachmentsList = TaskObject.taskAttachment;
            if (taskTypeId == 3 || taskTypeId == 31 || taskTypeId == 32 || taskTypeId == 36) $scope.groupByTaskRoundTrip('ApprovalRount');
            $scope.name = $scope.TaskBriefDetails.taskName;
            $scope.DescriptionChange = $scope.TaskBriefDetails.Description;
            $scope.company = $scope.TaskBriefDetails.dueDate;
            SeperateUsers();
        }

        function feedforfundingrequestTask(entityid) {
            fundrequestID = entityid;
            $scope.Fundingreqfeedhtml = '';
            $scope.entityNewsFeeds = {};
            var PagenoforScrollfund = 0;
            try {
                MyTaskService.GetEnityFeedsForFundingReq(entityid, PagenoforScrollfund, false).then(function (getFundingreqNewsFeedResult) {
                    PagenoforScrollfund += 1;
                    var fundingreqfundingreqfeeddivHtml = '';
                    $('#finicialfundingrequestfeeddiv').html('');
                    for (var i = 0; i < getFundingreqNewsFeedResult.Response.length; i++) {
                        var fundingreqfeeddivHtml = '';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li data-ID=' + getFundingreqNewsFeedResult.Response[i].FeedId + '>';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="avatar"> <img src=Handlers/UserImage.ashx?id=' + getFundingreqNewsFeedResult.Response[i].Actor + '' + '&time=' + $scope.DefaultImageSettings.ImageSpan + ' alt="Avatar"></span>';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="block" data-parent="parentforcomment"><div class="arrow-left"></div>';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="header"><a href=' + getFundingreqNewsFeedResult.Response[i].UserEmail + '>' + getFundingreqNewsFeedResult.Response[i].UserName + '</a></span>';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="body">' + getFundingreqNewsFeedResult.Response[i].FeedText + '</span>';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="footer"><span class="time">' + getFundingreqNewsFeedResult.Response[i].FeedHappendTime + '</span>';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<button class="btn btn-small pull-right" data-DynHTML="CommentHTML" id="Comment_' + getFundingreqNewsFeedResult.Response[i].FeedId + '">Comment</button></span></div>';
                        if (getFundingreqNewsFeedResult.Response[i].FeedComment != null) {
                            for (var j = 0; j < getFundingreqNewsFeedResult.Response[i].FeedComment.length; j++) {
                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<ul class="feedcontent"><li>';
                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="avatar"><img src= Handlers/UserImage.ashx?id=' + getFundingreqNewsFeedResult.Response[i].FeedComment[j].Actor + '&time=' + $scope.DefaultImageSettings.ImageSpan + ' alt="Avatar"></span>';
                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="block"><div class="arrow-left"></div>';
                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="header"><a href=' + getFundingreqNewsFeedResult.Response[i].FeedComment[j].UserEmail + '>' + getFundingreqNewsFeedResult.Response[i].FeedComment[j].UserName + '</a></span>';
                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '</span>';
                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="body">' + getFundingreqNewsFeedResult.Response[i].FeedComment[j].Comment + '</span>';
                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="footer"><span class="time">' + getFundingreqNewsFeedResult.Response[i].FeedComment[j].CommentedOn + '</span></span>';
                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '</div></li></ul>';
                            }
                        }
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '</li>';
                        $('#finicialfundingrequestfeeddiv').append(fundingreqfeeddivHtml);
                    }
                });
            } catch (e) { }
        }
        $scope.TooltipTaskName = function (description) {
            if (description != null & description != "") {
                return "<span class='preWrap-ws'>" + description + "</span>";
            } else {
                return "<label class=\"align-center\">No description</label>";
            }
        };
        $(window).on("ontaskAction", function (event) {
            $("#loadtaskedit").modal("hide");
            $scope.PageNo = 1;
            $scope.Total_rows = 0;
            GetMyTaskLists();
        });
        $(window).on("onChecklisttaskAction", function (event) {
            $scope.PageNo = 1;
            $scope.Total_rows = 0;
            GetMyTaskLists();
        });
        $(window).on("onBreadCrumtaskEditAction", function (event) {
            $("#loadtaskedit").modal("hide");
        });
        $scope.IsActiveEntity = function (EntityID, TypeId) {
            MyTaskService.IsActiveEntity(EntityID).then(function (result) {
                if (result.Response == true) {
                    if (TypeId == 5) {
                        $location.path('/mui/planningtool/costcentre/detail/section/' + EntityID);
                    } else if (TypeId == 10) {
                        $location.path('/mui/planningtool/objective/detail/section/' + EntityID);
                    } else {
                        $location.path('/mui/planningtool/default/detail/section/' + EntityID + '/overview');
                    }
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1866.Caption'));
                }
            });
        }
        $scope.DecodedTaskName = function (TaskName) {
            if (TaskName != null & TaskName != "") {
                return $('<div />').html(TaskName).text();
            } else {
                return "";
            }
        };

        function getMoreListItems() {
            $scope.PageNo = $scope.PageNo + 1;
            LoadRecords();
            var loader = $("#loader");
            loader.text("Loading New Items");
        }

        function LoadRecords() {
            var MyTaskParams = {};
            MyTaskParams.FilterByentityID = $scope.FilterByentityID;
            MyTaskParams.PageNo = $scope.PageNo;
            MyTaskParams.AssignRole = $scope.AssignRole;
            MyTaskParams.FilterStatusID = $scope.MulitipleStatus;
            MyTaskParams.CustomFilterID = $scope.CustomMulitipleStatus;
            if ($scope.MulitipleStatus.contains(2))
                $scope.MulitipleStatus.push(8);
            MyTaskService.GetMytasks(MyTaskParams).then(function (GetMytasksResult) {
                $scope.NewTime = $scope.DefaultImageSettings.ImageSpan;
                if (GetMytasksResult.StatusCode == 200) {
                    var response = GetMytasksResult.Response;
                    loading = false;
                    if (response != undefined) {
                        CreateSource(response);
                    }
                }
            });
            $(window).AdjustHeightWidth();
        }
        $scope.ShowTaskAssigneeTooltipContent = function (memObj, memNames) {
            var msg = "";
            var membersNamesArr = [];
            membersNamesArr = memNames.split(",");
            if (memObj != null) {
                for (var i = 0; i <= memObj.length - 1; i++) {
                    msg += '<div class="groupAssigneeTooltipContent"><img position="right" src="Handlers/UserImage.ashx?id=' + memObj[i] + '">' + membersNamesArr[i] + '</div>';
                }
            }
            return msg;
        }

        function checkListItemContents() {
            getMoreListItems(function () { });
        }
        var loading = false;

        function loadScrollSettings() {
            $(window).scroll(function () {
                if ($scope.Total_rows >= ($scope.PageNo * 15)) {
                    if (loading) {
                        return;
                    }
                    if ($(window).scrollTop() > $(document).height() - $(window).height() - 200) {
                        loading = true;
                        checkListItemContents();
                    }
                }
            });
        }

        function HolidayCount(dueDate) {
            $scope.testduedate = dateFormat(dueDate, $scope.format);
            var nbcount = 0;
            var today = new Date.create();
            var holiday_exist = $.grep($scope.tempholidays, function (e) {
                return new Date.create(e) <= dueDate && new Date.create(e) >= today;
            });
            var holiday_exist_min = $.grep($scope.tempholidays, function (e) {
                return new Date.create(e) >= dueDate && new Date.create(e) <= today;
            });
            if (dueDate > today) {
                var tempdates = getDates(today, dueDate);
            } else {
                var tempdates = getDates(dueDate, today);
            }
            if (tempdates != null) {
                var holiday_exist_nonbusinessdays = $.grep(tempdates, function (e) {
                    return (($scope.nonbusinessdays.sunday === "true" && e.getDay() === 0) || ($scope.nonbusinessdays.monday === "true" && e.getDay() === 1) || ($scope.nonbusinessdays.tuesday === "true" && e.getDay() === 2) || ($scope.nonbusinessdays.wednesday === "true" && e.getDay() === 3) || ($scope.nonbusinessdays.thursday === "true" && e.getDay() === 4) || ($scope.nonbusinessdays.friday === "true" && e.getDay() === 5) || ($scope.nonbusinessdays.saturday === "true" && e.getDay() === 6));
                });
            }
            if (holiday_exist_nonbusinessdays != null) {
                nbcount = holiday_exist_nonbusinessdays.length;
                if (new Date.create($scope.testduedate) > new Date.create()) {
                    if (holiday_exist != null) {
                        var Temp_holiday_exist = $.grep(holiday_exist, function (e) {
                            return (($scope.nonbusinessdays.sunday === "true" && new Date.create(e).getDay() === 0) || ($scope.nonbusinessdays.monday === "true" && new Date.create(e).getDay() === 1) || ($scope.nonbusinessdays.tuesday === "true" && new Date.create(e).getDay() === 2) || ($scope.nonbusinessdays.wednesday === "true" && new Date.create(e).getDay() === 3) || ($scope.nonbusinessdays.thursday === "true" && new Date.create(e).getDay() === 4) || ($scope.nonbusinessdays.friday === "true" && new Date.create(e).getDay() === 5) || ($scope.nonbusinessdays.saturday === "true" && new Date.create(e).getDay() === 6));
                        });
                        if (Temp_holiday_exist != null) {
                            if (Temp_holiday_exist.length > 0 || nbcount > 0) {
                                return (holiday_exist.length - Temp_holiday_exist.length) + nbcount;
                            } else {
                                return 0;
                            }
                        }
                    }
                } else {
                    if (holiday_exist_min != null) {
                        var Temp_holiday_exist = $.grep(holiday_exist_min, function (e) {
                            return (($scope.nonbusinessdays.sunday === "true" && new Date.create(e).getDay() === 0) || ($scope.nonbusinessdays.monday === "true" && new Date.create(e).getDay() === 1) || ($scope.nonbusinessdays.tuesday === "true" && new Date.create(e).getDay() === 2) || ($scope.nonbusinessdays.wednesday === "true" && new Date.create(e).getDay() === 3) || ($scope.nonbusinessdays.thursday === "true" && new Date.create(e).getDay() === 4) || ($scope.nonbusinessdays.friday === "true" && new Date.create(e).getDay() === 5) || ($scope.nonbusinessdays.saturday === "true" && new Date.create(e).getDay() === 6));
                        });
                        if (Temp_holiday_exist != null) {
                            if (Temp_holiday_exist.length > 0 || nbcount > 0) {
                                return (holiday_exist_min.length - Temp_holiday_exist.length) + nbcount;
                            } else {
                                return 0;
                            }
                        }
                    }
                }
            }
        }

        function getDates(startDate, stopDate) {
            var dateArray = new Array();
            var currentDate = startDate;
            while (currentDate <= stopDate) {
                dateArray.push(new Date(currentDate))
                currentDate = currentDate.addDays(1);
            }
            return dateArray;
        }
        $scope.$on("$destroy", function () {
            $timeout.cancel(mytasktimer);
            RecursiveUnbindAndRemove($("[ng-controller='mui.mytaskCtrl']"));
        });
        $scope.set_color = function (clr) {
            if (clr != null) return {
                'background-color': "#" + clr.toString().trim()
            };
            else return '';
        }

        //mytask paging concept STARTS HERE

        $scope.$on('RefreshTaskByID', function ($event, taskID) {

            $scope.ComplexTaskDataStructure = { Header: '', body: '' };
            $scope.PageNo = 1;
            $scope.Total_rows = 0;
            GetMyTaskLists();
        });
        function gettingCustomFilterValues() {
            $scope.ClientCustomStatus = [];
            $scope.CustomFilterCaption = "";
            //$('.multiselect').addClass('dropdown-toggle').removeClass('multiselect');
            setTimeout(function () { $(".btn.multiselect").attr("style", "min-width:100px"); }, 100);
            var ID = 91;
            MyTaskService.AttributeDetailsbyID(ID).then(function (Attrdetails) {
                if (Attrdetails.Response != null) {
                    $scope.CustomFilterCaption = Attrdetails.Response[0].Caption;
                    MyTaskService.GetAdminOptionListID(ID).then(function (optionlist) {
                        if (optionlist.Response != null)
                            $scope.ClientCustomStatus = optionlist.Response;
                    });
                }
            });
           
        }


    }
    app.controller("mui.mytaskCtrl", ['$scope', '$cookies', '$resource', '$compile', '$window', '$translate', '$timeout', '$location', 'MyTaskService', '$modal', muimytaskCtrl]);
})(angular, app);