﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace Presentation.Handlers
{
    /// <summary>
    /// Summary description for ReportImagePicHandler
    /// </summary>
    public class ReportImagePicHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            Decimal chunk = context.Request["chunk"] != null ? Decimal.Parse(context.Request["chunk"]) : 0;
            string fileName = context.Request["name"] != null ? context.Request["name"] : string.Empty;

            //TODO: change as needed for your application
            string uploadPath = context.Server.MapPath("~/Files/ReportFiles/Images/Temp/");
            //HttpContext.Current.Request.Files[0];
            HttpPostedFile fileUpload = HttpContext.Current.Request.Files[0];

            fileName = Guid.NewGuid().ToString() + new FileInfo(fileName).Extension;

            using (var fs = new FileStream(Path.Combine(uploadPath, fileName), chunk == 0 ? FileMode.Create : FileMode.Append))
            {
                var buffer = new byte[fileUpload.InputStream.Length];
                fileUpload.InputStream.Read(buffer, 0, buffer.Length);

                fs.Write(buffer, 0, buffer.Length);
            }


            System.Drawing.Image objImage = System.Drawing.Image.FromFile(Path.Combine(uploadPath, fileName));
            var width = objImage.Width.ToString();
            var height = objImage.Height.ToString();


            context.Response.Write(fileName + ',' + width + ',' + height);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}