﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Dam.Interface
{
    public interface IDamTypeAttributeRelation
    {

        #region Public Properties

        int ID
        {
            get;
            set;
        }

        int SortOrder
        {
            get;
            set;

        }
        int TypeID
        {
            get;
            set;

        }
        string Caption
        {
            get;
            set;
        }


        dynamic Options
        {
            get;
            set;
        }



        string DefaultValue
        {
            get;
            set;
        }
        #endregion
    }
}
