﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using DevExpress.ReportServer.ServiceModel.Client;
using DevExpress.ReportServer.ServiceModel.DataContracts;
using System.Web.Services;
using BrandSystems.Marcom.Utility;
using BrandSystems.Marcom.Core.Managers.Proxy;
using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.Interface;
using System.Collections;
using System.ServiceModel;
using Presentation.Utility;
namespace Presentation.External
{
    public partial class HistoryReport : System.Web.UI.Page
    {
        protected string LogoutAction { get; set; }
        protected string UserName { get; set; }
        protected string UserPassword { get; set; }
        protected string UserRole { get; set; }
        protected string LoginUrl { get; set; }
        protected string AfterLoginUrl { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.Form["TaskID"] != null)
                {
                    int DalimID = 0;
                    TenantResolver tenantres = new TenantResolver(System.Web.HttpContext.Current.Request.Url.Host.ToString());
                    Guid userSession = MarcomManagerFactory.GetSystemSession(tenantres.tenantID);
                    IMarcomManager managers = MarcomManagerFactory.GetMarcomManager(null, userSession);
                    Dictionary<string, string> externalInfo = new Dictionary<string, string>();

                    externalInfo = managers.ExternalTaskManager.GetDocumentViewerInfo(Convert.ToInt32(Request.Form["TaskID"]), Convert.ToString(Request.Form["UserEmail"]));
                    foreach (KeyValuePair<string, string> obj in externalInfo)
                    {
                        if (obj.Key == "DalimID")
                        {
                            DalimID = Convert.ToInt32(obj.Value);
                        }
                        else if (obj.Key == "userprofile")
                        {
                            UserRole = obj.Value.ToString();
                        }
                        else if (obj.Key == "externalUrl")
                        {
                            LogoutAction = obj.Value.ToString() + "Esprit/public/Login.jsp?DDMSAction=LogoutAction";
                            LoginUrl = obj.Value.ToString() + "Esprit/public/Login.jsp";
                            if (Convert.ToString(Request.Form["ReportType"]).ToUpper() == "HISTORYREPORT")
                                AfterLoginUrl = obj.Value.ToString() + "Esprit/public/Interface/historyReport/" + DalimID;
                            else if (Convert.ToString(Request.Form["ReportType"]).ToUpper() == "NOTEREPORT")
                                AfterLoginUrl = obj.Value.ToString() + "Esprit/public/Interface/noteReport/" + DalimID;
                        }
                        else if (obj.Key == "UserName")
                        {
                            UserName = Convert.ToString(Request.Form["UserEmail"]);
                        }
                        else if (obj.Key == "Password")
                        {
                            UserPassword = obj.Value.ToString();
                        }
                    }
                }
            }
            catch { }
        }
    }
}