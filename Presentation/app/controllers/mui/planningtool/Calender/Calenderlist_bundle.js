///#source 1 1 /app/controllers/mui/planningtool/calender/list-controller.js
(function(ng, app) {
    "use strict";

    function muiplanningtoolcalenderlistCtrl($scope, $timeout, $http, $compile, $resource, $location, $window, $cookies, $cookieStore, $translate, CalenderlistService, $modal) {
        $scope.Isfromcalender_mui = true;
        $scope.PageSize = 1;
        $scope.IsSelectAllChecked = false;
        $scope.CalenderisEdit = $('#content').attr('data-calendereditpermission');
        $scope.CalenderisEdit_mui = $('#content').attr('data-calendereditpermission');
        $scope.appliedfilter = "No filter applied";
        $scope.SelectedFilterID = 0;
        $scope.AddEditFilter = "Add new filter";
        CalenderlistService.GetFilterSettings(10).then(function(filterSettings) {
            $scope.filterValues = filterSettings.Response;
        });
        $scope.DetailFilterCreation = function(event) {
            if ($scope.SelectedFilterID == 0) {
                $('.FilterHolder').slideDown("slow");
                $scope.deletefiltershow = false;
                $("#rootlevelfilter").trigger('ClearScope', 10);
            } else {
                $("#rootlevelfilter").trigger('EditFilterSettingsByFilterID', $scope.SelectedFilterID);
                $scope.showSave = false;
                $scope.showUpdate = true;
                $('.FilterHolder').slideDown("slow");
            }
        };
        $scope.hideFilterSettings = function() {
            $('.FilterHolder').slideUp("slow")
            $timeout(function() {
                $(window).AdjustHeightWidth();
            }, 500);
        }
        $scope.ApplyFilter = function(filterid, filtername) {
            $scope.SelectedFilterID = filterid;
            if (filterid != 0) {
                $scope.AddEditFilter = 'Edit filter';
                $scope.appliedfilter = filtername;
                $('.FilterHolder').slideUp("slow")
            } else {
                $scope.appliedfilter = filtername;
                $('.FilterHolder').slideUp("slow")
                $("#rootlevelfilter").trigger('ClearScope', 10);
                $scope.AddEditFilter = $translate.instant('LanguageContents.Res_402.Caption');
            }
            $("#rootlevelfilter").trigger('ApplyRootLevelFilter', [filterid, filtername]);
        }
        $("#Div_Objlist").on('ClearAndReApply', function() {
            $scope.appliedfilter = $translate.instant('LanguageContents.Res_401.Caption');
            $scope.SelectedFilterID = 0;
            $scope.AddEditFilter = $translate.instant('LanguageContents.Res_402.Caption');
        });
        $("#Div_Objlist").on('ReloadFilterSettings', function(event, TypeID, filtername, filterid) {
            $scope.SelectedFilterID = filterid;
            if (filtername != '') {
                $("#rootlevelfilter").trigger('EditFilterSettingsByFilterID', $scope.SelectedFilterID);
                $scope.appliedfilter = filtername;
                $scope.AddEditFilter = 'Edit filter';
            }
            if (filtername == $translate.instant('LanguageContents.Res_401.Caption')) {
                $scope.appliedfilter = filtername;
                $scope.AddEditFilter = $translate.instant('LanguageContents.Res_401.Caption');
                $scope.SelectedFilterID = 0;
            }
            CalenderlistService.GetFilterSettings(TypeID).then(function(filterSettings) {
                $scope.filterValues = filterSettings.Response;
            });
        });
        $scope.Level = "0";
        var CurrentuserId = $scope.UserId;
        if (ShowAllRemember.Objective == true) {
            $scope.DisablePart = true;
            CurrentuserId = $scope.UserId;
            $("#btnParticipated").removeClass("active").addClass("active");
            $("#btnViewAll").removeClass("active");
        } else {
            $scope.DisablePart = false;
            CurrentuserId = 0;
            $("#btnViewAll").removeClass("active").addClass("active");
            $("#btnParticipated").removeClass("active");
        }
        $scope.tglHide = true;
        $scope.ShowAll = function(IsActive) {
            $scope.DisablePart = !$scope.DisablePart;
            $("#ListContainer table").html('');
            $("#treeHolder ul").html('');
            $('#RootLevelSelectAll').next('i').removeClass('checked');
            ClearCheckBoxes();
            if (ShowAllRemember.Objective == true) {
                CurrentuserId = $scope.UserId
            }
            if (IsActive) {
                $("#btnViewAll").removeClass("active").addClass("active");
                $("#btnParticipated").removeClass("active");
                CurrentuserId = 0;
                ShowAllRemember.Objective = false;
            } else {
                $("#btnParticipated").removeClass("active").addClass("active");
                $("#btnViewAll").removeClass("active");
                CurrentuserId = $scope.UserId;
                ShowAllRemember.Objective = true;
            }
            GetRootLevelActivityListCount();
            if ($("#ListColumn > thead tr th").length == 1)
                $('#ListColumn > thead').html("");
        };

        function ClearCheckBoxes() {
            $scope.IsSelectAllChecked = false;
            $('#ListContainer > table > tbody input:checkbox').each(function() {
                this.checked = false;
                $(this).next('i').removeClass('checked');
            });
        }
        $scope.EntityTypeID = 10;
        var TypeID = 10;
        $scope.selectedFilterID = 0;
        $scope.FilterID = {
            selectedFilterID: 0
        };
        $scope.AppllyFilterObj = {
            selectedattributes: []
        };
        $scope.onTreeDataMouseEnter = function() {
            var id = this.item.Id;
            setTimeout('$(\'#treeHolder > li > a[data-id="' + id + '"]\').addClass(\'hover\');', 50);
        };
        $scope.onTreeDataMouseLeave = function() {
            var id = this.item.Id;
            setTimeout('$(\'#treeHolder > li > a[data-id="' + id + '"]\').removeClass(\'hover\');', 50);
        };
        $scope.onTreeMouseEnter = function() {
            var id = this.item.Id;
            setTimeout('$(\'#listdataHolder > tr[data-id="' + id + '"]\').addClass(\'hover\');', 50);
        };
        $scope.onTreeMouseLeave = function() {
            var id = this.item.Id;
            setTimeout('$(\'#listdataHolder > tr[data-id="' + id + '"]\').removeClass(\'hover\');', 50);
        };
        $scope.listColumnDefsdata = {};
        $scope.NameExcludeFilter = function(columndefs) {
            if (columndefs.Field != "Name") {
                return true;
            }
        };
        $scope.load = function(parameters) {};
        $scope.DeleteEntity = function() {
            var IDList = new Array();
            var ID = 0;
            IDList = GetRootLevelSelectedAll();
            if (IDList.length != 0) {
                var object = {};
                object.ID = IDList;
                bootbox.confirm($translate.instant('LanguageContents.Res_16.Caption'), function(result) {
                    if (result) {
                        $timeout(function() {
                            CalenderlistService.DeleteEntity(object).then(function(data) {
                                if (data.Response == true) {
                                    for (var i = 0; i < IDList.length; i++) {
                                        var entityId = IDList[i];
                                        $('#treeHolder a[data-id=' + entityId + ']').remove();
                                        $('#ListContainer tr[data-id=' + entityId + ']').remove()
                                    }
                                    $('#RootLevelSelectAll').next('i').removeClass('checked');
                                    ClearCheckBoxes()
                                }
                            });
                        }, 100);
                    }
                });
            }
        };

        function SelectEntityIDs() {
            var IDList = new Array();
            $('#listdataHolder input:checked').each(function() {
                IDList.push($(this).parents('tr').attr('data-id'));
            });
            return IDList
        }
        $scope.sortOrder = "null";
        $scope.IsDesc = false;
        var IsDesc = false;
        var sortOrder = "";
        $scope.LoadRootLevelCalender = function(PageIndex, StartRowNo, MaxNoofRow, filterid, filterattributes) {
            $(window).AdjustHeightWidth();
            var FilterID = filterid;
            if (RememberObjFilterID != 0 || RememberObjFilterAttributes.length != 0) {
                FilterID = RememberObjFilterID;
                $scope.FilterID.selectedfilterattribtues = RememberObjFilterAttributes;
                if (RememberObjFilterAttributes.length > 0) {
                    $timeout(function() {
                        $("#rootlevelfilter").trigger('RemeberApplyFilterReLoad', [10, RememberObjFilterAttributes])
                    }, 10);
                }
                if (RememberObjFilterID != 0) {
                    $scope.appliedfilter = RemeberObjFilterName;
                    $scope.AddEditFilter = 'Edit filter';
                    $scope.SelectedFilterID = $scope.FilterID.selectedFilterID;
                }
            }
            var FilterObj = filterattributes;
            $window.ObjectiveFilterName = '';
            var getactivities = {};
            getactivities.StartRowNo = StartRowNo;
            getactivities.MaxNoofRow = MaxNoofRow;
            getactivities.FilterID = FilterID;
            getactivities.SortOrderColumn = $scope.sortOrder;
            getactivities.IsDesc = $scope.IsDesc;
            getactivities.IncludeChildren = false;
            getactivities.EntityID = '0';
            getactivities.UserID = CurrentuserId;
            getactivities.Level = $scope.Level;
            getactivities.IDArr = [];
            getactivities.FilterAttributes = FilterObj;
            CalenderlistService.CalenderRootLevel(getactivities).then(function(getactivitylist) {
                if (getactivitylist.Response != null && getactivitylist.Response.ColumnDefs.length > 0) {
                    var listColumnDefsdata = getactivitylist.Response.ColumnDefs;
                    var listContent = getactivitylist.Response.Data;
                    var contentnHtml = "";
                    var columnHtml = "<tr >";
                    var treeHtml = "";
                    var colStatus = false;
                    for (var i = 0; i < listContent.length; i++) {
                        var datStartUTCval = "";
                        var todayDate = new Date();
                        var checkDate = new Date();
                        datStartUTCval = listContent[i].PublishedOn.substr(6, (listContent[i].PublishedOn.indexOf('+') - 6));
                        var datstartval = new Date(parseInt(datStartUTCval));
                        switch (parseInt(listContent[i].Visibilitytype)) {
                            case 0:
                                checkDate = datstartval.addDays(parseInt(listContent[i].Visibilityperiod));
                                break;
                            case 1:
                                checkDate = datstartval.addWeeks(parseInt(listContent[i].Visibilityperiod));
                                break;
                            case 2:
                                checkDate = datstartval.addMonths(parseInt(listContent[i].Visibilityperiod));
                                break;
                            case 3:
                                checkDate = datstartval.addYears(parseInt(listContent[i].Visibilityperiod));
                                break;
                        }
                        if (checkDate >= todayDate || checkDate != todayDate) {
                            if (i != undefined) {
                                contentnHtml += "<tr data-over='true' class='ng-scope mo" + listContent[i]["Id"] + "' data-id=" + listContent[i]["Id"] + ">"
                                for (var j = 0; j < listColumnDefsdata.length; j++) {
                                    if (j != undefined && listColumnDefsdata[j].Field != "68") {
                                        if (colStatus == false) {
                                            columnHtml += "<th>";
                                            columnHtml += "<a  data-Column=" + listColumnDefsdata[j].Field + " >";
                                            columnHtml += "    <span>" + listColumnDefsdata[j].DisplayName + "</span>";
                                            columnHtml += "</a>";
                                            columnHtml += "</th>";
                                        }
                                        if (AttributeTypes.Period == listColumnDefsdata[j].Type)
                                            contentnHtml += "<td><span class='ng-binding'>" + (listContent[i]["TempPeriod"] != undefined ? listContent[i]["TempPeriod"] : "-") + "</span></td>";
                                        else if (AttributeTypes.DateTime == listColumnDefsdata[j].Type)
                                            contentnHtml += "<td><span class='ng-binding'>" + (listContent[i][listColumnDefsdata[j].Field] == null ? "-" : dateFormat(listContent[i][listColumnDefsdata[j].Field], $scope.DefaultSettings.DateFormat)) + "</span></td>";
                                        else
                                            contentnHtml += "<td><span class='ng-binding'>" + (listContent[i][listColumnDefsdata[j].Field] != undefined ? listContent[i][listColumnDefsdata[j].Field] : "-") + "</span></td>";
                                    }
                                }
                                treeHtml += "<li><a data-over='true' class='mo" + listContent[i]["Id"] + "' href='javascript:void(0)' data-id=" + listContent[i]["Id"] + "><i class='icon-'></i><span style='background-color: #" + listContent[i]["ColorCode"] + "' class='eicon-s margin-right5x' data-id=" + listContent[i]["Id"] + ">" + listContent[i]["ShortDescription"] + "</span><span class='treeItemName' data-id=" + listContent[i]["Id"] + ">" + listContent[i]["Name"] + "</span></a></li>";
                                var status = "";
                                if (listContent[i].IsLock == true) {
                                    contentnHtml += "<td><span class='pull-right'><label class='checkbox checkbox-custom '><input type='checkbox'><i class='checkbox " + ($scope.IsSelectAllChecked ? 'checked' : '') + "'></i></label></span></td>";
                                } else {
                                    contentnHtml += "<td><span class='pull-right'><label class='checkbox checkbox-custom '><input type='checkbox'><i class='checkbox " + ($scope.IsSelectAllChecked ? 'checked' : '') + "'></i></label></span></td>";
                                }
                                contentnHtml += "</tr>";
                                colStatus = true;
                            }
                        }
                    }
                    //if ($("#ListColumn > thead tr").length < 1) {
                        columnHtml += "<th><span class='pull-right'><label class='checkbox checkbox-custom pull-right'><input id='RootLevelSelectAll'  type='checkbox' ><i class='checkbox'></i></label></span></th>";
                        columnHtml += "</tr>";
                        $('#ListColumn > thead').html('');
                        $('#ListColumn > thead').html(columnHtml);
                    //}
                    if ($("#ListColumn > thead tr").length > 0) {
                        $('#ListContainer > table tbody[data-page="' + PageIndex + '"]').html(contentnHtml);
                        $('#ListContainer > table tbody[data-page="' + PageIndex + '"]').removeClass('pending').removeAttr('style');
                        $('#treeHolder  li[data-page="' + PageIndex + '"] > ul').html(treeHtml);
                        $('#treeHolder li[data-page="' + PageIndex + '"]').removeClass('pending').removeAttr('style');
                        ListCheckBoxitemClick();
                    }
                }
                $(window).AdjustHeightWidth();
            });
        }
        GetRootLevelActivityListCount();

        function GetRootLevelActivityListCount() {
            var FilterID = 0;
            $scope.noData = false;
            var ObjAttrs = $cookieStore.get('ObjAttrs' + parseInt($cookies['UserId'], 10));
            var ObjFilterID = $cookieStore.get('ObjFilterID' + parseInt($cookies['UserId'], 10));
            var ObjFilterName = $cookieStore.get('ObjFilterName' + parseInt($cookies['UserId'], 10));
            RemeberObjFilterName = ObjFilterName;
            if (ObjAttrs != undefined) {
                RememberObjFilterAttributes = ObjAttrs;
            }
            if (ObjFilterID != undefined) {
                RememberObjFilterID = ObjFilterID;
            }
            if ($scope.FilterID.selectedFilterID != undefined) {
                FilterID = $scope.FilterID.selectedFilterID;
            } else {
                FilterID = 0;
            }
            if (RememberObjFilterID != 0 || RememberObjFilterAttributes.length != 0) {
                FilterID = RememberObjFilterID;
                $scope.FilterID.selectedfilterattribtues = RememberObjFilterAttributes;
                if (RememberObjFilterAttributes.length > 0) {
                    $timeout(function() {
                        $("#rootlevelfilter").trigger('RemeberApplyFilterReLoad', [10, RememberObjFilterAttributes])
                    }, 10);
                }
                if (RememberObjFilterID != 0) {
                    $scope.appliedfilter = RemeberObjFilterName;
                    $scope.AddEditFilter = 'Edit filter';
                    $scope.SelectedFilterID = FilterID;
                }
            }
            $window.ObjectiveFilterName = '';
            var Node = {};
            Node.StartRowNo = 0;
            Node.MaxNoofRow = 20;
            Node.FilterID = FilterID;
            Node.SortOrderColumn = $scope.sortOrder;
            Node.IsDesc = $scope.IsDesc;
            Node.IncludeChildren = false;
            Node.EntityID = '0';
            Node.UserID = CurrentuserId;
            Node.Level = $scope.Level;
            Node.IDArr = [];
            Node.FilterAttributes = $scope.FilterID.selectedfilterattribtues;
            CalenderlistService.CalenderRootLevel(Node).then(function(getactivitylist) {
                if (getactivitylist.Response != null && getactivitylist.Response.Data != null) {
                    fnPageTemplate(parseInt(getactivitylist.Response.DataCount));
                    var PageIndex = 0;
                    if (getactivitylist.Response.ColumnDefs.length > 0) {
                        var listColumnDefsdata = getactivitylist.Response.ColumnDefs;
                        var listContent = getactivitylist.Response.Data;
                        var contentnHtml = "";
                        var columnHtml = "<tr >";
                        var treeHtml = "";
                        var colStatus = false;
                        for (var i = 0; i < listContent.length; i++) {
                            var datStartUTCval = "";
                            var todayDate = new Date();
                            var checkDate = new Date();
                            datStartUTCval = listContent[i].PublishedOn.substr(6, (listContent[i].PublishedOn.indexOf('+') - 6));
                            var datstartval = new Date(listContent[i].PublishedOn);
                            switch (parseInt(listContent[i].Visibilitytype)) {
                                case 0:
                                    checkDate = datstartval.addDays(parseInt(listContent[i].Visibilityperiod));
                                    break;
                                case 1:
                                    checkDate = datstartval.addWeeks(parseInt(listContent[i].Visibilityperiod));
                                    break;
                                case 2:
                                    checkDate = datstartval.addMonths(parseInt(listContent[i].Visibilityperiod));
                                    break;
                                case 3:
                                    checkDate = datstartval.addYears(parseInt(listContent[i].Visibilityperiod));
                                    break;
                            }
                            if (checkDate >= todayDate) {
                                if (i != undefined) {
                                    contentnHtml += "<tr data-over='true' class='ng-scope mo" + listContent[i]["Id"] + "' data-id=" + listContent[i]["Id"] + ">"
                                    for (var j = 0; j < listColumnDefsdata.length; j++) {
                                        if (j != undefined && listColumnDefsdata[j].Field != "68") {
                                            if (colStatus == false) {
                                                columnHtml += "<th>";
                                                columnHtml += "<a data-Column=" + listColumnDefsdata[j].Field + " >";
                                                columnHtml += "    <span>" + listColumnDefsdata[j].DisplayName + "</span>";
                                                columnHtml += "</a>";
                                                columnHtml += "</th>";
                                            }
                                            if (AttributeTypes.Period == listColumnDefsdata[j].Type)
                                                contentnHtml += "<td><span class='ng-binding'>" + (listContent[i]["TempPeriod"] != undefined ? listContent[i]["TempPeriod"] : "-") + "</span></td>";
                                            else if (AttributeTypes.DateTime == listColumnDefsdata[j].Type)
                                                contentnHtml += "<td><span class='ng-binding'>" + (listContent[i][listColumnDefsdata[j].Field] == null ? "-" : dateFormat(listContent[i][listColumnDefsdata[j].Field], $scope.DefaultSettings.DateFormat)) + "</span></td>";
                                            else
                                                contentnHtml += "<td><span class='ng-binding'>" + (listContent[i][listColumnDefsdata[j].Field] != undefined ? listContent[i][listColumnDefsdata[j].Field] : "-") + "</span></td>";
                                        }
                                    }
                                    treeHtml += "<li><a data-over='true' class='mo" + listContent[i]["Id"] + "' href='javascript:void(0)' data-id=" + listContent[i]["Id"] + "><i class='icon-'></i><span style='background-color: #" + listContent[i]["ColorCode"] + "' class='eicon-s margin-right5x' data-id=" + listContent[i]["Id"] + ">" + listContent[i]["ShortDescription"] + "</span><span class='treeItemName' data-id=" + listContent[i]["Id"] + ">" + listContent[i]["Name"] + "</span></a></li>";
                                    var status = "";
                                    if (listContent[i].IsLock == true) {
                                        contentnHtml += "<td><span class='pull-right'><label class='checkbox checkbox-custom '><input type='checkbox'><i class='checkbox'></i></label></span></td>";
                                    } else {
                                        contentnHtml += "<td><span class='pull-right'><label class='checkbox checkbox-custom '><input type='checkbox'><i class='checkbox'></i></label></span></td>";
                                    }
                                    contentnHtml += "</tr>";
                                    colStatus = true;
                                }
                            }
                        }
                        if ($("#ListColumn > thead tr").length < 3) {
                            columnHtml += "<th><span class='pull-right'><label class='checkbox checkbox-custom pull-right'><input id='RootLevelSelectAll'  type='checkbox' ><i class='checkbox'></i></label></span></th>";
                            columnHtml += "</tr>";
                            $('#ListColumn > thead').html(columnHtml);
                        }
                        if ($("#ListColumn > thead tr").length > 0) {
                            $("#ListContainer > table tbody").html('');
                            $("#treeHolder ul").html('');
                            $('#ListContainer').scrollTop(0);
                            $('#ListContainer > table tbody[data-page="' + PageIndex + '"]').html(contentnHtml);
                            $('#ListContainer > table tbody[data-page="' + PageIndex + '"]').removeClass('pending').removeAttr('style');
                            $('#treeHolder  li[data-page="' + PageIndex + '"] > ul').html(treeHtml);
                            $('#treeHolder li[data-page="' + PageIndex + '"]').removeClass('pending').removeAttr('style');
                            ListCheckBoxitemClick();
                        }
                        if ($scope.PageSize > 1) {
                            $scope.LoadRootLevelCalender(1, 20, 20, $scope.FilterID.selectedFilterID, $scope.AppllyFilterObj.selectedattributes);
                        }
                    }
                } else {
                    $scope.noData = true;
                }
                $(window).AdjustHeightWidth();
            });
        }
        var LoadedPage = 0;

        function fnPageTemplate(ItemCnt) {
            var PageSize = 1;
            var itemsPerPage = 20;
            var height = 34 * ItemCnt;
            if (ItemCnt > itemsPerPage) {
                height = 34 * itemsPerPage;
                PageSize = Math.ceil(ItemCnt / itemsPerPage);
            }
            $scope.PageSize = PageSize;
            var TreeTemplate = '';
            var ListTemplate = '';
            for (var i = 0; i <= PageSize; i++) {
                if (i != undefined) {
                    TreeTemplate += "<li class='pending' style='min-height: " + height + "px;' data-page='" + i + "'><ul class='nav nav-list'></ul></li>"
                    ListTemplate += "<tbody class='widthcent pending' style='min-height: " + height + "px;' data-page='" + i + "'></tbody>"
                }
            }
            if (TreeTemplate.length > 0) {
                $("#ListContainer > table").html(ListTemplate);
                $("#treeHolder").html(TreeTemplate);
            }
        }
        $('#ListContainer').scroll(function() {
            $("#treeHolder").scrollTop($(this).scrollTop());
            var areaHeight = $('#ListContainer').height();
            var areaTop = $('#ListContainer').position().top;
            var top = $(this).position().top - areaTop;
            var height = $('#ListContainer table').height();
            $('tbody.pending', '#ListContainer').each(function() {
                var datapage = parseInt($(this).attr('data-page'));
                if ((($(this).height() * datapage) / 3) < $("#treeHolder").scrollTop()) {
                    log("Page " + $(this).attr('data-page') + " is loaded");
                    $('#ListContainer > table tbody[data-page="' + $(this).attr('data-page') + '"]').removeClass('pending');
                    var StartRowNo = datapage * 20;
                    var MaxNoofRow = 20;
                    $(this).removeClass('pending');
                    $(this).removeAttr('style');
                    var filterattributes = [];
                    $('#treeHolder li[data-page="' + datapage + '"]').removeClass('pending');
                    $('#treeHolder li[data-page="' + datapage + '"]').removeAttr('style');
                    $scope.LoadRootLevelCalender(datapage, StartRowNo, MaxNoofRow, 0, filterattributes);
                }
            });
        });
        $("#rootLevelEntity").click(function(event) {
            $("#rootLevelEntity").trigger("onRootObjecitveCreation", [$scope.EntityTypeID])
        });
        $window.ListofEntityID = []
        $("#treeHolder").click(function(e) {
            var TargetControl = $(e.target);
            var id = $(e.target).attr("data-id");
            if (id != undefined) {
                ViewRootLevelEntity(id, e);
            }
            e.preventDefault();
        });
        $(document).on('click', '.checkbox-custom > input[id=RootLevelSelectAll]', function(e) {
            var status = this.checked;
            $('#ListContainer > table > tbody input:checkbox').each(function() {
                this.checked = status;
                if (status) {
                    $scope.IsSelectAllChecked = true;
                    $(this).next('i').addClass('checked');
                } else {
                    $scope.IsSelectAllChecked = false;
                    $(this).next('i').removeClass('checked');
                }
            });
        });
        $("#ViewSelect").click(function(event) {
            ViewRootLevelEntity(null, event)
        });

        function GetRootLevelSelectedAll() {
            var IDList = new Array();
            $('#ListContainer > table > tbody input:checked').each(function() {
                IDList.push($(this).parents('tr').attr('data-id'));
            });
            return IDList
        }
        $("#ListContainer").click(function (e) {
            var IDList = new Array();
            IDList = GetRootLevelSelectedAll();
            if (IDList.length == $("#treeHolder").find(".nav-list").find("li").length) {
                $('#RootLevelSelectAll').next('i').addClass('checked');
            }
        });
        function ListCheckBoxitemClick() {
            $('#ListContainer > table > tbody input:checkbox').click(function() {
                $("#RootLevelSelectAll").next().removeClass('checked')
                $scope.IsSelectAllChecked = false;
            });
        }

        function ViewRootLevelEntity(ID, event) {
            var IDList = new Array();
            if ($scope.IsSelectAllChecked == false) {
                if (ID != null) {
                    IDList.push(ID);
                    LoadDetailPart(event, IDList);
                } else {
                    IDList = GetRootLevelSelectedAll();
                    LoadDetailPart(event, IDList);
                }
            } else {
                $scope.IsSelectAllChecked = false;
                CalenderlistService.GetAllEntityIds().then(function(Entityresult) {
                    if (Entityresult.Response != null) {
                        IDList = Entityresult.Response;
                        LoadSelectAllDetailPart(event, IDList);
                    }
                });
            }
        }

        function LoadDetailPart(event, IDList) {
            if (IDList.length !== 0) {
                $window.ListofEntityID = [];
                $window.ListofEntityID = IDList;
                var TrackID = CreateHisory(IDList);
                event.preventDefault();
                var localScope = $(event.target).scope();
                if ($("#ViewSelect").attr('data-viewtype') == "listview") {
                    $location.path("mui/calender/detail/listview/" + TrackID);
                } else {
                    $location.path("mui/calender/detail/ganttview/" + TrackID);
                }
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_1806.Caption'));
            }
        }

        function LoadSelectAllDetailPart(event, IDList) {
            if (IDList.length !== 0) {
                $window.ListofEntityID = [];
                $window.ListofEntityID = IDList;
                var TrackID = CreateHisory(IDList);
                if ($("#ViewSelect").attr('data-viewtype') == "ganttview") {
                    $location.path("mui/calender/detail/ganttview/" + TrackID);
                } else {
                    $location.path("mui/calender/detail/listview/" + TrackID);
                }
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_1806.Caption'));
            }
        }
        $scope.CalendersList = [];

        function GetCalenders() {
            CalenderlistService.GetCalenders().then(function(result) {
                if (result.Response != null && result.Response != undefined) {
                    $scope.CalendersList = result.Response;
                }
            });
        }

        function GetEntitiesforSelectedCalender(calenderID, event) {
            CalenderlistService.GetEntitiesforSelectedCalender(calenderID).then(function(result) {
                if (result.Response != null && result.Response != undefined) {
                    var entityIDList = new Array();
                    entityIDList = result.Response;
                    LoadDetailPart(event, entityIDList);
                }
            });
        }
        $scope.OpenCalenderCreationPopup = function () {
            var modalInstance = $modal.open({
                templateUrl: 'views/mui/planningtool/calender/calendercreation.html',
                controller: "mui.planningtool.calender.calendercreationCtrl",
                resolve: {
                    params: function () {
                        return {
                        };
                    }
                },
                scope: $scope,
                windowClass: 'newCalenderModal popup-widthM',
                backdrop: "static"
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () { });
        }

        $scope.$on("$destroy", function() {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.calender.listCtrl']"));
        });
        $timeout(function() {}, 0);
        $scope.visible = false;
    }
    app.controller("mui.planningtool.calender.listCtrl", ['$scope', '$timeout', '$http', '$compile', '$resource', '$location', '$window', '$cookies', '$cookieStore', '$translate', 'CalenderlistService', '$modal', muiplanningtoolcalenderlistCtrl]);
})(angular, app);
///#source 1 1 /app/controllers/mui/planningtool/calender/detail/filtersettings-controller.js
(function(ng,app){"use strict";function muiplanningtoolcalenderdetailfiltersettingsCtrl($scope,$location,$resource,$timeout,$cookies,$compile,$cookieStore,$translate,CalenderfiltersettingsService){var IsSave=0;var IsUpdate=0;$scope.showSave=true;$scope.showUpdate=false;$scope.appliedfilter="No filter applied";$scope.deletefiltershow=false;$scope.ClearFilterAttributes=function(){$scope.ClearFieldsAndReApply();}
var rememberfilter=[];$scope.appliedfilter=$translate.instant('LanguageContents.Res_401.Caption');$scope.EntityTypeID=SystemDefinedEntityTypes.Objective;$scope.visible=false;$scope.filterValues=[];$scope.atributesRelationList=[];$scope.filterSettingValues=[];$scope.ObjectiveOwners={};$scope.Savefilter=true;$scope.ObjectiveFilter={ngObjectivefilterStatus:'',ngObjectivefilterOwner:[],ngObjectiveType:[],ngObjectivefilterSaveAs:'',ngObjectivefilterKeyword:''}
$scope.objectivefiltercreation=function(event){$scope.ClearScopeModle();$scope.Deletefilter=false;$scope.Applyfilter=true;$scope.Updatefilter=false;$scope.Savefilter=true;$scope.ngsaveasfilter='';$scope.ngKeywordtext='';IsSave=0;IsUpdate=0;};$("#rootlevelfilter").on("ClearScope",function(event){$scope.showSave=true;$scope.showUpdate=false;$scope.deletefiltershow=false;$scope.ClearScopeModle();});$("#rootlevelfilter").on('ApplyRootLevelFilter',function(event,filterid,filtername){$scope.ApplyFilter(filterid,filtername);});$("#rootlevelfilter").on('EditFilterSettingsByFilterID',function(event,filterid){$scope.showSave=false;$scope.showUpdate=true;$scope.deletefiltershow=true;$scope.LoadFilterSettingsByFilterID(event,filterid);});$scope.ClearScopeModle=function(){$scope.ngsaveasfilter="";$("#saveFilter").removeAttr('disabled');$scope.ObjectiveFilter.ngObjectivefilterOwner=[],$scope.ObjectiveFilter.ngObjectiveType=[],$scope.ObjectiveFilter.ngObjectivefilterSaveAs='',$scope.ObjectiveFilter.ngObjectivefilterKeyword=''
$scope.ObjectiveFilter.ngObjectivefilterStatus=[];}
$scope.ClearFieldsAndReApply=function(){$scope.ngsaveasfilter="";$("#saveFilter").removeAttr('disabled');$scope.ObjectiveFilter.ngObjectivefilterOwner=[],$scope.ObjectiveFilter.ngObjectiveType=[],$scope.ObjectiveFilter.ngObjectivefilterSaveAs='',$scope.ObjectiveFilter.ngObjectivefilterKeyword=''
$scope.ObjectiveFilter.ngObjectivefilterStatus=[];var StartRowNo=0;var MaxNoofRow=20;var PageIndex=0;var filterattr=[];$scope.showSave=true;$scope.showUpdate=false;$scope.deletefiltershow=false;RememberObjFilterID=0;RememberObjFilterAttributes=[];$cookieStore.remove('ObjAttrs'+parseInt($cookies['UserId'],10));$cookieStore.remove('ObjFilterID'+parseInt($cookies['UserId'],10));$cookieStore.remove('ObjFilterName'+parseInt($cookies['UserId'],10));RemeberObjFilterName="No filter applied";$("#Div_Objlist").trigger('ClearAndReApply');if($scope.EntityTypeID==10){$scope.LoadRootLevelObjective(PageIndex,StartRowNo,MaxNoofRow,0,filterattr)}}
CalenderfiltersettingsService.GetAllObjectiveMembers(parseInt($scope.EntityTypeID,10)).then(function(GettingObjectiveOwnersObj){var GettingObjectiveOwnersResult=GettingObjectiveOwnersObj.Response;$scope.ObjectiveOwners=GettingObjectiveOwnersObj.Response;});CalenderfiltersettingsService.GetFilterSettings(parseInt($scope.EntityTypeID,10)).then(function(filterSettings){$scope.filterValues=filterSettings.Response;});$timeout(function(){CalenderfiltersettingsService.GetFilterSettings(parseInt($scope.EntityTypeID,10)).then(function(filterSettings){$scope.filterValues=filterSettings.Response;});},100);$scope.FilterSave=function(){if(IsSave==1){return false;}
IsSave=1;if($scope.ObjectiveFilter.ngObjectivefilterSaveAs==''||$scope.ObjectiveFilter.ngObjectivefilterSaveAs==undefined){bootbox.alert($translate.instant('LanguageContents.Res_1902.Caption'));return false;}
var filterattributes=[];var FilterData={};var StartRowNo=0;var MaxNoofRow=20;var PageIndex=0;$("#saveFilter").attr('disabled','disabled');var FilterData={};FilterData.FilterId=0;FilterData.FilterName=$scope.ObjectiveFilter.ngObjectivefilterSaveAs;FilterData.Keyword=$scope.ObjectiveFilter.ngObjectivefilterKeyword;FilterData.UserId=1;FilterData.TypeID=parseInt($scope.EntityTypeID,10);FilterData.entityTypeId='';FilterData.StarDate='';FilterData.EndDate='';FilterData.IsDetailFilter=0;FilterData.WhereConditon=whereConditionData;var whereConditionData=[];var multiSelectVal=[];var usersVal=[];var orgLevel=[];if($scope.ObjectiveFilter.ngObjectivefilterOwner.length>0){for(var k=0;k<$scope.ObjectiveFilter.ngObjectivefilterOwner.length;k++){whereConditionData.push({'AttributeID':SystemDefiendAttributes.Owner,'SelectedValue':$scope.ObjectiveFilter.ngObjectivefilterOwner[k],'Level':0,'AttributeTypeId':AttributeTypes.Owner});}}
if($scope.ObjectiveFilter.ngObjectiveType.length>0){for(var k=0;k<$scope.ObjectiveFilter.ngObjectiveType.length;k++){whereConditionData.push({'AttributeID':SystemDefiendAttributes.ObjectiveType,'SelectedValue':$scope.ObjectiveFilter.ngObjectiveType[k],'Level':0,'AttributeTypeId':AttributeTypes.ObjectiveType});}}
if($scope.ObjectiveFilter.ngObjectivefilterStatus.length>0){for(var k=0;k<$scope.ObjectiveFilter.ngObjectivefilterStatus.length;k++){whereConditionData.push({'AttributeID':SystemDefiendAttributes.ObjectiveStatus,'SelectedValue':$scope.ObjectiveFilter.ngObjectivefilterStatus[k],'Level':0,'AttributeTypeId':AttributeTypes.ObjectiveType});}}
CalenderfiltersettingsService.InsertFilterSettings(FilterData).then(function(filterSettingsInsertresult){if(filterSettingsInsertresult.StatusCode==405){NotifyError($translate.instant('LanguageContents.Res_4283.Caption'));IsSave=0;}else{NotifySuccess($translate.instant('LanguageContents.Res_4401.Caption'));IsSave=0;$("#Div_Objlist").trigger('ReloadFilterSettings',[parseInt($scope.EntityTypeID,10),$scope.ObjectiveFilter.ngObjectivefilterSaveAs,filterSettingsInsertresult.Response]);filterSettingsInsertresult.WhereConditon=[];$scope.FilterID.selectedFilterID=filterSettingsInsertresult.Response;RememberObjFilterID=$scope.FilterID.selectedFilterID;RememberObjFilterAttributes=filterattributes;RemeberObjFilterName=filterSettingsInsertresult.FilterName;$cookieStore.put('ObjAttrs'+parseInt($cookies['UserId'],10),filterattributes);$cookieStore.put('ObjFilterID'+parseInt($cookies['UserId'],10),$scope.FilterID.selectedFilterID);$cookieStore.put('ObjFilterName'+parseInt($cookies['UserId'],10),filterSettingsInsertresult.FilterName);if($scope.EntityTypeID==10){$scope.LoadRootLevelObjective(PageIndex,StartRowNo,MaxNoofRow,$scope.FilterID.selectedFilterID,filterattributes)}}});var multiSelectVal=[];var usersVal=[];var orgLevel=[];};$scope.LoadFilterSettingsByFilterID=function(event,filterId){$scope.Updatefilter=false;$scope.Applyfilter=false;$scope.Deletefilter=false;$scope.Savefilter=false;$scope.filterSettingValues=[];$scope.FilterID.selectedFilterID=filterId;CalenderfiltersettingsService.GetFilterSettingValuesByFilertId($scope.FilterID.selectedFilterID).then(function(filterSettingsValues){$scope.filterSettingValues=filterSettingsValues.Response;for(var i=0;i<$scope.filterSettingValues.FilterValues.length;i++){if($scope.filterSettingValues.FilterValues[i].AttributeId==SystemDefiendAttributes.Owner&&$scope.filterSettingValues.FilterValues[i].AttributeTypeId==3)$scope.ObjectiveFilter.ngObjectivefilterOwner.push($scope.filterSettingValues.FilterValues[i].Value);else if($scope.filterSettingValues.FilterValues[i].AttributeId==SystemDefiendAttributes.ObjectiveType)$scope.ObjectiveFilter.ngObjectiveType.push($scope.filterSettingValues.FilterValues[i].Value);else if($scope.filterSettingValues.FilterValues[i].AttributeId==SystemDefiendAttributes.ObjectiveStatus)$scope.ObjectiveFilter.ngObjectivefilterStatus.push($scope.filterSettingValues.FilterValues[i].Value);}
$scope.ObjectiveFilter.ngObjectivefilterKeyword=$scope.filterSettingValues.Keyword;$scope.ObjectiveFilter.ngObjectivefilterSaveAs=$scope.filterSettingValues.FilterName;$scope.Updatefilter=true;$scope.Applyfilter=false;$scope.Deletefilter=true;$scope.Savefilter=false;});event.stopImmediatePropagation();event.stopPropagation();}
$scope.FilterUpdate=function(){if(IsUpdate==1){return false;}
IsUpdate=1;$scope.filterSettingValues.Keyword='';$scope.filterSettingValues.FilterName='';var FilterData={};var whereConditionData=[];var multiSelectVal=[];var usersVal=[];var orgLevel=[];var filterattributes=[];if($scope.ObjectiveFilter.ngObjectivefilterOwner.length>0){for(var k=0;k<$scope.ObjectiveFilter.ngObjectivefilterOwner.length;k++){whereConditionData.push({'AttributeID':SystemDefiendAttributes.Owner,'SelectedValue':$scope.ObjectiveFilter.ngObjectivefilterOwner[k],'Level':0,'AttributeTypeId':AttributeTypes.Owner});}}
if($scope.ObjectiveFilter.ngObjectiveType.length>0){for(var k=0;k<$scope.ObjectiveFilter.ngObjectiveType.length;k++){whereConditionData.push({'AttributeID':SystemDefiendAttributes.ObjectiveType,'SelectedValue':$scope.ObjectiveFilter.ngObjectiveType[k],'Level':0,'AttributeTypeId':AttributeTypes.ObjectiveType});}}
if($scope.ObjectiveFilter.ngObjectivefilterStatus.length>0){for(var k=0;k<$scope.ObjectiveFilter.ngObjectivefilterStatus.length;k++){whereConditionData.push({'AttributeID':SystemDefiendAttributes.ObjectiveStatus,'SelectedValue':$scope.ObjectiveFilter.ngObjectivefilterStatus[k],'Level':0,'AttributeTypeId':AttributeTypes.ObjectiveType});}}
FilterData.FilterId=parseInt($scope.FilterID.selectedFilterID,10);FilterData.FilterName=$scope.ObjectiveFilter.ngObjectivefilterSaveAs;FilterData.Keyword=$scope.ObjectiveFilter.ngObjectivefilterKeyword;FilterData.UserId=1;FilterData.TypeID=parseInt($scope.EntityTypeID,10);FilterData.entityTypeId='';FilterData.StarDate='';FilterData.EndDate='';FilterData.WhereConditon=whereConditionData;FilterData.IsDetailFilter=0;CalenderfiltersettingsService.InsertFilterSettings(FilterData).then(function(res){if(res.StatusCode==405){NotifyError($translate.instant('LanguageContents.Res_4358.Caption'));IsUpdate=0;}else{NotifySuccess($translate.instant('LanguageContents.Res_4399.Caption'));IsUpdate=0;res.WhereConditon=[];$("#Div_Objlist").trigger('ReloadFilterSettings',[parseInt($scope.EntityTypeID,10),$scope.ObjectiveFilter.ngObjectivefilterSaveAs,res.Response]);$scope.appliedfilter=$scope.ObjectiveFilter.ngObjectivefilterSaveAs;var StartRowNo=0;var MaxNoofRow=20;var PageIndex=0;RememberObjFilterID=res.Response;RememberObjFilterAttributes=filterattributes;RemeberObjFilterName=res.FilterName;$cookieStore.put('ObjAttrs'+parseInt($cookies['UserId'],10),filterattributes);$cookieStore.put('ObjFilterID'+parseInt($cookies['UserId'],10),res.Response);$cookieStore.put('ObjFilterName'+parseInt($cookies['UserId'],10),res.FilterName);if($scope.EntityTypeID==10){$scope.LoadRootLevelObjective(PageIndex,StartRowNo,MaxNoofRow,res.Response,filterattributes)}}});var multiSelectVal=[];var usersVal=[];var orgLevel=[];};$scope.ApplyFilter=function(FilterID,FilterName){var StartRowNo=0;var MaxNoofRow=20;var PageIndex=0;if(FilterID!=undefined&&FilterName!=undefined){$scope.FilterID.selectedFilterID=FilterID;$scope.appliedfilter=FilterName;}
$scope.filterSettingValues.Keyword='';$scope.filterSettingValues.FilterName='';var whereConditionData=[];var multiSelectVal=[];var usersVal=[];var orgLevel=[];if($scope.ObjectiveFilter.ngObjectivefilterOwner.length>0){for(var k=0;k<$scope.ObjectiveFilter.ngObjectivefilterOwner.length;k++){if($scope.ObjectiveFilter.ngObjectivefilterKeyword!=''){whereConditionData.push({'AttributeID':SystemDefiendAttributes.Owner,'SelectedValue':$scope.ObjectiveFilter.ngObjectivefilterOwner[k],'Level':0,'AttributeTypeId':AttributeTypes.Owner,'Keyword':$scope.ObjectiveFilter.ngObjectivefilterKeyword});}else{whereConditionData.push({'AttributeID':SystemDefiendAttributes.Owner,'SelectedValue':$scope.ObjectiveFilter.ngObjectivefilterOwner[k],'Level':0,'AttributeTypeId':AttributeTypes.Owner,'Keyword':''});}}}
if($scope.ObjectiveFilter.ngObjectiveType.length>0){for(var k=0;k<$scope.ObjectiveFilter.ngObjectiveType.length;k++){if($scope.ObjectiveFilter.ngObjectivefilterKeyword!=''){whereConditionData.push({'AttributeID':SystemDefiendAttributes.ObjectiveType,'SelectedValue':$scope.ObjectiveFilter.ngObjectiveType[k],'Level':0,'AttributeTypeId':AttributeTypes.ObjectiveType,'Keyword':$scope.ObjectiveFilter.ngObjectivefilterKeyword});}else{whereConditionData.push({'AttributeID':SystemDefiendAttributes.ObjectiveType,'SelectedValue':$scope.ObjectiveFilter.ngObjectiveType[k],'Level':0,'AttributeTypeId':AttributeTypes.ObjectiveType,'Keyword':''});}}}
if($scope.ObjectiveFilter.ngObjectivefilterStatus.length>0){for(var k=0;k<$scope.ObjectiveFilter.ngObjectivefilterStatus.length;k++){if($scope.ObjectiveFilter.ngObjectivefilterKeyword!=''){whereConditionData.push({'AttributeID':SystemDefiendAttributes.ObjectiveType,'SelectedValue':$scope.ObjectiveFilter.ngObjectivefilterStatus[k],'Level':0,'AttributeTypeId':AttributeTypes.ObjectiveType,'Keyword':$scope.ObjectiveFilter.ngObjectivefilterKeyword});}else{whereConditionData.push({'AttributeID':SystemDefiendAttributes.ObjectiveStatus,'SelectedValue':$scope.ObjectiveFilter.ngObjectivefilterStatus[k],'Level':0,'AttributeTypeId':AttributeTypes.ObjectiveType,'Keyword':''});}}}
if(whereConditionData.length==0&&$scope.ObjectiveFilter.ngObjectivefilterKeyword!=''){whereConditionData.push({'AttributeID':0,'SelectedValue':0,'Level':0,'AttributeTypeId':0,'Keyword':$scope.ObjectiveFilter.ngObjectivefilterKeyword});}
$scope.AppllyFilterObj.selectedattributes=[];if(FilterID!=undefined){$scope.AppllyFilterObj.selectedattributes=[];}else{$scope.AppllyFilterObj.selectedattributes=whereConditionData;}
if(FilterID==undefined){FilterID=0;}
RememberObjFilterID=FilterID;RememberObjFilterAttributes=$scope.AppllyFilterObj.selectedattributes;RemeberObjFilterName=$scope.appliedfilter;$cookieStore.put('ObjAttrs'+parseInt($cookies['UserId'],10),$scope.AppllyFilterObj.selectedattributes);$cookieStore.put('ObjFilterID'+parseInt($cookies['UserId'],10),FilterID);$cookieStore.put('ObjFilterName'+parseInt($cookies['UserId'],10),$scope.appliedfilter);if($scope.EntityTypeID==10){$scope.LoadRootLevelObjective(PageIndex,StartRowNo,MaxNoofRow,FilterID,$scope.AppllyFilterObj.selectedattributes)}};$scope.filtersettingsreset=function(){var StartRowNo=0;var MaxNoofRow=20;var PageIndex=0;$scope.FilterID.selectedFilterID=0;};$scope.DeleteFilter=function DeleteFilterSettingsValue(){var StartRowNo=0;var MaxNoofRow=20;var PageIndex=0;var filterattribtues=[];bootbox.confirm($translate.instant('LanguageContents.Res_2026.Caption'),function(result){if(result){$timeout(function(){var ID=$scope.FilterID.selectedFilterID;CalenderfiltersettingsService.DeleteFilterSettings(ID).then(function(deletefilterbyFilterId){if(deletefilterbyFilterId.StatusCode==405){NotifyError($translate.instant('LanguageContents.Res_4299.Caption'));}else{NotifySuccess($translate.instant('LanguageContents.Res_4398.Caption'));$('.FilterHolder').slideUp("slow");$scope.showSave=true;$scope.showUpdate=false;$scope.ClearScopeModle();$scope.ApplyFilter(0,'No filter applied');$("#Div_Objlist").trigger('ReloadFilterSettings',[parseInt($scope.EntityTypeID,10),'No filter applied',0]);$scope.FilterID.selectedFilterID=0;$scope.AppllyFilterObj.selectedattributes=[];if($scope.EntityTypeID==10){$scope.LoadRootLevelObjective(PageIndex,StartRowNo,MaxNoofRow,0,filterattribtues)}}});},100);}});};$("#rootlevelfilter").on('RemeberApplyFilterReLoad',function(event,TypeID,RememberPlanFilterAttributes){CalenderfiltersettingsService.GetAllObjectiveMembers(parseInt($scope.EntityTypeID,10)).then(function(GettingObjectiveOwnersObj){$scope.ObjectiveFilter.ngObjectivefilterOwner=[];$scope.ObjectiveFilter.ngObjectiveType=[];$scope.ObjectiveFilter.ngObjectivefilterKeyword='';$scope.ObjectiveFilter.ngObjectivefilterStatus=[];var GettingObjectiveOwnersResult=GettingObjectiveOwnersObj.Response;$scope.ObjectiveOwners=GettingObjectiveOwnersObj.Response;$timeout(function(){RememberFilterOnReLoad(RememberPlanFilterAttributes)},10);});});function RememberFilterOnReLoad(RememberPlanFilterAttributes){if($scope.EntityTypeID==10){if(RememberPlanFilterAttributes.length!=0){$('.FilterHolder').show()
$scope.ddlParententitytypeId=[];$scope.filterSettingValues.FilterValues=RememberPlanFilterAttributes;for(var i=0;i<$scope.filterSettingValues.FilterValues.length;i++){if($scope.filterSettingValues.FilterValues[i].AttributeID==SystemDefiendAttributes.Owner&&$scope.filterSettingValues.FilterValues[i].AttributeTypeId==3)$scope.ObjectiveFilter.ngObjectivefilterOwner.push($scope.filterSettingValues.FilterValues[i].SelectedValue);else if($scope.filterSettingValues.FilterValues[i].AttributeID==SystemDefiendAttributes.ObjectiveType)$scope.ObjectiveFilter.ngObjectiveType.push($scope.filterSettingValues.FilterValues[i].SelectedValue);else if($scope.filterSettingValues.FilterValues[i].AttributeID==SystemDefiendAttributes.ObjectiveStatus)$scope.ObjectiveFilter.ngObjectivefilterStatus.push($scope.filterSettingValues.FilterValues[i].SelectedValue);}
$scope.ObjectiveFilter.ngObjectivefilterKeyword=$scope.filterSettingValues.FilterValues[0].Keyword;}}}
$scope.ObjFilterStatus=[{Id:1,FilterStatus:'Active'},{Id:0,FilterStatus:'Deactivated'}];$scope.ObjTypeList=[{Id:1,objType:'Numeric(Quantitative)'},{Id:2,objType:'Numeric(Non Quantitative)'},{Id:3,objType:'Qualitative'},{Id:4,objType:'Rating'}];$scope.$on("$destroy",function(){RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.calender.detail.filtersettingsCtrl']"));});}
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}
app.controller("mui.planningtool.calender.detail.filtersettingsCtrl",['$scope','$location','$resource','$timeout','$cookies','$compile','$cookieStore','$translate','CalenderfiltersettingsService',muiplanningtoolcalenderdetailfiltersettingsCtrl]);})(angular,app);
///#source 1 1 /app/services/calenderlist-service.js
(function(ng,app){"use strict";function CalenderlistService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({CalenderRootLevel:CalenderRootLevel,GetAllEntityIds:GetAllEntityIds,GetFilterSettings:GetFilterSettings,DeleteEntity:DeleteEntity,GetCalenders:GetCalenders,GetEntitiesforSelectedCalender:GetEntitiesforSelectedCalender});function CalenderRootLevel(formobj){var request=$http({method:"post",url:"api/Metadata/CalenderRootLevel/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetAllEntityIds(){var request=$http({method:"get",url:"api/Metadata/GetAllEntityIds/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetFilterSettings(TypeID){var request=$http({method:"get",url:"api/Planning/GetFilterSettings/"+TypeID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function DeleteEntity(formobj){var request=$http({method:"post",url:"api/Planning/DeleteEntity/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetCalenders(){var request=$http({method:"get",url:"api/planning/GetCalenders/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetEntitiesforSelectedCalender(CalID){var request=$http({method:"get",url:"api/planning/GetEntitiesforSelectedCalender/"+CalID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}}
app.service("CalenderlistService",['$http','$q',CalenderlistService]);})(angular,app);
///#source 1 1 /app/services/calenderfiltersettings-service.js
(function(ng,app){"use strict";function CalenderfiltersettingsService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({GetAllObjectiveMembers:GetAllObjectiveMembers,GetFilterSettings:GetFilterSettings,InsertFilterSettings:InsertFilterSettings,GetFilterSettingValuesByFilertId:GetFilterSettingValuesByFilertId,DeleteFilterSettings:DeleteFilterSettings});function GetAllObjectiveMembers(EntityTypeId){var request=$http({method:"get",url:"api/user/GetAllObjectiveMembers/"+EntityTypeId,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetFilterSettings(TypeID){var request=$http({method:"get",url:"api/Planning/GetFilterSettings/"+TypeID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function InsertFilterSettings(formobj){var request=$http({method:"post",url:"api/Planning/InsertFilterSettings/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetFilterSettingValuesByFilertId(FilterID){var request=$http({method:"get",url:"api/Planning/GetFilterSettingValuesByFilertId/"+FilterID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function DeleteFilterSettings(FilterId){var request=$http({method:"delete",url:"api/Planning/DeleteFilterSettings/"+FilterId,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}}
app.service("CalenderfiltersettingsService",['$http','$q',CalenderfiltersettingsService]);})(angular,app);
