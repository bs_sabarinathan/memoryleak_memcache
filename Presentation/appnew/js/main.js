﻿'use strict';

/* Controllers */

angular.module('app')
  .controller('AppCtrl', ['$scope', '$translate', '$localStorage', '$window', '$state', '$interval', 'trafficCop', 'AccessService',
    function ($scope, $translate, $localStorage, $window, $state, $interval, trafficCop, AccessService) {
        // add 'ie' classes to html
        var isIE = !!navigator.userAgent.match(/MSIE/i);
        isIE && angular.element($window.document.body).addClass('ie');
        isSmartDevice($window) && angular.element($window.document.body).addClass('smart');

        // config
        $scope.app = {
            name: 'Secret CMS',
            version: '1.0.0',
            // for chart colors
            color: {
                primary: '#7266ba',
                info: '#23b7e5',
                success: '#27c24c',
                warning: '#fad733',
                danger: '#f05050',
                light: '#e8eff0',
                dark: '#3a3f51',
                black: '#1c2b36'
            },
            settings: {
                themeID: 1,
                navbarHeaderColor: 'bg-black',
                navbarCollapseColor: 'bg-white-only',
                asideColor: 'bg-black',
                headerFixed: true,
                asideFixed: false,
                asideFolded: false,
                asideDock: false,
                container: false
            },
            user: {
                name: $.cookie('Username'),
                id: $.cookie('UserId'),
                UserEmail: $.cookie('UserEmail'),
                isShowUshp: $.cookie('Currentwebid') == 1 ? true : false
            }
        }

        // save settings to local storage
        if (angular.isDefined($localStorage.settings)) {
            $scope.app.settings = $localStorage.settings;
        } else {
            $localStorage.settings = $scope.app.settings;
        }
        $scope.$watch('app.settings', function () {
            if ($scope.app.settings.asideDock && $scope.app.settings.asideFixed) {
                // aside dock and fixed must set the header fixed.
                $scope.app.settings.headerFixed = true;
            }
            // save to local storage
            $localStorage.settings = $scope.app.settings;
        }, true);

        // angular translate
        $scope.lang = { isopen: false, iswebopen: false };
        $scope.langs = { def: 'Default' };
        $scope.webids = [];
        $scope.selectwebid = $.cookie('Currentwebid');
        $scope.selectweb = "";

        loadwebsites();
        function loadwebsites() {
            AccessService.LoadWebSites().then(function (sites) {
                if (sites.Response != null) {
                    $scope.webids = [];
                    $scope.webids = sites.Response;
                    var obj = $.grep($scope.webids, function (web) {
                        return web.web_id == $scope.selectwebid;
                    })[0];
                    $scope.selectweb = obj != null ? obj.web_name : "";

                }
            });
        }


        $scope.selectLang = $scope.langs[$translate.proposedLanguage()] || "English";
        $scope.setLang = function (langKey, $event) {
            // set the current lang
            $scope.selectLang = $scope.langs[langKey];
            // You can change the language during runtime
            $translate.use(langKey);
            $scope.lang.isopen = !$scope.lang.isopen;
        };

        $scope.setweb = function (web, $event) {
            $scope.app.user.isShowUshp = web.web_id == 1 ? true : false;
            // set the current lang
            $scope.lang.iswebopen = !$scope.lang.iswebopen;
            AccessService.updatecurrentwebid(web.web_id, $scope.app.user.id).then(function (response) {
                if (response.StatusCode == 200) {
                    $.cookie('Currentwebid', web.web_id);
                    $scope.selectwebid = $.cookie('Currentwebid');
                    $scope.selectwebid = web.web_id;
                    $scope.selectweb = web.web_name;

                }
            });
            if ($state.current.name.toLowerCase().contains("abstract")) {
                $state.go('app.dashboard');
            }

        };

        $scope.Logout = function () {

            AccessService.Logout().then(function (logout1) {
                if (logout1.Response == null) {
                    $.removeCookie("UserId");
                    $.removeCookie("Username");
                    $.removeCookie("UserImage");
                    $.removeCookie("UserEmail");
                    $.removeCookie("Session");
                    window.location.replace("login.html");
                }
            });
        };


        $scope.$watch('selectwebid', function () {

            $scope.$broadcast('webidChanged',

                $scope.selectwebid);

        });

        function GetReturnURL() {
            var returnUrl = '';
            if (window.location.hash.length) {
                returnUrl += '?ReturnURL=' + encodeURIComponent(window.location.hash);
            }
            return returnUrl;
        }


        //session validator
        $interval(function () {
            if ($.cookie("Session") === undefined) {
                $.ajax({
                    type: "GET",
                    url: APIUrl + "User/IsUserSesstionPresent",
                    contentType: "application/json",
                    async: false,
                    success: function (tempdata) {
                        if (tempdata.StatusCode == 405) {
                            bootbox.alert("Your session has been expired. We will be redirecting you to login page. Click OK to proceed.", function () {
                                $.removeCookie("UserId");
                                $.removeCookie("Username");
                                $.removeCookie("UserImage");
                                $.removeCookie("UserEmail");
                                window.location.replace("login.html" + GetReturnURL());
                            });
                        }
                    },
                    error: function (data) {
                    }
                });

            }
        }, 30000);


        // Attach the traffic cop directly to the scope so we can more easily
        // output the totals (rather than having to set up a watcher to pipe the 
        // totals out of the service and into the scope for little-to-no benefit).
        $scope.trafficCop = trafficCop;

        // We can now watch the trafficCop service to see when there are pending
        // HTTP requests that we're waiting for.
        $scope.$watch(
            function calculateModelValue() {

                return (trafficCop.pending.all);

            },
            function handleModelChange(count) {

                //console.log(
                //    "Pending HTTP count:", count,
                //    "{",
                //        trafficCop.pending.get, "GET ,",
                //        trafficCop.pending.post, "POST",
                //    "}"
                //);

            }
        );


        $scope.refreshroute = function () {
            //$window.location.reload();
        }


        function isSmartDevice($window) {
            // Adapted from http://www.detectmobilebrowsers.com
            var ua = $window['navigator']['userAgent'] || $window['navigator']['vendor'] || $window['opera'];
            // Checks for iOs, Android, Blackberry, Opera Mini, and Windows mobile devices
            return (/iPhone|iPod|iPad|Silk|Android|BlackBerry|Opera Mini|IEMobile/).test(ua);
        }




    }]);
