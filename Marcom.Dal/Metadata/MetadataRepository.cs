﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandSystems.Marcom.Dal.Base;
using NHibernate;
using System.IO;
using System.Xml.Linq;
using System.Web;
using BrandSystems.Marcom.Dal.Metadata.Model;
using System.Collections;
using System.Xml.Serialization;
using System.Xml;
using NHibernate.Criterion;
using BrandSystems.Marcom.Dal.Common;
using BrandSystems.Marcom.Dal.Common.Model;
using BrandSystems.Marcom.Dal.Access.Model;
using System.Configuration;
using BrandSystems.Marcom.Dal.Utility;


namespace BrandSystems.Marcom.Dal.Metadata
{
    public class MetadataRepository : GenericRepository
    {
        public MetadataRepository(ISessionFactory sessionFactory)
            : base(sessionFactory)
        {


        }



        public int GetMaxSortOrder<T>(string propertyName)
        {
            ISession session = _sessionFactory.GetCurrentSession();
            int max = Convert.ToInt32(session.CreateCriteria(typeof(T)).SetProjection(Projections.Max(propertyName)).UniqueResult());

            return max;
        }

        public string GetXmlPath(int versionNo, int TenantID)
        {
            string mappingfilesPath = ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + GetTenantFilePath(TenantID);
            //mappingfilesPath = mappingfilesPath + "MetadataXML\\" + @"\MetadataWorking.xml";
            mappingfilesPath = mappingfilesPath + "MetadataXML" + @"\MetadataVersion_V" + Convert.ToString(versionNo) + ".xml";

            //mappingfilesPath = @"D:\MarcomPlatform\MarcomApplication_UITesting\Presentation\MetadataXML\MetadataWorking.xml";
            //string fileName = HttpContext.Current.Server.MapPath(@"MetadataXML\MetadataVersion_V" + Convert.ToString(versionNo) + ".xml");


            if (File.Exists(mappingfilesPath))
            {
                return mappingfilesPath;
            }
            else
            {
                return null;
            }
        }

        public string GetXmlWorkingPath(bool IsCurrentVersion, int TenantID)
        {

            string mappingfilesPath = ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + GetTenantFilePath(TenantID);
            if (IsCurrentVersion)
                mappingfilesPath = mappingfilesPath + "MetadataXML" + @"\CurrentMetadataWorking.xml";
            else
                mappingfilesPath = mappingfilesPath + "MetadataXML" + @"\FutureMetadataWorking.xml";
            if (File.Exists(mappingfilesPath))
                return mappingfilesPath;
            else
                return null;
        }

        public string GetSynchXMLPath(bool IsCurrentVersion, int TenantID)
        {
            string mappingfilesPath = ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + GetTenantFilePath(TenantID);
            if (IsCurrentVersion)
                mappingfilesPath = mappingfilesPath + "MetadataXML" + @"\CurrentSyncDBXML.xml";
            else
                mappingfilesPath = mappingfilesPath + "MetadataXML" + @"\FutureSyncDBXML.xml";
            if (File.Exists(mappingfilesPath))
                return mappingfilesPath;
            else
                return null;
        }

        public IList<T> GetObject<T>(XDocument xDoc)
        {
            try
            {
                //if (File.Exists(xmlpath))
                //{

                //// the using statement implicitally calls the dispose on exit
                //using (var DisposableObject = new DisposableClass(xmlpath))
                //{

                //XDocument xDoc = MarcomCacheDal<XDocument>.ReadXML(xmlpath,TenantID);
                IList<T> obj = null;
                if (typeof(T).Name == "EntityTypeDao" && xDoc.Descendants("EntityType").Count() > 0)
                {
                    try
                    {
                        obj = xDoc.Root.Elements("EntityType_Table").Elements("EntityType").Select(e => new EntityTypeDao
                        {
                            Id = Convert.ToInt32(e.Element("ID").Value),
                            Caption = e.Element("Caption").Value,
                            Description = e.Element("Description").Value,
                            ModuleID = Convert.ToInt32(e.Element("ModuleID").Value),
                            Category = Convert.ToInt32(e.Element("Category").Value),
                            //IsSystemDefined = Convert.ToBoolean(Convert.ToInt32(e.Element("IsSystemDefined").Value)),
                            //ParentEntityTypeId = Convert.ToInt32(e.Element("ParentEntityTypeID").Value),
                            ShortDescription = e.Element("ShortDescription").Value,
                            ColorCode = e.Element("ColorCode").Value,
                            IsAssociate = Convert.ToBoolean(Convert.ToInt32(e.Element("IsAssociate").Value)),
                            WorkFlowID = Convert.ToInt32(e.Element("WorkFlowID").Value),
                            IsRootLevel = Convert.ToBoolean(Convert.ToInt32(e.Element("IsRootLevel").Value))
                        }).Cast<T>().ToList();
                    }
                    catch (Exception ex)
                    {

                    }
                }
                else if (typeof(T).Name == "WidgetDao" && xDoc.Descendants("Widget").Count() > 0)
                {
                    obj = xDoc.Root.Elements("Widget_Table").Elements("Widget").Select(e => new WidgetDao
                    {
                        ID = e.Element("ID").Value,
                        Caption = e.Element("Caption").Value,
                        Description = e.Element("Description").Value,
                        WidgetTypeID = Convert.ToInt32(e.Element("WidgetTypeID").Value),
                        IsDynamic = Convert.ToBoolean(e.Element("IsDynamic").Value),
                        FilterID = Convert.ToInt32(e.Element("FilterID").Value),
                        AttributeID = Convert.ToInt32(e.Element("AttributeID").Value),
                        WidgetQuery = e.Element("WidgetQuery").Value,
                        VisualType = Convert.ToInt32(e.Element("VisualType").Value),
                        NoOfItem = Convert.ToInt32(e.Element("NoOfItem").Value)
                    }).Cast<T>().ToList();
                }
                else if (typeof(T).Name == "FeatureDao" && xDoc.Descendants("Feature").Count() > 0)
                {
                    obj = xDoc.Root.Elements("Feature_Table").Elements("Feature").Select(e => new FeatureDao
                    {
                        Id = Convert.ToInt32(e.Element("ID").Value),
                        Caption = e.Element("Caption").Value,
                        Description = e.Element("Description").Value
                    }).Cast<T>().ToList();
                }
                else if (typeof(T).Name == "ModuleDao" && xDoc.Descendants("Module").Count() > 0)
                {
                    obj = xDoc.Root.Elements("Module_Table").Elements("Module").Select(e => new ModuleDao
                    {
                        Id = Convert.ToInt32(e.Element("ID").Value),
                        Caption = e.Element("Caption").Value,
                        Description = e.Element("Description").Value,
                        IsEnable = Convert.ToBoolean(Convert.ToInt32(e.Element("IsEnable").Value))
                    }).Cast<T>().ToList();
                }
                else if (typeof(T).Name == "ModuleFeatureDao" && xDoc.Descendants("Module_Feature").Count() > 0)
                {
                    obj = xDoc.Root.Elements("Module_Feature_Table").Elements("Module_Feature").Select(e => new ModuleFeatureDao
                    {
                        Moduleid = Convert.ToInt32(e.Element("ModuleID").Value),
                        Featureid = Convert.ToInt32(e.Element("FeatureID").Value),
                        IsEnable = Convert.ToBoolean(Convert.ToInt32(e.Element("IsEnable").Value))
                    }).Cast<T>().ToList();
                }
                else if (typeof(T).Name == "EntityTypeFeatureDao" && xDoc.Descendants("EntityType_Feature").Count() > 0)
                {
                    obj = xDoc.Root.Elements("EntityType_Feature_Table").Elements("EntityType_Feature").Select(e => new EntityTypeFeatureDao
                    {
                        Id = Convert.ToInt32(e.Element("ID").Value),
                        TypeID = Convert.ToInt32(e.Element("TypeID").Value),
                        FeatureID = Convert.ToInt32(e.Element("FeatureID").Value)
                    }).Cast<T>().ToList();
                }
                else if (typeof(T).Name == "EntityTypeAttributeRelationDao" && xDoc.Descendants("EntityTypeAttributeRelation").Count() > 0)
                {
                    obj = xDoc.Root.Elements("EntityTypeAttributeRelation_Table").Elements("EntityTypeAttributeRelation").Select(e => new EntityTypeAttributeRelationDao
                    {
                        ID = Convert.ToInt32(e.Element("ID").Value),
                        EntityTypeID = Convert.ToInt32(e.Element("EntityTypeID").Value),
                        AttributeID = Convert.ToInt32(e.Element("AttributeID").Value),
                        ValidationID = e.Element("ValidationID").Value,
                        SortOrder = Convert.ToInt32(e.Element("SortOrder").Value),
                        DefaultValue = e.Element("DefaultValue").Value,
                        InheritFromParent = Convert.ToBoolean(Convert.ToInt32(e.Element("InheritFromParent").Value)),
                        IsReadOnly = Convert.ToBoolean(Convert.ToInt32(e.Element("IsReadOnly").Value)),
                        ChooseFromParentOnly = Convert.ToBoolean(Convert.ToInt32(e.Element("ChooseFromParentOnly").Value)),
                        IsValidationNeeded = Convert.ToBoolean(Convert.ToInt32(e.Element("IsValidationNeeded").Value)),
                        Caption = e.Element("Caption").Value,
                        IsSystemDefined = Convert.ToBoolean(Convert.ToInt32(e.Element("IsSystemDefined").Value)),
                        PlaceHolderValue = e.Element("PlaceHolderValue").Value,
                        MinValue = Convert.ToInt32(e.Element("MinValue").Value),
                        MaxValue = Convert.ToInt32(e.Element("MaxValue").Value),
                        IsHelptextEnabled = Convert.ToBoolean(Convert.ToInt32(e.Element("IsHelptextEnabled").Value)),
                        HelptextDecsription = Convert.ToString(e.Element("HelptextDecsription").Value)
                    }).Cast<T>().ToList();
                }
                else if (typeof(T).Name == "AttributeTypeDao" && xDoc.Descendants("AttributeType").Count() > 0)
                {
                    obj = xDoc.Root.Elements("AttributeType_Table").Elements("AttributeType").Select(e => new AttributeTypeDao
                    {
                        Id = Convert.ToInt32(e.Element("ID").Value),
                        Caption = e.Element("Caption").Value,
                        ClassName = e.Element("ClassName").Value,
                        IsSelectable = Convert.ToBoolean(Convert.ToInt32(e.Element("IsSelectable").Value)),
                        DataType = e.Element("DataType").Value,
                        SqlType = e.Element("SqlType").Value,
                        Length = Convert.ToInt32(e.Element("Length").Value),
                        IsNullable = Convert.ToBoolean(Convert.ToInt32(e.Element("IsNullable").Value))
                    }).Cast<T>().ToList();
                }
                else if (typeof(T).Name == "AttributeDao" && xDoc.Descendants("Attribute").Count() > 0)
                {
                    obj = xDoc.Root.Elements("Attribute_Table").Elements("Attribute").Select(e => new AttributeDao
                    {
                        Id = Convert.ToInt32(e.Element("ID").Value),
                        Caption = e.Element("Caption").Value.Trim(new char[] { '\n', '\r' }),
                        Description = e.Element("Description").Value.Trim(new char[] { '\n', '\r' }),
                        AttributeTypeID = Convert.ToInt32(e.Element("AttributeTypeID").Value),
                        IsSystemDefined = Convert.ToBoolean(Convert.ToInt32(e.Element("IsSystemDefined").Value)),
                        IsSpecial = Convert.ToBoolean(Convert.ToInt32(e.Element("IsSpecial").Value))
                    }).Cast<T>().ToList();
                }
                else if (typeof(T).Name == "OptionDao" && xDoc.Descendants("Option").Count() > 0)
                {
                    obj = xDoc.Root.Elements("Option_Table").Elements("Option").Select(e => new OptionDao
                    {
                        Id = Convert.ToInt32(e.Element("ID").Value),
                        Caption = e.Element("Caption").Value,
                        AttributeID = Convert.ToInt32(e.Element("AttributeID").Value),
                        SortOrder = Convert.ToInt32(e.Element("SortOrder").Value)
                    }).Cast<T>().ToList();
                }
                else if (typeof(T).Name == "MultiSelectDao" && xDoc.Descendants("MultiSelect").Count() > 0)
                {
                    obj = xDoc.Root.Elements("MultiSelect_Table").Elements("MultiSelect").Select(e => new MultiSelectDao
                    {
                        Entityid = Convert.ToInt32(e.Element("EntityID").Value),
                        Attributeid = Convert.ToInt32(e.Element("AttributeID").Value),
                        Optionid = Convert.ToInt32(e.Element("OptionID").Value)
                    }).Cast<T>().ToList();
                }
                else if (typeof(T).Name == "TreeLevelDao" && xDoc.Descendants("TreeLevel").Count() > 0)
                {
                    try
                    {
                        obj = xDoc.Root.Elements("TreeLevel_Table").Elements("TreeLevel").Select(e => new TreeLevelDao
                        {
                            Id = Convert.ToInt32(e.Element("ID").Value),
                            Level = Convert.ToInt32(e.Element("Level").Value),
                            LevelName = e.Element("LevelName").Value,
                            IsPercentage = Convert.ToBoolean(Convert.ToInt32(e.Element("IsPercentage").Value)),
                            AttributeID = Convert.ToInt32(e.Element("AttributeID").Value)
                        }).Cast<T>().ToList();
                    }
                    catch (Exception ex)
                    {

                    }
                }
                else if (typeof(T).Name == "TreeNodeDao" && xDoc.Descendants("TreeNode").Count() > 0)
                {
                    try
                    {
                        obj = xDoc.Root.Elements("TreeNode_Table").Elements("TreeNode").Select(e => new TreeNodeDao
                        {
                            Id = Convert.ToInt32(e.Element("ID").Value),
                            NodeID = Convert.ToInt32(e.Element("NodeID").Value),
                            ParentNodeID = Convert.ToInt32(e.Element("ParentNodeID").Value),
                            Level = Convert.ToInt32(e.Element("Level").Value),
                            KEY = e.Element("KEY").Value,
                            AttributeID = Convert.ToInt32(e.Element("AttributeID").Value),
                            Caption = e.Element("Caption").Value,
                            SortOrder = Convert.ToInt32(e.Element("SortOrder").Value),
                            ColorCode = e.Element("ColorCode").Value,
                        }).Cast<T>().ToList();
                    }
                    catch (Exception ex)
                    {

                    }
                }
                else if (typeof(T).Name == "TreeValueDao" && xDoc.Descendants("TreeValue").Count() > 0)
                {
                    obj = xDoc.Root.Elements("TreeValue_Table").Elements("TreeValue").Select(e => new TreeValueDao
                    {
                        Id = Convert.ToInt32(e.Element("ID").Value),
                        Nodeid = Convert.ToInt32(e.Element("NodeID").Value),
                        Attributeid = Convert.ToInt32(e.Element("AttributeID").Value),
                        Value = e.Element("Value").Value
                    }).Cast<T>().ToList();
                }
                else if (typeof(T).Name == "ValidationDao" && xDoc.Descendants("Validation").Count() > 0)
                {
                    obj = xDoc.Root.Elements("Validation_Table").Elements("Validation").Select(e => new ValidationDao
                    {
                        Id = Convert.ToInt32(e.Element("ID").Value),
                        EntityTypeID = Convert.ToInt32(e.Element("EntityTypeID").Value),
                        RelationShipID = Convert.ToInt32(e.Element("RelationShipID").Value),
                        Name = e.Element("Name").Value,
                        ValueType = e.Element("ValueType").Value,
                        Value = e.Element("Value").Value,
                        ErrorMessage = e.Element("ErrorMessage").Value
                    }).Cast<T>().ToList();
                }
                else if (typeof(T).Name == "EntityTypeHierarchyDao" && xDoc.Descendants("EntityType_Hierarchy").Count() > 0)
                {
                    obj = xDoc.Root.Elements("EntityType_Hierarchy_Table").Elements("EntityType_Hierarchy").Select(e => new EntityTypeHierarchyDao
                    {
                        Id = Convert.ToInt32(e.Element("ID").Value),
                        ParentActivityTypeID = Convert.ToInt32(e.Element("ParentActivityTypeID").Value),
                        ChildActivityTypeID = Convert.ToInt32(e.Element("ChildActivityTypeID").Value),
                        SortOrder = Convert.ToInt32(e.Element("SortOrder").Value)
                    }).Cast<T>().ToList();
                }
                else if (typeof(T).Name == "AttributeToAttributeRelationsDao" && xDoc.Descendants("AttributeToAttributeRelations").Count() > 0)
                {
                    obj = xDoc.Root.Elements("AttributeToAttributeRelations_Table").Elements("AttributeToAttributeRelations").Select(e => new AttributeToAttributeRelationsDao
                    {
                        ID = Convert.ToInt32(e.Element("ID").Value),
                        EntityTypeID = Convert.ToInt32(e.Element("EntityTypeID").Value),
                        AttributeID = Convert.ToInt32(e.Element("AttributeID").Value),
                        AttributeTypeID = Convert.ToInt32(e.Element("AttributeTypeID").Value),
                        AttributeOptionID = Convert.ToInt32(e.Element("AttributeOptionID").Value),
                        AttributeLevel = Convert.ToInt32(e.Element("AttributeLevel").Value),
                        AttributeRelationID = Convert.ToString(e.Element("AttributeRelationID").Value)
                    }).Cast<T>().ToList();
                }
                else if (typeof(T).Name == "EntityTypeStatusOptionsDao" && xDoc.Descendants("EntityTypeStatus_Options").Count() > 0)
                {
                    obj = xDoc.Root.Elements("EntityTypeStatus_Options_Table").Elements("EntityTypeStatus_Options").Select(e => new EntityTypeStatusOptionsDao
                    {
                        ID = Convert.ToInt32(e.Element("ID").Value),
                        EntityTypeID = Convert.ToInt32(e.Element("EntityTypeID").Value),
                        StatusOptions = e.Element("StatusOptions").Value,
                        ColorCode = e.Element("ColorCode").Value,
                        SortOrder = e.Element("SortOrder") == null ? 1 : Convert.ToInt32(e.Element("SortOrder").Value),
                        IsRemoved = e.Element("IsRemoved") == null ? false : Convert.ToBoolean(Convert.ToInt32(e.Element("IsRemoved").Value))
                    }).Cast<T>().ToList();
                }
                else if (typeof(T).Name == "DamTypeFileExtensionDao" && xDoc.Descendants("DamTypeFileExtension_Options").Count() > 0)
                {
                    obj = xDoc.Root.Elements("DamTypeFileExtension_Options_Table").Elements("DamTypeFileExtension_Options").Select(e => new DamTypeFileExtensionDao
                    {
                        ID = Convert.ToInt32(e.Element("ID").Value),
                        EntityTypeID = Convert.ToInt32(e.Element("EntityTypeID").Value),
                        ExtensionOptions = e.Element("ExtensionOptions").Value,
                        SortOrder = e.Element("SortOrder") == null ? 1 : Convert.ToInt32(e.Element("SortOrder").Value),
                        IsRemoved = e.Element("IsRemoved") == null ? false : Convert.ToBoolean(Convert.ToInt32(e.Element("IsRemoved").Value))
                    }).Cast<T>().ToList();
                }
                else if (typeof(T).Name == "AttributeGroupDao" && xDoc.Descendants("AttributeGroup").Count() > 0)
                {
                    obj = xDoc.Root.Elements("AttributeGroup_Table").Elements("AttributeGroup").Select(e => new AttributeGroupDao
                    {
                        Id = Convert.ToInt32(e.Element("ID").Value),
                        Caption = Convert.ToString(e.Element("Caption").Value),
                        Description = Convert.ToString(e.Element("Description").Value),
                        IsPredefined = Convert.ToBoolean(Convert.ToInt32(e.Element("IsPredefined").Value))
                    }).OrderBy(e => e.Id).Cast<T>().ToList();
                }
                else if (typeof(T).Name == "AttributeGroupAttributeRelationDao" && xDoc.Descendants("AttributeGroupAttributeRelation").Count() > 0)
                {
                    obj = xDoc.Root.Elements("AttributeGroupAttributeRelation_Table").Elements("AttributeGroupAttributeRelation").Select(e => new AttributeGroupAttributeRelationDao
                    {
                        ID = Convert.ToInt32(e.Element("ID").Value),
                        Caption = Convert.ToString(e.Element("Caption").Value),
                        AttributeGroupID = Convert.ToInt32(e.Element("AttributeGroupID").Value),
                        AttributeID = Convert.ToInt32(e.Element("AttributeID").Value),
                        SortOrder = Convert.ToInt32(e.Element("SortOrder").Value),
                        IsSearchable = Convert.ToBoolean(Convert.ToInt32(e.Element("IsSearchable").Value)),
                        ShowAsColumn = Convert.ToBoolean(Convert.ToInt32(e.Element("ShowAsColumn").Value))
                    }).OrderBy(e => e.SortOrder).Cast<T>().ToList();
                }
                else if (typeof(T).Name == "EntityTypeAttributeGroupRelationDao" && xDoc.Descendants("EntityTypeAttributeGroupRelation").Count() > 0)
                {
                    obj = xDoc.Root.Elements("EntityTypeAttributeGroupRelation_Table").Elements("EntityTypeAttributeGroupRelation").Select(e => new EntityTypeAttributeGroupRelationDao
                    {
                        ID = Convert.ToInt32(e.Element("ID").Value),
                        Caption = Convert.ToString(e.Element("Caption").Value),
                        AttributeGroupID = Convert.ToInt32(e.Element("AttributeGroupID").Value),
                        EntityTypeID = Convert.ToInt32(e.Element("EntityTypeID").Value),
                        LocationType = Convert.ToInt32(e.Element("LocationType").Value),
                        RepresentationType = Convert.ToBoolean(Convert.ToInt32(e.Element("RepresentationType").Value)),
                        SortOrder = Convert.ToInt32(e.Element("SortOrder").Value),
                        PageSize = Convert.ToInt32(e.Element("PageSize").Value),
                        IsTabInEntityCreation = Convert.ToBoolean(Convert.ToInt32(e.Element("IsTabInEntityCreation").Value)),
                        IsAttrGrpInheritFromParent = Convert.ToBoolean(Convert.ToInt32(e.Element("IsAttrGrpInheritFromParent").Value))
                    }).OrderBy(e => e.SortOrder).Cast<T>().ToList();
                }
                else if (typeof(T).Name == "UserVisibleInfoDao" && xDoc.Descendants("UserVisibleInfo").Count() > 0)
                {
                    obj = xDoc.Root.Elements("UserVisibleInfo_Table").Elements("UserVisibleInfo").Select(e => new UserVisibleInfoDao
                    {
                        AttributeId = Convert.ToInt32(e.Element("AttributeId").Value),
                        Id = Convert.ToInt32(e.Element("ID").Value)
                    }).Cast<T>().ToList();
                }
                else if (typeof(T).Name == "EntityTypeRoleAclDao" && xDoc.Descendants("EntityTypeRole_Acl").Count() > 0)
                {
                    obj = xDoc.Root.Elements("EntityTypeRole_Acl_Table").Elements("EntityTypeRole_Acl").Select(e => new EntityTypeRoleAclDao
                    {
                        ID = Convert.ToInt32(e.Element("ID").Value),
                        EntityTypeID = Convert.ToInt32(e.Element("EntityTypeID").Value),
                        EntityRoleID = Convert.ToInt32(e.Element("EntityRoleID").Value),
                        Sortorder = e.Element("Sortorder") == null ? 1 : Convert.ToInt32(e.Element("Sortorder").Value),
                        Caption = e.Element("Caption").Value,
                        ModuleID = Convert.ToInt32(e.Element("ModuleID").Value)
                    }).Cast<T>().ToList();
                }
                else if (typeof(T).Name == "AttributeGroupRoleAccessDao" && xDoc.Descendants("AttributeGroupRoleAccess").Count() > 0)
                {
                    obj = xDoc.Root.Elements("AttributeGroupRoleAccess_Table").Elements("AttributeGroupRoleAccess").Select(e => new AttributeGroupRoleAccessDao
                    {
                        ID = Convert.ToInt32(e.Element("ID").Value),
                        EntityTypeID = Convert.ToInt32(e.Element("EntityTypeID").Value),
                        AttributeGroupID = Convert.ToInt32(e.Element("AttributeGroupID").Value),
                        GlobalRoleID = Convert.ToInt32(e.Element("GlobalRoleID").Value)
                    }).OrderBy(e => e.GlobalRoleID).Cast<T>().ToList();
                }
                else if (typeof(T).Name == "EntitytasktypeDao" && xDoc.Descendants("EntityTaskType").Count() > 0)
                {
                    obj = xDoc.Root.Elements("EntityTaskType_Table").Elements("EntityTaskType").Select(e => new EntitytasktypeDao
                    {
                        Id = Convert.ToInt32(e.Element("ID").Value),
                        EntitytypeId = Convert.ToInt32(e.Element("EntitytypeId").Value),
                        TaskTypeId = Convert.ToInt32(e.Element("TaskTypeId").Value)
                    }).Cast<T>().ToList();
                }
                else if (typeof(T).Name == "TagOptionDao" && xDoc.Descendants("TagOption").Count() > 0)
                {
                    obj = xDoc.Root.Elements("TagOption_Table").Elements("TagOption").Select(e => new TagOptionDao
                    {
                        Id = Convert.ToInt32(e.Element("ID").Value),
                        Caption = e.Element("Caption").Value,
                        AttributeID = Convert.ToInt32(e.Element("AttributeID").Value),
                        UserID = Convert.ToInt32(e.Element("UserID").Value),
                        CreateOn = e.Element("CreateOn").Value,
                        UsedOn = e.Element("UsedOn").Value,
                        TotalHits = Convert.ToInt32(e.Element("TotalHits").Value),
                        PopularTagsToShow = Convert.ToInt32(e.Element("PopularTagsToShow").Value),
                        SortOrder = Convert.ToInt32(e.Element("SortOrder").Value)
                    }).Cast<T>().ToList();
                }
                else if (typeof(T).Name == "PopularTagWordsDao" && xDoc.Descendants("PopularTagWords").Count() > 0)
                {
                    obj = xDoc.Root.Elements("PopularTagWords_Table").Elements("PopularTagWords").Select(e => new PopularTagWordsDao
                    {
                        Id = Convert.ToInt32(e.Element("ID").Value),
                        AttributeID = Convert.ToInt32(e.Element("AttributeID").Value),
                        UsedOn = e.Element("UsedOn").Value,
                        TotalHits = Convert.ToInt32(e.Element("TotalHits").Value),
                    }).Cast<T>().ToList();
                }

                else if (typeof(T).Name == "AttributeSequenceDao" && xDoc.Descendants("AttributeSequence").Count() > 0)
                {
                    obj = xDoc.Root.Elements("AttributeSequence_Table").Elements("AttributeSequence").Select(e => new AttributeSequenceDao
                    {
                        Id = Convert.ToInt32(e.Element("ID").Value),
                        Pattern = e.Element("Pattern").Value,
                        AttributeID = Convert.ToInt32(e.Element("AttributeID").Value),
                        StartWithValue = Convert.ToInt32(e.Element("StartWithValue").Value)
                    }).Cast<T>().ToList();
                }
                else if (typeof(T).Name == "EntityobjectiveTypeDao" && xDoc.Descendants("EntityObjectiveType").Count() > 0)
                {
                    obj = xDoc.Root.Elements("EntityObjectiveType_Table").Elements("EntityObjectiveType").Select(e => new EntityobjectiveTypeDao
                    {
                        Id = Convert.ToInt32(e.Element("ID").Value),
                        EntitytypeId = Convert.ToInt32(e.Element("EntitytypeId").Value),
                        ObjectiveTypeId = Convert.ToInt32(e.Element("ObjectiveTypeId").Value),
                        ObjectiveBaseTypeId = Convert.ToInt32(e.Element("ObjectiveBaseTypeId").Value),
                        UnitId = Convert.ToInt32(e.Element("UnitId").Value)

                    }).Cast<T>().ToList();
                }
                return obj;
                //    }
                //}
                //else
                //{
                //    return null;
                //}
            }
            catch (Exception ex)
            {
                return null;
            }


        }

        public XElement GetXElement(BaseDao obj)
        {

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<root>");
            if (obj.GetType().Name == "ModuleFeatureDao")
            {
                sb.AppendLine("<Module_Feature_Table" + ">");
                sb.AppendLine("<Module_Feature>");
            }
            else if (obj.GetType().Name == "EntityTypeFeatureDao")
            {
                sb.AppendLine("<EntityType_Feature_Table" + ">");
                sb.AppendLine("<EntityType_Feature>");
            }
            else if (obj.GetType().Name == "EntityTypeHierarchyDao")
            {
                sb.AppendLine("<EntityType_Hierarchy_Table>");
                sb.AppendLine("<EntityType_Hierarchy" + ">");
            }
            else if (obj.GetType().Name == "EntityTypeStatusOptionsDao")
            {
                sb.AppendLine("<EntityTypeStatus_Options_Table>");
                sb.AppendLine("<EntityTypeStatus_Options" + ">");
            }
            else if (obj.GetType().Name == "DamTypeFileExtensionDao")
            {
                sb.AppendLine("<DamTypeFileExtension_Options_Table>");
                sb.AppendLine("<DamTypeFileExtension_Options" + ">");
            }
            else if (obj.GetType().Name == "EntitytasktypeDao")
            {
                sb.AppendLine("<EntityTaskType_Table>");
                sb.AppendLine("<EntityTaskType" + ">");
            }
            else if (obj.GetType().Name == "EntityTypeRoleAclDao")
            {
                sb.AppendLine("<EntityTypeRole_Acl_Table>");
                sb.AppendLine("<EntityTypeRole_Acl" + ">");
            }
            else if (obj.GetType().Name == "EntityobjectiveTypeDao")
            {
                sb.AppendLine("<EntityObjectiveType_Table" + ">");
                sb.AppendLine("<EntityObjectiveType>");
            }
            else
            {
                sb.AppendLine("<" + obj.GetType().Name.Replace("Dao", "") + "_Table" + ">");
                sb.AppendLine("<" + obj.GetType().Name.Replace("Dao", "") + ">");
            }

            foreach (var item in obj.GetType().GetProperties())
            {
                if (item.Name == "ModuleCaption")
                {
                    item.Name.Remove(4);
                }
                if (item.Name == "IsChanged" || item.Name == "IsDeleted")
                { }
                else
                {
                    if (item.Name == "Id")
                        sb.AppendLine("<" + item.Name.ToUpper() + ">");
                    else
                        sb.AppendLine("<" + item.Name + ">");

                    if (item.GetValue(obj, null).GetType() == typeof(Boolean))
                        sb.AppendLine((Convert.ToInt32(item.GetValue(obj, null))).ToString());
                    else
                        sb.AppendLine(item.GetValue(obj, null).ToString().Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;").Trim(new char[] { '\n', '\r' }));
                    if (item.Name == "Id")
                        sb.AppendLine("</" + item.Name.ToUpper() + ">");
                    else
                        sb.AppendLine("</" + item.Name + ">");
                }
            }

            if (obj.GetType().Name == "ModuleFeatureDao")
            {
                sb.AppendLine("</Module_Feature>");
                sb.AppendLine("</Module_Feature_Table" + ">");
            }
            else if (obj.GetType().Name == "EntityTypeFeatureDao")
            {
                sb.AppendLine("</EntityType_Feature>");
                sb.AppendLine("</EntityType_Feature_Table" + ">");
            }
            else if (obj.GetType().Name == "EntityTypeHierarchyDao")
            {
                sb.AppendLine("</EntityType_Hierarchy>");
                sb.AppendLine("</EntityType_Hierarchy_Table" + ">");
            }
            else if (obj.GetType().Name == "EntityTypeStatusOptionsDao")
            {
                sb.AppendLine("</EntityTypeStatus_Options>");
                sb.AppendLine("</EntityTypeStatus_Options_Table" + ">");
            }
            else if (obj.GetType().Name == "DamTypeFileExtensionDao")
            {
                sb.AppendLine("</DamTypeFileExtension_Options>");
                sb.AppendLine("</DamTypeFileExtension_Options_Table" + ">");
            }
            else if (obj.GetType().Name == "EntitytasktypeDao")
            {
                sb.AppendLine("</EntityTaskType>");
                sb.AppendLine("</EntityTaskType_Table" + ">");
            }
            else if (obj.GetType().Name == "EntityTypeRoleAclDao")
            {
                sb.AppendLine("</EntityTypeRole_Acl" + ">");
                sb.AppendLine("</EntityTypeRole_Acl_Table>");
            }
            else if (obj.GetType().Name == "EntityobjectiveTypeDao")
            {
                sb.AppendLine("</EntityObjectiveType>");
                sb.AppendLine("</EntityObjectiveType_Table" + ">");
            }
            else
            {
                sb.AppendLine("</" + obj.GetType().Name.Replace("Dao", "") + ">");
                sb.AppendLine("</" + obj.GetType().Name.Replace("Dao", "") + "_Table" + ">");
            }
            sb.AppendLine("</root>");
            return XElement.Parse(sb.ToString().Replace(Environment.NewLine, ""));
        }

        public XElement GetWidgetXElement(BaseDao obj)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<root>");

            sb.AppendLine("<" + obj.GetType().Name.Replace("Dao", "") + "_Table" + ">");
            sb.AppendLine("<" + obj.GetType().Name.Replace("Dao", "") + ">");

            foreach (var item in obj.GetType().GetProperties())
            {
                sb.AppendLine("<" + item.Name + ">");
                sb.AppendLine(item.GetValue(obj, null).ToString());
                sb.AppendLine("</" + item.Name + ">");
            }

            sb.AppendLine("</" + obj.GetType().Name.Replace("Dao", "") + ">");
            sb.AppendLine("</" + obj.GetType().Name.Replace("Dao", "") + "_Table" + ">");
            sb.AppendLine("</root>");
            return XElement.Parse(sb.ToString().Trim(new char[] { '\n', '\r' }));

        }

        public void CreateTableElement(string TableName, string xmlpath, int TenantID)
        {
            XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);

            System.Xml.XmlDocument resXml = new System.Xml.XmlDocument();
            string xmlcontent = "";

            XmlElement attribRoot = resXml.CreateElement("root");
            resXml.AppendChild(attribRoot);

            XmlElement attribModTableRoot = resXml.CreateElement(TableName);
            //Prabhu
            //attribModTableRoot.SetAttribute("maxid", "0");

            attribRoot.AppendChild(attribModTableRoot);
            xmlcontent = resXml.OuterXml.ToString();


            XElement subjectElementworking1 = docx.Descendants("root").FirstOrDefault();
            XDocument rootdoc = XDocument.Parse(xmlcontent);
            XElement subjectElement1 = rootdoc.Descendants("root").FirstOrDefault();


            subjectElementworking1.Add(subjectElement1.Nodes());

            //docx.Save(xmlpath);
            MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
        }
        public void CreateTableElementForSyncDB(string TableName, string xmlpath, int TenantID)
        {
            XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);

            System.Xml.XmlDocument resXml = new System.Xml.XmlDocument();
            string xmlcontent = "";

            XmlElement attribRoot = resXml.CreateElement("root");
            resXml.AppendChild(attribRoot);

            XmlElement attribModTableRoot = resXml.CreateElement(TableName);
            //Prabhu
            //attribModTableRoot.SetAttribute("maxid", "0");

            attribRoot.AppendChild(attribModTableRoot);
            xmlcontent = resXml.OuterXml.ToString();

            XElement subjectElementworking1 = docx.Descendants("root").FirstOrDefault();
            XDocument rootdoc = XDocument.Parse(xmlcontent);
            XElement subjectElement1 = rootdoc.Descendants("root").FirstOrDefault();

            subjectElementworking1.Add(subjectElement1.Nodes());
            //docx.Save(xmlpath);
            MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
        }
        public bool DeleteOptionByAttributeID<T>(string xmlpath, BaseDao obj, int TenantID)
        {
            if (typeof(T).Name == "OptionDao")
            {
                OptionDao mod = (OptionDao)obj;
                int attributeid = mod.AttributeID;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                docx.Root.SetAttributeValue("IsEdited", "1");

                var result = docx.Root.Elements("Option_Table").Elements("Option").Where(mattribid => Convert.ToInt32(mattribid.Element("AttributeID").Value) == attributeid).Select(m => m);
                foreach (XElement item in result)
                {
                    item.Remove();
                }
                if (docx.Root.Elements("Option_Table").Elements("Option").Count() == 0)
                {
                    docx.Root.Elements("Option_Table").Remove();
                }
                //docx.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
            }
            return true;
        }
        public bool DeleteObject<T>(string xmlpath, BaseDao obj, int TenantID)
        {
            if (typeof(T).Name == "EntityTypeDao")
            {
                EntityTypeDao mod = (EntityTypeDao)obj;
                int entitytypeid = mod.Id;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                docx.Root.SetAttributeValue("IsEdited", "1");

                var result = docx.Root.Elements("EntityType_Table").Elements("EntityType").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == entitytypeid).Select(m => m);
                foreach (XElement item in result)
                {
                    item.Remove();
                }
                if (docx.Root.Elements("EntityType_Table").Elements("EntityType").Count() == 0)
                {
                    docx.Root.Elements("EntityType_Table").Remove();
                }
                //docx.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
            }
            else if (typeof(T).Name == "WidgetDao")
            {
                WidgetDao wid = (WidgetDao)obj;
                string widgetid = wid.ID;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                var result = docx.Root.Elements("Widget_Table").Elements("Widget").Where(mid => Convert.ToString(mid.Element("ID").Value.Trim()) == widgetid).Select(m => m);
                foreach (XElement item in result)
                {
                    item.Remove();
                }
                if (docx.Root.Elements("Widget_Table").Elements("Widget").Count() == 0)
                {
                    docx.Root.Elements("Widget_Table").Remove();
                }
                //docx.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
            }
            else if (typeof(T).Name == "ModuleDao")
            {
                ModuleDao mod = (ModuleDao)obj;
                int moduleid = mod.Id;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                var result = docx.Root.Elements("Module_Table").Elements("Module").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == moduleid).Select(m => m);
                foreach (XElement item in result)
                {
                    item.Remove();
                }
                if (docx.Root.Elements("Module_Table").Elements("Module").Count() == 0)
                {
                    docx.Root.Elements("Module_Table").Remove();
                }
                //docx.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
            }
            else if (typeof(T).Name == "ModuleFeatureDao")
            {
                ModuleFeatureDao mod = (ModuleFeatureDao)obj;
                int moduleid = mod.Moduleid;
                int? featureid = mod.Featureid;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                var result = docx.Root.Elements("Module_Feature_Table").Elements("Module_Feature").Where(mid => Convert.ToInt32(mid.Element("Moduleid").Value) == moduleid && Convert.ToInt32(mid.Element("Featureid").Value) == featureid).Select(m => m);
                foreach (XElement item in result)
                {
                    item.Remove();
                }
                if (docx.Root.Elements("Module_Feature_Table").Elements("Module_Feature").Count() == 0)
                {
                    docx.Root.Elements("Module_Feature_Table").Remove();
                }
                //docx.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
            }
            else if (typeof(T).Name == "EntityTypeFeatureDao")
            {
                EntityTypeFeatureDao mod = (EntityTypeFeatureDao)obj;
                int typeid = mod.TypeID;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                var result = docx.Root.Elements("EntityType_Feature_Table").Elements("EntityType_Feature").Where(mid => Convert.ToInt32(mid.Element("TypeID").Value) == typeid).Select(m => m); //&& Convert.ToInt32(mid.Element("Featureid").Value) == featureid).Select(m => m);
                foreach (var item in result.ToList())
                {
                    item.Remove();
                }
                if (docx.Root.Elements("EntityType_Feature_Table").Elements("EntityType_Feature").Count() == 0)
                {
                    docx.Root.Elements("EntityType_Feature_Table").Remove();
                }
                //docx.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
            }
            else if (typeof(T).Name == "EntityTypeAttributeRelationDao")
            {
                EntityTypeAttributeRelationDao mod = (EntityTypeAttributeRelationDao)obj;
                int ID = mod.ID;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                docx.Root.SetAttributeValue("IsEdited", "1");

                var result = docx.Root.Elements("EntityTypeAttributeRelation_Table").Elements("EntityTypeAttributeRelation").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == ID).Select(m => m);
                foreach (XElement item in result.ToList())
                {
                    item.Remove();
                }
                if (docx.Root.Elements("EntityTypeAttributeRelation_Table").Elements("EntityTypeAttributeRelation").Count() == 0)
                {
                    docx.Root.Elements("EntityTypeAttributeRelation_Table").Remove();
                }
                //docx.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
            }
            else if (typeof(T).Name == "AttributeTypeDao")
            {
                AttributeTypeDao mod = (AttributeTypeDao)obj;
                int Id = mod.Id;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                var result = docx.Root.Elements("AttributeType_Table").Elements("AttributeType").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                foreach (XElement item in result)
                {
                    item.Remove();
                }
                if (docx.Root.Elements("AttributeType_Table").Elements("AttributeType").Count() == 0)
                {
                    docx.Root.Elements("AttributeType_Table").Remove();
                }
                //docx.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
            }
            else if (typeof(T).Name == "AttributeDao")
            {
                AttributeDao mod = (AttributeDao)obj;
                int Id = mod.Id;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                docx.Root.SetAttributeValue("IsEdited", "1");

                var result = docx.Root.Elements("Attribute_Table").Elements("Attribute").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                foreach (XElement item in result)
                {
                    if (item.Element("IsSystemDefined").Value.Trim().ToUpper() != "TRUE")
                        item.Remove();
                    else
                        return false;
                }
                if (docx.Root.Elements("Attribute_Table").Elements("Attribute").Count() == 0)
                {
                    docx.Root.Elements("Attribute_Table").Remove();
                }
                //docx.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
            }
            else if (typeof(T).Name == "OptionDao")
            {
                OptionDao mod = (OptionDao)obj;
                int Id = mod.Id;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                docx.Root.SetAttributeValue("IsEdited", "1");

                var result = docx.Root.Elements("Option_Table").Elements("Option").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                foreach (XElement item in result)
                {
                    item.Remove();
                }
                if (docx.Root.Elements("Option_Table").Elements("Option").Count() == 0)
                {
                    docx.Root.Elements("Option_Table").Remove();
                }
                //docx.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
            }
            else if (typeof(T).Name == "AttributeSequenceDao")
            {
                AttributeSequenceDao mod = (AttributeSequenceDao)obj;
                int Id = mod.AttributeID;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                docx.Root.SetAttributeValue("IsEdited", "1");

                var result = docx.Root.Elements("AttributeSequence_Table").Elements("AttributeSequence").Where(mid => Convert.ToInt32(mid.Element("AttributeID").Value) == Id).Select(m => m);
                foreach (XElement item in result)
                {
                    item.Remove();
                }
                if (docx.Root.Elements("AttributeSequence_Table").Elements("AttributeSequence").Count() == 0)
                {
                    docx.Root.Elements("AttributeSequence_Table").Remove();
                }
                //docx.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
            }
            else if (typeof(T).Name == "TagOptionDao")
            {
                TagOptionDao mod = (TagOptionDao)obj;
                int Id = mod.Id;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                docx.Root.SetAttributeValue("IsEdited", "1");

                var result = docx.Root.Elements("TagOption_Table").Elements("TagOption").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                foreach (XElement item in result)
                {
                    item.Remove();
                }
                if (docx.Root.Elements("TagOption_Table").Elements("TagOption").Count() == 0)
                {
                    docx.Root.Elements("TagOption_Table").Remove();
                }
                //docx.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
            }
            else if (typeof(T).Name == "MultiSelectDao")
            {
                MultiSelectDao mod = (MultiSelectDao)obj;
                int EntityID = mod.Entityid;
                int AttributeID = mod.Entityid;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                docx.Root.SetAttributeValue("IsEdited", "1");

                var result = docx.Root.Elements("MultiSelect_Table").Elements("MultiSelect").Where(mid => Convert.ToInt32(mid.Element("Entityid").Value) == EntityID && Convert.ToInt32(mid.Element("Attributeid").Value) == AttributeID).Select(m => m);
                foreach (XElement item in result)
                {
                    item.Remove();
                }
                if (docx.Root.Elements("MultiSelect_Table").Elements("MultiSelect").Count() == 0)
                {
                    docx.Root.Elements("MultiSelect_Table").Remove();
                }
                //docx.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
            }
            else if (typeof(T).Name == "TreeLevelDao")
            {
                TreeLevelDao mod = (TreeLevelDao)obj;
                int Id = mod.Id;
                int AttributeId = mod.AttributeID;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                docx.Root.SetAttributeValue("IsEdited", "1");

                var result = docx.Root.Elements("TreeLevel_Table").Elements("TreeLevel").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id && Convert.ToInt32(mid.Element("AttributeID").Value) == AttributeId).Select(m => m);
                foreach (XElement item in result)
                {
                    item.Remove();
                }
                if (docx.Root.Elements("TreeLevel_Table").Elements("TreeLevel").Count() == 0)
                {
                    docx.Root.Elements("TreeLevel_Table").Remove();
                }
                //docx.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
            }
            else if (typeof(T).Name == "TreeNodeDao")
            {
                TreeNodeDao mod = (TreeNodeDao)obj;
                int Id = mod.Id;
                int AttributeID = mod.AttributeID;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                docx.Root.SetAttributeValue("IsEdited", "1");

                var result = docx.Root.Elements("TreeNode_Table").Elements("TreeNode").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id && Convert.ToInt32(mid.Element("AttributeID").Value) == AttributeID).Select(m => m);
                foreach (XElement item in result)
                {
                    item.Remove();
                }
                if (docx.Root.Elements("TreeNode_Table").Elements("TreeNode").Count() == 0)
                {
                    docx.Root.Elements("TreeNode_Table").Remove();
                }
                //docx.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
            }
            else if (typeof(T).Name == "TreeValueDao")
            {
                TreeValueDao mod = (TreeValueDao)obj;
                int Id = mod.Id;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                docx.Root.SetAttributeValue("IsEdited", "1");

                var result = docx.Root.Elements("TreeValue_Table").Elements("TreeValue").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                foreach (XElement item in result)
                {
                    item.Remove();
                }
                if (docx.Root.Elements("TreeValue_Table").Elements("TreeValue").Count() == 0)
                {
                    docx.Root.Elements("TreeValue_Table").Remove();
                }
                //docx.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
            }
            else if (typeof(T).Name == "ValidationDao")
            {
                ValidationDao mod = (ValidationDao)obj;
                int Id = mod.Id;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                docx.Root.SetAttributeValue("IsEdited", "1");

                var result = docx.Root.Elements("Validation_Table").Elements("Validation").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                foreach (XElement item in result)
                {
                    item.Remove();
                }
                if (docx.Root.Elements("Validation_Table").Elements("Validation").Count() == 0)
                {
                    docx.Root.Elements("Validation_Table").Remove();
                }
                //docx.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
            }
            else if (typeof(T).Name == "EntityTypeHierarchyDao")
            {
                EntityTypeHierarchyDao entHeirarchydao = (EntityTypeHierarchyDao)obj;
                int parentActityTypeId = entHeirarchydao.ParentActivityTypeID;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                var result = docx.Root.Elements("EntityType_Hierarchy_Table").Elements("EntityType_Hierarchy").Where(mid => Convert.ToInt32(mid.Element("ParentActivityTypeID").Value) == parentActityTypeId).Select(m => m);
                foreach (var item in result.ToList())
                {
                    item.Remove();
                }
                if (docx.Root.Elements("EntityType_Hierarchy_Table").Elements("EntityType_Hierarchy").Count() == 0)
                {
                    docx.Root.Elements("EntityType_Hierarchy_Table").Remove();
                }
                //docx.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
            }
            else if (typeof(T).Name == "AttributeToAttributeRelationsDao")
            {
                AttributeToAttributeRelationsDao attrd = (AttributeToAttributeRelationsDao)obj;
                int entitytypeid = attrd.EntityTypeID;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                var result = docx.Root.Elements("AttributeToAttributeRelations_Table").Elements("AttributeToAttributeRelations").Where(mid => Convert.ToInt32(mid.Element("EntityTypeID").Value) == entitytypeid).Select(m => m);
                foreach (var item in result.ToList())
                {
                    item.Remove();
                }
                if (docx.Root.Elements("AttributeToAttributeRelations_Table").Elements("AttributeToAttributeRelations").Count() == 0)
                {
                    docx.Root.Elements("AttributeToAttributeRelations_Table").Remove();
                }
                //docx.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
            }
            else if (typeof(T).Name == "EntityTypeStatusOptionsDao")
            {
                EntityTypeStatusOptionsDao statusentity = (EntityTypeStatusOptionsDao)obj;
                int ID = statusentity.ID;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                docx.Root.SetAttributeValue("IsEdited", "1");

                var result = docx.Root.Elements("EntityTypeStatus_Options_Table").Elements("EntityTypeStatus_Options").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == ID).Select(m => m);
                foreach (var item in result.ToList())
                {
                    item.Remove();
                }
                if (docx.Root.Elements("EntityTypeStatus_Options_Table").Elements("EntityTypeStatus_Options").Count() == 0)
                {
                    docx.Root.Elements("EntityTypeStatus_Options_Table").Remove();
                }
                //docx.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
            }

            else if (typeof(T).Name == "DamTypeFileExtensionDao")
            {
                DamTypeFileExtensionDao statusentity = (DamTypeFileExtensionDao)obj;
                int ID = statusentity.ID;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                docx.Root.SetAttributeValue("IsEdited", "1");

                var result = docx.Root.Elements("DamTypeFileExtension_Options_Table").Elements("DamTypeFileExtension_Options").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == ID).Select(m => m);
                foreach (var item in result.ToList())
                {
                    item.Remove();
                }
                if (docx.Root.Elements("DamTypeFileExtension_Options_Table").Elements("DamTypeFileExtension_Options").Count() == 0)
                {
                    docx.Root.Elements("DamTypeFileExtension_Options_Table").Remove();
                }
                //docx.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
            }
            else if (typeof(T).Name == "AttributeGroupDao")
            {
                AttributeGroupDao mod = (AttributeGroupDao)obj;
                int attributegroupid = mod.Id;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                docx.Root.SetAttributeValue("IsEdited", "1");

                var result = docx.Root.Elements("AttributeGroup_Table").Elements("AttributeGroup").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == attributegroupid).Select(m => m);
                foreach (XElement item in result)
                {
                    item.Remove();
                }
                if (docx.Root.Elements("AttributeGroup_Table").Elements("AttributeGroup").Count() == 0)
                {
                    docx.Root.Elements("AttributeGroup_Table").Remove();
                }
                //docx.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
            }
            else if (typeof(T).Name == "AttributeGroupAttributeRelationDao")
            {
                AttributeGroupAttributeRelationDao mod = (AttributeGroupAttributeRelationDao)obj;
                int attributegroupid = mod.ID;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                docx.Root.SetAttributeValue("IsEdited", "1");

                var result = docx.Root.Elements("AttributeGroupAttributeRelation_Table").Elements("AttributeGroupAttributeRelation").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == attributegroupid).Select(m => m);
                foreach (XElement item in result.ToList())
                {
                    item.Remove();
                }
                if (docx.Root.Elements("AttributeGroupAttributeRelation_Table").Elements("AttributeGroupAttributeRelation").Count() == 0)
                {
                    docx.Root.Elements("AttributeGroupAttributeRelation_Table").Remove();
                }
                //docx.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
            }
            else if (typeof(T).Name == "EntityTypeAttributeGroupRelationDao")
            {
                EntityTypeAttributeGroupRelationDao mod = (EntityTypeAttributeGroupRelationDao)obj;
                int attributegroupid = mod.ID;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                docx.Root.SetAttributeValue("IsEdited", "1");

                var result = docx.Root.Elements("EntityTypeAttributeGroupRelation_Table").Elements("EntityTypeAttributeGroupRelation").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == attributegroupid).Select(m => m);
                foreach (XElement item in result.ToList())
                {
                    item.Remove();
                }
                if (docx.Root.Elements("EntityTypeAttributeGroupRelation_Table").Elements("EntityTypeAttributeGroupRelation").Count() == 0)
                {
                    docx.Root.Elements("EntityTypeAttributeGroupRelation_Table").Remove();
                }
                //docx.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
            }
            else if (typeof(T).Name == "UserVisibleInfoDao")
            {
                UserVisibleInfoDao usi = (UserVisibleInfoDao)obj;
                int attributeid = usi.AttributeId;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                docx.Root.SetAttributeValue("IsEdited", "1");

                var result = docx.Root.Elements("UserVisibleInfo_Table").Elements("UserVisibleInfo").Where(mid => Convert.ToInt32(mid.Element("AttributeId").Value) == attributeid).Select(m => m);
                foreach (XElement item in result.ToList())
                {
                    item.Remove();
                }
                if (docx.Root.Elements("UserVisibleInfo_Table").Elements("UserVisibleInfo").Count() == 0)
                {
                    docx.Root.Elements("UserVisibleInfo_Table").Remove();
                }
                //docx.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
            }
            else if (typeof(T).Name == "EntityTypeRoleAclDao")
            {
                EntityTypeRoleAclDao EntitypeRoleObj = (EntityTypeRoleAclDao)obj;
                int ID = EntitypeRoleObj.ID;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                var result = docx.Root.Elements("EntityTypeRole_Acl_Table").Elements("EntityTypeRole_Acl").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == ID).Select(m => m);
                foreach (XElement item in result.ToList())
                {
                    item.Remove();
                }
                if (docx.Root.Elements("EntityTypeRole_Acl_Table").Elements("EntityTypeRole_Acl").Count() == 0)
                {
                    docx.Root.Elements("EntityTypeRole_Acl_Table").Remove();
                }
                //docx.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
            }
            else if (typeof(T).Name == "AttributeGroupRoleAccessDao")
            {
                AttributeGroupRoleAccessDao mod = (AttributeGroupRoleAccessDao)obj;
                int entitytypeid = mod.EntityTypeID;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                docx.Root.SetAttributeValue("IsEdited", "1");

                var result = docx.Root.Elements("AttributeGroupRoleAccess_Table").Elements("AttributeGroupRoleAccess").Where(mid => Convert.ToInt32(mid.Element("EntityTypeID").Value) == entitytypeid).Select(m => m);
                foreach (XElement item in result.ToList())
                {
                    item.Remove();
                }
                if (docx.Root.Elements("AttributeGroupRoleAccess_Table").Elements("AttributeGroupRoleAccess").Count() == 0)
                {
                    docx.Root.Elements("AttributeGroupRoleAccess_Table").Remove();
                }
                //docx.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
            }
            return true;
        }
        public bool DeleteTableTag<T>(string xmlpath, BaseDao obj, int TenantID)
        {
            try
            {
                if (typeof(T).Name == "UserVisibleInfoDao")
                {
                    UserVisibleInfoDao usi = (UserVisibleInfoDao)obj;
                    int attributeid = usi.AttributeId;
                    XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                    docx.Root.SetAttributeValue("IsEdited", "1");

                    var result = docx.Root.Elements("UserVisibleInfo_Table").Elements("UserVisibleInfo").Select(m => m);
                    foreach (XElement item in result.ToList())
                    {
                        item.Remove();
                    }
                    if (docx.Root.Elements("UserVisibleInfo_Table").Elements("UserVisibleInfo").Count() == 0)
                    {
                        docx.Root.Elements("UserVisibleInfo_Table").Remove();
                    }
                    //docx.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public XElement SaveObject<T>(int TenantID, string xmlpath, BaseDao obj, int objMaxID = 0)
        {

            XElement subjectElement2 = null;
            XElement MetadataXML = null;
            if (typeof(T).Name == "EntityTypeDao")
            {
                EntityTypeDao EntityDao = (EntityTypeDao)obj;
                int Id = EntityDao.Id;
                string caption = EntityDao.Caption;
                string description = EntityDao.Description;
                int moduleid = EntityDao.ModuleID;
                //bool issystemdefined = EntityDao.IsSystemDefined;
                int category = EntityDao.Category;
                //int parententitytypeId = EntityDao.ParentEntityTypeId;
                string shortDescription = EntityDao.ShortDescription != null ? EntityDao.ShortDescription : "";
                string colorCode = EntityDao.ColorCode;
                bool isAssociate = EntityDao.IsAssociate;
                int? workflowid = EntityDao.WorkFlowID;
                bool isrootlevel = EntityDao.IsRootLevel;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                var insertresult = docx.Root.Elements("EntityType_Table").Elements("EntityType").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);

                if (insertresult.Count() == 0)
                {

                    //We need to add by default MaxId = 0 while creating the table
                    XDocument document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                    XElement subjectElementworking = document1.Descendants("EntityType_Table").FirstOrDefault();
                    if (subjectElementworking == null)
                    {
                        CreateTableElement("EntityType_Table", xmlpath, TenantID);
                        document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                        subjectElementworking = document1.Descendants("EntityType_Table").FirstOrDefault();
                    }
                    EntityDao.Id = objMaxID;
                    MetadataXML = GetXElement(EntityDao);
                    subjectElement2 = MetadataXML.Descendants("EntityType_Table").FirstOrDefault();
                    subjectElementworking.Add(subjectElement2.Nodes());
                    //document1.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(document1, xmlpath, TenantID);
                }
                else
                {
                    //XmlDocument doc = new XmlDocument();
                    //doc.Load(xmlpath);
                    var Updateresult = docx.Root.Elements("EntityType_Table").Elements("EntityType").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                    foreach (XElement item in Updateresult)
                    {
                        item.SetElementValue("Caption", caption == null || caption == "" ? "-" : caption);
                        item.SetElementValue("Description", description == null ? "" : description);
                        item.SetElementValue("ModuleID", moduleid);
                        // item.SetElementValue("IsSystemDefined", Convert.ToInt32(issystemdefined).ToString());
                        item.SetElementValue("Category", category);
                        //item.SetElementValue("ParentEntityTypeID", parententitytypeId);
                        item.SetElementValue("ShortDescription", shortDescription);
                        item.SetElementValue("ColorCode", colorCode);
                        item.SetElementValue("IsAssociate", Convert.ToInt32(isAssociate));
                        item.SetElementValue("WorkFlowID", Convert.ToInt32(workflowid));
                        item.SetElementValue("IsRootLevel", Convert.ToInt32(isrootlevel));

                        string xmlelement = "<root><EntityType_Table>";
                        MetadataXML = item;
                        xmlelement = xmlelement + MetadataXML.ToString() + "</EntityType_Table></root>";
                        MetadataXML = XElement.Parse(xmlelement);
                    }
                    //docx.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
                }
                XDocument docx1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                docx1.Root.SetAttributeValue("IsEdited", "1");
                //docx1.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx1, xmlpath, TenantID);
            }

            else if (typeof(T).Name == "WidgetDao")
            {
                WidgetDao newwidgetDao = (WidgetDao)obj;

                string Id = newwidgetDao.ID;
                string caption = newwidgetDao.Caption;
                string description = newwidgetDao.Description;
                int widgetTypeID = newwidgetDao.WidgetTypeID;
                bool isDynamic = newwidgetDao.IsDynamic;
                int filterid = newwidgetDao.FilterID;
                int attributeid = newwidgetDao.AttributeID;
                int dimensionID = newwidgetDao.DimensionID;
                string matrixID = newwidgetDao.MatrixID;
                string widgetQuery = newwidgetDao.WidgetQuery;
                int column = newwidgetDao.Column;
                int row = newwidgetDao.Row;
                int sizeX = newwidgetDao.SizeX;
                int sizeY = newwidgetDao.SizeY;
                int visualType = newwidgetDao.VisualType;
                int noofitem = newwidgetDao.NoOfItem;
                int noofyear = newwidgetDao.NoOfYear;
                int noofmonth = newwidgetDao.NoOfMonth;
                string ListOfEntityID = newwidgetDao.ListOfEntityID;

                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                var result = docx.Root.Elements("Widget_Table").Elements("Widget").Where(mid => Convert.ToString(mid.Element("ID").Value.Trim()) == Id).Select(m => m);

                if (result.Count() == 0)
                {
                    //We need to add by default MaxId = 0 while creating the table
                    XDocument document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                    XElement subjectElementworking = document1.Descendants("Widget_Table").FirstOrDefault();
                    if (subjectElementworking == null)
                    {
                        CreateTableElement("Widget_Table", xmlpath, TenantID);
                        document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                        subjectElementworking = document1.Descendants("Widget_Table").FirstOrDefault();
                    }

                    MetadataXML = GetWidgetXElement(newwidgetDao);
                    subjectElement2 = MetadataXML.Descendants("Widget_Table").FirstOrDefault();

                    subjectElementworking.Add(subjectElement2.Nodes());
                    //document1.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(document1, xmlpath, TenantID);
                }
                else
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(xmlpath);
                    var Updateresult = docx.Root.Elements("Widget_Table").Elements("Widget").Where(mid => Convert.ToString(mid.Element("ID").Value.Trim()) == Id).Select(m => m);
                    foreach (XElement item in Updateresult)
                    {
                        if (widgetTypeID == -100)
                        {
                            item.SetElementValue("Column", column);
                            item.SetElementValue("Row", row);
                            item.SetElementValue("SizeX", sizeX);
                            item.SetElementValue("SizeY", sizeY);
                        }
                        else
                        {
                            item.SetElementValue("Caption", caption);
                            item.SetElementValue("Description", description);
                            item.SetElementValue("WidgetTypeID", widgetTypeID);
                            item.SetElementValue("IsDynamic", isDynamic);
                            item.SetElementValue("FilterID", filterid);
                            item.SetElementValue("AttributeID", attributeid);
                            item.SetElementValue("DimensionID", dimensionID);
                            item.SetElementValue("MatrixID", matrixID);
                            item.SetElementValue("WidgetQuery", widgetQuery);
                            item.SetElementValue("Column", column);
                            item.SetElementValue("Row", row);
                            item.SetElementValue("SizeX", sizeX);
                            item.SetElementValue("SizeY", sizeY);
                            item.SetElementValue("VisualType", visualType);
                            item.SetElementValue("NoOfItem", noofitem);
                            item.SetElementValue("NoOfYear", noofyear);
                            item.SetElementValue("NoOfMonth", noofmonth);
                            item.SetElementValue("ListOfEntityID", ListOfEntityID);
                        }
                        string xmlelement = "<root><Widget_Table>";
                        MetadataXML = item;
                        xmlelement = xmlelement + MetadataXML.ToString() + "</Widget_Table></root>";
                        MetadataXML = XElement.Parse(xmlelement);
                    }
                    //docx.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
                }
            }
            else if (typeof(T).Name == "ModuleDao")
            {
                Encoding utf8 = new UTF8Encoding(false);
                ModuleDao moduleDao = (ModuleDao)obj;
                int Id = moduleDao.Id;
                string Caption = moduleDao.Caption;
                string Description = moduleDao.Description;
                int IsEnable = Convert.ToInt32(moduleDao.IsEnable);
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                var insertresult = docx.Root.Elements("Module_Table").Elements("Module").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                if (insertresult.Count() == 0)
                {


                    XDocument document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                    XElement subjectElementworking = document1.Descendants("Module_Table").FirstOrDefault();
                    if (subjectElementworking == null)
                    {
                        CreateTableElement("Module_Table", xmlpath, TenantID);
                        document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                        subjectElementworking = document1.Descendants("Module_Table").FirstOrDefault();
                    }

                    var maxid = subjectElementworking.Attribute("maxid");
                    int maxids = Convert.ToInt32(maxid.Value) + 1;
                    subjectElementworking.SetAttributeValue("maxid", maxids.ToString());
                    moduleDao.Id = maxids;

                    MetadataXML = GetXElement(moduleDao);
                    subjectElement2 = XElement.Parse(MetadataXML.Descendants("Module_Table").FirstOrDefault().ToString());
                    subjectElementworking.Add(subjectElement2.Nodes());
                    //document1.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(document1, xmlpath, TenantID);
                }
                else
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(xmlpath);
                    var Updateresult = docx.Root.Elements("Module_Table").Elements("Module").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                    foreach (XElement item in Updateresult)
                    {
                        item.SetElementValue("Caption", Caption);
                        item.SetElementValue("Description", Description);
                        item.SetElementValue("IsEnable", IsEnable);
                        string xmlelement = "<root><Module_Table>";
                        MetadataXML = item;
                        xmlelement = xmlelement + MetadataXML.ToString() + "</Module_Table></root>";
                        MetadataXML = XElement.Parse(xmlelement);

                        //MetadataXML = xmlelement;
                    }
                    //docx.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
                }
            }
            else if (typeof(T).Name == "ModuleFeatureDao")
            {
                ModuleFeatureDao modfeaturedao = (ModuleFeatureDao)obj;
                int moduleid = modfeaturedao.Moduleid;
                int? featureid = modfeaturedao.Featureid;
                bool IsEnable = modfeaturedao.IsEnable;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                var insertresult = docx.Root.Elements("Module_Feature_Table").Elements("Module_Feature").Where(mid => (Convert.ToInt32(mid.Element("ModuleID").Value) == moduleid) && (Convert.ToInt32(mid.Element("FeatureID").Value) == featureid)).Select(m => m);
                if (insertresult.Count() == 0)
                {
                    MetadataXML = GetXElement(modfeaturedao);
                    subjectElement2 = MetadataXML.Descendants("Module_Feature_Table").FirstOrDefault();
                    XDocument document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                    XElement subjectElementworking = document1.Descendants("Module_Feature_Table").FirstOrDefault();
                    if (subjectElementworking == null)
                    {
                        CreateTableElement("Module_Feature_Table", xmlpath, TenantID);
                        document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                        subjectElementworking = document1.Descendants("Module_Feature_Table").FirstOrDefault();
                    }
                    subjectElementworking.Add(subjectElement2.Nodes());
                    //document1.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(document1, xmlpath, TenantID);
                }
                else
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(xmlpath);
                    var Updateresult = docx.Root.Elements("Module_Feature_Table").Elements("Module_Feature").Where(mid => (Convert.ToInt32(mid.Element("ModuleID").Value) == moduleid) && (Convert.ToInt32(mid.Element("FeatureID").Value) == featureid)).Select(m => m);
                    foreach (XElement item in Updateresult)
                    {
                        item.SetElementValue("IsEnable", IsEnable);
                        string xmlelement = "<root><Module_Feature_Table>";
                        MetadataXML = item;
                        xmlelement = xmlelement + MetadataXML.ToString() + "</Module_Feature_Table></root>";
                        MetadataXML = XElement.Parse(xmlelement);
                    }
                    //docx.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
                }
            }
            else if (typeof(T).Name == "EntityTypeFeatureDao")
            {
                EntityTypeFeatureDao entityfeaturedao = (EntityTypeFeatureDao)obj;
                int id = entityfeaturedao.Id;
                int typeid = entityfeaturedao.TypeID;
                int featureid = entityfeaturedao.FeatureID;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                MetadataXML = GetXElement(entityfeaturedao);
                var insertresult = docx.Root.Elements("EntityType_Feature_Table").Elements("EntityType_Feature").Where(mid => Convert.ToInt32(mid.Element("TypeID").Value) == typeid && Convert.ToInt32(mid.Element("FeatureID").Value) == featureid).Select(m => m);
                if (insertresult.Count() == 0)
                {
                    XDocument document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                    XElement subjectElementworking = document1.Descendants("EntityType_Feature_Table").FirstOrDefault();
                    if (subjectElementworking == null)
                    {
                        CreateTableElement("EntityType_Feature_Table", xmlpath, TenantID);
                        document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                        subjectElementworking = document1.Descendants("EntityType_Feature_Table").FirstOrDefault();
                    }

                    var maxid = subjectElementworking.Attribute("maxid");
                    int maxids = Convert.ToInt32(maxid.Value) + 1;
                    subjectElementworking.SetAttributeValue("maxid", maxids.ToString());
                    entityfeaturedao.Id = maxids;

                    MetadataXML = GetXElement(entityfeaturedao);
                    subjectElement2 = MetadataXML.Descendants("EntityType_Feature_Table").FirstOrDefault();
                    subjectElementworking.Add(subjectElement2.Nodes());
                    //document1.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(document1, xmlpath, TenantID);
                }
                else
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(xmlpath);
                    var Updateresult = docx.Root.Elements("EntityType_Feature_Table").Elements("EntityType_Feature").Where(mid => Convert.ToInt32(mid.Element("TypeID").Value) == typeid && Convert.ToInt32(mid.Element("FeatureID").Value) == featureid).Select(m => m);
                    foreach (XElement item in Updateresult)
                    {
                        item.SetElementValue("TypeID", typeid);
                        item.SetElementValue("FeatureID", featureid);
                        string xmlelement = "<root><EntityType_Feature_Table>";
                        MetadataXML = item;
                        xmlelement = xmlelement + MetadataXML.ToString() + "</EntityType_Feature_Table></root>";
                        MetadataXML = XElement.Parse(xmlelement);
                    }
                    //docx.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
                }

            }
            else if (typeof(T).Name == "EntityTypeAttributeRelationDao")
            {
                try
                {

                    EntityTypeAttributeRelationDao entitytyperelationdao = (EntityTypeAttributeRelationDao)obj;
                    int ID = entitytyperelationdao.ID;
                    int entitytypeid = entitytyperelationdao.EntityTypeID;
                    int attributeid = entitytyperelationdao.AttributeID;
                    string validationid = entitytyperelationdao.ValidationID;
                    int sortorder = entitytyperelationdao.SortOrder;
                    string defaultvalue = entitytyperelationdao.DefaultValue;
                    int inheritfromparent = entitytyperelationdao.InheritFromParent ? 1 : 0;
                    int isreadonly = entitytyperelationdao.IsReadOnly ? 1 : 0;
                    int choosefromparentonly = entitytyperelationdao.ChooseFromParentOnly ? 1 : 0;
                    int isvalidationneeded = entitytyperelationdao.IsValidationNeeded ? 1 : 0;
                    string caption = entitytyperelationdao.Caption;
                    bool issystemdefined = entitytyperelationdao.IsSystemDefined;
                    string placeholder = entitytyperelationdao.PlaceHolderValue;
                    int MinValue = entitytyperelationdao.MinValue;
                    int MaxValue = entitytyperelationdao.MaxValue;
                    int IsHelptextEnabled = entitytyperelationdao.IsHelptextEnabled ? 1 : 0;
                    string HelptextDecsription = entitytyperelationdao.HelptextDecsription;

                    XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                    var insertresult = docx.Root.Elements("EntityTypeAttributeRelation_Table").Elements("EntityTypeAttributeRelation").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == ID).Select(m => m);
                    if (insertresult.Count() == 0)
                    {

                        XDocument document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                        XElement subjectElementworking = document1.Descendants("EntityTypeAttributeRelation_Table").FirstOrDefault();
                        if (subjectElementworking == null)
                        {
                            CreateTableElement("EntityTypeAttributeRelation_Table", xmlpath, TenantID);
                            document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                            subjectElementworking = document1.Descendants("EntityTypeAttributeRelation_Table").FirstOrDefault();
                        }

                        entitytyperelationdao.ID = objMaxID;

                        MetadataXML = GetXElement(entitytyperelationdao);
                        subjectElement2 = MetadataXML.Descendants("EntityTypeAttributeRelation_Table").FirstOrDefault();
                        subjectElementworking.Add(subjectElement2.Nodes());
                        //document1.Save(xmlpath);
                        MarcomCacheDal<bool>.SaveXML(document1, xmlpath, TenantID);
                    }
                    else
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(xmlpath);
                        var Updateresult = docx.Root.Elements("EntityTypeAttributeRelation_Table").Elements("EntityTypeAttributeRelation").Where(mid => (Convert.ToInt32(mid.Element("ID").Value) == ID)).Select(m => m);
                        foreach (XElement item in Updateresult)
                        {
                            item.SetElementValue("EntityTypeID", entitytypeid);
                            item.SetElementValue("AttributeID", attributeid);
                            item.SetElementValue("ValidationID", validationid);
                            item.SetElementValue("SortOrder", sortorder);
                            item.SetElementValue("DefaultValue", defaultvalue);
                            item.SetElementValue("InheritFromParent", Convert.ToInt32(inheritfromparent).ToString());
                            item.SetElementValue("IsReadOnly", Convert.ToInt32(isreadonly).ToString());
                            item.SetElementValue("ChooseFromParentOnly", choosefromparentonly);
                            item.SetElementValue("IsValidationNeeded", Convert.ToInt32(isvalidationneeded).ToString());
                            item.SetElementValue("IsSystemDefined", Convert.ToInt32(issystemdefined).ToString());
                            item.SetElementValue("Caption", caption);
                            item.SetElementValue("PlaceHolderValue", placeholder);
                            item.SetElementValue("MinValue", MinValue);
                            item.SetElementValue("MaxValue", MaxValue);
                            item.SetElementValue("IsHelptextEnabled", IsHelptextEnabled);
                            item.SetElementValue("HelptextDecsription", HelptextDecsription);
                            string xmlelement = "<root><EntityTypeAttributeRelation_Table>";
                            MetadataXML = item;
                            xmlelement = xmlelement + MetadataXML.ToString() + "</EntityTypeAttributeRelation_Table></root>";
                            MetadataXML = XElement.Parse(xmlelement);
                        }
                        //docx.Save(xmlpath);
                        MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
                    }
                    XDocument docx1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                    docx1.Root.SetAttributeValue("IsEdited", "1");
                    //docx1.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(docx1, xmlpath, TenantID);
                }
                catch (Exception ex)
                { throw ex; }
            }
            else if (typeof(T).Name == "AttributeTypeDao")
            {
                AttributeTypeDao attribtypedao = (AttributeTypeDao)obj;
                int Id = attribtypedao.Id;
                string Caption = attribtypedao.Caption;
                string ClassName = attribtypedao.ClassName;
                bool IsSelectable = attribtypedao.IsSelectable;
                string DataType = attribtypedao.DataType;
                string SqlType = attribtypedao.SqlType;
                int Length = attribtypedao.Length;
                bool IsNullable = attribtypedao.IsNullable;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                var insertresult = docx.Root.Elements("AttributeType_Table").Elements("AttributeType").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                if (insertresult.Count() == 0)
                {

                    XDocument document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                    XElement subjectElementworking = document1.Descendants("AttributeType_Table").FirstOrDefault();
                    if (subjectElementworking == null)
                    {
                        CreateTableElement("AttributeType_Table", xmlpath, TenantID);
                        document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                        subjectElementworking = document1.Descendants("AttributeType_Table").FirstOrDefault();
                    }
                    var maxid = subjectElementworking.Attribute("maxid");
                    int maxids = Convert.ToInt32(maxid.Value) + 1;
                    subjectElementworking.SetAttributeValue("maxid", maxids.ToString());
                    attribtypedao.Id = maxids;
                    MetadataXML = GetXElement(attribtypedao);
                    subjectElement2 = MetadataXML.Descendants("AttributeType_Table").FirstOrDefault();
                    subjectElementworking.Add(subjectElement2.Nodes());
                    //document1.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(document1, xmlpath, TenantID);
                }
                else
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(xmlpath);
                    var Updateresult = docx.Root.Elements("AttributeType_Table").Elements("AttributeType").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                    foreach (XElement item in Updateresult)
                    {
                        item.SetElementValue("Caption", Caption);
                        item.SetElementValue("ClassName", ClassName);
                        item.SetElementValue("IsSelectable", IsSelectable);
                        item.SetElementValue("DataType", DataType);
                        item.SetElementValue("SqlType", SqlType);
                        item.SetElementValue("Length", Length);
                        item.SetElementValue("IsNullable", IsNullable);

                        string xmlelement = "<root><AttributeType_Table>";
                        MetadataXML = item;
                        xmlelement = xmlelement + MetadataXML.ToString() + "</AttributeType_Table></root>";
                        MetadataXML = XElement.Parse(xmlelement);
                    }
                    //docx.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
                }
            }
            else if (typeof(T).Name == "AttributeDao")
            {
                AttributeDao attribdao = (AttributeDao)obj;
                int Id = attribdao.Id;
                string Caption = attribdao.Caption;
                string Description = attribdao.Description;
                int AttributeTypeID = attribdao.AttributeTypeID;
                bool IsSystemDefined = attribdao.IsSystemDefined;
                bool IsSpecial = attribdao.IsSpecial;

                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                var insertresult = docx.Root.Elements("Attribute_Table").Elements("Attribute").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                if (insertresult.Count() == 0)
                {

                    XDocument document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                    XElement subjectElementworking = document1.Descendants("Attribute_Table").FirstOrDefault();
                    if (subjectElementworking == null)
                    {
                        CreateTableElement("Attribute_Table", xmlpath, TenantID);
                        document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                        subjectElementworking = document1.Descendants("Attribute_Table").FirstOrDefault();
                    }

                    attribdao.Id = objMaxID;

                    MetadataXML = GetXElement(attribdao);

                    subjectElement2 = MetadataXML.Descendants("Attribute_Table").FirstOrDefault();
                    subjectElementworking.Add(subjectElement2.Nodes());
                    //document1.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(document1, xmlpath, TenantID);
                }
                else
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(xmlpath);
                    var Updateresult = docx.Root.Elements("Attribute_Table").Elements("Attribute").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                    foreach (XElement item in Updateresult)
                    {
                        if (item.Element("IsSystemDefined").Value.Trim().ToUpper() != "TRUE")
                        {
                            item.SetElementValue("Caption", Caption);
                            item.SetElementValue("Description", Description);
                            item.SetElementValue("AttributeTypeID", AttributeTypeID);
                            item.SetElementValue("IsSystemDefined", Convert.ToInt32(IsSystemDefined).ToString());
                            item.SetElementValue("IsSpecial", Convert.ToInt32(IsSpecial).ToString());

                            string xmlelement = "<root><Attribute_Table>";
                            MetadataXML = item;
                            xmlelement = xmlelement + MetadataXML.ToString() + "</Attribute_Table></root>";
                            MetadataXML = XElement.Parse(xmlelement);
                        }

                    }
                    //docx.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
                }
                XDocument docx1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                docx1.Root.SetAttributeValue("IsEdited", "1");
                //docx1.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx1, xmlpath, TenantID);
            }
            else if (typeof(T).Name == "OptionDao")
            {
                OptionDao option = (OptionDao)obj;
                int Id = option.Id;
                string Caption = option.Caption;
                int AttributeId = option.AttributeID;
                int SortOrder = option.SortOrder;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                var insertresult = docx.Root.Elements("Option_Table").Elements("Option").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                if (insertresult.Count() == 0)
                {

                    XDocument document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                    XElement subjectElementworking = document1.Descendants("Option_Table").FirstOrDefault();
                    if (subjectElementworking == null)
                    {
                        CreateTableElement("Option_Table", xmlpath, TenantID);
                        document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                        subjectElementworking = document1.Descendants("Option_Table").FirstOrDefault();
                    }

                    option.Id = objMaxID;
                    MetadataXML = GetXElement(option);
                    subjectElement2 = MetadataXML.Descendants("Option_Table").FirstOrDefault();
                    subjectElementworking.Add(subjectElement2.Nodes());
                    //document1.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(document1, xmlpath, TenantID);
                }
                else
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(xmlpath);
                    var Updateresult = docx.Root.Elements("Option_Table").Elements("Option").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                    foreach (XElement item in Updateresult)
                    {
                        item.SetElementValue("Caption", Caption);
                        item.SetElementValue("AttributeID", AttributeId);
                        item.SetElementValue("SortOrder", SortOrder);

                        string xmlelement = "<root><Option_Table>";
                        MetadataXML = item;
                        xmlelement = xmlelement + MetadataXML.ToString() + "</Option_Table></root>";
                        MetadataXML = XElement.Parse(xmlelement);

                    }
                    //docx.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
                }
            }

            else if (typeof(T).Name == "AttributeSequenceDao")
            {
                AttributeSequenceDao option = (AttributeSequenceDao)obj;
                int Id = option.Id;
                string Pattern = option.Pattern;
                int AttributeId = option.AttributeID;
                int StartWithValue = option.StartWithValue;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                var insertresult = docx.Root.Elements("AttributeSequence_Table").Elements("AttributeSequence").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                if (insertresult.Count() == 0)
                {

                    XDocument document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                    XElement subjectElementworking = document1.Descendants("AttributeSequence_Table").FirstOrDefault();
                    if (subjectElementworking == null)
                    {
                        CreateTableElement("AttributeSequence_Table", xmlpath, TenantID);
                        document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                        subjectElementworking = document1.Descendants("AttributeSequence_Table").FirstOrDefault();
                    }

                    option.Id = objMaxID;
                    MetadataXML = GetXElement(option);
                    subjectElement2 = MetadataXML.Descendants("AttributeSequence_Table").FirstOrDefault();
                    subjectElementworking.Add(subjectElement2.Nodes());
                    //document1.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(document1, xmlpath, TenantID);
                }
                else
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(xmlpath);
                    var Updateresult = docx.Root.Elements("AttributeSequence_Table").Elements("AttributeSequence").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                    foreach (XElement item in Updateresult)
                    {
                        item.SetElementValue("Pattern", Pattern);
                        item.SetElementValue("AttributeID", AttributeId);
                        item.SetElementValue("StartWithValue", StartWithValue);

                        string xmlelement = "<root><AttributeSequence_Table>";
                        MetadataXML = item;
                        xmlelement = xmlelement + MetadataXML.ToString() + "</AttributeSequence_Table></root>";
                        MetadataXML = XElement.Parse(xmlelement);

                    }
                    //docx.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
                }
                XDocument docx1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                docx1.Root.SetAttributeValue("IsEdited", "1");
                //docx1.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx1, xmlpath, TenantID);
            }
            else if (typeof(T).Name == "TagOptionDao")
            {
                TagOptionDao option = (TagOptionDao)obj;
                int Id = option.Id;
                string Caption = option.Caption;
                int AttributeId = option.AttributeID;
                int SortOrder = option.SortOrder;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                var insertresult = docx.Root.Elements("TagOption_Table").Elements("TagOption").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                if (insertresult.Count() == 0)
                {

                    XDocument document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                    XElement subjectElementworking = document1.Descendants("TagOption_Table").FirstOrDefault();
                    if (subjectElementworking == null)
                    {
                        CreateTableElement("TagOption_Table", xmlpath, TenantID);
                        document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                        subjectElementworking = document1.Descendants("TagOption_Table").FirstOrDefault();
                    }

                    option.Id = objMaxID;
                    option.TotalHits = 1;
                    MetadataXML = GetXElement(option);
                    subjectElement2 = MetadataXML.Descendants("TagOption_Table").FirstOrDefault();
                    subjectElementworking.Add(subjectElement2.Nodes());
                    //document1.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(document1, xmlpath, TenantID);
                }
                else
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(xmlpath);
                    var Updateresult = docx.Root.Elements("TagOption_Table").Elements("TagOption").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                    foreach (XElement item in Updateresult)
                    {
                        item.SetElementValue("Caption", Caption);
                        item.SetElementValue("AttributeID", AttributeId);
                        item.SetElementValue("SortOrder", SortOrder);
                        //item.SetElementValue("UserID", option.UserID);
                        item.SetElementValue("CreateOn", item.Element("CreateOn").Value);
                        item.SetElementValue("UsedOn", option.UsedOn);
                        item.SetElementValue("TotalHits", (Convert.ToInt32(item.Element("TotalHits").Value) + 1));
                        item.SetElementValue("PopularTagsToShow", option.PopularTagsToShow);

                        string xmlelement = "<root><TagOption_Table>";
                        MetadataXML = item;
                        xmlelement = xmlelement + MetadataXML.ToString() + "</TagOption_Table></root>";
                        MetadataXML = XElement.Parse(xmlelement);

                    }
                    //docx.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
                }
                XDocument docx1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                docx1.Root.SetAttributeValue("IsEdited", "1");
                //docx1.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx1, xmlpath, TenantID);
            }
            else if (typeof(T).Name == "PopularTagWordsDao")
            {
                PopularTagWordsDao poptagoption = (PopularTagWordsDao)obj;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                var insertresult = docx.Root.Elements("PopularTagWords_Table").Elements("PopularTagWords").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == poptagoption.Id).Select(m => m);
                if (insertresult.Count() == 0)
                {
                    poptagoption.Id = objMaxID;
                    poptagoption.TotalHits = 1;
                    MetadataXML = GetXElement(poptagoption);
                }
            }
            else if (typeof(T).Name == "MultiSelectDao")
            {
                MultiSelectDao mulsel = (MultiSelectDao)obj;
                int EntityId = mulsel.Entityid;
                int AttributeId = mulsel.Attributeid;
                int OptionId = mulsel.Optionid;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);

                XDocument document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                XElement subjectElementworking = document1.Descendants("MultiSelect_Table").FirstOrDefault();
                if (subjectElementworking == null)
                {
                    CreateTableElement("MultiSelect_Table", xmlpath, TenantID);
                    document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                    subjectElementworking = document1.Descendants("MultiSelect_Table").FirstOrDefault();
                }
                var maxid = subjectElementworking.Attribute("maxid");
                int maxids = Convert.ToInt32(maxid.Value) + 1;
                subjectElementworking.SetAttributeValue("maxid", maxids.ToString());
                mulsel.Id = maxids;
                MetadataXML = GetXElement(mulsel);
                subjectElement2 = MetadataXML.Descendants("MultiSelect_Table").FirstOrDefault();
                subjectElementworking.Add(subjectElement2.Nodes());
                //document1.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(document1, xmlpath, TenantID);
            }
            else if (typeof(T).Name == "TreeLevelDao")
            {
                TreeLevelDao treelvl = (TreeLevelDao)obj;
                int id = treelvl.Id;
                int Level = treelvl.Level;
                string LevelName = treelvl.LevelName;
                int IsPercentage = treelvl.IsPercentage ? 1 : 0;
                int AttributeId = treelvl.AttributeID;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                var insertresult = docx.Root.Elements("TreeLevel_Table").Elements("TreeLevel").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == id).Select(m => m);
                if (insertresult.Count() == 0)
                {

                    XDocument document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                    XElement subjectElementworking = document1.Descendants("TreeLevel_Table").FirstOrDefault();
                    if (subjectElementworking == null)
                    {
                        CreateTableElement("TreeLevel_Table", xmlpath, TenantID);
                        document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                        subjectElementworking = document1.Descendants("TreeLevel_Table").FirstOrDefault();
                    }

                    treelvl.Id = objMaxID;
                    MetadataXML = GetXElement(treelvl);
                    subjectElement2 = MetadataXML.Descendants("TreeLevel_Table").FirstOrDefault();
                    subjectElementworking.Add(subjectElement2.Nodes());
                    //document1.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(document1, xmlpath, TenantID);
                }
                else
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(xmlpath);
                    var Updateresult = docx.Root.Elements("TreeLevel_Table").Elements("TreeLevel").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == id && Convert.ToInt32(mid.Element("AttributeID").Value) == AttributeId).Select(m => m);
                    foreach (XElement item in Updateresult)
                    {
                        item.SetElementValue("Level", Level);
                        item.SetElementValue("AttributeId", AttributeId);
                        item.SetElementValue("LevelName", LevelName);
                        item.SetElementValue("IsPercentage", IsPercentage);

                        string xmlelement = "<root><TreeLevel_Table>";
                        MetadataXML = item;
                        xmlelement = xmlelement + MetadataXML.ToString() + "</TreeLevel_Table></root>";
                        MetadataXML = XElement.Parse(xmlelement);
                    }
                    //docx.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
                }
            }
            else if (typeof(T).Name == "TreeNodeDao")
            {
                TreeNodeDao treenode = (TreeNodeDao)obj;
                int Id = treenode.Id;
                int NodeID = treenode.NodeID;
                int ParentNodeID = treenode.ParentNodeID;
                int Level = treenode.Level;
                string KEY = treenode.KEY;
                int AttributeId = treenode.AttributeID;
                string Caption = treenode.Caption;
                int SortOrder = treenode.SortOrder;
                string ColorCode = treenode.ColorCode;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                var insertresult = docx.Root.Elements("TreeNode_Table").Elements("TreeNode").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                if (insertresult.Count() == 0)
                {

                    XDocument document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                    XElement subjectElementworking = document1.Descendants("TreeNode_Table").FirstOrDefault();
                    if (subjectElementworking == null)
                    {
                        CreateTableElement("TreeNode_Table", xmlpath, TenantID);
                        document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                        subjectElementworking = document1.Descendants("TreeNode_Table").FirstOrDefault();
                    }

                    treenode.Id = objMaxID;
                    MetadataXML = GetXElement(treenode);
                    subjectElement2 = MetadataXML.Descendants("TreeNode_Table").FirstOrDefault();
                    subjectElementworking.Add(subjectElement2.Nodes());
                    //document1.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(document1, xmlpath, TenantID);
                }
                else
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(xmlpath);
                    var Updateresult = docx.Root.Elements("TreeNode_Table").Elements("TreeNode").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id && Convert.ToInt32(mid.Element("AttributeID").Value) == AttributeId).Select(m => m);
                    foreach (XElement item in Updateresult)
                    {
                        item.SetElementValue("NodeID", NodeID);
                        item.SetElementValue("ParentNodeID", ParentNodeID);
                        item.SetElementValue("Level", Level);
                        item.SetElementValue("KEY", KEY);
                        item.SetElementValue("AttributeID", AttributeId);
                        item.SetElementValue("Caption", Caption);
                        item.SetElementValue("SortOrder", SortOrder);
                        item.SetElementValue("ColorCode", ColorCode);

                        string xmlelement = "<root><TreeNode_Table>";
                        MetadataXML = item;
                        xmlelement = xmlelement + MetadataXML.ToString() + "</TreeNode_Table></root>";
                        MetadataXML = XElement.Parse(xmlelement);
                    }
                    //docx.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
                }
            }
            else if (typeof(T).Name == "TreeValueDao")
            {
                TreeValueDao treeval = (TreeValueDao)obj;
                int Id = treeval.Id;
                int AttributeID = treeval.Attributeid;
                int NodeID = treeval.Nodeid;
                string Value = treeval.Value;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                var insertresult = docx.Root.Elements("TreeValue_Table").Elements("TreeValue").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                if (insertresult.Count() == 0)
                {

                    XDocument document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                    XElement subjectElementworking = document1.Descendants("TreeValue_Table").FirstOrDefault();
                    if (subjectElementworking == null)
                    {
                        CreateTableElement("TreeValue_Table", xmlpath, TenantID);
                        document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                        subjectElementworking = document1.Descendants("TreeValue_Table").FirstOrDefault();
                    }
                    var maxid = subjectElementworking.Attribute("maxid");
                    int maxids = Convert.ToInt32(maxid.Value) + 1;
                    subjectElementworking.SetAttributeValue("maxid", maxids.ToString());
                    treeval.Id = maxids;
                    MetadataXML = GetXElement(treeval);

                    subjectElement2 = MetadataXML.Descendants("TreeValue_Table").FirstOrDefault();
                    subjectElementworking.Add(subjectElement2.Nodes());
                    //document1.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(document1, xmlpath, TenantID);
                }
                else
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(xmlpath);
                    var Updateresult = docx.Root.Elements("TreeValue_Table").Elements("TreeValue").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                    foreach (XElement item in Updateresult)
                    {
                        item.SetElementValue("Attributeid", AttributeID);
                        item.SetElementValue("Nodeid", NodeID);
                        item.SetElementValue("Value", Value);

                        string xmlelement = "<root><TreeValue_Table>";
                        MetadataXML = item;
                        xmlelement = xmlelement + MetadataXML.ToString() + "</TreeValue_Table></root>";
                        MetadataXML = XElement.Parse(xmlelement);
                    }
                    //docx.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
                }
            }
            else if (typeof(T).Name == "ValidationDao")
            {
                ValidationDao valid = (ValidationDao)obj;
                int Id = valid.Id;
                int EntityTyepID = valid.EntityTypeID;
                int RelationShipID = valid.RelationShipID;
                string Name = valid.Name;
                string ValueType = valid.ValueType;
                string Value = valid.Value;
                string ErrorMessage = valid.ErrorMessage;

                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                var insertresult = docx.Root.Elements("Validation_Table").Elements("Validation").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                if (insertresult.Count() == 0)
                {

                    XDocument document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                    XElement subjectElementworking = document1.Descendants("Validation_Table").FirstOrDefault();
                    if (subjectElementworking == null)
                    {
                        CreateTableElement("Validation_Table", xmlpath, TenantID);
                        document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                        subjectElementworking = document1.Descendants("Validation_Table").FirstOrDefault();
                    }
                    valid.Id = objMaxID;
                    MetadataXML = GetXElement(valid);
                    subjectElement2 = MetadataXML.Descendants("Validation_Table").FirstOrDefault();
                    subjectElementworking.Add(subjectElement2.Nodes());
                    //document1.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(document1, xmlpath, TenantID);
                }
                else
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(xmlpath);
                    var Updateresult = docx.Root.Elements("Validation_Table").Elements("Validation").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                    foreach (XElement item in Updateresult)
                    {
                        item.SetElementValue("EntityTypeID", EntityTyepID);
                        item.SetElementValue("RelationShipID", RelationShipID);
                        item.SetElementValue("Name", Name);
                        item.SetElementValue("ValueType", ValueType);
                        item.SetElementValue("Value", Value);
                        item.SetElementValue("ErrorMessage", ErrorMessage);
                        string xmlelement = "<root><Validation_Table>";
                        MetadataXML = item;
                        xmlelement = xmlelement + MetadataXML.ToString() + "</Validation_Table></root>";
                        MetadataXML = XElement.Parse(xmlelement);
                    }
                    //docx.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
                }
                XDocument docx1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                docx1.Root.SetAttributeValue("IsEdited", "1");
                //docx1.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx1, xmlpath, TenantID);
            }
            else if (typeof(T).Name == "EntityTypeHierarchyDao")
            {
                EntityTypeHierarchyDao entityHeirarchdao = (EntityTypeHierarchyDao)obj;
                int Id = entityHeirarchdao.Id;
                int patrentActivityTypeId = entityHeirarchdao.ParentActivityTypeID;
                int childActivityTypeId = entityHeirarchdao.ChildActivityTypeID;
                int sortOrder = entityHeirarchdao.SortOrder;

                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);

                var insertresult = docx.Root.Elements("EntityType_Hierarchy_Table").Elements("EntityType_Hierarchy").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                if (insertresult.Count() == 0)
                {

                    XDocument document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                    XElement subjectElementworking = document1.Descendants("EntityType_Hierarchy_Table").FirstOrDefault();
                    if (subjectElementworking == null)
                    {
                        CreateTableElement("EntityType_Hierarchy_Table", xmlpath, TenantID);
                        document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                        subjectElementworking = document1.Descendants("EntityType_Hierarchy_Table").FirstOrDefault();
                    }

                    entityHeirarchdao.Id = objMaxID;
                    MetadataXML = GetXElement(entityHeirarchdao);

                    subjectElement2 = MetadataXML.Descendants("EntityType_Hierarchy_Table").FirstOrDefault();
                    subjectElementworking.Add(subjectElement2.Nodes());
                    //document1.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(document1, xmlpath, TenantID);
                }
                else
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(xmlpath);
                    var Updateresult = docx.Root.Elements("EntityType_Hierarchy_Table").Elements("EntityType_Hierarchy").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                    foreach (XElement item in Updateresult)
                    {
                        item.SetElementValue("ParentActivityTypeID", patrentActivityTypeId);
                        item.SetElementValue("ChildActivityTypeID", childActivityTypeId);
                        item.SetElementValue("SortOrder", sortOrder);

                        string xmlelement = "<root><EntityType_Hierarchy_Table>";
                        MetadataXML = item;
                        xmlelement = xmlelement + MetadataXML.ToString() + "</EntityType_Hierarchy_Table></root>";
                        MetadataXML = XElement.Parse(xmlelement);
                    }
                    //docx.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
                }
            }
            else if (typeof(T).Name == "AttributeToAttributeRelationsDao")
            {
                try
                {

                    AttributeToAttributeRelationsDao entitytyperelationdao = (AttributeToAttributeRelationsDao)obj;
                    int ID = entitytyperelationdao.ID;
                    int entitytypeid = entitytyperelationdao.EntityTypeID;
                    int attributeid = entitytyperelationdao.AttributeID;
                    int attributetypeid = entitytyperelationdao.AttributeTypeID;
                    int attributeoptionid = entitytyperelationdao.AttributeOptionID;
                    int attributeidlevel = entitytyperelationdao.AttributeLevel;
                    string attributeidrelationid = entitytyperelationdao.AttributeRelationID;

                    XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                    var insertresult = docx.Root.Elements("AttributeToAttributeRelations_Table").Elements("AttributeToAttributeRelations").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == ID).Select(m => m);
                    if (insertresult.Count() == 0)
                    {

                        XDocument document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                        XElement subjectElementworking = document1.Descendants("AttributeToAttributeRelations_Table").FirstOrDefault();
                        if (subjectElementworking == null)
                        {
                            CreateTableElement("AttributeToAttributeRelations_Table", xmlpath, TenantID);
                            document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                            subjectElementworking = document1.Descendants("AttributeToAttributeRelations_Table").FirstOrDefault();
                        }

                        entitytyperelationdao.ID = objMaxID;

                        MetadataXML = GetXElement(entitytyperelationdao);
                        subjectElement2 = MetadataXML.Descendants("AttributeToAttributeRelations_Table").FirstOrDefault();
                        subjectElementworking.Add(subjectElement2.Nodes());
                        //document1.Save(xmlpath);
                        MarcomCacheDal<bool>.SaveXML(document1, xmlpath, TenantID);
                    }
                    else
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(xmlpath);
                        var Updateresult = docx.Root.Elements("AttributeToAttributeRelations_Table").Elements("AttributeToAttributeRelations").Where(mid => (Convert.ToInt32(mid.Element("ID").Value) == ID)).Select(m => m);
                        foreach (XElement item in Updateresult)
                        {
                            item.SetElementValue("EntityTypeID", entitytypeid);
                            item.SetElementValue("AttributeID", attributeid);
                            item.SetElementValue("ValidationID", attributetypeid);
                            item.SetElementValue("SortOrder", attributeoptionid);
                            item.SetElementValue("DefaultValue", attributeidlevel);
                            item.SetElementValue("InheritFromParent", attributeidrelationid);
                            string xmlelement = "<root><EntityTypeAttributeRelation_Table>";
                            MetadataXML = item;
                            xmlelement = xmlelement + MetadataXML.ToString() + "</EntityTypeAttributeRelation_Table></root>";
                            MetadataXML = XElement.Parse(xmlelement);
                        }
                        //docx.Save(xmlpath);
                        MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
                    }
                }
                catch (Exception ex)
                { throw ex; }
            }
            else if (typeof(T).Name == "EntityTypeStatusOptionsDao")
            {
                EntityTypeStatusOptionsDao option = (EntityTypeStatusOptionsDao)obj;
                int Id = option.ID;
                int EntityTypeID = option.EntityTypeID;
                String Option = option.StatusOptions;
                int SortOrder = option.SortOrder;
                string ColorCode = option.ColorCode;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                var insertresult = docx.Root.Elements("EntityTypeStatus_Options_Table").Elements("EntityTypeStatus_Options").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                if (insertresult.Count() == 0)
                {

                    XDocument document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                    XElement subjectElementworking = document1.Descendants("EntityTypeStatus_Options_Table").FirstOrDefault();
                    if (subjectElementworking == null)
                    {
                        CreateTableElement("EntityTypeStatus_Options_Table", xmlpath, TenantID);
                        document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                        subjectElementworking = document1.Descendants("EntityTypeStatus_Options_Table").FirstOrDefault();
                    }
                    option.ID = objMaxID;
                    MetadataXML = GetXElement(option);
                    subjectElement2 = MetadataXML.Descendants("EntityTypeStatus_Options_Table").FirstOrDefault();
                    subjectElementworking.Add(subjectElement2.Nodes());
                    //document1.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(document1, xmlpath, TenantID);
                }
                else
                {
                    //XmlDocument doc = new XmlDocument();
                    //doc.Load(xmlpath);
                    var Updateresult = docx.Root.Elements("EntityTypeStatus_Options_Table").Elements("EntityTypeStatus_Options").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                    foreach (XElement item in Updateresult)
                    {
                        item.SetElementValue("StatusOptions", Option);
                        item.SetElementValue("SortOrder", SortOrder);
                        item.SetElementValue("ColorCode", ColorCode);
                        string xmlelement = "<root><EntityTypeStatus_Options_Table>";
                        MetadataXML = item;
                        xmlelement = xmlelement + MetadataXML.ToString() + "</EntityTypeStatus_Options_Table></root>";
                        MetadataXML = XElement.Parse(xmlelement);

                    }
                    //docx.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
                }
            }
            else if (typeof(T).Name == "DamTypeFileExtensionDao")
            {
                DamTypeFileExtensionDao option = (DamTypeFileExtensionDao)obj;
                int Id = option.ID;
                int EntityTypeID = option.EntityTypeID;
                String Option = option.ExtensionOptions;
                int SortOrder = option.SortOrder;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                var insertresult = docx.Root.Elements("DamTypeFileExtension_Options_Table").Elements("DamTypeFileExtension_Options").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                if (insertresult.Count() == 0)
                {

                    XDocument document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                    XElement subjectElementworking = document1.Descendants("DamTypeFileExtension_Options_Table").FirstOrDefault();
                    if (subjectElementworking == null)
                    {
                        CreateTableElement("DamTypeFileExtension_Options_Table", xmlpath, TenantID);
                        document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                        subjectElementworking = document1.Descendants("DamTypeFileExtension_Options_Table").FirstOrDefault();
                    }
                    option.ID = objMaxID;
                    MetadataXML = GetXElement(option);
                    subjectElement2 = MetadataXML.Descendants("DamTypeFileExtension_Options_Table").FirstOrDefault();
                    subjectElementworking.Add(subjectElement2.Nodes());
                    //document1.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(document1, xmlpath, TenantID);
                }
                else
                {
                    //XmlDocument doc = new XmlDocument();
                    //doc.Load(xmlpath);
                    var Updateresult = docx.Root.Elements("DamTypeFileExtension_Options_Table").Elements("DamTypeFileExtension_Options").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                    foreach (XElement item in Updateresult)
                    {
                        item.SetElementValue("ExtensionOptions", Option);
                        item.SetElementValue("SortOrder", SortOrder);
                        string xmlelement = "<root><DamTypeFileExtension_Options_Table>";
                        MetadataXML = item;
                        xmlelement = xmlelement + MetadataXML.ToString() + "</DamTypeFileExtension_Options_Table></root>";
                        MetadataXML = XElement.Parse(xmlelement);

                    }
                    //docx.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
                }
            }
            else if (typeof(T).Name == "EntitytasktypeDao")
            {
                EntitytasktypeDao entityTaskType = (EntitytasktypeDao)obj;
                int Id = entityTaskType.Id;
                int entityTypeID = entityTaskType.EntitytypeId;
                int tasktypeID = entityTaskType.TaskTypeId;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                var insertresult = docx.Root.Elements("EntityTaskType_Table").Elements("EntityTaskType").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                if (insertresult.Count() == 0)
                {

                    XDocument document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                    XElement subjectElementworking = document1.Descendants("EntityTaskType_Table").FirstOrDefault();
                    if (subjectElementworking == null)
                    {
                        CreateTableElement("EntityTaskType_Table", xmlpath, TenantID);
                        document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                        subjectElementworking = document1.Descendants("EntityTaskType_Table").FirstOrDefault();
                    }
                    entityTaskType.Id = objMaxID;
                    MetadataXML = GetXElement(entityTaskType);
                    subjectElement2 = MetadataXML.Descendants("EntityTaskType_Table").FirstOrDefault();
                    subjectElementworking.Add(subjectElement2.Nodes());
                    //document1.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(document1, xmlpath, TenantID);
                }
                else
                {
                    //XmlDocument doc = new XmlDocument();
                    //doc.Load(xmlpath);
                    var Updateresult = docx.Root.Elements("EntityTaskType_Table").Elements("EntityTaskType").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                    foreach (XElement item in Updateresult)
                    {
                        item.SetElementValue("EntitytypeId", entityTypeID);
                        item.SetElementValue("TaskTypeId", tasktypeID);
                        string xmlelement = "<root><EntityTaskType_Table>";
                        MetadataXML = item;
                        xmlelement = xmlelement + MetadataXML.ToString() + "</EntityTaskType_Table></root>";
                        MetadataXML = XElement.Parse(xmlelement);

                    }
                    //docx.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
                }
            }
            else if (typeof(T).Name == "EntityobjectiveTypeDao")
            {
                EntityobjectiveTypeDao entityObjectiveType = (EntityobjectiveTypeDao)obj;
                int Id = entityObjectiveType.Id;
                int entityTypeID = entityObjectiveType.EntitytypeId;
                int objectivetypeID = entityObjectiveType.ObjectiveTypeId;
                int objectivebaseTypeId = entityObjectiveType.ObjectiveBaseTypeId;
                int unitId = entityObjectiveType.UnitId;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                var insertresult = docx.Root.Elements("EntityObjectiveType_Table").Elements("EntityObjectiveType").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                if (insertresult.Count() == 0)
                {

                    XDocument document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                    XElement subjectElementworking = document1.Descendants("EntityObjectiveType_Table").FirstOrDefault();
                    if (subjectElementworking == null)
                    {
                        CreateTableElement("EntityObjectiveType_Table", xmlpath, TenantID);
                        document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                        subjectElementworking = document1.Descendants("EntityObjectiveType_Table").FirstOrDefault();
                    }
                    entityObjectiveType.Id = objMaxID;
                    MetadataXML = GetXElement(entityObjectiveType);
                    subjectElement2 = MetadataXML.Descendants("EntityObjectiveType_Table").FirstOrDefault();
                    subjectElementworking.Add(subjectElement2.Nodes());
                    //document1.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(document1, xmlpath, TenantID);
                }
                else
                {
                    //XmlDocument doc = new XmlDocument();
                    //doc.Load(xmlpath);
                    var Updateresult = docx.Root.Elements("EntityObjectiveType_Table").Elements("EntityObjectiveType").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                    foreach (XElement item in Updateresult)
                    {
                        item.SetElementValue("EntitytypeId", entityTypeID);
                        item.SetElementValue("ObjectiveTypeId", objectivetypeID);
                        item.SetElementValue("ObjectiveBaseTypeId", objectivebaseTypeId);
                        item.SetElementValue("UnitId", unitId);
                        string xmlelement = "<root><EntityObjectiveType_Table>";
                        MetadataXML = item;
                        xmlelement = xmlelement + MetadataXML.ToString() + "</EntityObjectiveType_Table></root>";
                        MetadataXML = XElement.Parse(xmlelement);

                    }
                    //docx.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
                }
            }
            else if (typeof(T).Name == "AttributeGroupDao")
            {
                AttributeGroupDao objDao = (AttributeGroupDao)obj;
                int Id = objDao.Id;
                string caption = objDao.Caption;
                bool IsPredefined = objDao.IsPredefined;

                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                var insertresult = docx.Root.Elements("AttributeGroup_Table").Elements("AttributeGroup").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);

                if (insertresult.Count() == 0)
                {

                    //We need to add by default MaxId = 0 while creating the table
                    XDocument document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                    XElement subjectElementworking = document1.Descendants("AttributeGroup_Table").FirstOrDefault();
                    if (subjectElementworking == null)
                    {
                        CreateTableElement("AttributeGroup_Table", xmlpath, TenantID);
                        document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                        subjectElementworking = document1.Descendants("AttributeGroup_Table").FirstOrDefault();
                    }

                    MetadataXML = GetXElement(objDao);
                    subjectElement2 = MetadataXML.Descendants("AttributeGroup_Table").FirstOrDefault();

                    subjectElementworking.Add(subjectElement2.Nodes());
                    //document1.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(document1, xmlpath, TenantID);
                }
                else
                {
                    //XmlDocument doc = new XmlDocument();
                    //doc.Load(xmlpath);
                    var Updateresult = docx.Root.Elements("AttributeGroup_Table").Elements("AttributeGroup").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                    foreach (XElement item in Updateresult)
                    {
                        item.SetElementValue("Caption", caption);
                        item.SetElementValue("Description", objDao.Description);
                        item.SetElementValue("IsPredefined", Convert.ToInt32(IsPredefined));

                        string xmlelement = "<root><AttributeGroup_Table>";
                        MetadataXML = item;
                        xmlelement = xmlelement + MetadataXML.ToString() + "</AttributeGroup_Table></root>";
                        MetadataXML = XElement.Parse(xmlelement);
                    }
                    //docx.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
                }
                XDocument docx1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                docx1.Root.SetAttributeValue("IsEdited", "1");
                //docx1.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx1, xmlpath, TenantID);
            }
            else if (typeof(T).Name == "AttributeGroupAttributeRelationDao")
            {
                AttributeGroupAttributeRelationDao objDao = (AttributeGroupAttributeRelationDao)obj;
                int Id = objDao.ID;
                string caption = objDao.Caption;
                int attributeID = objDao.AttributeID;
                bool IsSearchable = objDao.IsSearchable;
                bool ShowAsColumn = objDao.ShowAsColumn;

                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                var insertresult = docx.Root.Elements("AttributeGroupAttributeRelation_Table").Elements("AttributeGroupAttributeRelation").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);

                if (insertresult.Count() == 0)
                {

                    //We need to add by default MaxId = 0 while creating the table
                    XDocument document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                    XElement subjectElementworking = document1.Descendants("AttributeGroupAttributeRelation_Table").FirstOrDefault();
                    if (subjectElementworking == null)
                    {
                        CreateTableElement("AttributeGroupAttributeRelation_Table", xmlpath, TenantID);
                        document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                        subjectElementworking = document1.Descendants("AttributeGroupAttributeRelation_Table").FirstOrDefault();
                    }

                    MetadataXML = GetXElement(objDao);
                    subjectElement2 = MetadataXML.Descendants("AttributeGroupAttributeRelation_Table").FirstOrDefault();

                    subjectElementworking.Add(subjectElement2.Nodes());
                    //document1.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(document1, xmlpath, TenantID);
                }
                else
                {
                    //XmlDocument doc = new XmlDocument();
                    //doc.Load(xmlpath);
                    var Updateresult = docx.Root.Elements("AttributeGroupAttributeRelation_Table").Elements("AttributeGroupAttributeRelation").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                    foreach (XElement item in Updateresult)
                    {
                        item.SetElementValue("Caption", caption);
                        item.SetElementValue("AttributeID", attributeID);
                        item.SetElementValue("SortOrder", objDao.SortOrder);
                        item.SetElementValue("IsSearchable", Convert.ToInt32(IsSearchable));
                        item.SetElementValue("ShowAsColumn", Convert.ToInt32(ShowAsColumn));

                        string xmlelement = "<root><AttributeGroupAttributeRelation_Table>";
                        MetadataXML = item;
                        xmlelement = xmlelement + MetadataXML.ToString() + "</AttributeGroupAttributeRelation_Table></root>";
                        MetadataXML = XElement.Parse(xmlelement);
                    }
                    //docx.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
                }
                XDocument docx1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                docx1.Root.SetAttributeValue("IsEdited", "1");
                //docx1.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx1, xmlpath, TenantID);
            }
            else if (typeof(T).Name == "EntityTypeAttributeGroupRelationDao")
            {
                EntityTypeAttributeGroupRelationDao objDao = (EntityTypeAttributeGroupRelationDao)obj;
                int Id = objDao.ID;
                string caption = objDao.Caption;
                int attributeID = objDao.AttributeGroupID;
                int LocationType = objDao.LocationType;
                int RepresentationType = objDao.RepresentationType ? 1 : 0;
                int sortorder = objDao.SortOrder;
                int pagesize = objDao.PageSize;
                bool IsTabInEntityCreation = objDao.IsTabInEntityCreation;
                bool IsAttrGrpInheritFromParent = objDao.IsAttrGrpInheritFromParent;

                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                var insertresult = docx.Root.Elements("EntityTypeAttributeGroupRelation_Table").Elements("EntityTypeAttributeGroupRelation").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);

                if (insertresult.Count() == 0)
                {

                    //We need to add by default MaxId = 0 while creating the table
                    XDocument document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                    XElement subjectElementworking = document1.Descendants("EntityTypeAttributeGroupRelation_Table").FirstOrDefault();
                    if (subjectElementworking == null)
                    {
                        CreateTableElement("EntityTypeAttributeGroupRelation_Table", xmlpath, TenantID);
                        document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                        subjectElementworking = document1.Descendants("EntityTypeAttributeGroupRelation_Table").FirstOrDefault();
                    }

                    MetadataXML = GetXElement(objDao);
                    subjectElement2 = MetadataXML.Descendants("EntityTypeAttributeGroupRelation_Table").FirstOrDefault();

                    subjectElementworking.Add(subjectElement2.Nodes());
                    //document1.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(document1, xmlpath, TenantID);
                }
                else
                {
                    //XmlDocument doc = new XmlDocument();
                    //doc.Load(xmlpath);
                    var Updateresult = docx.Root.Elements("EntityTypeAttributeGroupRelation_Table").Elements("EntityTypeAttributeGroupRelation").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                    foreach (XElement item in Updateresult)
                    {
                        item.SetElementValue("Caption", caption);
                        item.SetElementValue("AttributeGroupID", attributeID);
                        item.SetElementValue("LocationType", LocationType);
                        item.SetElementValue("RepresentationType", RepresentationType);
                        item.SetElementValue("SortOrder", sortorder);
                        item.SetElementValue("PageSize", pagesize);
                        item.SetElementValue("IsTabInEntityCreation", Convert.ToInt32(IsTabInEntityCreation));
                        item.SetElementValue("IsAttrGrpInheritFromParent", Convert.ToInt32(IsAttrGrpInheritFromParent));

                        string xmlelement = "<root><EntityTypeAttributeGroupRelation_Table>";
                        MetadataXML = item;
                        xmlelement = xmlelement + MetadataXML.ToString() + "</EntityTypeAttributeGroupRelation_Table></root>";
                        MetadataXML = XElement.Parse(xmlelement);
                    }
                    //docx.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
                }
                XDocument docx1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                docx1.Root.SetAttributeValue("IsEdited", "1");
                //docx1.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx1, xmlpath, TenantID);
            }
            else if (typeof(T).Name == "UserVisibleInfoDao")
            {
                UserVisibleInfoDao objDao = (UserVisibleInfoDao)obj;
                int AttributeId = objDao.AttributeId;
                objDao.Id = objMaxID;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                var insertresult = docx.Root.Elements("UserVisibleInfo_Table").Elements("UserVisibleInfo").Where(mid => Convert.ToInt32(mid.Element("AttributeId").Value) == AttributeId).Select(m => m);
                if (insertresult.Count() == 0)
                {
                    //We need to add by default MaxId = 0 while creating the table
                    XDocument document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                    XElement subjectElementworking = document1.Descendants("UserVisibleInfo_Table").FirstOrDefault();
                    if (subjectElementworking == null)
                    {
                        CreateTableElement("UserVisibleInfo_Table", xmlpath, TenantID);
                        document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                        subjectElementworking = document1.Descendants("UserVisibleInfo_Table").FirstOrDefault();
                    }

                    MetadataXML = GetXElement(objDao);
                    subjectElement2 = MetadataXML.Descendants("UserVisibleInfo_Table").FirstOrDefault();

                    subjectElementworking.Add(subjectElement2.Nodes());
                    //document1.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(document1, xmlpath, TenantID);

                    XDocument xDoc = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                    var newxml = xDoc.Root.Elements("UserVisibleInfo_Table").FirstOrDefault();
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("<root>");
                    sb.AppendLine(newxml.ToString());
                    sb.AppendLine("</root>");
                    MetadataXML = XElement.Parse(sb.ToString().Replace(Environment.NewLine, ""));
                }

                else
                {
                    //XmlDocument doc = new XmlDocument();
                    //doc.Load(xmlpath);
                    //var Updateresult = docx.Root.Elements("UserVisibleInfo_Table").Elements("UserVisibleInfo").Where(mid => Convert.ToInt32(mid.Element("AttributeId").Value) == AttributeId).Select(m => m);
                    //foreach (XElement item in Updateresult)
                    //{
                    //    item.SetElementValue("AttributeID", AttributeId);

                    //    string xmlelement = "<root><UserVisibleInfo_Table>";
                    //    MetadataXML = item;
                    //    xmlelement = xmlelement + MetadataXML.ToString() + "</UserVisibleInfo_Table></root>";
                    //    MetadataXML = XElement.Parse(xmlelement);
                    //}
                    //docx.Save(xmlpath);

                    DeleteObject<UserVisibleInfoDao>(xmlpath, objDao, TenantID);

                }
                XDocument docx1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                docx1.Root.SetAttributeValue("IsEdited", "1");
                //docx1.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx1, xmlpath, TenantID);
            }
            else if (typeof(T).Name == "AttributeGroupRoleAccessDao")
            {
                AttributeGroupRoleAccessDao objDao = (AttributeGroupRoleAccessDao)obj;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                var insertresult = docx.Root.Elements("AttributeGroupRoleAccess_Table").Elements("AttributeGroupRoleAccess").Where(mid => Convert.ToInt32(mid.Element("AttributeGroupID").Value) == objDao.AttributeGroupID && Convert.ToInt32(mid.Element("ID").Value) == objDao.ID).Select(m => m);

                if (insertresult.Count() == 0)
                {
                    //We need to add by default MaxId = 0 while creating the table
                    XDocument document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                    XElement subjectElementworking = document1.Descendants("AttributeGroupRoleAccess_Table").FirstOrDefault();
                    if (subjectElementworking == null)
                    {
                        CreateTableElement("AttributeGroupRoleAccess_Table", xmlpath, TenantID);
                        document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                        subjectElementworking = document1.Descendants("AttributeGroupRoleAccess_Table").FirstOrDefault();
                    }

                    MetadataXML = GetXElement(objDao);
                    subjectElement2 = MetadataXML.Descendants("AttributeGroupRoleAccess_Table").FirstOrDefault();

                    subjectElementworking.Add(subjectElement2.Nodes());
                    //document1.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(document1, xmlpath, TenantID);
                }
                XDocument docx1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                docx1.Root.SetAttributeValue("IsEdited", "1");
                //docx1.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx1, xmlpath, TenantID);
            }
            else if (typeof(T).Name == "EntityTypeRoleAclDao")
            {
                EntityTypeRoleAclDao option = (EntityTypeRoleAclDao)obj;
                int Id = option.ID;
                int EntityTypeID = option.EntityTypeID;
                String Caption = option.Caption;
                int EntityRoleID = option.EntityRoleID;
                int ModuleID = option.ModuleID;
                int Sortorder = option.Sortorder;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                var insertresult = docx.Root.Elements("EntityTypeRole_Acl_Table").Elements("EntityTypeRole_Acl").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                if (insertresult.Count() == 0)
                {

                    XDocument document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                    XElement subjectElementworking = document1.Descendants("EntityTypeRole_Acl_Table").FirstOrDefault();
                    if (subjectElementworking == null)
                    {
                        CreateTableElement("EntityTypeRole_Acl_Table", xmlpath, TenantID);
                        document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                        subjectElementworking = document1.Descendants("EntityTypeRole_Acl_Table").FirstOrDefault();
                    }
                    option.ID = objMaxID;
                    MetadataXML = GetXElement(option);
                    subjectElement2 = MetadataXML.Descendants("EntityTypeRole_Acl_Table").FirstOrDefault();
                    subjectElementworking.Add(subjectElement2.Nodes());
                    //document1.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(document1, xmlpath, TenantID);
                }
                else
                {
                    //XmlDocument doc = new XmlDocument();
                    //doc.Load(xmlpath);
                    var Updateresult = docx.Root.Elements("EntityTypeRole_Acl_Table").Elements("EntityTypeRole_Acl").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                    foreach (XElement item in Updateresult)
                    {
                        item.SetElementValue("Caption", Caption);
                        item.SetElementValue("EntityRoleID", EntityRoleID);
                        item.SetElementValue("EntityTypeID", EntityTypeID);
                        item.SetElementValue("ModuleID", ModuleID);
                        item.SetElementValue("Sortorder", Sortorder);
                        string xmlelement = "<root><EntityTypeRole_Acl_Table>";
                        MetadataXML = item;
                        xmlelement = xmlelement + MetadataXML.ToString() + "</EntityTypeRole_Acl_Table></root>";
                        MetadataXML = XElement.Parse(xmlelement);

                    }
                    //docx.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(docx, xmlpath, TenantID);
                }
            }

            return MetadataXML;
        }

        public bool SaveSyncDbXML(XElement Xmlvalue, string tablename, string xmlpath, int Id, int TenantID)
        {
            XDocument document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);

            if (Id == 0)
            {
                XElement subjectElementworking = document1.Descendants(tablename + "_Table").FirstOrDefault();
                if (subjectElementworking == null)
                {
                    CreateTableElementForSyncDB(tablename + "_Table", xmlpath, TenantID);
                    document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                    subjectElementworking = document1.Descendants(tablename + "_Table").FirstOrDefault();
                }

                XElement MetadataXML = Xmlvalue;
                XElement subjectElement2 = MetadataXML.Descendants(tablename + "_Table").FirstOrDefault();
                subjectElementworking.Add(subjectElement2.Nodes());
                //document1.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(document1, xmlpath, TenantID);
            }
            else
            {
                //document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                var Updateresult = document1.Root.Elements(tablename + "_Table").Elements(tablename).Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                foreach (XElement item in Updateresult)
                {
                    item.Remove();
                }
                //document1.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(document1, xmlpath, TenantID);
                if (Updateresult != null)
                {
                    XElement subjectElementworking = document1.Descendants(tablename + "_Table").FirstOrDefault();
                    if (subjectElementworking == null)
                    {
                        CreateTableElementForSyncDB(tablename + "_Table", xmlpath, TenantID);
                        document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                        subjectElementworking = document1.Descendants(tablename + "_Table").FirstOrDefault();
                    }

                    XElement MetadataXML = Xmlvalue;
                    XElement subjectElement2 = MetadataXML.Descendants(tablename + "_Table").FirstOrDefault();
                    subjectElementworking.Add(subjectElement2.Nodes());
                    //document1.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(document1, xmlpath, TenantID);
                }
            }

            return true;
        }
        public int GetLatestMetadataVersion(string xmlpath)
        {
            string[] array1 = Directory.GetFiles(xmlpath);
            int latestverion = 0;
            for (int i = 0; i < array1.Length; i++)
            {
                if (array1[i].Contains("MetadataVersion"))
                {
                    string[] a = array1[i].Split('_');
                    int b = Convert.ToInt32(a[1].ToString().Replace("V", "").Replace(".xml", ""));
                    if (latestverion < b)
                        latestverion = b;
                }
            }
            return latestverion;
        }

        public bool ImportXml(string xmlpath)
        {
            try
            {
                XDocument docx = XDocument.Load(xmlpath);

            }
            catch (Exception ex) { }
            return false;
        }

        public XElement SaveTagOptionObject<T>(int TenantID, string xmlpath, BaseDao obj, int objMaxID = 0)
        {
            XElement subjectElement2 = null;
            XElement MetadataXML = null;
            if (typeof(T).Name == "TagOptionDao")
            {
                TagOptionDao option = (TagOptionDao)obj;
                int Id = option.Id;
                string Caption = option.Caption;
                int AttributeId = option.AttributeID;
                int SortOrder = option.SortOrder;
                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                var insertresult = docx.Root.Elements("TagOption_Table").Elements("TagOption").Where(mid => Convert.ToInt32(mid.Element("ID").Value) == Id).Select(m => m);
                if (insertresult.Count() == 0)
                {

                    XDocument document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                    XElement subjectElementworking = document1.Descendants("TagOption_Table").FirstOrDefault();
                    if (subjectElementworking == null)
                    {
                        CreateTableElement("TagOption_Table", xmlpath, TenantID);
                        document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                        subjectElementworking = document1.Descendants("TagOption_Table").FirstOrDefault();
                    }

                    option.Id = objMaxID;
                    option.TotalHits = 1;
                    MetadataXML = GetXElement(option);
                    subjectElement2 = MetadataXML.Descendants("TagOption_Table").FirstOrDefault();
                    subjectElementworking.Add(subjectElement2.Nodes());
                    //document1.Save(xmlpath);
                    MarcomCacheDal<bool>.SaveXML(document1, xmlpath, TenantID);
                }

                XDocument docx1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                //docx1.Save(xmlpath);
                MarcomCacheDal<bool>.SaveXML(docx1, xmlpath, TenantID);
            }
            return MetadataXML;
        }
        public string GetTenantFilePath(int TenantID)
        {
            //Get the tenant related file path

            BrandSystems.Marcom.Dal.Tenants.TenantSelection tfp = new BrandSystems.Marcom.Dal.Tenants.TenantSelection();
            return tfp.GetTenantFilePath(TenantID);
        }

    }
}
