﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Metadata.Interface
{
    public interface IDamTagWordsSelect
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int Assetid
        {
            get;
            set;

        }

        int Attributeid
        {
            get;
            set;

        }
        int Optionid
        {
            get;
            set;

        }



        #endregion
    }
}
