﻿(function (ng, app) {
    "use strict";

    function muiplanningtoolcostcentredetailoverviewCtrl($scope, $timeout, $http, $compile, $resource, $cookies, $stateParams, $location, $window, $translate, CcoverviewService, $modal) {
        var cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName + '\\' + TenantFilePath).replace(/\\/g, "\/");
        var imagesrcpath = TenantFilePath;
        if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
            imagesrcpath = cloudpath;
        }
        $scope.GetNonBusinessDaysforDatePicker();
        $scope.Calanderopen = function ($event, model) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calanderopened = true;
            $scope.fields["DatePart_Calander_Open" + model] = true;
        };
        $scope.EntityUserListInCC = [];
        $scope.PersonalUserIdsInCC = [];
        $('#OverAllEntityStats').select2("enable", false)
        var cancelNewsTimerevent;
        $scope.StopeUpdateStatusonPageLoad = false;
        $scope.EntityStatusResult = [];
        $scope.listAttriToAttriResult = [];
        $scope.ShowHideAttributeOnRelation = {};
        $scope.items = [];
        $scope.treeSources = {};
        $scope.treelevels = {};
        $scope.DropDownTreeOptionValues = {};
        $scope.uploader = {};
        $scope.UploaderCaption = {};
        $scope.NormalDropdownCaption = {};
        $scope.NormalMultiDropdownCaption = {};
        $scope.treeTexts = {};
        $scope.treeSelection = [];
        $scope.MilestonePrevStatus = '';
        $scope.normaltreeSources = {};
        $scope.userimg = parseInt($cookies['UserId']);
        $scope.OntimeStatusLists = [{
            Id: 0,
            Name: 'On time',
            BtnBgColor: 'btn-success',
            AbgColor: 'background-image: linear-gradient(to bottom, #62C462, #51A351); color: #fff;',
            SelectedClass: 'btn-success'
        }, {
            Id: 1,
            Name: 'Delayed',
            BtnBgColor: 'btn-warning',
            AbgColor: 'background-image: linear-gradient(to bottom, #FBB450, #F89406); color: #fff;',
            SelectedClass: 'btn-warning'
        }, {
            Id: 2,
            Name: 'On hold',
            BtnBgColor: 'btn-danger',
            AbgColor: 'background-image: linear-gradient(to bottom, #EE5F5B, #BD362F); color: #fff;',
            SelectedClass: 'btn-danger'
        }]
        $scope.Ontime = 'On time';
        var Version = 1;
        $scope.milestoneStatusOptions = [];
        $scope.milestoneStatusOptions = [{
            "Id": 1,
            "Caption": "Reached"
        }, {
            "Id": 0,
            "Caption": "Not Reached"
        }];
        $scope.fiscalyears = [];
        var getEntityNewsFeedResult = "";
        GlistFeedgroup();

        function GlistFeedgroup() {
            CcoverviewService.GetFeedFilter().then(function (GetFeedglistvalue) {
                $scope.FeedgResult = GetFeedglistvalue.Response;
                $scope.Feedglist = [];
                if ($scope.FeedgResult.length > 0) {
                    for (var i = 0; i < $scope.FeedgResult.length; i++) {
                        $scope.Feedglist.push({
                            ID: $scope.FeedgResult[i].Id,
                            Name: $scope.FeedgResult[i].FeedGroup
                        });
                    }
                }
            });
        };

        function LoadDetailTemp(AttrToAttrRelByIDFrEntInOverView, EntAttrDet, CostcentreFinOverview) {
            var ID = $stateParams.ID;
            try {
                $scope.fields = {
                    usersID: 0,
                };
                $scope.tree = {};
                $scope.fieldKeys = [];
                $scope.options = {};
                $scope.setFieldKeys = function () {
                    var keys = [];
                    angular.forEach($scope.fields, function (key) {
                        keys.push(key);
                        $scope.fieldKeys = keys;
                    });
                }
                $scope.ownername = $cookies['Username'];
                $scope.ownerid = $cookies['UserId'];
                $scope.ownerEmail = $cookies['UserEmail'];
                $scope.listAttriToAttriResult = [];
                try {
                    CcoverviewService.GetAttributeToAttributeRelationsByIDForEntityInOverView(0, ID).then(function () {
                        var entityAttrToAttrRelation = AttrToAttrRelByIDFrEntInOverView
                        $scope.listAttriToAttriResult = [];
                        $scope.listAttriToAttriResult = entityAttrToAttrRelation;
                    });
                } catch (ex) { }
                $scope.StopeUpdateStatusonPageLoad = false;
                $scope.dyn_Cont = '';
                var getentityattributesdetails = EntAttrDet;
                $scope.attributedata = getentityattributesdetails;
                $scope.dyn_Cont += '<div class="control-group"><label class="control-label" for="label">' + $translate.instant('LanguageContents.Res_69.Caption') + '</label><div class="controls"><label class="control-label" for="label">' + ID + '</label></div></div>'
                if ($scope.IsLock == undefined) $scope.IsLock = false;
                if ($scope.IsLock == false) $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\"\>' + $translate.instant('LanguageContents.Res_4985.Caption') + '</label><div class=\"controls\"><a xeditable href=\"javascript:;\" ng-click="costcentreentityclick()" attributeTypeID="1" entityid="' + ID + '" attributeTypeID="1" attributeid="68" id=\"ActivityName\" data-ng-model=\"ActivityName"\   my-qtip2 qtip-content=\"' + $translate.instant('LanguageContents.Res_4985.Caption') + '\"  data-type=\"text\" data-original-title=\"CostCentre Name\">' + $scope.DecodedTextName($scope.RootLevelEntityName) + '</a></div></div>';
                else if ($scope.IsLock == true) $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\"\>' + $translate.instant('LanguageContents.Res_4985.Caption') + '</label><div class=\"controls\"><label class="control-label">' + $scope.DecodedTextName($scope.RootLevelEntityName) + '</label></div></div>';
                $scope["showActivityName"] = true;
                $scope["EditActivityName"] = false;
                $scope["showOwner"] = true;
                $scope["EditOwner"] = false;
                for (var i = 0; i < $scope.attributedata.length; i++) {
                    if ($scope.attributedata[i].TypeID == 6) {
                        $scope.dyn_Cont2 = '';
                        var CaptionObj = $scope.attributedata[i].Caption.split(",");
                        for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                            if (j == 0) {
                                if (CaptionObj[j] != undefined) {
                                    $scope.items.push({
                                        caption: $scope.attributedata[i].Lable[j].Label,
                                        level: j + 1
                                    });
                                    $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                    $scope.settreeTexts();
                                    $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                    $scope.setFieldKeys();
                                } else {
                                    $scope.items.push({
                                        caption: $scope.attributedata[i].Lable[j].Label,
                                        level: j + 1
                                    });
                                    $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    $scope.settreeTexts();
                                    $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    $scope.setFieldKeys();
                                }
                            } else {
                                if (CaptionObj[j] != undefined) {
                                    $scope.items.push({
                                        caption: $scope.attributedata[i].Lable[j].Label,
                                        level: j + 1
                                    });
                                    $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                    $scope.settreeTexts();
                                    $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                    $scope.setFieldKeys();
                                } else {
                                    $scope.items.push({
                                        caption: $scope.attributedata[i].Lable[j].Label,
                                        level: j + 1
                                    });
                                    $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    $scope.settreeTexts();
                                    $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    $scope.setFieldKeys();
                                }
                            }
                        }
                        $scope.treelevels["dropdown_levels_" + $scope.attributedata[i].ID] = $scope.items;
                        $scope.settreelevels();
                        $scope.items = [];
                        $scope.settreeSources();
                        $scope.settreeTexts();
                        $scope.settreelevels();
                        $scope.setFieldKeys();
                        for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID + "_" + (j + 1)] = true;
                            var inlineEditabletitile = $scope.treelevels['dropdown_levels_' + $scope.attributedata[i].ID][j].caption;
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><a xeditabletreedropdown href=\"javascript:;\" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" InheritFromParent="' + $scope.attributedata[i].IsInheritFromParent + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '" attributeid="' + $scope.attributedata[i].ID + '" id="fields.DropDown_' + $scope.attributedata[i].ID + "_" + (j + 1) + '" data-ng-model=\"dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" attributename=' + inlineEditabletitile + ' data-type=' + inlineEditabletitile + $scope.attributedata[i].ID + ' >{{treeTexts.dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</a></div></div>';
                        }
                    } else if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == false) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        var textvalue = "-";
                        if ($scope.attributedata[i].Caption != undefined) textvalue = $scope.attributedata[i].Caption;
                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label">' + textvalue + '</label></div></div>';
                        } else {
                            if ($scope.IsLock == false) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditable href=\"javascript:;\" ng-click="costcentreentityclick()" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"SingleLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"SingleLineText_' + $scope.attributedata[i].ID + '\"  data-type=\"text\"  my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '">' + textvalue + '</a></div></div>';
                            else if ($scope.IsLock == true) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label">' + textvalue + '</label></div></div>';
                        }
                    } else if ($scope.attributedata[i].TypeID == 2) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        var textvalue = "-";
                        if ($scope.attributedata[i].Caption != undefined) textvalue = $scope.attributedata[i].Caption;
                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label">' + textvalue.replace(/\n/g, "<br/>") + '</label></div></div>';
                        } else {
                            if ($scope.IsLock == false) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditable href=\"javascript:;\" ng-click="costcentremultilineentityclick()" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"MultiLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"SingleLineText_' + $scope.attributedata[i].ID + '\" data-type=\"textarea\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '">' + textvalue.replace(/\n/g, "<br/>") + '</a></div></div>';
                            else if ($scope.IsLock == true) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label">' + textvalue.replace(/\n/g, "<br/>") + '</label></div></div>';
                        }
                    } else if ($scope.attributedata[i].TypeID == 11) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        $scope.fields["uploader_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                        $scope.setFieldKeys();
                        $scope.UploaderCaption["uploader_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group ng-scope\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable.toString() + '</label>';
                        $scope.dyn_Cont += '<div class=\"controls\">';
                        if ($scope.attributedata[i].Caption == "" || $scope.attributedata[i].Caption == null && $scope.attributedata[i].Caption == undefined) {
                            $scope.attributedata[i].Caption = $scope.attributedata[i].Lable;
                        }
                        if ($scope.attributedata[i].Value == "" || $scope.attributedata[i].Value == null && $scope.attributedata[i].Value == undefined) {
                            $scope.attributedata[i].Value = "NoThumpnail.jpg";
                        }
                        $scope.dyn_Cont += '<div class="entityDetailImgPreviewHolder"><img src="' + imagesrcpath + 'UploadedImages/' + $scope.attributedata[i].Value + '" alt="' + $scope.attributedata[i].Caption + '"';
                        $scope.dyn_Cont += 'class="entityDetailImgPreview" id="UploaderPreview_' + $scope.attributedata[i].ID + '"></div>';
                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '</a></div></div>';
                        } else {
                            if ($scope.IsLock == false) {
                                $scope.dyn_Cont += "<a class='margin-left10x' ng-click='UploadImagefileCC(" + $scope.attributedata[i].ID + ")' attributeTypeID='" + $scope.attributedata[i].TypeID + "'";
                                $scope.dyn_Cont += 'entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="Uploader_' + $scope.attributedata[i].ID + '"';
                                $scope.dyn_Cont += 'my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '"';
                                $scope.dyn_Cont += ' class="ng-pristine ng-valid" attributename=\"' + $scope.UploaderCaption["UploaderCaption_" + $scope.attributedata[i].ID] + '\">Select Image';
                                $scope.dyn_Cont += '</a></div></div>';
                            } else if ($scope.IsLock == true) $scope.dyn_Cont += '</a></div></div>';
                        }
                    } else if ($scope.attributedata[i].TypeID == 3) {
                        if ($scope.attributedata[i].ID == SystemDefiendAttributes.Owner && $scope.attributedata[i].IsSpecial == true) {
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                if ($scope.attributedata[i].options != null) {
                                    if ($scope.attributedata[i].options.length > 0)
                                        $scope.normaltreeSources["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].options;
                                }
                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                } else {
                                    if ($scope.IsLock == false) $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    else if ($scope.IsLock == true) $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                }
                            } else {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                    $scope.dyn_Cont += '<div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label>';
                                    $scope.dyn_Cont += '</div></div>';
                                } else {
                                    if ($scope.IsLock == false) {
                                        $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                        $scope.dyn_Cont += '<div class="controls"><a  xeditabledropdown href=\"javascript:;\"';
                                        $scope.dyn_Cont += 'attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '"';
                                        $scope.dyn_Cont += 'attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"';
                                        $scope.dyn_Cont += 'data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '"';
                                        $scope.dyn_Cont += 'attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\"';
                                        $scope.dyn_Cont += 'data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a>';
                                        $scope.dyn_Cont += '</div></div>';
                                    } else if ($scope.IsLock == true) {
                                        $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                        $scope.dyn_Cont += '<div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label>';
                                        $scope.dyn_Cont += '</div></div>';
                                    }
                                }
                            }
                        } else {
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                                if ($scope.attributedata[i].Caption[0].length > 1) {
                                    $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                    $scope.setFieldKeys();
                                    $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                    $scope.setNormalDropdownCaption();
                                    if ($scope.attributedata[i].IsReadOnly == true) {
                                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                    } else {
                                        if ($scope.IsLock == false) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                        else if ($scope.IsLock == true) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                    }
                                } else {
                                    $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                    $scope.setFieldKeys();
                                    $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                    $scope.setNormalDropdownCaption();
                                    if ($scope.attributedata[i].IsReadOnly == true) {
                                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                    } else {
                                        if ($scope.IsLock == false) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                        else if ($scope.IsLock == true) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                    }
                                }
                            } else {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                } else {
                                    if ($scope.IsLock == false) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    else if ($scope.IsLock == true) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                }
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 4) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption.length > 1) {
                                $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                $scope.setFieldKeys();
                                $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalMultiDropdownCaption();
                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                } else {
                                    if ($scope.IsLock == false) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditablemultiselectdropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalMultiDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    else if ($scope.IsLock == true) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                }
                            } else {
                                $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalMultiDropdownCaption();
                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                } else {
                                    if ($scope.IsLock == false) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditablemultiselectdropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalMultiDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    else if ($scope.IsLock == true) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                }
                            }
                        } else {
                            $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = "-";
                            $scope.setFieldKeys();
                            $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalMultiDropdownCaption();
                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                            } else {
                                if ($scope.IsLock == false) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditablemultiselectdropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalMultiDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                else if ($scope.IsLock == true) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 7) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        $scope.fields["Tree_" + $scope.attributedata[i].ID] = [];
                        $scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID] = JSON.parse($scope.attributedata[i].tree).Children;
                        $scope.staticTreesrcdirec["Attr_" + $scope.attributedata[i].ID] = JSON.parse($scope.attributedata[i].tree).Children;
                        GetTreeCheckedNodes($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID], $scope.attributedata[i].ID);
                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class="control-group relative">';
                        $scope.dyn_Cont += '<label class="control-label">' + $scope.attributedata[i].Lable + ' </label>';
                        $scope.dyn_Cont += '<div class="controls">';
                        if ($scope.IsLock == false) {
                            $scope.dyn_Cont += '<div xeditabletree  editabletypeid="treeType_' + $scope.attributedata[i].ID + '" attributename=\"' + $scope.attributedata[i].Lable + '\" isreadonly="' + $scope.attributedata[i].IsReadOnly + '\" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '"  data-type="treeType_' + $scope.attributedata[i].ID + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"' + $scope.attributedata[i].ID + '\" data-ng-model=\"tree_' + $scope.attributedata[i].ID + '"\    data-original-title=\"' + $scope.attributedata[i].Lable + '\">';
                            if ($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID].length > 0) {
                                treeTextVisbileflag = false;
                                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID])) {
                                    $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = true;
                                } else $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
                            } else {
                                $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
                            }
                            $scope.dyn_Cont += '<eu-tree ng-show=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\" tree-data=\"treesrcdirec.Attr_' + $scope.attributedata[i].ID + '\" treeplace="detail" node-attributeid="' + $scope.attributedata[i].ID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                            $scope.dyn_Cont += '<span ng-hide=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\">-<span>';
                            $scope.dyn_Cont += ' </div>';
                        } else {
                            if ($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID].length > 0) {
                                treeTextVisbileflag = false;
                                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID])) {
                                    $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = true;
                                } else $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
                            } else {
                                $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
                            }
                            $scope.dyn_Cont += '<eu-tree ng-show=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\" tree-data=\"treesrcdirec.Attr_' + $scope.attributedata[i].ID + '\"  node-attributeid="' + $scope.attributedata[i].ID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                            $scope.dyn_Cont += '<span ng-hide=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\">-<span>';
                        }
                        $scope.dyn_Cont += '</div></div>';
                    } else if ($scope.attributedata[i].TypeID == 12) {
                        $scope.dyn_Cont2 = '';
                        var CaptionObj = $scope.attributedata[i].Caption;
                        for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                            if ($scope.attributedata[i].Lable.length == 1) {
                                var k = j;
                                var treeTexts = [];
                                var fields = [];
                                $scope.items.push({
                                    caption: $scope.attributedata[i].Lable[j].Label,
                                    level: j + 1
                                });
                                if (k == CaptionObj.length) {
                                    $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                } else {
                                    if (CaptionObj[k] != undefined) {
                                        for (k; k < CaptionObj.length; k++) {
                                            treeTexts.push(CaptionObj[k]);
                                            $scope.settreeTexts();
                                            fields.push(CaptionObj[k]);
                                            $scope.setFieldKeys();
                                        }
                                        $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = treeTexts.join(", ");
                                        $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = fields.join(", ");
                                    } else {
                                        $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    }
                                }
                            } else {
                                if (j == 0) {
                                    if (CaptionObj[j] != undefined) {
                                        $scope.items.push({
                                            caption: $scope.attributedata[i].Lable[j].Label,
                                            level: j + 1
                                        });
                                        $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                        $scope.settreeTexts();
                                        $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                        $scope.setFieldKeys();
                                    } else {
                                        $scope.items.push({
                                            caption: $scope.attributedata[i].Lable[j].Label,
                                            level: j + 1
                                        });
                                        $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        $scope.settreeTexts();
                                        $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        $scope.setFieldKeys();
                                    }
                                } else {
                                    var k = j;
                                    if (j == ($scope.attributedata[i].Lable.length - 1)) {
                                        var treeTexts = [];
                                        var fields = [];
                                        $scope.items.push({
                                            caption: $scope.attributedata[i].Lable[j].Label,
                                            level: j + 1
                                        });
                                        if (k == CaptionObj.length) {
                                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        } else {
                                            if (CaptionObj[k] != undefined) {
                                                for (k; k < CaptionObj.length; k++) {
                                                    treeTexts.push(CaptionObj[k]);
                                                    $scope.settreeTexts();
                                                    fields.push(CaptionObj[k]);
                                                    $scope.setFieldKeys();
                                                }
                                                $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = treeTexts.join(", ");
                                                $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = fields.join(", ");
                                            } else {
                                                $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                                $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            }
                                        }
                                    } else {
                                        if (CaptionObj[j] != undefined) {
                                            $scope.items.push({
                                                caption: $scope.attributedata[i].Lable[j].Label,
                                                level: j + 1
                                            });
                                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                            $scope.settreeTexts();
                                            $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                            $scope.setFieldKeys();
                                        } else {
                                            $scope.items.push({
                                                caption: $scope.attributedata[i].Lable[j].Label,
                                                level: j + 1
                                            });
                                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            $scope.settreeTexts();
                                            $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            $scope.setFieldKeys();
                                        }
                                    }
                                }
                            }
                        }
                        $scope.treelevels["multiselectdropdown_levels_" + $scope.attributedata[i].ID] = $scope.items;
                        $scope.settreelevels();
                        $scope.items = [];
                        $scope.settreeSources();
                        $scope.settreeTexts();
                        $scope.settreelevels();
                        $scope.setFieldKeys();
                        for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID + "_" + (j + 1)] = true;
                            var inlineEditabletitile = $scope.treelevels['multiselectdropdown_levels_' + $scope.attributedata[i].ID][j].caption;
                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><label class="control-label">{{treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</label></div></div>';
                            } else {
                                if ($scope.IsLock == false) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><a xeditablemultiselecttreedropdown href=\"javascript:;\" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" InheritFromParent="' + $scope.attributedata[i].IsInheritFromParent + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '" attributeid="' + $scope.attributedata[i].ID + '" id="fields.MultiSelectDropDown_' + $scope.attributedata[i].ID + "_" + (j + 1) + '" data-ng-model=\"multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\"  attributename=' + inlineEditabletitile + ' data-type=' + inlineEditabletitile + ' >{{treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</a></div></div>';
                                } else if ($scope.IsLock == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><label class="control-label">{{treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</label></div></div>';
                                }
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 5 && $scope.attributedata[i].ID != SystemDefiendAttributes.ApproveTime) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        var datStartUTCval = "";
                        var datstartval = "";
                        var inlineEditabletitile = $scope.attributedata[i].Caption;
                        if ($scope.attributedata[i].Value != null || $scope.attributedata[i].Value != undefined) {
                            datStartUTCval = $scope.attributedata[i].Value.substr(0, 10);
                            datstartval = new Date.create(datStartUTCval);
                            $scope.fields["DateTime_" + $scope.attributedata[i].ID] = dateFormat(datstartval, $scope.GetDefaultSettings.DateFormat);
                            $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = formatteddateFormat(datstartval, "dd/MM/yyyy");
                        } else {
                            $scope.fields["DateTime_" + $scope.attributedata[i].ID] = "-";
                            $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = "-";
                        }
                        if ($scope.attributedata[i].ID != SystemDefiendAttributes.CreationDate) {
                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                            } else {
                                if ($scope.IsLock == false) {
                                    $scope.MinValue = $scope.attributedata[i].MinValue;
                                    $scope.MaxValue = $scope.attributedata[i].MaxValue;
                                    $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID] = new Date.create();
                                    $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID] = new Date.create();
                                    if ($scope.MinValue < 0) {
                                        $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MinValue + 1));
                                    } else {
                                        $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MinValue));
                                    }
                                    if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                        $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MaxValue - 1));
                                    } else {
                                        $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].getDate() + 100000);
                                    }
                                    var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID], $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID]);
                                    $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID] = (temp.MinDate);
                                    $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID] = (temp.MaxDate);
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a xeditabletreedatetime data-primaryid="' + $scope.attributedata[i].ID + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-datetime_id=\"DateTime_' + $scope.attributedata[i].ID + '\" data-ng-model=\"DateTime_' + $scope.attributedata[i].ID + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"datetime' + $scope.attributedata[i].ID + '\" href=\"javascript:;\" attributename="datetime">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    $scope.fields["DatePart_" + $scope.attributedata[i].ID] = null;
                                } else if ($scope.IsLock == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                }
                            }
                        } else {
                            $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                        }
                    } else if ($scope.attributedata[i].TypeID == 16) {
                        var dateExpireUTCval = "";
                        var dateExpireval = "";
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        var inlineEditabletitile = $scope.attributedata[i].Caption;
                        var dateExpire;
                        if ($scope.attributedata[i].Value != null || $scope.attributedata[i].Value != undefined) {
                            var dateExpireUTCval = "";
                            var dateExpireval = "";
                            dateExpireUTCval = $scope.attributedata[i].Value.substr(0, 10);
                            dateExpireval = new Date(dateExpireUTCval);
                            dateExpire = dateFormat(ConvertDateFromStringToString(ConvertDateToString(dateExpireUTCval)).toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3").toString('dd-MM-yyyy'), $scope.GetDefaultSettings.DateFormat);
                            $scope.fields["DateTime_" + $scope.attributedata[i].ID] = dateExpire;
                            $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = dateExpire;
                        } else {
                            dateExpire = "-"
                            $scope.fields["DateTime_" + $scope.attributedata[i].ID] = "-";
                            $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = "-";
                        }
                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">' + dateExpire + '</span></div></div>';
                        } else {
                            if ($scope.IsLock == false) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable"><a data-AttributeID="' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" data-expiredate="' + dateExpire + '" data-attrlable="' + $scope.attributedata[i].Lable + '" ng-click="openExpireAction($event)">' + dateExpire + '</a></span></div></div>';
                            } else if ($scope.IsLock == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable"> ' + dateExpire + '</span></div></div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 17) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption.length > 1) {
                                $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                $scope.fields["TagWordsSeleted_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Value;
                            }
                        } else {
                            $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = "-";
                            $scope.fields["TagWordsSeleted_" + $scope.attributedata[i].ID] = [];
                        }
                        $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                        } else {
                            if ($scope.IsLock == false) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabletagwords href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="TagWordsCaption_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.TagWordsCaption_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                            } else if ($scope.IsLock == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 18) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                        }
                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                    } else if ($scope.attributedata[i].TypeID == 9) {
                        $scope.fields["CheckBoxSelection_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Value;
                        $scope.dyn_Cont += "<div ng-show= ShowHideAttributeOnRelation.Attribute_" + $scope.attributedata[i].ID + "  class=\"control-group\"><label class=\"control-label\" for=\"fields.CheckBoxSelection_ " + $scope.attributedata[i].AttributeID + "\">" + $scope.attributedata[i].Caption + " : </label><div class=\"controls\"><input ng-click=\"saveDropdownTree(" + $scope.attributedata[i].ID + "," + $scope.attributedata[i].TypeID + "," + parseInt($stateParams.ID) + "," + $scope.fields["CheckBoxSelection_" + $scope.attributedata[i].ID] + "  )\"  type=\"checkbox\" ng-model=\"fields.CheckBoxSelection_" + $scope.attributedata[i].ID + "\" id=\"CheckBoxSelection_" + $scope.attributedata[i].ID + "\" placeholder=\"" + $scope.attributedata[i].Caption + "\"></div></div>";
                    }
                }
                $("#dynamicdetail").html($compile($scope.dyn_Cont)($scope));
                $scope.detailsLoader = false;
                $scope.detailsData = true;
                HideAttributeToAttributeRelationsOnPageLoad();
                i = 0;
                for (i = 0; i < $scope.attributedata.length; i++) {
                    if (($scope.attributedata[i].TypeID == 3 || $scope.attributedata[i].TypeID == 4 || $scope.attributedata[i].TypeID == 6 || $scope.attributedata[i].TypeID == 7 || $scope.attributedata[i].TypeID == 12) && $scope.attributedata[i].IsSpecial == false) {
                        if ($scope.attributedata[i].TypeID == 12) {
                            for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                                if ($scope.attributedata[i].Value[j] != undefined && $scope.attributedata[i].Value[j] != null) {
                                    if (($scope.attributedata[i].Lable.length - 1) == j) {
                                        var k = $scope.attributedata[i].Value.length - $scope.attributedata[i].Lable.length;
                                        for (k; k < $scope.attributedata[i].Value.length; k++) {
                                            $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Lable[j].Level, $scope.attributedata[i].Value[k].Nodeid, 12);
                                        }
                                    } else {
                                        $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Lable[j].Level, $scope.attributedata[i].Value[j].Nodeid, 12);
                                    }
                                }
                            }
                        } else if ($scope.attributedata[i].TypeID == 6) {
                            for (var j = 0; j < $scope.attributedata[i].Lable.length - 1; j++) {
                                if ($scope.attributedata[i].Value[j] != undefined && $scope.attributedata[i].Value[j] != null) $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Lable[j].Level, $scope.attributedata[i].Value[j].Nodeid, 6);
                            }
                        } else if ($scope.attributedata[i].TypeID == 4) {
                            $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Level, $scope.attributedata[i].Value, 4);
                        } else if ($scope.attributedata[i].TypeID == 7) {
                            $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Level, $scope.fields["Tree_" + $scope.attributedata[i].ID], 7);
                        } else {
                            $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Level, $scope.attributedata[i].Value, 3);
                        }
                    }
                }
                $scope.CurrentWorkflowStepName = "";
                $scope.workflowstepOptions = [];
                $scope.workflowSummaryList = [];
                $scope.SelectedWorkflowStep = 0;
                $scope.WorkFlowStepID = 0;
                $scope.WorkflowSummaryControls = '';
                try {
                    $scope.UpdateEntityWorkFlowStep = function (stepID) {
                        var TaskStatusData = {};
                        TaskStatusData.EntityID = parseInt($stateParams.ID, 10);
                        TaskStatusData.Status = stepID;
                        CcoverviewService.UpdateEntityActiveStatus(TaskStatusData).then(function (TaskStatusResult) {
                            if (TaskStatusResult.StatusCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_4375.Caption'));
                            } else {
                                NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                                for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                                    for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                                        var EntityId = $scope.ListViewDetails[i].data.Response.Data[j]["Id"];
                                        if (EntityId == parseInt($stateParams.ID, 10)) {
                                            if ($scope.ListViewDetails[i].data.Response.Data[j][SystemDefiendAttributes.Status] != undefined) $scope.ListViewDetails[i].data.Response.Data[j][SystemDefiendAttributes.Status] = $scope.CurrentWorkflowStepName;
                                            else $scope.ListViewDetails[i].data.Response.Data[j]["Status"] = $scope.CurrentWorkflowStepName;
                                            return false;
                                        }
                                    }
                                }
                                $scope.SelectedWorkflowStep = 0;
                            }
                        });
                    }
                } catch (exc) { }
                $scope.openExpireAction = function (event) {
                    var attrevent = event.currentTarget;
                    $scope.SourceAttributeID = attrevent.dataset.attributeid;
                    $scope.SourceAttributename = attrevent.dataset.attrlable;
                    $scope.dateExpireval = attrevent.dataset.expiredate;
                    var sourcefrom = 2;
                    var modalInstance = $modal.open({
                        templateUrl: 'views/mui/expirehandleractions.html',
                        controller: "mui.muiexpireaddActionsCtrl",
                        resolve: {
                            params: function () {
                                return {
                                    dateExpireval: $scope.dateExpireval,
                                    AssetEditID: parseInt($stateParams.ID),
                                    assetEntityId: parseInt($stateParams.ID),
                                    sourcefrom: sourcefrom,
                                    assettypeidfrbtn: $scope.EntityTypeID,
                                    ProductionEntityID: parseInt($stateParams.ID),
                                    SourceAttributeID: $scope.SourceAttributeID,
                                    SourceAttributename: $scope.SourceAttributename
                                };
                            }
                        },
                        scope: $scope,
                        windowClass: 'expirehandlerAddActionsPopup popup-widthL',
                        backdrop: "static"
                    });
                    modalInstance.result.then(function (selectedItem) {
                        $scope.selected = selectedItem;
                    }, function () { });
                }
                $scope.saveDropdownTree = function (attrID, attributetypeid, entityTypeid, optionarray) {
                    if (attrID == 57) {
                        var updateentityattrib = {};
                        updateentityattrib.NewValue = optionarray == "" ? ["0"] : [optionarray];
                        updateentityattrib.AttributetypeID = attributetypeid;
                        updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                        updateentityattrib.AttributeID = attrID;
                        updateentityattrib.Level = 0;
                        CcoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                            if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                            else NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                            UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                        });
                        $scope.treeSelection = [];
                    } else if (attributetypeid == 1) {
                        var updateentityattrib = {};
                        updateentityattrib.NewValue = optionarray == "" ? ["0"] : [optionarray];
                        updateentityattrib.AttributetypeID = attributetypeid;
                        updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                        updateentityattrib.AttributeID = attrID;
                        updateentityattrib.Level = 0;
                        if (attrID == 68) {
                            $(window).trigger("UpdateEntityName", optionarray);
                            $("ul li a[data-entityid='" + $stateParams.ID + "'] span[data-name='text']").text([optionarray]);
                        }
                        CcoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                            if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                            else {
                                $scope.fields['SingleLineTextValue_' + attrID] = optionarray;
                                NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                                UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                                if (attrID == 68) {
                                    $('#breadcrumlink').text(optionarray);
                                }
                            }
                        });
                        $scope.treeSelection = [];
                    } else if (attributetypeid == 3 || attributetypeid == 4 || attributetypeid == 17) {
                        var updateentityattrib = {};
                        updateentityattrib.NewValue = optionarray;
                        updateentityattrib.AttributetypeID = attributetypeid;
                        updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                        updateentityattrib.AttributeID = attrID;
                        updateentityattrib.Level = 0;
                        CcoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                            if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                            else $scope.ShowHideAttributeToAttributeRelations(attrID, 0, 0, attributetypeid, optionarray);
                            NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                            UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                        });
                        $scope.treeSelection = [];
                    } else if (attributetypeid == 6) {
                        var updateentityattrib = {};
                        updateentityattrib.NewValue = optionarray == "" ? ["0"] : [optionarray];
                        updateentityattrib.AttributetypeID = attributetypeid;
                        updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                        updateentityattrib.AttributeID = attrID;
                        updateentityattrib.Level = 0;
                        CcoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                            if (updateentityattribresult.StatusCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                            } else {
                                NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                                for (var i = 0; i < optionarray.length; i++) {
                                    if (optionarray[i] != 0) {
                                        var level = i + 1;
                                        UpdateTreeScope(attrID + "_" + level, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"][data-ng-model="dropdown_text_' + attrID + "_" + level + '"]').text());
                                    }
                                }
                                for (var j = 0; j < optionarray.length; j++) {
                                    if (optionarray[j] != 0) {
                                        $scope.ShowHideAttributeToAttributeRelations(attrID, (j) + 1, optionarray.length, attributetypeid, optionarray[j]);
                                    }
                                }
                            }
                        });
                        $scope.treeSelection = [];
                    } else if (attributetypeid == 12) {
                        var updateentityattrib = {};
                        updateentityattrib.NewValue = optionarray == "" ? ["0"] : [optionarray];
                        updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                        updateentityattrib.AttributeID = attrID;
                        updateentityattrib.Level = 0;
                        updateentityattrib.AttributetypeID = attributetypeid;
                        CcoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                            if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                            else {
                                for (var j = 0; j < optionarray.length; j++) {
                                    if (optionarray[j] != 0) {
                                        $scope.ShowHideAttributeToAttributeRelations(attrID, (j) + 1, optionarray.length, attributetypeid, optionarray[j]);
                                    }
                                }
                                NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                                UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                            }
                        });
                        $scope.treeSelection = [];
                    } else if (attributetypeid == 8) {
                        if (!isNaN(optionarray.toString())) {
                            var updateentityattrib = {};
                            updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                            updateentityattrib.AttributeID = attrID;
                            updateentityattrib.Level = 0;
                            updateentityattrib.NewValue = optionarray == "" ? ["0"] : [optionarray];
                            updateentityattrib.AttributetypeID = attributetypeid;
                            CcoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                                if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                                else {
                                    $scope.fields['SingleLineTextValue_' + attrID] = optionarray == "" ? "0" : optionarray;
                                    NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                                    UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                                }
                            });
                        } else {
                            NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                        }
                        $scope.treeSelection = [];
                    } else if (attributetypeid == 5) {
                        var sdate = new Date($scope.fields['DateTime_Dir_' + attrID].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
                        if (sdate == 'NaN-NaN-NaN') {
                            sdate = ConvertDateToString(ConvertStringToDateByFormat($scope.fields['DateTime_Dir_' + periodid].toString(), GlobalUserDateFormat))
                        }
                        var updateentityattrib = {};
                        updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                        updateentityattrib.AttributeID = attrID;
                        updateentityattrib.Level = 0;
                        updateentityattrib.NewValue = [sdate];
                        updateentityattrib.AttributetypeID = attributetypeid;
                        CcoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                            if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                            else {
                                NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                                UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                                $scope.fields["DateTime_" + attrID] = ConvertDateFromStringToString(ConvertDateToString(sdate));
                            }
                        });
                        $scope.treeSelection = [];
                    } else if (attributetypeid == 9) {
                        var updateentityattrib = {};
                        updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                        updateentityattrib.AttributeID = attrID;
                        updateentityattrib.Level = 0;
                        updateentityattrib.NewValue = ($('#CheckBoxSelection_' + attrID)[0].checked) ? [1] : [0];
                        updateentityattrib.AttributetypeID = attributetypeid;
                        CcoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                            if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                            else {
                                NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                                $scope.fields['CheckBoxSelection_' + attrID] = $('#CheckBoxSelection_' + attrID)[0].checked;
                            }
                        });
                    }
                    $scope.treeSelection = [];
                    $timeout(function () {
                        $scope.TimerForLatestFeed();
                    }, 5000);
                };
                $scope.enabletext = function () {
                    $('#username').editable({
                        placement: 'right',
                        title: 'Select Status'
                    });
                }
                $scope.saveDetailBlock = function (val) {
                    var NewValue = '';
                    if (val == 1) {
                        NewValue = $scope.txtActivityName;
                        $scope["showActivityName"] = true;
                        $scope["EditActivityName"] = false;
                    }
                    if (val == 2) {
                        NewValue = $scope.fields.DropDown_Members.Id;
                        $scope["showOwner"] = true;
                        $scope["EditOwner"] = false;
                    }
                    if (val == 3) {
                        NewValue = $scope.fields.DropDown_FiscalYear.Id;
                        $scope["HideInlineEdit0"] = true;
                        $scope["showinlineedit0"] = false;
                    }
                    var updateentityattrib = {};
                    updateentityattrib.ID = val;
                    updateentityattrib.EntityID = ID;
                    updateentityattrib.NewValue = NewValue;
                    CcoverviewService.SaveDetailBlock(updateentityattrib).then(function (updateentityattrib) {
                        $scope.TimerForLatestFeed();
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                    });
                };
            } catch (e) { }
            $scope.AddNewsFeed = function () {
                if ($("#feedtextholder").text() != "") {
                    return false;
                }
                var addnewsfeed = {};
                addnewsfeed.Actor = parseInt($cookies['UserId'], 10);
                addnewsfeed.TemplateID = 2;
                addnewsfeed.EntityID = parseInt($stateParams.ID);
                addnewsfeed.TypeName = "";
                addnewsfeed.AttributeName = "";
                addnewsfeed.FromValue = "";
                addnewsfeed.ToValue = $('#feedcomment').text();
                addnewsfeed.PersonalUserIds = $scope.PersonalUserIdsInCC;
                CcoverviewService.PostFeed(addnewsfeed).then(function (savenewsfeed) { });
                $scope.PersonalUserIdsInCC = [];
                $('#feedcomment').empty();
                if (document.getElementById('feedcomment').innerHTML.replace(/(<([^>]+)>)/ig, '').replace(/ /g, '').length == 0) {
                    document.getElementById('feedcomment').innerHTML = '<span id=\'feedtextholder\' class=\'placeholder\'>' + $translate.instant('LanguageContents.Res_1202.Caption') + '...</span>';
                }
                $scope.TimerForLatestFeed();
            };
            try {
                $scope.GettingNewsFeedTime = function (serverTime, happendTime) {
                    var appServerTime = serverTime;
                    var feedHappendTime = happendTime;
                    var timeDiff = Math.abs(appServerTime - feedHappendTime);
                }
            } catch (e) { }
            try {
                $scope.financialchartpoints = [];
                var GetFinancialSummaryBlockResult = CostcentreFinOverview;
                var FinancialResult = GetFinancialSummaryBlockResult;
                $scope.financialchartpoints = [{
                    label: $translate.instant('LanguageContents.Res_4971.Caption'),
                    y: parseInt((FinancialResult['AvailabletoSpent'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                }, {
                    label: $translate.instant('LanguageContents.Res_1725.Caption'),
                    y: parseInt((FinancialResult['Spent'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                }, {
                    label: $translate.instant('LanguageContents.Res_5531.Caption'),
                    y: parseInt((FinancialResult['Committed'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                }, {
                    label: $translate.instant('LanguageContents.Res_4970.Caption'),
                    y: parseInt((FinancialResult['UnAllocatedAmount'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                }, {
                    label: $translate.instant('LanguageContents.Res_1740.Caption'),
                    y: parseInt((FinancialResult['ApprovedAllocation'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                }, {
                    label: $translate.instant('LanguageContents.Res_4968.Caption'),
                    y: parseInt((FinancialResult['BudgetDeviation'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                }, {
                    label: $translate.instant('LanguageContents.Res_4967.Caption'),
                    y: parseInt((FinancialResult['ApprovedBudget'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                }, {
                    label: $translate.instant('LanguageContents.Res_1703.Caption'),
                    y: parseInt((FinancialResult['InRequests'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                }, {
                    label: $translate.instant('LanguageContents.Res_1744.Caption'),
                    y: parseInt((FinancialResult['PlannedAmount'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                }, {
                    label: $translate.instant('LanguageContents.Res_5532.Caption'),
                    y: parseInt((FinancialResult['TotalAssignedAmount'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                }];
                CanvasJS.addColorSet("finColorSet1", ["#BDF2A1", "#9ECF6E", "#DDDD69", "#4DA3CE", "#B08BEB", "#3EA0DD", "#F5A52A", "#23BFAA", "#FAA586", "#EB8CC6"]);
                var chart = new CanvasJS.Chart("financialchart", {
                    animationEnabled: true,
                    colorSet: "finColorSet1",
                    axisX: {
                        tickColor: "white",
                        tickLength: 5,
                        labelFontFamily: "sans-serif",
                        labelFontColor: "#7F7F7F",
                        labelFontSize: 10,
                        labelFontWeight: "normal",
                    },
                    axisY: {
                        tickLength: 0,
                        labelFontSize: 0,
                        lineThickness: 0,
                        gridThickness: 0,
                    },
                    data: [{
                        type: "stackedBar",
                        indexLabel: "{y}",
                        indexLabelFontSize: 12,
                        indexLabelFontWeight: "bold",
                        indexLabelPlacement: "outside",
                        indexLabelOrientation: "horizontal",
                        legendMarkerColor: "gray",
                        dataPoints: $scope.financialchartpoints
                    }]
                });
                chart.render();
            } catch (e) { }
        };
        $scope.FeedgroupFilterByOptionChange = function () {
            if ($scope.Newsfilter.FeedgroupMulitipleFilterStatus.length == 0 || $scope.Newsfilter.FeedgroupMulitipleFilterStatus == undefined) {
                $scope.Newsfilter.FeedgroupMulitipleFilterStatus = [];
                $scope.Newsfilter.Feedgrouplistvalues = '-1';
                $scope.LoadNewsFeedBlock();
            } else {
                $scope.Newsfilter.FeedgroupMulitipleFilterStatus = $scope.Newsfilter.FeedgroupMulitipleFilterStatus;
                $scope.Newsfilter.Feedgrouplistvalues = '';
                $scope.Newsfilter.Feedgrouplistvalues = $scope.Newsfilter.FeedgroupMulitipleFilterStatus.join(',');
                $scope.LoadNewsFeedBlock();
            }
        }
        $scope.treeSourcesObj = [];
        $scope.settreeSources = function () {
            var keys = [];
            angular.forEach($scope.treeSources, function (key) {
                keys.push(key);
                $scope.treeSourcesObj = keys;
            });
        }
        $scope.treelevelsObj = [];
        $scope.settreelevels = function () {
            var keys1 = [];
            angular.forEach($scope.treelevels, function (key) {
                keys1.push(key);
                $scope.treelevelsObj = keys1;
            });
        }
        $scope.NormalDropdownCaptionObj = [];
        $scope.setNormalDropdownCaption = function () {
            var keys1 = [];
            angular.forEach($scope.NormalDropdownCaption, function (key) {
                keys1.push(key);
                $scope.NormalDropdownCaptionObj = keys1;
            });
        }
        $scope.NormalMultiDropdownCaptionObj = [];
        $scope.setNormalMultiDropdownCaption = function () {
            var keys1 = [];
            angular.forEach($scope.NormalMultiDropdownCaption, function (key) {
                keys1.push(key);
                $scope.NormalMultiDropdownCaptionObj = keys1;
            });
        }
        $scope.treeTextsObj = [];
        $scope.settreeTexts = function () {
            var keys2 = [];
            angular.forEach($scope.treeTexts, function (key) {
                keys2.push(key);
                $scope.treeTextsObj = keys2;
            });
        }
        $scope.fields = {
            usersID: ''
        };
        $scope.fieldKeys = [];
        $scope.setFieldKeys = function () {
            var keys = [];
            angular.forEach($scope.fields, function (key) {
                keys.push(key);
                $scope.fieldKeys = keys;
            });
        }
        app.value('$strapConfig', {
            datepicker: {
                language: 'fr',
                format: 'M d, yyyy'
            }
        });
        $scope.tempresultHolder = [];
        $scope.GetEntityMembers = function () {
            $scope.PersonalUserIdsInCC = [];
            $scope.EntityUserListInCC = [];
            CcoverviewService.GetMember($stateParams.ID).then(function (member) {
                var IsUnique = true;
                if (member.Response != null) {
                    $.each(member.Response, function (index, value) {
                        if (value.IsInherited == false) {
                            IsUnique = true;
                            if ($scope.EntityUserListInCC.length > 0) {
                                $.each($scope.EntityUserListInCC, function (i, v) {
                                    if (v.Userid == value.Userid) {
                                        IsUnique = false;
                                    }
                                });
                                if (IsUnique == true) $scope.EntityUserListInCC.push(value);
                            } else $scope.EntityUserListInCC.push(value);
                        }
                    });
                }
            });
        }
        $scope.LoadCostcenterRelatedData = function () {
            CcoverviewService.GetCostCenterRelatedDataOnLoad(parseInt($stateParams.ID), parseInt($cookies['UserId']), 0, false).then(function (CostcenterRelatedDataResult) {
                if (CostcenterRelatedDataResult.StatusCode != 405) {
                    if (CostcenterRelatedDataResult.Response != null) {
                        $scope.tempresultHolder = CostcenterRelatedDataResult.Response;
                        funloadEntityStatusForCCEdit($scope.tempresultHolder[0], $scope.tempresultHolder[1]);
                        LoadTaskSummaryDetlEdit($scope.tempresultHolder[2]);
                        $scope.LoadCostcenterRelatedDataSet2();
                    }
                }
            });
        }
        $scope.LoadCostcenterRelatedData();
        $scope.tempresultHolderSet2 = [];
        $scope.LoadCostcenterRelatedDataSet2 = function () {
            CcoverviewService.GetCostCenterRelatedDataOnLoad_Set2(parseInt($stateParams.ID), parseInt($cookies['UserId']), 0, false).then(function (CostcenterRelatedDataResultSet2) {
                if (CostcenterRelatedDataResultSet2.StatusCode != 405) {
                    if (CostcenterRelatedDataResultSet2.Response != null) {
                        $scope.tempresultHolderSet2 = CostcenterRelatedDataResultSet2.Response;
                        LoadDetailTemp($scope.tempresultHolderSet2[0], $scope.tempresultHolderSet2[1], $scope.tempresultHolderSet2[2]);
                    }
                }
            });
        }
        $scope.LoadNewsFeedBlock = function () {
            var ID = $stateParams.ID;
            var PagenoforScroll = 0;
            $scope.FeedHtml = '';
            $scope.entityNewsFeeds = {};
            $scope.userimgsrc = '';
            $scope.commentuserimgsrc = '';
            try {
                $('#EntityFeedsdiv').unbind('scroll');
                $('#EntityFeedsdiv').scroll(function () {
                    if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                        PagenoforScroll += 2;
                        CcoverviewService.GetEnityFeeds(ID, PagenoforScroll, $scope.Newsfilter.Feedgrouplistvalues).then(function (getEntityNewsFeedResultForScroll) {
                            var feeddivHtml = '';
                            for (var i = 0; i < getEntityNewsFeedResultForScroll.Response.length; i++) {
                                var feedcomCount = getEntityNewsFeedResultForScroll.Response[i].FeedComment != null ? getEntityNewsFeedResultForScroll.Response[i].FeedComment.length : 0;
                                feeddivHtml = '';
                                if (getEntityNewsFeedResultForScroll.Response[i].Actor == parseInt($cookies['UserId'])) {
                                    $scope.userimgsrc = $scope.NewUserImgSrc;
                                } else {
                                    $scope.userimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResultForScroll.Response[i].Actor;
                                }
                                feeddivHtml = feeddivHtml + '<li data-parent="NewsParent"  data-ID=' + getEntityNewsFeedResultForScroll.Response[i].FeedId + '>';
                                feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                                feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.userimgsrc + ' alt="Avatar"></div>';
                                feeddivHtml = feeddivHtml + '<div class="cmnt" data-parent="parentforcomment">';
                                feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResultForScroll.Response[i].UserEmail + '>' + getEntityNewsFeedResultForScroll.Response[i].UserName + '</a></h5></div>';
                                feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResultForScroll.Response[i].FeedText + '</p></div>';
                                feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResultForScroll.Response[i].FeedHappendTime + '</span>';
                                feeddivHtml = feeddivHtml + '<span class="cmntActions" ><a  data-DynHTML="CommentHTML"  data-commnetid="' + getEntityNewsFeedResultForScroll.Response[i].FeedId + '" id="OverviewComment_' + getEntityNewsFeedResultForScroll.Response[i].FeedId + '" >' + $translate.instant('LanguageContents.Res_5051.Caption') + '</a></span>';
                                if (feedcomCount > 0) {
                                    feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResult.Response[i].FeedId + '" > ' + $translate.instant('LanguageContents.Res_4896.Caption') + ' ' + (feedcomCount - 1) + +$translate.instant('LanguageContents.Res_5054.Caption') + '</a></span></div></div></div>';
                                } else {
                                    feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResult.Response[i].FeedId + '" ></a></span></div></div></div>';
                                }
                                feeddivHtml = feeddivHtml + '<ul class="subComment">';
                                if (getEntityNewsFeedResultForScroll.Response[i].FeedComment != '' && getEntityNewsFeedResultForScroll.Response[i].FeedComment != null) {
                                    var j = 0;
                                    if (getEntityNewsFeedResultForScroll.Response[i].FeedComment[j].Actor == parseInt($cookies['UserId'])) {
                                        $scope.commentuserimgsrc = $scope.NewUserImgSrc;
                                    } else {
                                        $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResultForScroll.Response[i].FeedComment[j].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                    }
                                    for (var k = 0; k < getEntityNewsFeedResultForScroll.Response[i].FeedComment.length; k++) {
                                        $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                        if (k == 0) {
                                            feeddivHtml = feeddivHtml + '<li';
                                        } else {
                                            feeddivHtml = feeddivHtml + '<li style = "display:none;"';
                                        }
                                        feeddivHtml = feeddivHtml + ' id="feedcomment_' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].Id + '"">';
                                        feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                                        feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.commentuserimgsrc + ' alt="Avatar"></div>';
                                        feeddivHtml = feeddivHtml + '<div class="cmnt">';
                                        feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].UserEmail + '>' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].UserName + '</a></h5></div>';
                                        feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].Comment + '</p></div>';
                                        feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].CommentedOn + '</span></div>';
                                        feeddivHtml = feeddivHtml + '</div></div></li>';
                                    }
                                }
                                feeddivHtml = feeddivHtml + '</ul></li>';
                                $('#EntityFeedsdiv').append(feeddivHtml);
                            }
                        });
                    }
                });
            } catch (e) { }
            try {
                $('#EntityFeedsdiv').html('');
                CcoverviewService.GetEnityFeeds(ID, 0, $scope.Newsfilter.Feedgrouplistvalues).then(function (getEntityNewsFeedRes) {
                    getEntityNewsFeedResult = getEntityNewsFeedRes;
                    var feeddivHtml = '';
                    $('#EntityFeedsdiv').html('');
                    if (getEntityNewsFeedResult.Response != null) {
                        for (var i = 0; i < getEntityNewsFeedResult.Response.length; i++) {
                            var feedcomCount = getEntityNewsFeedResult.Response[i].FeedComment != null ? getEntityNewsFeedResult.Response[i].FeedComment.length : 0;
                            feeddivHtml = '';
                            if (getEntityNewsFeedResult.Response[i].Actor == parseInt($cookies['UserId'])) {
                                $scope.userimgsrc = $scope.NewUserImgSrc;
                            } else {
                                $scope.userimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].Actor;
                            }
                            feeddivHtml = feeddivHtml + '<li data-parent="NewsParent" data-ID=' + getEntityNewsFeedResult.Response[i].FeedId + '>';
                            feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                            feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.userimgsrc + ' alt="Avatar"></div>';
                            feeddivHtml = feeddivHtml + '<div class="cmnt" data-parent="parentforcomment">';
                            feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResult.Response[i].UserEmail + '>' + getEntityNewsFeedResult.Response[i].UserName + '</a></h5></div>';
                            feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResult.Response[i].FeedText + '</p></div>';
                            feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResult.Response[i].FeedHappendTime + '</span>';
                            feeddivHtml = feeddivHtml + '<span class="cmntActions" ><a  data-DynHTML="CommentHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedId + '" id="OverviewComment_' + getEntityNewsFeedResult.Response[i].FeedId + '" >' + $translate.instant('LanguageContents.Res_5051.Caption') + '</a></span>';
                            if (feedcomCount > 0) {
                                feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResult.Response[i].FeedId + '" >' + $translate.instant('LanguageContents.Res_4896.Caption') + ' ' + (feedcomCount - 1) + $translate.instant('LanguageContents.Res_5054.Caption') + '</a></span></div></div></div>';
                            } else {
                                feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResult.Response[i].FeedId + '" ></a></span></div></div></div>';
                            }
                            feeddivHtml = feeddivHtml + '<ul class="subComment">';
                            if (getEntityNewsFeedResult.Response[i].FeedComment != '' && getEntityNewsFeedResult.Response[i].FeedComment != null) {
                                var j = 0;
                                if (getEntityNewsFeedResult.Response[i].FeedComment[j].Actor == parseInt($cookies['UserId'])) {
                                    $scope.commentuserimgsrc = $scope.NewUserImgSrc;
                                } else {
                                    $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].FeedComment[j].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                }
                                for (var k = 0; k < getEntityNewsFeedResult.Response[i].FeedComment.length; k++) {
                                    $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].FeedComment[k].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                    if (k == 0) {
                                        feeddivHtml = feeddivHtml + '<li';
                                    } else {
                                        feeddivHtml = feeddivHtml + '<li style = "display:none;"';
                                    }
                                    feeddivHtml = feeddivHtml + ' id="feedcomment_' + getEntityNewsFeedResult.Response[i].FeedComment[k].Id + '"">';
                                    feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                                    feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.commentuserimgsrc + ' alt="Avatar"></div>';
                                    feeddivHtml = feeddivHtml + '<div class="cmnt">';
                                    feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResult.Response[i].FeedComment[k].UserEmail + '>' + getEntityNewsFeedResult.Response[i].FeedComment[k].UserName + '</a></h5></div>';
                                    feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResult.Response[i].FeedComment[k].Comment + '</p></div>';
                                    feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResult.Response[i].FeedComment[k].CommentedOn + '</span></div>';
                                    feeddivHtml = feeddivHtml + '</div></div></li>';
                                }
                            }
                            feeddivHtml = feeddivHtml + '</ul></li>';
                            $('#EntityFeedsdiv').append(feeddivHtml);
                        }
                    }
                    NewsFeedUniqueTimer = $timeout(function () {
                        $scope.TimerForLatestFeed();
                    }, 30000);
                });
            } catch (e) { }
            $scope.TimerForLastestFeedCallBack = function () {
                $scope.TimerForLatestFeed();
                NewsFeedUniqueTimer = $timeout(function () {
                    $scope.TimerForLastestFeedCallBack();
                }, 30000);
            }
            try {
                $scope.TimerForLatestFeed = function () {
                    $scope.FeedHtml = '';
                    $scope.entityNewsFeeds = {};
                    CcoverviewService.GetLastEntityFeeds(ID, $scope.Newsfilter.Feedgrouplistvalues).then(function (getEntityNewsFeedResult1) {
                        var feeddivHtml = '';
                        if (getEntityNewsFeedResult1.Response != null) {
                            for (var i = 0; i < getEntityNewsFeedResult1.Response.length; i++) {
                                var feedcomCount = getEntityNewsFeedResult1.Response[i].FeedComment != null ? getEntityNewsFeedResult1.Response[i].FeedComment.length : 0;
                                feeddivHtml = '';
                                if (getEntityNewsFeedResult1.Response[i].Actor == parseInt($cookies['UserId'])) {
                                    $scope.userimgsrc = $scope.NewUserImgSrc;
                                } else {
                                    $scope.userimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult1.Response[i].Actor;
                                }
                                feeddivHtml = feeddivHtml + '<li data-parent="NewsParent" data-ID=' + getEntityNewsFeedResult1.Response[i].FeedId + '>';
                                feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                                feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.userimgsrc + ' alt="Avatar"></div>';
                                feeddivHtml = feeddivHtml + '<div class="cmnt" data-parent="parentforcomment">';
                                feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResult1.Response[i].UserEmail + '>' + getEntityNewsFeedResult1.Response[i].UserName + '</a></h5></div>';
                                feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResult1.Response[i].FeedText + '</p></div>';
                                feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResult1.Response[i].FeedHappendTime + '</span>';
                                feeddivHtml = feeddivHtml + '<span class="cmntActions" ><a  data-DynHTML="CommentHTML"  data-commnetid="' + getEntityNewsFeedResult1.Response[i].FeedId + '" id="OverviewComment_' + getEntityNewsFeedResult1.Response[i].FeedId + '" >' + $translate.instant('LanguageContents.Res_5051.Caption') + '</a></span>';
                                if (feedcomCount > 0) {
                                    feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResult1.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResult1.Response[i].FeedId + '" > ' + $translate.instant('LanguageContents.Res_4896.Caption') + ' ' + (feedcomCount - 1) + $translate.instant('LanguageContents.Res_5054.Caption') + '</a></span></div></div></div>';
                                } else {
                                    feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResult1.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResult1.Response[i].FeedId + '" ></a></span></div></div></div>';
                                }
                                feeddivHtml = feeddivHtml + '<ul class="subComment"';
                                if (getEntityNewsFeedResult1.Response[i].FeedComment != '' && getEntityNewsFeedResult1.Response[i].FeedComment != null) {
                                    var j = 0;
                                    if (getEntityNewsFeedResult1.Response[i].FeedComment[j].Actor == parseInt($cookies['UserId'])) {
                                        $scope.commentuserimgsrc = $scope.NewUserImgSrc;
                                    } else {
                                        $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult1.Response[i].FeedComment[j].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                    }
                                    for (var k = 0; k < getEntityNewsFeedResult1.Response[i].FeedComment.length; k++) {
                                        $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult1.Response[i].FeedComment[k].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                        if (k == 0) {
                                            feeddivHtml = feeddivHtml + '<li';
                                        } else {
                                            feeddivHtml = feeddivHtml + '<li style = "display:none;"';
                                        }
                                        feeddivHtml = feeddivHtml + ' id="feedcomment_' + getEntityNewsFeedResult1.Response[i].FeedComment[k].Id + '"">';
                                        feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                                        feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.commentuserimgsrc + ' alt="Avatar"></div>';
                                        feeddivHtml = feeddivHtml + '<div class="cmnt">';
                                        feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResult1.Response[i].FeedComment[k].UserEmail + '>' + getEntityNewsFeedResult1.Response[i].FeedComment[k].UserName + '</a></h5></div>';
                                        feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResult1.Response[i].FeedComment[k].Comment + '</p></div>';
                                        feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResult1.Response[i].FeedComment[k].CommentedOn + '</span></div>';
                                        feeddivHtml = feeddivHtml + '</div></div></li>';
                                    }
                                }
                                feeddivHtml = feeddivHtml + '</ul></li>';
                                $('#EntityFeedsdiv [data-id]').each(function () {
                                    if (parseInt($(this).attr('data-id')) == getEntityNewsFeedResult1.Response[i].FeedId) {
                                        $(this).remove();
                                    }
                                });
                                $('#EntityFeedsdiv').prepend(feeddivHtml);
                            }
                        }
                    });
                }
            } catch (e) { }
            $('#EntityFeedsdiv').on('click', 'a[data-DynHTML="CommentshowHTML"]', function (event) {
                event.stopImmediatePropagation();
                var feedid1 = event.target.attributes["id"].nodeValue;
                var feedid = feedid1.substring(13);
                var feedfilter = $.grep(getEntityNewsFeedResult.Response, function (e) {
                    return e.FeedId == parseInt(feedid);
                });
                $("#OverviewCostfeed_" + feedid).next('div').hide();
                $("#OverviewCostfeed_" + feedid).hide();
                for (var i = 0; i < feedfilter[0].FeedComment.length; i++) {
                    if ($('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '')[0].style.display != "none") {
                        $(this)[0].innerHTML = $translate.instant('LanguageContents.Res_4896.Caption') + (feedfilter[0].FeedComment.length - 1) + $translate.instant('LanguageContents.Res_5054.Caption');
                        if (i != 0) $('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideUp(1000, function () { });
                        else $('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { });
                    } else {
                        $('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { });
                        $(this)[0].innerHTML = $translate.instant('LanguageContents.Res_4960.Caption');
                    }
                }
            });
            $('#EntityFeedsdiv').on('click', 'a[data-DynHTML="CommentHTML"]', function (event) {
                var commentuniqueid = this.id;
                event.stopImmediatePropagation();
                var imgpath = "Handlers/UserImage.ashx?id=" + parseInt($cookies['UserId']) + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                $(this).hide();
                var feeddivHtml = '';
                feeddivHtml = feeddivHtml + '<li class="writeNewComment">';
                feeddivHtml = feeddivHtml + '<div class="newComment"><div class="userAvatar"><img data-role="user-avatar" src=\'Handlers/UserImage.ashx?id=' + parseInt($cookies['UserId']) + '&time=' + $scope.DefaultImageSettings.ImageSpan + '\'></div>';
                feeddivHtml = feeddivHtml + '<div class="textarea-wrapper" data-role="textarea"><div contenteditable="true" tabindex="0" class="textarea" id="feedcomment_' + commentuniqueid + '"></div></div>';
                feeddivHtml = feeddivHtml + '<button type="submit" class="btn btn-primary" data-dynCommentBtn="ButtonHTML" >' + $translate.instant('LanguageContents.Res_5051.Caption') + '</button></div></li>';
                var currentobj = $(this).parents('li[data-parent="NewsParent"]').find('ul');
                if ($(this).parents('li[data-parent="NewsParent"]').find('ul').children().length > 0) $(this).parents('li[data-parent="NewsParent"]').find('ul li:first').before(feeddivHtml);
                else $(this).parents('li[data-parent="NewsParent"]').find('ul').html(feeddivHtml);
                $timeout(function () {
                    $('#feedcomment_' + commentuniqueid).html('').focus();
                }, 10);
            });
            $('#EntityFeedsdiv').on('click', 'button[data-dyncommentbtn="ButtonHTML"]', function (event) {
                event.stopImmediatePropagation();
                if ($(this).prev().eq(0).text().toString().trim().length > 0) {
                    var addfeedcomment = {};
                    var FeedidforComment = $(this).parents("li:eq(1)").attr('data-id');
                    var myDate = new Date();
                    addfeedcomment.FeedID = $(this).parents("li:eq(1)").attr('data-id');
                    addfeedcomment.Actor = parseInt($cookies['UserId']);
                    addfeedcomment.Comment = $(this).prev().eq(0).text();
                    var d = new Date();
                    var month = d.getMonth() + 1;
                    var day = d.getDate();
                    var hrs = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                    var output = d.getFullYear() + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day + ' ' + hrs;
                    var _this = $(this);
                    CcoverviewService.InsertFeedComment(addfeedcomment).then(function (saveusercomment) {
                        var feeddivHtml = '';
                        feeddivHtml = feeddivHtml + '<li>';
                        feeddivHtml = feeddivHtml + '<div class=\"newsFeed\">';
                        feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=\'Handlers/UserImage.ashx?id=' + parseInt($cookies['UserId']) + '&time=' + $scope.DefaultImageSettings.ImageSpan + '\' alt="Avatar"></div>';
                        feeddivHtml = feeddivHtml + '<div class="cmnt">';
                        feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5> <a href="undefined">' + $cookies['Username'] + '</a></h5></div>';
                        feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + saveusercomment.Response + '</p></div>';
                        feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + $translate.instant('LanguageContents.Res_5055.Caption') + '</span></div>';
                        feeddivHtml = feeddivHtml + '</div></div></div></li>';
                        $("#EntityFeedsdiv").find('li[data-id=' + FeedidforComment + ']').first().find('li:first').before(feeddivHtml)
                        $(".writeNewComment").remove();
                        $('#OverviewComment_' + addfeedcomment.FeedID).show();
                    });
                }
            });
        }
        $scope.LoadNewsFeedBlock();
        $scope.$on('LoadCostcentreSectionDetail', function (event, data) {
            $('#OverAllEntityStats').select2("enable", false)
            if (NewsFeedUniqueTimer != undefined) {
                $timeout.cancel(NewsFeedUniqueTimer);
            }
            EnableDisableLoader();
            $scope.GetEntityMembers();
            $scope.LoadNewsFeedBlock();
            $scope.EntityStatusResult = [];
            $scope.DynamicTaskListDescriptionObj = '-';
            $scope.LoadCostcenterRelatedData();
            $scope.StatusAttributeData = [];
            $scope.LoadStatusblock = false;
            $timeout(function () { OverviewStatusAttribute(); }, 50);
        });
        $scope.LoadFinancialSummanyBlock = function (CostCentreID) {
            try {
                $scope.financialchartpoints = [];
                CcoverviewService.GettingCostcentreFinancialOverview(CostCentreID).then(function (GetFinancialSummaryBlockResult) {
                    var FinancialResult = GetFinancialSummaryBlockResult.Response;
                    $scope.financialchartpoints = [{
                        label: $translate.instant('LanguageContents.Res_4971.Caption'),
                        y: parseInt((FinancialResult['AvailabletoSpent'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                    }, {
                        label: $translate.instant('LanguageContents.Res_1725.Caption'),
                        y: parseInt((FinancialResult['Spent'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                    }, {
                        label: $translate.instant('LanguageContents.Res_5531.Caption'),
                        y: parseInt((FinancialResult['Committed'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                    }, {
                        label: $translate.instant('LanguageContents.Res_4970.Caption'),
                        y: parseInt((FinancialResult['UnAllocatedAmount'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                    }, {
                        label: $translate.instant('LanguageContents.Res_1740.Caption'),
                        y: parseInt((FinancialResult['ApprovedAllocation'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                    }, {
                        label: $translate.instant('LanguageContents.Res_4968.Caption'),
                        y: parseInt((FinancialResult['BudgetDeviation'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                    }, {
                        label: $translate.instant('LanguageContents.Res_4967.Caption'),
                        y: parseInt((FinancialResult['ApprovedBudget'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                    }, {
                        label: $translate.instant('LanguageContents.Res_1703.Caption'),
                        y: parseInt((FinancialResult['InRequests'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                    }, {
                        label: $translate.instant('LanguageContents.Res_1744.Caption'),
                        y: parseInt((FinancialResult['PlannedAmount'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                    }, {
                        label: $translate.instant('LanguageContents.Res_5531.Caption'),
                        y: parseInt((FinancialResult['TotalAssignedAmount'] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').replace(/ /g, ''))
                    }];
                    CanvasJS.addColorSet("finColorSet1", ["#BDF2A1", "#9ECF6E", "#DDDD69", "#4DA3CE", "#B08BEB", "#3EA0DD", "#F5A52A", "#23BFAA", "#FAA586", "#EB8CC6"]);
                    var chart = new CanvasJS.Chart("financialchart", {
                        animationEnabled: true,
                        colorSet: "finColorSet1",
                        axisX: {
                            tickColor: "white",
                            tickLength: 5,
                            labelFontFamily: "sans-serif",
                            labelFontColor: "#7F7F7F",
                            labelFontSize: 10,
                            labelFontWeight: "normal",
                        },
                        axisY: {
                            tickLength: 0,
                            labelFontSize: 0,
                            lineThickness: 0,
                            gridThickness: 0,
                        },
                        data: [{
                            type: "stackedBar",
                            indexLabel: "{y}",
                            indexLabelFontSize: 12,
                            indexLabelFontWeight: "bold",
                            indexLabelPlacement: "outside",
                            indexLabelOrientation: "horizontal",
                            legendMarkerColor: "gray",
                            dataPoints: $scope.financialchartpoints
                        }]
                    });
                    chart.render();
                });
            } catch (e) { }
        }

        function LoadDetail() {
            var ID = $stateParams.ID;
            try {
                $scope.fields = {
                    usersID: 0,
                };
                $scope.tree = {};
                $scope.fieldKeys = [];
                $scope.options = {};
                $scope.setFieldKeys = function () {
                    var keys = [];
                    angular.forEach($scope.fields, function (key) {
                        keys.push(key);
                        $scope.fieldKeys = keys;
                    });
                }
                $scope.ownername = $cookies['Username'];
                $scope.ownerid = $cookies['UserId'];
                $scope.ownerEmail = $cookies['UserEmail'];
                $scope.listAttriToAttriResult = [];
                try {
                    CcoverviewService.GetAttributeToAttributeRelationsByIDForEntityInOverView(0, ID).then(function (entityAttrToAttrRelation) {
                        $scope.listAttriToAttriResult = [];
                        $scope.listAttriToAttriResult = entityAttrToAttrRelation.Response;
                    });
                } catch (ex) { }
                $scope.StopeUpdateStatusonPageLoad = false;
                $scope.dyn_Cont = '';
                CcoverviewService.GetEntityAttributesDetails(ID).then(function (getentityattributesdetails) {
                    $scope.attributedata = getentityattributesdetails.Response;
                    $scope.dyn_Cont += '<div class="control-group"><label class="control-label" for="label">' + $translate.instant('LanguageContents.Res_69.Caption') + '</label><div class="controls"><label class="control-label" for="label">' + ID + '</label></div></div>'
                    if ($scope.IsLock == undefined) $scope.IsLock = false;
                    if ($scope.IsLock == false) $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\"\>' + $translate.instant('LanguageContents.Res_4985.Caption') + '</label><div class=\"controls\"><a xeditable href=\"javascript:;\" ng-click="costcentreentityclick()" attributeTypeID="1" entityid="' + ID + '" attributeTypeID="1" attributeid="68" id=\"ActivityName\" data-ng-model=\"ActivityName"\   my-qtip2 qtip-content=\"' + $translate.instant('LanguageContents.Res_4985.Caption') + '\"  data-type=\"text\" data-original-title=\"CostCentre Name\">' + $scope.DecodedTextName($scope.RootLevelEntityName) + '</a></div></div>';
                    else if ($scope.IsLock == true) $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\"\>' + $translate.instant('LanguageContents.Res_4985.Caption') + '</label><div class=\"controls\"><label class="control-label">' + $scope.DecodedTextName(getentityattributesdetails.Response[0].Value) + '</label></div></div>';
                    $scope["showActivityName"] = true;
                    $scope["EditActivityName"] = false;
                    $scope["showOwner"] = true;
                    $scope["EditOwner"] = false;
                    for (var i = 0; i < $scope.attributedata.length; i++) {
                        if ($scope.attributedata[i].TypeID == 6) {
                            $scope.dyn_Cont2 = '';
                            var CaptionObj = $scope.attributedata[i].Caption.split(",");
                            for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                                if (j == 0) {
                                    if (CaptionObj[j] != undefined) {
                                        $scope.items.push({
                                            caption: $scope.attributedata[i].Lable[j].Label,
                                            level: j + 1
                                        });
                                        $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                        $scope.settreeTexts();
                                        $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                        $scope.setFieldKeys();
                                    } else {
                                        $scope.items.push({
                                            caption: $scope.attributedata[i].Lable[j].Label,
                                            level: j + 1
                                        });
                                        $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        $scope.settreeTexts();
                                        $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        $scope.setFieldKeys();
                                    }
                                } else {
                                    if (CaptionObj[j] != undefined) {
                                        $scope.items.push({
                                            caption: $scope.attributedata[i].Lable[j].Label,
                                            level: j + 1
                                        });
                                        $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                        $scope.settreeTexts();
                                        $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                        $scope.setFieldKeys();
                                    } else {
                                        $scope.items.push({
                                            caption: $scope.attributedata[i].Lable[j].Label,
                                            level: j + 1
                                        });
                                        $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        $scope.settreeTexts();
                                        $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        $scope.setFieldKeys();
                                    }
                                }
                            }
                            $scope.treelevels["dropdown_levels_" + $scope.attributedata[i].ID] = $scope.items;
                            $scope.settreelevels();
                            $scope.items = [];
                            $scope.settreeSources();
                            $scope.settreeTexts();
                            $scope.settreelevels();
                            $scope.setFieldKeys();
                            for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID + "_" + (j + 1)] = true;
                                var inlineEditabletitile = $scope.treelevels['dropdown_levels_' + $scope.attributedata[i].ID][j].caption;
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><a xeditabletreedropdown href=\"javascript:;\" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" InheritFromParent="' + $scope.attributedata[i].IsInheritFromParent + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '" attributeid="' + $scope.attributedata[i].ID + '" id="fields.DropDown_' + $scope.attributedata[i].ID + "_" + (j + 1) + '" data-ng-model=\"dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" attributename=' + inlineEditabletitile + ' data-type=' + inlineEditabletitile + $scope.attributedata[i].ID + ' >{{treeTexts.dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</a></div></div>';
                            }
                        } else if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == false) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                            var textvalue = "-";
                            if ($scope.attributedata[i].Caption != undefined) textvalue = $scope.attributedata[i].Caption;
                            if ($scope.IsLock == false) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditable href=\"javascript:;\" ng-click="costcentreentityclick()" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"SingleLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"SingleLineText_' + $scope.attributedata[i].ID + '\"  data-type=\"text\"  my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '">' + textvalue + '</a></div></div>';
                            else if ($scope.IsLock == true) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label">' + textvalue + '</label></div></div>';
                        } else if ($scope.attributedata[i].TypeID == 2) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                            var textvalue = "-";
                            if ($scope.attributedata[i].Caption != undefined) textvalue = $scope.attributedata[i].Caption;
                            if ($scope.IsLock == false) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditable href=\"javascript:;\" ng-click="costcentremultilineentityclick()" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"MultiLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"SingleLineText_' + $scope.attributedata[i].ID + '\" data-type=\"textarea\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '">' + textvalue.replace(/\n/g, "<br/>") + '</a></div></div>';
                            else if ($scope.IsLock == true) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label">' + textvalue.replace(/\n/g, "<br/>") + '</label></div></div>';
                        } else if ($scope.attributedata[i].TypeID == 11) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                            $scope.fields["uploader_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                            $scope.setFieldKeys();
                            $scope.UploaderCaption["uploader_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group ng-scope\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable.toString() + '</label>';
                            $scope.dyn_Cont += '<div class=\"controls\">';
                            if ($scope.attributedata[i].Caption == "" || $scope.attributedata[i].Caption == null && $scope.attributedata[i].Caption == undefined) {
                                $scope.attributedata[i].Caption = $scope.attributedata[i].Lable;
                            }
                            if ($scope.attributedata[i].Value == "" || $scope.attributedata[i].Value == null && $scope.attributedata[i].Value == undefined) {
                                $scope.attributedata[i].Value = "NoThumpnail.jpg";
                            }
                            $scope.dyn_Cont += '<img id="UploaderPreview_' + $scope.attributedata[i].ID + '" src="' + imagesrcpath + 'UploadedImages/' + $scope.attributedata[i].Value + '" alt="' + $scope.attributedata[i].Caption + '"';
                            $scope.dyn_Cont += 'class="entityDetailImgPreview" style="height: 112px; display: inline-block; margin-top: 5px; width: 112px; border: 1px solid #ddd; background-color: #eee; text-align: center;">';
                            if ($scope.IsLock == false) {
                                $scope.dyn_Cont += "<a  style='margin-left: 17px;' ng-click='UploadImagefileCC(" + $scope.attributedata[i].ID + ")' attributeTypeID='" + $scope.attributedata[i].TypeID + "'";
                                $scope.dyn_Cont += 'entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="Uploader_' + $scope.attributedata[i].ID + '"';
                                $scope.dyn_Cont += 'my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '"';
                                $scope.dyn_Cont += ' class="ng-pristine ng-valid" attributename=\"' + $scope.UploaderCaption["UploaderCaption_" + $scope.attributedata[i].ID] + '\">Select Image';
                                $scope.dyn_Cont += '</a></div></div>';
                            } else if ($scope.IsLock == true) $scope.dyn_Cont += '</a></div></div>';
                        } else if ($scope.attributedata[i].TypeID == 3) {
                            if ($scope.attributedata[i].ID == SystemDefiendAttributes.Owner && $scope.attributedata[i].IsSpecial == true) {
                                if ($scope.attributedata[i].Caption[0] != undefined) {
                                    $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                    $scope.setFieldKeys();
                                    $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                    $scope.setNormalDropdownCaption();
                                    if ($scope.IsLock == false) $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    else if ($scope.IsLock == true) $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                } else {
                                    $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                    $scope.setFieldKeys();
                                    $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                    $scope.setNormalDropdownCaption();
                                    if ($scope.IsLock == false) {
                                        $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                        $scope.dyn_Cont += '<div class="controls"><a  xeditabledropdown href=\"javascript:;\"';
                                        $scope.dyn_Cont += 'attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '"';
                                        $scope.dyn_Cont += 'attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"';
                                        $scope.dyn_Cont += 'data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '"';
                                        $scope.dyn_Cont += 'attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\"';
                                        $scope.dyn_Cont += 'data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a>';
                                        $scope.dyn_Cont += '</div></div>';
                                    } else if ($scope.IsLock == true) {
                                        $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                        $scope.dyn_Cont += '<div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label>';
                                        $scope.dyn_Cont += '</div></div>';
                                    }
                                }
                            } else {
                                if ($scope.attributedata[i].Caption[0] != undefined) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                                    if ($scope.attributedata[i].Caption[0].length > 1) {
                                        $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                        $scope.setFieldKeys();
                                        $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                        $scope.setNormalDropdownCaption();
                                        if ($scope.IsLock == false) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                        else if ($scope.IsLock == true) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                    } else {
                                        $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                        $scope.setFieldKeys();
                                        $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                        $scope.setNormalDropdownCaption();
                                        if ($scope.IsLock == false) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                        else if ($scope.IsLock == true) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                    }
                                } else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                                    $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                    $scope.setFieldKeys();
                                    $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                    $scope.setNormalDropdownCaption();
                                    if ($scope.IsLock == false) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    else if ($scope.IsLock == true) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                }
                            }
                        } else if ($scope.attributedata[i].TypeID == 4) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                if ($scope.attributedata[i].Caption.length > 1) {
                                    $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                    $scope.setFieldKeys();
                                    $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                    $scope.setNormalMultiDropdownCaption();
                                    if ($scope.IsLock == false) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditablemultiselectdropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalMultiDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    else if ($scope.IsLock == true) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                } else {
                                    $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = "-";
                                    $scope.setFieldKeys();
                                    $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                    $scope.setNormalMultiDropdownCaption();
                                    if ($scope.IsLock == false) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditablemultiselectdropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalMultiDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    else if ($scope.IsLock == true) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                }
                            } else {
                                $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalMultiDropdownCaption();
                                if ($scope.IsLock == false) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditablemultiselectdropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalMultiDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                else if ($scope.IsLock == true) $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                            }
                        } else if ($scope.attributedata[i].TypeID == 10) {
                            var inlineEditabletitile = $scope.attributedata[i].Caption;
                            perioddates = [];
                            $scope.dyn_Cont += '<div class="period control-group nomargin" data-periodcontainerID="periodcontainerID">';
                            if ($scope.attributedata[i].Value == "-") {
                                $scope.IsStartDateEmpty = true;
                                $scope.dyn_Cont += '<div class="control-group nomargin" data-addperiodID="addperiodID">';
                                $scope.dyn_Cont += '</div>';
                            } else {
                                for (var j = 0; j < $scope.attributedata[i].Value.length; j++) {
                                    var datStartUTCval = "";
                                    var datstartval = "";
                                    var datEndUTCval = "";
                                    var datendval = "";
                                    datStartUTCval = $scope.attributedata[i].Value[j].Startdate.substr(0, 10);
                                    datstartval = new Date.create(datStartUTCval);
                                    datEndUTCval = $scope.attributedata[i].Value[j].EndDate.substr(0, 10);
                                    datendval = new Date.create(datEndUTCval);
                                    perioddates.push({
                                        ID: $scope.attributedata[i].Value[j].Id,
                                        value: datendval
                                    });
                                    $scope.fields["PeriodStartDateopen_" + $scope.attributedata[i].Value[j].Id] = false;
                                    $scope.fields["PeriodEndDateopen_" + $scope.attributedata[i].Value[j].Id] = false;
                                    $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id] = dateFormat(datstartval, $scope.GetDefaultSettings.DateFormat);
                                    $scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] = dateFormat(datendval, $scope.GetDefaultSettings.DateFormat);
                                    $scope.fields["PeriodStartDate_Dir_" + $scope.attributedata[i].Value[j].Id] = formatteddateFormat(datstartval, "dd/MM/yyyy");
                                    $scope.fields["PeriodEndDate_Dir_" + $scope.attributedata[i].Value[j].Id] = formatteddateFormat(datendval, "dd/MM/yyyy");
                                    if ($scope.attributedata[i].Value[j].Description == undefined) {
                                        $scope.fields["PeriodDateDesc_Dir_" + $scope.attributedata[i].Value[j].Id] = "-";
                                        $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = "";
                                    } else {
                                        $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = $scope.attributedata[i].Value[j].Description;
                                        $scope.fields["PeriodDateDesc_Dir_" + $scope.attributedata[i].Value[j].Id] = $scope.attributedata[i].Value[j].Description;
                                    }
                                    $('#fsedateid').css("visibility", "hidden");
                                    $scope.dyn_Cont += '<div class="control-group nomargin" data-dynPeriodID="' + $scope.attributedata[i].Value[j].Id + '">';
                                    $scope.dyn_Cont += '<div class="inputHolder span11">';
                                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label>';
                                    $scope.dyn_Cont += '<div class="controls">';
                                    if ($scope.attributedata[i].IsReadOnly == true) {
                                        $scope.dyn_Cont += '<label class="control-label">{{fields.PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '}}';
                                        $scope.dyn_Cont += ' to {{fields.PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '}}</label>';
                                    } else {
                                        if ($scope.IsLock == false) {
                                            $scope.MinValue = $scope.attributedata[i].MinValue;
                                            $scope.MaxValue = $scope.attributedata[i].MaxValue;
                                            $scope.MinValue = $scope.attributedata[i].MinValue;
                                            $scope.MaxValue = $scope.attributedata[i].MaxValue;
                                            $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID] = new Date.create();
                                            $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID] = new Date.create();
                                            if ($scope.MinValue < 0) {
                                                $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MinValue + 1));
                                            } else {
                                                $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MinValue));
                                            }
                                            if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                                $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MaxValue - 1));
                                            } else {
                                                $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].getDate() + 100000);
                                            }
                                            var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID], $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID]);
                                            $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID] = (temp.MinDate);
                                            $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID] = (temp.MaxDate);
                                            $scope.dyn_Cont += '<a xeditabletreedropdown1 data-primaryid="' + $scope.attributedata[i].Value[j].Id + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodstartdate_id=\"PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '\" data-ng-model=\"PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"perdiodate' + $scope.attributedata[i].Value[j].Id + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '}}</a>';
                                            $scope.dyn_Cont += '<a> to </a><a xeditabletreedropdown1 data-primaryid="' + $scope.attributedata[i].Value[j].Id + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodenddate_id=\"PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '\" data-ng-model=\"PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\"  data-type=\"perdiodate' + $scope.attributedata[i].Value[j].Id + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '}}</a>';
                                        } else if ($scope.IsLock == true) {
                                            $scope.dyn_Cont += '<label class="control-label">{{fields.PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '}}';
                                            $scope.dyn_Cont += ' to {{fields.PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '}}</label>';
                                        }
                                    }
                                    $scope.dyn_Cont += '</div></div>';
                                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $translate.instant('LanguageContents.Res_5051.Caption') + inlineEditabletitile + '</label>';
                                    $scope.dyn_Cont += '<div class="controls">';
                                    if ($scope.attributedata[i].IsReadOnly == true) {
                                        $scope.dyn_Cont += '<label class="control-label">{{fields.PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '}}</label>';
                                    } else {
                                        if ($scope.IsLock == false) {
                                            $scope.dyn_Cont += '<a xeditabletreedropdown1 data-primaryid="' + $scope.attributedata[i].Value[j].Id + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodenddate_id=\"PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '\" data-ng-model=\"PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\"  data-type=\"perdiodate' + $scope.attributedata[i].Value[j].Id + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '}}</a>';
                                        } else if ($scope.IsLock == true) {
                                            $scope.dyn_Cont += '<label class="control-label">{{fields.PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '}}</label>';
                                        }
                                    }
                                    $scope.dyn_Cont += '</div></div></div>';
                                    if (j != 0) {
                                        if ($scope.IsLock == false) {
                                            $scope.dyn_Cont += '<div class="buttonHolder span1"><a ng-click="deletePeriodDate(' + $scope.attributedata[i].Value[j].Id + ')"><i class="icon-remove"></i></a></div>';
                                        }
                                    }
                                    $scope.dyn_Cont += '</div>';
                                    if (j == ($scope.attributedata[i].Value.length - 1)) {
                                        $scope.dyn_Cont += '<div class="control-group nomargin" data-addperiodID="addperiodID">';
                                        $scope.dyn_Cont += '</div>';
                                    }
                                }
                            }
                            $scope.dyn_Cont += ' </div>';
                            $scope.dyn_Cont += '<div class="control-group nomargin">';
                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<label  data-tempid="startendID" class="control-label" for="label">' + $translate.instant('LanguageContents.Res_5056.Caption') + '</label>';
                                $scope.dyn_Cont += '<a class="ng-pristine ng-valid editable editable-click" xeditabletreedropdown1 data-primaryid="0" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodstartdate_id=\"PeriodStartDate_0" data-ng-model=\"PeriodStartDate_0"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"perdiodate0" href=\"javascript:;\" attributename="perdiodate">[' + $translate.instant('LanguageContents.Res_5065.Caption') + ' ]</a>';
                            } else {
                                if ($scope.IsLock == false) {
                                    if ($scope.attributedata[i].Value == "-") {
                                        $scope.dyn_Cont += '<label id="fsedateid"  class="control-label" for="label">' + inlineEditabletitile + '</label>';
                                    }
                                    $scope.dyn_Cont += '<div class="controls">';
                                    $scope.dyn_Cont += '<a class="ng-pristine ng-valid editable editable-click" xeditabletreedropdown1 data-primaryid="0" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodstartdate_id=\"PeriodStartDate_0" data-ng-model=\"PeriodStartDate_0"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"perdiodate0" href=\"javascript:;\" attributename="perdiodate">[Add ' + inlineEditabletitile + ' ]</a>';
                                    $scope.dyn_Cont += '</div>';
                                } else if ($scope.IsLock == true) {
                                    $scope.dyn_Cont += '<span></span>';
                                }
                            }
                            $scope.dyn_Cont += '</div>';
                        } else if ($scope.attributedata[i].TypeID == 5 && $scope.attributedata[i].ID != SystemDefiendAttributes.ApproveTime) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                            var datStartUTCval = "";
                            var datstartval = "";
                            var inlineEditabletitile = $scope.attributedata[i].Caption;
                            if ($scope.attributedata[i].Value != null || $scope.attributedata[i].Value != undefined) {
                                datStartUTCval = $scope.attributedata[i].Value.substr(0, 10);
                                datstartval = new Date.create(datStartUTCval);
                                $scope.fields["DateTime_" + $scope.attributedata[i].ID] = dateFormat(datstartval, $scope.GetDefaultSettings.DateFormat);
                                $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = formatteddateFormat(datstartval, "dd/MM/yyyy");
                            } else {
                                $scope.fields["DateTime_" + $scope.attributedata[i].ID] = "-";
                                $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = "-";
                            }
                            if ($scope.attributedata[i].ID != SystemDefiendAttributes.CreationDate) {
                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                } else {
                                    if ($scope.IsLock == false) {
                                        $scope.MinValue = $scope.attributedata[i].MinValue;
                                        $scope.MaxValue = $scope.attributedata[i].MaxValue;
                                        $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID] = new Date.create();
                                        $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID] = new Date.create();
                                        if ($scope.MinValue < 0) {
                                            $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MinValue + 1));
                                        } else {
                                            $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MinValue));
                                        }
                                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                            $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MaxValue - 1));
                                        } else {
                                            $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].getDate() + 100000);
                                        }
                                        var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID], $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID]);
                                        $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID] = (temp.MinDate);
                                        $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID] = (temp.MaxDate);
                                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a xeditabletreedatetime data-primaryid="' + $scope.attributedata[i].ID + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-datetime_id=\"DateTime_' + $scope.attributedata[i].ID + '\" data-ng-model=\"DateTime_' + $scope.attributedata[i].ID + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"datetime' + $scope.attributedata[i].ID + '\" href=\"javascript:;\" attributename="datetime">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                        $scope.fields["DatePart_" + $scope.attributedata[i].ID] = null;
                                    } else if ($scope.IsLock == true) {
                                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                    }
                                }
                            } else {
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                            }
                        } else if ($scope.attributedata[i].TypeID == 12) {
                            $scope.dyn_Cont2 = '';
                            var CaptionObj = $scope.attributedata[i].Caption;
                            for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                                if ($scope.attributedata[i].Lable.length == 1) {
                                    var k = j;
                                    var treeTexts = [];
                                    var fields = [];
                                    $scope.items.push({
                                        caption: $scope.attributedata[i].Lable[j].Label,
                                        level: j + 1
                                    });
                                    if (k == CaptionObj.length) {
                                        $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    } else {
                                        if (CaptionObj[k] != undefined) {
                                            for (k; k < CaptionObj.length; k++) {
                                                treeTexts.push(CaptionObj[k]);
                                                $scope.settreeTexts();
                                                fields.push(CaptionObj[k]);
                                                $scope.setFieldKeys();
                                            }
                                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = treeTexts.join(", ");
                                            $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = fields.join(", ");
                                        } else {
                                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        }
                                    }
                                } else {
                                    if (j == 0) {
                                        if (CaptionObj[j] != undefined) {
                                            $scope.items.push({
                                                caption: $scope.attributedata[i].Lable[j].Label,
                                                level: j + 1
                                            });
                                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                            $scope.settreeTexts();
                                            $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                            $scope.setFieldKeys();
                                        } else {
                                            $scope.items.push({
                                                caption: $scope.attributedata[i].Lable[j].Label,
                                                level: j + 1
                                            });
                                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            $scope.settreeTexts();
                                            $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            $scope.setFieldKeys();
                                        }
                                    } else {
                                        var k = j;
                                        if (j == ($scope.attributedata[i].Lable.length - 1)) {
                                            var treeTexts = [];
                                            var fields = [];
                                            $scope.items.push({
                                                caption: $scope.attributedata[i].Lable[j].Label,
                                                level: j + 1
                                            });
                                            if (k == CaptionObj.length) {
                                                $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                                $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            } else {
                                                if (CaptionObj[k] != undefined) {
                                                    for (k; k < CaptionObj.length; k++) {
                                                        treeTexts.push(CaptionObj[k]);
                                                        $scope.settreeTexts();
                                                        fields.push(CaptionObj[k]);
                                                        $scope.setFieldKeys();
                                                    }
                                                    $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = treeTexts.join(", ");
                                                    $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = fields.join(", ");
                                                } else {
                                                    $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                                    $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                                }
                                            }
                                        } else {
                                            if (CaptionObj[j] != undefined) {
                                                $scope.items.push({
                                                    caption: $scope.attributedata[i].Lable[j].Label,
                                                    level: j + 1
                                                });
                                                $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                                $scope.settreeTexts();
                                                $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                                $scope.setFieldKeys();
                                            } else {
                                                $scope.items.push({
                                                    caption: $scope.attributedata[i].Lable[j].Label,
                                                    level: j + 1
                                                });
                                                $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                                $scope.settreeTexts();
                                                $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                                $scope.setFieldKeys();
                                            }
                                        }
                                    }
                                }
                            }
                            $scope.treelevels["multiselectdropdown_levels_" + $scope.attributedata[i].ID] = $scope.items;
                            $scope.settreelevels();
                            $scope.items = [];
                            $scope.settreeSources();
                            $scope.settreeTexts();
                            $scope.settreelevels();
                            $scope.setFieldKeys();
                            for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID + "_" + (j + 1)] = true;
                                var inlineEditabletitile = $scope.treelevels['multiselectdropdown_levels_' + $scope.attributedata[i].ID][j].caption;
                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><label class="control-label">{{treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</label></div></div>';
                                } else {
                                    if ($scope.IsLock == false) {
                                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><a xeditablemultiselecttreedropdown href=\"javascript:;\" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" InheritFromParent="' + $scope.attributedata[i].IsInheritFromParent + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '" attributeid="' + $scope.attributedata[i].ID + '" id="fields.MultiSelectDropDown_' + $scope.attributedata[i].ID + "_" + (j + 1) + '" data-ng-model=\"multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\"  attributename=' + inlineEditabletitile + ' data-type=' + inlineEditabletitile + ' >{{treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</a></div></div>';
                                    } else if ($scope.IsLock == true) {
                                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><label class="control-label">{{treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</label></div></div>';
                                    }
                                }
                            }
                        } else if ($scope.attributedata[i].TypeID == 5 && $scope.attributedata[i].ID != SystemDefiendAttributes.ApproveTime) {
                            var datStartUTCval = "";
                            var datstartval = "";
                            datstartval = new Date($scope.attributedata[i].Value);
                            $scope.fields["DateTime_" + $scope.attributedata[i].ID] = dateFormat(ConvertDateFromStringToString(ConvertDateToString(datstartval)).toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3").toString('dd-MM-yyyy'), $scope.GetDefaultSettings.DateFormat);
                            $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = dateFormat(ConvertDateFromStringToString(ConvertDateToString(datstartval)).toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3").toString('dd-MM-yyyy'), $scope.GetDefaultSettings.DateFormat);
                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                            } else {
                                if ($scope.IsLock == false) {
                                    $scope.MinValue = $scope.attributedata[i].MinValue;
                                    $scope.MaxValue = $scope.attributedata[i].MaxValue;
                                    $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID] = new Date.create();
                                    $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID] = new Date.create();
                                    if ($scope.MinValue < 0) {
                                        $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MinValue + 1));
                                    } else {
                                        $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MinValue));
                                    }
                                    if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                        $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MaxValue - 1));
                                    } else {
                                        $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].getDate() + 100000);
                                    }
                                    var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID], $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID]);
                                    $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID] = (temp.MinDate);
                                    $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID] = (temp.MaxDate);
                                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a xeditabletreedatetime data-primaryid="' + $scope.attributedata[i].ID + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-datetime_id=\"DateTime_' + $scope.attributedata[i].ID + '\" data-ng-model=\"DateTime_' + $scope.attributedata[i].ID + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"datetime' + $scope.attributedata[i].ID + '\" href=\"javascript:;\" attributename="datetime">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                } else if ($scope.IsLock == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                }
                            }
                        } else if ($scope.attributedata[i].TypeID == 17) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                if ($scope.attributedata[i].Caption.length > 1) {
                                    $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                    $scope.fields["TagWordsSeleted_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Value;
                                }
                            } else {
                                $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = "-";
                                $scope.fields["TagWordsSeleted_" + $scope.attributedata[i].ID] = [];
                            }
                            $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                            } else {
                                if ($scope.IsLock == false) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabletagwords href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="TagWordsCaption_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.TagWordsCaption_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                } else if ($scope.IsLock == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                }
                            }
                        }
                    }
                    $("#dynamicdetail").html($compile($scope.dyn_Cont)($scope));
                    HideAttributeToAttributeRelationsOnPageLoad();
                    i = 0;
                    for (i = 0; i < $scope.attributedata.length; i++) {
                        if (($scope.attributedata[i].TypeID == 3 || $scope.attributedata[i].TypeID == 6 || $scope.attributedata[i].TypeID == 12) && $scope.attributedata[i].IsSpecial == false) {
                            if ($scope.attributedata[i].TypeID == 12) {
                                for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                                    if ($scope.attributedata[i].Value[j] != undefined && $scope.attributedata[i].Value[j] != null) {
                                        if (($scope.attributedata[i].Lable.length - 1) == j) {
                                            var k = $scope.attributedata[i].Value.length - $scope.attributedata[i].Lable.length;
                                            for (k; k < $scope.attributedata[i].Value.length; k++) {
                                                $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Lable[j].Level, $scope.attributedata[i].Value[k].Nodeid);
                                            }
                                        } else {
                                            $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Lable[j].Level, $scope.attributedata[i].Value[j].Nodeid);
                                        }
                                    }
                                }
                            } else if ($scope.attributedata[i].TypeID == 6) {
                                for (var j = 0; j < $scope.attributedata[i].Lable.length - 1; j++) {
                                    if ($scope.attributedata[i].Value[j] != undefined && $scope.attributedata[i].Value[j] != null) $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Lable[j].Level, $scope.attributedata[i].Value[j].Nodeid);
                                }
                            } else {
                                $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Level, $scope.attributedata[i].Value);
                            }
                        }
                    }
                });
                $scope.CurrentWorkflowStepName = "";
                $scope.workflowstepOptions = [];
                $scope.workflowSummaryList = [];
                $scope.SelectedWorkflowStep = 0;
                $scope.WorkFlowStepID = 0;
                $scope.WorkflowSummaryControls = '';
                try {
                    $scope.UpdateEntityWorkFlowStep = function (stepID) {
                        var TaskStatusData = {};
                        TaskStatusData.EntityID;
                        parseInt($stateParams.ID, 10);
                        TaskStatusData.Status = stepID;
                        CcoverviewService.UpdateEntityActiveStatus(TaskStatusData).then(function (TaskStatusResult) {
                            if (TaskStatusResult.StatusCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_4375.Caption'));
                            } else {
                                NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                                for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                                    for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                                        var EntityId = $scope.ListViewDetails[i].data.Response.Data[j]["Id"];
                                        if (EntityId == parseInt($stateParams.ID, 10)) {
                                            if ($scope.ListViewDetails[i].data.Response.Data[j][SystemDefiendAttributes.Status] != undefined) $scope.ListViewDetails[i].data.Response.Data[j][SystemDefiendAttributes.Status] = $scope.CurrentWorkflowStepName;
                                            else $scope.ListViewDetails[i].data.Response.Data[j]["Status"] = $scope.CurrentWorkflowStepName;
                                            return false;
                                        }
                                    }
                                }
                                $scope.SelectedWorkflowStep = 0;
                            }
                        });
                    }
                } catch (exc) { }
                $scope.saveDropdownTree = function (attrID, attributetypeid, entityTypeid, optionarray) {
                    if (attrID == 57) {
                        var updateentityattrib = {};
                        updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                        updateentityattrib.AttributeID = attrID;
                        updateentityattrib.Level = 0;
                        updateentityattrib.NewValue = optionarray == "" ? ["0"] : [optionarray];
                        updateentityattrib.AttributetypeID = attributetypeid;
                        CcoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                            if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                            else NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                            UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                        });
                        $scope.treeSelection = [];
                    } else if (attributetypeid == 1) {
                        var updateentityattrib = {};
                        updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                        updateentityattrib.AttributeID = attrID;
                        updateentityattrib.Level = 0;
                        updateentityattrib.NewValue = optionarray == "" ? ["0"] : [optionarray]
                        updateentityattrib.AttributetypeID = attributetypeid;
                        if (attrID == 68) {
                            $(window).trigger("UpdateEntityName", optionarray);
                            $("ul li a[data-entityid='" + $stateParams.ID + "'] span[data-name='text']").text([optionarray]);
                        }
                        CcoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                            if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                            else {
                                $scope.fields['SingleLineTextValue_' + attrID] = optionarray;
                                NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                                UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                                if (attrID == 68) {
                                    $('#breadcrumlink').text(optionarray);
                                }
                            }
                        });
                        $scope.treeSelection = [];
                    } else if (attributetypeid == 3 || attributetypeid == 4 || attributetypeid == 17) {
                        var updateentityattrib = {};
                        updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                        updateentityattrib.AttributeID = attrID;
                        updateentityattrib.Level = 0;
                        updateentityattrib.NewValue = optionarray == "" ? ["0"] : [optionarray];
                        updateentityattrib.AttributetypeID = attributetypeid;
                        CcoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                            if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                            else $scope.ShowHideAttributeToAttributeRelations(attrID, 0, 0, attributetypeid, optionarray);
                            NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                            UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                        });
                        $scope.treeSelection = [];
                    } else if (attributetypeid == 6) {
                        var updateentityattrib = {};
                        updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                        updateentityattrib.AttributeID = attrID;
                        updateentityattrib.Level = 0;
                        updateentityattrib.NewValue = optionarray;
                        updateentityattrib.AttributetypeID = attributetypeid;
                        CcoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                            if (updateentityattribresult.StatusCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                            } else {
                                NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                                for (var i = 0; i < optionarray.length; i++) {
                                    if (optionarray[i] != 0) {
                                        var level = i + 1;
                                        UpdateTreeScope(attrID + "_" + level, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"][data-ng-model="dropdown_text_' + attrID + "_" + level + '"]').text());
                                    }
                                }
                                for (var j = 0; j < optionarray.length; j++) {
                                    if (optionarray[j] != 0) {
                                        $scope.ShowHideAttributeToAttributeRelations(attrID, (j) + 1, optionarray.length, attributetypeid, optionarray[j]);
                                    }
                                }
                            }
                        });
                        $scope.treeSelection = [];
                    } else if (attributetypeid == 12) {
                        var updateentityattrib = {};
                        updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                        updateentityattrib.AttributeID = attrID;
                        updateentityattrib.Level = 0;
                        updateentityattrib.NewValue = optionarray;
                        updateentityattrib.AttributetypeID = attributetypeid;
                        CcoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                            if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                            else {
                                for (var j = 0; j < optionarray.length; j++) {
                                    if (optionarray[j] != 0) {
                                        $scope.ShowHideAttributeToAttributeRelations(attrID, (j) + 1, optionarray.length, attributetypeid, optionarray[j]);
                                    }
                                }
                                NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                                UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                            }
                        });
                        $scope.treeSelection = [];
                    } else if (attributetypeid == 8) {
                        if (!isNaN(optionarray.toString())) {
                            var updateentityattrib = {};
                            updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                            updateentityattrib.AttributeID = attrID;
                            updateentityattrib.Level = 0;
                            updateentityattrib.NewValue = optionarray == "" ? ["0"] : [optionarray];
                            updateentityattrib.AttributetypeID = attributetypeid;
                            CcoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                                if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                                else {
                                    $scope.fields['SingleLineTextValue_' + attrID] = optionarray == "" ? 0 : optionarray;
                                    NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                                    UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                                }
                            });
                        } else {
                            NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                        }
                        $scope.treeSelection = [];
                    } else if (attributetypeid == 5) {
                        var sdate = new Date($scope.fields['DateTime_Dir_' + attrID].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
                        if (sdate == 'NaN-NaN-NaN') {
                            sdate = ConvertDateToString(ConvertStringToDateByFormat($scope.fields['DateTime_Dir_' + periodid].toString(), GlobalUserDateFormat))
                        }
                        var updateentityattrib = {};
                        updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                        updateentityattrib.AttributeID = attrID;
                        updateentityattrib.Level = 0;
                        updateentityattrib.NewValue = [sdate];
                        updateentityattrib.AttributetypeID = attributetypeid;
                        CcoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                            if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                            else {
                                NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                                UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                                $scope.fields["DateTime_" + attrID] = ConvertDateFromStringToString(ConvertDateToString(sdate));
                            }
                        });
                        $scope.treeSelection = [];
                    }
                    $scope.treeSelection = [];
                    $timeout(function () {
                        $scope.TimerForLatestFeed();
                    }, 5000);
                };
                $scope.enabletext = function () {
                    $('#username').editable({
                        placement: 'right',
                        title: 'Select Status'
                    });
                }
                $scope.saveDetailBlock = function (val) {
                    var NewValue = '';
                    if (val == 1) {
                        NewValue = $scope.txtActivityName;
                        $scope["showActivityName"] = true;
                        $scope["EditActivityName"] = false;
                    }
                    if (val == 2) {
                        NewValue = $scope.fields.DropDown_Members.Id;
                        $scope["showOwner"] = true;
                        $scope["EditOwner"] = false;
                    }
                    if (val == 3) {
                        NewValue = $scope.fields.DropDown_FiscalYear.Id;
                        $scope["HideInlineEdit0"] = true;
                        $scope["showinlineedit0"] = false;
                    }
                    var updateentityattrib = {};
                    updateentityattrib.ID = val;
                    updateentityattrib.EntityID = ID;
                    updateentityattrib.NewValue = NewValue;
                    CcoverviewService.SaveDetailBlock(updateentityattrib).then(function () {
                        $scope.TimerForLatestFeed();
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                    });
                };
            } catch (e) { }
            try {
                $scope.GettingNewsFeedTime = function (serverTime, happendTime) {
                    var appServerTime = serverTime;
                    var feedHappendTime = happendTime;
                    var timeDiff = Math.abs(appServerTime - feedHappendTime);
                }
            } catch (e) { }
            $scope.LoadFinancialSummanyBlock(ID);
        };
        EnableDisableLoader();

        function EnableDisableLoader() {
            $scope.detailsLoader = true;
            $scope.detailsData = false;
        }
        $scope.treesrcdirec = {};
        $scope.staticTreesrcdirec = {};
        $scope.TreeEmptyAttributeObj = {};
        $scope.treeNodeSelectedHolder = new Array();
        var apple_selected, tree, treedata_avm, treedata_geography;
        $scope.my_tree_handler = function (branch, parentArr) {
            var _ref;
            if ((_ref = branch.data) != null ? _ref.description : void 0) {
                return $scope.output += '(' + branch.data.description + ')';
            }
        };
        var treeTextVisbileflag = false;

        function IsNotEmptyTree(treeObj) {
            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    treeTextVisbileflag = true;
                    return treeTextVisbileflag;
                } else {
                    IsNotEmptyTree(node.Children);
                }
            }
            return treeTextVisbileflag;
        }
        $scope.savetreeDetail = function (attrID, attributetypeid, entityTypeid) {
            if (attributetypeid == 7) {
                $scope.treeNodeSelectedHolder = [];
                GetTreeObjecttoSave(attrID);
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.NewValue = $scope.treeNodeSelectedHolder;
                updateentityattrib.newTree = $scope.staticTreesrcdirec["Attr_" + attrID];
                updateentityattrib.oldTree = $scope.treesrcdirec["Attr_" + attrID];
                updateentityattrib.AttributetypeID = attributetypeid;
                CcoverviewService.SaveDetailBlockForTreeLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    } else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        CcoverviewService.GetAttributeTreeNodeByEntityID(attrID, parseInt($stateParams.ID, 10)).then(function (GetTree) {
                            $scope.treesrcdirec["Attr_" + attrID] = JSON.parse(GetTree.Response).Children;
                            if ($scope.treeNodeSelectedHolder.length > 0) $scope.TreeEmptyAttributeObj["Attr_" + attrID] = true;
                            else $scope.TreeEmptyAttributeObj["Attr_" + attrID] = false;
                            $timeout(function () {
                                $scope.TimerForLatestFeed();
                            }, 3000);
                        });
                        $scope.fields["Tree_" + attrID].splice(0, $scope.fields["Tree_" + attrID].length);
                        GetTreeCheckedNodes($scope.treeNodeSelectedHolder, attrID);
                        $scope.ShowHideAttributeToAttributeRelations(attrID, 0, 0, attributetypeid, $scope.fields["Tree_" + attrID]);
                    }
                });
            }
        }

        function GetTreeObjecttoSave(attributeid) {
            GenerateTreeStructure($scope.staticTreesrcdirec["Attr_" + attributeid]);
        }
        var treeformflag = false;

        function GenerateTreeStructure(treeobj) {
            for (var i = 0, node; node = treeobj[i++];) {
                if (node.ischecked == true) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                        return e.AttributeId == node.AttributeId && e.id == node.id;
                    });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolder.push(node);
                    }
                    treeformflag = false;
                    if (ischildSelected(node.Children)) {
                        GenerateTreeStructure(node.Children);
                    } else {
                        GenerateTreeStructure(node.Children);
                    }
                } else GenerateTreeStructure(node.Children);
            }
        }

        function ischildSelected(children) {
            for (var j = 0, child; child = children[j++];) {
                if (child.ischecked == true) {
                    treeformflag = true;
                    return treeformflag
                }
            }
            return treeformflag;
        }
        $scope.costcentreentityclick = function () {
            $timeout(function () {
                $('[class=input-medium]').focus().select()
            }, 10);
        };
        $scope.costcentremultilineentityclick = function () {
            $timeout(function () {
                $('[class=input-large]').focus().select()
            }, 10);
        };

        function GetTreeCheckedNodes(treeobj, attrID) {
            for (var i = 0, node; node = treeobj[i++];) {
                if (node.ischecked == true) {
                    $scope.fields["Tree_" + attrID].push(node.id);
                }
                if (node.Children.length > 0) GetTreeCheckedNodes(node.Children, attrID);
            }
        }

        function HideAttributeToAttributeRelationsOnPageLoad() {
            try {
                if ($scope.listAttriToAttriResult != undefined) {
                    for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
                        var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
                                } else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                }
                            }
                        }
                    }
                }
            } catch (e) { }
        }
        $scope.ShowHideAttributeToAttributeRelationsOnpageLoad = function (attrID, attributeLevel, attrVal, attrType) {
            try {
                var optionValue = attrVal;
                var attributesToShow = [];
                if (attrType == 3) {
                    attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeOptionID == optionValue;
                    })[0]);
                } else if (attrType == 4) {
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 7) {
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 6 || attrType == 12) {
                    if (attrVal != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrVal != null) ? parseInt(attrVal, 10) : 0) && e.AttributeLevel == ((attributeLevel != null) ? parseInt(attributeLevel, 10) : 0));
                        })[0]);
                    }
                }
                if (attributesToShow[0] != undefined) {
                    for (var i = 0; i < attributesToShow.length; i++) {
                        var attrRelIDs = attributesToShow[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = true;
                                } else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = true;
                                }
                            }
                        }
                    }
                }
            } catch (e) { }
        }

        function RecursiveAttributeToAttributeHide(res, attrID, attrLevel, attrType) {
            var recursiveAttrID = '';
            var attributesToHide = [];
            if (attrLevel > 0) {
                attributesToHide.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID && e.AttributeLevel >= attrLevel);
                }));
            } else {
                attributesToHide.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID);
                }));
            }
            if (attributesToHide[0] != undefined) {
                for (var i = 0; i < attributesToHide[0].length; i++) {
                    var attrRelIDs = attributesToHide[0][i].AttributeRelationID.toString().split(',');
                    if (attrRelIDs != undefined) {
                        for (var j = 0; j < attrRelIDs.length; j++) {
                            if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return (e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))) && e.AttributeLevel == attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].length));
                                }));
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType);
                                    }
                                }
                            } else {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                $scope.fields['ListSingleSelection_' + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = "";
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                }));
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $scope.ShowHideAttributeToAttributeRelations = function (attrID, attributeLevel, levelcnt, attrType, attrVal) {
            try {
                var recursiveAttrID = '';
                var optionValue = '';
                var attributesToHide = [];
                var hideAttributeOtherThanSelected = [];
                if (attrID == SystemDefiendAttributes.FiscalYear) {
                    try {
                        $scope.changeCostCenterSource();
                    } catch (e) { }
                }
                if ($scope.listAttriToAttriResult == null && $scope.listAttriToAttriResult == undefined) {
                    return false;
                }
                RecursiveAttributeToAttributeHide($scope.listAttriToAttriResult, attrID, attributeLevel, attrType);
                if (attrType == 3) {
                    optionValue = attrVal;
                    attributesToHide.push($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && e.AttributeOptionID == optionValue;
                    })[0]);
                } else if (attrType == 4) {
                    optionValue = attrVal;
                    attributesToHide = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 6) {
                    var attrval = $scope.fields["DropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToHide = [];
                        attributesToHide.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                } else if (attrType == 7) {
                    optionValue = attrVal;
                    attributesToHide = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 12) {
                    var attrval = $scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToHide = [];
                        attributesToHide.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                }
                if (attributesToHide[0] != undefined) {
                    for (var i = 0; i < attributesToHide.length; i++) {
                        var attrRelIDs = attributesToHide[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = true;
                                } else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = true;
                                }
                            }
                        }
                    }
                }
            } catch (e) { }
        }
        $scope.OverAllEntityStatusVis = true;
        $scope.ReloadFromTimeout = false;

        function funloadEntityStatusForCCEdit(EntityStatusByID, EntityStatus) {
            var EntityStatusResult = EntityStatusByID;
            $scope.OverAllEntityStatusVis = true;
            var EntityStatusRes = EntityStatus;
            $scope.OverAllEntityStatusVis = true;
            if (EntityStatusRes.length > 0) {
                $scope.EntityStatusResult = EntityStatusRes;
                $scope.Showstatus = false;
                $timeout(function () {
                    if (EntityStatusResult != null) {
                        $scope.OverAllEntityStatus = EntityStatusResult.Status;
                        $scope.OverAllEntityInTime = EntityStatusResult.TimeStatus;
                        $scope.DynamicTaskListDescriptionObj = (EntityStatusResult.Comment != null ? EntityStatusResult.Comment : '-');
                        $scope.Ontime = OnSelectionChange(EntityStatusResult.TimeStatus);
                        $scope.ReloadFromTimeout = true;
                        $scope.StopeUpdateStatusonPageLoad = true;
                    }
                }, 100);
            } else {
                $scope.Showstatus = true;
            }
            $scope.OverAllEntityStatusVis = false;
            lockEntityStatus()
        }

        function lockEntityStatus() {
            if ($scope.IsLock == true) {
                $timeout(function () {
                    $('#OverAllEntityStats').select2("enable", false)
                    $('#OntimeStatus').attr('disabled', 'disabled');
                    $("#entitystatuscommentID").attr('readonly', true);
                    $scope.updatecommnet = false;
                    $scope.cancelcommnet = false;
                    50
                });
            }
            else if ($scope.IsLock == false) {
                $timeout(function () {
                    $('#OverAllEntityStats').select2("enable", true)
                    $('#OntimeStatus').attr('disabled', false);
                    $("#entitystatuscommentID").attr('readonly', false);
                    $scope.updatecommnet = true;
                    $scope.cancelcommnet = true;
                    50
                });
            }
        }

        function OnSelectionChange(Selectionid) {
            var Newvalue = $.grep($scope.OntimeStatusLists, function (n, i) {
                return ($scope.OntimeStatusLists[i].Id == Selectionid);
            });
            $scope.DynamicClass = Newvalue[0].SelectedClass;
            return Newvalue[0].Name;
        }
        $scope.TaskListSummerydetal = []
        $scope.GetEntityMembers();
        $scope.LoadNewsFeedBlock();
        $scope.DynamicTaskListDescriptionObj = "-";

        function ClearSummaryDetl() {
            $scope.TaskListSummerydetal = []
            $scope.DynamicTaskListDescriptionObj = "-";
            $scope.Ontime = OnSelectionChange(0);
            $scope.Ontimecomment = '-';
            $scope.TasksInprgress = 0;
            $scope.Unassigned = 0;
            $scope.Overduetasks = 0;
            $scope.Unabletocomplete = 0;
            $scope.TaskListSummerydetal = [];
            FindmaxWidth();
        }
        $scope.DynamicClass = 'btn-success';

        function FindmaxWidth() {
            var maxwidth = $scope.TasksInprgress;
            if (maxwidth < $scope.Unassigned) maxwidth = $scope.Unassigned;
            if (maxwidth < $scope.Overduetasks) maxwidth = $scope.Overduetasks;
            if (maxwidth < $scope.Unabletocomplete) maxwidth = $scope.Unabletocomplete;
            if (maxwidth == 0) {
                $scope.taskinprgwidth = 0;
                $scope.unassignwidth = 0;
                $scope.overduetaskwidth = 0;
                $scope.unabletocompletewidth = 0;
                if (document.getElementById('taskinprg') != undefined) {
                    document.getElementById('taskinprg').style.width = $scope.taskinprgwidth + '%';
                }
                if (document.getElementById('unassign') != undefined) {
                    document.getElementById('unassign').style.width = $scope.unassignwidth + '%';
                }
                if (document.getElementById('overduetask') != undefined) {
                    document.getElementById('overduetask').style.width = $scope.overduetaskwidth + '%';
                }
                if (document.getElementById('unabletocomplete') != undefined) {
                    document.getElementById('unabletocomplete').style.width = $scope.unabletocompletewidth + '%';
                }
            } else {
                $scope.taskinprgwidth = ((($scope.TasksInprgress / maxwidth) * 100) / 100) * 80;
                $scope.unassignwidth = ((($scope.Unassigned / maxwidth) * 100) / 100) * 80;
                $scope.overduetaskwidth = ((($scope.Overduetasks / maxwidth) * 100) / 100) * 80;
                $scope.unabletocompletewidth = ((($scope.Unabletocomplete / maxwidth) * 100) / 100) * 80;
                if (document.getElementById('taskinprg') != undefined) {
                    document.getElementById('taskinprg').style.width = $scope.taskinprgwidth + '%';
                }
                if (document.getElementById('unassign') != undefined) {
                    document.getElementById('unassign').style.width = $scope.unassignwidth + '%';
                }
                if (document.getElementById('overduetask') != undefined) {
                    document.getElementById('overduetask').style.width = $scope.overduetaskwidth + '%';
                }
                if (document.getElementById('unabletocomplete') != undefined) {
                    document.getElementById('unabletocomplete').style.width = $scope.unabletocompletewidth + '%';
                }
            }
        }

        function LoadTaskSummaryDetlEdit(EntityTaskLists) {
            ClearSummaryDetl();
            $scope.TaskListSummerydetal = []
            $scope.DynamicTaskListDescriptionObj = "-";
            var TaskSummaryresult = EntityTaskLists;
            if (TaskSummaryresult.length > 0) {
                $scope.ShowEmptyTaskSummary = false;
                $scope.ShowLoadedTaskSummary = true;
                $scope.TaskListSummerydetal = TaskSummaryresult;
                $scope.OverAllStatus = $scope.TaskListSummerydetal[0].ActiveEntityStateID;
                $scope.Ontimecomment = ($scope.TaskListSummerydetal[0].OnTimeComment != null ? $scope.TaskListSummerydetal[0].OnTimeComment : '-');
                $scope.TasksInprgress = $scope.TaskListSummerydetal[0].TaskInProgress;
                $scope.Unassigned = $scope.TaskListSummerydetal[0].UnAssignedTasks;
                $scope.Overduetasks = $scope.TaskListSummerydetal[0].OverDueTasks;
                $scope.Unabletocomplete = $scope.TaskListSummerydetal[0].UnableToComplete;
                FindmaxWidth();
            } else {
                $scope.ShowEmptyTaskSummary = true;
                $scope.ShowLoadedTaskSummary = false;
            }
        }
        $('#EntityFeedsdiv').on('click', 'a[data-id="taskpopupopen"]', function (event) {
            var entityid = $(this).attr('data-entityid');
            var taskid = $(this).attr('data-taskid');
            var typeid = $(this).attr('data-typeid');
            CcoverviewService.IsActiveEntity(taskid).then(function (result) {
                if (result.Response == true) {
                    $scope.$emit("pingMUITaskEdit", {
                        TaskId: taskid,
                        EntityId: entityid
                    });
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                }
            });
        });
        $('#EntityFeedsdiv').on('click', 'a[data-id="feedpath"]', function (event) {
            event.stopImmediatePropagation();
            var entityid = $(this).attr('data-entityid');
            var typeid = $(this).attr('data-typeid');
            var parentid = $(this).attr('data-parentid');
            CcoverviewService.IsActiveEntity($(this).attr('data-entityid')).then(function (result) {
                if (result.Response == true) {
                    if (typeid == 11 || typeid == 10) $location.path('/mui/planningtool/default/detail/section/' + parentid + '/' + 'objective' + '');
                    else if (typeid == 5) $location.path('/mui/planningtool/default/costcentre/detail/section/' + entityid + '/overview');
                    else $location.path('/mui/planningtool/default/detail/section/' + entityid + '/overview');
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                }
            });
        });
        $('#EntityFeedsdiv').on('click', 'a[data-command="openlink"]', function (event) {
            var TargetControl = $(this);
            var mypage = TargetControl.attr('data-Name');
            var myname = TargetControl.attr('data-Name');
            var w = 1200;
            var h = 800
            var winprops = 'height=' + h + ',width=' + w + ',top=0,left=50,scrollbars=yes,resizable'
            var win = window.open(mypage, myname, winprops)
        });
        $(window).on("ontaskActionnotification", function (event) {
            $("#loadNotificationtask").modal("hide");
        });
        $('#EntityFeedsdiv').on('click', 'a[data-id="openfundrequestpopup"]', function (event) {
            var EntityID = $(this).attr('data-parentid');
            var funderqid = $(this).attr('data-entityid');
            CcoverviewService.IsActiveEntity(funderqid).then(function (result) {
                if (result.Response == true) {
                    $("#feedFundingRequestModal").modal("show");
                    //$('#feedFundingRequestModal').trigger("onNewsfeedCostCentreFundingRequestsAction", [funderqid]);
                    $scope.$emit('onNewsfeedCostCentreFundingRequestsAction', funderqid);
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                }
            });
        });
        $('#EntityFeedsdiv').on('click', 'a[data-id="costcenterlink"]', function (event) {
            event.stopImmediatePropagation();
            var entityid = $(this).attr('data-costcentreid');
            CcoverviewService.IsActiveEntity($(this).attr('data-costcentreid')).then(function (result) {
                if (result.Response == true) {
                    $location.path('/mui/planningtool/costcentre/detail/section/' + entityid + '/overview');
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                }
            });
        });
        $scope.Clearxeditabletreedropdown = function Clearxeditabletreedropdown(attrid, levelcnt, currentlevel) {
            currentlevel = currentlevel + 1;
            for (var i = currentlevel; i <= levelcnt; i++) {
                $scope['dropdown_' + attrid + '_' + i] = "";
            }
        }
        $scope.AddDefaultEndDate = function (stratdateval, enddateval) {
            if ($scope.fields[stratdateval] == null) {
                $scope.fields[enddateval] = null;
            } else {
                var objsetenddate = new Date($scope.fields[stratdateval]);
                $scope.fields[enddateval] = objsetenddate.addDays(7);
            }
        };
                
        $scope.drpdirectiveSource = {};

        $scope.IsSourceformed = function (attrID, levelcnt, attributeLevel, attrType) {
            if (levelcnt > 0) {
                var dropdown_text = '', subid = 0;
                var currntlevel = attributeLevel + 1;
                if (attributeLevel == 1) {
                    var dropdown_text = 'dropdown_text_' + attrID + '_' + attributeLevel;
                    var idtomatch = $scope['dropdown_' + attrID + '_' + attributeLevel] != undefined ? ($scope['dropdown_' + attrID + '_' + attributeLevel].id != undefined ? $scope['dropdown_' + attrID + '_' + attributeLevel].id : $scope['dropdown_' + attrID + '_' + attributeLevel]) : 0;
                    if (idtomatch != 0)
                        $scope.drpdirectiveSource["dropdown_" + attrID + "_" + attributeLevel] = $.grep($scope.treeSources["dropdown_" + attrID].Children,
                            function (e) {
                                return e.id == idtomatch;
                            }
                        )[0];
                }
                for (var j = currntlevel; j <= levelcnt; j++) {
                    dropdown_text = 'dropdown_text_' + attrID + '_' + j;
                    subid = $scope['dropdown_' + attrID + '_' + (j)] != undefined ? ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined ? $scope['dropdown_' + attrID + '_' + (j - 1)].id : $scope['dropdown_' + attrID + '_' + (j)]) : 0;
                    $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = {};
                    if (subid != 0 && subid > 0) {
                        if ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined) {
                            if ($scope['dropdown_' + attrID + '_' + (j - 1)].Children != undefined)
                                $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $scope['dropdown_' + attrID + '_' + (j - 1)].Children;
                            $scope['dropdown_' + attrID + '_' + (j)] = 0;
                        }
                        else {
                            if ($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)].Children != undefined && $scope['dropdown_' + attrID + '_' + (j - 1)] != undefined)
                                $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = ($.grep($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)].Children, function (e) { return e.id == subid; }))[0];
                        }
                    }
                    else {
                        if ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined) {
                            if ($scope['dropdown_' + attrID + '_' + (j - 1)].Children != undefined)
                                $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $scope['dropdown_' + attrID + '_' + (j - 1)].Children;
                            $scope['dropdown_' + attrID + '_' + j] = 0;
                        }
                        else {
                            if ($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)] != undefined) {
                                var res = [];
                                if ($scope['dropdown_' + attrID + '_' + (j)] > 0)
                                    $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $.grep($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)]['Children'], function (e) { return e.id == $scope['dropdown_' + attrID + '_' + (j)] })[0];
                                else
                                    $scope['dropdown_' + attrID + '_' + (j)] = 0;
                            }
                        }
                    }
                }
            }
        }

        $scope.BindChildDropdownSource = function (attrID, levelcnt, attributeLevel, attrType) {

            if (levelcnt > 0) {
                var currntlevel = attributeLevel + 1;
                for (var j = currntlevel; j <= levelcnt; j++) {
                    $scope.DropDownTreeOptionValues["Options" + attrID + "_" + j].splice(0, $scope.DropDownTreeOptionValues["Options" + attrID + "_" + j].length);
                    if (attrType == 6) {
                        $scope["dropdown_" + attrID + "_" + j] = 0;
                        $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)].splice(0, $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)].length);
                    } else if (attrType == 12) {
                        if (j == levelcnt) $scope["multiselectdropdown_" + attrID + "_" + j] = [];
                        else $scope["multiselectdropdown_" + attrID + "_" + j] = "";
                    }
                }
                if (attrType == 6) {
                    if ($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != null && $scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != undefined) {
                        var children = $.grep($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel], function (e) { return e.id == parseInt($scope["dropdown_" + attrID + "_" + attributeLevel]) })[0].Children;

                        if (children != undefined) {
                            var subleveloptions = [];
                            $.each(children, function (i, el) {
                                subleveloptions.push(el);
                            });
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = subleveloptions;
                        }
                        else
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = [];
                    }
                } else if (attrType == 12) {
                    if ($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != null && $scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != undefined) {

                        var sublevel_res = [];
                        if ($scope["multiselectdropdown_" + attrID + "_" + attributeLevel] != undefined && $scope["multiselectdropdown_" + attrID + "_" + attributeLevel] > 0)
                            sublevel_res = $.grep($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel], function (e) { return e.id == parseInt($scope["multiselectdropdown_" + attrID + "_" + attributeLevel]) })[0].Children;
                        if (sublevel_res != undefined) {
                            var subleveloptions = [];
                            $.each(sublevel_res, function (i, el) {
                                subleveloptions.push(el);
                            });
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = subleveloptions;
                        }
                        else
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = [];
                    }
                }
            }
        }

        $scope.isModelUpdated = function (attrID, levelcnt, attributeLevel, attrType, flag) {
            var currntlevel = attributeLevel + 1;
            var dropdown_text = 'dropdown_text_' + attrID + '_' + currntlevel, modelVal = {};
            for (var j = currntlevel; j <= levelcnt; j++) {
                $scope['dropdown_' + attrID + '_' + currntlevel] = 0;
                modelVal = ($.grep($scope["ddtoption_" + attrID + "_" + currntlevel].data, function (e) {
                    return e.Caption.toString() == $scope.treeTexts[dropdown_text].toString().trim()
                }))[0];
                if (modelVal != undefined) {
                    $scope['dropdown_' + attrID + '_' + currntlevel] = modelVal.id;
                }

            }
        }        

        function UpdateTreeScope(ColumnName, Value) {
            for (var k = 0; k < $scope.ListViewDetails[0].data.Response.ColumnDefs.length; k++) {
                if (ColumnName == $scope.ListViewDetails[0].data.Response.ColumnDefs[k].Field) {
                    for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                        for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                            var EntityId = $scope.ListViewDetails[i].data.Response.Data[j]["Id"];
                            if (EntityId == parseInt($stateParams.ID, 10)) {
                                $scope.ListViewDetails[i].data.Response.Data[j][ColumnName] = Value;
                                return false;
                            }
                        }
                    }
                }
            }
        }
        $scope.StatusAttributeData = [];
        $scope.LoadStatusblock = false;
        $timeout(function () { OverviewStatusAttribute(); }, 50);

        function OverviewStatusAttribute() {
            CcoverviewService.FetchEntityStatusTree($stateParams.ID).then(function (GetStatusvalue) {
                $scope.StatusAttributeData = GetStatusvalue.Response;
                $scope.LoadStatusblock = true;
            });
        }
        $scope.changestatusoption = function (attrid, optionid, optioncaption, optionCmt, optionLvl, type, parentnode, optioncolorcode) {
            if (type == "option") {
                if (optionid == $scope.entitystatuscomment["LevelSelectedId_" + parentnode]) return false;
            }
            if (type == "comment") {
                if (optionCmt == $scope.entitystatuscomment["LevelComment_" + parentnode]) return false;
            }
            var metadata = [{
                "AttributeId": attrid,
                "EntityId": $stateParams.ID,
                "Level": optionLvl,
                "NodeId": optionid,
                "comment": optionCmt,
                "type": type,
                "ParentNode": parentnode,
                "optioncaption": optioncaption,
                "optioncolorcode": optioncolorcode
            }];
            UpdateEntityStatusMetadata(metadata);
        }
        $scope.entitystatuscomment = {};
        $scope.setfocusonComment = function (id, comment) {
            $scope.entitystatuscomment["LevelComment_" + id] = comment;
            $timeout(function () {
                $("#statuscomment_" + id + "").focus().select()
            }, 10);
        };
        $scope.getSelectedOptionvalue = function (levelid, selectvalue, selectid, colorcode) {
            $scope.entitystatuscomment["LevelSelectedCaption_" + levelid] = selectvalue;
            $scope.entitystatuscomment["LevelSelectedId_" + levelid] = selectid;
            $scope.entitystatuscomment["LevelSelectedColorCode_" + levelid] = colorcode;
        }
        $scope.ClearEntityComment = function (attrid, id, option) {
            refreshStatus(attrid, id, option);
        }

        function refreshStatus(attrid, id, option) {
            var updatecopedata = $.grep($scope.StatusAttributeData, function (rel) {
                return rel.attributeid == attrid && rel.levelid == id;
            })[0];
            if (updatecopedata != null) {
                if (option == "comment") updatecopedata.optioncomment = $scope.entitystatuscomment["LevelComment_" + id];
                else {
                    updatecopedata.selectid = $scope.entitystatuscomment["LevelSelectedId_" + id];
                    updatecopedata.selectvalue = $scope.entitystatuscomment["LevelSelectedCaption_" + id];
                    updatecopedata.optioncolorcode = $scope.entitystatuscomment["LevelSelectedColorCode_" + id];
                }
            }
        }

        function UpdateEntityStatusMetadata(metadata) {
            var upd = {};
            upd.EntityID = $stateParams.ID;
            upd.metadata = metadata;
            CcoverviewService.updateOverviewStatus(upd).then(function (Result) {
                if (Result.StatusCode == 200) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                    $scope.entitystatuscomment["LevelSelectedCaption_" + metadata[0]["ParentNode"]] = metadata[0]["optioncaption"];
                    $scope.entitystatuscomment["LevelSelectedId_" + metadata[0]["ParentNode"]] = metadata[0]["NodeId"];
                    $scope.entitystatuscomment["LevelSelectedColorCode_" + metadata[0]["ParentNode"]] = metadata[0]["optioncolorcode"];
                    $scope.entitystatuscomment["LevelComment_" + metadata[0]["ParentNode"]] = metadata[0]["comment"] != "" ? metadata[0]["comment"] : "-";
                    refreshStatus(metadata[0]["AttributeId"], metadata[0]["ParentNode"], metadata[0]["type"]);
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4351.Caption'));
                    refreshStatus(metadata[0]["AttributeId"], metadata[0]["ParentNode"], metadata[0]["type"]);
                }
            });
        }
        $scope.UpdateEntityOverAllStatus = function (OverAllEntityStatus) {
            if (OverAllEntityStatus != "" && OverAllEntityStatus != null && OverAllEntityStatus != undefined) {
                var upd = {};
                upd.EntityID = parseInt($stateParams.ID);
                upd.EntityStatusID = $scope.OverAllEntityStatus;
                upd.OnTimeStatus = $scope.OverAllEntityInTime;
                upd.OnTimeComment = $scope.DynamicTaskListDescriptionObj;
                CcoverviewService.UpdateEntityStatus(upd).then(function (Result) {
                    if (Result.StatusCode == 200) {
                        NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                        for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                            for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                                var EntityId = $scope.ListViewDetails[i].data.Response.Data[j]["Id"];
                                if (EntityId == parseInt($stateParams.ID, 10)) {
                                    if ($scope.ListViewDetails[i].data.Response.Data[j][SystemDefiendAttributes.EntityOnTimeStatus] != undefined) {
                                        $scope.ListViewDetails[i].data.Response.Data[j][SystemDefiendAttributes.EntityOnTimeStatus] = $("#OntimeStatus").text().trim();
                                    } else {
                                        $scope.ListViewDetails[i].data.Response.Data[j]["EntityOnTimeStatus"] = $("#OntimeStatus").text().trim();
                                    }
                                    if ($scope.ListViewDetails[i].data.Response.Data[j][SystemDefiendAttributes.EntityStatus] != undefined) $scope.ListViewDetails[i].data.Response.Data[j][SystemDefiendAttributes.EntityStatus] = $('#OverAllEntityStats option:selected').text();
                                    else $scope.ListViewDetails[i].data.Response.Data[j]["Status"] = $('#OverAllEntityStats option:selected').text();
                                    $timeout(function () {
                                        $scope.TimerForLatestFeed();
                                    }, 2000);
                                    return false;
                                }
                            }
                        }
                    }
                });
            }
        }
        $scope.UploadAttributeId = 0;
        $scope.UploadImagefileCC = function (attributeId) {
            $scope.UploadAttributeId = attributeId;
            $("#pickfilesUploaderAttrCC").click();
        }
        $timeout(function () {
            StrartUpload_UploaderAttrCC();
        }, 100);

        function StrartUpload_UploaderAttrCC() {
            $('.moxie-shim').remove();
            var uploader_Attr = new plupload.Uploader({
                runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                browse_button: 'pickfilesUploaderAttrCC',
                container: 'filescontainerfrCC',
                max_file_size: '10000mb',
                flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                url: 'Handlers/UploadHandler.ashx?Type=Attachment',
                chunk_size: '64Kb',
                multi_selection: false,
                multipart_params: {}
            });
            uploader_Attr.bind('Init', function (up, params) { });
            uploader_Attr.init();
            uploader_Attr.bind('FilesAdded', function (up, files) {
                up.refresh();
                uploader_Attr.start();
            });
            uploader_Attr.bind('UploadProgress', function (up, file) { });
            uploader_Attr.bind('Error', function (up, err) {
                bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                up.refresh();
            });
            uploader_Attr.bind('FileUploaded', function (up, file, response) {
                SaveFileDetails(file, response.response);
            });
            uploader_Attr.bind('BeforeUpload', function (up, file) {
                $.extend(up.settings.multipart_params, {
                    id: file.id,
                    size: file.size
                });
            });
        }

        function SaveFileDetails(file, response) {
            var extension = file.name.substring(file.name.lastIndexOf("."))
            var resultArr = response.split(",");
            var uplaodImageObject = {};
            uplaodImageObject.Name = file.name;
            uplaodImageObject.VersionNo = 1;
            uplaodImageObject.MimeType = resultArr[1];
            uplaodImageObject.Extension = extension;
            uplaodImageObject.OwnerID = $cookies['UserId'];
            uplaodImageObject.CreatedOn = Date.now();
            uplaodImageObject.Checksum = "";
            uplaodImageObject.ModuleID = 1;
            uplaodImageObject.EntityID = parseInt($stateParams.ID, 10);
            uplaodImageObject.AttributeID = $scope.UploadAttributeId;
            uplaodImageObject.Size = file.size;
            uplaodImageObject.FileName = resultArr[0] + extension;
            var PreviewID = "UploaderPreview_" + $scope.UploadAttributeId;
            CcoverviewService.UpdateImageName(uplaodImageObject).then(function (uplaodImageObjectResult) {
                if (uplaodImageObjectResult.Response != 0) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4808.Caption'));
                    $('#' + PreviewID).attr('src', 'UploadedImages/' + uplaodImageObject.FileName);
                }
            });
        }
        $scope.ChangeCostCentreCurrencyType = function () {
            $scope.SelectedCostCentreCurrency.Name = $.grep($scope.CurrencyFormatsList, function (e) {
                return e.Id == $scope.SelectedCostCentreCurrency.TypeId;
            })[0].Name;
            $scope.SelectedCostCentreCurrency.ShortName = $.grep($scope.CurrencyFormatsList, function (e) {
                return e.Id == $scope.SelectedCostCentreCurrency.TypeId;
            })[0].ShortName;
            CcoverviewService.GetCostCentreCurrencyRateById(0, $scope.SelectedCostCentreCurrency.TypeId, false).then(function (resCurrencyRate) {
                if (resCurrencyRate.Response != null) {
                    $scope.SelectedCostCentreCurrency.Rate = parseFloat(resCurrencyRate.Response[1]);
                    $scope.LoadFinancialSummanyBlock($stateParams.ID);
                }
            });
        }
        $scope.changeduedate_changed = function (duedate, ID) {
            if (duedate != null) {
                var test = isValidDate(duedate.toString(), $scope.format.toString());
                if (test) {
                    var a = $.grep($scope.tempholidays, function (e) {
                        return e == dateFormat(duedate, $scope.format);
                    });
                    if (a != null) {
                        if (a.length > 0) {
                            bootbox.alert($translate.instant('LanguageContents.Res_5747.Caption'));
                            $scope.fields["DatePart_" + ID] = "";
                        }
                    }
                } else {
                    $scope.fields["DatePart_" + ID] = "";
                    bootbox.alert($translate.instant('LanguageContents.Res_5748.Caption'));
                }
            }
        }
        $scope.changeperioddate_changed = function (date, datetype) {
            if (date != null) {
                var test = isValidDate(date.toString(), $scope.format.toString());
                if (test) {
                    var a = $.grep($scope.tempholidays, function (e) {
                        return e == dateFormat(date, $scope.format);
                    });
                    if (a != null) {
                        if (a.length > 0) {
                            bootbox.alert($translate.instant('LanguageContents.Res_5747.Caption'));
                            if (datetype == "StartDate") $scope.items[0].startDate = "";
                            else $scope.item[0].endDate = "";
                        }
                    }
                } else {
                    if (datetype == "StartDate") $scope.items[0].startDate = "";
                    else $scope.item[0].endDate = "";
                    bootbox.alert($translate.instant('LanguageContents.Res_5748.Caption'));
                }
            }
        }

        function isValidDate(dateval, dateformat) {
            var formatlen;
            var defaultdateVal = [];
            defaultdateVal = dateval.length;
            formatlen = dateformat.length;
            if (formatlen == defaultdateVal || defaultdateVal > formatlen) return true;
            else return false;
        };

        $scope.settxtColor = function (hexcolor) {
            var r = parseInt(hexcolor.substr(0, 2), 16);
            var g = parseInt(hexcolor.substr(2, 2), 16);
            var b = parseInt(hexcolor.substr(4, 2), 16);
            var yiq = ((r * 299) + (g * 587) + (b * 114)) / 1000;
            return (yiq >= 160) ? 'black' : 'white';
        }

        $scope.overallstatusformat = function (data) {
            var fontColor = 'blue';
            var txtcolor = 'white';
            $("#select2-drop").addClass("bgDropdown");
            if (data.id != "" || data.text != "") {
                for (var i = 0; i < $scope.EntityStatusResult.length; i++) {
                    var res = $scope.EntityStatusResult[i];
                    if (res.ID == data.id) {
                        var stausoption = res.StatusOptions;
                        var colorcode = res.ColorCode;
                        if (colorcode == null || colorcode.trim() == "")
                            txtcolor = $scope.settxtColor("ffffff");
                        else
                            txtcolor = $scope.settxtColor(colorcode);
                        break;
                    }

                }
            }
            else {
                return "<div class='select2-chosen-label' style='background-color:#" + fontColor + "; color:" + txtcolor + "'>" + data.text + "</div>";
            }

            return "<div class='select2-chosen-label'style='background-color:#" + colorcode + "; color:" + txtcolor + "'>" + stausoption + "</div>";
        };

        $scope.select2Config = {
            formatResult: $scope.overallstatusformat,
            formatSelection: $scope.overallstatusformat
        };

        $scope.$on("$destroy", function () {
            $timeout.cancel(NewsFeedUniqueTimer);
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.costcentre.detail.overviewCtrl']"));
        });
    }
    app.controller("mui.planningtool.costcentre.detail.overviewCtrl", ['$scope', '$timeout', '$http', '$compile', '$resource', '$cookies', '$stateParams', '$location', '$window', '$translate', 'CcoverviewService', '$modal', muiplanningtoolcostcentredetailoverviewCtrl]);
})(angular, app);