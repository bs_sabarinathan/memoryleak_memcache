///#source 1 1 /app/controllers/mui/planningtool/costcentre/detail-controller.js
(function (ng, app) {
    "use strict";

    function muiplanningtoolcostcentredetailCtrl($scope, $timeout, $http, $compile, $resource, $stateParams, $window, $location, $templateCache, $cookies, $translate, CcdetailService, $modal) {

        var cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName).replace(/\\/g, "\/");
        if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {

            $scope.reportPreviewImg = cloudpath + "/Files/ReportFiles/Images/Preview/";
        }
        else {
            $scope.reportPreviewImg = "/Files/ReportFiles/Images/Preview/";
        }

        var FilterID = 0;
        var timer;
        var model;
        $scope.Calanderopen = function ($event, model1) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calanderopened = true;
            $scope.Ganttenddate1 = false;
            model = model1;
        };
        $scope.dynCalanderopen = function ($event, model) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calanderopened = false;
            $scope.Ganttenddate1 = true;
        };
        $scope.Newsfilter = {
            FeedgroupMulitipleFilterStatus: [],
            Feedgrouplistvalues: '-1'
        };
        var CollectionIds = "";
        if ($.cookie('ListofEntityID') == undefined || $.cookie('ListofEntityID') == null || $.cookie('ListofEntityID') == "") {
            $.cookie('ListofEntityID', GetGuidValue($stateParams.TrackID));
        }
        CollectionIds = $.cookie('ListofEntityID').split(",");
        $scope.CurrentTrackingUrl = $stateParams.TrackID;
        $window.ListofEntityID = CollectionIds;
        $("#plandetailfilterdiv").css('display', 'block')
        $scope.CurrentCostcentreID = 0;
        $scope.Level = "0";
        $scope.ExpandingEntityID = '0';
        $scope.costcentreidforreport = '0';
        $scope.FilterType = 2;
        $scope.CostCentreEntityType = "";
        $scope.CurrentTreeLevel = 0;
        $scope.SelectedCostCentreCurrency = {
            TypeId: 0,
            Name: '',
            ShortName: '',
            Rate: 1
        };
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.costcentre.detailCtrl']"));
        });
        $("#costcentredetailfilter").on('loadfiltersettings', function (event, TypeID) {
            $("#costcentredetailfiltersettings").trigger('loaddetailfiltersettings', 5);
        });
        $("#costcentredetailfilter").on('applyplandetailfilter', function (event, filterid, filtername, IsDetailFilter) {
            $("#costcentredetailfiltersettings").trigger('applyplandetailfilter', [filterid, filtername, IsDetailFilter]);
        });
        $("#costcentredetailfilter").on('EditFilterSettingsByFilterID', function (event, filterid, typeid, filtertypeval) {
            $("#costcentredetailfiltersettings").trigger('EditFilterSettingsByFilterID', [filterid, typeid, filtertypeval]);
        });
        $("#costcentredetailfilter").on('ClearScope', function (event, filterid) {
            $("#costcentredetailfiltersettings").trigger('ClearScope', [filterid]);
        });
        $("#costcentredetailfilter").on('ClearAndReApply', function (event, filterid) {
            $scope.appliedfilter = $translate.instant('LanguageContents.Res_401.Caption');
            $scope.SelectedFilterID = 0;
            $scope.SelectedFilterType = 2;
            $scope.AddEditFilter = $translate.instant('LanguageContents.Res_402.Caption');
        });
        $scope.ListViewDetails = [{
            data: "",
            PageIndex: 0,
            UniqueID: "",
            StartRowNo: 0,
            MaxNoofRow: 30,
            FilterID: 0,
            SortOrderColumn: "null",
            IsDesc: false,
            IncludeChildren: false,
            EntityID: '0',
            Level: '0',
            IDArr: [],
            FilterAttributes: []
        }];
        $scope.ListTemplate = "";
        $scope.GanttViewDataPageTemplate = '';
        $scope.GanttViewBlockPageTemplate = '';
        $('#TreeCtrlCss').remove();
        var GlobalChildNode = new Array();
        GlobalChildNode = $window.ListofEntityID;
        $scope.EntityHighlight = false;
        if ($stateParams.ID != undefined && $stateParams.ID != 0) {
            GetCostCentreCurrencyRateById(parseInt($stateParams.ID), 0);
            $scope.ExpandingEntityID = $stateParams.ID;
            $scope.Level = "100";
            $scope.EntityHighlight = true;
        } else {
            GlobalChildNode = $window.ListofEntityID;
            $scope.Level = $window.TreeLevel == undefined ? "0" : $window.TreeLevel;
            if ($window.ListofEntityID.length == 1 && $window.TotalChildrenForSelecedCC == 0) {
                GetCostCentreCurrencyRateById($window.ListofEntityID[0], 0);
            } else {
                $scope.SelectedCostCentreCurrency.Rate = 1;
                $scope.SelectedCostCentreCurrency.ShortName = $scope.DefaultSettings.CurrencyFormat.Name;
                $scope.SelectedCostCentreCurrency.Name = $scope.DefaultSettings.CurrencyFormat.ShortName;
                $scope.SelectedCostCentreCurrency.TypeId = $scope.DefaultSettings.CurrencyFormat.Id;
                CcdetailService.GetCurrencyListFFsettings().then(function (CurrencyListResult) {
                    $scope.CurrencyFormatsList = CurrencyListResult.Response;
                });
            }
            GetTreeCount();
        }
        var FilterObj = [];
        $("#EntitiesTree").on("loadactivityfromfilterforCostCenter", function (event, filterid, ApplyFilterobj, filtertype) {
            if (filtertype == undefined) $scope.FilterType = 2;
            $scope.FilterType = filtertype;
            $scope.ListViewDetails = [{
                data: "",
                PageIndex: 0,
                UniqueID: "",
                StartRowNo: 0,
                MaxNoofRow: 30,
                FilterID: 0,
                SortOrderColumn: "null",
                IsDesc: false,
                IncludeChildren: false,
                EntityID: '0',
                Level: '0',
                IDArr: [],
                FilterAttributes: []
            }];
            $scope.ListTemplate = "";
            $scope.GanttViewDataPageTemplate = '';
            $scope.GanttViewBlockPageTemplate = '';
            if (filterid != undefined) {
                FilterID = filterid;
            } else {
                FilterID = 0;
            }
            FilterObj = ApplyFilterobj;
            if ($stateParams.ID != undefined && $stateParams.ID != 0) $scope.ExpandingEntityID = $stateParams.ID;
            else GlobalChildNode = $window.ListofEntityID;
            $('#TreeCtrlCss').remove();
            GetTreeCount();
        });
        $scope.loadcostcentrefromsearchByID = function (id, level) {
            $scope.Activity.IsActivitySectionLoad = false;
            $scope.Level = level;
            FilterID = 0;
            FilterObj = [];
            $scope.ListViewDetails = [{
                data: "",
                PageIndex: 0,
                UniqueID: "",
                StartRowNo: 0,
                MaxNoofRow: 30,
                FilterID: 0,
                SortOrderColumn: "null",
                IsDesc: false,
                IncludeChildren: false,
                EntityID: '0',
                Level: '0',
                IDArr: [],
                FilterAttributes: []
            }];
            $scope.ListTemplate = "";
            $scope.GanttViewDataPageTemplate = '';
            $scope.GanttViewBlockPageTemplate = '';
            GlobalChildNode = new Array();
            GlobalChildNode.push(id)
            $scope.ExpandingEntityID = '0';
            $scope.EntityHighlight = true
            $('#TreeCtrlCss').remove();
            GetTreeCount();
        }

        function GetTreeCount() {
            GetCostCenterEntityType();
            if ($scope.FilterType == undefined) $scope.FilterType = 2;
            $('#EntitiesTree').scrollTop(0);
            $scope.TreeClassList = [];
            var Node = {};
            Node.StartRowNo = 0;
            Node.MaxNoofRow = 20;
            Node.FilterID = FilterID;
            Node.SortOrderColumn = "null";
            Node.IsDesc = false;
            Node.IncludeChildren = true;
            Node.EntityID = '0';
            Node.Level = $scope.Level;
            Node.ExpandingEntityID = $scope.ExpandingEntityID;
            Node.FilterType = $scope.FilterType;
            Node.IDArr = GlobalChildNode;
            Node.FilterAttributes = FilterObj;
            $scope.NodeForward = Node;
            CcdetailService.CostCentreDetail(Node).then(function (NodeCnt) {
                if (NodeCnt.Response != null && NodeCnt.Response.Data != null) {
                    fnPageTemplate(parseInt(NodeCnt.Response.DataCount));
                    if (NodeCnt.Response.Data != null) {
                        $scope.Tempattrgroupfilter = [];
                        var TreeItem = "";
                        var CostCentreID = 0;
                        var Constructunique = '';
                        if ($scope.FilterType == 6) {
                            var optID = "";
                            var totcnt = 0;
                            for (var i = 0; i < NodeCnt.Response.Data.length; i++) {
                                if (optID != NodeCnt.Response.Data[i].OptionCaption) {
                                    totcnt = $.grep(NodeCnt.Response.Data, function (e) {
                                        return e.OptionID == NodeCnt.Response.Data[i].OptionID
                                    });
                                    $scope.Tempattrgroupfilter.push({
                                        1: "",
                                        ApprovedAllocatedAmount: 0,
                                        ApprovedBudget: 0,
                                        ApprovedSubAllocatedAmount: 0,
                                        Available: 0,
                                        BudgetDeviation: 0,
                                        ColorCode: "000000",
                                        Commited: 0,
                                        CostCenterID: 0,
                                        EntityID: 0,
                                        EntityStateID: 0,
                                        Id: 0,
                                        InRequest: 0,
                                        IsLock: true,
                                        Level: -1,
                                        MileStone: "<root/>",
                                        Name: NodeCnt.Response.Data[i]["OptionCaption"],
                                        ParentID: 0,
                                        Period: "<root/>",
                                        Permission: 0,
                                        PlannedAmount: 0,
                                        ShortDescription: "G",
                                        Spent: 0,
                                        Status: "",
                                        TempPeriod: "-",
                                        TotalChildrenCount: totcnt.length + 1,
                                        TypeID: 0,
                                        TypeName: "@Option",
                                        UniqueKey: "0" + i,
                                        class: "1" + "0" + i
                                    });
                                }
                                $scope.Tempattrgroupfilter.push({
                                    1: NodeCnt.Response.Data[i]["1"],
                                    ApprovedAllocatedAmount: parseInt(NodeCnt.Response.Data[i]["ApprovedAllocatedAmount"], 10),
                                    ApprovedBudget: parseInt(NodeCnt.Response.Data[i]["ApprovedBudget"], 10),
                                    ApprovedSubAllocatedAmount: parseInt(NodeCnt.Response.Data[i]["ApprovedSubAllocatedAmount"], 10),
                                    Available: parseInt(NodeCnt.Response.Data[i]["Available"], 10),
                                    BudgetDeviation: parseInt(NodeCnt.Response.Data[i]["BudgetDeviation"], 10),
                                    ColorCode: NodeCnt.Response.Data[i]["ColorCode"],
                                    Commited: parseInt(NodeCnt.Response.Data[i]["Commited"], 10),
                                    CostCenterID: parseInt(NodeCnt.Response.Data[i]["CostCenterID"], 10),
                                    EntityID: parseInt(NodeCnt.Response.Data[i]["EntityID"], 10),
                                    EntityStateID: parseInt(NodeCnt.Response.Data[i]["EntityStateID"], 10),
                                    Id: parseInt(NodeCnt.Response.Data[i]["InRequest"], 10),
                                    InRequest: parseInt(NodeCnt.Response.Data[i]["1"], 10),
                                    IsLock: NodeCnt.Response.Data[i]["IsLock"],
                                    Level: parseInt(NodeCnt.Response.Data[i]["Level"], 10),
                                    MileStone: NodeCnt.Response.Data[i]["MileStone"],
                                    Name: NodeCnt.Response.Data[i]["Name"],
                                    ParentID: parseInt(NodeCnt.Response.Data[i]["ParentID"], 10),
                                    Period: NodeCnt.Response.Data[i]["Period"],
                                    Permission: NodeCnt.Response.Data[i]["Permission"],
                                    PlannedAmount: parseInt(NodeCnt.Response.Data[i]["PlannedAmount"], 10),
                                    ShortDescription: NodeCnt.Response.Data[i]["ShortDescription"],
                                    Spent: parseInt(NodeCnt.Response.Data[i]["Spent"], 10),
                                    Status: NodeCnt.Response.Data[i]["Status"],
                                    TempPeriod: NodeCnt.Response.Data[i]["TempPeriod"],
                                    TotalChildrenCount: NodeCnt.Response.Data[i]["TotalChildrenCount"],
                                    TypeID: NodeCnt.Response.Data[i]["TypeID"],
                                    TypeName: NodeCnt.Response.Data[i]["TypeName"],
                                    UniqueKey: NodeCnt.Response.Data[i]["UniqueKey"],
                                    class: NodeCnt.Response.Data[i]["class"]
                                });
                                optID = NodeCnt.Response.Data[i].OptionCaption;
                            }
                            NodeCnt.Response.Data = $scope.Tempattrgroupfilter;
                            if ($scope.ListViewDetails[0].data == "") {
                                $scope.ListViewDetails[0].data = NodeCnt;
                                $scope.ListViewDetails[0].PageIndex = 0;
                                $scope.ListViewDetails[0].UniqueID = "";
                                $scope.ListViewDetails[0].StartRowNo = 0;
                                $scope.ListViewDetails[0].MaxNoofRow = 30;
                                $scope.ListViewDetails[0].FilterID = FilterID;
                                $scope.ListViewDetails[0].SortOrderColumn = "null";
                                $scope.ListViewDetails[0].IsDesc = false;
                                $scope.ListViewDetails[0].IncludeChildren = true;
                                $scope.ListViewDetails[0].EntityID = '0';
                                $scope.ListViewDetails[0].Level = $scope.Level;
                                $scope.ListViewDetails[0].IDArr = GlobalChildNode;
                                $scope.ListViewDetails[0].FilterAttributes = FilterObj;
                            } else {
                                $scope.ListViewDetails.push({
                                    data: NodeCnt,
                                    PageIndex: 0,
                                    UniqueID: "",
                                    StartRowNo: 0,
                                    MaxNoofRow: 30,
                                    FilterID: FilterID,
                                    SortOrderColumn: "null",
                                    IsDesc: false,
                                    IncludeChildren: true,
                                    EntityID: '0',
                                    Level: $scope.Level,
                                    IDArr: GlobalChildNode,
                                    FilterAttributes: FilterObj
                                });
                            }
                            $scope.ParenttreeList = $scope.Tempattrgroupfilter;
                            var rootID = 0;
                            for (var i = 0; i < $scope.ParenttreeList.length; i++) {
                                var UniqueId = '';
                                var IscostCenter = false;
                                var ExtraSpace = "<i class='icon-'></i>";
                                CostCentreID = $scope.ParenttreeList[i]["CostCenterID"];
                                UniqueId = UniqueKEY($scope.ParenttreeList[i]["class"]);
                                var GeneralUniquekey = UniqueKEY($scope.ParenttreeList[i]["class"].toString());
                                var ClassName = GenateClass(GeneralUniquekey);
                                ClassName += " mo" + UniqueId;
                                if ($scope.ParenttreeList[i]["TypeID"] == 5) {
                                    IscostCenter = true;
                                    ExtraSpace = "";
                                }
                                TreeItem += "<li data-over='true' data-CostCentreID=" + CostCentreID + "  class='" + ClassName + "'  data-uniqueKey='" + UniqueId + "'><a data-EntityLevel='" + $scope.ParenttreeList[i]["Level"] + "' data-role='Activity' data-iscostcentre='" + IscostCenter + "' data-EntityTypeID='" + $scope.ParenttreeList[i]["TypeID"] + "'  data-entityID=" + $scope.ParenttreeList[i]["Id"] + "  data-colorcode=" + $scope.ParenttreeList[i]["ColorCode"] + " data-shorttext=" + $scope.ParenttreeList[i]["ShortDescription"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-entityname=\"" + $scope.ParenttreeList[i]["Name"] + "\"> ";
                                if ($scope.ParenttreeList[i]["TypeName"] != "@Option") TreeItem += "<i class='icon-'></i>";
                                TreeItem += ExtraSpace + Space($scope.ParenttreeList[i]["UniqueKey"], 0);
                                if ($scope.ParenttreeList[i]["ParentID"] == 0) {
                                    var j = i + 1;
                                    if (j < $scope.ParenttreeList.length && $scope.ParenttreeList[j]["ParentID"] != 0) $scope.ParenttreeList[i]["TotalChildrenCount"] = 1;
                                }
                                if ($scope.ParenttreeList[i]["TotalChildrenCount"] > 0) {
                                    if (UniqueId.split("-").length <= $scope.Level || $scope.ParenttreeList[i]["ParentID"] == 0 || parseInt($scope.Level) == 100 || (i == 0)) {
                                        TreeItem += "<i data-role='Arrow' data-IsExpanded='true' data-CostCentreID=" + CostCentreID + " data-IsConnected='true' data-icon='" + UniqueId + "' class='icon-caret-down' data-isExpanded=true  data-isChildrenPresent=false data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeList[i]["Id"] + " data-TotalChildrenCount=" + $scope.ParenttreeList[i]["TotalChildrenCount"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-UniqueId=\"" + UniqueId + "\"></i>";
                                    } else {
                                        TreeItem += "<i data-role='Arrow' data-IsExpanded='false' data-CostCentreID=" + CostCentreID + " data-IsConnected='false' data-icon='" + UniqueId + "' class='icon-caret-right' data-isExpanded=false data-isChildrenPresent=true data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeList[i]["Id"] + " data-TotalChildrenCount=" + $scope.ParenttreeList[i]["TotalChildrenCount"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-UniqueId=\"" + UniqueId + "\"></i>";
                                    }
                                } else {
                                    TreeItem += "<i class='icon-'></i>";
                                }
                                TreeItem += " <span style='background-color: #" + $scope.ParenttreeList[i]["ColorCode"].trim() + ";'  class='eicon-s margin-right5x'>" + $scope.ParenttreeList[i]["ShortDescription"] + "</span>";
                                TreeItem += "<span  data-EntityLevel='" + $scope.ParenttreeList[i]["Level"] + "' data-name='text' data-CostCentreIDForReport=" + CostCentreID + " >" + $scope.ParenttreeList[i]["Name"] + "</span>";
                                if (parseInt(TreeVal[i]["TypeID"]) == 5) TreeItem += "<i data-role='Overview' class='icon-reorder menu' data-toggle='modal' data-AccessValue=" + TreeVal[i].Permission + " data-typeID=" + TreeVal[i]["TypeID"] + " data-entityID=" + TreeVal[i]["Id"] + "></i>";
                                TreeItem += "</a> </li>";
                            }
                        } else {
                            if ($scope.ListViewDetails[0].data == "") {
                                $scope.ListViewDetails[0].data = NodeCnt;
                                $scope.ListViewDetails[0].PageIndex = 0;
                                $scope.ListViewDetails[0].UniqueID = "";
                                $scope.ListViewDetails[0].StartRowNo = 0;
                                $scope.ListViewDetails[0].MaxNoofRow = 30;
                                $scope.ListViewDetails[0].FilterID = FilterID;
                                $scope.ListViewDetails[0].SortOrderColumn = "null";
                                $scope.ListViewDetails[0].IsDesc = false;
                                $scope.ListViewDetails[0].IncludeChildren = true;
                                $scope.ListViewDetails[0].EntityID = '0';
                                $scope.ListViewDetails[0].Level = $scope.Level;
                                $scope.ListViewDetails[0].IDArr = GlobalChildNode;
                                $scope.ListViewDetails[0].FilterAttributes = FilterObj;
                            } else {
                                $scope.ListViewDetails.push({
                                    data: NodeCnt,
                                    PageIndex: 0,
                                    UniqueID: "",
                                    StartRowNo: 0,
                                    MaxNoofRow: 30,
                                    FilterID: FilterID,
                                    SortOrderColumn: "null",
                                    IsDesc: false,
                                    IncludeChildren: true,
                                    EntityID: '0',
                                    Level: $scope.Level,
                                    IDArr: GlobalChildNode,
                                    FilterAttributes: FilterObj
                                });
                            }
                            $scope.LoadCostCentreGantHeader($scope.ListViewDetails[0].IDArr);
                            $scope.ParenttreeList = NodeCnt.Response.Data;
                            var rootID = 0;
                            for (var i = 0; i < $scope.ParenttreeList.length; i++) {
                                var UniqueId = '';
                                var IscostCenter = false;
                                var ExtraSpace = "<i class='icon-'></i>";
                                CostCentreID = $scope.ParenttreeList[i]["CostCenterID"];
                                if (rootID == 0) {
                                    rootID = parseInt($scope.ParenttreeList[i]["class"]);
                                    $scope.CurrentTreeLevel = parseInt($scope.ParenttreeList[i]["Level"]);
                                } else if (parseInt($scope.ParenttreeList[i]["class"].split(".")[0]) != rootID) {
                                    rootID = parseInt($scope.ParenttreeList[i]["class"]);
                                    $scope.CurrentTreeLevel = parseInt($scope.ParenttreeList[i]["Level"]);
                                }
                                if ($scope.ParenttreeList[i]["TypeID"] == 5) {
                                    ExtraSpace = "";
                                    IscostCenter = true;
                                }
                                UniqueId = UniqueKEY($scope.ParenttreeList[i]["class"]);
                                var GeneralUniquekey = UniqueKEY($scope.ParenttreeList[i]["class"].toString());
                                var ClassName = GenateClass(GeneralUniquekey);
                                ClassName += " mo" + UniqueId;
                                TreeItem += "<li data-over='true' data-CostCentreID=" + CostCentreID + "  class='" + ClassName + "'  data-uniqueKey='" + UniqueId.toString() + "'><a data-EntityLevel='" + $scope.ParenttreeList[i]["Level"] + "' data-role='Activity' data-iscostcentre='" + IscostCenter + "' data-EntityTypeID='" + $scope.ParenttreeList[i]["TypeID"] + "'  data-entityID=" + $scope.ParenttreeList[i]["Id"] + "  data-colorcode=" + $scope.ParenttreeList[i]["ColorCode"] + " data-shorttext=" + $scope.ParenttreeList[i]["ShortDescription"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-entityname=\"" + $scope.ParenttreeList[i]["Name"] + "\"> ";
                                TreeItem += ExtraSpace + Space($scope.ParenttreeList[i]["class"], $scope.CurrentTreeLevel);
                                if ($scope.ParenttreeList[i]["ParentID"] == 0) {
                                    var j = i + 1;
                                    if (j < $scope.ParenttreeList.length && $scope.ParenttreeList[j]["ParentID"] != 0) $scope.ParenttreeList[i]["TotalChildrenCount"] = 1;
                                }
                                if ($scope.ParenttreeList[i]["TotalChildrenCount"] > 0) {
                                    if ($scope.ParenttreeList[i]["TypeID"] == 5 && $scope.ParenttreeList[i]["ParentID"] == 0 || parseInt($scope.Level) == 100 || i == 0) {
                                        TreeItem += "<i data-role='Arrow' data-IsExpanded='true' data-CostCentreID=" + CostCentreID + " data-IsConnected='true' data-icon='" + UniqueId + "' class='icon-caret-down' data-isExpanded=true  data-isChildrenPresent=false data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeList[i]["Id"] + " data-TotalChildrenCount=" + $scope.ParenttreeList[i]["TotalChildrenCount"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-UniqueId=\"" + UniqueId + "\"></i>";
                                    } else {
                                        TreeItem += "<i data-role='Arrow' data-IsExpanded='false' data-CostCentreID=" + CostCentreID + " data-IsConnected='false' data-icon='" + UniqueId + "' class='icon-caret-right' data-isExpanded=false data-isChildrenPresent=true data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeList[i]["Id"] + " data-TotalChildrenCount=" + $scope.ParenttreeList[i]["TotalChildrenCount"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-UniqueId=\"" + UniqueId + "\"></i>";
                                    }
                                } else {
                                    TreeItem += "<i class='icon-'></i>";
                                }
                                TreeItem += " <span data-uniqueKey='" + UniqueId + "' style='background-color: #" + $scope.ParenttreeList[i]["ColorCode"].trim() + ";'  class='eicon-s margin-right5x'>" + $scope.ParenttreeList[i]["ShortDescription"] + "</span>";
                                TreeItem += "<span data-name='text' data-EntityLevel='" + $scope.ParenttreeList[i]["Level"] + "' data-CostCentreIDForReport=" + CostCentreID + " >" + $scope.ParenttreeList[i]["Name"] + "</span>";
                                if (parseInt($scope.ParenttreeList[i]["TypeID"]) == 5) TreeItem += "<i data-role='Overview' class='icon-reorder menu' data-toggle='modal' data-AccessValue=" + $scope.ParenttreeList[i].Permission + " data-typeID=" + $scope.ParenttreeList[i]["TypeID"] + " data-entityID=" + $scope.ParenttreeList[i]["Id"] + "></i>";
                                TreeItem += "</a> </li>";
                            }
                        }
                        var LoadedPage = 0;
                        $scope.$broadcast('onTreePageCreation', [NodeCnt, 0, ""]);
                        $('#EntitiesTree li[data-page=' + LoadedPage + '] > ul').attr("data-items", i);
                        $('#EntitiesTree li[data-page=' + LoadedPage + '] > ul').append($compile(TreeItem)($scope));
                        $('#EntitiesTree li[data-page=' + LoadedPage + ']').removeClass('pending').addClass('done').removeAttr('style');
                        if (FilterID > 0 || FilterObj.length > 0) {
                            if ($('#EntitiesTree li[data-page=' + LoadedPage + '] > ul').children().length == 0) {
                                $('#EntitiesTree li[data-page=' + LoadedPage + '] > ul').parent().css('display', 'none');
                                $('#EntitiesTree li[data-page=' + LoadedPage + ']').css('display', 'none');
                                clearTimeout(timer);
                                timer = setTimeout(function () {
                                    StartPageLoad(false)
                                }, 1);
                            }
                            if (($('#EntitiesTree li[data-page=' + LoadedPage + '] > ul').children().length > 0) && ($('#EntitiesTree li[data-page=' + LoadedPage + '] > ul').children().length < 10)) {
                                clearTimeout(timer);
                                timer = setTimeout(function () {
                                    StartPageLoad(false)
                                }, 1);
                            }
                        }
                        if ($scope.EntityHighlight == true) {
                            $("#EntitiesTree li.active").removeClass('active')
                            $("#EntitiesTree  li[data-CostCentreID=" + $scope.CurrentCostcentreID + "] a[data-entityid=" + $stateParams.ID + "]").parent('li').addClass('active');
                            $scope.EntityHighlight = false;
                        }
                        if (SearchEntityID != 0) {
                            $location.path('/mui/planningtool/costcentre/detail/section/' + SearchEntityID + '/overview');
                            SearchEntityID = 0;
                        }
                    }
                } else {
                    $("#GanttDataContainer").html("");
                    $("#GanttBlockContainer").html("");
                    $("#EntitiesTree").html("");
                    $("#ListHolder").html("");
                    if (FilterID > 0 || FilterObj.length > 0) {
                        $(window).AdjustHeightWidth();
                    }
                }
                $scope.disableShowAll = false;
            });
            $(window).AdjustHeightWidth();
        }
        $scope.LoadCostCentreGantHeader = function (id) {
            var EntityIDs = [];
            var dates = [];
            var tempstartdates = [];
            var tempenddates = [];
            var start = "";
            var end = "";
            var datStartUTCval = "";
            var datstartval = [];
            var datEndUTCval = "";
            var datendval = [];
            var maxstartDates = "";
            var maxendDates = "";
            $scope.Gnttsettings.StartDate = "";
            $scope.Gnttsettings.EndDate = "";
            $scope.GanttCurrentDates = {};
            $scope.Gnttsettings.StartDate = Date.parse('-1year').set({
                day: 1,
                month: 0
            }).toString('yyyy-MM-dd');
            $scope.Gnttsettings.EndDate = Date.parse('+year').set({
                day: 31,
                month: 11
            }).toString('yyyy-MM-dd');
            CcdetailService.GetEntitysLinkedToCostCenter(id).then(function (GetEntityIds) {
                if (GetEntityIds.Response.length != null) {
                    for (var i = 0; i < GetEntityIds.Response.length; i++) {
                        EntityIDs.push(GetEntityIds.Response[i].EntityID);
                    }
                    CcdetailService.GetPeriodByIdForGantt(EntityIDs.join(",") == "" ? "0" : EntityIDs.join(",")).then(function (GantRstl) {
                        if (GantRstl.Response.length != null) {
                            for (var i = 0; i < GantRstl.Response.length; i++) {
                                datStartUTCval = GantRstl.Response[i].Startdate.substr(6, (GantRstl.Response[i].Startdate.indexOf('+') - 6));
                                datstartval.push(new Date.create(parseInt(datStartUTCval)));
                                datEndUTCval = GantRstl.Response[i].EndDate.substr(6, (GantRstl.Response[i].EndDate.indexOf('+') - 6));
                                datendval.push(new Date.create(parseInt(datEndUTCval)));
                            }
                            maxstartDates = ConvertDateToString(new Date.create(Math.min.apply(null, datstartval)));
                            maxendDates = ConvertDateToString(new Date.create(Math.max.apply(null, datendval)));
                            if (maxstartDates != "") start = maxstartDates;
                            if (maxendDates != "") end = maxendDates;
                            if (end == "NaN-NaN-NaN") {
                                $scope.Gnttsettings.StartDate = Date.parse('-1year').set({
                                    day: 1,
                                    month: 0
                                }).toString('yyyy-MM-dd');
                                $scope.Gnttsettings.EndDate = Date.parse('+year').set({
                                    day: 31,
                                    month: 11
                                }).toString('yyyy-MM-dd');
                            }
                            var currentYear = new Date.create().getFullYear();
                            var strtDate = maxstartDates.substring(0, 4);
                            var endingDate = maxendDates.substring(0, 4);
                            if (currentYear < endingDate) {
                                $scope.Gnttsettings.StartDate = Date.parse('-1year').set({
                                    day: 1,
                                    month: 0
                                }).toString('yyyy-MM-dd');
                                $scope.Gnttsettings.EndDate = Date.parse(maxendDates).set({
                                    day: 31,
                                    month: 11
                                }).toString('yyyy-MM-dd');;
                            } else {
                                $scope.Gnttsettings.StartDate = Date.parse('-1year').set({
                                    day: 1,
                                    month: 0
                                }).toString('yyyy-MM-dd');
                                $scope.Gnttsettings.EndDate = Date.parse('+year').set({
                                    day: 31,
                                    month: 11
                                }).toString('yyyy-MM-dd');
                            }
                            if (start == "NaN-NaN-NaN" || start == "") start = Date.parse('-1year').set({
                                day: 1,
                                month: 0
                            }).toString('yyyy-MM-dd');
                            if (end == "NaN-NaN-NaN" || end == "") end = Date.parse('+1year').set({
                                day: 31,
                                month: 11
                            }).toString('yyyy-MM-dd');
                            start = Date.parse(start);
                            end = Date.parse(end);
                            var dates = [];
                            dates[start.getFullYear()] = [];
                            dates[start.getFullYear()][start.getMonth()] = [start];
                            var last = start;
                            while (last.compareTo(end) == -1) {
                                var next = last.clone().addDays(1);
                                if (!dates[next.getFullYear()]) {
                                    dates[next.getFullYear()] = [];
                                }
                                if (!dates[next.getFullYear()][next.getMonth()]) {
                                    dates[next.getFullYear()][next.getMonth()] = [];
                                }
                                dates[next.getFullYear()][next.getMonth()].push(next);
                                last = next;
                            }
                            $scope.dates = dates;
                            return dates;
                        }
                    });
                }
            });
        }
        var TreePageLoadTimer = undefined;

        function fnPageTemplate(ItemCnt) {
            $scope.GanttViewDataPageTemplate = "";
            $scope.GanttViewBlockPageTemplate = "";
            var PageSize = 1;
            var itemsPerPage = 30;
            if (ItemCnt > itemsPerPage) {
                PageSize = ItemCnt / itemsPerPage;
            }
            var TreeTemplate = '';
            var ListTemplate = '';
            var height = 34 * 30;
            for (var i = 0; i < PageSize; i++) {
                TreeTemplate += "<li class='pending listviewBGDiv' style='min-height: " + height + "px;' data-page='" + i + "'><ul class='nav nav-list CCnav-list nav-list-menu'></ul></li>"
                ListTemplate += "<tbody class='pending listviewBGDiv' style='min-height: " + height + "px;' data-page='" + i + "'></tbody>";
                $scope.GanttViewDataPageTemplate += '<tbody class="widthcent"  data-page="' + i + '"><tr><td style="height: ' + (height - 14) + 'px;border-color: #fff;"></td></tr></tbody>';
                $scope.GanttViewBlockPageTemplate += '<div data-page="' + i + '" style="min-height: ' + height + 'px;" class="ganttview-data-blocks-page widthInherit"></div>';
            }
            if (TreeTemplate.length > 0) {
                $scope.GanttViewDataPageTemplate += '<tbody class="blankTreeNode"></tbody>';
                $scope.ListTemplate = ListTemplate;
                $("#EntitiesTree").trigger('onListViewTemplateCreation', [ListTemplate]);
                $("#EntitiesTree").html($compile(TreeTemplate + '<li class="blankTreeNode"></li>')($scope));
            }
        }

        function GetCostCenterEntityType() {
            CcdetailService.GetEntityTypeByID(5).then(function (GetCostCentreEnityType) {
                if (GetCostCentreEnityType.Response != null) {
                    $scope.CostCentreEntityType = GetCostCentreEnityType.Response;
                }
            });
        }
        
        var pageQueue = [];
        $('#EntitiesTree').scroll(function () {
            $("#ListHolder").scrollTop($(this).scrollTop());
            $("#GanttDataContainer").scrollTop($(this).scrollTop());
            $timeout.cancel(timer);
            timer = $timeout(function () {
                StartPageLoad(false)
            }, 150);
        });
        $(window).scroll(function () {
            $timeout.cancel(timer);
            timer = $timeout(function () {
                StartPageLoad(true)
            }, 30);
        });

        function StartPageLoad(windowScroll) {
            var obj = $('#EntitiesTree');
            var areaHeight = 0;
            var areaTop = 0;
            if (windowScroll) {
                areaHeight = $(window).height();
                areaTop = $(window).scrollTop();
            } else {
                areaHeight = $(obj).height();
                areaTop = $(obj).position().top;
            }
            $('li.pending', obj).each(function () {
                var top = $(this).position().top - areaTop;
                var height = $(this).height();
                if (top + height + height < 0) { } else if (top > areaHeight) { } else {
                    $(this).removeClass('pending').addClass('inprogress');
                    pageQueue.push($(this).attr('data-page'));
                }
            });
        }
        $timeout(function () { LoadPageContent() }, 30);

        function LoadPageContent() {
            var PageNo = pageQueue.pop();
            if (PageNo == undefined) {
                setTimeout(LoadPageContent, 30);
            } else {
                var StartRowNo = PageNo * 30;
                var MaxNoofRow = 30;
                $timeout.cancel(recall);
                var recall = $timeout(function () {
                    TreeLoadPage(PageNo, StartRowNo, MaxNoofRow);
                }, 100);
            }
        }

        function UniqueKEY(UniKey) {
            var substr = UniKey.split('.');
            var id = "";
            var row = substr.length;
            if (row > 1) {
                for (var i = 0; i < row; i++) {
                    id += substr[i];
                    if (i != row - 1) {
                        id += "-";
                    }
                }
                return id;
            }
            return UniKey;
        }
        $scope.TreeClassList = [];

        function GenateClass(UniKey) {
            var ToLength = UniKey.lastIndexOf('-');
            if (ToLength == -1) {
                return "";
            }
            var id = UniKey.substring(0, ToLength);
            var afterSplit = id.split('-');
            var result = "";
            for (var i = 0; i < afterSplit.length; i++) {
                result += SplitClss(id, i + 1);
            }
            return result;
        }

        function SplitClss(NewId, lengthofdata) {
            var afterSplit = NewId.split('-');
            var finalresult = " p";
            for (var i = 0; i < lengthofdata; i++) {
                finalresult += afterSplit[i];
                if (i != lengthofdata - 1) {
                    finalresult += "-"
                }
            }
            return finalresult;
        }

        function Space(UniKey, ParentLevel) {
            var substr = UniKey.split('.');
            var itag = "";
            var row = (substr.length - ParentLevel);
            if (row > 0) {
                for (var i = 1; i < row; i++) {
                    itag += "<i class='icon-'></i>";
                }
            }
            return itag;
        }

        function TreeSpace(SpaceLength) {
            var itag = "";
            if (SpaceLength > 0) {
                for (var i = 1; i < SpaceLength; i++) {
                    itag += "<i class='icon-'></i>";
                }
            }
            return itag;
        }

        function LoadChildTreeNodes(data, ChildCount, UniqueId, CostCenterId) {
            if (ChildCount > 0) {
                if ($('#treeSettings li i[data-icon=' + UniqueId + ']').attr("data-isExpanded") == "true") {
                    $('#treeSettings li i[data-icon=' + UniqueId + ']').attr("class", "icon-caret-right");
                    Collapse(UniqueId);
                    $('#treeSettings li i[data-icon=' + UniqueId + ']').attr("data-isExpanded", "false");
                    $('#EntitiesTree').scroll();
                } else {
                    $('#treeSettings li i[data-icon=' + UniqueId + ']').attr("class", "icon-caret-down");
                    Expand(UniqueId);
                    $('#treeSettings li i[data-icon=' + UniqueId + ']').attr("data-isExpanded", "true");
                }
                if ($('#treeSettings li i[data-icon=' + UniqueId + ']').attr("data-isChildrenPresent") == "true") {
                    $scope.ExpandingEntityID = data;
                    TreeLoad(true, UniqueId, CostCenterId, false);
                    $('#treeSettings li i[data-icon=' + UniqueId + ']').attr("data-isChildrenPresent", "false");
                }
            }
        }
        var PathforBreadCrum = "";
        $scope.array = [];

        function TreeLoad(ParentOrChild, IdUnique, CostCenterId, CostCentrecreation) {
            var StartRowNo = 0;
            var MaxNoofRow = 30;
            var TreeItem = "";
            var ChildInclude = true;
            TreeItem = "";
            var Constructunique = IdUnique.substr(0, IdUnique.indexOf("-")) + ".";
            if (CostCentrecreation == false) {
                var parentNode = {};
                parentNode.StartRowNo = StartRowNo;
                parentNode.MaxNoofRow = MaxNoofRow;
                parentNode.FilterID = 0;
                parentNode.SortOrderColumn = "null";
                parentNode.IsDesc = false;
                parentNode.IncludeChildren = false;
                parentNode.EntityID = CostCenterId;
                parentNode.Level = $scope.Level;
                parentNode.ExpandingEntityID = $scope.ExpandingEntityID;
                parentNode.FilterType = 2;
                parentNode.IDArr = GlobalChildNode;
                parentNode.FilterAttributes = FilterObj;
                var LoadedPage = 0;
                CcdetailService.CostCentreDetail(parentNode).then(function (ParentNode) {
                    $scope.ParenttreeList = ParentNode.Response.Data;
                    if ($scope.ParenttreeList != null) {
                        $scope.ListViewDetails.push({
                            data: ParentNode,
                            PageIndex: LoadedPage,
                            UniqueID: IdUnique,
                            StartRowNo: StartRowNo,
                            MaxNoofRow: MaxNoofRow,
                            FilterID: 0,
                            SortOrderColumn: "null",
                            IsDesc: false,
                            IncludeChildren: false,
                            EntityID: CostCenterId,
                            Level: $scope.Level,
                            IDArr: GlobalChildNode,
                            FilterAttributes: []
                        });
                        var CostCentreID = '0';
                        var IsCostCentre = false;
                        var rootID = 0;
                        for (var i = 0; i < $scope.ParenttreeList.length; i++) {
                            var ExtraSpace = "<i class='icon-'></i>";
                            var IscostCenter = false;
                            var UniqueId = '';
                            CostCentreID = $scope.ParenttreeList[i]["CostCenterID"];
                            if ($scope.ParenttreeList[i]["TypeID"] == 5) {
                                ExtraSpace = "";
                                IscostCenter = true;
                            }
                            UniqueId = UniqueKEY($scope.ParenttreeList[i]["class"].toString());
                            var GeneralUniquekey = UniqueKEY($scope.ParenttreeList[i]["class"].toString());
                            var ClassName = GenateClass(GeneralUniquekey);
                            ClassName += " mo" + UniqueId;
                            TreeItem += "<li data-over='true' data-CostCentreID=" + CostCentreID + "  class='" + ClassName + "'  data-uniqueKey='" + UniqueId.toString() + "'><a data-EntityLevel='" + $scope.ParenttreeList[i]["Level"] + "' data-role='Activity' data-iscostcentre=" + IscostCenter + " data-EntityTypeID=" + $scope.ParenttreeList[i]["TypeID"] + "  data-entityID=" + $scope.ParenttreeList[i]["Id"] + "  data-colorcode=" + $scope.ParenttreeList[i]["ColorCode"] + " data-shorttext=" + $scope.ParenttreeList[i]["ShortDescription"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-entityname=\"" + $scope.ParenttreeList[i]["Name"] + "\"> ";
                            TreeItem += ExtraSpace + Space($scope.ParenttreeList[i]["class"], $scope.CurrentTreeLevel);
                            if (ParentNode.Response.Data[i]["TotalChildrenCount"] > 0) {
                                TreeItem += "<i data-role='Arrow' data-CostCentreID=" + CostCentreID + " data-IsConnected='false' data-icon='" + UniqueId + "' class='icon-caret-right' data-isExpanded=false data-isChildrenPresent=true data-isChildLoaded=true data-ParentID=" + ParentNode.Response.Data[i]["Id"] + " data-TotalChildrenCount=" + ParentNode.Response.Data[i]["TotalChildrenCount"] + " data-Permission=" + ParentNode.Response.Data[i].Permission + " data-UniqueId=\"" + UniqueId + "\"></i>";
                            } else {
                                TreeItem += "<i class='icon-'></i>";
                            }
                            TreeItem += " <span data-uniqueKey='" + UniqueId + "' style='background-color: #" + $scope.ParenttreeList[i]["ColorCode"].trim() + ";'  class='eicon-s margin-right5x'>" + $scope.ParenttreeList[i]["ShortDescription"] + "</span>";
                            TreeItem += "<span class='treeItemName' data-name='text' data-CostCentreIDForReport=" + CostCentreID + ">" + $scope.ParenttreeList[i]["Name"] + "</span>";
                            if (parseInt($scope.ParenttreeList[i]["TypeID"]) == 5) TreeItem += "<i data-role='Overview' class='icon-reorder menu' data-toggle='modal' data-AccessValue=" + $scope.ParenttreeList[i].Permission + " data-typeID=" + $scope.ParenttreeList[i]["TypeID"] + " data-entityID=" + $scope.ParenttreeList[i]["Id"] + "></i>";
                            TreeItem += "</a> </li>";
                        }
                        $('#treeSettings li[data-uniqueKey=' + IdUnique + ']').after($compile(TreeItem)($scope));
                        if (CostCentrecreation == true) {
                            $("#EntitiesTree li.active").removeClass('active')
                            $('#EntitiesTree  li[data-uniqueKey=' + UniqueId + ']').addClass('active');
                            if ($('#EntitiesTree  li[data-uniqueKey=' + IdUnique + '] i').attr('data-isChildrenPresent') == undefined) {
                                $('#EntitiesTree  span[data-uniqueKey=' + IdUnique + ']').prev('i').remove();
                                $('#EntitiesTree  span[data-uniqueKey=' + IdUnique + ']').before($compile('<i class="icon-caret-down" data-uniqueid="' + IdUnique + '" data-Permission="false" data-totalchildrencount="1" data-parentid=' + $('#EntitiesTree  span[data-uniqueKey=' + IdUnique + ']').attr('data-parentid') + ' data-ischildloaded="true" data-ischildrenpresent="false" data-isexpanded="true" data-icon="' + IdUnique + '" data-role="Arrow"></i>')($scope));
                            }
                        }
                        if ($scope.EntityHighlight == true) {
                            $("#EntitiesTree li.active").removeClass('active')
                            $("#EntitiesTree  li a[data-entityid=" + $stateParams.ID + "]").parent('li').addClass('active');
                            $scope.EntityHighlight = false;
                        }
                        $scope.$broadcast('onTreePageCreation', [ParentNode, 0, IdUnique]);
                    }
                });
            } else {
                var parentNode = {};
                parentNode.StartRowNo = StartRowNo;
                parentNode.MaxNoofRow = MaxNoofRow;
                parentNode.FilterID = 0;
                parentNode.SortOrderColumn = "null";
                parentNode.IsDesc = false;
                parentNode.IncludeChildren = true;
                parentNode.EntityID = CostCenterId;
                parentNode.Level = $scope.Level;
                parentNode.ExpandingEntityID = $scope.ExpandingEntityID;
                parentNode.FilterType = 2;
                parentNode.IDArr = GlobalChildNode;
                parentNode.FilterAttributes = FilterObj;
                var LoadedPage = 0;
                CcdetailService.CostCentreTreeDetail(parentNode).then(function (ParentNode) {
                    $scope.ParenttreeList = ParentNode.Response.Data;
                    if ($scope.ParenttreeList != null) {
                        $scope.ListViewDetails.push({
                            data: ParentNode,
                            PageIndex: LoadedPage,
                            UniqueID: IdUnique,
                            StartRowNo: StartRowNo,
                            MaxNoofRow: MaxNoofRow,
                            FilterID: 0,
                            SortOrderColumn: "null",
                            IsDesc: false,
                            IncludeChildren: false,
                            EntityID: CostCenterId,
                            Level: $scope.Level,
                            IDArr: GlobalChildNode,
                            FilterAttributes: []
                        });
                        var CostCentreID = '0';
                        var IsCostCentre = false;
                        var rootID = 0;
                        for (var i = 0; i < $scope.ParenttreeList.length; i++) {
                            var ExtraSpace = "<i class='icon-'></i>";
                            var IscostCenter = false;
                            var UniqueId = '';
                            CostCentreID = $scope.ParenttreeList[i]["CostCenterID"];
                            if ($scope.ParenttreeList[i]["TypeID"] == 5) {
                                ExtraSpace = "";
                                IscostCenter = true;
                            }
                            UniqueId = UniqueKEY($scope.ParenttreeList[i]["class"].toString());
                            var GeneralUniquekey = UniqueKEY($scope.ParenttreeList[i]["class"].toString());
                            var ClassName = GenateClass(GeneralUniquekey);
                            ClassName += " mo" + UniqueId;
                            TreeItem += "<li data-over='true' data-CostCentreID=" + CostCentreID + "  class='" + ClassName + "'  data-uniqueKey='" + UniqueId.toString() + "'><a data-EntityLevel='" + $scope.ParenttreeList[i]["Level"] + "' data-role='Activity' data-iscostcentre=" + IscostCenter + " data-EntityTypeID=" + $scope.ParenttreeList[i]["TypeID"] + "  data-entityID=" + $scope.ParenttreeList[i]["Id"] + "  data-colorcode=" + $scope.ParenttreeList[i]["ColorCode"] + " data-shorttext=" + $scope.ParenttreeList[i]["ShortDescription"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-entityname=\"" + $scope.ParenttreeList[i]["Name"] + "\"> ";
                            TreeItem += ExtraSpace + Space($scope.ParenttreeList[i]["class"], $scope.CurrentTreeLevel);
                            if (ParentNode.Response.Data[i]["TotalChildrenCount"] > 0) {
                                TreeItem += "<i data-role='Arrow' data-CostCentreID=" + CostCentreID + " data-IsConnected='false' data-icon='" + UniqueId + "' class='icon-caret-right' data-isExpanded=false data-isChildrenPresent=true data-isChildLoaded=true data-ParentID=" + ParentNode.Response.Data[i]["Id"] + " data-TotalChildrenCount=" + ParentNode.Response.Data[i]["TotalChildrenCount"] + " data-Permission=" + ParentNode.Response.Data[i].Permission + " data-UniqueId=\"" + UniqueId + "\"></i>";
                            } else {
                                TreeItem += "<i class='icon-'></i>";
                            }
                            TreeItem += " <span data-uniqueKey='" + UniqueId + "' style='background-color: #" + $scope.ParenttreeList[i]["ColorCode"].trim() + ";'  class='eicon-s margin-right5x'>" + $scope.ParenttreeList[i]["ShortDescription"] + "</span>";
                            TreeItem += "<span class='treeItemName' data-name='text' data-CostCentreIDForReport=" + CostCentreID + ">" + $scope.ParenttreeList[i]["Name"] + "</span>";
                            if (parseInt($scope.ParenttreeList[i]["TypeID"]) == 5) TreeItem += "<i data-role='Overview' class='icon-reorder menu' data-toggle='modal' data-AccessValue=" + $scope.ParenttreeList[i].Permission + " data-typeID=" + $scope.ParenttreeList[i]["TypeID"] + " data-entityID=" + $scope.ParenttreeList[i]["Id"] + "></i>";
                            TreeItem += "</a> </li>";
                        }
                        $('#treeSettings li[data-uniqueKey=' + IdUnique + ']').after($compile(TreeItem)($scope));
                        if (CostCentrecreation == true) {
                            $("#EntitiesTree li.active").removeClass('active')
                            $('#EntitiesTree  li[data-uniqueKey=' + UniqueId + ']').addClass('active');
                            if ($('#EntitiesTree  li[data-uniqueKey=' + IdUnique + '] i').attr('data-isChildrenPresent') == undefined) {
                                $('#EntitiesTree  span[data-uniqueKey=' + IdUnique + ']').prev('i').remove();
                                $('#EntitiesTree  span[data-uniqueKey=' + IdUnique + ']').before($compile('<i class="icon-caret-down" data-uniqueid="' + IdUnique + '" data-Permission="false" data-totalchildrencount="1" data-parentid=' + $('#EntitiesTree  span[data-uniqueKey=' + IdUnique + ']').attr('data-parentid') + ' data-ischildloaded="true" data-ischildrenpresent="false" data-isexpanded="true" data-icon="' + IdUnique + '" data-role="Arrow"></i>')($scope));
                            }
                        }
                        if ($scope.EntityHighlight == true) {
                            $("#EntitiesTree li.active").removeClass('active')
                            $("#EntitiesTree  li a[data-entityid=" + $stateParams.ID + "]").parent('li').addClass('active');
                            $scope.EntityHighlight = false;
                        }
                        $scope.$broadcast('onTreePageCreation', [ParentNode, 0, IdUnique]);
                    }
                });
            }
            $(window).AdjustHeightWidth();
        }
        $(document).click(function () {
            $('#moduleContextmenu').hide();
        })
        $("#treeSettings").click(function (e) {
            if ($scope.disableShowAll == false) {
                $(window).AdjustHeightWidth();
                var TargetControl = $(e.target);
                $scope.muiiscalender.Iscalender = false;
                $scope.muiiscalender.CalenderID = 0;
                $scope.EntityLevelfrFin.levelfrFinancial = TargetControl.attr('data-EntityLevel');
                var col = "";
                if (TargetControl[0].tagName == "I" && TargetControl.attr('data-role') == 'Arrow') {
                    $scope.Level = TargetControl.parent().attr('data-entitylevel');
                    LoadChildTreeNodes(TargetControl.attr('data-ParentID'), TargetControl.attr('data-TotalChildrenCount'), TargetControl.attr('data-UniqueId'), TargetControl.attr('data-costcentreid'))
                } else if (TargetControl[0].tagName == "I" && TargetControl.attr('data-role') == 'Overview') {
                    $window.SubscriptionEntityTypeID = TargetControl.find('i[data-role="Overview"]').attr('data-typeid');
                    $window.GlobalUniquekey = TargetControl.parents('li').attr('data-uniquekey');
                    $(window).trigger("GloblaCCUniqueKeyAccess", [$window.GlobalUniquekey]);
                    $scope.CurrentCostcentreID = TargetControl.attr('data-entityID');
                    $timeout(function () {
                        if ($scope.ContextMenuRecords.ChildContextData != null && $scope.ContextMenuRecords.ChildContextData != "") {
                            $scope.entitytpesdata = $.grep($scope.ContextMenuRecords.ChildContextData, function (e) {
                                return e.ParentTypeId == 5 && e.EntityTypeAccessIsEnabled == true
                            });
                            $('#moduleContextmenu').css('top', $(TargetControl[0]).offset().top + 22 + 'px').show();
                            e.preventDefault();
                            return false;
                        } else if ($scope.ShowDeleteOption == true || $scope.ShowDeleteOption == true) {
                            $('#moduleContextmenu').css('top', $(TargetControl[0]).offset().top + 22 + 'px').show();
                            e.preventDefault();
                            return false;
                        }
                    }, 50);
                } else if (TargetControl[0].tagName == "A" && TargetControl.attr('data-role') == 'Activity') {
                    $scope.costcentreidforreport = $(TargetControl).children("span").eq(1).attr('data-costcentreidforreport');
                    if ($("#EntitiesTree li").hasClass('active')) $("#EntitiesTree li.active").removeClass('active');
                    $(TargetControl).parents('li').addClass("active");
                    $scope.CurrentCostcentreID = $(TargetControl).parents('li').attr('data-CostCentreID');
                    e.preventDefault();
                    var localScope = $(e.target).scope();
                    localScope.$apply(function () {
                        var id = TargetControl.attr('data-entityID');
                        $window.EntityTypeID = id;
                        $window.SubscriptionEntityTypeID = TargetControl.find('i[data-role="Overview"]').attr('data-typeid');
                        $window.GlobalUniquekey = TargetControl.parent().attr('data-uniquekey');
                        $scope.EntityTypeID = TargetControl.attr('data-entitytypeid');
                        if (TargetControl.attr('data-iscostcentre') == "true")
                            $location.path('/mui/planningtool/costcentre/detail/section/' + TargetControl.attr('data-entityID') + '/overview');
                        else {
                            //$location.path('/mui/planningtool/costcentre/detail/sectionentity/' + TargetControl.attr('data-entityID'));
                            if (!localStorage.lasttab)
                                $location.path('/mui/planningtool/costcentre/detail/sectionentity/' + TargetControl.attr('data-entityID') + '/overview');
                            else
                                $location.path('/mui/planningtool/costcentre/detail/sectionentity/' + TargetControl.attr('data-entityID') + '/' + localStorage.getItem('lasttab'));
                        }
                    });
                    $('#moduleContextmenu').css('top', $(TargetControl[0]).offset().top + 22 + 'px').show();
                } else if (TargetControl[0].tagName == "SPAN" && TargetControl.attr('data-name') == 'text') {
                    $scope.costcentreidforreport = $(TargetControl).attr('data-costcentreidforreport');
                    if ($("#EntitiesTree li").hasClass('active')) $("#EntitiesTree li.active").removeClass('active');
                    $(TargetControl).parents('li').addClass("active");
                    $scope.CurrentCostcentreID = $(TargetControl).parents('li').attr('data-CostCentreID');
                    e.preventDefault();
                    var localScope = $(e.target).scope();
                    $scope.EntityTypeID = TargetControl.parent().attr('data-entitytypeid');
                    localScope.$apply(function () {
                        var id = TargetControl.parent().attr('data-entityID');
                        $window.EntityTypeID = id;
                        $window.SubscriptionEntityTypeID = TargetControl.next().attr('data-typeid');
                        $window.GlobalUniquekey = TargetControl.parent().parent().attr('data-uniquekey');
                        if (TargetControl.parent().attr('data-iscostcentre') == "true")
                            $location.path('/mui/planningtool/costcentre/detail/section/' + TargetControl.parent().attr('data-entityID') + '/overview');
                        else {
                            if (!localStorage.lasttab)
                                $location.path('/mui/planningtool/costcentre/detail/sectionentity/' + TargetControl.parent().attr('data-entityID') + '/overview');
                            else
                                $location.path('/mui/planningtool/costcentre/detail/sectionentity/' + TTargetControl.parent().attr('data-entityID') + '/' + localStorage.getItem('lasttab'));
                        }
                        // $location.path('/mui/planningtool/costcentre/detail/sectionentity/' + TargetControl.parent().attr('data-entityID'));
                    });
                }
                $(window).AdjustHeightWidth();
            }
        });

        function GenerateCss() {
            var row = $scope.TreeClassList.length;
            var cssStyle = '<style type="text/css" id="TreeCtrlCss">';
            $('#TreeCtrlCss').remove()
            for (var i = 0; i < row; i++) {
                cssStyle += '.' + $scope.TreeClassList[i].trim();
                cssStyle += "{display: none;}";
            }
            $('head').append(cssStyle + "</style>");
        }

        function Collapse(value) {
            if ($scope.TreeClassList.indexOf("p" + value) == -1) {
                $scope.TreeClassList.push("p" + value);
            }
            GenerateCss();
        }

        function Expand(value) {
            var index = $scope.TreeClassList.indexOf("p" + value)
            if (index != -1) {
                $scope.TreeClassList.splice(index, 1);
            }
            GenerateCss();
        }

        function TreeLoadPage(PageNo, StartRowNo, MaxNoofRow) {
            var TreeItem = "";
            var ChildInclude = true;
            var Constructunique = '';
            var parentNode = {};
            parentNode.StartRowNo = StartRowNo;
            parentNode.MaxNoofRow = MaxNoofRow;
            parentNode.FilterID = 0;
            parentNode.SortOrderColumn = "null";
            parentNode.IsDesc = false;
            parentNode.IncludeChildren = true;
            parentNode.EntityID = '0';
            parentNode.Level = $scope.Level;
            parentNode.ExpandingEntityID = $scope.ExpandingEntityID;
            parentNode.FilterType = $scope.FilterType;
            parentNode.IDArr = GlobalChildNode;
            parentNode.FilterAttributes = FilterObj;
            CcdetailService.CostCentreDetail(parentNode).then(function (ParentNode) {
                var optID = "";
                var totcnt = 0;
                var IsAppendVal = false;
                var preval = "";
                if ($scope.FilterType == 6) {
                    $scope.Tempattrgroupfilter = [];
                    for (var i = 0; i < ParentNode.Response.Data.length; i++) {
                        IsAppendVal = false;
                        preval = "";
                        if (optID != ParentNode.Response.Data[i].OptionCaption) {
                            totcnt = $.grep(ParentNode.Response.Data, function (e) {
                                return e.OptionID == ParentNode.Response.Data[i].OptionID
                            });
                            for (var x = 0; x < $scope.ListViewDetails.length; x++) {
                                preval = $.grep($scope.ListViewDetails[x].data.Response.Data, function (e) {
                                    return e.Name == ParentNode.Response.Data[i]["OptionCaption"]
                                });
                                if (preval.length > 0) {
                                    IsAppendVal = true;
                                    break;
                                }
                            }
                            if (IsAppendVal == false) {
                                $scope.Tempattrgroupfilter.push({
                                    1: "",
                                    ApprovedAllocatedAmount: 0,
                                    ApprovedBudget: 0,
                                    ApprovedSubAllocatedAmount: 0,
                                    Available: 0,
                                    BudgetDeviation: 0,
                                    ColorCode: "000000",
                                    Commited: 0,
                                    CostCenterID: 0,
                                    EntityID: 0,
                                    EntityStateID: 0,
                                    Id: 0,
                                    InRequest: 0,
                                    IsLock: true,
                                    Level: -1,
                                    MileStone: "<root/>",
                                    Name: ParentNode.Response.Data[i]["OptionCaption"],
                                    ParentID: 0,
                                    Period: "<root/>",
                                    Permission: 0,
                                    PlannedAmount: 0,
                                    ShortDescription: "G",
                                    Spent: 0,
                                    Status: "",
                                    TempPeriod: "-",
                                    TotalChildrenCount: totcnt.length + 1,
                                    TypeID: 0,
                                    TypeName: "@Option",
                                    UniqueKey: "-0" + i,
                                    class: "1.3" + i
                                });
                            }
                        }
                        $scope.Tempattrgroupfilter.push({
                            1: ParentNode.Response.Data[i]["1"],
                            ApprovedAllocatedAmount: parseInt(ParentNode.Response.Data[i]["ApprovedAllocatedAmount"], 10),
                            ApprovedBudget: parseInt(ParentNode.Response.Data[i]["ApprovedBudget"], 10),
                            ApprovedSubAllocatedAmount: parseInt(ParentNode.Response.Data[i]["ApprovedSubAllocatedAmount"], 10),
                            Available: parseInt(ParentNode.Response.Data[i]["Available"], 10),
                            BudgetDeviation: parseInt(ParentNode.Response.Data[i]["BudgetDeviation"], 10),
                            ColorCode: ParentNode.Response.Data[i]["ColorCode"],
                            Commited: parseInt(ParentNode.Response.Data[i]["Commited"], 10),
                            CostCenterID: parseInt(ParentNode.Response.Data[i]["CostCenterID"], 10),
                            EntityID: parseInt(ParentNode.Response.Data[i]["EntityID"], 10),
                            EntityStateID: parseInt(ParentNode.Response.Data[i]["EntityStateID"], 10),
                            Id: parseInt(ParentNode.Response.Data[i]["InRequest"], 10),
                            InRequest: parseInt(ParentNode.Response.Data[i]["1"], 10),
                            IsLock: ParentNode.Response.Data[i]["IsLock"],
                            Level: parseInt(ParentNode.Response.Data[i]["Level"], 10),
                            MileStone: ParentNode.Response.Data[i]["MileStone"],
                            Name: ParentNode.Response.Data[i]["Name"],
                            ParentID: parseInt(ParentNode.Response.Data[i]["ParentID"], 10),
                            Period: ParentNode.Response.Data[i]["Period"],
                            Permission: ParentNode.Response.Data[i]["Permission"],
                            PlannedAmount: parseInt(ParentNode.Response.Data[i]["PlannedAmount"], 10),
                            ShortDescription: ParentNode.Response.Data[i]["ShortDescription"],
                            Spent: parseInt(ParentNode.Response.Data[i]["Spent"], 10),
                            Status: ParentNode.Response.Data[i]["Status"],
                            TempPeriod: ParentNode.Response.Data[i]["TempPeriod"],
                            TotalChildrenCount: ParentNode.Response.Data[i]["TotalChildrenCount"],
                            TypeID: ParentNode.Response.Data[i]["TypeID"],
                            TypeName: ParentNode.Response.Data[i]["TypeName"],
                            UniqueKey: ParentNode.Response.Data[i]["UniqueKey"],
                            class: ParentNode.Response.Data[i]["class"]
                        });
                        optID = ParentNode.Response.Data[i].OptionCaption;
                    }
                    $scope.ParenttreeNewList = $scope.Tempattrgroupfilter;
                    ParentNode.Response.Data = $scope.Tempattrgroupfilter;
                } else $scope.ParenttreeNewList = ParentNode.Response.Data;
                $scope.ParenttreeNewList = ParentNode.Response.Data;
                if ($scope.ParenttreeNewList != null) {
                    $scope.ListViewDetails.push({
                        data: ParentNode,
                        PageIndex: PageNo,
                        UniqueID: "",
                        StartRowNo: StartRowNo,
                        MaxNoofRow: MaxNoofRow,
                        FilterID: 0,
                        SortOrderColumn: "null",
                        IsDesc: false,
                        IncludeChildren: true,
                        EntityID: '0',
                        Level: $scope.Level,
                        IDArr: GlobalChildNode,
                        FilterAttributes: []
                    });
                    var CostCentreID = '0';
                    for (var i = 0; i < $scope.ParenttreeNewList.length; i++) {
                        var UniqueId = '';
                        var IscostCenter = false;
                        var ExtraSpace = "<i class='icon-'></i>";
                        CostCentreID = $scope.ParenttreeNewList[i]["CostCenterID"];
                        var rootID = 0;
                        if ($scope.ParenttreeNewList[i]["TypeID"] == 5) {
                            ExtraSpace = "";
                            IscostCenter = true;
                        }
                        UniqueId = UniqueKEY($scope.ParenttreeNewList[i]["class"]);
                        var GeneralUniquekey = UniqueKEY($scope.ParenttreeNewList[i]["class"].toString());
                        var ClassName = GenateClass(GeneralUniquekey);
                        ClassName += " mo" + UniqueId;
                        TreeItem += "<li data-over='true' data-CostCentreID=" + CostCentreID + "  class='" + ClassName + "'  data-uniqueKey='" + UniqueId + "'><a data-EntityLevel='" + $scope.ParenttreeNewList[i]["Level"] + "' data-role='Activity' data-iscostcentre='" + IscostCenter + "' data-EntityTypeID='" + $scope.ParenttreeNewList[i]["TypeID"] + "'  data-entityID=" + $scope.ParenttreeNewList[i]["Id"] + " data-colorcode=" + $scope.ParenttreeNewList[i]["ColorCode"] + " data-shorttext=" + $scope.ParenttreeNewList[i]["ShortDescription"] + " data-Permission=" + $scope.ParenttreeNewList[i].Permission + " data-entityname=\"" + $scope.ParenttreeNewList[i]["Name"] + "\"> ";
                        TreeItem += ExtraSpace + Space($scope.ParenttreeNewList[i]["UniqueKey"], 0);
                        if ($scope.ParenttreeNewList[i]["TotalChildrenCount"] > 0) {
                            var level = ($scope.Level == 0 ? 2 : $scope.Level);
                            if (GeneralUniquekey.split("-").length <= level) {
                                TreeItem += "<i data-role='Arrow' data-icon='" + UniqueId + "' class='icon-caret-down' data-CostCentreID=" + CostCentreID + " data-isExpanded=true  data-isChildrenPresent=false data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeNewList[i]["Id"] + " data-TotalChildrenCount=" + $scope.ParenttreeNewList[i]["TotalChildrenCount"] + " data-Permission=" + $scope.ParenttreeNewList[i].Permission + " data-UniqueId=\"" + UniqueId + "\"></i>";
                            } else {
                                TreeItem += "<i data-role='Arrow' data-icon='" + UniqueId + "' class='icon-caret-right' data-CostCentreID=" + CostCentreID + " data-isExpanded=false data-isChildrenPresent=true data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeNewList[i]["Id"] + " data-TotalChildrenCount=" + $scope.ParenttreeNewList[i]["TotalChildrenCount"] + " data-Permission=" + $scope.ParenttreeNewList[i].Permission + " data-UniqueId=\"" + UniqueId + "\"></i>";
                            }
                        } else {
                            TreeItem += "<i class='icon-'></i>";
                        }
                        TreeItem += " <span style='background-color: #" + $scope.ParenttreeNewList[i]["ColorCode"].trim() + ";'  class='eicon-s margin-right5x'>" + $scope.ParenttreeNewList[i]["ShortDescription"] + "</span>";
                        TreeItem += " <span class='treeItemName' data-name='text' data-CostCentreIDForReport=" + CostCentreID + ">" + $scope.ParenttreeNewList[i]["Name"] + "</span>";
                        TreeItem += "</a> </li>";
                    }
                    if (TreeItem.length > 0) {
                        $('#EntitiesTree li[data-page=' + PageNo + '] > ul').html($compile(TreeItem)($scope));
                        $('#EntitiesTree li[data-page=' + PageNo + ']').removeClass('inprogress').addClass('done').removeAttr('style');
                        if ($('#EntitiesTree li[data-page=' + PageNo + '] > ul').attr("data-items") == undefined) {
                            $('#EntitiesTree li[data-page=' + PageNo + '] > ul').attr("data-items", i);
                        } else {
                            var items = parseInt($('#EntitiesTree li[data-page=' + PageNo + '] > ul').attr("data-items")) * i;
                            $('#EntitiesTree li[data-page=' + PageNo + '] > ul').attr("data-items", items);
                        }
                        if ($scope.EntityHighlight == true) {
                            $("#EntitiesTree li.active").removeClass('active')
                            $("#EntitiesTree  li a[data-entityid=" + $stateParams.ID + "]").parent('li').addClass('active');
                            $scope.EntityHighlight = false;
                        }
                        $scope.$broadcast('onTreePageCreation', [ParentNode, PageNo, ""]);
                        setTimeout(LoadPageContent, 0);
                    }
                }
                $(window).AdjustHeightWidth();
            });
        };
        $scope.listofreportEntityID = '';
        $scope.CostCentreListofReportRecords = function () {

            $("#ReportPageModel").modal('show');
            $scope.chkFinancialDetl = true;
            $scope.chkMetadataDetl = true;
            $scope.chkTaskDetl = true;
            $scope.chkMemberDetl = true;
            $scope.StructuralReportDiv = false;
            $scope.ReportGenerate = false;
            $scope.ReportGanttGenerate = false;
            $scope.ReportEdit = false;
            $scope.ReportTypes = [];
            $scope.SelectedReport = "";
            $scope.GanttReportDiv = false;
            $scope.CustomReportDiv = false;
            $scope.ReportImageURL = "NoImage.png";
            $scope.reportDescription = "-";
            $scope.showCustomEntityCheck = false;
            $scope.showCustomSubLevels = false;
            $scope.CustomEntityCheck = "";
            $scope.CustomSubLevelsCheck = "";
            var IDLIST = new Array();
            var ExpandingEntityID = 0;
            var IncludeChildren = true;
            if ($stateParams.ID != undefined && $stateParams.ID != 0) {
                ExpandingEntityID = $stateParams.ID;
            }
            if (FilterID > 0) {
                IncludeChildren = false;
            }
            CcdetailService.ShowReports(0, true).then(function (saveviewscomment) {
                if (saveviewscomment.Response != null) {
                    var resultcustom = saveviewscomment.Response;
                    for (var i = 0, reportDatacustom; reportDatacustom = resultcustom[i++];) {
                        $scope.ReportTypes.push({
                            "OID": reportDatacustom.OID.toString(),
                            "Caption": reportDatacustom.Caption,
                            "ImageURl": "",
                            "Description": ""
                        });
                    }
                } else { }
            });
            CcdetailService.GetFinancialReportSettings().then(function (ReportResultData) {
                var result = ReportResultData.Response;
                for (var i = 0, reportData; reportData = result[i++];) {
                    $scope.ReportTypes.push({
                        "OID": "F_" + reportData.ID.toString(),
                        "Caption": reportData.ReportSettingsName,
                        "ImageURl": reportData.ReportImage,
                        "Description": reportData.Description
                    });
                }
            });
        }
        $scope.changeReportType = function () {
            var eidlist = $scope.listofreportEntityID;
            $scope.Reportreset = [];
            $scope.selectvalue = 0;
            $scope.selectvalue = $scope.SelectedReport;
            $scope.ReportImageURL = "NoImage.png";
            $scope.reportDescription = "-";
            $scope.GanttReportDiv = false;
            $scope.CustomReportDiv = false;
            $scope.StructuralReportDiv = false;
            $scope.reportDescription = "-";
            $scope.showCustomEntityCheck = false;
            $scope.showCustomSubLevels = false;
            $scope.CustomEntityCheck = "";
            $scope.CustomSubLevelsCheck = "";
            if ($scope.selectvalue.contains('F_')) {
                $scope.ReportGenerate = true;
                $scope.ReportGanttGenerate = false;
                $scope.CustomReportDiv = false;
                var ReportTypeData = $.grep($scope.ReportTypes, function (e) {
                    return e.OID == $scope.selectvalue;
                });
                $scope.ReportImageURL = ReportTypeData[0].ImageURl;
                $scope.reportDescription = ReportTypeData[0].Description;
            } else if ($scope.selectvalue == -200) {
                $scope.reportDescription = "Structure report"
                $scope.ReportImageURL = "Structure.jpg";
                $scope.StructuralReportDiv = true;
                $scope.ReportGenerate = true;
                $scope.ReportGanttGenerate = false;
                $scope.CustomReportDiv = false;
                $scope.showCustomEntityCheck = false;
                $scope.showCustomSubLevels = false;
                if ($stateParams.ID != undefined && $stateParams.ID != 0) {
                    $scope.SelectedEntityCheckDivStr = true;
                    $scope.SubLevelsCheckDivStr = true;
                } else {
                    $scope.SelectedEntityCheckDivStr = false;
                    $scope.SubLevelsCheckDivStr = false;
                }
            } else {
                if ($scope.selectvalue > 0) {
                    $scope.ReportEdit = true;
                    $scope.ReportGenerate = true;
                    $scope.ReportGanttGenerate = false;
                    $scope.GanttReportDiv = false;
                    if ($stateParams.ID != undefined && $stateParams.ID != 0) {
                        $scope.CustomReportDiv = true;
                    } else {
                        $scope.CustomReportDiv = false;
                    }
                    CcdetailService.ShowReports($scope.selectvalue, true).then(function (BindReports) {
                        if (BindReports.Response != null) {
                            $scope.Reportreset = BindReports.Response;
                            var count = BindReports.Response.length;
                            $scope.showCustomEntityCheck = $scope.Reportreset[count - 1].EntityLevel;
                            $scope.showCustomSubLevels = $scope.Reportreset[count - 1].SubLevel;
                            $scope.reportDescription = $scope.Reportreset[count - 1].Description;
                            $scope.ReportValue = $scope.Reportreset[count - 1].OID;
                            $scope.ReportImageURL = $scope.Reportreset[count - 1].Preview;
                            $scope.ReportEdit = "devexpress.reportserver:open?reportId=" + $scope.ReportValue + "&revisionId=0&preview=0";
                        } else {
                            NotifyError($translate.instant('LanguageContents.Res_4523.Caption'));
                        }
                    });
                } else if ($scope.selectvalue == -100 || $scope.selectvalue == -101) {
                    $scope.ReportEdit = false;
                    $scope.ReportGenerate = false;
                    $scope.ReportGanttGenerate = true;
                    $scope.reportDescription = "Generate an excel report of the gantt view. Use filters to filter out desired data before generating a report.";
                    $scope.ReportImageURL = "GanttReport.png";
                    $scope.GanttReportDiv = true;
                    $scope.Ganttoption = '';
                    $scope.GanttEntityType = '';
                    $scope.GanttstartDate = "";
                    $scope.Ganttenddate = "";
                    $scope.GanttSelectedEntityCheck = "";
                    $scope.GanttAllSubLevelsCheck = "";
                    $scope.CustomReportDiv = false;
                    if ($stateParams.ID != undefined && $stateParams.ID != 0) {
                        $scope.SelectedEntityCheckDiv = true;
                        $scope.SubLevelsCheckDiv = true;
                    } else {
                        $scope.SelectedEntityCheckDiv = false;
                        $scope.SubLevelsCheckDiv = false;
                    }
                    CcdetailService.GetEntityTypefromDB().then(function (getEntityTypes) {
                        $scope.GanttEntityTypes = getEntityTypes.Response;
                    });
                    CcdetailService.GetAdminSettingselemntnode('ReportSettings', 'GanttViewReport', 0).then(function (getattribtuesResult) {
                        var getattribtues = getattribtuesResult.Response;
                        if ((getattribtues != undefined || getattribtues != null) && (getattribtues.GanttViewReport != undefined || getattribtues.GanttViewReport != null)) {
                            if (getattribtues.GanttViewReport.Attributes.Attribute.length > 0) {
                                $scope.Ganttoptions = getattribtues.GanttViewReport.Attributes.Attribute;
                            } else if (getattribtues.GanttViewReport.Attributes.Attribute.Id != "") {
                                $scope.Ganttoptionsdata = [];
                                $scope.Ganttoptionsdata.push({
                                    "Id": getattribtues.GanttViewReport.Attributes.Attribute.Id,
                                    "Field": getattribtues.GanttViewReport.Attributes.Attribute.Field,
                                    "DisplayName": getattribtues.GanttViewReport.Attributes.Attribute.DisplayName
                                });
                                $scope.Ganttoptions = $scope.Ganttoptionsdata;
                            }
                        }
                    });
                } else {
                    $scope.ReportEdit = false;
                    $scope.ReportGenerate = false;
                    $scope.GanttReportDiv = false;
                }
            }
        }
        $scope.changeEntityType = function () {
            if ($scope.GanttEntityType.length > 0) {
                $scope.SelectedEntityCheckDiv = false;
                $scope.SubLevelsCheckDiv = false;
                $scope.GanttSelectedEntityCheck = "";
                $scope.GanttAllSubLevelsCheck = "";
            } else if ($stateParams.ID != undefined && $stateParams.ID != 0) {
                $scope.SelectedEntityCheckDiv = true;
                $scope.SubLevelsCheckDiv = true;
            }
        };
        $scope.ChangeEntitylevelCheck = function () {
            if ($scope.GanttSelectedEntityCheck == true) {
                $scope.GanttAllSubLevelsCheck = false;
            }
        };
        $scope.ChangeSubLevelsCheck = function () {
            if ($scope.GanttAllSubLevelsCheck == true) {
                $scope.GanttSelectedEntityCheck = false;
            }
        };
        //$scope.AddDefaultEndDate = function (startdate) {
        //    $("#EntityMetadata").addClass('notvalidate');
        //    var startDate = new Date.create(startdate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
        //    if (startDate == null) {
        //        $scope.Ganttenddate = null
        //    } else {
        //        $scope.Ganttenddate = dateFormat(startDate.addDays(7), $scope.DefaultSettings.DateFormat);
        //    }
        //};
        $scope.CustomChangeEntitylevelCheck = function () {
            if ($scope.CustomEntityCheck == true) {
                $scope.CustomSubLevelsCheck = false;
            }
        };
        $scope.CustomChangeSubLevelsCheck = function () {
            if ($scope.CustomSubLevelsCheck == true) {
                $scope.CustomEntityCheck = false;
            }
        };
        $scope.ChangeEntitylevelCheckStr = function () {
            if ($scope.GanttSelectedEntityCheckStr == true) {
                $scope.GanttAllSubLevelsCheckStr = false;
            }
        };
        $scope.ChangeSubLevelsCheckStr = function () {
            if ($scope.GanttAllSubLevelsCheckStr == true) {
                $scope.GanttSelectedEntityCheckStr = false;
            }
        };
        $scope.hideReportPopup = function () {
            if ($scope.selectvalue.contains('F_')) {
                var reportid = $scope.selectvalue.split('F_')[1];
                var mypage = "reports/FinancialReport.html?id=" + parseInt(reportid) + "&session=" + $cookies["Session"];
                var myname = "reports/FinancialReport.html?id=" + parseInt(reportid) + "&session=" + $cookies["Session"];
                var w = 1200;
                var h = 800
                var winprops = 'height=' + h + ',width=' + w + ',top=0,left=50,scrollbars=yes,resizable'
                var win = window.open(mypage, myname, winprops)
            } else if ($scope.selectvalue == -200) {
                $scope.CustomReportDiv = false;
                $scope.showCustomEntityCheck = false;
                $scope.showCustomSubLevels = false;
                $scope.CustomEntityCheck = "";
                $scope.CustomSubLevelsCheck = "";
                $scope.ProcessReport = "Structure Report";
                $("#ReportPageModel").modal("hide");
                $('#loadingReportPageModel').modal("show");
                var ExpandingEntityIDStr = 0;
                var IncludeChildrenStr = true;
                if ($stateParams.ID != undefined && $stateParams.ID != 0 && ($scope.GanttSelectedEntityCheckStr == true)) {
                    ExpandingEntityIDStr = $stateParams.ID;
                }
                if ((FilterID == 0) && (ExpandingEntityIDStr > 0) && ($scope.GanttSelectedEntityCheckStr == "") && ($scope.GanttAllSubLevelsCheckStr == "")) {
                    ExpandingEntityIDStr = 0;
                    IncludeChildrenStr = true;
                }
                if ((FilterID > 0) && (ExpandingEntityIDStr > 0) && ($scope.GanttSelectedEntityCheckStr == "") && ($scope.GanttAllSubLevelsCheckStr == "")) {
                    ExpandingEntityIDStr = 0;
                    IncludeChildrenStr = false;
                } else if (FilterID > 0 || ($scope.GanttSelectedEntityCheckStr == true)) {
                    IncludeChildrenStr = false;
                }
                CcdetailService.GetStrucuralRptDetail(($scope.chkFinancialDetl != undefined ? $scope.chkFinancialDetl : false), ($scope.chkMetadataDetl != undefined ? $scope.chkMetadataDetl : false), ($scope.chkTaskDetl != undefined ? $scope.chkTaskDetl : false), ($scope.chkMemberDetl != undefined ? $scope.chkMemberDetl : false), ExpandingEntityIDStr, ($scope.GanttAllSubLevelsCheckStr != undefined ? $scope.GanttAllSubLevelsCheckStr : false)).then(function (RptResult) {
                    if (RptResult.StatusCode == 200 && RptResult.Response != null) {
                        var a = document.createElement('a'),
							fileid = RptResult.Response,
							extn = '.xlsx';
                        var filename = 'StructuralReport.xlsx';
                        a.href = 'DownloadReport.aspx?FileID=' + fileid + '&FileFriendlyName=' + filename + '&Ext=' + extn + '';
                        a.download = fileid + extn;
                        document.body.appendChild(a);
                        $('#loadingReportPageModel').modal("hide");
                        a.click();
                    }
                });
            } else {
                if ($scope.selectvalue == -100 || $scope.selectvalue == -101) {
                    var GetActivityListrepose = [];
                    if ($scope.GanttstartDate.toString().length > 0 && $scope.Ganttenddate.toString().length > 0) {
                        var sdate = new Date.create($scope.GanttstartDate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
                        var edate = new Date.create($scope.Ganttenddate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
                        var diffval = ((parseInt(new Date.create(edate.toString('dd/MM/yyyy')).getTime()) - parseInt((new Date.create(sdate.toString('dd/MM/yyyy')).getTime()))));
                        if (diffval < 0) {
                            $scope.GanttstartDate = "";
                            $scope.Ganttenddate = "";
                            bootbox.alert($translate.instant('LanguageContents.Res_4767.Caption'));
                            return false;
                        }
                    } else if ($scope.GanttstartDate.toString().length > 0 || $scope.Ganttenddate.toString().length > 0) {
                        $scope.GanttstartDate = "";
                        $scope.Ganttenddate = "";
                        bootbox.alert($translate.instant('LanguageContents.Res_4578.Caption'));
                        return false;
                    }
                    var ExpandingEntityID = 0;
                    var IncludeChildren = true;
                    if ($stateParams.ID != undefined && $stateParams.ID != 0) {
                        ExpandingEntityID = $stateParams.ID;
                    }
                    if ((FilterID == 0) && (ExpandingEntityID > 0) && ($scope.GanttSelectedEntityCheck == "") && ($scope.GanttAllSubLevelsCheck == "")) {
                        ExpandingEntityID = 0;
                        IncludeChildren = true;
                    }
                    if ((FilterID > 0) && (ExpandingEntityID > 0) && ($scope.GanttSelectedEntityCheck == "") && ($scope.GanttAllSubLevelsCheck == "")) {
                        ExpandingEntityID = 0;
                        IncludeChildren = false;
                    } else if (FilterID > 0 || ($scope.GanttSelectedEntityCheck == true)) {
                        IncludeChildren = false;
                    }
                    $("#ReportPageModel").modal("hide");
                    $('#loadingReportPageModel').modal("show");
                    $scope.ProcessReport = "Gantt View Report";
                    var GetCoststListrepose = [];
                    var IsMonthlyGantt = $scope.selectvalue == -100 ? false : true;
                    var Node3 = {};
                    Node3.FilterID = FilterID;
                    Node3.SortOrderColumn = "null";
                    Node3.IsDesc = false;
                    Node3.IncludeChildren = IncludeChildren;
                    Node3.EntityID = $scope.costcentreidforreport;
                    Node3.Level = 100;
                    Node3.ExpandingEntityID = ExpandingEntityID;
                    Node3.IsMonthly = IsMonthlyGantt;
                    Node3.IDArr = GlobalChildNode;
                    Node3.FilterAttributes = FilterObj;
                    Node3.EntityTypeArr = $scope.GanttEntityType;
                    Node3.OptionAttributeArr = $scope.Ganttoption;
                    Node3.GanttstartDate = $scope.GanttstartDate;
                    Node3.Ganttenddate = $scope.Ganttenddate;
                    CcdetailService.CostCentreDetailReportSystem(Node3).then(function (GetCoststList3) {
                        if (GetCoststList3.Response != null) {
                            GetCoststListrepose = GetCoststList3.Response;
                            var a = document.createElement('a'),
								fileid = GetCoststListrepose,
								extn = '.xlsx';
                            var datetimetdy = ConvertDateToString(new Date.create(), $scope.DefaultSettings.DateFormat);
                            var filename = (IsMonthlyGantt == false ? 'Gantt-view-(Quarterly)-' : 'Gantt-view-(Monthly)-') + datetimetdy + extn;
                            a.href = 'DownloadReport.aspx?FileID=' + fileid + '&FileFriendlyName=' + filename + '&Ext=' + extn + '';
                            a.download = (IsMonthlyGantt == false ? 'Gantt-view-(Quarterly)-' : 'Gantt-view-(Monthly)-') + datetimetdy;
                            document.body.appendChild(a);
                            $('#loadingReportPageModel').modal("hide");
                            a.click();
                        }
                    });
                } else if ($scope.selectvalue > 0) {
                    var IDLIST = new Array();
                    var ExpandingEntityID = 0;
                    var IncludeChildren = true;
                    if ($stateParams.ID != undefined && $stateParams.ID != 0) {
                        ExpandingEntityID = $stateParams.ID;
                    }
                    if ((FilterID == 0) && (ExpandingEntityID > 0) && ($scope.CustomEntityCheck == "") && ($scope.CustomSubLevelsCheck == "")) {
                        ExpandingEntityID = 0;
                        IncludeChildren = true;
                    }
                    if ((FilterID > 0) && (ExpandingEntityID > 0) && ($scope.CustomEntityCheck == "") && ($scope.CustomSubLevelsCheck == "")) {
                        ExpandingEntityID = 0;
                        IncludeChildren = false;
                    } else if (FilterID > 0 || ($scope.CustomEntityCheck == true)) {
                        IncludeChildren = false;
                    }
                    $scope.listofreportEntityID = '';
                    var Node1 = {};
                    Node1.FilterID = FilterID;
                    Node1.SortOrderColumn = "null";
                    Node1.IsDesc = false;
                    Node1.IncludeChildren = IncludeChildren;
                    Node1.EntityID = $scope.costcentreidforreport;
                    Node1.Level = 100;
                    Node1.ExpandingEntityID = ExpandingEntityID;
                    Node1.IDArr = GlobalChildNode;
                    Node1.FilterAttributes = FilterObj;
                    CcdetailService.CostCentreDetailReport(Node1).then(function (GetCostCenterActivityReportList) {
                        if (GetCostCenterActivityReportList.Response != null) {
                            if (GetCostCenterActivityReportList.Response.length > 0) {
                                for (var i = 0; i < GetCostCenterActivityReportList.Response.length; i++) {
                                    IDLIST.push(GetCostCenterActivityReportList.Response[i]);
                                }
                            }
                            $scope.listofreportEntityID = IDLIST.join(",");
                            var param = {
                                'ID': $scope.ReportValue,
                                'SID': $scope.listofreportEntityID
                            };
                            var w = 1200;
                            var h = 800
                            var winprops = 'height=' + h + ',width=' + w + ',top=0,left=50,scrollbars=yes,resizable'
                            OpenWindowWithPost("reportviewer.aspx", winprops, "NewFile", param);
                        }
                    });
                }
            }
        }
        $scope.SelectedFilterID = 0;
        $scope.SelectedFilterType = 1;
        $scope.appliedfilter = $translate.instant('LanguageContents.Res_401.Caption');
        $scope.AddEditFilter = $translate.instant('LanguageContents.Res_402.Caption');
        CcdetailService.GetFilterSettingsForDetail(5).then(function (filterSettings) {
            $scope.filterValues = filterSettings.Response;
        });

        function OpenWindowWithPost(url, windowoption, name, params) {
            var form = document.createElement("form");
            form.setAttribute("method", "post");
            form.setAttribute("action", url);
            form.setAttribute("target", name);
            for (var i in params) {
                if (params.hasOwnProperty(i)) {
                    var input = document.createElement('input');
                    input.type = 'hidden';
                    input.name = i;
                    input.value = params[i];
                    form.appendChild(input);
                }
            }
            document.body.appendChild(form);
            window.open("report.aspx", name, windowoption);
            form.submit();
            document.body.removeChild(form);
        }
        $scope.CostExpandtext = $translate.instant('LanguageContents.Res_4939.Caption');
        $scope.CostExpndClass = 'icon-caret-right';
        $scope.disableShowAll = false;
        $scope.ExpandAll = function () {
            if ($scope.disableShowAll == false) {
                $scope.disableShowAll = true;
                $scope.ListViewDetails = [{
                    data: "",
                    PageIndex: 0,
                    UniqueID: "",
                    StartRowNo: 0,
                    MaxNoofRow: 30,
                    FilterID: 0,
                    SortOrderColumn: "null",
                    IsDesc: false,
                    IncludeChildren: false,
                    EntityID: '0',
                    Level: '0',
                    IDArr: [],
                    FilterAttributes: []
                }];
                $scope.ListTemplate = "";
                $scope.GanttViewDataPageTemplate = '';
                $scope.GanttViewBlockPageTemplate = '';
                $('#TreeCtrlCss').remove();
                if ($scope.CostExpandtext == $translate.instant('LanguageContents.Res_4939.Caption')) {
                    $scope.CostExpandtext = $translate.instant('LanguageContents.Res_4940.Caption');
                    $scope.CostExpndClass = 'icon-caret-down';
                    $scope.Level = "100";
                    $scope.ExpandingEntityID = '0';
                    GetTreeCount();
                } else {
                    $scope.CostExpandtext = $translate.instant('LanguageContents.Res_4939.Caption');
                    $scope.CostExpndClass = 'icon-caret-right';
                    $scope.Level = "0";
                    $scope.ExpandingEntityID = '0';
                    GetTreeCount();
                }
                $('.ganttview-data').scrollTop(0);
                $('.list_rightblock').scrollTop(0);
            }
        }
        $scope.SelectedFilterType = 2;
        $scope.SelectedFilterID = 0;
        $scope.appliedfilter = $translate.instant('LanguageContents.Res_401.Caption');
        $scope.AddEditFilter = $translate.instant('LanguageContents.Res_402.Caption');
        CcdetailService.GetFilterSettingsForDetail(5).then(function (filterSettings) {
            $scope.filterValues = filterSettings.Response;
        });
        $scope.DetailFilterCreation = function (event) {
            if ($scope.SelectedFilterID == 0) {
                $('.FilterHolder').slideDown("slow")
                $("#costcentredetailfilter").trigger('ClearScope', $scope.SelectedFilterID);
            } else {
                $("#costcentredetailfilter").trigger('EditFilterSettingsByFilterID', [$scope.SelectedFilterID, 5, $scope.SelectedFilterType]);
                $scope.showSave = false;
                $scope.showUpdate = true;
                $('.FilterHolder').slideDown("slow")
            }
        };
        $scope.ApplyFilter = function (filterid, filtername, IsDetailFilter) {
            $scope.SelectedFilterID = filterid;
            $scope.SelectedFilterType = IsDetailFilter;
            if (filterid != 0) {
                $("#Filtersettingdiv").removeClass("btn-group open");
                $("#Filtersettingdiv").addClass("btn-group");
                $('.FilterHolder').slideUp("slow")
                $scope.AddEditFilter = $translate.instant('LanguageContents.Res_5053.Caption');
                $scope.appliedfilter = filtername;
            } else {
                $scope.appliedfilter = filtername;
                $scope.AddEditFilter = $translate.instant('LanguageContents.Res_402.Caption');
                $('.FilterHolder').slideUp("slow");
                $("#costcentredetailfilter").trigger('ClearScope', $scope.SelectedFilterID);
            }
            $("#costcentredetailfilter").trigger('applyplandetailfilter', [filterid, filtername, IsDetailFilter]);
        }
        $("#costcentredetailfilter").on('reloadccdetailfilter', function (event, typeId, filtername, filterid, filtertype) {
            $scope.SelectedFilterID = filterid;
            if (filtername != '') {
                $("#costcentredetailfilter").trigger('EditFilterSettingsByFilterID', [$scope.SelectedFilterID, 5, filtertype]);
                $scope.appliedfilter = filtername;
                $scope.AddEditFilter = $translate.instant('LanguageContents.Res_5053.Caption');
            }
            if (filtername == $translate.instant('LanguageContents.Res_401.Caption')) {
                $scope.appliedfilter = $translate.instant('LanguageContents.Res_401.Caption');
                $scope.AddEditFilter = $translate.instant('LanguageContents.Res_402.Caption');
                $scope.SelectedFilterID = 0;
                $('.FilterHolder').slideUp("slow")
                $scope.SelectedFilterType = 2;
            }
            CcdetailService.GetFilterSettingsForDetail(typeId).then(function (filterSettings) {
                $scope.filterValues = filterSettings.Response;
                $scope.atributesRelationList = [];
            });
        });
        $("#moduleContextmenu").click(function () {
            CcdetailService.GetCostCentreAssignedAmount($scope.CurrentCostcentreID).then(function (ResAssgnAmnt) {
                createsubcostcentre($scope.CurrentCostcentreID, ResAssgnAmnt.Response);
            });
        });
        $("#EntitiesTree").on("LoadCostCentreTree", function (event, EntityID, UniqueKey) {
            $scope.SelectedCostCentreCurrency.Rate = 1;
            $scope.SelectedCostCentreCurrency.ShortName = $scope.DefaultSettings.CurrencyFormat.Name;
            $scope.SelectedCostCentreCurrency.Name = $scope.DefaultSettings.CurrencyFormat.ShortName;
            $scope.SelectedCostCentreCurrency.TypeId = $scope.DefaultSettings.CurrencyFormat.Id;
            $scope.ExpandingEntityID = EntityID;
            TreeLoad(false, UniqueKey, EntityID, true);
        });

        function GetCostCentreCurrencyRateById(CostCentreID, CurrencyId) {
            CcdetailService.GetCostCentreCurrencyRateById(CostCentreID, 0, false).then(function (resCurrencyRate) {
                if (resCurrencyRate.Response != null) {
                    $scope.SelectedCostCentreCurrency.Rate = parseFloat(resCurrencyRate.Response[1]);
                    $scope.GetCurrencyList(parseInt(resCurrencyRate.Response[0]));
                } else $scope.GetCurrencyList(0);
            });
        }
        $scope.GetCurrencyList = function (CostCentreCurrencyId) {
            CcdetailService.GetCurrencyListFFsettings().then(function (CurrencyListResult) {
                if (CurrencyListResult.Response != null) {
                    $scope.CurrencyFormatsList = CurrencyListResult.Response;
                    if (CostCentreCurrencyId != 0) $scope.SelectedCostCentreCurrency.TypeId = CostCentreCurrencyId;
                    else $scope.SelectedCostCentreCurrency.TypeId = $scope.DefaultSettings.CurrencyFormat.Id;
                    $scope.SelectedCostCentreCurrency.Name = $.grep($scope.CurrencyFormatsList, function (e) {
                        return e.Id == $scope.SelectedCostCentreCurrency.TypeId;
                    })[0].Name;
                    $scope.SelectedCostCentreCurrency.ShortName = $.grep($scope.CurrencyFormatsList, function (e) {
                        return e.Id == $scope.SelectedCostCentreCurrency.TypeId;
                    })[0].ShortName;
                }
            });
        }
        $scope.ChangeCostCentreCurrencyType = function (currencyId) {
            $scope.SelectedCostCentreCurrency.Name = $.grep($scope.CurrencyFormatsList, function (e) {
                return e.Id == currencyId;
            })[0].Name;
            $scope.SelectedCostCentreCurrency.ShortName = $.grep($scope.CurrencyFormatsList, function (e) {
                return e.Id == currencyId;
            })[0].ShortName;
            $("#CostCentreCurrencyTypediv").removeClass("btn-group open");
            $("#CostCentreCurrencyTypediv").addClass("btn-group");
            CcdetailService.GetCostCentreCurrencyRateById(0, currencyId, false).then(function (resCurrencyRate) {
                if (resCurrencyRate.Response != null) {
                    $scope.SelectedCostCentreCurrency.Rate = parseFloat(resCurrencyRate.Response[1]);
                    $scope.SelectedCostCentreCurrency.TypeId = parseInt(resCurrencyRate.Response[0]);
                    $scope.$broadcast('onTreePageCreation', ["", 0, ""]);
                }
            });
        }

        function createsubcostcentre(ccID, amount) {
            var modalInstance = $modal.open({
                templateUrl: 'views/mui/planningtool/costcentre/subcostcentrecreation.html',
                controller: "mui.planningtool.costcentre.subcostcentrecreationCtrl",
                resolve: {
                    params: function () {
                        return {
                            ccID: ccID,
                            amount: amount,
                            UniqueID: $window.GlobalUniquekey
                        };
                    }
                },
                scope: $scope,
                windowClass: 'subcostcentremodal popup-widthL',
                backdrop: "static"
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () { });
        }
    }
    app.controller("mui.planningtool.costcentre.detailCtrl", ['$scope', '$timeout', '$http', '$compile', '$resource', '$stateParams', '$window', '$location', '$templateCache', '$cookies', '$translate', 'CcdetailService', '$modal', muiplanningtoolcostcentredetailCtrl]);
})(angular, app);
///#source 1 1 /app/controllers/mui/planningtool/costcentre/SubCostCentreCreation-controller.js
(function (ng, app) {
    "use strict";

    function muisubcostcentrecreationCtrl($scope, $location, $resource, $timeout, $cookies, $window, $compile, $translate, SubcostcentrecreationService, $modalInstance, params) {
        $scope.GetNonBusinessDaysforDatePicker();
        $("#rootLevelEntity").on("onRootCostCentreCreation", function (event, id) {
            $("#btnsubWizardFinish").removeAttr('disabled');
            $('#MysubWizard').wizard('stepLoaded');
            window["tid_subwizard_steps_all_complete_count"] = 0;
            window["tid_subwizard_steps_all_complete"] = setInterval(function () {
                KeepAllStepsMarkedComplete();
            }, 25);
            $scope.ClearModelObject($scope.fields);
            $('#btnsubWizardFinish').hide();
            $('#btnsubWizardNext').show();
            $('#btnsubWizardPrev').hide();
            $timeout(function () {
                HideAttributeToAttributeRelationsOnPageLoad();
            });
            $scope.UserimageNewTime = new Date.create().getTime().toString();
        });
        $scope.SelectedCostCentreCurrency = { "TypeId": 0, "Rate": 1, "ShortName": '' };
        $scope.EnableDisableControlsHolder = {};
        $scope.ParentID = 0;
        $scope.AvailableAssignAmount = 0;
        $scope.treeSourcesObj = [];
        $scope.treeSources = {};
        $scope.Dropdown = [];
        $scope.settreeSources = function () {
            var keys = [];
            angular.forEach($scope.treeSources, function (key) {
                keys.push(key);
                $scope.treeSourcesObj = keys;
            });
        }
        $scope.treeNodeSelectedHolder = new Array();
        var apple_selected, tree, treedata_avm, treedata_geography;
        $scope.my_tree_handler = function (branch, parentArr) {
            $scope.fields["Tree_" + branch.AttributeId].splice(0, $scope.fields["Tree_" + branch.AttributeId].length);
            var _ref;
            $scope.output = "You selected: " + branch.Caption;
            if ((_ref = branch.data) != null ? _ref.description : void 0) {
                return $scope.output += '(' + branch.data.description + ')';
            }
            if (branch.ischecked == true) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length == 0) {
                    $scope.treeNodeSelectedHolder.push(branch);
                }
                for (var i = 0, parent; parent = parentArr[i++];) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                        return e.AttributeId == parent.AttributeId && e.id == parent.id;
                    });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolder.push(parent);
                    }
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(branch), 1);
                    if (branch.Children.length > 0) {
                        RemoveRecursiveChildTreenode(branch.Children);
                    }
                }
            }
            if ($scope.treesrcdirec["Attr_" + branch.AttributeId].length > 0) {
                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + branch.AttributeId])) {
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = true;
                } else $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            } else {
                $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }
            GetTreeCheckedNodes(parentArr, branch.AttributeId);
            $scope.ShowHideAttributeToAttributeRelations(branch.AttributeId, 0, 0, 7);
        };

        function GetTreeCheckedNodes(treeobj, attrID) {
            for (var i = 0, node; node = treeobj[i++];) {
                if (node.ischecked == true) {
                    $scope.fields["Tree_" + attrID].push(node.id);
                }
                if (node.Children.length > 0) GetTreeCheckedNodes(node.Children, attrID);
            }
        }

        function IsNotEmptyTree(treeObj) {
            var flag = false;
            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    flag = true;
                    return flag;
                }
            }
            return flag;
        }

        function RemoveRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == child.AttributeId && e.id == child.id;
                });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(child), 1);
                    if (child.Children.length > 0) {
                        RemoveRecursiveChildTreenode(child.Children);
                    }
                }
            }
        }
        $scope.treesrcdirec = {};
        $scope.my_tree = tree = {};
        $scope.ClearModelObject = function (ModelObject) {
            for (var variable in ModelObject) {
                if (typeof ModelObject[variable] === "string") {
                    if (variable !== "ListSingleSelection_69") {
                        ModelObject[variable] = "";
                    }
                } else if (typeof ModelObject[variable] === "number") {
                    ModelObject[variable] = null;
                } else if (Array.isArray(ModelObject[variable])) {
                    ModelObject[variable] = [];
                } else if (typeof ModelObject[variable] === "object") {
                    ModelObject[variable] = {};
                }
            }
            $scope.MemberLists = [];
            $scope.ngfinancialAmount = "0";
            // $scope.$apply();
        }
        var totalWizardSteps = $('#MysubWizard').wizard('totnumsteps').totstep;
        $('#btnsubWizardFinish').hide();
        $('#btnsubWizardNext').show();
        $('#btnsubWizardPrev').hide();
        window["tid_subwizard_steps_all_complete_count"] = 0;
        window["tid_subwizard_steps_all_complete"] = setInterval(function () {
            KeepAllStepsMarkedComplete();
        }, 25);
        $scope.changeTab = function () {
            window["tid_subwizard_steps_all_complete_count"] = 0;
            window["tid_subwizard_steps_all_complete"] = setInterval(function () {
                KeepAllStepsMarkedComplete();
            }, 25);
            var currentWizardStep = $('#MysubWizard').wizard('selectedItem').step;
            var totalWizardSteps = $('#MysubWizard').wizard('totnumsteps').totstep;
            if (currentWizardStep === 1) {
                $('#btnsubWizardFinish').hide();
                $('#btnsubWizardNext').show();
                $('#btnsubWizardPrev').hide();
            } else if (currentWizardStep === totalWizardSteps) {
                $('#btnsubWizardFinish').show();
                $('#btnsubWizardNext').hide();
                $('#btnsubWizardPrev').show();
            } else {
                $('#ngfinancialAmount').autoNumeric('init', $scope.DecimalSettings['FinancialAutoNumeric']);
                $('[id=ngsubfinancialAmount]').css('text-align', 'right');
                setTimeout(function () {
                    $('[id=ngsubfinancialAmount]:enabled:visible:first').focus().select()
                }, 100);
                $('#btnsubWizardFinish').hide();
                $('#btnsubWizardNext').show();
                $('#btnsubWizardPrev').show();
            }
        }
        $scope.changeCostcentreTab2 = function (e) {
            $("#btnTemp").click();
            $("#SubCostCentreMetadata").removeClass('notvalidate');
            if ($("#SubCostCentreMetadata .error").length > 0) {
                e.stopImmediatePropagation();
                e.stopPropagation();
                return false;
            }
        }

        function KeepAllStepsMarkedComplete() {
            $("#MysubWizard ul.steps").find("li").addClass("complete");
            $("#MysubWizard ul.steps").find("span.badge").addClass("badge-success");
            window["tid_subwizard_steps_all_complete_count"]++;
            if (window["tid_subwizard_steps_all_complete_count"] >= 20) {
                clearInterval(window["tid_subwizard_steps_all_complete"]);
            }
        }
        $scope.changePrevTab = function () {
            window["tid_subwizard_steps_all_complete_count"] = 0;
            window["tid_subwizard_steps_all_complete"] = setInterval(function () {
                KeepAllStepsMarkedComplete();
            }, 25);
            $('#MysubWizard').wizard('previous');
            var currentWizardStep = $('#MysubWizard').wizard('selectedItem').step;
            var totalWizardSteps = $('#MysubWizard').wizard('totnumsteps').totstep;
            if (currentWizardStep === 1) {
                $('#btnsubWizardPrev').hide();
            }
            if (currentWizardStep < totalWizardSteps) {
                $('#ngfinancialAmount').autoNumeric('init', $scope.DecimalSettings['FinancialAutoNumeric']);
                $('[id=ngsubfinancialAmount]').css('text-align', 'right');
                setTimeout(function () {
                    $('[id=ngsubfinancialAmount]:enabled:visible:first').focus().select()
                }, 100);
                $('#btnsubWizardFinish').hide();
                $('#btnsubWizardNext').show();
            } else {
                $('#btnsubWizardFinish').show();
                $('#btnsubWizardNext').hide();
            }
        }
        $scope.status = true;
        $scope.changenexttab = function () {
            window["tid_subwizard_steps_all_complete_count"] = 0;
            window["tid_subwizard_steps_all_complete"] = setInterval(function () {
                KeepAllStepsMarkedComplete();
            }, 25);
            $("#btnTemp").click();
            if ($("#SubCostCentreMetadata .error").length > 0) {
                return false;
            }
            $('#MysubWizard').wizard('next', '');
            $('#ngfinancialAmount').autoNumeric('init', $scope.DecimalSettings['FinancialAutoNumeric']);
            $('[id=ngsubfinancialAmount]').css('text-align', 'right');
            setTimeout(function () {
                $('[id=ngsubfinancialAmount]:enabled:visible:first').focus().select()
            }, 100);
            var currentWizardStep = $('#MysubWizard').wizard('selectedItem').step
            var totalWizardSteps = $('#MysubWizard').wizard('totnumsteps').totstep;
            if (currentWizardStep > 1) {
                $('#btnsubWizardPrev').show();
            }
            if (currentWizardStep === totalWizardSteps) {
                $('#btnsubWizardFinish').show();
                $('#btnsubWizardNext').hide();
            } else {
                $('#btnsubWizardFinish').hide();
                $('#btnsubWizardNext').show();
            }
        }
        var ownername = $cookies['Username'];
        var ownerid = $cookies['UserId'];
        $scope.OwnerName = ownername;
        $scope.OwnerID = ownerid;
        $scope.ownerEmail = $cookies['UserEmail'];
        $scope.AutoCompleteSelectedObj = [];
        $scope.AutoCompleteSelectedUserObj = {};
        $scope.AutoCompleteSelectedUserObj.UserSelection = [];
        $scope.atributesRelationList = [];
        $scope.OwnerList = [];
        $scope.treePreviewObj = {};
        $scope.owner = {};

        SubcostcentrecreationService.GetUserById(parseInt($scope.OwnerID, 10)).then(function (Getowner) {
            $scope.owner = Getowner.Response;
            $scope.QuickInfo1AttributeCaption = $scope.owner.QuickInfo1AttributeCaption;
            $scope.QuickInfo2AttributeCaption = $scope.owner.QuickInfo2AttributeCaption;
            $scope.OwnerList.push({
                "Roleid": 1,
                "RoleName": "Owner",
                "UserEmail": $scope.ownerEmail,
                "DepartmentName": $scope.owner.Designation,
                "Title": $scope.owner.Title,
                "Userid": parseInt($scope.OwnerID, 10),
                "UserName": $scope.OwnerName,
                "IsInherited": '0',
                "InheritedFromEntityid": '0',
                "QuickInfo1": $scope.owner.QuickInfo1,
                "QuickInfo2": $scope.owner.QuickInfo2
            });
        });
        SubcostcentrecreationService.GetEntityStatus(5, false, 0).then(function (getentitytypestatus) {
            $scope.EntityStatusResult = getentitytypestatus.Response;
        });
        $scope.UserLists = [];
        $scope.MemberLists = [];
        $scope.contrls = '';
        GetEntityTypeRoleAccess(5);

        function GetEntityTypeRoleAccess(rootID) {
            SubcostcentrecreationService.GetEntityTypeRoleAccess(rootID).then(function (role) {
                $scope.Roles = role.Response;
            });
        }
        var Version = 1;
        $scope.wizard = {
            newval: ''
        };
        $scope.dyn_Cont = '';
        $scope.items = [];
        $scope.lastSubmit = [];
        $scope.addNew = function () {
            $scope.items.push({
                startDate: [],
                endDate: [],
                comment: '',
                sortorder: 0
            });
        };
        $scope.submitOne = function (item) {
            $scope.lastSubmit = angular.copy(item);
        };
        $scope.deleteOne = function (item) {
            $scope.lastSubmit.splice($.inArray(item, $scope.lastSubmit), 1);
            $scope.items.splice($.inArray(item, $scope.items), 1);
        };
        $scope.submitAll = function () {
            $scope.lastSubmit = angular.copy($scope.items);
        }
        $scope.entityObjectString = '{';
        $scope.fields = {
            usersID: ''
        };
        $scope.dynamicEntityValuesHolder = {};
        $scope.fieldKeys = [];
        $scope.OptionObj = {};
        $scope.fieldoptions = [];
        $scope.setoptions = function () {
            var keys = [];
            angular.forEach($scope.OptionObj, function (key) {
                keys.push(key);
                $scope.fieldoptions = keys;
            });
        }
        $scope.setFieldKeys = function () {
            var keys = [];
            angular.forEach($scope.fields, function (key) {
                keys.push(key);
                $scope.fieldKeys = keys;
            });
        }
        $scope.Entityamountcurrencytypeitem = [];
        $scope.optionsLists = [];
        $scope.AttributeData = [];
        $scope.entityName = "";
        $scope.AutoCompleteSelectedUserObj = { "UserSelection": [] };
        $scope.CostcenteAssignedAmount = 0;
        $scope.count = 1;
        $scope.addUsers = function () {
            var membervalues = $.grep($scope.Roles, function (e) {
                return e.ID == parseInt($scope.fields.userRoles, 10)
            })[0];
            if (membervalues != undefined) {
                $scope.MemberLists.push({
                    "TID": $scope.count,
                    "UserEmail": $scope.AutoCompleteSelectedUserObj.UserSelection.Email,
                    "DepartmentName": $scope.AutoCompleteSelectedUserObj.UserSelection.Designation,
                    "Title": $scope.AutoCompleteSelectedUserObj.UserSelection.Title,
                    "Roleid": parseInt(membervalues.ID, 10),
                    "RoleName": membervalues.Caption,
                    "Userid": parseInt($scope.AutoCompleteSelectedUserObj.UserSelection.Id, 10),
                    "UserName": $scope.AutoCompleteSelectedUserObj.UserSelection.FirstName + ' ' + $scope.AutoCompleteSelectedUserObj.UserSelection.LastName,
                    "IsInherited": '0',
                    "InheritedFromEntityid": '0',
                    "QuickInfo1": $scope.AutoCompleteSelectedUserObj.UserSelection.QuickInfo1,
                    "QuickInfo2": $scope.AutoCompleteSelectedUserObj.UserSelection.QuickInfo2
                });
                $scope.count = $scope.count + 1;
            }
            $scope.fields.usersID = '';
            $scope.AutoCompleteSelectedUserObj = {};
            $scope.AutoCompleteSelectedUserObj.UserSelection = [];
            $scope.AutoCompleteSelectedObj = [];
            $('#ddlsubrole').select2('val', '');
        };
        $scope.deleteOptions = function (item) {
            $scope.MemberLists.splice($.inArray(item.Userid, $scope.MemberLists), 1);
        };
        if ($('#ngsubfinancialAmount').length != 0) {
            $('#ngfinancialAmount').autoNumeric('init', $scope.DecimalSettings['FinancialAutoNumeric']);
            $('[id=ngsubfinancialAmount]').css('text-align', 'right');
            setTimeout(function () {
                $('[id=ngsubfinancialAmount]:enabled:visible:first').focus().select()
            }, 100);
        }
        $scope.saveCostcentreEntity = function () {
            $scope.UniqueKey = params.UniqueID;
            if ($scope.ParentID > 0) if (parseInt($('#ngsubfinancialAmount').val().replace(/\s/g, ''), 10) > $scope.AvailableAssignAmount) {
                bootbox.alert("Assigned amount is greater than Available amount " + $scope.AvailableAssignAmount);
                return false;
            }
            if ($scope.SelectedCostCentreCurrency.TypeId == 0 || $scope.SelectedCostCentreCurrency.TypeId == "") {
                bootbox.alert("Please select currency type.");
                return false;
            }
            $("#btnsubWizardFinish").attr('disabled', 'disabled');
            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                    for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                        if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined && $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "") {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": [$scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id],
                                "Level": $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 7) {
                    var treenodes = [];
                    treenodes = $.grep($scope.treeNodeSelectedHolder, function (e) {
                        return e.AttributeId == $scope.atributesRelationList[i].AttributeID;
                    });
                    for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": [parseInt(nodeval.id, 10)],
                            "Level": parseInt(nodeval.Level, 10)
                        });
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                    for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                        var levelCount = $scope.atributesRelationList[i].Levels.length;
                        if (levelCount == 1) {
                            for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].id],
                                    "Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].Level
                                });
                            }
                        } else {
                            if ($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                                if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
                                    for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
                                        $scope.AttributeData.push({
                                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                            "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                            "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].id],
                                            "Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].Level
                                        });
                                    }
                                } else {
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id],
                                        "Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level
                                    });
                                }
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                    if ($scope.atributesRelationList[i].IsSpecial == true) {
                        if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": parseInt($scope.OwnerList[0].Userid, 10),
                                "Level": 0
                            });
                        }
                    } else if ($scope.atributesRelationList[i].IsSpecial == false) {
                        var fiscalyear = $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined > 0 ? $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] : 0;
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": parseInt(fiscalyear, 10),
                            "Level": 0
                        });
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 33) {
                    if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined && $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != "") {
                        var value = $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID];
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": (value != "" || value != undefined) ? parseInt(value, 10) : 0,
                            "Level": 0,
                            "Value": "-1"
                        });
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime) {
                    var MyDate = new Date.create();
                    var MyDateString;
                    if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                        if ($scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] == "" || $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] == undefined || $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] == null) {
                            MyDate.setDate(MyDate.getDate());
                            MyDateString = null;
                        } else {
                            MyDateString = $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID].toString('MM/dd/yyyy');
                        }
                    } else {
                        MyDateString = "";
                    }
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": MyDateString,
                        "Level": 0
                    });
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                    if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Name) $scope.entityName = $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID];
                    else {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID].toString(),
                            "Level": 0
                        });
                    }
                } else if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.AssignedAmount) {
                    $scope.CostcenteAssignedAmount = 0;
                    if (isNaN(parseFloat($('#ngsubfinancialAmount').val(), 10)) == false && parseInt($('#ngsubfinancialAmount').val(), 10) != 0) $scope.CostcenteAssignedAmount = parseFloat((parseFloat($('#ngsubfinancialAmount').val().replace(/\s/g, ''), 10)) / $scope.SelectedCostCentreCurrency.Rate);
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": $scope.CostcenteAssignedAmount,
                        "Level": 0
                    });
                } else if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.AvailableAssignedAmount) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": $scope.CostcenteAssignedAmount,
                        "Level": 0
                    });
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": $scope.fields['TextMultiLine_' + $scope.atributesRelationList[i].AttributeID].toString(),
                        "Level": 0
                    });
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                    if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                        if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID].length > 0) {
                            var multiselectiObject = $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID];
                            for (var k = 0; k < multiselectiObject.length; k++) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": parseInt(multiselectiObject[k], 10),
                                    "Level": 0
                                });
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID] == undefined ? 0 : $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID].toString(),
                        "Level": 0
                    });
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 17) {
                    if ($scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                        for (var j = 0; j < $scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID].length; j++) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": parseInt($scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID][j], 10),
                                "Level": 0,
                                "Value": "-1"
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 19) {
                    if ($scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] == "") {
                        $scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = "0";
                    }
                    $scope.Entityamountcurrencytypeitem.push({
                        amount: parseFloat($scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID].replace(/ /g, '')),
                        currencytype: $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID],
                        Attributeid: $scope.atributesRelationList[i].AttributeID
                    });
                }
            }
            var saveCostcentre = {};
            saveCostcentre.ParentId = $scope.ParentID;
            saveCostcentre.TypeId = 5;
            saveCostcentre.Name = $scope.entityName;
            saveCostcentre.AssignedAmount = $scope.CostcenteAssignedAmount;
            saveCostcentre.EntityMembers = [];
            saveCostcentre.Periods = [];
            saveCostcentre.CurrencyFormat = $scope.SelectedCostCentreCurrency.TypeId;
            saveCostcentre.Entityamountcurrencytype = [];
            for (var m = 0; m < $scope.items.length; m++) {
                $scope.items[m].startDate = ($scope.items[m].startDate.length != 0 ? ConvertDateToString($scope.items[m].startDate) : '');
                $scope.items[m].endDate = ($scope.items[m].startDate.length != 0 ? ConvertDateToString($scope.items[m].endDate) : '');
            }
            saveCostcentre.Periods.push($scope.items);
            saveCostcentre.AttributeData = $scope.AttributeData;
            saveCostcentre.EntityMembers = $scope.MemberLists;
            $scope.curram = [];
            for (var m = 0; m < $scope.Entityamountcurrencytypeitem.length; m++) {
                $scope.curram.push({
                    amount: $scope.Entityamountcurrencytypeitem[m].amount,
                    currencytype: $scope.Entityamountcurrencytypeitem[m].currencytype,
                    Attributeid: $scope.Entityamountcurrencytypeitem[m].Attributeid
                });
            }
            saveCostcentre.Entityamountcurrencytype.push($scope.curram);
            SubcostcentrecreationService.CreateCostcentre(saveCostcentre).then(function (costcentreCreationResult) {
                if (costcentreCreationResult.StatusCode == 405 || costcentreCreationResult.Response == 0) {
                    NotifyError($translate.instant('LanguageContents.Res_4278.Caption'));
                    $scope.Entityamountcurrencytypeitem = [];
                    $scope.closeSubcostcentrecreationPopup();
                } else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4175.Caption'));
                    $scope.Entityamountcurrencytypeitem = [];
                    $scope.MemberLists = [];
                    var IDList = new Array();
                    IDList.push(costcentreCreationResult.Response);
                    $window.ListofEntityID = IDList;
                    var TrackID = CreateHisory(IDList);
                    $scope.Activity.IsActivitySectionLoad = true;
                    $("#EntitiesTree").trigger("LoadCostCentreTree", [costcentreCreationResult.Response, $scope.UniqueKey]);
                    $location.path('/mui/planningtool/costcentre/detail/section/' + costcentreCreationResult.Response + '/overview');
                    setTimeout(function () {
                        $scope.closeSubcostcentrecreationPopup();
                    }, 100);
                }
            });
        }
        $scope.UniqueKey = "";
        $(window).on("GloblaCCUniqueKeyAccess", function (event, UniqueKey) {
            $scope.UniqueKey = UniqueKey;
        });
        $scope.addRootEntity = function () {
            $scope.EnableAdd = true;
            $scope.EnableUpdate = false;
            $scope.step = 0;
            $scope.EnableOptionUpdate = false;
            $scope.EnableOptionAdd = true;
        };
        $scope.treelevels = [];
        $scope.tree = {};
        $scope.Options = {};
        $scope.OptionObj = {};
        $scope.listAttriToAttriResult = [];
        $scope.ShowHideAttributeOnRelation = {};
        subcostcentremodalopen(params.ccID, params.amount);

        function subcostcentremodalopen(ParentID, ParentAvailableAmt) {
            $scope.ParentID = ParentID;
            $scope.AvailableAssignAmount = ParentAvailableAmt;
            $scope.dyn_Cont = '';
            $("#btnsubWizardFinish").removeAttr('disabled');
            $('#MysubWizard').wizard('stepLoaded');
            window["tid_subwizard_steps_all_complete_count"] = 0;
            window["tid_subwizard_steps_all_complete"] = setInterval(function () {
                KeepAllStepsMarkedComplete();
            }, 25);
            $scope.ClearModelObject($scope.fields);
            delete $scope.fields.regex;
            $('#btnsubWizardFinish').hide();
            $('#btnsubWizardNext').show();
            $('#btnsubWizardPrev').hide();
            $scope.treePreviewObj = {};
            setTimeout(function () {
                loadAttributeMetadata();
            }, 100);
            if ($scope.AttributeData.length > 0) $scope.AttributeData.splice(0, $scope.AttributeData.length);
        }

        function loadAttributeMetadata() {
            $scope.SelectedCostCentreCurrency.TypeId = $scope.DefaultSettings.CurrencyFormat.Id;
            SubcostcentrecreationService.GetAttributeToAttributeRelationsByIDForEntity(5).then(function (entityAttrToAttrRelation) {
                $scope.listAttriToAttriResult = entityAttrToAttrRelation.Response;
            });
            SubcostcentrecreationService.GetEntityTypeAttributeRelationWithLevelsByID(5, $scope.ParentID).then(function (entityAttributesRelation) {
                $scope.atributesRelationList = entityAttributesRelation.Response;
                for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                    if ($scope.atributesRelationList[i].IsHelptextEnabled == true) {
                        var attribteHelptext = $scope.atributesRelationList[i].HelptextDecsription == null ? "-" : $scope.atributesRelationList[i].HelptextDecsription.trim() == "" ? "-" : $scope.atributesRelationList[i].HelptextDecsription;
                        var qtip = 'my-qtip2 qtip-content="' + attribteHelptext + '"';
                    }
                    else
                        var qtip = "";
                    if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        if ($scope.atributesRelationList[i].IsSpecial == true) {
                            if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
                                //$scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " : </label><div class=\"controls\"> <input type=\"text\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" dirownernameautopopulate placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                                $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " : </label><div class=\"controls\"> <input type=\"text\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" dirownernameautopopulate placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.OwnerList[0].UserName;
                                $scope.setFieldKeys();
                            }
                        } else if ($scope.atributesRelationList[i].IsSpecial == false) {
                            $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                            //$scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " : </label><div class=\"controls\"> <select ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ui-select2 ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ",0,0,3)\"> <option value=\"\"> Select " + $scope.atributesRelationList[i].AttributeCaption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \"value=\"{{ndata.Id}}\">{{ndata.Caption}}</option> </select></div></div>";
                            $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " : </label><div class=\"controls\"> <select ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ui-select2 ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ",0,0,3)\"> <option value=\"\"> Select " + $scope.atributesRelationList[i].AttributeCaption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \"value=\"{{ndata.Id}}\">{{ndata.Caption}}</option> </select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                            $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = "";
                            $scope.setFieldKeys();
                        }
                    }
                    else if ($scope.atributesRelationList[i].AttributeTypeID == 33) {
                        $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $translate.instant('LanguageContents.Res_5613.Caption') + "</label><div class=\"controls bgDropdown\"><select ui-select2=\"select2Config\" id=\"OverAllEntityStats\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" name=\"OverAllEntityStatus\" data-placeholder=\"Entity Status\">";
                        $scope.dyn_Cont += "<option value=\"0\"> Select " + $translate.instant('LanguageContents.Res_5613.Caption') + "</option>";
                        $scope.dyn_Cont += "<option ng-repeat=\"Aitem in EntityStatusResult\" value=\"{{Aitem.ID}}\">{{Aitem.StatusOptions}}</option>";
                        $scope.dyn_Cont += "</select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                        $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = 0;

                        $scope.select2Config = {
                            formatResult: $scope.overallstatusformat,
                            formatSelection: $scope.overallstatusformat
                        };
                    }
                    else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        if ($scope.atributesRelationList[i].AttributeID != 70) {
                            // $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " : </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"></div></div>";
                            $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " : </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                            $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                            $scope.setFieldKeys();
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        //$scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group control-group-textarea\"><label class=\"control-label\" for=\"fields.TextMultiLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><textarea ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"small-textarea\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" name=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\" rows=\"3\"></textarea></div></div>";
                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group control-group-textarea\"><label class=\"control-label\" for=\"fields.TextMultiLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><textarea ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"small-textarea\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" name=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\" rows=\"3\"></textarea><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                        $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                        $scope.setFieldKeys();
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        var param1 = new Date.create();
                        var param2 = param1.getDate() + '/' + param1.getMonth() + '/' + param1.getFullYear();
                        var MyDate = new Date.create();
                        var MyDateString;
                        MyDate.setDate(MyDate.getDate() + 1);
                        MyDateString = ('0' + MyDate.getDate()).slice(-2) + '/' + ('0' + (MyDate.getMonth() + 1)).slice(-2) + '/' + MyDate.getFullYear();
                        $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                        $scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
                        $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                        $scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
                        $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        if ($scope.MinValue < 0) {
                            $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                        } else {
                            $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                        }
                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                        } else {
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
                        }
                        var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID], $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID]);
                        $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MinDate);
                        $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MaxDate);
                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DatePart_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " : </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" data-date-format=\"dd-mm-yyyy\"  type=\"text\" ng-click=\"Calanderopen($event," + $scope.fields["DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID] + ")\"  datepicker-popup=\"{{format}}\"  is-open=\"fields.DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID + "\" min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" ng-change=\"setTimeout(changeduedate_changed(fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + "," + $scope.atributesRelationList[i].AttributeID + "),3000)\" ng-model=\"fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                        $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                        $scope.setFieldKeys();
                        $scope.fields["fields.DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID] = false;
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 19) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["dListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.EnableDisableControlsHolder["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.CurrencyFormatsList;
                        $scope['origninalamountvalue_' + $scope.atributesRelationList[i].AttributeID] = '0';
                        $scope['currRate_' + $scope.atributesRelationList[i].AttributeID] = 1;
                        $scope.setoptions();
                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\"  class=\"currencyTextbox   margin-right5x\" ng-change=\"Getamountentered(" + $scope.atributesRelationList[i].AttributeID + ")\" ng-disabled=\"EnableDisableControlsHolder.dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-model=\"fields.dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.dListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"GetCostCentreCurrencyRateByIdforammountcurrattr(" + $scope.atributesRelationList[i].AttributeID + ")\" class=\"currencySelector\" ><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.ShortName}}</option></select></div>";
                        $scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = '0';
                        $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.DefaultSettings.CurrencyFormat.Id;
                        $scope.setFieldKeys();
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                        var totLevelCnt = $scope.atributesRelationList[i].Levels.length;
                        for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                return item.Caption
                            };
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                return item.Caption
                            };
                            if (j == 0) {
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                //$scope.dyn_Cont += "<input ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\"  ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                $scope.dyn_Cont += "<input ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\"  ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                $scope.settreeSources();
                            } else {
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];
                                $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                //$scope.dyn_Cont += "<input ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\"  ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                $scope.dyn_Cont += "<input ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\"  ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                            }
                            $scope.setFieldKeys();
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 7) {
                        $scope.fields["Tree_" + $scope.atributesRelationList[i].AttributeID] = [];
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID] = JSON.parse($scope.atributesRelationList[i].tree).Children;
                        if ($scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID].length > 0) {
                            if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID])) {
                                $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = true;
                            } else $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = false;
                        } else {
                            $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = false;
                        }
                        $scope.dyn_Cont += '<div ng-show=\"ShowHideAttributeOnRelation.Attribute_' + $scope.atributesRelationList[i].AttributeID + '\" class="control-group treeNode-control-group">';
                        $scope.dyn_Cont += '<label class="control-label">' + $scope.atributesRelationList[i].AttributeCaption + '</label>';
                        $scope.dyn_Cont += '<div class="controls treeNode-controls">';
                        $scope.dyn_Cont += '<div class="input-group inlineBlock treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + $scope.atributesRelationList[i].AttributeID + '" placeholder="Search" treecontext="treeNodeSearchDropdown_Attr_' + $scope.atributesRelationList[i].AttributeID + '"></div>';
                        $scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_Attr_' + $scope.atributesRelationList[i].AttributeID + '">';
                        $scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
                        $scope.dyn_Cont += '<abn-tree tree-filter="filterValue_' + $scope.atributesRelationList[i].AttributeID + '" accessable="' + $scope.atributesRelationList[i].IsReadOnly + '" tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationList[i].AttributeID + '\" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';
                        $scope.dyn_Cont += '</div><span ng-show=\"' + $scope.atributesRelationList[i].IsHelptextEnabled + '\" class=\"signInfo signInfo margin-left20x\"><i my-qtip2 qtip-content=\"' + attribteHelptext + '\" class=\"icon-info-sign\"></i></span></div></div>';
                        $scope.dyn_Cont += '<div class="control-group staticTreeGroup" ng-show=\"treePreviewObj.Attr_' + $scope.atributesRelationList[i].AttributeID + '\">';
                        $scope.dyn_Cont += '<div class="controls">';
                        $scope.dyn_Cont += '<eu-tree tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationList[i].AttributeID + '\" node-attributeid="' + $scope.atributesRelationList[i].AttributeID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                        //$scope.dyn_Cont += '</div></div>';
                        $scope.dyn_Cont += '</div></div>';
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                        $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = [];
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                        $scope.setoptions();
                        //$scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListMultiSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select class=\"multiselect\"   data-placeholder=\"Select filter\"  ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" multiple=\"multiple\"   multiselect-dropdown ng-model=\"fields.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ",0,0,4)\" ></select></div></div>";
                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListMultiSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select class=\"multiselect\"   data-placeholder=\"Select filter\"  ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" multiple=\"multiple\"   multiselect-dropdown ng-model=\"fields.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ",0,0,4)\" ></select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span>    </div></div>";
                        var defaultmultiselectvalue = $scope.atributesRelationList[i].DefaultValue.split(',');
                        if ($scope.atributesRelationList[i].DefaultValue != "") {
                            for (var j = 0; j < defaultmultiselectvalue.length; j++) {
                                $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID].push($.grep($scope.atributesRelationList[i].Options, function (e) {
                                    return e.Id == defaultmultiselectvalue[j];
                                })[0].Id);
                            }
                        } else {
                            $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = "";
                        }
                        $scope.setFieldKeys();
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 10) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                        $scope.setoptions();
                        $scope.items.push({
                            startDate: [],
                            endDate: [],
                            comment: '',
                            sortorder: 0
                        });
                        $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                        $scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
                        $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        if ($scope.MinValue < 0) {
                            $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                        } else {
                            $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                        }
                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                        } else {
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
                        }
                        var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID], $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID]);
                        $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MinDate);
                        $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MaxDate);
                        $scope.dyn_Cont += "    <div class=\"control-group\"><label for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-label\">" + $scope.atributesRelationList[i].Caption + "</label><div class=\"controls\"><div class=\"period\" id=\"periodcontrols\" ng-repeat=\"item in items\" ng-form=\"subForm\">";
                        $scope.dyn_Cont += "<div class=\"row-fluid\"><div class=\"inputHolder span4\">";
                        $scope.dyn_Cont += "<input class=\"sdate Period_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"AddDefaultEndDate(item)\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"Calanderopen($event," + $scope.fields["DatePart_Calander_Open" + "items.startDate"] + ")\" datepicker-popup=\"{{format}}\"  is-open=\"DatePart_Calander_Open" + "items.startDate" + "\" min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\"  id=\"item.startDate\" type=\"text\" name=\"startDate\" ng-change=\"changeperioddate_changed(item.startDate,'StartDate')\" ng-model=\"item.startDate\" placeholder=\"-- Start date --\" /><input class=\"edate Period_" + $scope.atributesRelationList[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\" name=\"enddate\" id=\"item.endDate\" ng-change=\"changeperioddate_changed(item.endDate,'EndDate')\" ng-model=\"item.endDate\" ng-click=\"Calanderopen($event," + $scope.fields["DatePart_Calander_Open" + "items.endDate"] + ")\"  datepicker-popup=\"{{format}}\"  is-open=\"DatePart_Calander_Open" + "items.endDate" + "\" min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\"  placeholder=\"-- End date --\" /><input class=\"dateComment\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\"  name=\"comment\"   ng-model=\"item.comment\" placeholder=\"-- " + $scope.atributesRelationList[i].Caption + " Comment --\" />";
                        // $scope.dyn_Cont += "</div><div class=\"buttonHolder span1\">";
                        $scope.dyn_Cont += "<span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div><div class=\"buttonHolder span1\">";
                        $scope.dyn_Cont += "<a ng-show=\"$first==false\" ng-click=\"deleteOne(item)\"><i class=\"icon-remove\"></i></a></div></div></div><a ng-click=\"addNew()\">[Add " + $scope.atributesRelationList[i].Caption + "]</a></div></div>";
                        $scope.fields["Period_" + $scope.atributesRelationList[i].AttributeID] = "";
                        $scope.setFieldKeys();
                        $scope.fields["DatePart_Calander_Open" + "items.startDate"] = false;
                        $scope.fields["DatePart_Calander_Open" + "items.endDate"] = false;
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                        var totLevelCnt1 = $scope.atributesRelationList[i].Levels.length;
                        for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                            if (totLevelCnt1 == 1) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                    return item.Caption
                                };
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                    return item.Caption
                                };
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                                $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                //$scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                $scope.setFieldKeys();
                            } else {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                    return item.Caption
                                };
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                    return item.Caption
                                };
                                if (j == 0) {
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                    $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                    // $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"MultiSelectDropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                    $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"MultiSelectDropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                    $scope.settreeSources();
                                } else {
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];
                                    if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
                                        $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                        // $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"MultiSelectDropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                        $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"MultiSelectDropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                    } else {
                                        $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                        //$scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"MultiSelectDropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  type=\"hidden\" /></div></div>";
                                        $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"MultiSelectDropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  type=\"hidden\" /><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                    }
                                }
                                $scope.setFieldKeys();
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 17) {
                        $scope.fields["ListTagwords_" + $scope.atributesRelationList[i].AttributeID] = [];
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.OptionObj["tagoption_" + $scope.atributesRelationList[i].AttributeID] = [];
                        $scope.setoptions();
                        $scope.tempscope = [];
                        var helptext = $scope.atributesRelationList[i].HelptextDecsription == "" ? "" : $scope.atributesRelationList[i].HelptextDecsription.toString();
                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\">";
                        $scope.dyn_Cont += "<label class=\"control-label\" for=\"fields.ListTagwords_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label>";
                        $scope.dyn_Cont += "<div class=\"controls\">";
                        //$scope.dyn_Cont += "<directive-tagwords item-attrid = \"" + $scope.atributesRelationList[i].AttributeID + "\" item-show-hide-progress =\"null\" item-tagword-id=\"fields.ListTagwords_" + $scope.atributesRelationList[i].AttributeID + "\" item-tagword-list=\"tempscope\"></directive-tagwords>";
                        $scope.dyn_Cont += "<directive-tagwords item-attrid = \"" + $scope.atributesRelationList[i].AttributeID + "\" item-show-hide-progress =\"null\" item-tagword-id=\"fields.ListTagwords_" + $scope.atributesRelationList[i].AttributeID + "\" item-tagword-list=\"tempscope\" item-isenable=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" item-attributehelptext=\"" + helptext.toString() + "\"></directive-tagwords>";
                        $scope.dyn_Cont += "</div></div>";
                        if ($scope.atributesRelationList[i].InheritFromParent) { } else { }
                        $scope.setFieldKeys();
                    }
                    $scope.setFieldKeys();
                }
                $scope.dyn_Cont += ' <input style="display: none" type="submit" id="btnTemp" class="ng-scope" invisible>';
                $scope.setFieldKeys();
                $("#SubCostCentreMetadata").html($compile($scope.dyn_Cont)($scope));
                var mar = ($scope.DecimalSettings.FinancialAutoNumeric.vMax).substr(($scope.DecimalSettings.FinancialAutoNumeric.vMax).indexOf(".") + 1);
                $scope.mindec = "";
                if (mar.length == 1) {
                    $("[id^='dTextSingleLine_']").autoNumeric('init', {
                        aSep: ' ',
                        vMin: "0",
                        mDec: "1"
                    });
                }
                if (mar.length != 1) {
                    if (mar.length < 5) {
                        $scope.mindec = "0.";
                        for (i = 0; i < mar.length; i++) {
                            $scope.mindec = $scope.mindec + "0";
                        }
                        $("[id^='dTextSingleLine_']").autoNumeric('init', {
                            aSep: ' ',
                            vMin: $scope.mindec
                        });
                    } else {
                        $("[id^='dTextSingleLine_']").autoNumeric('init', {
                            aSep: ' ',
                            vMin: "0",
                            mDec: "0"
                        });
                    }
                }
                setTimeout(function () {
                    $('[id^=TextSingleLine]:enabled:visible:first').focus().select()
                }, 1000);
                GetValidationList();
                UpdateInheritFromParentScopes();
                $timeout(function () {
                    HideAttributeToAttributeRelationsOnPageLoad();
                }, 200);
            });
        }

        function UpdateInheritFromParentScopes() {
            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                    if ($scope.atributesRelationList[i].IsSpecial == true) { } else if ($scope.atributesRelationList[i].IsSpecial == false) {
                        if (($scope.atributesRelationList[i].InheritFromParent) || ($scope.atributesRelationList[i].ChooseFromParentOnly == true && $scope.atributesRelationList[i].IsReadOnly == true)) {
                            var attrInheritval = ($.grep($scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID], function (e) {
                                return e.Id == $scope.atributesRelationList[i].ParentValue[0]
                            }));
                            if (attrInheritval != undefined) {
                                if (attrInheritval.length > 0) {
                                    $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = attrInheritval[0].Id;
                                }
                            }
                        } else {
                            $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                        }
                        if ($scope.atributesRelationList[i].IsReadOnly) {
                            $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = true;
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                    if ($scope.atributesRelationList[i].IsSpecial == false) {
                        if (($scope.atributesRelationList[i].InheritFromParent) || ($scope.atributesRelationList[i].ChooseFromParentOnly == true && $scope.atributesRelationList[i].IsReadOnly == true)) {
                            if ($scope.atributesRelationList[i].ParentValue[0] != undefined) {
                                $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = [];
                                for (var j = 0; j < $scope.atributesRelationList[i].ParentValue[0].length; j++) {
                                    var attrInheritval = ($.grep($scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID], function (e) {
                                        return e.Id == $scope.atributesRelationList[i].ParentValue[0][j]
                                    }));
                                    if (attrInheritval != undefined) {
                                        if (attrInheritval.length > 0) {
                                            $scope.fields["Selection_" + $scope.atributesRelationList[i].AttributeID].push(attrInheritval[0].Id);
                                        }
                                    }
                                }
                            }
                        } else {
                            $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = [];
                            var defaultmultiselectvalue = $scope.atributesRelationList[i].DefaultValue.split(',');
                            if ($scope.atributesRelationList[i].DefaultValue != "") {
                                for (var j = 0; j < defaultmultiselectvalue.length; j++) {
                                    $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID].push($.grep($scope.atributesRelationList[i].Options, function (e) {
                                        return e.Id == defaultmultiselectvalue[j];
                                    })[0].Id);
                                }
                            }
                        }
                        if ($scope.atributesRelationList[i].IsReadOnly) {
                            $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = true;
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                    if (($scope.atributesRelationList[i].InheritFromParent) || ($scope.atributesRelationList[i].ChooseFromParentOnly == true && $scope.atributesRelationList[i].IsReadOnly == true)) {
                        var levelCount = $scope.Inherritingtreelevels["dropdown_levels_" + $scope.atributesRelationList[i].ID].length
                        for (var j = 1; j <= levelCount; j++) {
                            var dropdown_text = "dropdown_text_" + $scope.atributesRelationList[i].ID + "_" + j;
                            if (j == 1) {
                                if ($scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j)].data.length > 0) {
                                    $scope.fields["DropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j] = ($.grep($scope.treeSources["dropdown_" + $scope.atributesRelationList[i].AttributeID].Children, function (e) {
                                        return e.Caption == $scope.treeTexts[dropdown_text]
                                    }))[0];
                                }
                            } else {
                                if ($scope.fields["DropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)] != null) if ($scope.fields["DropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children != undefined) $scope.fields["DropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j] = ($.grep($scope.fields["DropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children, function (e) {
                                    return e.Caption == $scope.treeTexts[dropdown_text]
                                }))[0];
                            }
                        }
                    }
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                    if (($scope.atributesRelationList[i].InheritFromParent) || ($scope.atributesRelationList[i].ChooseFromParentOnly == true && $scope.atributesRelationList[i].IsReadOnly == true)) {
                        var levelCount = $scope.Inherritingtreelevels["multiselectdropdown_levels_" + $scope.atributesRelationList[i].ID].length
                        for (var j = 1; j <= levelCount; j++) {
                            var dropdown_text = "multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + j;
                            if (j == 1) {
                                if ($scope.atributesRelationList[i].InheritFromParent) {
                                    if ($scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j)].data.length > 0) {
                                        $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j] = ($.grep($scope.treeSources["multiselectdropdown_" + $scope.atributesRelationList[i].AttributeID].Children, function (e) {
                                            return e.Caption == $scope.multiselecttreeTexts[dropdown_text]
                                        }))[0];
                                    }
                                }
                            } else {
                                $scope.ShowHideAttributeToAttributeRelations($scope.atributesRelationList[i].AttributeID, j, levelCount, 12);
                                if ($scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)] != null) {
                                    if ($scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children != undefined) {
                                        if (j == levelCount) {
                                            var k = j;
                                            if ($scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children.length > 0) {
                                                $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j] = [];
                                                for (var l = 0; l < $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children.length; l++) {
                                                    $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j].push(($.grep($scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children, function (e) {
                                                        return e.Caption.toString().trim() == $scope.multiselecttreeTexts[dropdown_text][l].toString().trim();
                                                    }))[0]);
                                                }
                                            }
                                        } else {
                                            $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j] = ($.grep($scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children, function (e) {
                                                return e.Caption == $scope.multiselecttreeTexts[dropdown_text]
                                            }))[0];
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                    if ($scope.atributesRelationList[i].InheritFromParent) {
                        $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue;
                    } else {
                        $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                    }
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                    if ($scope.atributesRelationList[i].InheritFromParent) {
                        $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue;
                    } else {
                        $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                    }
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 5) {
                    if ($scope.atributesRelationList[i].InheritFromParent) {
                        $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = ConvertDateFromStringToString($scope.atributesRelationList[i].ParentValue.toString(), $scope.DefaultSettings.DateFormat);
                    }
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 9) {
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 10) {
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 11) {
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = false;
                    }
                }
            }
        }
        $scope.AddDefaultEndDate = function (objdateval) {
            $("#SubCostCentreMetadata").addClass('notvalidate');
            if (objdateval.startDate == null) {
                objdateval.endDate = null
            } else {
                objdateval.endDate = new Date.create(objdateval.startDate);
                objdateval.endDate = objdateval.endDate.addDays(7);
            }
        };

        function GetValidationList() {
            SubcostcentrecreationService.GetValidationDationByEntitytype(5).then(function (GetValidationresult) {
                if (GetValidationresult.Response != null) {
                    $scope.listValidationResult = GetValidationresult.Response;
                    if ($scope.listAttriToAttriResult != null) {
                        for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
                            var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
                            if (attrRelIDs != undefined) {
                                for (var j = 0; j < attrRelIDs.length; j++) {
                                    if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                        $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                                            return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString());
                                        });
                                    } else {
                                        $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                                            return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                        });
                                    }
                                }
                            }
                        }
                    }
                    $("#SubCostCentreMetadata").nod($scope.listValidationResult, {
                        'delay': 200,
                        'submitBtnSelector': '#btnTemp',
                        'silentSubmit': 'true'
                    });
                    if ($scope.listValidationResult.length != 0) {
                        for (var i = 0 ; i < $scope.listValidationResult.length; i++) {
                            if ($scope.listValidationResult[i][1] == "presence") {
                                $($scope.listValidationResult[i][0]).parent().addClass('relative');
                                $("<i class=\"icon-asterisk validationmark margin-right5x\"></i>").insertAfter($scope.listValidationResult[i][0]);
                            }
                        }
                    }
                }
            });
        }

        function RecursiveAttributeToAttributeHide(res, attrID, attrLevel, attrType) {
            var recursiveAttrID = '';
            var attributesToShow = [];
            if (attrLevel > 0) {
                attributesToShow.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID && e.AttributeLevel >= attrLevel);
                }));
            } else {
                attributesToShow.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID);
                }));
            }
            if (attributesToShow[0] != undefined) {
                for (var i = 0; i < attributesToShow[0].length; i++) {
                    var attrRelIDs = attributesToShow[0][i].AttributeRelationID.toString().split(',');
                    if (attrRelIDs != undefined) {
                        for (var j = 0; j < attrRelIDs.length; j++) {
                            if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return (e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))) && e.AttributeLevel == attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].length));
                                }));
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType);
                                    }
                                }
                            } else {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                $scope.fields['ListSingleSelection_' + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = "";
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                }));
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $scope.ShowHideAttributeToAttributeRelations = function (attrID, attributeLevel, levelcnt, attrType) {
            try {
                var recursiveAttrID = '';
                var optionValue = '';
                var attributesToShow = [];
                var hideAttributeOtherThanSelected = [];
                if (levelcnt > 0) {
                    var currntlevel = attributeLevel + 1;
                    for (var j = currntlevel; j <= levelcnt; j++) {
                        $scope.Dropdown["OptionValues" + attrID + "_" + j].data.splice(0, $scope.Dropdown["OptionValues" + attrID + "_" + j].data.length);
                        if (attrType == 6) {
                            $scope.fields["DropDown_" + attrID + "_" + j] = "";
                        } else if (attrType == 12) {
                            if (j == levelcnt) $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = [];
                            else $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = "";
                        }
                    }
                    if (attrType == 6) {
                        if ($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                            $.each($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                            });
                        }
                    } else if (attrType == 12) {
                        if ($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                            $.each($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                            });
                        }
                    }
                }
                if (attrID == SystemDefiendAttributes.FiscalYear) {
                    try {
                        $scope.changeCostCenterSource();
                    } catch (e) { }
                }
                if ($scope.listAttriToAttriResult == null && $scope.listAttriToAttriResult == undefined) {
                    return false;
                }
                RecursiveAttributeToAttributeHide($scope.listAttriToAttriResult, attrID, attributeLevel, attrType);
                if (attrType == 3) {
                    if ($scope.fields['ListSingleSelection_' + attrID] != null) {
                        optionValue = parseInt(($scope.fields['ListSingleSelection_' + attrID]), 10);
                    } else {
                        optionValue = 0;
                    }
                    attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && e.AttributeOptionID == optionValue;
                    })[0]);
                } else if (attrType == 4) {
                    if ($scope.fields['ListMultiSelection_' + attrID] != null) {
                        optionValue = $scope.fields['ListMultiSelection_' + attrID];
                    } else {
                        optionValue = 0;
                    }
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 6) {
                    var attrval = $scope.fields["DropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                } else if (attrType == 7) {
                    if ($scope.fields['Tree_' + attrID] != null) {
                        optionValue = $scope.fields['Tree_' + attrID];
                    } else {
                        optionValue = 0;
                    }
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 12) {
                    var attrval = $scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                }
                if (attributesToShow[0] != undefined) {
                    for (var i = 0; i < attributesToShow.length; i++) {
                        var attrRelIDs = attributesToShow[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = true;
                                } else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = true;
                                }
                            }
                        }
                    }
                }
            } catch (e) { }
        }

        function HideAttributeToAttributeRelationsOnPageLoad() {
            try {
                if ($scope.listAttriToAttriResult != undefined) {
                    for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
                        var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
                                } else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                }
                            }
                        }
                    }
                }
            } catch (e) { }
        }

        function RecursiveAttributeToAttributeHide(res, attrID, attrLevel, attrType) {
            var recursiveAttrID = '';
            var attributesToShow = [];
            if (attrLevel > 0) {
                attributesToShow.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID && e.AttributeLevel >= attrLevel);
                }));
            } else {
                attributesToShow.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID);
                }));
            }
            if (attributesToShow[0] != undefined) {
                for (var i = 0; i < attributesToShow[0].length; i++) {
                    var attrRelIDs = attributesToShow[0][i].AttributeRelationID.toString().split(',');
                    if (attrRelIDs != undefined) {
                        for (var j = 0; j < attrRelIDs.length; j++) {
                            if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return (e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))) && e.AttributeLevel == attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].length));
                                }));
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType);
                                    }
                                }
                            } else {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                $scope.fields['ListSingleSelection_' + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = "";
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                }));
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $scope.ShowHideAttributeToAttributeRelations = function (attrID, attributeLevel, levelcnt, attrType) {
            try {
                var recursiveAttrID = '';
                var optionValue = '';
                var attributesToShow = [];
                var hideAttributeOtherThanSelected = [];
                if (levelcnt > 0) {
                    var currntlevel = attributeLevel + 1;
                    for (var j = currntlevel; j <= levelcnt; j++) {
                        $scope.Dropdown["OptionValues" + attrID + "_" + j].data.splice(0, $scope.Dropdown["OptionValues" + attrID + "_" + j].data.length);
                        if (attrType == 6) {
                            $scope.fields["DropDown_" + attrID + "_" + j] = "";
                        } else if (attrType == 12) {
                            if (j == levelcnt) $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = [];
                            else $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = "";
                        }
                    }
                    if (attrType == 6) {
                        if ($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                            $.each($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                            });
                        }
                    } else if (attrType == 12) {
                        if ($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                            $.each($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                            });
                        }
                    }
                }
                if (attrID == SystemDefiendAttributes.FiscalYear) {
                    try {
                        $scope.changeCostCenterSource();
                    } catch (e) { }
                }
                if ($scope.listAttriToAttriResult == null && $scope.listAttriToAttriResult == undefined) {
                    return false;
                }
                RecursiveAttributeToAttributeHide($scope.listAttriToAttriResult, attrID, attributeLevel, attrType);
                if (attrType == 3) {
                    if ($scope.fields['ListSingleSelection_' + attrID] != null) {
                        optionValue = parseInt(($scope.fields['ListSingleSelection_' + attrID]), 10);
                    } else {
                        optionValue = 0;
                    }
                    attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && e.AttributeOptionID == optionValue;
                    })[0]);
                } else if (attrType == 4) {
                    if ($scope.fields['ListMultiSelection_' + attrID] != null) {
                        optionValue = $scope.fields['ListMultiSelection_' + attrID];
                    } else {
                        optionValue = 0;
                    }
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 6) {
                    var attrval = $scope.fields["DropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                } else if (attrType == 7) {
                    if ($scope.fields['Tree_' + attrID] != null) {
                        optionValue = $scope.fields['Tree_' + attrID];
                    } else {
                        optionValue = 0;
                    }
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 12) {
                    var attrval = $scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                }
                if (attributesToShow[0] != undefined) {
                    for (var i = 0; i < attributesToShow.length; i++) {
                        var attrRelIDs = attributesToShow[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = true;
                                } else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = true;
                                }
                            }
                        }
                    }
                }
            } catch (e) { }
        }

        function HideAttributeToAttributeRelationsOnPageLoad() {
            try {
                if ($scope.listAttriToAttriResult != undefined) {
                    for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
                        var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
                                } else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                }
                            }
                        }
                    }
                }
            } catch (e) { }
        }
        SubcostcentrecreationService.GetCurrencyListFFsettings().then(function (CurrencyListResult) {
            if (CurrencyListResult.Response != null) $scope.CurrencyFormatsList = CurrencyListResult.Response;
        });
        $scope.GetCostCentreCurrencyRateById = function () {
            SubcostcentrecreationService.GetCostCentreCurrencyRateById(0, $scope.SelectedCostCentreCurrency.TypeId, true).then(function (resCurrencyRate) {
                if (resCurrencyRate.Response != null) {
                    $scope.SelectedCostCentreCurrency.Rate = parseFloat(resCurrencyRate.Response[1] == 0 ? 1 : resCurrencyRate.Response[1]);
                    $scope.AvailableAssignAmount = $scope.AvailableAssignAmount * $scope.SelectedCostCentreCurrency.Rate;
                    $scope.SelectedCostCentreCurrency.ShortName = $.grep($scope.CurrencyFormatsList, function (e) {
                        return e.Id == $scope.SelectedCostCentreCurrency.TypeId;
                    })[0].ShortName;
                }
            });
        }
        $scope.GetCostCentreCurrencyRateByIdforammountcurrattr = function (atrid) {
            SubcostcentrecreationService.GetCostCentreCurrencyRateById(0, $scope.fields["ListSingleSelection_" + atrid], true).then(function (resCurrencyRate) {
                if (resCurrencyRate.Response != null) {
                    $scope['currRate_' + atrid] = parseFloat(resCurrencyRate.Response[1]);
                    if ($scope['origninalamountvalue_' + atrid] != 0) {
                        $scope.fields["dTextSingleLine_" + atrid] = (parseFloat($scope['origninalamountvalue_' + atrid]) * $scope['currRate_' + atrid]).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ');
                    }
                }
            });
        }
        $scope.Getamountentered = function (atrid) {
            if (1 == $scope.fields["ListSingleSelection_" + atrid]) $scope['origninalamountvalue_' + atrid] = $scope.fields["dTextSingleLine_" + atrid].replace(/ /g, '');
            else $scope['origninalamountvalue_' + atrid] = $scope.fields["dTextSingleLine_" + atrid].replace(/ /g, '') / 1 / $scope['currRate_' + atrid];
        }
        $scope.changeduedate_changed = function (duedate, ID) {
            if (duedate != null) {
                var test = isValidDate(duedate.toString(), $scope.format.toString());
                if (test) {
                    var a = $.grep($scope.tempholidays, function (e) {
                        return e == dateFormat(duedate, $scope.format);
                    });
                    if (a != null) {
                        if (a.length > 0) {
                            bootbox.alert("Selected date should not same as non businessday");
                            $scope.fields["DatePart_" + ID] = "";
                        }
                    }
                } else {
                    $scope.fields["DatePart_" + ID] = "";
                    bootbox.alert("Select valid duedate");
                }
            }
        }
        $scope.changeperioddate_changed = function (date, datetype) {
            if (date != null) {
                var test = isValidDate(date.toString(), $scope.format.toString());
                if (test) {
                    var a = $.grep($scope.tempholidays, function (e) {
                        return e == dateFormat(date, $scope.format);
                    });
                    if (a != null) {
                        if (a.length > 0) {
                            bootbox.alert("Selected date should not same as non businessday");
                            if (datetype == "StartDate") $scope.items[0].startDate = "";
                            else $scope.item[0].endDate = "";
                        }
                    }
                } else {
                    if (datetype == "StartDate") $scope.items[0].startDate = "";
                    else $scope.item[0].endDate = "";
                    bootbox.alert("Select valid duedate");
                }
            }
        }

        function isValidDate(dateval, dateformat) {
            var formatlen;
            var defaultdateVal = [];
            defaultdateVal = dateval.length;
            formatlen = dateformat.length;
            if (formatlen == defaultdateVal || defaultdateVal > formatlen) return true;
            else return false;
        };

        $scope.EntityStatusResult = [];

        $scope.settxtColor = function (hexcolor) {
            var r = parseInt(hexcolor.substr(0, 2), 16);
            var g = parseInt(hexcolor.substr(2, 2), 16);
            var b = parseInt(hexcolor.substr(4, 2), 16);
            var yiq = ((r * 299) + (g * 587) + (b * 114)) / 1000;
            return (yiq >= 160) ? 'black' : 'white';
        }

        $scope.overallstatusformat = function (data) {
            var fontColor = 'blue';
            var txtcolor = 'white';
            $("#select2-drop").addClass("bgDropdown");
            if (data.id == "0" && data.text != "") {
                return "<div class='select2-chosen-label'style='background-color: #FEFEFE; color: #444444; font-size: 11px;'>" + data.text + "</div>";
            }
            else if (data.id != "" || data.text != "") {
                for (var i = 0; i < $scope.EntityStatusResult.length; i++) {
                    var res = $scope.EntityStatusResult[i];
                    if (res.ID == data.id) {
                        var stausoption = res.StatusOptions;
                        var colorcode = res.ColorCode;
                        if (colorcode == null || colorcode.trim() == "")
                            txtcolor = $scope.settxtColor("ffffff");
                        else
                            txtcolor = $scope.settxtColor(colorcode);
                        break;
                    }
                }
            }
            else {
                return "<div class='select2-chosen-label' style='background-color:#" + fontColor + "; color:" + txtcolor + "'>" + data.text + "</div>";
            }
            return "<div class='select2-chosen-label'style='background-color:#" + colorcode + ";color:" + txtcolor + "'>" + stausoption + "</div>";
        };

        $scope.closeSubcostcentrecreationPopup = function () {
            $modalInstance.dismiss('cancel');
        }
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.costcentre.subcostcentrecreationCtrl']"));
        });
    }
    app.controller("mui.planningtool.costcentre.subcostcentrecreationCtrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$window', '$compile', '$translate', 'SubcostcentrecreationService', '$modalInstance', 'params', muisubcostcentrecreationCtrl]);
})(angular, app);
///#source 1 1 /app/controllers/mui/planningtool/costcentre/detail/detailfilter-controller.js
(function (ng, app) {
    "use strict";

    function muiplanningtoolcostcentredetaildetailfilterCtrl($scope, $location, $resource, $timeout, $cookies, $compile, $window, $translate, CcdetailfilterService) {
        $scope.attributegroupTypeid = 0;
        $scope.treeNodeSelectedHolderValues = [];
        var IsSave = 0;
        var IsUpdate = 0;
        $scope.deletefiltershow = false;
        $scope.showSave = true;
        $scope.showUpdate = false;
        $scope.EntityTypeID = 0;
        $scope.appliedfilter = 'No filter applied';
        $scope.FilterFields = {};
        $scope.PeriodOptionValue = {};
        var DateValidate = dateFormat('1990-01-01', $scope.DefaultSettings.DateFormat);
        var elementNode = 'DetailFilter';
        var selectedfilterid = 0;
        $scope.items = [];
        $scope.PercentageVisibleSettings = {};
        $scope.DropDownFilterTreePricing = {};
        $scope.TreePricing = [];
        $scope.FilterDataXML = [];
        if ($window.CostCentreFilterName != '' && $window.CostCentreFilterName != 'No filter applied') { } else {
            $window.CostCentreFilterName = '';
        }
        var TypeID = 5;
        $scope.visible = false;
        $scope.dynamicEntityValuesHolder = {};
        $scope.dyn_Cont = '';
        $scope.fieldKeys = [];
        $scope.options = {};
        $scope.setFieldKeys = function () {
            var keys = [];
            angular.forEach($scope.FilterFields, function (key) {
                keys.push(key);
                $scope.fieldKeys = keys;
            });
        }
        $scope.OptionObj = {};
        $scope.fieldoptions = [];
        $scope.setoptions = function () {
            var keys = [];
            angular.forEach($scope.OptionObj, function (key) {
                keys.push(key);
                $scope.fieldoptions = keys;
            });
        }
        $scope.filterValues = [];
        $scope.atributesRelationList = [];
        $scope.atributesRelationListWithTree = [];
        $scope.filterSettingValues = [];
        $scope.atributeGroupList = [];
        $scope.DetailFilterCreation = function (event) {
            $scope.ClearScopeModle($scope.FilterFields);
            $scope.FilterSettingsLoad();
            $scope.Deletefilter = false;
            $scope.Applyfilter = true;
            $scope.Updatefilter = false;
            $scope.Savefilter = true;
            $scope.ngsaveasfilter = '';
            $scope.ngKeywordtext = '';
            IsSave = 0;
            IsUpdate = 0;
        };
        $("#DetailFilterCreation").on("onDetailFilterCreation", function (event) {
            $scope.ClearcostCentrefilterScopeModle();
        });
        $("#costcentredetailfiltersettings").on("ClearScope", function (event, filterid) {
            $scope.showSave = true;
            $scope.showUpdate = false;
            $scope.deletefiltershow = false;
            $scope.ClearScopeModle();
        });
        $scope.ClearFilterAttributes = function () {
            $scope.ClearFieldsAndReApply();
        }
        $scope.ClearScopeModle = function () {
            $scope.items = [];
            $scope.clearscope = !$scope.clearscope;
            $scope.ngKeywordtext = '';
            $scope.ngsaveasfilter = '';
            $scope.ddlEntitymember = [];
            $("#activitydetailfilter").removeAttr('disabled');
            for (var variable in $scope.FilterFields) {
                if (typeof $scope.FilterFields[variable] === "string") {
                    if (variable !== "ListSingleSelection_69") {
                        $scope.FilterFields[variable] = "";
                    }
                } else if (typeof $scope.FilterFields[variable] === "number") {
                    $scope.FilterFields[variable] = null;
                } else if (Array.isArray($scope.FilterFields[variable])) {
                    $scope.FilterFields[variable] = [];
                } else if (typeof $scope.FilterFields[variable] === "object") {
                    $scope.FilterFields[variable] = {};
                }
            }
            $scope.treePreviewObj = {};
            $scope.treeNodeSelectedHolderValues = [];
            var treecount = $.grep($scope.atributesRelationList, function (e) {
                return e.AttributeTypeId == 7
            });
            for (var i = 0; i < treecount.length; i++) {
                ClearSavedTreeNode(treecount[i].AttributeId);
            }
            $scope.costcemtreObject = [];
            $scope.MemberLists = [];
            $scope.attributegroupTypeid = 0;
        }
        $scope.ClearFieldsAndReApply = function () {
            $scope.attributegroupTypeid = 0;
            IsSave = 0;
            $scope.items = [];
            $scope.clearscope = !$scope.clearscope;
            $scope.ngsaveasfilter = '';
            $scope.ngKeywordtext = '';
            $scope.ddlParententitytypeId = [];
            $("#activitydetailfilter").removeAttr('disabled');
            for (var variable in $scope.FilterFields) {
                if (typeof $scope.FilterFields[variable] === "string") {
                    if (variable !== "ListSingleSelection_69") {
                        $scope.FilterFields[variable] = "";
                    }
                } else if (typeof $scope.FilterFields[variable] === "number") {
                    $scope.FilterFields[variable] = null;
                } else if (Array.isArray($scope.FilterFields[variable])) {
                    $scope.FilterFields[variable] = [];
                } else if (typeof $scope.FilterFields[variable] === "object") {
                    $scope.FilterFields[variable] = {};
                }
            }
            $scope.treePreviewObj = {};
            $scope.treeNodeSelectedHolderValues = [];
            var treecount = $.grep($scope.atributesRelationList, function (e) {
                return e.AttributeTypeId == 7
            });
            for (var i = 0; i < treecount.length; i++) {
                ClearSavedTreeNode(treecount[i].AttributeId);
            }
            var pricingObj = $.grep($scope.atributesRelationList, function (e) {
                return e.AttributeTypeId == 13
            });
            if (pricingObj != undefined)
                if (pricingObj.length > 0)
                    for (var t = 0, obj; obj = pricingObj[t++];) {
                        for (var z = 0, price; price = $scope.DropDownFilterTreePricing["AttributeId_Levels_" + obj.AttributeId + ""][z++];) {
                            if (price.selection != null)
                                if (price.selection.length > 0) price.selection = [];
                        }
                    }
            $scope.ShowPeriodOptions = false;
            $scope.costcemtreObject = [];
            $scope.MemberLists = [];
            var ApplyFilterobj = [];
            $scope.showSave = true;
            $scope.showUpdate = false;
            $scope.deletefiltershow = false;
            $("#EntitiesTree").trigger("loadactivityfromfilterforCostCenter", [0, ApplyFilterobj]);
            $("#costcentredetailfilter").trigger('ClearAndReApply');
        }
        $scope.ClearcostCentrefilterScopeModle = function () {
            for (var variable in $scope.FilterFields) {
                if (typeof $scope.FilterFields[variable] === "string") {
                    if (variable !== "ListSingleSelection_69") {
                        $scope.FilterFields[variable] = "";
                    }
                } else if (typeof $scope.FilterFields[variable] === "number") {
                    $scope.FilterFields[variable] = null;
                } else if (Array.isArray($scope.FilterFields[variable])) {
                    $scope.FilterFields[variable] = [];
                } else if (typeof $scope.FilterFields[variable] === "object") {
                    $scope.FilterFields[variable] = {};
                }
            }
            $scope.costcemtreObject = [];
            $scope.MemberLists = [];
        }

        function KeepAllStepsMarkedComplete() {
            $("#MyWizard ul.steps").find("li").addClass("complete");
            $("#MyWizard ul.steps").find("span.badge").addClass("badge-success");
            window["tid_wizard_steps_all_complete_count"]++;
            if (window["tid_wizard_steps_all_complete_count"] >= 20) {
                clearInterval(window["tid_wizard_steps_all_complete"]);
            }
        }
        $scope.hideFilterSettings = function () {
            $('.FilterHolder').slideUp("slow")
            $timeout(function () {
                $(window).AdjustHeightWidth();
            }, 500);
        }
        $("#costcentredetailfiltersettings").on('loaddetailfiltersettings', function (event, TypeID) {
            $scope.FilterSettingsLoad();
        });
        $scope.OntimeStatusLists = [{
            Id: 0,
            Name: 'On time',
            BtnBgColor: 'btn-success',
            AbgColor: 'background-image: linear-gradient(to bottom, #62C462, #51A351); color: #fff;',
            SelectedClass: 'btn-success'
        }, {
            Id: 1,
            Name: 'Delayed',
            BtnBgColor: 'btn-warning',
            AbgColor: 'background-image: linear-gradient(to bottom, #FBB450, #F89406); color: #fff;',
            SelectedClass: 'btn-warning'
        }, {
            Id: 2,
            Name: 'On hold',
            BtnBgColor: 'btn-danger',
            AbgColor: 'background-image: linear-gradient(to bottom, #EE5F5B, #BD362F); color: #fff;',
            SelectedClass: 'btn-danger'
        }]
        $scope.FilterSettingsLoad = function () {
            $scope.atributeGroupList = [];
            $scope.attributegroupTypeid = 0;
            $scope.EntityHierarchyTypesResult = [];
            if ($window.CostCentreFilterName == '') {
                $window.CostCentreFilterName = '';
            }
            $scope.treesrcdirec = {};
            $scope.treePreviewObj = {};
            $scope.dyn_Cont = '';
            $scope.Updatefilter = false;
            $scope.Applyfilter = true;
            $scope.Deletefilter = false;
            $scope.Savefilter = false;
            $("#dynamic_Controls").html('');
            $scope.atributesRelationList = [];
            $scope.atributesRelationListWithTree = [];
            $scope.ngsaveasfilter = '';
            $scope.ngKeywordtext = '';
            var OptionFrom = 0;
            var IsKeyword = "false";
            var IsEntityType = "false";
            var IsEntityMember = "false";
            var CurrentActiveVersion = 0;
            var KeywordOptionsresponse;
            $scope.PeriodOptions = [{
                Id: 1,
                FilterPeriod: 'Between'
            }, {
                Id: 2,
                FilterPeriod: 'Within'
            }];
            CcdetailfilterService.GetOptionsFromXML(elementNode, 5).then(function (KeywordOptionsResult) {
                KeywordOptionsresponse = KeywordOptionsResult.Response
                OptionFrom = KeywordOptionsresponse.split(',')[0];
                IsKeyword = KeywordOptionsresponse.split(',')[1];
                IsEntityType = KeywordOptionsresponse.split(',')[3];
                IsEntityMember = KeywordOptionsresponse.split(',')[4];
                CurrentActiveVersion = KeywordOptionsresponse.split(',')[2];
                CcdetailfilterService.GettingEntityTypeHierarchyForAdminTree(6, 3).then(function (GerParentEntityData) {
                    $scope.entitytpesdata = GerParentEntityData.Response;
                    $scope.tagAllOptions.data = [];
                    if (GerParentEntityData.Response != null) {
                        $.each(GerParentEntityData.Response, function (i, el) {
                            $scope.tagAllOptions.data.push({
                                "id": el.Id,
                                "text": el.Caption,
                                "ShortDescription": el.ShortDescription,
                                "ColorCode": el.ColorCode
                            });
                        });
                    }
                    $scope.ddlParententitytypeId = $scope.EntityHierarchyTypesResult;
                    var IDList1 = new Array();
                    var filterattidsmeber = {};
                    filterattidsmeber.IDList = $window.ListofEntityID;;
                    filterattidsmeber.TypeID = TypeID;
                    filterattidsmeber.FilterType = 'DetailFilter';
                    filterattidsmeber.OptionFrom = OptionFrom;
                    filterattidsmeber.IsEntityMember = IsEntityMember
                    $scope.tagmemberOptions = [];
                    CcdetailfilterService.GettingFilterEntityMember(filterattidsmeber).then(function (entityyMemberRelation) {
                        $scope.entityMemberRelationdata = entityyMemberRelation.Response;
                        if (entityyMemberRelation.Response != null) {
                            $.each(entityyMemberRelation.Response, function (i, el) {
                                $scope.tagmemberOptions.push({
                                    "id": el.Id,
                                    "Name": el.FirstName + ' ' + el.LastName
                                });
                            });
                        }
                        var filterattids = {};
                        filterattids.TypeID = TypeID;
                        filterattids.FilterType = 'DetailFilter';
                        filterattids.OptionFrom = OptionFrom;
                        filterattids.IDList = $window.ListofEntityID;
                        filterattids.IsEntityMember = IsEntityMember
                        CcdetailfilterService.GettingFilterAttribute(filterattids).then(function (entityAttributesRelation) {
                            $scope.atributesRelationList = entityAttributesRelation.Response;
                            if ($scope.atributesRelationList != undefined) {
                                for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                                    if ($scope.atributesRelationList[i].AttributeTypeId == 6) {
                                        $scope.dyn_Cont += '<div class="control-group"><span>' + $scope.atributesRelationList[i].DisplayName + ' : </span>';
                                        $scope.dyn_Cont += '<div class="controls"><select multiple="multiple"  class=\"multiselect\" multiselect-dropdown data-placeholder="Select ' + $scope.atributesRelationList[i].DisplayName + ' options" ng-model="FilterFields.DropDown_' + $scope.atributesRelationList[i].AttributeId + '_' + $scope.atributesRelationList[i].TreeLevel + '"\  ng-options=\"opt.Id as opt.Caption for opt in  atributesRelationList[' + i + '].LevelTreeNodes \">';
                                        $scope.dyn_Cont += '</select></div></div>';
                                        $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] = [];
                                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 12) {
                                        $scope.dyn_Cont += '<div class="control-group"><span>' + $scope.atributesRelationList[i].DisplayName + ' : </span>';
                                        $scope.dyn_Cont += '<div class="controls"><select multiple="multiple"  class=\"multiselect\" multiselect-dropdown data-placeholder="Select ' + $scope.atributesRelationList[i].DisplayName + ' options" ng-model="FilterFields.DropDown_' + $scope.atributesRelationList[i].AttributeId + '_' + $scope.atributesRelationList[i].TreeLevel + '"\  ng-options=\"opt.Id as opt.Caption for opt in  atributesRelationList[' + i + '].LevelTreeNodes \">';
                                        $scope.dyn_Cont += '</select></div></div>';
                                        $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] = [];
                                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 13) {
                                        var k = $scope.TreePricing.length;
                                        var treecount12 = $.grep($scope.FilterDataXML, function (e) {
                                            return e.AttributeId == $scope.atributesRelationList[i].AttributeId
                                        });
                                        if (treecount12.length == 0) {
                                            var mm = $scope.atributesRelationList[i].AttributeId;
                                            $scope.PercentageVisibleSettings["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeId.toString() + ""] = false;
                                            $scope.DropDownFilterTreePricing["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeId.toString() + ""] = $scope.atributesRelationList[i].DropdowntreePricingAttr;
                                            $scope.dyn_Cont += "<div drowdowntreepercentagemultiselectionfilter  data-purpose='entity' data-attributeid=" + $scope.atributesRelationList[i].AttributeId.toString() + "></div>";
                                            $scope.FilterDataXML.push({
                                                "AttributeId": parseInt($scope.atributesRelationList[i].AttributeId)
                                            });
                                        }
                                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 3) {
                                        if ($scope.atributesRelationList[i].AttributeId == SystemDefiendAttributes.Owner) {
                                            $scope.dyn_Cont += "<div class=\"control-group\"><span>" + $scope.atributesRelationList[i].DisplayName + "</span> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"  id=\"DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"     ng-options=\"ndata.Id as (ndata.FirstName+' '+ndata.LastName)  for ndata in  atributesRelationList[" + i + "].Users \"></select></div>";
                                            $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] = [];
                                        } else if ($scope.atributesRelationList[i].AttributeId == SystemDefiendAttributes.EntityStatus) {
                                            var EntityStatusOptions = $scope.atributesRelationList[i].EntityStatusOptionValues
                                            $scope.tagAllOptionsEntityStatus.data = [];
                                            if (EntityStatusOptions != null) {
                                                $.each(EntityStatusOptions, function (i, el) {
                                                    $scope.tagAllOptionsEntityStatus.data.push({
                                                        "id": el.ID,
                                                        "text": el.StatusOptions,
                                                        "ShortDescription": el.ShortDesc,
                                                        "ColorCode": el.ColorCode
                                                    });
                                                });
                                            }
                                            $scope.dyn_Cont += "<div class='control-group'>";
                                            $scope.dyn_Cont += "<span>" + $scope.atributesRelationList[i].DisplayName + " : </span><div class=\"controls\">";
                                            $scope.dyn_Cont += "<input id='ddlEntityStatus' class='width2x' placeholder='Select Entity Status Options' type='hidden' ui-select2='tagAllOptionsEntityStatus' ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\" />";
                                            $scope.dyn_Cont += "</div></div>";
                                        } else if ($scope.atributesRelationList[i].AttributeId == SystemDefiendAttributes.EntityOnTimeStatus) {
                                            $scope.dyn_Cont += '<div class=\"control-group\"><span>' + $scope.atributesRelationList[i].DisplayName + ' : </span>';
                                            $scope.dyn_Cont += "<div class=\"controls\"><select  class=\"multiselect\" multiselect-dropdown  multiple=\"multiple\" ";
                                            $scope.dyn_Cont += "data-placeholder=\"Select Entity OnTime Status\" ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"";
                                            $scope.dyn_Cont += "id=\"DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"  ng-options=\"opt.Id as opt.Name for opt in  OntimeStatusLists \">";
                                            $scope.dyn_Cont += "</select>";
                                            $scope.dyn_Cont += "</div></div>";
                                        } else {
                                            $scope.dyn_Cont += '<div class=\"control-group\"><span>' + $scope.atributesRelationList[i].DisplayName + ' : </span>';
                                            $scope.dyn_Cont += '<div class=\"controls\"> <select multiple="multiple"  class=\"multiselect\" multiselect-dropdown data-placeholder="Select ' + $scope.atributesRelationList[i].DisplayName + ' options" ng-model=\"FilterFields.DropDown_' + $scope.atributesRelationList[i].AttributeId + '"\" ng-options=\"opt.Id as opt.Caption for opt in  atributesRelationList[' + i + '].OptionValues \">';
                                            $scope.dyn_Cont += '</select></div></div>';
                                            $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] = [];
                                        }
                                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 4) {
                                        $scope.dyn_Cont += "<div class=\"control-group\"><span>" + $scope.atributesRelationList[i].DisplayName + "</span>";
                                        $scope.dyn_Cont += "<div class=\"controls\"><select  class=\"multiselect\" multiselect-dropdown  multiple=\"multiple\"";
                                        $scope.dyn_Cont += "data-placeholder=\"Select " + $scope.atributesRelationList[i].DisplayName + "\"";
                                        $scope.dyn_Cont += "ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"";
                                        $scope.dyn_Cont += "id=\"DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"  ng-options=\"opt.Id as opt.Caption for opt in  atributesRelationList[" + i + "].OptionValues \">";
                                        $scope.dyn_Cont += "</select>";
                                        $scope.dyn_Cont += "</div></div>";
                                        $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] = [];
                                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 10) {
                                        $scope.setoptions();
                                        $scope.items = [];
                                        $scope.items.push({
                                            startDate: '',
                                            endDate: ''
                                        });
                                        $scope.dyn_Cont += "<div class=\"control-group\"><span>" + $scope.atributesRelationList[i].DisplayName + "</span><div class=\"controls\"><div class=\"period nomargin\" id=\"periodcontrols\"  ng-form=\"subForm\">";
                                        $scope.dyn_Cont += "<div class=\"row-fluid\">";
                                        $scope.dyn_Cont += "<input class=\"sdate\" data-date-format='" + $scope.DefaultSettings.DateFormat + "' id=\"items.startDate\" type=\"text\" value=\"\" name=\"startDate\" ng-model=\"items.startDate\" ng-click=\"Calanderopen($event," + $scope.fields["DatePart_Calander_Open" + "items.startDate"] + ")\"  datepicker-popup=\"{{format}}\"  is-open=\"DatePart_Calander_Open" + "items.startDate" + "\" min-date=\"minDate\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"-- Start date --\" ng-change=\"OpenOptionsForPeriod()\"/><input class=\"edate\" type=\"text\" data-date-format='" + $scope.DefaultSettings.DateFormat + "' ng-click=\"Calanderopen($event," + $scope.fields["DatePart_Calander_Open" + "items.endDate"] + ")\"  datepicker-popup=\"{{format}}\"  is-open=\"DatePart_Calander_Open" + "items.endDate" + "\" min-date=\"minDate\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" value=\"\" name=\"enddate\" id=\"items.endDate\" ng-model=\"items.endDate\" placeholder=\"-- End date --\" ng-change=\"OpenOptionsForPeriod()\"/>";
                                        $scope.dyn_Cont += "</div></div></div></div>";
                                        $scope.FilterFields["Period_" + $scope.atributesRelationList[i].AttributeId] = [];
                                        $scope.setFieldKeys();
                                        $scope.dyn_Cont += "<div ng-show=\"ShowPeriodOptions\" class=\"control-group\" id=\"ShowPeriodOptions\" data-attrid=\"" + $scope.atributesRelationList[i].AttributeId + "\"><span>Period range : </span><div class=\"controls\">";
                                        $scope.dyn_Cont += "<select class=\"nomargin\" ui-select2 ng-model=\"PeriodOptionValue\"><option ng-repeat=\"ndata in PeriodOptions\" value=\"{{ndata.Id}}\">{{ndata.FilterPeriod}}</option></select></div></div>";
                                        $scope.ShowPeriodOptions = false;
                                        $scope.fields["DatePart_Calander_Open" + "items.startDate"] = false;
                                        $scope.fields["DatePart_Calander_Open" + "items.endDate"] = false;
                                    }
                                }
                            }
                            if ($scope.atributesRelationList.length > 0) $scope.atributeGroupList = $.grep($scope.atributesRelationList, function (e) {
                                return e.AttributeTypeId == 3 || e.AttributeTypeId == 4 || e.AttributeTypeId == 6 || e.AttributeTypeId == 7 || e.AttributeTypeId == 12
                            });
                            if (IsEntityType == "True") {
                                $scope.dyn_Cont += "<div class='control-group'>";
                                $scope.dyn_Cont += "<span>EntityType : </span><div class=\"controls\">";
                                $scope.dyn_Cont += "<input id='ddlChildren' class='width2x' placeholder='Select EntityType Options' type='hidden' ui-select2='tagAllOptions' ng-model='ddlParententitytypeId' />";
                                $scope.dyn_Cont += "</div></div>";
                            }
                            $scope.atributesRelationListWithTree = $.grep(entityAttributesRelation.Response, function (e) {
                                return e.AttributeTypeId == 7
                            })
                            for (var i = 0; i < $scope.atributesRelationListWithTree.length; i++) {
                                if ($scope.atributesRelationListWithTree[i].AttributeTypeId == 7) {
                                    $scope.treePreviewObj = {};
                                    $scope.treesrcdirec["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId] = JSON.parse($scope.atributesRelationListWithTree[i].tree).Children;
                                    if ($scope.treesrcdirec["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId].length > 0) {
                                        if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId])) {
                                            $scope.treePreviewObj["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId] = true;
                                        } else $scope.treePreviewObj["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId] = false;
                                    } else {
                                        $scope.treePreviewObj["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId] = false;
                                    }
                                    $scope.dyn_Cont += '<div class="control-group treeNode-control-group">';
                                    $scope.dyn_Cont += '<span>' + $scope.atributesRelationListWithTree[i].DisplayName + '</span>';
                                    $scope.dyn_Cont += '<div class="controls treeNode-controls">';
                                    $scope.dyn_Cont += '<div class="input-group treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + $scope.atributesRelationListWithTree[i].AttributeId + '" placeholder="Search" treecontext="treeNodeSearchDropdowns_Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '"></div>';
                                    $scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdowns_Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '">';
                                    $scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
                                    $scope.dyn_Cont += '<abn-tree tree-filter="filterValue_' + $scope.atributesRelationListWithTree[i].AttributeId + '" tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '\" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';
                                    $scope.dyn_Cont += '</div></div>';
                                    $scope.dyn_Cont += '<div class="control-group staticTreeGroup" ng-show=\"treePreviewObj.Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '\">';
                                    $scope.dyn_Cont += '<div class="controls">';
                                    $scope.dyn_Cont += '<eu-tree tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '\" node-attributeid="' + $scope.atributesRelationListWithTree[i].AttributeId + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                                    $scope.dyn_Cont += '</div></div></div>';
                                }
                            }
                            if (IsKeyword == "True") {
                                $scope.dyn_Cont += "<div class=\"control-group\"><span>Keyword : </span><input type='text' ng-model='ngKeywordtext' id='ngKeywordtext' placeholder='Filter Keyword'></div>";
                            }
                            if (IsEntityMember == "True") {
                                $scope.dyn_Cont += "<div class=\"control-group\"><span>Members </span> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model='ddlEntitymember'  id= 'ddlChildrenmember'     ng-options=\"ndata.id as (ndata.Name)  for ndata in  tagmemberOptions \"></select></div>";
                            }
                            $("#dynamic_Controls").html($compile($scope.dyn_Cont)($scope));
                            $scope.Applyfilter = true;
                            $scope.Updatefilter = false;
                            $scope.Deletefilter = false;
                            $scope.Savefilter = true;
                        });
                    });
                });
            });
        }
        $timeout(function () { $scope.FilterSettingsLoad() }, 100);
        $scope.OpenOptionsForPeriod = function () {
            $scope.PeriodOptionValue = {};
            $scope.PeriodOptionValue = 1;
            var startdate = $scope.items.startDate;
            var enddate = $scope.items.endDate;
            if (startdate != null && enddate != null) {
                if (startdate != undefined && enddate != undefined) {
                    $scope.ShowPeriodOptions = true;
                } else $scope.ShowPeriodOptions = false;
            } else $scope.ShowPeriodOptions = false;
        }
        $scope.FilterSave = function () {
            if (IsSave == 1) {
                return false;
            }
            IsSave = 1;
            if ($scope.ngsaveasfilter == '' || $scope.ngsaveasfilter == undefined) {
                bootbox.alert($translate.instant('LanguageContents.Res_1902.Caption'));
                IsSave = 0;
                return false;
            }
            var ApplyFilterobj = [];
            var FilterData = {};
            var whereConditionData = [];
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
            var fiscalyear = [];
            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeId == 6) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                        orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                        for (var k = 0; k < orgLevel.length; k++) {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                'Level': $scope.atributesRelationList[i].TreeLevel,
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 12) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                        orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                        for (var k = 0; k < orgLevel.length; k++) {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                'Level': $scope.atributesRelationList[i].TreeLevel,
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 13) {
                    var Array = $scope.atributesRelationList[i].DropdowntreePricingAttr.length;
                    for (var ii = 0; ii < Array; ii++) {
                        if ($scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection != null) {
                            var j = $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection.length;
                            for (var k = 0; k < j; k++) {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection[k],
                                    'Level': ii + 1,
                                    'AttributeTypeId': 13,
                                    'EntityTypeIDs': entitytypeIdforFilter,
                                    'Keyword': ''
                                });
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 3 && $scope.atributesRelationList[i].AttributeId == SystemDefiendAttributes.Owner) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] != undefined) {
                        usersVal = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId];
                        for (var k = 0; k < usersVal.length; k++) {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': usersVal[k],
                                'Level': 0,
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 7) {
                    var treenodes = [];
                    treenodes = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == $scope.atributesRelationList[i].AttributeId;
                    });
                    for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                        whereConditionData.push({
                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                            'SelectedValue': parseInt(nodeval.id, 10),
                            'Level': parseInt(nodeval.Level, 10),
                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                        });
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 3 || $scope.atributesRelationList[i].AttributeTypeId == 4) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] != undefined) {
                        fiscalyear = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId]
                        for (var k = 0; k < fiscalyear.length; k++) {
                            if (fiscalyear[k].Id != undefined) {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': fiscalyear[k].Id,
                                    'Level': 0,
                                    'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                                });
                            } else {
                                if ($scope.atributesRelationList[i].AttributeId == 71) {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': fiscalyear[k].id,
                                        'Level': 0,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                                    });
                                } else {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': fiscalyear[k],
                                        'Level': 0,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                                    });
                                }
                            }
                        }
                    }
                }
            }
            var filtertype = 2;
            if ($scope.attributegroupTypeid != 0 && $scope.attributegroupTypeid != undefined) {
                whereConditionData.splice(0, whereConditionData.length);
                var res = $.grep($scope.atributesRelationList, function (e) {
                    return e.AttributeId == parseInt($scope.attributegroupTypeid, 10)
                })[0];
                if (res.AttributeId == 71) {
                    for (var k = 0; k < res.EntityStatusOptionValues.length; k++) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': res.EntityStatusOptionValues[k].ID,
                            'Level': 0,
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'StartDate': "",
                            'EndDate': ""
                        });
                    }
                } else if (res.AttributeTypeId == 6 || res.AttributeTypeId == 12) {
                    for (var k = 0; k < res.LevelTreeNodes.length; k++) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': res.LevelTreeNodes[k].Id,
                            'Level': res.LevelTreeNodes[k].Level,
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'StartDate': "",
                            'EndDate': ""
                        });
                    }
                } else if (res.AttributeTypeId == 7) {
                    GetAllTreeObject(res.AttributeId);
                    var treenodes = [];
                    treenodes = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == res.AttributeId;
                    });
                    for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': parseInt(nodeval.id, 10),
                            'Level': parseInt(nodeval.Level, 10),
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': ''
                        });
                    }
                } else {
                    for (var k = 0; k < res.OptionValues.length; k++) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': res.OptionValues[k].Id,
                            'Level': 0,
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'StartDate': "",
                            'EndDate': ""
                        });
                    }
                }
                FilterData.IsDetailFilter = 6;
                filtertype = 6;
            } else FilterData.IsDetailFilter = 1;
            var entitytypeIdforFilter = '';
            if ($scope.ddlParententitytypeId != undefined) {
                if ($scope.ddlParententitytypeId[0] != undefined) {
                    for (var l = 0; l < $scope.ddlParententitytypeId.length; l++) {
                        if (entitytypeIdforFilter == '') {
                            entitytypeIdforFilter = $scope.ddlParententitytypeId[l].id;
                        } else {
                            entitytypeIdforFilter = entitytypeIdforFilter + "," + $scope.ddlParententitytypeId[l].id;
                        }
                    }
                }
            }
            var entitymemberIdforFilter = '';
            if ($scope.ddlEntitymember != undefined) {
                if ($scope.ddlEntitymember[0] != undefined) {
                    for (var x = 0; x < $scope.ddlEntitymember.length; x++) {
                        if (entitymemberIdforFilter == '') {
                            entitymemberIdforFilter = $scope.ddlEntitymember[x];
                        } else {
                            entitymemberIdforFilter = entitymemberIdforFilter + "," + $scope.ddlEntitymember[x];
                        }
                    }
                }
            }
            FilterData.FilterId = 0;
            FilterData.FilterName = $scope.ngsaveasfilter;
            FilterData.Keyword = $scope.ngKeywordtext;
            FilterData.UserId = 1;
            FilterData.TypeID = TypeID;
            if (entitytypeIdforFilter != '') {
                FilterData.entityTypeId = entitytypeIdforFilter + ',5';
            } else {
                FilterData.entityTypeId = '';
            }
            FilterData.EntitymemberId = entitymemberIdforFilter
            if ($scope.items.startDate != undefined && $scope.items.endDate != undefined) {
                FilterData.StarDate = dateFormat($scope.items.startDate, 'yyyy/mm/dd');
                FilterData.EndDate = dateFormat($scope.items.endDate, 'yyyy/mm/dd');
                whereConditionData.push({
                    'AttributeID': $("#ShowPeriodOptions").attr("data-attrid"),
                    'SelectedValue': $scope.PeriodOptionValue,
                    'Level': 0,
                    'AttributeTypeId': 10
                });
            } else {
                FilterData.EndDate = '';
                FilterData.StarDate = '';
            }
            FilterData.WhereConditon = whereConditionData;
            CcdetailfilterService.InsertFilterSettings(FilterData).then(function (filterSettingsInsertresult) {
                $('#FilterSettingsModal').modal('hide');
                if (filterSettingsInsertresult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4279.Caption'));
                    IsSave = 0;
                } else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4397.Caption'));
                    IsSave = 0;
                    $("#EntitiesTree").trigger("loadactivityfromfilterforCostCenter", [filterSettingsInsertresult.Response, ApplyFilterobj, filtertype]);
                    $("#costcentredetailfilter").trigger('reloadccdetailfilter', [TypeID, $scope.ngsaveasfilter, filterSettingsInsertresult.Response, filtertype]);
                }
            });
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
        };
        $("#costcentredetailfiltersettings").on('EditFilterSettingsByFilterID', function (event, filterId, typeId, filtertypeval) {
            $scope.showSave = false;
            $scope.showUpdate = true;
            $scope.deletefiltershow = true;
            $scope.LoadFilterSettingsByFilterID(event, filterId, typeId, filtertypeval);
        });
        $scope.LoadFilterSettingsByFilterID = function (event, filterId, typeId, filtertypeval) {
            $scope.ClearScopeModle();
            $scope.Updatefilter = true;
            $scope.Applyfilter = false;
            $scope.Deletefilter = true;
            $scope.Savefilter = false;
            $scope.filterSettingValues = [];
            selectedfilterid = filterId;
            CcdetailfilterService.GetFilterSettingValuesByFilertId(filterId).then(function (filterSettingsValues) {
                $scope.filterSettingValues = filterSettingsValues.Response;
                if ($scope.filterSettingValues != null) {
                    if (filtertypeval != 6) {
                        for (var i = 0; i < $scope.filterSettingValues.FilterValues.length; i++) {
                            if ($scope.filterSettingValues.FilterValues[i].AttributeTypeId == 3 && $scope.filterSettingValues.FilterValues[i].AttributeId == SystemDefiendAttributes.Owner) {
                                if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId] != undefined) $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId].push($scope.filterSettingValues.FilterValues[i].Value);
                            } else if ($scope.filterSettingValues.FilterValues[i].AttributeTypeId == 3 || $scope.filterSettingValues.FilterValues[i].AttributeTypeId == 4) {
                                if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId] != undefined) {
                                    if ($scope.filterSettingValues.FilterValues[i].AttributeId == 71) {
                                        var statusselectedoptions = $.grep($scope.atributesRelationList, function (e) {
                                            return e.AttributeId == parseInt(71);
                                        })[0].EntityStatusOptionValues;
                                        var seletedoptionvalue = [];
                                        for (var sts = 0; sts < $.grep($scope.filterSettingValues.FilterValues, function (e) {
                                                return e.AttributeId == parseInt(71);
                                        }).length; sts++) {
                                            seletedoptionvalue.push($.grep($scope.tagAllOptionsEntityStatus.data, function (e) {
                                                return e.id == parseInt($scope.filterSettingValues.FilterValues[sts].Value);
                                            })[0]);
                                            if (seletedoptionvalue != null) {
                                                $.each(seletedoptionvalue, function (i, el) {
                                                    $scope.tagAllOptionsEntityStatus.data.push({
                                                        "id": el.ID,
                                                        "text": el.StatusOptions,
                                                        "ShortDescription": el.ShortDesc,
                                                        "ColorCode": el.ColorCode
                                                    });
                                                });
                                            }
                                            $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = seletedoptionvalue;
                                        }
                                    } else {
                                        $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId].push($scope.filterSettingValues.FilterValues[i].Value);
                                    }
                                }
                            } else if ($scope.filterSettingValues.FilterValues[i].Level != 0 && $scope.filterSettingValues.FilterValues[i].AttributeTypeId == 6) {
                                if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId + "_" + $scope.filterSettingValues.FilterValues[i].Level] != undefined) {
                                    $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId + "_" + $scope.filterSettingValues.FilterValues[i].Level].push($scope.filterSettingValues.FilterValues[i].Value);
                                }
                            } else if ($scope.filterSettingValues.FilterValues[i].AttributeTypeId == 7) {
                                $scope.treesrcdirec["Attr_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = JSON.parse($scope.filterSettingValues.FilterValues[i].TreeValues).Children;
                                GetTreeObjecttoSave($scope.filterSettingValues.FilterValues[i].AttributeId);
                                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.filterSettingValues.FilterValues[i].AttributeId])) {
                                    $scope.treePreviewObj["Attr_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = true;
                                } else $scope.treePreviewObj["Attr_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = false;
                            } else if ($scope.filterSettingValues.FilterValues[i].Level != 0 && $scope.filterSettingValues.FilterValues[i].AttributeTypeId == 12) {
                                if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId + "_" + $scope.filterSettingValues.FilterValues[i].Level] != undefined) {
                                    $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId + "_" + $scope.filterSettingValues.FilterValues[i].Level].push($scope.filterSettingValues.FilterValues[i].Value);
                                }
                            } else if ($scope.filterSettingValues.FilterValues[i].AttributeTypeId == 13) {
                                $scope.DropDownFilterTreePricing["AttributeId_Levels_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = $scope.filterSettingValues.FilterValues[i].DropdowntreePricingData;
                            } else if ($scope.filterSettingValues.FilterValues[i].AttributeTypeId == 10) {
                                if ($scope.filterSettingValues.StartDate != null) {
                                    $scope.ShowPeriodOptions = true;
                                    $scope.PeriodOptionValue = $scope.filterSettingValues.FilterValues[i].Value;
                                }
                            }
                        }
                    } else $scope.attributegroupTypeid = $scope.filterSettingValues.FilterValues[0].AttributeId;
                    $scope.items.push({
                        startDate: $scope.filterSettingValues.StartDate,
                        endDate: $scope.filterSettingValues.EndDate
                    });
                    CcdetailfilterService.GettingEntityTypeHierarchyForAdminTree(6, 3).then(function (GerParentEntityData) {
                        $scope.entitytpesdata = GerParentEntityData.Response;
                        $scope.tagAllOptions.data = [];
                        if (GerParentEntityData.Response != null) {
                            $.each(GerParentEntityData.Response, function (i, el) {
                                $scope.tagAllOptions.data.push({
                                    "id": el.Id,
                                    "text": el.Caption,
                                    "ShortDescription": el.ShortDescription,
                                    "ColorCode": el.ColorCode
                                });
                            });
                        }
                    });
                    $scope.EntityHierarchyTypesResult = [];
                    for (var l = 0; l < filterSettingsValues.Response.EntityTypeID.split(',').length; l++) {
                        if (filterSettingsValues.Response.EntityTypeID.split(',')[l] != '0') {
                            if (filterSettingsValues.Response.EntityTypeID.split(',')[l] != "5") $scope.EntityHierarchyTypesResult.push($.grep($scope.tagAllOptions.data, function (e) {
                                return e.id == filterSettingsValues.Response.EntityTypeID.split(',')[l]
                            })[0]);
                        }
                    }
                    $scope.ddlParententitytypeId = $scope.EntityHierarchyTypesResult == undefined ? 0 : $scope.EntityHierarchyTypesResult[0] == undefined ? 0 : $scope.EntityHierarchyTypesResult;
                    if (filterSettingsValues.Response.EntityMemberID != "" && filterSettingsValues.Response.EntityMemberID != null) {
                        for (var l = 0; l < filterSettingsValues.Response.EntityMemberID.split(',').length; l++) {
                            if (filterSettingsValues.Response.EntityMemberID.split(',')[l] != '0') {
                                if ($.grep($scope.tagmemberOptions, function (e) {
                                        return e.id == filterSettingsValues.Response.EntityMemberID.split(',')[l]
                                })[0] != undefined) {
                                    var mval = filterSettingsValues.Response.EntityMemberID.split(',')[l];
                                    if (mval != "" && mval != undefined) $scope.ddlEntitymember.push(parseInt(mval));
                                }
                            }
                        }
                    }
                    var startdateupdate = $scope.filterSettingValues.StartDate;
                    var enddateupdate = $scope.filterSettingValues.EndDate;
                    var editstartdate = startdateupdate.split('/');
                    var editenddate = enddateupdate.split('/');
                    startdateupdate = dateFormat(startdateupdate, $scope.DefaultSettings.DateFormat);
                    enddateupdate = dateFormat(enddateupdate, $scope.DefaultSettings.DateFormat);
                    if ($scope.filterSettingValues.StartDate == "" || $scope.filterSettingValues.EndDate == "") {
                        $scope.items.push({
                            startDate: "",
                            endDate: ""
                        });
                    } else if ($scope.filterSettingValues.StartDate != DateValidate || $scope.filterSettingValues.EndDate != DateValidate) {
                        $scope.items.startDate = startdateupdate;
                        $scope.items.endDate = enddateupdate;
                    } else {
                        $scope.items.push({
                            startDate: DateValidate,
                            endDate: DateValidate
                        });
                    }
                    $scope.ngKeywordtext = $scope.filterSettingValues.Keyword;
                    $scope.ngsaveasfilter = $scope.filterSettingValues.FilterName;
                    $scope.Updatefilter = true;
                    $scope.Applyfilter = false;
                    $scope.Deletefilter = true;
                    $scope.Savefilter = false;
                }
            });
        }
        $scope.FilterUpdate = function () {
            if (IsUpdate == 1) {
                return false;
            }
            IsUpdate = 1;
            var ApplyFilterobj = [];
            var FilterData = {};
            $scope.filterSettingValues.Keyword = '';
            $scope.filterSettingValues.FilterName = '';
            var whereConditionData = [];
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
            var fiscalyear = [];
            var entitytypeIdforFilter = '';
            if ($scope.ddlParententitytypeId != undefined) {
                if ($scope.ddlParententitytypeId[0] != undefined) {
                    for (var l = 0; l < $scope.ddlParententitytypeId.length; l++) {
                        if (entitytypeIdforFilter == '') {
                            entitytypeIdforFilter = $scope.ddlParententitytypeId[l].id;
                        } else {
                            entitytypeIdforFilter = entitytypeIdforFilter + "," + $scope.ddlParententitytypeId[l].id;
                        }
                    }
                }
            }
            var entitymemberIdforFilter = '';
            if ($scope.ddlEntitymember != undefined) {
                if ($scope.ddlEntitymember[0] != undefined) {
                    for (var x = 0; x < $scope.ddlEntitymember.length; x++) {
                        if (entitymemberIdforFilter == '') {
                            entitymemberIdforFilter = $scope.ddlEntitymember[x];
                        } else {
                            entitymemberIdforFilter = entitymemberIdforFilter + "," + $scope.ddlEntitymember[x];
                        }
                    }
                }
            }
            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeId == 6) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                        orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                        for (var k = 0; k < orgLevel.length; k++) {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                'Level': $scope.atributesRelationList[i].TreeLevel,
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                'EntityTypeIDs': entitytypeIdforFilter,
                                'EntityMemberIDs': entitymemberIdforFilter
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 12) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                        orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                        for (var k = 0; k < orgLevel.length; k++) {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                'Level': $scope.atributesRelationList[i].TreeLevel,
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                'EntityTypeIDs': entitytypeIdforFilter,
                                'EntityMemberIDs': entitymemberIdforFilter
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 13) {
                    var Array = $scope.atributesRelationList[i].DropdowntreePricingAttr.length;
                    for (var ii = 0; ii < Array; ii++) {
                        if ($scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection != null) {
                            var j = $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection.length;
                            for (var k = 0; k < j; k++) {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection[k],
                                    'Level': ii + 1,
                                    'AttributeTypeId': 13,
                                    'EntityTypeIDs': entitytypeIdforFilter,
                                    'Keyword': ''
                                });
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 3 && $scope.atributesRelationList[i].AttributeId == SystemDefiendAttributes.Owner) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] != undefined) {
                        usersVal = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId];
                        for (var k = 0; k < usersVal.length; k++) {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': usersVal[k],
                                'Level': 0,
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                'EntityTypeIDs': entitytypeIdforFilter,
                                'EntityMemberIDs': entitymemberIdforFilter
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 7) {
                    var treenodes = [];
                    treenodes = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == $scope.atributesRelationList[i].AttributeId;
                    });
                    for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                        whereConditionData.push({
                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                            'SelectedValue': parseInt(nodeval.id, 10),
                            'Level': parseInt(nodeval.Level, 10),
                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 3 || $scope.atributesRelationList[i].AttributeTypeId == 4) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] != undefined) {
                        fiscalyear = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId]
                        for (var k = 0; k < fiscalyear.length; k++) {
                            if (fiscalyear[k].Id != undefined) {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': fiscalyear[k].Id,
                                    'Level': 0,
                                    'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                    'EntityTypeIDs': entitytypeIdforFilter,
                                    'EntityMemberIDs': entitymemberIdforFilter
                                });
                            } else {
                                if ($scope.atributesRelationList[i].AttributeId == 71) {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': fiscalyear[k].id,
                                        'Level': 0,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                        'EntityTypeIDs': entitytypeIdforFilter,
                                        'EntityMemberIDs': entitymemberIdforFilter
                                    });
                                } else {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': fiscalyear[k],
                                        'Level': 0,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                        'EntityTypeIDs': entitytypeIdforFilter,
                                        'EntityMemberIDs': entitymemberIdforFilter
                                    });
                                }
                            }
                        }
                    }
                }
            }
            FilterData.FilterId = selectedfilterid;
            FilterData.TypeID = TypeID;
            FilterData.FilterName = $scope.ngsaveasfilter;
            FilterData.Keyword = $scope.ngKeywordtext;
            FilterData.UserId = 1;
            FilterData.entityTypeId = entitytypeIdforFilter;
            FilterData.IsDetailFilter = 1;
            FilterData.EntitymemberId = entitymemberIdforFilter
            if ($scope.items.startDate != undefined && $scope.items.endDate != undefined) {
                FilterData.StarDate = dateFormat($scope.items.startDate, 'yyyy/mm/dd');
                FilterData.EndDate = dateFormat($scope.items.endDate, 'yyyy/mm/dd');
                whereConditionData.push({
                    'AttributeID': $("#ShowPeriodOptions").attr("data-attrid"),
                    'SelectedValue': $scope.PeriodOptionValue,
                    'Level': 0,
                    'AttributeTypeId': 10,
                    'EntityTypeIDs': entitytypeIdforFilter,
                    'EntityMemberIDs': entitymemberIdforFilter
                });
            } else {
                FilterData.EndDate = '';
                FilterData.StarDate = '';
            }
            var filtertype = 2;
            if ($scope.attributegroupTypeid != 0 && $scope.attributegroupTypeid != undefined) {
                whereConditionData.splice(0, whereConditionData.length);
                var res = $.grep($scope.atributesRelationList, function (e) {
                    return e.AttributeId == parseInt($scope.attributegroupTypeid, 10)
                })[0];
                if (res.AttributeId == 71) {
                    for (var k = 0; k < res.EntityStatusOptionValues.length; k++) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': res.EntityStatusOptionValues[k].ID,
                            'Level': 0,
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'StartDate': "",
                            'EndDate': "",
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                } else if (res.AttributeTypeId == 6 || res.AttributeTypeId == 12) {
                    for (var k = 0; k < res.LevelTreeNodes.length; k++) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': res.LevelTreeNodes[k].Id,
                            'Level': res.LevelTreeNodes[k].Level,
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'StartDate': "",
                            'EndDate': "",
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                } else if (res.AttributeTypeId == 7) {
                    GetAllTreeObject(res.AttributeId);
                    var treenodes = [];
                    treenodes = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == res.AttributeId;
                    });
                    for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': parseInt(nodeval.id, 10),
                            'Level': parseInt(nodeval.Level, 10),
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                } else {
                    for (var k = 0; k < res.OptionValues.length; k++) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': res.OptionValues[k].Id,
                            'Level': 0,
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'StartDate': "",
                            'EndDate': "",
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                }
                filtertype = 6;
                FilterData.IsDetailFilter = 6;
            } else FilterData.IsDetailFilter = 1;
            if (whereConditionData.length == 0 && entitymemberIdforFilter != '') {
                whereConditionData.push({
                    'AttributeID': 0,
                    'SelectedValue': 0,
                    'Level': 0,
                    'AttributeTypeId': 0,
                    'EntityTypeIDs': entitytypeIdforFilter,
                    'Keyword': '',
                    'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                    'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                    'EntityMemberIDs': entitymemberIdforFilter
                });
            }
            FilterData.WhereConditon = whereConditionData;
            CcdetailfilterService.InsertFilterSettings(FilterData).then(function (filterSettingsUpdateResult) {
                if (filterSettingsUpdateResult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4358.Caption'));
                    IsUpdate = 0;
                } else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4400.Caption'));
                    IsUpdate = 0;
                    $scope.appliedfilter = $scope.ngsaveasfilter;
                    $("#EntitiesTree").trigger("loadactivityfromfilterforCostCenter", [filterSettingsUpdateResult.Response, ApplyFilterobj, filtertype]);
                    $("#costcentredetailfilter").trigger('reloadccdetailfilter', [TypeID, $scope.ngsaveasfilter, filterSettingsUpdateResult.Response, filtertype]);
                }
            });
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
        };
        $scope.FilterID = 0;
        $("#costcentredetailfiltersettings").on('applyplandetailfilter', function (event, FilterID, FilterName, filterval) {
            $scope.ApplyFilter(FilterID, FilterName, filterval);
        });
        $scope.ApplyFilter = function (FilterID, FilterName, filterval) {
            var filtertype = 2;
            $('#FilterSettingsModal').modal('hide');
            var StartRowNo = 0;
            var MaxNoofRow = 30;
            var PageIndex = 0;
            if (FilterID != undefined) {
                selectedfilterid = FilterID;
                $scope.FilterID = FilterID;
                filtertype = filterval;
            }
            $scope.appliedfilter = FilterName;
            $window.CostCentreFilterName = FilterName;
            if (FilterID == 0) {
                $scope.appliedfilter = "No filter applied";
            }
            var whereConditionData = [];
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
            var fiscalyear = [];
            var entitytypeIdforFilter = '';
            if ($scope.ddlParententitytypeId != undefined) {
                if ($scope.ddlParententitytypeId[0] != undefined) {
                    for (var l = 0; l < $scope.ddlParententitytypeId.length; l++) {
                        if (entitytypeIdforFilter == '') {
                            entitytypeIdforFilter = $scope.ddlParententitytypeId[l].id;
                        } else {
                            entitytypeIdforFilter = entitytypeIdforFilter + "," + $scope.ddlParententitytypeId[l].id;
                        }
                    }
                }
            }
            var entitymemberIdforFilter = '';
            if ($scope.ddlEntitymember != undefined) {
                if ($scope.ddlEntitymember[0] != undefined) {
                    for (var x = 0; x < $scope.ddlEntitymember.length; x++) {
                        if (entitymemberIdforFilter == '') {
                            entitymemberIdforFilter = $scope.ddlEntitymember[x];
                        } else {
                            entitymemberIdforFilter = entitymemberIdforFilter + "," + $scope.ddlEntitymember[x];
                        }
                    }
                }
            }
            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeId == 6) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                        orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                        for (var k = 0; k < orgLevel.length; k++) {
                            if ($scope.ngKeywordtext != '') {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                    'Level': $scope.atributesRelationList[i].TreeLevel,
                                    'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                    'EntityTypeIDs': '',
                                    'Keyword': $scope.ngKeywordtext,
                                    'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                    'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                    'EntityMemberIDs': ''
                                });
                            } else {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                    'Level': $scope.atributesRelationList[i].TreeLevel,
                                    'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                    'EntityTypeIDs': '',
                                    'Keyword': '',
                                    'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                    'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                    'EntityMemberIDs': ''
                                });
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 12) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                        orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                        for (var k = 0; k < orgLevel.length; k++) {
                            if ($scope.ngKeywordtext != '') {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                    'Level': $scope.atributesRelationList[i].TreeLevel,
                                    'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                    'EntityTypeIDs': '',
                                    'Keyword': $scope.ngKeywordtext,
                                    'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                    'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                    'EntityMemberIDs': ''
                                });
                            } else {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                    'Level': $scope.atributesRelationList[i].TreeLevel,
                                    'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                    'EntityTypeIDs': '',
                                    'Keyword': '',
                                    'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                    'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                    'EntityMemberIDs': ''
                                });
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 13) {
                    var Array = $scope.atributesRelationList[i].DropdowntreePricingAttr.length;
                    for (var ii = 0; ii < Array; ii++) {
                        if ($scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection != null) {
                            var j = $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection.length;
                            for (var k = 0; k < j; k++) {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection[k],
                                    'Level': ii + 1,
                                    'AttributeTypeId': 13,
                                    'EntityTypeIDs': '',
                                    'Keyword': '',
                                    'EntityMemberIDs': ''
                                });
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 7) {
                    var treenodes = [];
                    treenodes = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == $scope.atributesRelationList[i].AttributeId;
                    });
                    for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                        if ($scope.ngKeywordtext != '') whereConditionData.push({
                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                            'SelectedValue': parseInt(nodeval.id, 10),
                            'Level': parseInt(nodeval.Level, 10),
                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': $scope.ngKeywordtext,
                            'EntityMemberIDs': ''
                        });
                        else whereConditionData.push({
                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                            'SelectedValue': parseInt(nodeval.id, 10),
                            'Level': parseInt(nodeval.Level, 10),
                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'EntityMemberIDs': ''
                        });
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 3 && $scope.atributesRelationList[i].AttributeId == SystemDefiendAttributes.Owner) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] != undefined) {
                        usersVal = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId];
                        for (var k = 0; k < usersVal.length; k++) {
                            if ($scope.ngKeywordtext != '') {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': usersVal[k],
                                    'Level': 0,
                                    'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                    'EntityTypeIDs': '',
                                    'Keyword': $scope.ngKeywordtext,
                                    'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                    'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                    'EntityMemberIDs': ''
                                });
                            } else {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': usersVal[k],
                                    'Level': 0,
                                    'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                    'EntityTypeIDs': '',
                                    'Keyword': '',
                                    'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                    'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                    'EntityMemberIDs': ''
                                });
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 3 || $scope.atributesRelationList[i].AttributeTypeId == 4) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] != undefined) {
                        fiscalyear = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId]
                        for (var k = 0; k < fiscalyear.length; k++) {
                            if (fiscalyear[k].Id != undefined) {
                                if ($scope.ngKeywordtext != '') {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': fiscalyear[k].Id,
                                        'Level': 0,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                        'EntityTypeIDs': '5',
                                        'Keyword': $scope.ngKeywordtext,
                                        'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                        'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                        'EntityMemberIDs': ''
                                    });
                                } else {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': fiscalyear[k].Id,
                                        'Level': 0,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                        'EntityTypeIDs': '',
                                        'Keyword': '',
                                        'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                        'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                        'EntityMemberIDs': ''
                                    });
                                }
                            } else {
                                if ($scope.ngKeywordtext != '') {
                                    if ($scope.atributesRelationList[i].AttributeId == 71) {
                                        whereConditionData.push({
                                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                            'SelectedValue': fiscalyear[k].id,
                                            'Level': 0,
                                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                            'EntityTypeIDs': '5',
                                            'Keyword': $scope.ngKeywordtext,
                                            'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                            'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                            'EntityMemberIDs': ''
                                        });
                                    } else {
                                        whereConditionData.push({
                                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                            'SelectedValue': fiscalyear[k],
                                            'Level': 0,
                                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                            'EntityTypeIDs': '5',
                                            'Keyword': $scope.ngKeywordtext,
                                            'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                            'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                            'EntityMemberIDs': ''
                                        });
                                    }
                                } else {
                                    if ($scope.atributesRelationList[i].AttributeId == 71) {
                                        whereConditionData.push({
                                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                            'SelectedValue': fiscalyear[k].id,
                                            'Level': 0,
                                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                            'EntityTypeIDs': '',
                                            'Keyword': '',
                                            'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                            'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                            'EntityMemberIDs': ''
                                        });
                                    } else {
                                        whereConditionData.push({
                                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                            'SelectedValue': fiscalyear[k],
                                            'Level': 0,
                                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                            'EntityTypeIDs': '',
                                            'Keyword': '',
                                            'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                            'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                            'EntityMemberIDs': ''
                                        });
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (entitytypeIdforFilter != '') {
                if ($scope.ngKeywordtext != '') {
                    whereConditionData.push({
                        'AttributeID': 0,
                        'SelectedValue': 0,
                        'Level': 0,
                        'AttributeTypeId': 0,
                        'EntityTypeIDs': entitytypeIdforFilter + ',5',
                        'Keyword': $scope.ngKeywordtext,
                        'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                        'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                        'EntityMemberIDs': entitymemberIdforFilter
                    });
                } else {
                    whereConditionData.push({
                        'AttributeID': 0,
                        'SelectedValue': 0,
                        'Level': 0,
                        'AttributeTypeId': 0,
                        'EntityTypeIDs': entitytypeIdforFilter + ',5',
                        'Keyword': '',
                        'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                        'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                        'EntityMemberIDs': entitymemberIdforFilter
                    });
                }
            }
            if (whereConditionData.length == 0 && $scope.ngKeywordtext != '') {
                whereConditionData.push({
                    'AttributeID': 0,
                    'SelectedValue': 0,
                    'Level': 0,
                    'AttributeTypeId': 0,
                    'EntityTypeIDs': entitytypeIdforFilter,
                    'Keyword': $scope.ngKeywordtext,
                    'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                    'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                    'EntityMemberIDs': entitymemberIdforFilter
                });
            }
            if (whereConditionData.length == 0 && ($scope.items.startDate != undefined || $scope.items.endDate != undefined)) {
                whereConditionData.push({
                    'AttributeID': 0,
                    'SelectedValue': 0,
                    'Level': 0,
                    'AttributeTypeId': 0,
                    'EntityTypeIDs': entitytypeIdforFilter,
                    'Keyword': $scope.ngKeywordtext,
                    'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                    'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                    'EntityMemberIDs': entitymemberIdforFilter
                });
            }
            if (whereConditionData.length == 0 && $scope.entitymemberIdforFilter != '') {
                whereConditionData.push({
                    'AttributeID': 0,
                    'SelectedValue': 0,
                    'Level': 0,
                    'AttributeTypeId': 0,
                    'EntityTypeIDs': entitytypeIdforFilter,
                    'Keyword': $scope.ngKeywordtext,
                    'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                    'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                    'EntityMemberIDs': entitymemberIdforFilter
                });
            }
            var StartDate;
            var EndDate;
            var ApplyFilterobj = [];
            var optionperiod = $scope.PeriodOptionValue;
            if ($scope.items.startDate == undefined) optionperiod = 0;
            if ($scope.items.endDate == undefined) optionperiod = 0;
            if ($scope.items.startDate != undefined && $scope.items.endDate != undefined) optionperiod = $scope.PeriodOptionValue;
            if ($scope.items.startDate != undefined) StartDate = dateFormat($scope.items.startDate, 'yyyy/mm/dd');
            if ($scope.items.endDate != undefined) EndDate = dateFormat($scope.items.endDate, 'yyyy/mm/dd');
            if ($scope.items.startDate != undefined || $scope.items.endDate != undefined) whereConditionData.push({
                'AttributeID': $("#ShowPeriodOptions").attr("data-attrid"),
                'SelectedValue': optionperiod,
                'Level': 0,
                'AttributeTypeId': 10,
                'EntityTypeIDs': entitytypeIdforFilter,
                'Keyword': $scope.ngKeywordtext,
                'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                'EntityMemberIDs': entitymemberIdforFilter
            });
            var ApplyFilterobj = [];
            if ($scope.attributegroupTypeid != 0 && $scope.attributegroupTypeid != undefined) {
                whereConditionData.splice(0, whereConditionData.length);
                var res = $.grep($scope.atributesRelationList, function (e) {
                    return e.AttributeId == parseInt($scope.attributegroupTypeid, 10)
                })[0];
                if (res.AttributeId == 71) {
                    for (var k = 0; k < res.EntityStatusOptionValues.length; k++) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': res.EntityStatusOptionValues[k].ID,
                            'Level': 0,
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'StartDate': "",
                            'EndDate': "",
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                } else if (res.AttributeTypeId == 6 || res.AttributeTypeId == 12) {
                    for (var k = 0; k < res.LevelTreeNodes.length; k++) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': res.LevelTreeNodes[k].Id,
                            'Level': res.LevelTreeNodes[k].Level,
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'StartDate': "",
                            'EndDate': "",
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                } else if (res.AttributeTypeId == 7) {
                    GetAllTreeObject(res.AttributeId);
                    var treenodes = [];
                    treenodes = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == res.AttributeId;
                    });
                    for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': parseInt(nodeval.id, 10),
                            'Level': parseInt(nodeval.Level, 10),
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                } else if (res.AttributeTypeId == 3 && res.AttributeId == SystemDefiendAttributes.Owner) {
                    for (var k = 0; k < res.Users.length; k++) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': res.Users[k].Id,
                            'Level': 0,
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'StartDate': "",
                            'EndDate': "",
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                } else {
                    for (var k = 0; k < res.OptionValues.length; k++) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': res.OptionValues[k].Id,
                            'Level': 0,
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'StartDate': "",
                            'EndDate': "",
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                }
                ApplyFilterobj = whereConditionData;
                filtertype = 6;
            } else {
                if (FilterID != 0) {
                    ApplyFilterobj = whereConditionData;
                } else {
                    ApplyFilterobj = [];
                }
            }
            $("#EntitiesTree").trigger("loadactivityfromfilterforCostCenter", [FilterID, ApplyFilterobj, filtertype])
        };
        $scope.filtersettingsreset = function () {
            var StartRowNo = 0;
            var MaxNoofRow = 30;
            var PageIndex = 0;
            selectedfilterid = 0;
        };
        $scope.DeleteFilter = function DeleteFilterSettingsValue() {
            var ApplyFilterobj = [];
            bootbox.confirm($translate.instant('LanguageContents.Res_2026.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        var ID = selectedfilterid;
                        CcdetailfilterService.DeleteFilterSettings(ID).then(function (deletefilterbyFilterId) {
                            if (deletefilterbyFilterId.StatusCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_4299.Caption'));
                            } else {
                                NotifySuccess($translate.instant('LanguageContents.Res_4398.Caption'));
                                $("#EntitiesTree").trigger("loadactivityfromfilterforCostCenter", [0, ApplyFilterobj]);
                                $("#costcentredetailfilter").trigger('reloadccdetailfilter', [TypeID, 'No filter applied', 0]);
                            }
                        });
                    }, 100);
                }
            });
        };
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.costcentre.detail.detailfilterCtrl']"));
        });
        $scope.tagAllOptionsEntityStatus = {
            multiple: true,
            allowClear: true,
            data: $scope.ReassignMembersDataEntityStatus,
            formatResult: $scope.formatResultEntityStatus,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };
        $scope.tagAllOptions = {
            multiple: true,
            allowClear: true,
            data: $scope.ReassignMembersData,
            formatResult: $scope.formatResult,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };
        $scope.ReassignMembersData = [];
        $scope.ReassignMembersDataEntityStatus = [];
        $scope.formatResult = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.formatResultEntityStatus = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.formatSelection = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.formatSelectionForEntityStauts = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.tagAllOptions = {
            multiple: true,
            allowClear: true,
            data: $scope.ReassignMembersData,
            formatResult: $scope.formatResult,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };
        $scope.tagAllOptionsEntityStatus = {
            multiple: true,
            allowClear: true,
            data: $scope.ReassignMembersDataEntityStatus,
            formatResult: $scope.formatResultEntityStatus,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };

        function IsNotEmptyTree(treeObj) {
            var flag = false;
            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    flag = true;
                    return flag;
                }
            }
            return flag;
        }

        function ClearSavedTreeNode(attributeid) {
            for (var i = 0, branch; branch = $scope.treesrcdirec["Attr_" + attributeid][i++];) {
                branch.ischecked = false;
                if (branch.Children.length > 0) {
                    ClearRecursiveChildTreenode(branch.Children);
                }
            }
        }

        function ClearRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                child.ischecked = false;
                if (child.Children.length > 0) {
                    ClearRecursiveChildTreenode(child.Children);
                }
            }
        }

        function GetTreeObjecttoSave(attributeid) {
            $scope.treeNodeSelectedHolderValues = [];
            for (var i = 0, branch; branch = $scope.treesrcdirec["Attr_" + attributeid][i++];) {
                if (branch.ischecked == true) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == branch.AttributeId && e.id == branch.id;
                    });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolderValues.push(branch);
                    }
                    if (branch.Children.length > 0) {
                        FormRecursiveChildTreenode(branch.Children);
                    }
                }
            }
        }

        function GetAllTreeObject(attributeid) {
            $scope.treeNodeSelectedHolderValues = [];
            for (var i = 0, branch; branch = $scope.treesrcdirec["Attr_" + attributeid][i++];) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length == 0) {
                    $scope.treeNodeSelectedHolderValues.push(branch);
                }
                if (branch.Children.length > 0) {
                    FormRecursiveChildTreenode(branch.Children);
                }
            }
        }

        function FormRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                if (child.ischecked == true) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == child.AttributeId && e.id == child.id;
                    });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolderValues.push(child);
                        if (child.Children.length > 0) {
                            FormRecursiveChildTreenode(child.Children);
                        }
                    }
                }
            }
        }
        var apple_selected, tree, treedata_avm, treedata_geography;
        $scope.my_tree_handler = function (branch, parentArr) {
            var _ref;
            $scope.output = "You selected: " + branch.Caption;
            if ((_ref = branch.data) != null ? _ref.description : void 0) {
                return $scope.output += '(' + branch.data.description + ')';
            }
            if (branch.ischecked == true) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length == 0) {
                    $scope.treeNodeSelectedHolderValues.push(branch);
                }
                for (var i = 0, parent; parent = parentArr[i++];) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == parent.AttributeId && e.id == parent.id;
                    });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolderValues.push(parent);
                    }
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolderValues.splice($scope.treeNodeSelectedHolderValues.indexOf(branch), 1);
                    if (branch.Children.length > 0) {
                        RemoveRecursiveChildTreenode(branch.Children);
                    }
                }
            }
            if ($scope.treesrcdirec["Attr_" + branch.AttributeId].length > 0) {
                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + branch.AttributeId])) {
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = true;
                } else $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            } else {
                $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }

            function RemoveRecursiveChildTreenode(children) {
                for (var j = 0, child; child = children[j++];) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == child.AttributeId && e.id == child.id;
                    });
                    if (remainRecord.length > 0) {
                        $scope.treeNodeSelectedHolderValues.splice($.inArray(child, $scope.treeNodeSelectedHolderValues), 1);
                        if (child.Children.length > 0) {
                            RemoveRecursiveChildTreenode(child.Children);
                        }
                    }
                }
            }
        }
    }
    app.controller("mui.planningtool.costcentre.detail.detailfilterCtrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$compile', '$window', '$translate', 'CcdetailfilterService', muiplanningtoolcostcentredetaildetailfilterCtrl]);
})(angular, app);
///#source 1 1 /app/services/ccdetail-service.js
(function(ng,app){"use strict";function CcdetailService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({GetCurrencyListFFsettings:GetCurrencyListFFsettings,GetEntitysLinkedToCostCenter:GetEntitysLinkedToCostCenter,GetPeriodByIdForGantt:GetPeriodByIdForGantt,GetFilterSettingsForDetail:GetFilterSettingsForDetail,GetCostCentreAssignedAmount:GetCostCentreAssignedAmount,GetCostCentreCurrencyRateById:GetCostCentreCurrencyRateById,GetAdminSettingselemntnode:GetAdminSettingselemntnode,ShowReports:ShowReports,GetFinancialReportSettings:GetFinancialReportSettings,GetStrucuralRptDetail:GetStrucuralRptDetail,CostCentreDetailReportSystem:CostCentreDetailReportSystem,CostCentreDetail:CostCentreDetail,GetEntityTypeByID:GetEntityTypeByID,CostCentreTreeDetail:CostCentreTreeDetail,GetEntityTypefromDB:GetEntityTypefromDB,CostCentreDetailReport:CostCentreDetailReport});function GetCurrencyListFFsettings(){var request=$http({method:"get",url:"api/Planning/GetCurrencyListFFsettings/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetEntitysLinkedToCostCenter(ID){var request=$http({method:"get",url:"api/Planning/GetEntitysLinkedToCostCenter/"+ID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetPeriodByIdForGantt(ID){var request=$http({method:"get",url:"api/Planning/GetPeriodByIdForGantt/"+ID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetFilterSettingsForDetail(TypeID){var request=$http({method:"get",url:"api/Planning/GetFilterSettingsForDetail/"+TypeID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetCostCentreAssignedAmount(CostCentreID){var request=$http({method:"get",url:"api/Planning/GetCostCentreAssignedAmount/"+CostCentreID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetCostCentreCurrencyRateById(EntityId,CurrencyId,IsCostCentreCreation){var request=$http({method:"get",url:"api/Planning/GetCostCentreCurrencyRateById/"+EntityId+"/"+CurrencyId+"/"+IsCostCentreCreation,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetAdminSettingselemntnode(LogoSettings,elemntnode,typeid){var request=$http({method:"get",url:"api/common/GetAdminSettingselemntnode/"+LogoSettings+"/"+elemntnode+"/"+typeid,params:{action:"get",}});return(request.then(handleSuccess,handleError));}
function ShowReports(OID,Show){var request=$http({method:"get",url:"api/Report/ShowReports/"+OID+"/"+Show,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetFinancialReportSettings(){var request=$http({method:"get",url:"api/Report/GetFinancialReportSettings/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetStrucuralRptDetail(IsshowFinancialDetl,IsDetailIncluded,IsshowTaskDetl,IsshowMemberDetl,ExpandingEntityIDStr,IncludeChildrenStr){var request=$http({method:"get",url:"api/Report/GetStrucuralRptDetail/"+IsshowFinancialDetl+"/"+IsDetailIncluded+"/"+IsshowTaskDetl+"/"+IsshowMemberDetl+"/"+ExpandingEntityIDStr+"/"+IncludeChildrenStr,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function CostCentreDetailReportSystem(formobj){var request=$http({method:"post",url:"api/Report/CostCentreDetailReportSystem/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function CostCentreDetail(formobj){var request=$http({method:"post",url:"api/Metadata/CostCentreDetail/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetEntityTypeByID(ID){var request=$http({method:"get",url:"api/Metadata/GetEntityTypeByID/"+ID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function CostCentreTreeDetail(formobj){var request=$http({method:"post",url:"api/Metadata/CostCentreTreeDetail/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetEntityTypefromDB(){var request=$http({method:"get",url:"api/Metadata/GetEntityTypefromDB/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function CostCentreDetailReport(formobj){var request=$http({method:"post",url:"api/Metadata/CostCentreDetailReport/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}}
app.service("CcdetailService",['$http','$q',CcdetailService]);})(angular,app);
///#source 1 1 /app/services/SubCostCentreCreation-service.js
(function(ng,app){"use strict";function SubcostcentrecreationService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({GetUserById:GetUserById,CreateCostcentre:CreateCostcentre,GetCurrencyListFFsettings:GetCurrencyListFFsettings,GetCostCentreCurrencyRateById:GetCostCentreCurrencyRateById,GetAttributeToAttributeRelationsByIDForEntity:GetAttributeToAttributeRelationsByIDForEntity,GetEntityTypeAttributeRelationWithLevelsByID:GetEntityTypeAttributeRelationWithLevelsByID,GetValidationDationByEntitytype:GetValidationDationByEntitytype,GetEntityTypeRoleAccess:GetEntityTypeRoleAccess, GetEntityStatus: GetEntityStatus});function GetUserById(ID){var request=$http({method:"get",url:"api/user/GetUserById/"+ID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function CreateCostcentre(formobj){var request=$http({method:"post",url:"api/Planning/CreateCostcentre/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetCurrencyListFFsettings(){var request=$http({method:"get",url:"api/Planning/GetCurrencyListFFsettings/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetCostCentreCurrencyRateById(EntityId,CurrencyId,IsCostCentreCreation){var request=$http({method:"get",url:"api/Planning/GetCostCentreCurrencyRateById/"+EntityId+"/"+CurrencyId+"/"+IsCostCentreCreation,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetAttributeToAttributeRelationsByIDForEntity(ID){var request=$http({method:"get",url:"api/Metadata/GetAttributeToAttributeRelationsByIDForEntity/"+ID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetEntityTypeAttributeRelationWithLevelsByID(ID,ParentID){var request=$http({method:"get",url:"api/Metadata/GetEntityTypeAttributeRelationWithLevelsByID/"+ID+"/"+ParentID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetValidationDationByEntitytype(EntityTypeID){var request=$http({method:"get",url:"api/Metadata/GetValidationDationByEntitytype/"+EntityTypeID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetEntityTypeRoleAccess(EntityTypeID) { var request = $http({ method: "get", url: "api/access/GetEntityTypeRoleAccess/" + EntityTypeID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
function GetEntityStatus(entityTypeID, isAdmin, entityId) { var request = $http({ method: "get", url: "api/Metadata/GetEntityStatus/" + entityTypeID + "/" + isAdmin + "/" + entityId, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}}
app.service("SubcostcentrecreationService",['$http','$q',SubcostcentrecreationService]);})(angular,app);
///#source 1 1 /app/services/ccdetailfilter-service.js
(function(ng,app){"use strict";function CcdetailfilterService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({GetOptionsFromXML:GetOptionsFromXML,GettingEntityTypeHierarchyForAdminTree:GettingEntityTypeHierarchyForAdminTree,GettingFilterEntityMember:GettingFilterEntityMember,GettingFilterAttribute:GettingFilterAttribute,InsertFilterSettings:InsertFilterSettings,GetFilterSettingValuesByFilertId:GetFilterSettingValuesByFilertId,DeleteFilterSettings:DeleteFilterSettings});function GetOptionsFromXML(elementNode,typeid){var request=$http({method:"get",url:"api/Metadata/GetOptionsFromXML/"+elementNode+"/"+typeid,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GettingEntityTypeHierarchyForAdminTree(EntityTypeID,ModuleID){var request=$http({method:"get",url:"api/Metadata/GettingEntityTypeHierarchyForAdminTree/"+EntityTypeID+"/"+ModuleID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GettingFilterEntityMember(formobj){var request=$http({method:"post",url:"api/Metadata/GettingFilterEntityMember/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GettingFilterAttribute(formobj){var request=$http({method:"post",url:"api/Metadata/GettingFilterAttribute/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function InsertFilterSettings(formobj){var request=$http({method:"post",url:"api/Planning/InsertFilterSettings/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetFilterSettingValuesByFilertId(FilterID){var request=$http({method:"get",url:"api/Planning/GetFilterSettingValuesByFilertId/"+FilterID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function DeleteFilterSettings(FilterId){var request=$http({method:"delete",url:"api/Planning/DeleteFilterSettings/"+FilterId,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){response.data.StatusCode=response.status;return(response.data);}}
app.service("CcdetailfilterService",['$http','$q',CcdetailfilterService]);})(angular,app);
