using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;
using BrandSystems.Marcom.Core.Access.Interface;

namespace BrandSystems.Marcom.Core.Planning
{

    /// <summary>
    /// Entity object for table 'PM_Entity'.
    /// </summary>

    internal class Entity : IEntity
    {
        #region Member Variables

        protected int _id;
        protected int _parentid;
        protected int _typeid;
        protected bool _active;
        protected string _uniquekey;
        protected int _entityid;
        protected bool _islock;
        protected string _name;
        protected int _version;
        protected int _level;
        protected int _activeEntityStateId;
        protected int _entityStateId;
        protected bool _enabledisableworkflow;
        protected IList<IAttributeData> _attributedata;
        protected IList<IEntityRoleUser> _entitymembers;
        protected IList<IEntityCostReleations> _entitycostcenters;
        protected IEntityPresentation _presentation;
        protected IList<IEntityPeriod> _periods;
        protected IList<IFinancial> _financials;
        protected IList<IFundingRequest> _fundingrequests;
        protected IObjective _objectives;
        protected ICostCenter _costcenter;
        protected string _ownername;



        #endregion

        #region Constructors
        public Entity() { }

        public Entity(int pId, int pParentid, int pTypeid, bool pActive, string pUniqueKey, int pEntityid, bool pIsLock, string pName, int pVersion, int pLevel, int pActiveEntityStateID, int pEntityStateID, bool pEnableDisableWorkflow, string pOwnerName)
        {
            this._id = pId;
            this._parentid = pParentid;
            this._typeid = pTypeid;
            this._active = pActive;
            this._uniquekey = pUniqueKey;
            this._entityid = pEntityid;
            this._islock = pIsLock;
            this._name = pName;
            this._version = pVersion;
            this._level = pLevel;
            this._activeEntityStateId = pActiveEntityStateID;
            this._entityStateId = pEntityStateID;
            this._enabledisableworkflow = pEnableDisableWorkflow;
            this._ownername = pOwnerName;
        }

        #endregion

        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public int Parentid
        {
            get { return _parentid; }
            set { _parentid = value; }

        }

        public int Typeid
        {
            get { return _typeid; }
            set { _typeid = value; }

        }

        public bool Active
        {
            get { return _active; }
            set { _active = value; }

        }

        public string UniqueKey
        {
            get { return _uniquekey; }
            set
            {
                if (value != null && value.Length > 450)
                    throw new ArgumentOutOfRangeException("UniqueKey", "UniqueKey value, cannot contain more than 450 characters");
                _uniquekey = value;
            }

        }

        public int EntityID
        {
            get { return _entityid; }
            set { _entityid = value; }

        }

        public bool IsLock
        {
            get { return _islock; }
            set { _islock = value; }

        }

        public string Name
        {
            get { return _name; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("Name", "Name value, cannot contain more than 250 characters");
                _name = value;
            }

        }

        public int Version
        {
            get { return _version; }
            set { _version = value; }

        }

        public int Level
        {
            get { return _level; }
            set { _level = value; }

        }

        public int ActiveEntityStateID
        {
            get { return _activeEntityStateId; }
            set { _activeEntityStateId = value; }
        }
        public int EntityStateID
        {
            get { return _entityStateId; }
            set { _entityStateId = value; }
        }
        public bool EnableDisableWorkflow
        {
            get { return _enabledisableworkflow; }
            set { _enabledisableworkflow = value; }

        }
        public IList<IAttributeData> AttributeData
        {
            get { return _attributedata; }
            set { _attributedata = value; }
        }
        public IList<IEntityRoleUser> EntityMembers
        {
            get { return _entitymembers; }
            set { _entitymembers = value; }
        }
        public IList<IEntityCostReleations> EntityCostcenters
        {
            get { return _entitycostcenters; }
            set { _entitycostcenters = value; }
        }
        public IEntityPresentation Presentation
        {
            get { return _presentation; }
            set { _presentation = value; }

        }

        public IList<IEntityPeriod> Periods
        {
            get { return _periods; }
            set { _periods = value; }

        }

        public IList<IFinancial> Financials
        {
            get { return _financials; }
            set { _financials = value; }

        }

        public IList<IFundingRequest> FundingRequests
        {
            get { return _fundingrequests; }
            set { _fundingrequests = value; }

        }

        public IObjective Objectives
        {
            get { return _objectives; }
            set { _objectives = value; }

        }
        public ICostCenter Costcenter
        {
            get { return _costcenter; }
            set { _costcenter = value; }
        }


        public string OwnerName
        {
            get { return _ownername; }
            set
            {
                _ownername = value;
            }

        }


        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            Entity castObj = null;
            try
            {
                castObj = (Entity)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                this._id.Equals(castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }
    public class UIEntitydescendants
    {
        public string name { get; set; }
        public int id { get; set; }
        public bool checked1 { get; set; }
        public List<UIEntitydescendants> children { get; set; }
    }
}
