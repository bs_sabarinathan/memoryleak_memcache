using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    /// <summary>
    /// IUserMailSubscription interface for table 'CM_UserMailSubscription'.
    /// </summary>
    public interface IUserMailSubscription
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int Userid
        {
            get;
            set;

        }

        DateTimeOffset LastSentOn
        {
            get;
            set;

        }

        DateTimeOffset LastUpdatedOn
        {
            get;
            set;

        }

        bool IsEmailEnable
        {
            get;
            set;

        }

        string DayName
        {
            get;
            set;

        }

        TimeSpan Timing
        {
            get;
            set;

        }

        bool RecapReport
        {
            get;
            set;

        }

        
        

        #endregion
    }
}
