﻿
// Create an application module for our application.
var app = angular.module("app", ['ui.select2', 'ngRoute', "ngResource", 'ngCookies', 'ngGrid', '$strap.directives', 'angular-accordion']);

app.config([
    '$compileProvider',
    function ($compileProvider) {
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|mailto|devexpress.reportserver):/);
        // Angular before v1.2 uses $compileProvider.urlSanitizationWhitelist(...)
    }
]);

app.directive('a', function () {
    return {
        restrict: 'E',
        link: function (scope, elem, attrs) {
            var attr = elem.attr('data-toggle');
            if (typeof attr !== 'undefined' && attr !== false) {
                elem.on('click', function (e) {
                    e.preventDefault();
                });
            }
        }
    };
});

var GlobalIsWorkspaces = false;
var NewsFeedUniqueTimer = null;
var NewsFeedUniqueTimerForTask = null;
var AdditionalSettingsObj = [];

var RememberPlanFilterID = 0;
var RememberPlanFilterAttributes = [];
var RemeberPlanFilterName = "No filter applied";

var RememberCCFilterID = 0;
var RememberCCFilterAttributes = [];
var RemeberCCFilterName = "No filter applied";

var RememberObjFilterID = 0;
var RememberObjFilterAttributes = [];
var RemeberObjFilterName = "No filter applied";

var PreviewGeneratorTimer = null;
var EditPreviewGeneratorTimer = null;
var GlobalAssetEditID = 0;

var tempLightboxguid = []; 
//$.ajax({
//    type: "GET",
//    url: "common/GetAdditionalSettings",
//    contentType: "application/json",
//    async: false,
//    success: function (tempdata) {

//        Defaultdate = tempdata.Response[0].SettingValue;
//        AdditionalSettingsObj = tempdata.Response;
//        app.value('$strapConfig', {
//            datepicker: {
//                format: Defaultdate
//            }
//        });
//    },
//    error: function (data) {
//    }
//});


//xeditable mode setting
$.fn.editable.defaults.mode = 'Popup';
//Custom Angular Routes from server side
var data;
var IsGlobalAdmin = false;
var GlobalUserDateFormat = '';
$.ajax({
    type: "GET",
    url: "api/common/GetCalendarNavigationConfig",
    contentType: "application/json",
    async: false,
    success: function (tempdata) {
        data = tempdata.Response;
        eval(data);
    },
    error: function (data) {
    }
});

$.ajax({
    type: "GET",
    url: "api/access/CheckUserIsAdmin",
    contentType: "application/json",
    async: false,
    success: function (tempdata) {
        if (tempdata.Response != null && tempdata.Response != false) {
            IsGlobalAdmin = true;
        }
    },
    error: function (data) {
    }
});

var UserTime;
function NotifySuccess(msg) {
    $('.top-right').notify({ message: { text: msg }, type: 'success' }).show();
}
function NotifyError(msg) {
    $('.top-right').notify({ message: { text: msg }, type: 'danger' }).show();
}
var IsLock = false;
var SearchEntityID = 0;
var EntityTypeID = 0;
var SearchEntityLevel = "0";
Number.prototype.formatMoney = function (c, d, t) {
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

String.prototype.contains = function (chr) {
    var str = this;
    if (str.indexOf(chr) == -1)
        return false;
    else
        return true;
};

if (!String.prototype.startsWith) {
    Object.defineProperty(String.prototype, 'startsWith', {
        enumerable: false,
        configurable: false,
        writable: false,
        value: function (searchString, position) {
            position = position || 0;
            return this.indexOf(searchString, position) === position;
        }
    });
}

//System defined attributes
var SystemDefiendAttributes = {
    FiscalYear: 1,
    Name: 68,
    Owner: 69,
    MilestoneStatus: 67,
    MilestoneEntityID: 66,
    ObjectiveType: 107,
    AssignedAmount: 59,
    Status: 70,
    EntityStatus: 71,
    ObjectiveStatus: 108,
    MyRoleGlobalAccess: 74,
    MyRoleEntityAccess: 75,
    EntityOnTimeStatus: 77,
    ParentEntityName: -10
}
//Financial Funding costcenter systemdefined attributes
var finAttributesEnum = (function () { var finAttributesEnum = { 1: "Planned", 2: "In requests", 3: "Approved allocations", 4: "Name" }; return { get: function (value) { return finAttributesEnum[value]; } } })();
var finPurchaseOrderAttributesEnum = (function () { var finPurchaseOrderAttributesEnum = { 5: { Name: "PONumber", Language: "Res_1769" }, 6: { Name: "strAmount", Language: "Res_1746" }, 7: { Name: "strCreatedDate", Language: "Res_31" }, 8: { Name: "SupplierName", Language: "Res_22" }, 9: { Name: "Description", Language: "Res_1736" }, 10: { Name: "RequesterName", Language: "Res_1770" }, 11: { Name: "strAppprovalDate", Language: "Res_1771" }, 12: { Name: "strSendDate", Language: "Res_1772" }, 13: { Name: "ApproverName", Language: "Res_1770" } }; return { get: function (value) { return finPurchaseOrderAttributesEnum[value]; } } })();
var finSpentAttributesEnum = (function () { var finSpentAttributesEnum = { 15: { Name: "InvoiceNumber", Language: "Res_1769" }, 16: { Name: "strCreatedDate", Language: "Res_1746" }, 17: { Name: "POID", Language: "Res_31" }, 18: { Name: "SupplierName", Language: "Res_22" }, 19: { Name: "Description", Language: "Res_1736" }, 20: { Name: "Amount", Language: "Res_1736" } }; return { get: function (value) { return finSpentAttributesEnum[value]; } } })();
var assetAction = (function () { var assetAction = { 1: "AddtoLightBox", 2: "Task", 3: "ApprovalTask", 4: "PublishDAM", 5: "DuplicateAsset", 6: "Delete", 7: "Download" }; return { get: function (value) { return assetAction[value]; } } })();

//System defined attributes
var SystemDefiendFinancialMetadata = {
    FundingCostcenter: 3,
    PO: 1,
    Spent: 2
}

//EntityTypes
var SystemDefinedEntityTypes = {
    Milestone: 1,
    Objective: 10,
    CostCentre: 5,
    AdditionalObjective: 11
}

//AttributeTypes
var AttributeTypes = {
    Owner: 3,
    ObjectiveType: 4,
    Period: 10
}

//Show all Remember
var ShowAllRemember = {
    Activity: true,
    CostCentre: true,
    Objective: true
}

var ForecstDivisonIds = {
    Yearly: 1,
    Monthly: 2,
    Quaterly: 3,
    Half_yearly: 4
}

var EntityRoles =
{
    Owner: 1,
    Editer: 2,
    Viewer: 3,
    ExternalParticipate: 5,
    BudgerApprover: 8

}

var Defaultdate = '';

function Mimer(Extenstion) {
    switch (Extenstion) {
        case "gif":
        case "png":
        case "jpeg":
        case "bmp":
        case "tiff":
        case "emf":
        case "eps":
        case "jpg":
        case "psd":
        case "tif":
            return "Image"
            break;
        case "pdf":
        case "doc":
        case "docx":
        case "ppt":
        case "pptx":
        case "xls":
        case "xlsx":
        case "txt":
        case "xml":
            return "Document"
            break;
        case "aac":
        case "mka":
        case "mp3":
        case "ogg":
        case "wav":
            return "Audio"
            break;
        case "mov":
        case "wmv":
        case "mp4":
        case "flv":
        case "webm":
        case "avi":
        case "mkv":
        case "mkv":
        case "m4v":
            return "Video"
            break;
        case "zip":
        case "rar":
        case "7z":
            return "Zip"
            break;
        default:
            return "Other";
    }
}
function dateFormat(date, format) {
    format = format.toUpperCase();
    var dateSample = date;
    date = new Date(Date.parse(date.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"), "yyyy-MM-dd"));
    if (date.getDate() == "1" && date.getMonth() == "0" && date.getFullYear() == '1970') {
        date = new Date(dateSample);
    }
    format = format.replace("DD", (date.getDate() < 10 ? '0' : '') + date.getDate()); // Pad with '0' if needed
    format = format.replace("D", (date.getDate() < 10 ? '' : '') + date.getDate());
    format = format.replace("MM", (date.getMonth() < 9 ? '0' : '') + (date.getMonth() + 1)); // Months are zero-based
    format = format.replace("M", (date.getMonth() < 9 ? '' : '') + (date.getMonth() + 1));
    format = format.replace("YYYY", date.getFullYear());
    return format;
}

//This function to return given string into UTC Date object. Input date string must be in 'yyyy mm dd' format
function ConvertStringToDate(date) {
    var dateVal;
    if (date.contains('/'))
        dateVal = date.split('/');
    else
        dateVal = date.split('-');
    return date = new Date(Date.UTC(dateVal[0], dateVal[1] - 1, dateVal[2]))
}

//This function to return given string into UTC Date object. Need to pass input date format to get converted properly
function ConvertStringToDateByFormat(date, inputformat) {
    // Calculate date parts and replace instances in format string accordingly
    var dateSample = date;
    var dateVal;
    if (date.contains('/')) {
        dateVal = date.split('/');
    } else {
        dateVal = date.split('-');
    }
    var defaultFormat = inputformat.toUpperCase();
    if (defaultFormat.startsWith('D')) {
        date = new Date(Date.UTC(dateVal[2], dateVal[1] - 1, dateVal[0]));
    } else if (defaultFormat.startsWith('M')) {
        date = new Date(Date.UTC(dateVal[2], dateVal[0] - 1, dateVal[1]));
    } else {
        date = new Date(Date.UTC(dateVal[0], dateVal[1] - 1, dateVal[2]));
    }
    return date;
}

//This function is to Convert a Date Object to String. This always retures in yyyy-MM-dd format
function ConvertDateToString(date) {
    if (typeof date == "string")
        date = new Date(date);
    return (date.getFullYear() + "-" + ((date.getMonth() < 9 ? '0' : '') + (date.getMonth() + 1)) + "-" + ((date.getDate() < 10 ? '0' : '') + date.getDate()));
}

//This function is to Convert a Date string to String. This always accepts in yyyy-MM-dd format and retures as per the Global date settings format
function ConvertDateFromStringToString(date) {
    var dateParts;
    if (date.contains('/'))
        dateParts = date.split('/');
    else
        dateParts = date.split('-');
    var dateObj = new Date(Date.UTC(dateParts[0], dateParts[1] - 1, dateParts[2]));
    var format = GlobalUserDateFormat;
    format = format.replace("DD", (dateObj.getDate() < 10 ? '0' : '') + dateObj.getDate()); // Pad with '0' if needed
    format = format.replace("D", (dateObj.getDate() < 10 ? '' : '') + dateObj.getDate());
    format = format.replace("MM", (dateObj.getMonth() < 9 ? '0' : '') + (dateObj.getMonth() + 1)); // Months are zero-based
    format = format.replace("M", (dateObj.getMonth() < 9 ? '' : '') + (dateObj.getMonth() + 1));
    format = format.replace("YYYY", dateObj.getFullYear());
    return format;
}

//This function is to Convert a Date string to String by taking global date format as input format. This always retures in yyyy-MM-dd format
function ConvertDateFromStringToStringByFormat(date) {
    var dateSample = date;
    var dateVal;
    if (date.contains('/')) {
        dateVal = date.split('/');
    } else {
        dateVal = date.split('-');
    }
    var defaultFormat = GlobalUserDateFormat;
    if (defaultFormat.startsWith('D')) {
        dateSample = dateVal[2] + "-" + dateVal[1] + "-" + dateVal[0];
    } else if (defaultFormat.startsWith('M')) {
        dateSample = dateVal[2] + "-" + dateVal[0] + "-" + dateVal[1];
    } else {
        dateSample = dateVal[0] + "-" + dateVal[1] + "-" + dateVal[2];
    }
    return dateSample;
}

function FormatStandard(date, format) {
    format = format.toUpperCase();
    var dateSample = date;
    var dateVal;
    if (date.contains('/'))
        dateVal = date.split('/');
    else
        dateVal = date.split('-');
    date = new Date(Date.UTC(dateVal[0], dateVal[1] - 1, dateVal[2]))
    format = format.replace("DD", (date.getDate() < 10 ? '0' : '') + date.getDate()); // Pad with '0' if needed
    format = format.replace("D", (date.getDate() < 10 ? '' : '') + date.getDate());
    format = format.replace("MM", (date.getMonth() < 9 ? '0' : '') + (date.getMonth() + 1)); // Months are zero-based
    format = format.replace("M", (date.getMonth() < 9 ? '' : '') + (date.getMonth() + 1));
    format = format.replace("YYYY", date.getFullYear());
    return format;
}

function CreateHisory(IDList) {
    var trackValue = IDList[0];
    for (ti = 1; ti < IDList.length; ti++) {
        trackValue += ',' + IDList[ti];
    }
    $.cookie('ListofEntityID', IDList);
    var Guid = GenerateGUID();
    InsertGUID(Guid, trackValue);
    return Guid;

}

function GenerateGUID() {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c == 'x' ? r : (r & 0x7 | 0x8)).toString(16);
    });
    return uuid;
};

function InsertGUID(Guid, Value) {

    var ApplicationUrlTrack = {
        "TrackID": "" + Guid + "",
        "TrackValue": "" + Value + ""
    };
    var TrackData = JSON.stringify(ApplicationUrlTrack);

    $.ajax({
        type: "POST",
        url: "api/common/InsertUpdateApplicationUrlTrack",
        contentType: "application/json",
        data: TrackData,
        async: true,
        success: function (tempdata) {
            if (tempdata.Response != false) {
            }
        },
        error: function (data) {
        }
    });

}

function GetGuidValue(trackid) {
    var guid = "";
    $.ajax({
        type: "GET",
        url: "api/common/GetApplicationUrlTrackByID/" + trackid,
        contentType: "application/json",
        data: { TrackID: trackid },
        async: false,
        success: function (tempdata) {
            if (tempdata.Response != null) {
                guid = tempdata.Response;
            }
        },
        error: function (data) {
        }
    });
    return guid;
}




$(document).on("change", "#EntityMetadata select", function () {
    var ActualAttributeID = $(this).attr('id')
    var AttrID = $(this).attr('id').replace('ListSingleSelection_', '').replace('DropDownTree_', '');
    var OptionVal = $('option[value="' + $(this).val() + '"]', this).text().toString().trim();
    //HideDynamicControls(13, AttrID, OptionVal)
});



function FixPace() {
    if (window.oldProgress == undefined) {
        window.oldProgress = $('.pace-progress').attr('data-progress-text').replace('%', '');
    } else {
        var newProgress = $('.pace-progress').attr('data-progress-text').replace('%', '');
        if (parseFloat(newProgress) < parseFloat(window.oldProgress)) {
            window.oldProgress = newProgress;
        }
        else if ((parseFloat(newProgress) - parseFloat(window.oldProgress)) < 1 && parseFloat(newProgress) > 98) {
            $('body').removeClass('pace-running').addClass('pace-done');
            $('body .pace').removeClass('pace-active').addClass('pace-inactive');
        } else {
            window.oldProgress = newProgress;
        }
    }
}

setInterval('FixPace();', 500);

function isIE() {
    var myNav = navigator.userAgent.toLowerCase();
    return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
}


$(document).on("mouseover", "#separator", function () {
    if (!$(this).hasClass('ui-draggable')) {
        $("#separator").draggable({
            axis: "x",
            grid: [20, 20],
            stop: function (event, ui) {
                if (event.clientX > 599) {
                    $('#content').css('margin-left', 600 + 'px');
                    $('#separator').css('left', '0');
                    $('#left').css('width', 600 + 'px');
                } else if (event.clientX < 229) {
                    $('#content').css('margin-left', 230 + 'px');
                    $('#separator').css('left', '0');
                    $('#left').css('width', 230 + 'px');
                    if ($('#TreeOpenClose').find('i').hasClass('icon-chevron-left')) {
                        TreeOpenCloseClick($('#TreeOpenClose'));
                    } else {
                        $('#content').css('margin-left', 62 + 'px');
                        $('#separator').css('left', '0');
                        $('#left').css('width', 62 + 'px');
                    }
                } else {
                    if ($('#TreeOpenClose').find('i').hasClass('icon-chevron-right')) {
                        TreeOpenCloseClick($('#TreeOpenClose'));
                    }
                    $('#content').css('margin-left', (event.clientX + 1) + 'px');
                    $('#separator').css('left', '0');
                    $('#left').css('width', (event.clientX + 1) + 'px');
                }
            }
        });
    }
});

$(document).on("mouseover", "#attachmentseparator", function () {
    if (!$(this).hasClass('ui-draggable')) {
        $("#attachmentseparator").draggable({
            axis: "x",
            grid: [20, 20],
            stop: function (event, ui) {
                var ClientX = event.clientX - ($('#left').width() + 38);
                if (ClientX > 599) {
                    $('#right_DamView').css('margin-left', 610 + 'px');

                    $('#left_folderTree').css('width', 600 + 'px');
                    $('#attachmentseparator').attr('style', 'right: -1px;');
                } else if (ClientX < 229) {
                    $('#right_DamView').css('margin-left', 240 + 'px');

                    $('#left_folderTree').css('width', 230 + 'px');
                    $('#attachmentseparator').attr('style', 'right: -1px;');
                } else {
                    $('#right_DamView').css('margin-left', (ClientX + 1 + 10) + 'px');

                    $('#left_folderTree').css('width', (ClientX + 1) + 'px');
                    $('#attachmentseparator').attr('style', 'right: -1px;');
                }
            }
        });
    }
});

function TreeOpenCloseClick(obj) {
    if ($(obj).find('i').hasClass('icon-chevron-left')) {
        $(obj).attr('data-width', $('#left').width());
        $('#left .ActionBtnArea').next().hide();
        $('#treeSettings').hide();
        $(obj).find('i').removeClass('icon-chevron-left').addClass('icon-chevron-right');
        $('#content').css('margin-left', 62 + 'px');
        $('#separator').css('left', '0');
        $('#left').css('width', 62 + 'px');
    } else {
        $('#left .ActionBtnArea').next().show();
        $('#treeSettings').show();
        $(obj).find('i').removeClass('icon-chevron-right').addClass('icon-chevron-left');
        $('#content').css('margin-left', $(obj).attr('data-width') + 'px');
        $('#separator').css('left', '0');
        $('#left').css('width', $(obj).attr('data-width') + 'px');
    }
}

$(document).on("click", "#TreeOpenClose", function () {

    TreeOpenCloseClick($(this));

});


function RecursiveUnbindAndRemove($jElement) { // remove this element's and all of its children's click events 
    $jElement.children().each(function () { RecursiveUnbindAndRemove($(this)); });
    $jElement.unbind();
    $jElement.remove();
}

window.CreateDOCGeneratedAsset = "", window.CreateversionDOCGeneratedAsset = "";
//$.ajax({
//    type: "GET",
//    url: "common/GetOptimakerAddresspoints",
//    contentType: "application/json",
//    async: false,
//    success: function (tempdata) {
//        var strArr = tempdata.Response.split(",");
//        if (strArr.length > 0) {
//            window.CreateDOCGeneratedAsset = strArr[0];
//            window.CreateversionDOCGeneratedAsset = strArr[1];
//        }

//    },
//    error: function (data) {
//    }
//});

window.entityId = 0, window.folderid = 0, window.isword = false, window.createdbyid = 0, window.assetId = 0;
function setGlobalOptimakerSettings(eid, fid, isword, createdby) {
    window.entityId = eid, window.folderid = fid, window.isword = isword, window.createdbyid = createdby;
}

function setAssetIdfrGlobalOptimakerSettings(assetid, eid, createdBy) {
    window.assetId = assetid;
    window.entityId = eid;
    window.createdbyid = createdBy;
}

