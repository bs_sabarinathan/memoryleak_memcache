﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning;
using BrandSystems.Marcom.Core.Access.Interface;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
   public interface IBaseEntity
    {
        #region Public Properties

        int Id
        {
            get;
            set;
        }

        int Parentid
        {
            get;
            set;

        }

        int Typeid
        {
            get;
            set;

        }

        bool Active
        {
            get;
            set;

        }

        string UniqueKey
        {
            get;
            set;

        }

        int EntityID
        {
            get;
            set;

        }

        bool IsLock
        {
            get;
            set;

        }

        string Name
        {
            get;
            set;

        }
        int Version
        {
            get;
            set;
        }
        int Level 
        { 
            get; 
            set; 
        }
        int ActiveEntityStateID 
        { 
            get; 
            set; 
        }
        int EntityStateID 
        { 
            get; 
            set; 
        }
        bool EnableDisableWorkflow
        {
            get;
            set;
        }
        #endregion
    }
}
